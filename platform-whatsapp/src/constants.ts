import { WAMessageStatus } from '@adiwajshing/baileys'

// Date.distantFuture === January 1, 4001 at 12:00:00 AM GMT
export const CHAT_MUTE_DURATION_S = 64092211200

export const TEN_YEARS_IN_SECONDS = 315569520

export const READ_STATUS = [WAMessageStatus.READ, WAMessageStatus.PLAYED]
