// const TerserPlugin = require('terser-webpack-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')
const path = require('path')

module.exports = {
  mode: 'production',
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  entry: path.resolve(__dirname, 'src/bridge/entry.ts'),
  output: {
    path: path.resolve(__dirname, 'binaries'),
    filename: 'bridge-entry.js',
    libraryTarget: 'commonjs2',
  },
  module: {
    rules: [
      {
        test: /\.d\.ts?$/,
        exclude: /node_modules/,
        use: 'null-loader',
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules|\.d\.ts?$/,
        use: [{
          loader: 'ts-loader',
          options: {
            configFile: 'tsconfig.json',
            transpileOnly: true
          },
        }],
      },
    ],
  },
  resolve: {
    alias: {
      'Signal-Desktop': path.resolve(__dirname, 'Signal-Desktop'),
    },
    // modules: [
    //   path.resolve(__dirname, 'node_modules'),
    //   path.resolve(__dirname, 'Signal-Desktop/node_modules'),
    // ],
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
  },
  externals: [
    '@signalapp/libsignal-client',
    '@signalapp/libsignal-client/zkgroup',
    'better-sqlite',
    'fs-xattr',
    // hjson,toml,cson,properties,x2js is for the config npm module
    'hjson',
    'toml',
    'cson',
    'properties',
    'x2js',
  ],
  plugins: [
    new ForkTsCheckerWebpackPlugin(),
  ],
  optimization: {
    nodeEnv: false,
    // minimize: false,
    // minimizer: [
    //   new TerserPlugin({
    //     parallel: true,
    //     // sourceMap: true,
    //     terserOptions: {
    //       ecma: 2022,
    //       keep_fnames: true,
    //     },
    //   }),
    // ],
    // splitChunks: {
    //   chunks: 'all',
    // },
  },
  watchOptions: {
    ignored: /node_modules/,
  },
}
