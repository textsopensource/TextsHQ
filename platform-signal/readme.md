# platform-signal

* Depends on [our fork](https://github.com/TextsHQ/Signal-Desktop) of [witchent's fork](https://github.com/witchent/Signal-Desktop/tree/1.35.2) of [Signal Desktop](https://github.com/signalapp/Signal-Desktop).
* Adapted code from [witchent's node-signal-client](https://github.com/witchent/node-signal-client/tree/1.35.2).

## Building native deps

Since platform-signal runs with Electron which has a [different ABI](https://github.com/electron/node-abi), you'll be asked to rebuild native deps, but prebuilt binaries are available in the desktop app so you can use `scripts/replace-native-deps.sh` instead.

## src/ts

These three files are copied from Signal-Desktop:

- background.ts
- window.d.ts
- textsecure.d.ts

Whenever possible, try not editing them, but add things to preload-global.ts or preload-signal.ts instead.

### Updating background.ts

After bumping Signal-Desktop, run `patch.sh` to update `background.ts`, if the patch applies cleanly, commit the changes. Otherwise, change `background.ts` manually, then regenerate `backgroud.diff` with `diff.sh`
