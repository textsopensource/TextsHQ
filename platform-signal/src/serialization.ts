import v8 from 'v8'

/**
 * Functions can't be serialized.
 * https://nodejs.org/api/child_process.html#child_process_advanced_serialization
 */
export function sanitizeObjectForSerialization<T>(obj: T) {
  if (obj == null) return obj
  const objType = typeof obj
  if (objType === 'function') return undefined
  if (objType !== 'object') return obj
  if (Buffer.isBuffer(obj)) return obj
  // @ts-expect-error: isByteBuffer is not declared in window.d.ts.
  if (window.dcodeIO.ByteBuffer.isByteBuffer(obj)) return obj.toBuffer()
  if (Array.isArray(obj)) return obj.map(i => sanitizeObjectForSerialization(i))
  const ret = {}
  for (const [key, value] of Object.entries(obj)) {
    ret[key] = sanitizeObjectForSerialization(value)
  }
  return ret
}

export function findUnserializableValue(obj: any) {
  require('lodash').cloneDeepWith(obj, (value: any) => {
    try {
      v8.serialize(value)
    } catch (err) {
      console.log('failed to serialize', { value }, typeof value, value?.constructor.name)
    }
  })
}
