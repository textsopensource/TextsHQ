import React, { useState, useEffect } from 'react'
import QRCode from 'qrcode.react'
import type { AuthProps } from '@textshq/platform-sdk'

type Status = 'none' | 'qrcode' | 'authenticating' | 'success'

export const AuthForm: React.FC<AuthProps> = ({ api, login }) => {
  const [url, setUrl] = useState(null)
  const [status, setStatus] = useState<Status>('none')
  useEffect(() => {
    api.onLoginEvent(({ type, data }) => {
      switch (type) {
        case 'link-url':
          setStatus('qrcode')
          setUrl(data)
          break
        case 'link-authenticating':
          setStatus('authenticating')
          break
        case 'link-success':
          setStatus('success')
          login({ username: data })
          break
        default:
      }
    })
    api.login()
  }, [api])
  if (status === 'qrcode' && url) {
    return (
      <div>
        <div className="list">
          <div><span>1</span>Open the Signal app on your phone</div>
          <div><span>2</span>Go to Settings → Linked Devices</div>
          <div><span>3</span>Tap "Link New Device" on iPhone or the "+" button on Android</div>
          <div><span>4</span>Scan the QR code with your phone</div>
        </div>
        <div className="text-center">
          <QRCode includeMargin value={url} size={256} />
        </div>
        {(process.platform as string) === 'ios' && <button onClick={() => window.open(url)}>Link with Signal.app</button>}
        <footer>Signal doesn't transfer your conversation history to new linked devices for security.</footer>
      </div>
    )
  }
  if (status === 'authenticating') {
    return <p>Authenticating...</p>
  }
  if (status === 'success') {
    return <p>Authenticated, loading...</p>
  }
  return <p>Loading...</p>
}

export default AuthForm
