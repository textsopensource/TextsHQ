/**
 * Preload globals, anything specific to Whisper and Signal should be put in
 * preload-signal.ts.
 */

import _ from 'lodash'
import events from 'events'
import semver from 'semver'

declare let window: any

// @ts-expect-error
global.window = global
window.XMLHttpRequest = require('xhr2')
window.Blob = require('cross-blob')

window.btoa = (str: string) => Buffer.from(str).toString('base64')

window.moment = require('moment')
window.PQueue = require('p-queue').default
window._ = require('underscore')
window.Backbone = require('backbone')

// const setGlobalIndexedDbShimVars = require('indexeddbshim')
// setGlobalIndexedDbShimVars()
require('fake-indexeddb/auto')

const IS_DEV = process.env.NODE_ENV === 'development'

const noop = () => {}
const proxyThatReturnsNoopForEverything = (name: string, og = {}) => new Proxy(og, {
  get: (target, key) => {
    if (key in target) return target[key]
    if (IS_DEV) console.log(`[preload-global] returning noop function for ${name}.${key.toString()}`)
    return noop
  },
})

window.emitter = new events.EventEmitter()
window.getEnvironment = () => 'production'

window.ipcEmitter = {
  setMaxListeners() {},
  on(name: string, fn: Function) {
    const event = {
      sender: {
        send(name: string, ...args) {
          window.emitter.emit(name, ...args)
        },
      },
    }
    window.emitter.on(name, (...args) => fn(event, ...args))
  },
  once(name: string, fn: Function) {
    const event = {}
    window.emitter.once(name, (...args) => fn(event, ...args))
  },
  send(name: string, ...args) {
    window.emitter.emit(name, ...args)
  },
}

window.emitter.on('ensure-file-permissions', () => {
  setTimeout(() => {
    window.emitter.emit('ensure-file-permissions-done')
  })
})

window.$ = proxyThatReturnsNoopForEverything('window.$')
window.getBuiltInImages = () => Promise.resolve([])
window.getTitle = () => ''
window.setBadgeCount = count => ''
window.updateTrayIcon = unreadCount => ''

window.navigator = {
  onLine: true,
  userAgent: 'nodejs',
  appName: 'nodejs',
  hardwareConcurrency: 1,
}

function now() {
  const date = new Date()
  return date.toJSON()
}

function logAtLevel(level, prefix, ...args) {
  console.log(prefix, now(), ...args)
}

window.log = {
  fatal: _.partial(logAtLevel, 'fatal', 'FATAL'),
  error: _.partial(logAtLevel, 'error', 'ERROR'),
  warn: _.partial(logAtLevel, 'warn', 'WARN '),
  info: _.partial(logAtLevel, 'info', 'INFO '),
  debug: _.partial(logAtLevel, 'debug', 'DEBUG'),
  trace: _.partial(logAtLevel, 'trace', 'TRACE'),
}

window.i18n = (locale, messages) => ''

// need this to avoid opaque origin error in indexeddb shim
window.location = { ancestorOrigins: {}, href: 'http://localhost/', origin: 'http://localhost', protocol: 'http:', host: 'localhost', hostname: 'localhost', port: '80', pathname: '/', search: '', hash: '' }

window.Event = function (type) {
  this.type = type
}

window.FileReader = function () {
  this.readAsArrayBuffer = blob => {
    this.result = blob
    this.onloadend()
  }
}

// Redux actions are not needed for our use case but to stop warnings:
// Adapted from background.js initializeRedux
window.reduxStore = proxyThatReturnsNoopForEverything('reduxStore', {
  app: proxyThatReturnsNoopForEverything('reduxStore.app'),
  badges: proxyThatReturnsNoopForEverything('reduxStore.badges'),
  conversations: proxyThatReturnsNoopForEverything('reduxStore.conversations'),
  stickers: proxyThatReturnsNoopForEverything('reduxStore.stickers'),
})
window.reduxStore.getState = () => window.reduxStore

// adapted from Signal-Desktop/ts/background.ts
window.reduxActions = {
  accounts: proxyThatReturnsNoopForEverything('accounts'),
  app: proxyThatReturnsNoopForEverything('app'),
  audioPlayer: proxyThatReturnsNoopForEverything('audioPlayer'),
  audioRecorder: proxyThatReturnsNoopForEverything('audioRecorder'),
  badges: proxyThatReturnsNoopForEverything('badges'),
  calling: proxyThatReturnsNoopForEverything('calling'),
  composer: proxyThatReturnsNoopForEverything('composer'),
  conversations: proxyThatReturnsNoopForEverything('conversations'),
  crashReports: proxyThatReturnsNoopForEverything('crashReports'),
  emojis: proxyThatReturnsNoopForEverything('emojis'),
  expiration: proxyThatReturnsNoopForEverything('expiration'),
  globalModals: proxyThatReturnsNoopForEverything('globalModals'),
  items: proxyThatReturnsNoopForEverything('items'),
  linkPreviews: proxyThatReturnsNoopForEverything('linkPreviews'),
  network: proxyThatReturnsNoopForEverything('network'),
  safetyNumber: proxyThatReturnsNoopForEverything('safetyNumber'),
  search: proxyThatReturnsNoopForEverything('search'),
  stickers: proxyThatReturnsNoopForEverything('stickers'),
  stories: proxyThatReturnsNoopForEverything('stories'),
  updates: proxyThatReturnsNoopForEverything('updates'),
  user: proxyThatReturnsNoopForEverything('user'),
}

window.crashReports = proxyThatReturnsNoopForEverything('window.crashReports')

window.nodeSetImmediate = setImmediate
window.cancelIdleCallback = noop
window.requestIdleCallback = fn => {
  fn({ didTimeout: true, timeRemaining: () => 100 })
  return 1
}
window.WebSocket = {}
window.getSfuUrl = noop
window.registerForActive = noop
window.readyForUpdates = noop
window.logAppLoadedEvent = noop
window.isActive = () => true

window.dispatchEvent = event => window.emitter.emit(event.type)

// Flags for testing
window.GV2_ENABLE_SINGLE_CHANGE_PROCESSING = true
window.GV2_ENABLE_CHANGE_PROCESSING = true
window.GV2_ENABLE_STATE_PROCESSING = true

window.GV2_MIGRATION_DISABLE_ADD = false
window.GV2_MIGRATION_DISABLE_INVITE = false

window.isBeforeVersion = (toCheck, baseVersion) => {
  try {
    return semver.lt(toCheck, baseVersion)
  } catch (error) {
    window.log.error(
      `isBeforeVersion error: toCheck: ${toCheck}, baseVersion: ${baseVersion}`,
      error && error.stack ? error.stack : error,
    )
    return true
  }
}
window.isAfterVersion = (toCheck, baseVersion) => {
  try {
    return semver.gt(toCheck, baseVersion)
  } catch (error) {
    console.error(
      `isBeforeVersion error: toCheck: ${toCheck}, baseVersion: ${baseVersion}`,
      error && error.stack ? error.stack : error,
    )
    return true
  }
}

window.logAppLoadedEvent = noop

window.DEFAULT_CONVERSATION_COLOR = {
  color: 'ultramarine',
}

window.i18n = x => x
window.i18n.getLocale = () => 'en_US'
window.getLocale = () => 'en_US'

window.setAutoHideMenuBar = noop
window.setMenuBarVisibility = noop
window.addEventListener = noop
