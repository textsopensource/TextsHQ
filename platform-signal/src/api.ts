/* eslint-disable global-require */
import os from 'os'
import url from 'url'
import path from 'path'
import { promises as fs } from 'fs'
import {
  PlatformAPI,
  OnServerEventCallback,
  LoginResult,
  Paginated,
  Thread,
  Message,
  MessageContent,
  MessageSendOptions,
  CurrentUser,
  PaginationArg,
  AccountInfo,
  ActivityType,
  texts,
  OnConnStateChangeCallback,
  ConnectionStatus,
  User,
  ServerEventType,
  PhoneNumber,
  ServerEvent,
  StateSyncEvent,
  SerializedSession,
} from '@textshq/platform-sdk'
import urlRegex from 'url-regex'
import { memoize, pick } from 'lodash'
import { setTimeout as setTimeoutAsync } from 'timers/promises'
import type { EventEmitter } from 'events'

import './windows'
import './preload-global'
import { AttachmentType, mapMessage, mapServerEvent, mapThread, convertUserMentions, getFilePath, mapThreadMetaOnly } from './mappers'
import { fetchLinkPreview } from './util'
import type { LinkPreviewType } from './types/Message'
// import './ts/window.d'

declare global {
  interface Window {
    emitter: EventEmitter
  }
}

function createPromise<T>() {
  let promiseResolve: (value: T | PromiseLike<T>) => void
  const promise = new Promise<T>(resolve => { promiseResolve = resolve })
  return {
    resolve: promiseResolve,
    promise,
  }
}

declare let window: any

const DEBUG = !!process.env.DEBUG
const DEVICE_NAME = `${os.hostname()} on Texts`

async function getConversation(conversationId: string) {
  const conversation = await window.ConversationController.get(conversationId)
  return conversation?.attributes
}

async function enqueueReactionForSend(messageId, emoji, remove) {
  const { enqueueReactionForSend } = require('Signal-Desktop/ts/reactions/enqueueReactionForSend')
  return enqueueReactionForSend({
    messageId,
    emoji,
    remove,
  })
}

async function getProfile(conversationId: string) {
  const { getProfile } = require('Signal-Desktop/ts/util/getProfile')
  const conversation = window.ConversationController.get(conversationId)
  await getProfile(conversation.get('uuid'), conversation.get('e164'))
}

function waitForNextSyncComplete(): Promise<void> {
  const { resolve, promise } = createPromise<void>()
  window.Whisper.events.once('storageService:syncComplete', () => resolve())
  return promise
}

// Cache for memoize that ignores empty values
class CustomCache extends Map {
  set(key: string, value: string) {
    if (!value) {
      return
    }
    return super.set(key, value)
  }
}

const customMemoize = (fn: (...args: string[]) => string | null) => {
  const memoized = memoize(fn)
  memoized.cache = new CustomCache()
  return memoized
}

export default class SignalAPI implements PlatformAPI {
  // for mappers
  getConversation = getConversation

  currentUserUUID: string

  private async _getUserByConversationId(conversationId: string) {
    const data = await getConversation(conversationId)
    const user: User & { uuid: string } = {
      id: data.id,
      phoneNumber: data.e164,
      fullName: [data.profileName, data.profileFamilyName].filter(Boolean).join(' '),
      isSelf: data.uuid === this.currentUserUUID,
      imgURL: data.profileAvatar?.path
        ? url.pathToFileURL(path.join(this.accountInfo.dataDirPath, 'attachments.noindex', data.profileAvatar.path)).href
        : undefined,
      uuid: data.uuid,
    }
    return user
  }

  /**
   * sourceId can be e164, groupId or groupV2Id. conversationId is the DB row
   * id. Because some events (e.g. 'typing' event) return sourceId, we need to
   * convert it to conversationId to work with other parts of the system.
   */
  getConversationIdBySourceId = memoize((sourceId: string) => {
    const conversation = window.ConversationController.get(sourceId)
    return conversation?.id
  })

  getProfileName = (threadID: string) => this._getProfileName(threadID) || 'someone'

  _getProfileName = customMemoize((threadID: string) => {
    const person = window.ConversationController.get(threadID)
    if (!person) {
      return null
    }

    return person.getProfileName() || person.getTitle()
  })

  getUserByConversationId = memoize(this._getUserByConversationId)

  accountInfo: AccountInfo

  private connStateChangeCallback: OnConnStateChangeCallback

  private onEvent: OnServerEventCallback

  private pendingMessageQueue: ServerEvent[] = []

  // If a thread has no message/activity yet, Signal doesn't fully initialize it.
  private readonly threadsWithoutTitle: Set<string> = new Set()

  private readonly threadsWithTitle: Set<string> = new Set()

  private groupSyncPromise: Promise<void>

  private initialEvents = createPromise<void>()

  private handleServerEvent = async (msg: any) => {
    const event = await mapServerEvent(msg, this)
    if (!event) return
    this.checkThreadTitle(event)
    if (event.type === ServerEventType.USER_ACTIVITY) {
      // For user activity event (e.g. typing), use `await` to emit title
      // update before the typing event.
      await this.handleThreadWithoutTitle(event.threadID)
    } else if ('objectIDs' in event) {
      // No need to `await` for other event (e.g. message).
      this.handleThreadWithoutTitle(event.objectIDs?.threadID)
    }
    this.onEvent([event])
  }

  init = async (session: SerializedSession, accountInfo: AccountInfo) => {
    global.TEXTS_DATA_DIR = accountInfo.dataDirPath
    this.accountInfo = accountInfo
    const { resolve: resolveGroupSync, promise: groupSyncPromise } = createPromise<void>()
    this.groupSyncPromise = groupSyncPromise;

    (window.emitter as EventEmitter)
      .on('error', ev => {
        if (DEBUG) console.log('signal-client error', ev.error)
      })
      .on('unauthorized', () => {
        this.connStateChangeCallback({ status: ConnectionStatus.UNAUTHORIZED })
      })
      .on('texts-event', msg => {
        if (msg.type === 'groupSync') {
          return resolveGroupSync()
        }
        // texts.log('texts-event', msg)
        if (this.onEvent) {
          this.handleServerEvent(msg)
        } else {
          this.pendingMessageQueue.push(msg)
        }
      })
      // .on('receipt', ev => {
      //   const pushMessage = ev.proto
      //   const timestamp = pushMessage.timestamp.toNumber()
      //   if (DEBUG) {
      //     console.log(
      //       'delivery receipt from',
      //       pushMessage.source + '.' + pushMessage.sourceDevice,
      //       timestamp,
      //     )
      //   }
      // })
      // .on('read', ev => {
      //   const read_at = ev.timestamp
      //   const { sender, timestamp } = ev.read
      //   if (DEBUG) console.log('read receipt', sender, timestamp)
    // })
    const { default: main } = await import('./preload-signal')
    await main()
    const { startApp } = require('./ts/background')
    await startApp()
    await new Promise<void>(resolve => {
      window.emitter.once('storage_ready', () => {
        resolve()
      })
    })
    if (session) {
      await window.waitForEmptyEventQueue()
      await this.updateSelfID()
    }
  }

  private updateSelfID = async () => {
    this.currentUserUUID = await window.textsecure.storage.user.getUuid().value
  }

  private loginEventCallback: (message: { type: string, data?: string }) => void

  login = async (creds: any): Promise<LoginResult> => {
    if (!creds) {
      window.Whisper.events.on('registration_done', async () => {
        const ourNumber = window.textsecure.storage.user.getNumber()
        await this.updateSelfID()
        this.loginEventCallback({ type: 'link-success', data: ourNumber })
      })
      await window.getAccountManager().registerSecondDevice(
        (url: string) => this.loginEventCallback({ type: 'link-url', data: url }),
        async () => {
          this.loginEventCallback({ type: 'link-authenticating' })
          return DEVICE_NAME
        },
      )
      return
    }
    await this.groupSyncPromise
    return { type: 'success' }
  }

  onLoginEvent = (onEvent: (data: any) => void) => {
    this.loginEventCallback = onEvent
  }

  logout = async () => {
    try {
      await window.Signal.Data.removeAll()
      await window.Signal.Data.close()
      await window.Signal.Data.removeDB()
      await window.Signal.Data.removeOtherData()
    } finally {
      await fs.rm(this.accountInfo.dataDirPath, { recursive: true })
    }
  }

  dispose() {}

  onConnectionStateChange = (onEvent: OnConnStateChangeCallback) => {
    this.connStateChangeCallback = onEvent
  }

  getCurrentUser = async (): Promise<CurrentUser> => {
    const user = await this.getUserByConversationId(this.currentUserUUID).catch(() => undefined)
    if (!user) return { id: this.currentUserUUID, displayText: '' }
    return {
      ...user,
      displayText: user.phoneNumber,
    }
  }

  private checkThreadTitle = async (event: ServerEvent) => {
    if (event.type === ServerEventType.STATE_SYNC
      && event.mutationType === 'upsert'
      && ['thread', 'message'].includes(event.objectName)
      && !this.threadsWithTitle.has(event.objectIDs.threadID)
    ) {
      this.threadsWithoutTitle.add(event.objectIDs.threadID)
    } else if (event.type === ServerEventType.USER_ACTIVITY && !this.threadsWithTitle.has(event.threadID)) {
      // It's possible this is the first time we see this threadID.
      this.threadsWithoutTitle.add(event.threadID)
    }
  }

  private handleThreadWithoutTitle = async (threadID: string) => {
    if (!threadID || this.threadsWithTitle.has(threadID)) {
      return
    }

    await getProfile(threadID)
    const conv = await getConversation(threadID)
    const thread = await mapThread(conv, this)
    if (!thread.title) {
      this.threadsWithoutTitle.add(threadID)
      return
    }

    const events: StateSyncEvent[] = [
      {
        type: ServerEventType.STATE_SYNC,
        objectIDs: {},
        mutationType: 'upsert',
        objectName: 'thread',
        entries: [thread],
      }, {
        // Update participants of this thread.
        type: ServerEventType.STATE_SYNC,
        mutationType: 'upsert',
        objectName: 'participant',
        objectIDs: {
          threadID,
        },
        entries: thread.participants.items.filter(item => !item.isSelf),
      },
    ]
    if (conv.type === 'private') {
      events.push({
        // Update participants for threads containing this contact.
        type: ServerEventType.STATE_SYNC,
        mutationType: 'update',
        objectName: 'participant',
        objectIDs: {},
        entries: [
          await this.getUserByConversationId(conv.id),
        ],
      })
    }
    this.onEvent(events)
    this.threadsWithTitle.add(threadID)
    this.threadsWithoutTitle.delete(threadID)
  }

  subscribeToEvents = (onEvent: OnServerEventCallback) => {
    this.onEvent = onEvent
    Promise.all(this.pendingMessageQueue.map(msg => mapServerEvent(msg, this)))
      .then(serverEvents => {
        const events: ServerEvent[] = []
        const threadIDs: Set<string> = new Set()
        serverEvents.forEach(event => {
          if (!event) return
          events.push(event)
          this.checkThreadTitle(event)
          if (!('objectIDs' in event)) return
          const threadID = event.objectIDs?.threadID
          if (threadID) {
            threadIDs.add(threadID)
          }
        })
        threadIDs.forEach(threadID => this.handleThreadWithoutTitle(threadID))
        onEvent(events)
      }).then(() => this.initialEvents.resolve())
    this.pendingMessageQueue = []
  }

  serializeSession = () => ({})

  getUser = async ({ phoneNumber }: { phoneNumber: PhoneNumber }) => {
    if (!phoneNumber) return
    const conv = await window.ConversationController.getOrCreateAndWait(phoneNumber, 'private')
    if (!conv) return
    if (conv.isSMSOnly()) return
    return {
      id: conv.id,
      fullName: [conv.attributes.profileName, conv.attributes.profileFamilyName].filter(Boolean).join(' '),
      phoneNumber,
    }
  }

  searchUsers = async (_typed: string): Promise<User[]> => {
    const typed = _typed.toLowerCase()
    const contacts = await window.sql.getAllConversations()
    const matched = contacts.filter(c => c.type === 'private').map(({ uuid, e164, profileName = '', profileFamilyName = '' }) => {
      if (!uuid) return
      const fullName = [profileName, profileFamilyName].filter(Boolean).join(' ').trim()
      if (
        e164?.includes(typed)
        || fullName?.toLowerCase().includes(typed)
      ) {
        return {
          id: e164,
          phoneNumber: e164,
          fullName,
        }
      }
    }).filter(Boolean).slice(0, 10)
    return matched
  }

  createThread = async (userIDs: string[], title?: string) => {
    const isGroup = !!title // Title is required for groups but not for contacts.
    const { createGroupV2 } = require('../Signal-Desktop/ts/groups')
    const conv = await (isGroup
      ? createGroupV2({ name: title, conversationIds: userIDs, avatar: null, expireTimer: 0 })
      : window.ConversationController.getOrCreateAndWait(userIDs[0], 'private'))
    const convData = await getConversation(conv.id) // get full conversation data with member list and title/name
    const thread = await mapThread(convData, this)
    if (!thread) return
    return thread
  }

  updateThread = async (threadID: string, updates: Partial<Thread>) => {
    const conversation = await window.ConversationController.get(threadID)
    if ('title' in updates) {
      await conversation.updateGroupAttributesV2({ title: updates.title })
    }
    if (typeof updates.messageExpirySeconds !== 'undefined') {
      await conversation.updateExpirationTimer(updates.messageExpirySeconds)
    }
    if ('mutedUntil' in updates) {
      const mutedUntil = updates.mutedUntil === 'forever' ? Number.MAX_SAFE_INTEGER : 0
      await conversation.setMuteExpiration(mutedUntil)
    }
  }

  getThreads = async (): Promise<Paginated<Thread>> => {
    await this.initialEvents.promise
    const convs = await window.sql.getAllConversations()
    const items: Thread[] = await Promise.all(convs.map(async conv => {
      const thread = await mapThread(conv, this)
      if (!thread) return
      if (thread.title) {
        this.threadsWithTitle.add(thread.id)
      } else {
        this.threadsWithoutTitle.add(thread.id)
      }
      return thread
    }))
    return {
      items: items.filter(Boolean),
      hasMore: false,
      oldestCursor: null,
    }
  }

  // used for quickly fetching all conversations without having to fetch participants and messages
  getThreadsMetaOnly = async (): Promise<Thread[]> => {
    await Promise.all([this.initialEvents.promise, waitForNextSyncComplete()])
    const convs = await window.sql.getAllConversations()
    return convs.map(conv => mapThreadMetaOnly(conv)).filter(Boolean)
  }

  deleteThread = async (threadID: string) => {
    const conv = window.ConversationController.get(threadID)
    await conv.destroyMessages({})
  }

  getMessages = async (threadID: string, pagination: PaginationArg): Promise<Paginated<Message>> => {
    await this.initialEvents.promise
    let receivedAt: number
    if (pagination?.cursor) {
      const message = await window.sql.getMessageById(pagination.cursor)
      if (!message) {
        return {
          items: [],
          hasMore: false,
        }
      }
      receivedAt = message.received_at
    }
    const fn = pagination?.direction === 'after' ? window.sql.getNewerMessagesByConversation : window.sql.getOlderMessagesByConversation
    const rows = await fn(threadID, {
      receivedAt: receivedAt || undefined,
      limit: 20,
    })
    const rawMessages: any[] = await rows.map(x => JSON.parse(x.json))
    const items = await Promise.all(rawMessages.map(msg => mapMessage(msg, this)))
    for (const senderID of new Set(items.map(x => x.senderID))) {
      if (!this.threadsWithTitle.has(senderID)) {
        this.handleThreadWithoutTitle(senderID)
      }
    }
    return {
      items,
      hasMore: items.length >= 20,
    }
  }

  sendMessage = async (threadID: string, content: MessageContent, options: MessageSendOptions) => {
    const conversation = window.ConversationController.get(threadID)
    const { text, mentions } = await convertUserMentions(this, content)
    const attachments = []
    let attachmentBuffer: Buffer
    if (content.filePath) {
      attachmentBuffer = await fs.readFile(content.filePath)
    } else if (content.fileBuffer) {
      attachmentBuffer = content.fileBuffer
    }
    if (attachmentBuffer) {
      attachments.push({
        data: attachmentBuffer,
        size: attachmentBuffer.byteLength,
        contentType: content.mimeType || '',
        fileName: content.fileName,
      })
    }
    let quote = null
    if (options.quotedMessageID) {
      const { getMessageById } = require('Signal-Desktop/ts/messages/getMessageById')
      const quotingMessage = await getMessageById(options.quotedMessageID)
      quote = await conversation.makeQuote(quotingMessage)
    }
    const links = urlRegex().exec(text)
    const preview: LinkPreviewType[] = []
    if (links) {
      try {
        const previews = await Promise.all(links.map(href => fetchLinkPreview(href)))
        preview.push(...previews.filter(Boolean))
      } catch (err) { texts.error(err) }
    }
    const sentAt = Date.now()
    const msg = await conversation.enqueueMessageForSend({
      body: text,
      attachments,
      quote,
      preview,
      mentions,
    }, {
      timestamp: sentAt,
    })
    let unmapped = msg
    let attemptsRemaining = 299 // total 59 seconds
    while (--attemptsRemaining && !unmapped.synced) {
      unmapped = await window.sql.getMessageById(msg.id)
      await setTimeoutAsync(200)
    }
    if (!unmapped.synced) return false
    return [await mapMessage(unmapped, this)]
  }

  addReaction = async (
    threadID: string,
    messageID: string,
    reactionKey: string,
  ) => enqueueReactionForSend(messageID, reactionKey, false)

  removeReaction = async (
    threadID: string,
    messageID: string,
    reactionKey: string,
  ) => enqueueReactionForSend(messageID, reactionKey, true)

  deleteMessage = async (threadID: string, messageID: string, forEveryone: boolean) => {
    if (forEveryone) {
      const conversation = window.ConversationController.get(threadID)
      const message = await window.sql.getMessageById(messageID)
      await conversation.sendDeleteForEveryoneMessage({
        id: messageID,
        timestamp: message.sent_at,
      })
    }
    await window.sql.removeMessage(messageID)
  }

  sendReadReceipt = async (threadID: string, messageID: string) => {
    const msg = await window.sql.getMessageById(messageID)
    const conv = window.ConversationController.get(threadID)
    await conv.markRead(msg.received_at)
  }

  sendActivityIndicator = async (type: ActivityType, threadID: string) => {
    const conv = window.ConversationController.get(threadID)
    await conv.sendTypingMessage(type === ActivityType.TYPING)
  }

  addParticipant = async (threadID: string, participantID: string) => {
    const conv = window.ConversationController.get(threadID)

    const [alreadyIn, alreadyInvited] = await Promise.all([
      conv.isMember(participantID),
      conv.isMemberPending(participantID),
    ])

    if (alreadyIn || alreadyInvited) {
      let text: string
      if (alreadyIn) {
        text = 'This user is already in the group.'
      } else if (alreadyInvited) {
        text = 'This user already has an invite and they need to accept before they can join.'
      }

      this.onEvent([
        {
          type: ServerEventType.TOAST,
          toast: {
            text,
          },
        },
      ])

      await this._refreshThread(threadID)

      return
    }

    try {
      await conv.addMembersV2([participantID])

      const isPending = await conv.isMemberPending(participantID) // The user would return as pending if they were invited instead of added

      if (isPending) {
        this.onEvent([
          {
            type: ServerEventType.TOAST,
            toast: {
              text: 'This user can’t be automatically added to this group by you. They’ve been invited to join, and won’t see any group messages until they accept.', // copy yanked from Signal's desktop app
            },
          },
        ])
      }
    } catch (error) {
      this.onEvent([
        {
          type: ServerEventType.TOAST,
          toast: {
            text: 'Something went wrong when adding the participant. Make sure you have the appropriate permissions and try again.',
          },
        },
      ])

      texts.error('[signal:addParticipant]', error)
      texts.Sentry.captureException(error)
    }

    await this._refreshThread(threadID)
  }

  removeParticipant = async (threadID: string, participantID: string) => {
    const isSelf = (participantID === this.currentUserUUID)

    const [conv, personToRemove] = await Promise.all([
      window.ConversationController.getOrCreateAndWait(threadID, 'group'),
      !isSelf && getConversation(participantID),
    ])

    try {
      await conv.removeFromGroupV2(isSelf ? this.currentUserUUID : personToRemove.uuid)
    } catch (error) {
      this.onEvent([
        {
          type: ServerEventType.TOAST,
          toast: {
            text: 'Something went wrong when removing the participant. Make sure you have the appropriate permissions and try again.',
          },
        },
      ])

      texts.error('[signal:removeParticipant]', error)
      texts.Sentry.captureException(error)
    }

    await this._refreshThread(threadID)
  }

  _refreshThread = async (threadID: string) => { // used to refresh the thread to update the optimistic state (when add/remove participant fails for example)
    const convData = await getConversation(threadID)
    const thread = await mapThread(convData, this)

    this.onEvent([
      {
        type: ServerEventType.THREAD_MESSAGES_REFRESH,
        threadID,
      },
      {
        type: ServerEventType.STATE_SYNC,
        mutationType: 'update',
        objectName: 'thread',
        objectIDs: {},
        entries: [
          pick(thread, [
            'id',
            'participants',
            'title',
            'isUnread',
            'isReadOnly',
          ]),
        ],
      }])
  }

  archiveThread = async (threadID: string, archived: boolean) => {
    const conv = window.ConversationController.get(threadID)
    await conv.setArchived(archived)
  }

  private _getAsset = async (type: AttachmentType, messageID: string, index = 0, count = 0) => {
    if (count > 7) return
    const msg = await window.sql.getMessageById(messageID)
    let attachment
    if (type === 'sticker') {
      attachment = msg.sticker.data
    } else if (type === 'preview') {
      attachment = msg.preview[index].image
    } else {
      attachment = msg[type][index]
    }
    if (attachment.pending || !attachment.path) {
      // FIXME: emit event
      await setTimeoutAsync(1000)
      return this._getAsset(type, messageID, index, count + 1)
    }
    return getFilePath(this.accountInfo, attachment.path)
  }

  getAsset = async (_, type: AttachmentType, filePath: string) => {
    texts.log('get asset', type, filePath)
    const [messageID, index] = filePath.split('_')
    return this._getAsset(type, messageID, +index)
  }

  registerForPushNotifications = async (type: 'android', token: string) => {
    await window.textsecure.messaging.server.gcm(token, true)
  }

  unregisterForPushNotifications = async (type: 'android', token: string) => {
    await window.textsecure.messaging.server.gcm(token, false)
  }
}
