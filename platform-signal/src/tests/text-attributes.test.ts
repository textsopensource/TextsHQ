import { mapTextAttributes } from '../text-attributes'

const scbMock: any = {
  getProfileName() {
    return 'User'
  },
}

const userID = '59774d84-09ac-4aec-a940-ff8790bdf4f5'

const cases = [
  {
    body: '✌️￼ hi👌 test￼ cc',
    bodyRanges: [
      {
        start: 2,
        length: 1,
        mentionUuid: userID,
      },
      {
        start: 13,
        length: 1,
        mentionUuid: userID,
      },
    ],
    result: {
      text: '✌️@User hi👌 test@User cc',
      textAttributes: {
        entities: [
          {
            from: 2,
            to: 7,
            mentionedUser: {
              username: 'User',
              id: userID,
            },
          },
          {
            from: 16,
            to: 21,
            mentionedUser: {
              username: 'User',
              id: userID,
            },
          },
        ],
      },
    },
  },
]

test('text attributes', async () => {
  for (const c of cases) {
    const result = await mapTextAttributes(scbMock, c.body, c.bodyRanges)
    expect(result).toEqual(c.result)
  }
})
