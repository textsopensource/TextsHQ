import { convertUserMentions } from '../mappers'

const userID = '59774d84-09ac-4aec-a940-ff8790bdf4f5'
const uuid = '434eaf3a-f683-4df5-8b97-0df826c9c10b'
const phoneNumber = '11234567890'

const scbMock: any = {
  getUserByConversationId() {
    return {
      id: userID,
      phoneNumber,
      fullName: 'John Doe',
      uuid,
    }
  },
}

const cases = [
  {
    content: {
      text: `✌️@${phoneNumber} hi`,
      mentionedUserIDs: [userID],
    },
    result: {
      text: '✌️\ufffc hi',
      mentions: [
        {
          start: 2,
          length: 1,
          mentionUuid: uuid,
        },
      ],
    },
  },
]

test('convertUserMentions', async () => {
  for (const c of cases) {
    const result = await convertUserMentions(scbMock, c.content)
    expect(result).toEqual(c.result)
  }
})
