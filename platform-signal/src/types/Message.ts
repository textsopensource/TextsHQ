/**
 * Adapted from Signal-Desktop.
 */

// import { Attachment } from './Attachment'
// import { ContactType } from './Contact'

export type WhatIsThis = any
export type TBD = any

export enum ReactionSource {
  FromSomeoneElse,
  FromSync,
  FromThisDevice,
}

export enum ReadStatus {
  Unread = 1,
  Read = 0,
  Viewed = 2,
}

export enum SendStatus {
  Failed = 'Failed',
  Pending = 'Pending',
  Sent = 'Sent',
  Delivered = 'Delivered',
  Read = 'Read',
  Viewed = 'Viewed',
}

export type ProfileNameChangeType = {
  type: 'name'
  oldName: string
  newName: string
}

export type SendState = Readonly<{
  status:
  | SendStatus.Pending
  | SendStatus.Failed
  | SendStatus.Sent
  | SendStatus.Delivered
  | SendStatus.Read
  | SendStatus.Viewed
  updatedAt?: number
}>

export type SendStateByConversationId = Record<string, SendState>
// Signal-Desktop/ts/groups.ts

export type UUIDStringType =
  `${string}-${string}-${string}-${string}-${string}`

type GroupV2AccessCreateChangeType = {
  type: 'create'
}
type GroupV2AccessAttributesChangeType = {
  type: 'access-attributes'
  newPrivilege: number
}
type GroupV2AccessMembersChangeType = {
  type: 'access-members'
  newPrivilege: number
}
type GroupV2AccessInviteLinkChangeType = {
  type: 'access-invite-link'
  newPrivilege: number
}
type GroupV2AnnouncementsOnlyChangeType = {
  type: 'announcements-only'
  announcementsOnly: boolean
}
type GroupV2AvatarChangeType = {
  type: 'avatar'
  removed: boolean
}
type GroupV2TitleChangeType = {
  type: 'title'
  // Allow for null, because the title could be removed entirely
  newTitle?: string
}
type GroupV2GroupLinkAddChangeType = {
  type: 'group-link-add'
  privilege: number
}
type GroupV2GroupLinkResetChangeType = {
  type: 'group-link-reset'
}
type GroupV2GroupLinkRemoveChangeType = {
  type: 'group-link-remove'
}

// No disappearing messages timer change type - message.expirationTimerUpdate used instead

type GroupV2MemberAddChangeType = {
  type: 'member-add'
  uuid: UUIDStringType
}
type GroupV2MemberAddFromInviteChangeType = {
  type: 'member-add-from-invite'
  uuid: UUIDStringType
  inviter?: UUIDStringType
}
type GroupV2MemberAddFromLinkChangeType = {
  type: 'member-add-from-link'
  uuid: UUIDStringType
}
type GroupV2MemberAddFromAdminApprovalChangeType = {
  type: 'member-add-from-admin-approval'
  uuid: UUIDStringType
}
type GroupV2MemberPrivilegeChangeType = {
  type: 'member-privilege'
  uuid: UUIDStringType
  newPrivilege: number
}
type GroupV2MemberRemoveChangeType = {
  type: 'member-remove'
  uuid: UUIDStringType
}

type GroupV2PendingAddOneChangeType = {
  type: 'pending-add-one'
  uuid: UUIDStringType
}
type GroupV2PendingAddManyChangeType = {
  type: 'pending-add-many'
  count: number
}
// Note: pending-remove is only used if user didn't also join the group at the same time
type GroupV2PendingRemoveOneChangeType = {
  type: 'pending-remove-one'
  uuid: UUIDStringType
  inviter?: UUIDStringType
}
// Note: pending-remove is only used if user didn't also join the group at the same time
type GroupV2PendingRemoveManyChangeType = {
  type: 'pending-remove-many'
  count: number
  inviter?: UUIDStringType
}

type GroupV2AdminApprovalAddOneChangeType = {
  type: 'admin-approval-add-one'
  uuid: UUIDStringType
}
// Note: admin-approval-remove-one is only used if user didn't also join the group at
//   the same time
type GroupV2AdminApprovalRemoveOneChangeType = {
  type: 'admin-approval-remove-one'
  uuid: UUIDStringType
  inviter?: UUIDStringType
}
export type GroupV2DescriptionChangeType = {
  type: 'description'
  removed?: boolean
  // Adding this field cannot remove previous field for backwards compatibility
  description?: string
}

export type GroupV2ChangeDetailType =
  | GroupV2AccessAttributesChangeType
  | GroupV2AccessCreateChangeType
  | GroupV2AccessInviteLinkChangeType
  | GroupV2AccessMembersChangeType
  | GroupV2AdminApprovalAddOneChangeType
  | GroupV2AdminApprovalRemoveOneChangeType
  | GroupV2AnnouncementsOnlyChangeType
  | GroupV2AvatarChangeType
  | GroupV2DescriptionChangeType
  | GroupV2GroupLinkAddChangeType
  | GroupV2GroupLinkRemoveChangeType
  | GroupV2GroupLinkResetChangeType
  | GroupV2MemberAddChangeType
  | GroupV2MemberAddFromAdminApprovalChangeType
  | GroupV2MemberAddFromInviteChangeType
  | GroupV2MemberAddFromLinkChangeType
  | GroupV2MemberPrivilegeChangeType
  | GroupV2MemberRemoveChangeType
  | GroupV2PendingAddManyChangeType
  | GroupV2PendingAddOneChangeType
  | GroupV2PendingRemoveManyChangeType
  | GroupV2PendingRemoveOneChangeType
  | GroupV2TitleChangeType

export type GroupV2ChangeType = {
  from?: UUIDStringType
  details: Array<GroupV2ChangeDetailType>
}

// Signal-Desktop/ts/types/Util.ts

export type BodyRangeType = {
  start: number
  length: number
  mentionUuid?: string
  replacementText?: string
  conversationID?: string
}

export type BodyRangesType = Array<BodyRangeType>

// Signal-Desktop/ts/types/Attachment.ts

export type ThumbnailType = {
  height: number
  width: number
  url?: string
  contentType: string
  path: string
  // Only used when quote needed to make an in-memory thumbnail
  objectUrl?: string
}

export type AttachmentType = {
  error?: boolean
  blurHash?: string
  caption?: string
  contentType: string
  fileName?: string
  /** Not included in protobuf, needs to be pulled from flags */
  isVoiceMessage?: boolean
  /** For messages not already on disk, this will be a data url */
  url?: string
  size: number
  fileSize?: string
  pending?: boolean
  width?: number
  height?: number
  path?: string
  screenshot?: {
    height: number
    width: number
    url?: string
    contentType: string
    path: string
  }
  screenshotData?: Uint8Array
  screenshotPath?: string
  flags?: number
  thumbnail?: ThumbnailType
  isCorrupted?: boolean
  downloadJobId?: string
  cdnNumber?: number
  cdnId?: string
  cdnKey?: string
  data?: Uint8Array

  /** Legacy field. Used only for downloading old attachments */
  id?: number
}

// Signal-Desktop/ts/model-types.d.ts

export type LastMessageStatus =
  | 'paused'
  | 'error'
  | 'partial-sent'
  | 'sending'
  | 'sent'
  | 'delivered'
  | 'read'
  | 'viewed'

type TaskResultType = any

export type CustomError = Error & {
  identifier?: string
  number?: string
  data?: object
  retryAfter?: number
}

export type GroupMigrationType = {
  areWeInvited: boolean
  droppedMemberIds: Array<string>
  invitedMembers: Array<GroupV2PendingMemberType>
}

export type QuotedMessageType = {
  attachments: Array<typeof window.WhatIsThis>
  // `author` is an old attribute that holds the author's E164. We shouldn't use it for
  //   new messages, but old messages might have this attribute.
  author?: string
  authorUuid?: string
  bodyRanges?: BodyRangesType
  id: number
  referencedMessageNotFound: boolean
  isViewOnce: boolean
  text?: string
  messageId: string
}

export type RetryOptions = Readonly<{
  type: 'session-reset'
  uuid: string
  e164: string
  now: number
}>

export type GroupV1Update = {
  avatarUpdated?: boolean
  joined?: Array<string>
  left?: string | 'You'
  name?: string
}

export type MessageReactionType = {
  emoji: undefined | string
  fromId: string
  targetAuthorUuid: string
  targetTimestamp: number
  timestamp: number
  isSentByConversationId?: Record<string, boolean>
}

export type LinkPreviewType = {
  title: string
  description?: string
  domain: string
  url: string
  isStickerPack: boolean
  image?: Readonly<AttachmentType>
  date?: number
}

export type MessageAttributesType = {
  bodyPending?: boolean
  bodyRanges?: BodyRangesType
  callHistoryDetails?: TBD
  changedId?: string
  dataMessage?: Uint8Array | null
  decrypted_at?: number
  deletedForEveryone?: boolean
  deletedForEveryoneTimestamp?: number
  errors?: Array<CustomError>
  expirationStartTimestamp?: number | null
  expireTimer?: number
  groupMigration?: GroupMigrationType
  group_update?: GroupV1Update
  hasAttachments?: boolean
  hasFileAttachments?: boolean
  hasVisualMediaAttachments?: boolean
  isErased?: boolean
  isTapToViewInvalid?: boolean
  isViewOnce?: boolean
  key_changed?: string
  local?: boolean
  logger?: unknown
  message?: unknown
  messageTimer?: unknown
  profileChange?: ProfileNameChangeType
  quote?: QuotedMessageType
  reactions?: Array<MessageReactionType>
  requiredProtocolVersion?: number
  retryOptions?: RetryOptions
  sourceDevice?: number
  supportedVersionAtReceive?: unknown
  synced?: boolean
  unidentifiedDeliveryReceived?: boolean
  verified?: boolean
  verifiedChanged?: string

  id: string
  type:
  | 'call-history'
  | 'chat-session-refreshed'
  | 'delivery-issue'
  | 'group'
  | 'group-v1-migration'
  | 'group-v2-change'
  | 'incoming'
  | 'keychange'
  | 'message-history-unsynced'
  | 'outgoing'
  | 'profile-change'
  | 'timer-notification'
  | 'universal-timer-notification'
  | 'change-number-notification'
  | 'verified-change'
  body?: string
  attachments?: Array<AttachmentType>
  preview?: Array<WhatIsThis>
  sticker?: {
    packId: string
    stickerId: number
    packKey: string
    data?: AttachmentType
  }
  sent_at: number
  unidentifiedDeliveries?: Array<string>
  contact?: Array<TBD>
  conversationId: string
  reaction?: WhatIsThis

  expirationTimerUpdate?: {
    expireTimer: number
    fromSync?: unknown
    source?: string
    sourceUuid?: string
  }
  // Legacy fields for timer update notification only
  flags?: number
  groupV2Change?: GroupV2ChangeType
  // Required. Used to sort messages in the database for the conversation timeline.
  received_at: number
  received_at_ms?: number
  // More of a legacy feature, needed as we were updating the schema of messages in the
  //   background, when we were still in IndexedDB, before attachments had gone to disk
  // We set this so that the idle message upgrade process doesn't pick this message up
  schemaVersion?: number
  // This should always be set for new messages, but older messages may not have them. We
  //   may not have these for outbound messages, either, as we have not needed them.
  serverGuid?: string
  serverTimestamp?: number
  source?: string
  sourceUuid?: UUIDStringType

  timestamp: number

  // Backwards-compatibility with prerelease data schema
  invitedGV2Members?: Array<GroupV2PendingMemberType>
  droppedGV2MemberIds?: Array<string>

  sendHQImages?: boolean

  // Should only be present for incoming messages
  readStatus?: ReadStatus

  // Should only be present for outgoing messages
  sendStateByConversationId?: SendStateByConversationId
}

export type ConversationAttributesTypeType = 'private' | 'group'

export type ConversationAttributesType = {
  accessKey?: string | null
  addedBy?: string
  badges?: Array<
  | { id: string }
  | {
    id: string
    expiresAt: number
    isVisible: boolean
  }
  >
  capabilities?: TBD
  color?: string
  conversationColor?: TBD
  customColor?: TBD
  customColorId?: string
  discoveredUnregisteredAt?: number
  draftChanged?: boolean
  draftAttachments?: Array<TBD>
  draftBodyRanges?: Array<BodyRangeType>
  draftTimestamp?: number | null
  inbox_position: number
  isPinned: boolean
  lastMessageDeletedForEveryone: boolean
  lastMessageStatus?: LastMessageStatus | null
  markedUnread: boolean
  messageCount: number
  messageCountBeforeMessageRequests?: number | null
  messageRequestResponseType?: number
  muteExpiresAt?: number
  dontNotifyForMentionsIfMuted?: boolean
  profileAvatar?: null | {
    hash: string
    path: string
  }
  profileKeyCredential?: string | null
  profileKeyVersion?: string | null
  quotedMessageId?: string | null
  sealedSender?: unknown
  sentMessageCount: number
  sharedGroupNames?: Array<string>

  id: string
  type: ConversationAttributesTypeType
  timestamp?: number | null

  // Shared fields
  active_at?: number | null
  draft?: string | null
  isArchived?: boolean
  lastMessage?: string | null
  name?: string
  needsStorageServiceSync?: boolean
  needsVerification?: boolean
  profileSharing: boolean
  storageID?: string
  storageUnknownFields?: string
  unreadCount?: number
  version: number

  // Private core info
  uuid?: UUIDStringType
  e164?: string

  // Private other fields
  about?: string
  aboutEmoji?: string
  profileFamilyName?: string
  profileKey?: string
  profileName?: string
  verified?: number
  profileLastFetchedAt?: number
  pendingUniversalTimer?: string
  username?: string

  // Group-only
  groupId?: string
  // A shorthand, representing whether the user is part of the group. Not strictly for
  //   when the user manually left the group. But historically, that was the only way
  //   to leave a group.
  left?: boolean
  groupVersion?: number

  // GroupV1 only
  members?: Array<string>
  derivedGroupV2Id?: string

  // GroupV2 core info
  masterKey?: string
  secretParams?: string
  publicParams?: string
  revision?: number
  senderKeyInfo?: {
    createdAtDate: number
    distributionId: string
    memberDevices: Array<TBD>
  }

  // GroupV2 other fields
  accessControl?: {
    attributes: TBD
    members: TBD
    addFromInviteLink: TBD
  }
  announcementsOnly?: boolean
  avatar?: {
    url: string
    path: string
    hash?: string
  } | null
  avatars?: Array<TBD>
  description?: string
  expireTimer?: number
  membersV2?: Array<GroupV2MemberType>
  pendingMembersV2?: Array<GroupV2PendingMemberType>
  pendingAdminApprovalV2?: Array<GroupV2PendingAdminApprovalType>
  groupInviteLinkPassword?: string
  previousGroupV1Id?: string
  previousGroupV1Members?: Array<string>
  acknowledgedGroupNameCollisions?: TBD

  // Used only when user is waiting for approval to join via link
  isTemporary?: boolean
  temporaryMemberCount?: number

  // Avatars are blurred for some unapproved conversations, but users can manually unblur
  //   them. If the avatar was unblurred and then changed, we don't update this value so
  //   the new avatar gets blurred.
  //
  // This value is useless once the message request has been approved. We don't clean it
  //   up but could. We don't persist it but could (though we'd probably want to clean it
  //   up in that case).
  unblurredAvatarPath?: string
}

export type GroupV2MemberType = {
  uuid: UUIDStringType
  role: TBD
  joinedAtVersion: number

  // Note that these are temporary flags, generated by applyGroupChange, but eliminated
  //   by applyGroupState. They are used to make our diff-generation more intelligent but
  //   not after that.
  joinedFromLink?: boolean
  approvedByAdmin?: boolean
}

export type GroupV2PendingMemberType = {
  addedByUserId?: UUIDStringType
  uuid: UUIDStringType
  timestamp: number
  role: TBD
}

export type GroupV2PendingAdminApprovalType = {
  uuid: UUIDStringType
  timestamp: number
}

export type VerificationOptions = {
  key?: null | Uint8Array
  viaContactSync?: boolean
  viaStorageServiceSync?: boolean
  viaSyncMessage?: boolean
}

export type ShallowChallengeError = CustomError & {
  readonly retryAfter: number
  readonly data: TBD
}

export type ReactionAttributesType = {
  emoji: string
  remove?: boolean
  targetAuthorUuid: string
  targetTimestamp: number
  fromId: string
  timestamp: number
  source: ReactionSource
}
