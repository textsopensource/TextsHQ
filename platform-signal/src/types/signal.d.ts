/**
 * Custom Signal types.
 */

import { EnvelopeClass, DataMessageClass } from './textsecure'

export type SignalMessage = Omit<EnvelopeClass, 'id'> & {
  message: Omit<DataMessageClass, 'toArrayBuffer'>
}
