#!/usr/bin/env sh

cd src/ts
cp ../../Signal-Desktop/ts/background.ts .
cp ../../Signal-Desktop/ts/window.d.ts .
cp ../../Signal-Desktop/ts/textsecure.d.ts .

patch background.ts background.diff
cd ../..
