import url from 'url'
import Path from 'path'
import {
  Message,
  Thread,
  ServerEvent,
  MessageActionType,
  MessageAttachmentType,
  ServerEventType,
  MessageAttachment,
  UNKNOWN_DATE,
  AccountInfo,
  Participant,
  MessageReaction,
  MessageSeen,
  texts,
  ActivityType,
  MessageContent,
  MessageLink,
} from '@textshq/platform-sdk'
// import type { AttachmentType as DownloadAttachmentType } from 'Signal-Desktop/ts/types/Attachment'

import { mapTextAttributes } from './text-attributes'
import { BodyRangeType, GroupV2ChangeDetailType, MessageAttributesType, SendStateByConversationId, SendStatus } from './types/Message'
import { formatSeconds, retryIfNull } from './util'
import type SignalAPI from './api'

type DownloadAttachmentType = any
declare let sql: any

const mimeTypeToAttachmentType = (mimeType: string): MessageAttachmentType => {
  const mainType = mimeType.split('/')[0]
  return (
    {
      image: MessageAttachmentType.IMG,
      audio: MessageAttachmentType.AUDIO,
      video: MessageAttachmentType.VIDEO,
    }[mainType] || MessageAttachmentType.UNKNOWN
  )
}

export type AttachmentType = 'attachments' | 'sticker' | 'preview'

export const getFilePath = (accountInfo: AccountInfo, path: string): string =>
  url.pathToFileURL(Path.join(accountInfo.dataDirPath, 'attachments.noindex', path)).href

function mapAttachment(
  accountInfo: AccountInfo,
  messageID: string,
  attachment: DownloadAttachmentType,
  type: AttachmentType,
  index = 0,
): MessageAttachment {
  if (!attachment) return
  const {
    cdnId,
    cdnKey,
    contentType,
    width,
    height,
    fileName,
    path,
  } = attachment
  return {
    id: cdnId || cdnKey || path?.split('/').pop() || messageID,
    type: mimeTypeToAttachmentType(contentType),
    mimeType: contentType,
    fileName,
    size: {
      width,
      height,
    },
    srcURL: path
      ? getFilePath(accountInfo, path)
      : `asset://${accountInfo.accountID}/${type}/${messageID}_${index}`,
  }
}

type SignalMsgPreview = {
  url: string
  title: string
  image: DownloadAttachmentType
  description: string
}

type SignalMsg = MessageAttributesType & {
  // todo review extra types
  expires_at: number
}

/**
 * @see
 * Signal-Desktop/ts/groupChange.ts
 * Signal-Desktop/_locales/en/messages.json
 */
const mapGroupAction = async (
  change: GroupV2ChangeDetailType,
  sapi: SignalAPI,
  fromId?: string,
): Promise<string> => {
  const { currentUserUUID } = sapi
  if (!change) return ''

  switch (change.type) {
    case 'create': {
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (fromYou) return 'You created the group.'
      return `${fromMemberName} created the group.`
    }

    case 'access-attributes': {
      const { newPrivilege } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (newPrivilege === 3) {
        if (fromYou) return 'You changed who can edit group info to "Only admins."'
        if (fromMemberID) return `${fromMemberName} changed who can edit group info to "Only admins."`
        return 'An admin changed who can edit group info to "Only admins."'
      }

      if (newPrivilege === 2) {
        if (fromYou) return 'You changed who can edit group info to "All members."'
        if (fromMemberID) return `${fromMemberName} changed who can edit group info to "All members."`
        return 'An admin changed who can edit group info to "All members."'
      }

      return ''
    }

    case 'avatar': {
      const { removed } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (removed) {
        if (fromYou) return 'You removed the group avatar.'
        if (fromMemberID) return `${fromMemberName} removed the group avatar.`
        return 'A member removed the group avatar.'
      }

      if (fromYou) return 'You changed the group avatar.'
      if (fromMemberID) return `${fromMemberName} changed the group avatar.`
      return 'A member changed the group avatar.'
    }

    case 'title': {
      const { newTitle } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (newTitle) {
        if (fromYou) return `You changed the group name to "${newTitle}".`
        if (fromMemberID) return `${fromMemberName} changed the group name to "${newTitle}".`
        return `A member changed the group name to "${newTitle}".`
      }

      if (fromYou) return 'You removed the group name.'
      if (fromMemberID) return `${fromMemberName} removed the group name.`

      return 'A member removed the group name.'
    }

    case 'group-link-add': {
      const { privilege } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (privilege === 3) {
        if (fromYou) return 'You turned on the group link with admin approval enabled.'
        if (fromMemberID) return `${fromMemberName} turned on the group link with admin approval enabled.`
        return 'The group link has been turned on with admin approval enabled.'
      }

      if (privilege === 1) {
        if (fromYou) return 'You turned on the group link with admin approval disabled.'
        if (fromMemberID) return `${fromMemberName} turned on the group link with admin approval disabled.`
        return 'The group link has been turned on with admin approval disabled.'
      }
      // This shouldn't happen
      return ''
    }

    case 'member-add': {
      const memberID = change.uuid
      const name = await sapi.getProfileName(memberID)
      // mapped.action = {
      //   type: MessageActionType.THREAD_PARTICIPANTS_ADDED,
      //   actorParticipantID: memberID,
      //   participantIDs: [],
      // }
      return `${name} joined`
    }

    case 'member-privilege': {
      const newAdminMemberID = change.uuid
      const fromMemberID = fromId

      const newAdminName = await sapi.getProfileName(newAdminMemberID)
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      return `${fromMemberName} made ${newAdminName} an admin.`
    }

    case 'access-invite-link': {
      const { newPrivilege } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      const from = newPrivilege === 3
        ? `${fromMemberName} enabled admin approval for the group link.`
        : `${fromMemberName} disabled admin approval for the group link.`

      const unknown = newPrivilege === 3
        ? 'Admin approval for the group link has been enabled.'
        : 'Admin approval for the group link has been disabled.'

      return fromMemberID ? from : unknown
    }

    case 'access-members': {
      const { newPrivilege } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      const from = newPrivilege === 2
        ? `${fromMemberName} changed who can edit group membership to "Only admins."`
        : `${fromMemberName} changed who can edit group membership.`

      const unknown = newPrivilege === 2
        ? 'An admin changed who can edit group membership to "Only admins."'
        : 'An admin changed who can edit group membership.'

      return fromMemberID ? from : unknown
    }

    case 'member-add-from-invite': {
      const invitedId = change.uuid
      const invitedName = await sapi.getProfileName(invitedId)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      const from = `${fromMemberName} added invited member ${invitedName}.`
      const unknown = `A member added invited member ${invitedName}.`

      return fromMemberID ? from : unknown
    }

    case 'member-add-from-link': {
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      return `${fromMemberName} joined the group via the group link.`
    }

    case 'member-add-from-admin-approval': {
      const { uuid } = change
      const weAreJoiner = uuid === currentUserUUID
      const joinerName = await sapi.getProfileName(uuid)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      if (weAreJoiner) {
        return `${fromMemberName} approved your request to join the group.`
      } if (fromMemberID === currentUserUUID) {
        return `You approved a request to join the group from ${joinerName}.`
      }
      return `${fromMemberName} approved a request to join the group from ${joinerName}.`
    }

    case 'member-remove': {
      const { uuid } = change
      const weAreLeaver = uuid === currentUserUUID
      const leaverName = await sapi.getProfileName(uuid)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      if (weAreLeaver) {
        if (fromMemberID === currentUserUUID) {
          return 'You left the group.'
        } if (fromMemberID) {
          return `${fromMemberName} removed you.`
        }
        return 'You were removed from the group.'
      } if (fromMemberID === currentUserUUID) {
        return `You removed ${leaverName}.`
      } if (fromMemberID && fromMemberID === uuid) {
        return `${fromMemberName} left the group.`
      } if (fromMemberID) {
        return `${fromMemberName} removed ${leaverName}.`
      }
      return `A member removed ${leaverName}..`
    }

    case 'pending-add-one': {
      const { uuid } = change
      const weAreInvited = uuid === currentUserUUID
      const joinerName = await sapi.getProfileName(uuid)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      if (weAreInvited) {
        if (fromMemberID) {
          return `${fromMemberName} invited you to the group.`
        }
        return 'You were invited to the group.'
      } if (fromMemberID === currentUserUUID) {
        return `You invited ${joinerName} to the group.`
      } if (fromMemberID) {
        return `${fromMemberName} invited 1 person to the group.`
      }
      return 'One person was invited to the group.'
    }

    case 'pending-add-many': {
      const { count } = change
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)

      if (fromMemberID === currentUserUUID) {
        return `You invited ${count} people to the group.`
      } if (fromMemberID) {
        return `${fromMemberName} invited ${count} people to the group.`
      }
      return `${count} people were invited to the group.`
    }

    case 'pending-remove-one': {
      const { uuid, inviter } = change
      const joinerName = await sapi.getProfileName(uuid)
      const inviterName = await sapi.getProfileName(inviter)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      const weAreInvited = uuid === currentUserUUID
      const weAreInviter = Boolean(inviter && inviter === currentUserUUID)
      const sentByInvited = Boolean(fromMemberID && fromMemberID === uuid)
      const sentByInviter = Boolean(fromMemberID && inviter && fromMemberID === inviter)

      if (weAreInviter) {
        if (sentByInvited) {
          return `${joinerName} declined your invitation to the group.`
        } if (fromYou) {
          return `You rescinded your invitation to ${joinerName}.`
        } if (fromMemberID) {
          return `${fromMemberName} revoked the invitation to the group you sent to ${joinerName}.`
        }
        return `An admin revoked the invitation to the group you sent to ${joinerName}.`
      } if (sentByInvited) {
        if (fromYou) {
          return 'You declined the invitation to the group.'
        } if (inviter) {
          return `1 person invited by ${inviterName} declined the invitation to the group.`
        }
        return '1 person declined their invitation to the group.'
      } if (inviter && sentByInviter) {
        if (weAreInvited) {
          return `${inviterName} revoked their invitation to you.`
        }
        return `${inviterName} revoked their invitation to 1 person.`
      } if (inviter) {
        if (fromYou) {
          return `You revoked an invitation to the group for 1 person invited by ${inviterName}.`
        } if (fromMemberID) {
          return `${fromMemberName} revoked an invitation to the group for 1 person invited by ${inviterName}.`
        }
        return `An admin revoked an invitation to the group for 1 person invited by ${inviterName}.`
      } if (fromYou) {
        return 'You revoked an invitation to the group for 1 person.'
      } if (fromMemberID) {
        return `${fromMemberName} revoked an invitation to the group for 1 person.`
      }
      return 'An admin revoked an invitation to the group for 1 person.'
    }

    case 'pending-remove-many': {
      const { count, inviter } = change
      const inviterName = await sapi.getProfileName(inviter)
      const weAreInviter = Boolean(inviter && inviter === currentUserUUID)

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (weAreInviter) {
        if (fromYou) {
          return `You rescinded your invitation to ${count} people.`
        } if (fromMemberID) {
          return `${fromMemberName} revoked the invitations to the group you sent to ${count} people.`
        }
        return `An admin revoked the invitations to the group you sent to ${count} people.`
      } if (inviter) {
        if (fromYou) {
          return `You revoked invitations to the group for ${count} people invited by ${inviterName}.`
        } if (fromMemberID) {
          return `${fromMemberName} revoked invitations to the group for ${count} people invited by ${inviterName}.`
        }
        return `An admin revoked invitations to the group for ${count} people invited by ${inviterName}.`
      } if (fromYou) {
        return `You revoked invitations to the group for ${count} people.`
      } if (fromMemberID) {
        return `${fromMemberName} revoked invitations to the group for ${count} people.`
      }
      return `An admin revoked invitations to the group for ${count} people.`
    }

    case 'admin-approval-add-one': {
      const { uuid } = change
      const joinerName = await sapi.getProfileName(uuid)
      const weAreJoiner = uuid === currentUserUUID

      if (weAreJoiner) {
        return 'You sent a request to join the group.'
      }
      return `${joinerName} requested to join via the group link.`
    }

    case 'admin-approval-remove-one': {
      const { uuid } = change
      const joinerName = await sapi.getProfileName(uuid)
      const weAreJoiner = uuid === currentUserUUID

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (weAreJoiner) {
        if (fromYou) {
          return 'You canceled your request to join the group.'
        }
        return 'Your request to join the group has been denied by an admin.'
      } if (fromYou) {
        return `You denied a request to join the group from ${joinerName}.`
      } if (fromMemberID && fromMemberID === uuid) {
        return `${joinerName} canceled their request to join the group.`
      } if (fromMemberID) {
        return `${fromMemberName} denied a request to join the group from ${joinerName}.`
      }
      return `${joinerName} canceled their request to join the group.`
    }

    case 'group-link-reset': {
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (fromYou) {
        return 'You reset the group link.'
      } if (fromMemberID) {
        return `${fromMemberName} reset the group link.`
      }
      return 'The group link has been reset.'
    }

    case 'group-link-remove': {
      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (fromYou) {
        return 'You turned off the group link.'
      } if (fromMemberID) {
        return `${fromMemberName} turned off the group link.`
      }
      return 'The group link has been turned off.'
    }

    case 'description': {
      const { removed } = change

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (removed) {
        if (fromYou) {
          return 'You removed the group description.'
        } if (fromMemberID) {
          return `${fromMemberName} removed the group description.`
        }
        return 'The group description was removed.'
      } if (fromYou) {
        return 'You changed the group description.'
      } if (fromMemberID) {
        return `${fromMemberName} changed the group description.`
      }
      return 'The group description was changed.'
    }

    case 'announcements-only': {
      const { announcementsOnly } = change

      const fromMemberID = fromId
      const fromMemberName = await sapi.getProfileName(fromMemberID)
      const fromYou = fromMemberID === currentUserUUID

      if (announcementsOnly) {
        if (fromYou) {
          return 'You changed the group settings to only allow admins to send messages.'
        }
        if (fromMemberID) {
          return `${fromMemberName} changed the group settings to only allow admins to send messages.`
        }
        return 'The group was changed to only allow admins to send messages.'
      } if (fromYou) {
        return 'You changed the group settings to allow all members to send messages.'
      } if (fromMemberID) {
        return `${fromMemberName} changed the group settings to allow all members to send messages.`
      }
      return 'The group was changed to allow all members to send messages.'
    }
    default:
      break
  }
}

function mapMessageSeen(sendStateByConversationId: SendStateByConversationId) {
  if (!sendStateByConversationId) return
  const seen: MessageSeen = {}
  for (const [convId, { status }] of Object.entries(sendStateByConversationId)) {
    if (status === SendStatus.Read || status === SendStatus.Viewed) seen[convId] = UNKNOWN_DATE
  }
  return seen
}

export async function mapMessage(
  msg: SignalMsg,
  sapi: SignalAPI,
): Promise<Message> {
  if (typeof msg !== 'object' || !msg) {
    texts.log({ msg })
    texts.Sentry.captureMessage(`platform-signal received invalid message: ${typeof msg}`)
    return
  }

  const { expirationTimerUpdate, quote } = msg
  const timestamp = msg.timestamp || msg.received_at_ms
  // note: we probably dont need `|| msg.source` here
  const senderID = await sapi.getConversationIdBySourceId(msg.type === 'incoming' ? (msg.sourceUuid || msg.source) : sapi.currentUserUUID)
  let linkedMessageId: string

  if (quote) {
    const linkedMessages = await sql.getMessagesBySentAt(quote.id)
    // we won't have a linked message for messages from before the account was linked on texts
    if (linkedMessages.length) linkedMessageId = linkedMessages?.[0].id
  }

  const mapped: Message = {
    _original: JSON.stringify(msg),
    id: msg.id,
    timestamp: new Date(timestamp),
    senderID,
    threadID: msg.conversationId,
    text: msg.body,
    isSender: msg.type === 'outgoing',
    seen: mapMessageSeen(msg.sendStateByConversationId),
    isAction: undefined,
    action: undefined,
    linkedMessage: quote ? {
      id: linkedMessageId,
      text: quote.text,
      senderID: await sapi.getConversationIdBySourceId(quote.author ?? quote.authorUuid),
    } : undefined,
    expiresInSeconds: msg.expireTimer || (msg.expires_at ? (msg.expires_at - msg.expirationStartTimestamp) / 1000 : undefined),
  }

  if (msg.body && msg.bodyRanges) {
    const { text, textAttributes } = await mapTextAttributes(sapi, msg.body, msg.bodyRanges)
    mapped.text = text
    mapped.textAttributes = textAttributes
  }

  if (msg.preview) {
    mapped.links = msg.preview.map(({ url, title, image, description }: SignalMsgPreview, index): MessageLink => ({
      url,
      title,
      summary: description,
      img: mapAttachment(sapi.accountInfo, msg.id, image, 'preview', index)?.srcURL,
    }))
  }

  if (!mapped.text) {
    if (msg.group_update && msg.group_update.joined) {
      mapped.isAction = true
      mapped.action = {
        type: MessageActionType.THREAD_PARTICIPANTS_ADDED,
        actorParticipantID: senderID,
        participantIDs: [],
      }
      mapped.text = `${msg.group_update.joined} joined`
    }
    if (expirationTimerUpdate) {
      let name = ''
      if (expirationTimerUpdate.source) {
        name = await sapi.getProfileName(expirationTimerUpdate.source) || expirationTimerUpdate.source
      }
      mapped.isAction = true
      if (expirationTimerUpdate.expireTimer) {
        mapped.text = name
          ? `${name} set disappearing message time to ${formatSeconds(expirationTimerUpdate.expireTimer)}.`
          : `Disappearing message time was set to ${formatSeconds(expirationTimerUpdate.expireTimer)}.`
      } else {
        mapped.text = name
          ? `${name} disabled disappearing messages.`
          : 'Disappearing messages was disabled.'
      }
    }

    switch (msg.type) {
      case 'change-number-notification':
        mapped.senderID = await sapi.getConversationIdBySourceId(msg.sourceUuid)
        mapped.text = '{{sender}} changed their phone number'
        mapped.isAction = true
        mapped.parseTemplate = true
        break
      case 'delivery-issue':
        mapped.text = "A message couldn't be delivered"
        mapped.isAction = true
        mapped.parseTemplate = true
        mapped.isErrored = true
        break
      case 'group-v1-migration':
        mapped.text = 'This group was upgraded to a new group.'
        mapped.isAction = true
        break
      case 'keychange':
        mapped.text = 'Your safety number with {{sender}} has changed.'
        mapped.isAction = true
        mapped.parseTemplate = true
        break
      case 'group-v2-change': {
        const promises = msg.groupV2Change.details.map(d => mapGroupAction(d, sapi, msg.groupV2Change.from))

        mapped.text = (await Promise.all(promises)).filter(Boolean).join('\n')
        mapped.isAction = true
        mapped.parseTemplate = true
        break
      }
      case 'profile-change':
        if (msg.profileChange.type === 'name') {
          mapped.senderID = msg.changedId
          mapped.text = `{{sender}} changed their profile name from ${msg.profileChange.oldName} to ${msg.profileChange.newName}.`
          mapped.isAction = true
          mapped.parseTemplate = true
        }
        break

      default:
        break
    }

    if (msg.deletedForEveryone) {
      mapped.isDeleted = true
    }
  }

  if (msg.reactions) {
    mapped.reactions = await Promise.all(msg.reactions.map<Promise<MessageReaction>>(
      async ({ emoji, targetTimestamp, fromId }) => ({
        id: fromId,
        reactionKey: emoji,
        participantID: fromId,
        emoji: true,
      }),
    ))
  }
  if (msg.attachments?.length) {
    mapped.attachments = msg.attachments.map((a, index) => mapAttachment(sapi.accountInfo, msg.id, a, 'attachments', index)).filter(Boolean)
  } else if (msg.sticker) {
    if (!mapped.attachments) mapped.attachments = []
    const sticker = mapAttachment(sapi.accountInfo, msg.id, msg.sticker.data, 'sticker')
    if (sticker) mapped.attachments.push(sticker)
  }
  if (Array.isArray(msg.errors) && msg.errors.length > 0) {
    const json = JSON.stringify(msg.errors, null, 2)
    mapped.isErrored = true
    if (mapped.textHeading) mapped.textHeading += json
    else mapped.textHeading = json
  }
  return mapped
}

export function mapThreadMetaOnly(conv: any): Pick<Thread, '_original' | 'id' | 'title' | 'isUnread' | 'isReadOnly' | 'isArchived' | 'messageExpirySeconds' | 'mutedUntil'> {
  return {
    _original: JSON.stringify(conv),
    id: conv.id,
    title: conv.name,
    isUnread: conv.unreadCount > 0,
    isReadOnly: conv.left,
    isArchived: conv.isArchived,
    messageExpirySeconds: conv.expireTimer,
    mutedUntil: conv.muteExpiresAt > 0 ? 'forever' : undefined,
  }
}

export async function mapThread(
  conv: any,
  sapi: SignalAPI,
): Promise<Thread> {
  const { id } = conv
  if (!id) {
    texts.log('null thread.id for conv', conv)
    return
  }
  let participantItems: Participant[] = []
  if (conv.type === 'private') {
    participantItems = [
      await sapi.getUserByConversationId(id),
    ]
  } else if (conv.members) {
    participantItems = await Promise.all(
      conv.members.map(id => sapi.getUserByConversationId(id)),
    )
  } else if (conv.membersV2) {
    participantItems = await Promise.all(
      conv.membersV2.map(member => sapi.getUserByConversationId(member.uuid || member.conversationId)),
    )
  }

  let lastMessage: Message
  if (conv.messageCount) {
    const lastActiveMessage = await sql.getLastConversationMessage({
      conversationId: conv.id,
    })
    if (lastActiveMessage) {
      lastMessage = await mapMessage(lastActiveMessage, sapi)
    }
  }

  return {
    ...mapThreadMetaOnly(conv),
    type: conv.type === 'private' ? 'single' : 'group',
    messages: {
      items: lastMessage ? [lastMessage] : [],
      hasMore: !!lastMessage,
      oldestCursor: null,
    },
    participants: {
      items: participantItems,
      hasMore: false,
      oldestCursor: null,
    },
    timestamp: lastMessage
      ? lastMessage.timestamp
      : (conv.timestamp ? new Date(conv.timestamp) : undefined),
    imgURL: conv.profileAvatar?.path
      ? getFilePath(sapi.accountInfo, conv.profileAvatar.path)
      : undefined,
  }
}

export async function getMessageBySentAt(
  sentAt: number,
  sapi: SignalAPI,
): Promise<Message> {
  const messages = await sql.getMessagesBySentAt(sentAt)
  if (!messages.length) return
  return mapMessage(messages[0], sapi)
}

export async function mapServerEvent(
  msg: any,
  sapi: SignalAPI,
): Promise<ServerEvent> {
  switch (msg.type) {
    case 'conversation': {
      const conv = msg.data
      const thread = await mapThread(conv, sapi)
      if (!thread.id) return
      return {
        type: ServerEventType.STATE_SYNC,
        objectIDs: {},
        mutationType: 'upsert',
        objectName: 'thread',
        entries: [thread],
      }
    }
    case 'message':
    case 'sent': {
      let { timestamp } = msg.data
      if (msg.data.message.delete && msg.data.message.delete.targetSentTimestamp) {
        timestamp = msg.data.message.delete.targetSentTimestamp
      }
      const ret = await retryIfNull(async () => {
        const message = await getMessageBySentAt(timestamp, sapi)
        if (!message) return
        return {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: message.threadID,
          },
          mutationType: 'upsert',
          objectName: 'message',
          entries: [message],
        }
      })
      if (ret) {
        return ret
      }
      // For some messages (e.g. emoji update), msg.data.timestamp is not the
      // original msg id, so refresh the thread instead.
      const isSender = msg.type === 'sent'
      const sourceId = msg.data.message.groupV2?.id || msg.data.message.group?.id || (isSender ? msg.data.destination : msg.data.source)
      const threadID = await sapi.getConversationIdBySourceId(sourceId)
      return {
        type: ServerEventType.THREAD_MESSAGES_REFRESH,
        threadID,
      }
    }
    case 'read': { // Outgoing message read by others
      const messages = await sql.getMessagesBySentAt(msg.receipt.timestamp)
      if (!messages.length) return
      const firstMessage: SignalMsg = messages[0]
      const threadID = firstMessage.conversationId
      return {
        type: ServerEventType.STATE_SYNC,
        objectIDs: { threadID },
        mutationType: 'update',
        objectName: 'message',
        entries: [{
          id: firstMessage.id,
          seen: mapMessageSeen(firstMessage.sendStateByConversationId),
        }],
      }
    }
    case 'readSync': { // Incoming message ready by myself on another instance
      const messages = await sql.getMessagesBySentAt(msg.read.timestamp)
      if (!messages.length) return
      const threadID = messages[0].conversationId
      const conversation = await sapi.getConversation(threadID)
      return {
        type: ServerEventType.STATE_SYNC,
        mutationType: 'update',
        objectName: 'thread',
        objectIDs: {},
        entries: [
          {
            id: threadID,
            isUnread: conversation.unreadCount > 0,
          },
        ],
      }
    }
    case 'typing': {
      const threadID = await sapi.getConversationIdBySourceId(msg.typing.groupV2Id || msg.typing.groupId || msg.sender)
      const participantID = await sapi.getConversationIdBySourceId(msg.sender || msg.senderUuid)
      return {
        type: ServerEventType.USER_ACTIVITY,
        activityType: msg.typing.stopped ? ActivityType.NONE : ActivityType.TYPING,
        threadID,
        participantID,
        durationMs: 10_000,
      }
    }
    case 'fetchLatest': {
      if (msg.eventType !== 2) { // 2: FETCH_LATEST_ENUM.STORAGE_MANIFEST
        return
      }

      return {
        type: ServerEventType.STATE_SYNC,
        mutationType: 'update',
        objectName: 'thread',
        objectIDs: {},
        entries: await sapi.getThreadsMetaOnly(),
      }
    }
    default:
      break
  }
}

export async function convertUserMentions(
  sapi: SignalAPI,
  content: MessageContent,
): Promise<{
    text?: string
    mentions?: BodyRangeType[]
  }> {
  if (!content.text) return {}
  let text = ''
  const mentions: BodyRangeType[] = []
  let cursor = 0
  for (const userID of (content.mentionedUserIDs || [])) {
    const { phoneNumber, uuid } = await sapi.getUserByConversationId(userID) || { phoneNumber: '', uuid: '' }
    const token = `@${phoneNumber}`
    const index = content.text.indexOf(token, cursor)
    if (cursor < index) {
      text += content.text.slice(cursor, index)
    }
    mentions.push({
      start: text.length,
      length: 1,
      mentionUuid: uuid,
    })
    cursor = index + token.length
    text += '\ufffc'
  }
  text += content.text.slice(cursor)
  return {
    text,
    mentions,
  }
}
