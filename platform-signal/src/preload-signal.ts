/**
 * Preload Whisper and Signal globals for background.ts. Whenever possible, try
 * not editing background.ts so that it's easier to sync with Signal-Desktop.
 * Add things to preload-global.ts or preload-signal.ts to get it working.
 */

import path from 'path'
import fs from 'fs'
import crypto from 'crypto'
import _ from 'lodash'
import sizeOf from 'image-size'
import { LocalStorage } from 'node-localstorage'
// import './ts/window.d'

declare let Backbone: any
declare let TEXTS_DATA_DIR: any
const userDataPath = TEXTS_DATA_DIR

declare global {
  interface Window {
    sql: any
    sqlChannels: any
    log: Function
  }
}

const noop = () => {}
const proxyThatReturnsNoopForEverything = (name: string) => new Proxy({}, {
  get: (target, key) => {
    if (key in target) return target[key]
    console.log(`[preload-signal] returning noop function for ${name}.${key.toString()}`)
    return noop
  },
})

global.localStorage = new LocalStorage(userDataPath)

/**
 * This is to make getImageDimensions of visual_attachment.js in Signal-Desktop
 * happy.
 */
class ImageElementMock {
  handlers: Map<string, Function> = new Map()

  naturalWidth = 0

  naturalHeight = 0

  set src(src: string) {
    this.setSrc(src)
  }

  async setSrc(src: string) {
    let buf: Buffer
    if (src.startsWith('data:')) {
      const base64 = src.slice(src.indexOf(',') + 1)
      buf = Buffer.from(base64, 'base64')
    } else {
      buf = await fs.promises.readFile(src)
    }
    if (!buf) {
      this.handlers.get('error')?.()
    }
    const dimension = sizeOf(buf)
    if (dimension) {
      this.naturalWidth = dimension.width
      this.naturalHeight = dimension.height
      this.handlers.get('load')?.()
    } else {
      this.handlers.get('error')?.()
    }
  }

  addEventListener(name: string, handler: Function) {
    this.handlers.set(name, handler)
  }
}

window.document = {
  addEventListener() {},
  body: {
    addEventListener() {},
    addClass() {
      return this
    },
    removeClass() {},
  },
  createElement(tag: string) {
    if (tag === 'img') {
      return new ImageElementMock()
    }
    return {
      style: {},
      setAttribute() {},
    }
  },
  querySelector() {},
  documentElement: proxyThatReturnsNoopForEverything('document.documentElement'),
} as any

window.SignalContext = window.SignalContext || {}
window.SignalContext.getPath = () => userDataPath
window.SignalContext.log = window.log
require('Signal-Desktop/ts/windows/context') // unsure if this is used anywhere
const { Timers } = require('Signal-Desktop/ts/context/Timers')

window.SignalContext.timers = new Timers()
const { Crypto } = require('Signal-Desktop/ts/context/Crypto')

window.SignalContext.crypto = new Crypto()

const config = require('Signal-Desktop/config/default.json')
const configProduction = require('Signal-Desktop/config/production.json')

// Needed to ask for devices, and not saved in production
const packageJson = require('Signal-Desktop/package.json')

config.version = packageJson.version

// Overwrite urls for production used
config.serverUrl = configProduction.serverUrl
config.storageUrl = configProduction.storageUrl
config.cdn = configProduction.cdn
config.serverPublicParams = configProduction.serverPublicParams
config.serverTrustRoot = configProduction.serverTrustRoot

window.SignalContext.config = config
window.SignalContext.getAppInstance = () => config.appInstance
window.SignalContext.getNodeVersion = () => config.node_version

// FROM: preload.js
window.platform = process.platform
window.getTitle = () => ''
// window.getEnvironment = () => 'production'
window.getVersion = () => config.version
window.getExpiration = () => config.buildExpiration
window.getHostName = () => config.hostname
window.getServerTrustRoot = () => config.serverTrustRoot
window.getServerPublicParams = () => config.serverPublicParams
window.isBehindProxy = () => Boolean(config.proxyUrl)

window.textsecure = require('Signal-Desktop/ts/textsecure').default
require('Signal-Desktop/libtextsecure/protocol_wrapper')

// adapted from Signal-Desktop/preload.js
window.WebAPI = (window.textsecure as any).WebAPI.initialize({
  url: config.serverUrl,
  storageUrl: config.storageUrl,
  updatesUrl: config.updatesUrl,
  directoryVersion: parseInt(config.directoryVersion, 10),
  directoryUrl: config.directoryUrl,
  directoryEnclaveId: config.directoryEnclaveId,
  directoryTrustAnchor: config.directoryTrustAnchor,
  directoryV2Url: config.directoryV2Url,
  directoryV2PublicKey: config.directoryV2PublicKey,
  directoryV2CodeHashes: config.directoryV2CodeHashes,
  cdnUrlObject: {
    0: config.cdn['0'],
    2: config.cdn['2'],
  },
  certificateAuthority: config.certificateAuthority,
  contentProxyUrl: config.contentProxyUrl,
  proxyUrl: config.proxyUrl,
  version: config.version,
})

const Signal = require('Signal-Desktop/js/modules/signal')
const Attachments = require('Signal-Desktop/ts/windows/attachments')

window.Signal = Signal.setup({
  Attachments,
  userDataPath,
  getRegionCode: () => window.storage.get('regionCode'),
  logger: window.log,
})
window.Signal.Services.initializeNetworkObserver = noop
window.Signal.Services.initializeUpdateListener = noop
window.Signal.Services.calling = proxyThatReturnsNoopForEverything('window.Signal.Services.calling')

const keyStore = {
  put<T>(key: string, value: T) {
    fs.writeFileSync(
      path.join(userDataPath, key),
      window.textsecure.utils.jsonThing(value),
    )
  },
  get<T>(key: string, defaultValue?: T) {
    try {
      const raw = fs.readFileSync(path.join(userDataPath, key))
      if (typeof raw === 'undefined') {
        return defaultValue
      }
      return JSON.parse(`${raw.toString()}`)
    } catch (e) {
      return defaultValue
    }
  },
  remove(key: string) {
    try {
      fs.unlinkSync(path.join(userDataPath, key))
    } catch (e) {
      console.error(e)
    }
  },
}

require('Signal-Desktop/js/reliable_trigger')
require('Signal-Desktop/js/database')

window.Whisper.events = _.clone(Backbone.Events)
window.addEventListener = window.Whisper.events.on

window.libphonenumber = require('google-libphonenumber').PhoneNumberUtil.getInstance()
window.libphonenumber.PhoneNumberFormat = require('google-libphonenumber').PhoneNumberFormat
require('Signal-Desktop/js/libphonenumber-util')

require('Signal-Desktop/ts/models/messages')
require('Signal-Desktop/ts/models/conversations')

require('Signal-Desktop/ts/backbone/views/whisper_view')
// require('Signal-Desktop/ts/views/conversation_view')
// require('Signal-Desktop/ts/views/inbox_view')
require('Signal-Desktop/ts/SignalProtocolStore')
require('./ts/background')

require('Signal-Desktop/js/expiring_messages')
require('Signal-Desktop/js/expiring_tap_to_view_messages')

require('Signal-Desktop/ts/util/registration')
// require('Signal-Desktop/js/reactions')

require('Signal-Desktop/js/wall_clock_listener')
require('Signal-Desktop/ts/textsecure/RotateSignedPreKeyListener')

const attachments = require('Signal-Desktop/app/attachments')
const attachmentChannel = require('Signal-Desktop/app/attachment_channel')

/*
Signal-Desktop does this instead:
  const { MainSQL } = require('ts/sql/main')
which creates a worker thread requiring Signal-Desktop/ts/sql/Server
instead we require it directly and assign sqlCall
*/
window.sql = require('Signal-Desktop/ts/sql/Server').default
window.sql.sqlCall = (methodName: string, args: any[]) =>
  window.sql[methodName](...args)

window.sqlChannels = require('Signal-Desktop/app/sql_channel')

window.Whisper.events.on('unauthorized', () => {
  console.log('unauthorized!')
})
window.Whisper.events.on('reconnectTimer', () => {
  console.log('reconnect timer!')
})

// sync with cleanupOrphanedAttachments in platform-signal/Signal-Desktop/app/main.ts
async function cleanupOrphanedAttachments() {
  const allAttachments = await attachments.getAllAttachments(userDataPath)
  const orphanedAttachments = await window.sql.sqlCall('removeKnownAttachments', [
    allAttachments,
  ])
  await attachments.deleteAll({
    userDataPath,
    attachments: orphanedAttachments,
  })

  await attachments.deleteAllBadges({
    userDataPath,
    pathsToKeep: await window.sql.sqlCall('getAllBadgeImageFileLocalPaths', []),
  })

  const allStickers = await attachments.getAllStickers(userDataPath)
  const orphanedStickers = await window.sql.sqlCall('removeKnownStickers', [
    allStickers,
  ])
  await attachments.deleteAllStickers({
    userDataPath,
    stickers: orphanedStickers,
  })

  const allDraftAttachments = await attachments.getAllDraftAttachments(
    userDataPath,
  )
  const orphanedDraftAttachments = await window.sql.sqlCall(
    'removeKnownDraftAttachments',
    [allDraftAttachments],
  )
  await attachments.deleteAllDraftAttachments({
    userDataPath,
    attachments: orphanedDraftAttachments,
  })
}

export default async function main() {
  let key = keyStore.get('key')
  // FROM: main.js
  if (!key) {
    console.log(
      'key/initialize: Generating new encryption key, since we did not find it on disk',
    )
    // https://www.zetetic.net/sqlcipher/sqlcipher-api/#key
    key = crypto.randomBytes(32).toString('hex')
    keyStore.put('key', key)
  }

  // Adapted
  const sqlInitPromise = window.sql.initialize({
    configDir: userDataPath,
    key,
    logger: window.log,
  })

  // ...
  const success = await sqlInitPromise

  // if (!success) {
  //   console.log('sql.initialize was unsuccessful; returning early')
  //   return
  // }

  try {
    await attachments.clearTempPath(userDataPath)
  } catch (error) {
    console.error(
      'main/ready: Error deleting temp dir:',
      error && error.stack ? error.stack : error,
    )
  }
  await attachmentChannel.initialize({
    configDir: userDataPath,
    cleanupOrphanedAttachments,
  })
}

export const { emitter } = window
