import { parentPort } from 'worker_threads'
import PlatformAPI from '../api'

const papi = new PlatformAPI()

const DEBUG = !!process.env.DEBUG

export type BridgeToMainMessage = {
  reqID: number
  type: 'method-result'
  result?: any
  error?: { name: string, message: string }
} | {
  type: 'callback'
  methodName: string
  args: any[]
}

export type MainToBridgeMessage = {
  reqID: number
  type: 'call-method'
  methodName: string
  args: any[]
  isCallback: boolean
}

const callParent = parentPort
  ? (message: BridgeToMainMessage) => { parentPort.postMessage(message) }
  : (message: BridgeToMainMessage) => { process.send(message) }

const onMessageHandler = async (msg: 'cleanup' | 'powermonitor-on-resume' | MainToBridgeMessage) => {
  if (msg === 'cleanup') return
  if (msg === 'powermonitor-on-resume') return
  if (DEBUG) console.log('message from parent:', msg)
  switch (msg.type) {
    case 'call-method': {
      const { reqID, methodName, args, isCallback } = msg
      try {
        const method = papi[methodName]
        const result = await (isCallback
          ? method?.((...cbArgs: any[]) => {
            callParent({
              type: 'callback',
              methodName,
              args: cbArgs,
            })
          })
          : method?.(...args))
        callParent({
          type: 'method-result',
          reqID,
          result,
        })
      } catch (err) {
        console.error('bridge-entry error', { methodName, args }, err)
        callParent({
          type: 'method-result',
          reqID,
          error: { name: err.name, message: err.message },
        })
      }
      return
    }
    default:
      console.log(msg)
      throw Error('unknown message')
  }
}

(parentPort ?? process).on('message', onMessageHandler)

process.on('uncaughtException', err => {
  console.error('Unhandled Exception', err)
})
process.on('unhandledRejection', err => {
  console.error('Unhandled Promise Rejection', err)
})
