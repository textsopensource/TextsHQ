// calls PlatformAPI running in a worker thread
// we do this for signal since it pollutes globals

import { PlatformAPI, AccountInfo, texts, SerializedSession } from '@textshq/platform-sdk'
import path from 'path'
import { promises as fs } from 'fs'
import { parentPort } from 'worker_threads'

import { Bridge, ChildProcessBridge, WorkerBridge } from './bridges'
import type { BridgeToMainMessage, MainToBridgeMessage } from './entry'

if (texts.IS_DEV) {
  // eslint-disable-next-line import/no-extraneous-dependencies, global-require
  require('source-map-support').install()
}

const PLATFORM_DIR_NAME = 'platform-signal'

const ROOT_DIR_PATH = process.env.NODE_ENV === 'development' && process.platform as string !== 'ios'
  ? path.join(__dirname, `../../../packages/${PLATFORM_DIR_NAME}/binaries`)
  : path.join(texts.constants.BUILD_DIR_PATH, PLATFORM_DIR_NAME)

const BRIDGE_ENTRY_JS_PATH = path.join(ROOT_DIR_PATH, 'bridge-entry.js')
const NODE_CONFIG_DIR = path.join(ROOT_DIR_PATH, 'empty-config')

const SelectedBridge = process.env.PAPI_PROXY_USE_CP_BRIDGE ? ChildProcessBridge : WorkerBridge

// this is like PlatformAPIRelayer
class BridgedPlatformAPI implements Partial<PlatformAPI> {
  private bridge: Bridge

  private readonly requestQueue = new Map<number, { resolve: Function, reject: Function }>()

  // key: methodName, value: callback
  private readonly callbacks = new Map<string, Function>()

  private requestId = 0

  private callMethod<T>(methodName: string, args: any[], isCallback = false) {
    return new Promise<T>((resolve, reject) => {
      const reqID = ++this.requestId
      this.requestQueue.set(reqID, { resolve, reject })
      this.bridge.postMessage({
        type: 'call-method',
        reqID,
        methodName,
        args,
        isCallback,
      } as MainToBridgeMessage)
    })
  }

  private handleWorkerCleanupMessage = (value: any) => {
    if (value === 'cleanup') {
      this.dispose()
    }
  }

  private async initBridge(dataDirPath: string) {
    await fs.mkdir(dataDirPath, { recursive: true })
    parentPort?.on('message', this.handleWorkerCleanupMessage)
    this.bridge = new SelectedBridge(BRIDGE_ENTRY_JS_PATH, { NODE_CONFIG_DIR }, dataDirPath)
    await this.bridge.initPromise
    this.bridge.onMessage((msg: BridgeToMainMessage) => {
      switch (msg.type) {
        case 'method-result': {
          const { reqID, result, error } = msg
          if (this.requestQueue.has(reqID)) {
            const promise = this.requestQueue.get(reqID)
            if (error) {
              promise.reject(new Error(error.name + ' ' + error.message))
            } else { // result can be undefined
              promise.resolve(result)
            }
            this.requestQueue.delete(reqID)
          } else {
            throw Error('unknown method-result message')
          }
          return
        }
        case 'callback': {
          const { methodName, args } = msg
          this.callbacks.get(methodName)?.(...args)
          break
        }
        default:
          console.log(msg)
          throw Error('unknown BridgeToMainMessage')
      }
    })
  }

  constructor() {
    // eslint-disable-next-line no-constructor-return
    return new Proxy(this, {
      get(target, prop) {
        if (prop in target) return target[prop]
        return (...args: any[]) => {
          if (typeof args[0] === 'function') {
            target.callbacks.set(prop as string, args[0])
            return target.callMethod(prop as string, undefined, true)
          }
          return target.callMethod(prop as string, args)
        }
      },
      has: () => true,
    })
  }

  init = async (session: SerializedSession, accountInfo: AccountInfo) => {
    await this.initBridge(accountInfo.dataDirPath)
    return this.callMethod<void>('init', [session, accountInfo])
  }

  dispose = async () => {
    await this.callMethod('dispose', [])
    this.bridge?.dispose()
    parentPort?.off('message', this.handleWorkerCleanupMessage)
  }
}

export default BridgedPlatformAPI
