import type { TextAttributes } from '@textshq/platform-sdk'
import type SignalAPI from './api'
import type { BodyRangeType } from './types/Message'

export async function mapTextAttributes(
  scb: SignalAPI,
  body: string,
  bodyRanges: BodyRangeType[],
): Promise<{
    text: string
    textAttributes: TextAttributes
  }> {
  let text = ''
  let cursor = 0
  const entities = []
  for (const { start, length, mentionUuid } of bodyRanges) {
    if (start > cursor) {
      text += body.slice(cursor, start)
    }
    cursor = start + length
    const username = await scb.getProfileName(mentionUuid.toLowerCase())
    const mention = `@${username}`
    const from = Array.from(text).length
    entities.push({
      from,
      to: from + mention.length,
      mentionedUser: {
        username,
        id: mentionUuid,
      },
    })
    text += mention
  }
  text += body.slice(cursor)

  return {
    text,
    textAttributes: {
      entities,
    },
  }
}
