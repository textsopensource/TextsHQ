import { setTimeout as setTimeoutAsync } from 'timers/promises'
import { texts } from '@textshq/platform-sdk'
import { getLinkPreview } from 'link-preview-js'

import type { LinkPreviewType } from './types/Message'

export async function fetchLinkPreview(text: string) {
  try {
    // @ts-expect-error
    const preview = await getLinkPreview(text, { imagesPropertyType: 'og', followRedirects: true })
    const linkPreview: LinkPreviewType = {
      description: 'description' in preview ? preview.description : '',
      title: 'title' in preview ? preview.title : '',
      url: text,
      domain: preview.url,
      isStickerPack: false,
    }
    if ('images' in preview && preview.images?.length) {
      const imageUrl = preview.images[0]
      const buffer = await texts.fetch(imageUrl)
      if (buffer.body) {
        linkPreview.image = {
          data: buffer.body,
          size: buffer.body.byteLength,
          contentType: buffer.headers['content-type'] || 'image/png',
        }
      }
    }
    return linkPreview
  } catch (err) {
    texts.error(err)
    texts.Sentry.captureException(err)
  }
  return null
}

const pluralize = (count: number, singular: string) => `${singular}${count > 1 ? 's' : ''}`

export function formatSeconds(seconds: number) {
  if (seconds < 60) return `${seconds} seconds`
  const minutes = seconds / 60
  if (minutes < 60) return `${minutes.toFixed(0)} ${pluralize(minutes, 'minute')}`
  const hours = minutes / 60
  if (hours < 24) return `${hours.toFixed(0)} ${pluralize(hours, 'hour')}`
  const days = hours / 24
  if (days < 7) return `${days.toFixed(0)} ${pluralize(days, 'day')}`
  const weeks = days / 7
  return `${weeks.toFixed(0)} ${pluralize(weeks, 'week')}`
}

export async function retryIfNull(fn: () => Promise<any>, times = 1) {
  if (times > 10) return

  const res = await fn()
  if (res) return res

  await setTimeoutAsync(300 * times)
  return retryIfNull(fn, times + 1)
}
