#!/usr/bin/env sh

git submodule update --init
cd Signal-Desktop
yarn --ignore-optional
yarn postinstall
yarn build-protobuf
cd ..

for mod in {backbone,underscore,google-libphonenumber}; do
  rm node_modules/$mod
  ln -s ../Signal-Desktop/node_modules/$mod node_modules/$mod
done

cp Signal-Desktop/node_modules/vm2/lib/{bridge,setup-sandbox}.js binaries/
src/ts/patch.sh
yarn build
