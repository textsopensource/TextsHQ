#!/usr/bin/env sh

yarn esbuild --version

# also see https://github.com/signalapp/Signal-Desktop/blob/main/scripts/esbuild.js
#
# hjson,toml,cson,properties,x2js is for the config npm module
# fixes ✘ [ERROR] Could not resolve "X" in esbuild
# Signal-Desktop/node_modules/config/lib/config.js

yarn esbuild src/bridge/entry.ts --target=node16 --outfile=binaries/bridge-entry.js --bundle --platform=node \
        --minify-whitespace --minify-syntax --keep-names \
        --external:@signalapp/libsignal-client \
        --external:better-sqlite \
        --external:fs-xattr \
        --external:hjson \
        --external:toml \
        --external:cson \
        --external:properties \
        --external:x2js

ls -lah binaries
