#!/usr/bin/env sh

rm -rf Signal-Desktop/node_modules/@signalapp/libsignal-client
rm -rf Signal-Desktop/node_modules/better-sqlite3

rm -rf node_modules/@signalapp/libsignal-client
rm -rf node_modules/better-sqlite3

mkdir -p node_modules/@signalapp
ln -fs ../../../../texts-app-desktop/app/node_modules/@signalapp/libsignal-client node_modules/@signalapp/libsignal-client
ln -fs ../../../texts-app-desktop/app/node_modules/better-sqlite node_modules/better-sqlite
