// to self
_ = {
  items: [
    {
      message: {
        metadata: {
          thread: {
            other_user_id: { int: -100000229845392n, num: -100000229845392 },
            thread_fbid: [506297343],
          },
          id: 'mid.$cAABa8x4taY-BDg7TvF63MmJiG0bt',
          offline_threading_id: {
            int: -6824979158437414638n,
            num: -6824979158437415000,
          },
          sender: { int: -100000229845392n, num: -100000229845392 },
          timestamp: { int: -1627201833917n, num: -1627201833917 },
          tags: ['source:chat:web:light_speed:inbox', 'app_id:772021112871879'],
        },
        text: '5',
        attachments: [],
      },
    },
  ],
  first_seq_id: { int: -100000229845392n, num: -100000229845392 },
}

// to another user
_ = {
  items: [
    {
      message: {
        metadata: {
          thread: {
            other_user_id: { int: -1369353851n, num: -1369353851 },
            thread_fbid: [-778129798, 506297343],
          },
          id: 'mid.$cAABa80-zx_WBDihDyV63OL6JUeiH',
          offline_threading_id: {
            int: -6824986151302064264n,
            num: -6824986151302064000,
          },
          sender: { int: -100000229845392n, num: -100000229845392 },
          timestamp: { int: -1627203501002n, num: -1627203501002 },
          tags: ['source:chat:web:light_speed:inbox', 'app_id:772021112871879'],
        },
        text: 'testing, pls mute!',
        attachments: [],
      },
    },
  ],
  first_seq_id: { int: -100000229845392n, num: -100000229845392 },
}

// to a group
_ = {
  items: [
    { message: { metadata: {} } },
    {
      message: {
        metadata: {
          thread: {
            other_user_id: { int: -3575407235904788n, num: -3575407235904788 },
          },
          id: 'mid.$gAAyz0F45NROBDioXu163OTNURUdd',
          offline_threading_id: {
            int: -6824986652923217758n,
            num: -6824986652923218000,
          },
          sender: { int: -100000229845392n, num: -100000229845392 },
          timestamp: { int: -1627203620796n, num: -1627203620796 },
          tags: ['source:chat:web:light_speed:inbox', 'app_id:772021112871879'],
        },
        text: 'testing, pls mute',
        attachments: [],
      },
    },
  ],
  first_seq_id: { int: -100000229845392n, num: -100000229845392 },
}
