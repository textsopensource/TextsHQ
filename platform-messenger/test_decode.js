const assert = require('assert')
const {
  thriftDescriptors,
  thriftReadToObject,
  unzipAsync,
} = require('./dist/mqttot')

async function testSendMessageResponse() {
  const payload = Buffer.from(
    '78da62106b6998b762cd8be3ff17320a3200000000ffff030030f6066e',
    'hex'
  )
  const parsed = thriftReadToObject((await unzipAsync(payload)).slice(1), [
    thriftDescriptors.int64('offline_threading_id', 1),
    thriftDescriptors.boolean('success', 2),
    thriftDescriptors.binary('error_message', 4),
  ])
  console.log('testSendMessageResponse:', parsed)
  assert.deepEqual(
    parsed,
    {
      offline_threading_id: {
        int: -5836541565400760323n,
        num: -5836541565400760000,
      },
      success: true,
    },
    'SendMessageResponse should be parsed correctly'
  )
}

async function testMessageSyncPayload() {
  const payload = Buffer.from(
    '78da629094d1919111fbb2f3eb4c2e0609a5dccc143d9564471070734f2dca2b88b788770a3731342d2f2cf64b4e8bac4812dbb6f1cdf1d71bae7ed8c308d125766cf78d553be2043535148af34b8b9253ad9233124bacca5393ac7232d3334ae28b0b525353c082e2890505f199295646464606c69686e61616460606169646a26c42355a50373048b066a45656565af2303088ed69e504638814000000ffff030092e73e2b',
    'hex'
  )
  let unzipped = await unzipAsync(payload)
  // unzipped = Buffer.from(
  //   '1644742f57454a797352723679545966333439696d475100191c2c1c1c16dcd3deb5e1c02d0018226d69642e2463414141414166306e6e597141414a6167616c357a314657395046457316d8c4e2d3b795eaf3bc0116c8a5e2e7dec02d16d4e18af5b95e11292820736f757263653a636861743a7765623a6c696768745f73706565643a63686174176170705f69643a323232303339313738383230303839321506127c2a26c8a5e2e7dec02ddcd3deb5e1c02d00001807686579206a6f65390c000016bc0416bc0416c8a5e2e7dec02d00',
  //   'hex'
  // )
  unzipped = unzipped.slice(unzipped.indexOf(0) + 1)
  const parsed = thriftReadToObject(unzipped, [
    thriftDescriptors.listOfStruct('items', 1, [
      thriftDescriptors.struct('message', 0, [
        thriftDescriptors.struct('metadata', 0, [
          thriftDescriptors.struct('thread', 0, [
            thriftDescriptors.int64('other_user_id', 1),
            thriftDescriptors.setOfInt64('thread_fbid', 2), // why it's a set?
          ]),
          thriftDescriptors.binary('id', 2),
          thriftDescriptors.int64('offline_threading_id', 3),
          thriftDescriptors.int64('sender', 4),
          thriftDescriptors.int64('timestamp', 5),
          thriftDescriptors.binary('action_summary', 7),
          thriftDescriptors.listOfBinary('tags', 8),
          thriftDescriptors.binary('message_unsendability', 12),
        ]),
        thriftDescriptors.binary('text', 2),
        thriftDescriptors.int64('sticker', 4),
        thriftDescriptors.listOfStruct('attachments', 5, []),
        thriftDescriptors.struct('extra_metadata', 7, []),
      ]),
      thriftDescriptors.struct('own_read_receipt', 4, []),
      thriftDescriptors.struct('add_member', 8, []),
      thriftDescriptors.struct('remove_member', 9, []),
      thriftDescriptors.struct('name_change', 10, []),
      thriftDescriptors.struct('avatar_change', 11, []),
      thriftDescriptors.struct('thread_change', 17, []),
      thriftDescriptors.struct('read_receipt', 19, []),
      thriftDescriptors.struct('unknown_receipt_1', 25, []),
      thriftDescriptors.struct('binary', 42, []),
    ]),
    thriftDescriptors.int64('first_seq_id', 2),
    thriftDescriptors.int64('last_seq_id', 3),
    thriftDescriptors.int64('viewer', 4),
    thriftDescriptors.binary('subscribe_ok', 11),
    thriftDescriptors.binary('error', 12),
  ])
  console.log(
    'testMessageSyncPayload:',
    JSON.stringify(
      parsed,
      (k, v) => {
        return typeof v == 'bigint' ? v.toString() : v
      },
      2
    )
  )
  assert.deepEqual(
    parsed,
    {
      items: [
        {
          message: {
            metadata: {
              thread: {
                other_user_id: {
                  int: '-1369353851',
                  num: -1369353851,
                },
                thread_fbid: [-778129798],
              },
              id: 'mid.$cAAAAAFGernp_8_BW415wqsNcfYxb',
              offline_threading_id: {
                int: '-6805126793003895900',
                num: -6805126793003896000,
              },
              sender: {
                int: '-1369353851',
                num: -1369353851,
              },
              timestamp: {
                int: '-1622468660964',
                num: -1622468660964,
              },
              tags: [
                'source:chat:web:light_speed:chat',
                'app_id:2220391788200892',
              ],
            },
            text: 'heyyy',
            attachments: [],
          },
        },
      ],
      first_seq_id: {
        int: '-1369353851',
        num: -1369353851,
      },
    },
    'MessageSyncPayload should be parsed correctly'
  )
}

;(async function run() {
  try {
    await Promise.all([
      testSendMessageResponse(),
      // just a line break
      testMessageSyncPayload(),
    ])
  } catch (e) {
    console.error(e)
    process.exit(1)
  }
})()
