export const uuid = () =>
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = (Math.random() * 16) | 0
    const v = c === 'x' ? r : (r & 0x3) | 0x8
    return v.toString(16)
  })

export const stringifyWithBigInt = (
  payload: Parameters<typeof JSON.stringify>[0],
  indent?: Parameters<typeof JSON.stringify>[2],
) =>
  JSON.stringify(
    payload,
    (_, value) => (typeof value === 'bigint' ? value.toString() : value),
    indent,
  )

// { prop: 'val', ...objPropIfTruthyValue('prop2', val), ...objPropIfExclusiveValue('prop3', 'val3', val) } => { prop: 'val', prop3: 'val3' }
export const objPropIfTruthyValue = (key: string, value: any) => (value ? { [key]: value } : {})
export const objPropIfExclusiveValue = (key: string, value: any, otherValue: any) => (!otherValue ? { [key]: value } : {})
