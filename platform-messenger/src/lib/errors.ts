export interface HTTPResponseError extends Error {
  code: number
  subcode: number
  code_str: string
  error_user_msg?: string
}

export interface OAuthException extends HTTPResponseError {
  error_data: any
  type: 'OAuthException'
}

export interface InvalidAccessToken extends OAuthException {}

export interface TwoFactorRequired extends OAuthException {
  error_data: {
    login_first_factor: string
    auth_token: string
    machine_id?: string
    uid: number
  }
}

export interface InvalidEmail extends OAuthException {}

export interface IncorrectPassword extends OAuthException {}

export interface GraphMethodException extends HTTPResponseError {
  type: 'GraphMethodException'
}

export const isInavlidAccessTokenError = (
  error: any,
): error is InvalidAccessToken => error?.code == 190

export const isInvalidEmailError = (error: any): error is InvalidEmail => error?.code == 400

export const isIncorrectPasswordError = (
  error: any,
): error is IncorrectPassword => error?.code == 401

export const isTwoFactorRequiredError = (
  error: any,
): error is TwoFactorRequired => error?.code == 406

export const isOAuthExceptionError = (error: any): error is OAuthException => error?.type == 'OAuthException'

export const isGraphMethodExceptionError = (
  error: any,
): error is GraphMethodException => error?.type == 'GraphMethodException'

class MQTTBaseError extends Error {
  constructor(message?: string) {
    super(message)
    // @ts-expect-error -- set the name to the class's actual name
    this.name = this.__proto__.constructor.name
  }
}

export class ClientDisconnectedError extends MQTTBaseError {}

export class EmptyPacketError extends MQTTBaseError {}

export class ConnectionFailedError extends MQTTBaseError {}

export class RequestResponseOverMQTTTimeoutError extends MQTTBaseError {}

export class RequestResponseOverMQTTUnknownRequestError extends MQTTBaseError {}
