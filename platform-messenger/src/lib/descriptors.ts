import { t } from '@f0rr0/thrift-compact-protocol'
import { ExtensibleAttachment } from './graphql/responses'

export interface ClientAppSettings {
  request_id: string
  client_request_id: string
  make_user_available_when_in_foreground: boolean
}

export const mqttThriftHeaderSchema = t.struct({
  traceInfo: t.field(1, t.binary()),
})

export const threadKeySchema = t.struct({
  other_user_id: t.field(1, t.int64()).optional(),
  thread_fbid: t.field(2, t.int64()).optional(),
})

export const messageMetadataSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  id: t.field(2, t.binary()),
  offline_threading_id: t.field(3, t.int64()).optional(),
  sender: t.field(4, t.int64()),
  timestamp: t.field(5, t.int64()),
  should_buzz_device: t.field(6, t.boolean()),
  admin_text: t.field(7, t.binary()).optional(),
  tags: t.field(8, t.list(t.binary())),
  thread_read_state_effect: t.field(9, t.int32()).optional(),
  skip_bump_thread: t.field(10, t.boolean()),
  skip_snippet_update: t.field(11, t.boolean()).optional(),
  message_unsendability: t.field(12, t.binary()).optional(),
  snippet: t.field(13, t.binary()).optional(),
  cid: t.field(
    17,
    t.struct({
      conversationFbid: t.field(1, t.int64()).optional(),
      canonicalParticipantFbids: t.field(2, t.set(t.int64())),
    }),
  ),
  folder_id: t
    .field(
      1002,
      t.struct({
        systemFolderId: t.field(1, t.int32()),
        userCreatedId: t.field(2, t.int64()),
      }),
    )
    .optional(),
})

export const imageInfoSchema = t.struct({
  width: t.field(1, t.int32()),
  height: t.field(2, t.int32()),
  imageURIMap: t.field(3, t.map(t.int32(), t.binary())).optional(),
  imageSource: t.field(4, t.int32()).optional(), // ImageSource
  rawImageURI: t.field(5, t.binary()).optional(),
  rawImageURIFormat: t.field(6, t.binary()).optional(),
  animatedImageURIMap: t.field(7, t.map(t.int32(), t.binary())).optional(),
  imageURIMapFormat: t.field(8, t.binary()).optional(),
  animatedImageURIMapFormat: t.field(9, t.binary()).optional(),
  renderAsSticker: t.field(10, t.boolean()).optional(),
  miniPreview: t.field(11, t.binary()).optional(),
  blurredImageURI: t.field(12, t.binary()).optional(),
})

export const videoInfoSchema = t.struct({
  width: t.field(1, t.int32()),
  height: t.field(2, t.int32()),
  durationMs: t.field(3, t.int32()),
  thumbnailUri: t.field(4, t.binary()),
  videoUri: t.field(5, t.binary()),
  source: t.field(6, t.int32()), // VideoSource
  rotation: t.field(7, t.int32()),
  loopCount: t.field(8, t.int32()),
})

export const audioInfoSchema = t.struct({
  isVoicemail: t.field(1, t.boolean()),
  callId: t.field(2, t.binary()),
  url: t.field(3, t.binary()),
  durationMs: t.field(4, t.int32()),
  samplingFrequencyHz: t.field(5, t.int32()),
  // waveformData: t.field(6, t.list()), list of float
})

export const attachmentSchema = t.struct({
  id: t.field(1, t.binary()),
  mimeType: t.field(2, t.binary()).optional(),
  filename: t.field(3, t.binary()).optional(),
  fbid: t.field(4, t.int64()).optional(),
  fileSize: t.field(5, t.int64()).optional(),
  attributionInfo: t
    .field(
      6,
      t.struct({
        attributionAppId: t.field(1, t.int64()),
        attributionMetadata: t.field(2, t.binary()),
        attributionAppName: t.field(3, t.binary()),
        attributionAppIconURI: t.field(4, t.binary()),
        androidPackageName: t.field(5, t.binary()),
        iOSStoreId: t.field(6, t.int64()),
        // otherUserAppScopedFbIds: t.field(7, t.map(t.int64(), t.int64())),
        visibility: t.field(
          8,
          t.struct({
            hideAttribution: t.field(1, t.boolean()),
            hideInstallButton: t.field(2, t.boolean()),
            hideReplyButton: t.field(3, t.boolean()),
            disableBroadcasting: t.field(4, t.boolean()),
            hideAppIcon: t.field(5, t.boolean()),
          }),
        ),
        replyActionType: t.field(9, t.int32()),
        customReplyAction: t.field(10, t.binary()),
        attributionType: t.field(11, t.int64()),
      }),
    )
    .optional(),
  xmaGraphQL: t.field(7, t.binary(t.TBinaryType.STRING)).optional(),
  blobGraphQL: t.field(8, t.binary()).optional(),
  imageMetadata: t.field(10, imageInfoSchema).optional(),
  videoMetadata: t.field(11, videoInfoSchema).optional(),
  audioMetadata: t.field(12, audioInfoSchema).optional(),
  data: t.field(13, t.map(t.binary(), t.binary())).optional(),
  nodeMediaFbid: t.field(14, t.int64()).optional(),
  clientAttachmentType: t.field(16, t.int32()).optional(), // AttachmentType
  useRefCounting: t.field(1007, t.boolean()),
})

export const parseExtensibleAttachment = (
  attachment: t.TypeOf<typeof attachmentSchema>,
) => {
  const data = JSON.parse(attachment.xmaGraphQL)
  const rawMediaKey = `extensible_message_attachment:${attachment.id}`
  const expectedKey = Buffer.from(
    Buffer.from(Buffer.from(rawMediaKey).toString('utf-8')).toString('base64'),
    'utf-8',
  )
    .toString()
    .replace(/=+$/, '')
  return (data[expectedKey] || Object.values(data)[0]) as ExtensibleAttachment
}

export const reactionSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  message_id: t.field(2, t.binary()),
  action: t.field(3, t.int32()), // ReactionAction
  reaction_sender_id: t.field(4, t.int64()),
  reaction: t.field(5, t.binary()).optional(),
  message_sender_id: t.field(6, t.int64()),
  offline_threading_id: t.field(7, t.binary()).optional(),
  reaction_timestamp: t.field(8, t.int64()).optional(),
  reaction_style: t.field(9, t.int64()).optional(),
})

export const messageSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  text: t.field(2, t.binary()).optional(),
  sticker: t.field(4, t.int64()).optional(),
  attachments: t.field(5, t.list(attachmentSchema)).optional(),
  extra_metadata: t.field(7, t.map(t.binary(), t.binary(t.TBinaryType.BINARY))),
})

export const messageReplySchema = t.struct({
  replied_to_message: t.field(1, messageSchema).optional(),
  message: t.field(2, messageSchema),
  status: t.field(3, t.int32()).optional(), // MessageReplyStatus
})

export const unsendMessageSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  message_id: t.field(2, t.binary()),
  deletion_timestamp: t.field(3, t.int64()),
  sender_id: t.field(4, t.int64()),
  message_timestamp: t.field(5, t.int64()), // 0
  admin_text: t.field(6, t.binary()),
})

export const extendedAddMemberParticipantSchema = t.struct({
  addee_user_id: t.field(1, t.int64()),
  adder_user_id: t.field(2, t.int64()),
  source: t.field(3, t.int32()), // AddMemberSource
  timestamp: t.field(4, t.int64()),
})

export const extendedAddMemberSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  users: t.field(2, t.list(extendedAddMemberParticipantSchema)),
})

export const readReceiptSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  user_id: t.field(2, t.int64()),
  read_at: t.field(3, t.int64()),
  read_to: t.field(4, t.int64()),
})

export const markReadOwnSchema = t.struct({
  threads: t.field(1, t.list(threadKeySchema)),
  // folders: t.field(2, t.list()),
  read_to: t.field(3, t.int64()),
  read_at: t.field(4, t.int64()),
})

export const nameChangeSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  new_name: t.field(2, t.binary()),
})

export const avatarChangeSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  new_avatar: t.field(2, attachmentSchema),
})

export const adminMessageSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  action: t.field(2, t.binary()), // TODO: Use t.enum
  action_data: t.field(3, t.map(t.binary(), t.binary())),
  // ttl: t.field(4, t.int64())
})

export const participantInfoSchema = t.struct({
  id: t.field(1, t.int64()),
  first_name: t.field(2, t.binary()),
  full_name: t.field(3, t.binary()),
  is_messenger_user: t.field(4, t.boolean()),
  // profile_pic_uri_map: t.field(5, t.map(t.int32(), t.binary())),
})

export const addMemberSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  users: t.field(2, t.list(participantInfoSchema)),
})

export const removeMemberSchema = t.struct({
  metadata: t.field(1, messageMetadataSchema),
  user_id: t.field(2, t.int64()),
})

export const deliveryReceiptSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  actorFbId: t.field(2, t.int64()),
  deviceId: t.field(3, t.int64()).optional(),
  appId: t.field(4, t.int64()).optional(),
  timestampMs: t.field(5, t.int64()).optional(),
  messageIds: t.field(6, t.list(t.binary())),
  deliveredWatermarkTimestampMs: t.field(7, t.int64()),
  source: t.field(8, t.binary()),
})

const locationUpdateSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  live_locations: t.field(
    2,
    t.list(
      t.struct({
        id: t.field(1, t.int64()),
        sender_id: t.field(2, t.int64()),
        data: t
          .field(
            3,
            t.struct({
              latitide: t.field(1, t.int64()), // 10 digits without decimal
              longitude: t.field(2, t.int64()), // 10 digits without decimal
              timestamp_milliseconds: t.field(3, t.int64()), // miliseconds
              accuracy_millimeters: t.field(4, t.int64()).optional(),
              speed_millimeters_per_second: t.field(5, t.int64()).optional(),
              altitude_millimeters: t.field(6, t.int64()).optional(),
              altitude_accuracy_millimeters: t.field(7, t.int64()).optional(),
              bearing_degrees: t.field(8, t.int64()).optional(),
            }),
          )
          .optional(),
        expiration_time: t.field(4, t.int64()),
        can_stop_sending_location: t.field(5, t.boolean()),
        should_show_eta: t.field(6, t.boolean()),
        offline_threading_id: t.field(7, t.binary()).optional(),
        message_id: t.field(8, t.binary()),
        location_title: t.field(9, t.binary()).optional(),
        is_active: t.field(10, t.boolean()),
        stop_reason: t.field(11, t.int32()).optional(),
        destination: t
          .field(
            12,
            t.struct({
              latitide: t.field(1, t.int64()), // 10 digits without decimal
              longitude: t.field(2, t.int64()), // 10 digits without decimal
              label: t.field(3, t.binary()).optional(),
            }),
          )
          .optional(),
        device_id: t.field(13, t.binary()).optional(),
        full_address: t.field(14, t.binary()).optional(),
        sender_name: t.field(15, t.binary()).optional(),
      }),
    ),
  ),
})

export const muteSettingsSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  mute_end_timestamp: t.field(2, t.int64()),
})

export const messageSyncInnerEventSchema = t.struct({
  location_update: t.field(8, locationUpdateSchema).optional(),
  reaction: t.field(10, reactionSchema).optional(),
  extended_add_member: t.field(52, extendedAddMemberSchema).optional(),
  message_reply: t.field(55, messageReplySchema).optional(),
  unsend_message: t.field(67, unsendMessageSchema).optional(),
  mute_settings: t.field(86, muteSettingsSchema).optional(),
})

export const messageSyncInnerEventPayloadSchema = t.struct({
  items: t.field(1, t.binary(t.TBinaryType.BINARY)),
})

export const messageSyncInnerEventPayloadUnwrappedSchema = t.struct({
  items: t.field(1, t.list(messageSyncInnerEventSchema)),
})

export const noOpSchema = t.struct({
  numNoOps: t.field(1, t.int32()).optional(),
})

export const forcedFetchSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  id: t.field(2, t.binary()).optional(),
  is_lazy: t.field(3, t.boolean()),
})

export const groupThreadSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  participants: t.field(2, t.list(participantInfoSchema)),
})

export const markUnreadOwnSchema = t.struct({
  thread: t.field(1, t.list(threadKeySchema)),
  // folders: t.field(2, t.list(t.int32())).optional(),
  timestamp: t.field(3, t.int64()),
})

export const messageDeleteSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  message_ids: t.field(2, t.list(t.binary())),
  maybe_actor_fbid: t.field(5, t.int64()),
})

export const threadDeleteSchema = t.struct({
  threads: t.field(1, t.list(threadKeySchema)),
  maybe_actor_fbid: t.field(2, t.int64()),
})

export const threadFolderSchema = t.struct({
  thread: t.field(1, threadKeySchema),
  folder: t.field(2, t.int32()),
  maybe_actor_fbid: t.field(5, t.int64()),
})

export const muteSettingsSchemaWithActorFbId = muteSettingsSchema.merge(
  t.struct({
    thread: t.field(1, threadKeySchema),
    mute_end_timestamp: t.field(2, t.int64()),
    actor_fbid: t.field(3, t.int64()),
  }),
)

export const sentMessageSchema = t.struct({
  messageMetadata: t.field(1, messageMetadataSchema),
  attachments: t.field(2, t.list(attachmentSchema)),
  ttl: t.field(3, t.int32()).optional(),
  data: t.field(4, t.map(t.binary(), t.binary())).optional(),
  irisSeqId: t.field(1000, t.int64()).optional(),
  requestContext: t.field(1012, t.map(t.binary(), t.binary())).optional(),
  randomNonce: t.field(1013, t.int32()).optional(),
  participants: t
    .field(
      1014,
      t.list(
        t.struct({
          id: t.field(1, t.int64()),
        }),
      ),
    )
    .optional(),
  irisTags: t.field(1015, t.list(t.binary())).optional(),
  metaTags: t.field(1016, t.list(t.binary())).optional(),
  tqSeqId: t.field(1017, t.int64()).optional(),
})

export const messageSyncEventSchema = t.struct({
  no_op: t.field(1, noOpSchema).optional(),
  message: t.field(2, messageSchema).optional(),
  group_thread: t.field(3, groupThreadSchema).optional(),
  mark_read_own: t.field(4, markReadOwnSchema).optional(),
  mark_unread_own: t.field(5, markUnreadOwnSchema).optional(),
  message_delete: t.field(6, messageDeleteSchema).optional(),
  thread_delete: t.field(7, threadDeleteSchema).optional(),
  add_member: t.field(8, addMemberSchema).optional(),
  remove_member: t.field(9, removeMemberSchema).optional(),
  name_change: t.field(10, nameChangeSchema).optional(),
  avatar_change: t.field(11, avatarChangeSchema).optional(),
  mute_settings: t.field(12, muteSettingsSchemaWithActorFbId).optional(),
  thread_folder: t.field(14, threadFolderSchema).optional(),
  admin_message: t.field(17, adminMessageSchema).optional(),
  forced_fetch: t.field(18, forcedFetchSchema).optional(),
  read_receipt: t.field(19, readReceiptSchema).optional(),
  sent_message: t.field(22, sentMessageSchema).optional(),
  delivery_receipt: t.field(25, deliveryReceiptSchema).optional(),
  inner_items: t.field(42, messageSyncInnerEventPayloadSchema).optional(),
})

export const messageSyncEventUnwrappedInnerItemsSchema = t.struct({
  inner_items: t
    .field(42, messageSyncInnerEventPayloadUnwrappedSchema)
    .optional(),
})

export const messageSyncEventUnwrappedSchema = messageSyncEventSchema.merge(
  messageSyncEventUnwrappedInnerItemsSchema,
)

// class MessageSyncError(ExtensibleEnum):
//     QUEUE_OVERFLOW = "ERROR_QUEUE_OVERFLOW"
//     QUEUE_UNDERFLOW = "ERROR_QUEUE_UNDERFLOW"

export const messageSyncPayloadSchema = t.struct({
  // TODO - messageSyncEventSchema is wrong somewhere and throwing the parsing off
  items: t.field(1, t.list(messageSyncEventSchema)).optional(), // deltas
  firstDeltaSeqId: t.field(2, t.int64()).optional(),
  lastIssuedSeqId: t.field(3, t.int64()).optional(),
  // queueEntityId: t.field(4, t.int64()).optional(),
  failedSend: t
    .field(
      10,
      t.struct({
        // errorCode: t.field(4, t.int32()),
        // errorMessage: t.field(2, t.binary()),
        // isRetryable: t.field(3, t.boolean()),
        // offlineThreadingId: t.field(1, t.int64()),
      }),
    )
    .optional(),
  syncToken: t.field(11, t.binary()).optional(),
  errorCode: t.field(12, t.binary()).optional(), // TODO: use t.enum
})

export const messageSyncDeltas = t.struct({
  items: t.field(1, t.list(messageSyncEventSchema)).optional(),
})

export const messageSyncPayloadItemsUnwrappedSchema = t.struct({
  items: t.field(1, t.list(messageSyncEventUnwrappedSchema)).optional(),
})

export const messageSyncPayloadUnwrappedSchema = (messageSyncPayloadSchema).merge(
  messageSyncPayloadItemsUnwrappedSchema,
)

export const regionHintUnwrappedSchema = t.struct({
  code: t.field(1, t.binary()),
})

export const regionHintPayloadSchema = t.struct({
  region_hint: t.field(2, t.binary(t.TBinaryType.BINARY)),
})

export const groupCreationMutationRequestSchema = t.struct({
  creator_id: t.field(1, t.int64()),
  participants: t.field(
    2,
    t.list(
      t.struct({
        id: t.field(1, t.int64()),
      }),
    ),
  ),
  fb_group_id: t.field(3, t.binary()).optional(),
  co_creator_ids: t.field(4, t.list(t.int64())),
  offline_threading_id: t.field(5, t.int64()),
  name: t.field(6, t.binary()),
  description: t.field(7, t.binary()).optional(),
  approval_mode: t.field(8, t.int32()).optional(),
  joinable_mode: t.field(9, t.int32()).optional(),
  discoverable_mode: t.field(10, t.int32()).optional(),
  video_room_mode: t.field(11, t.int32()).optional(),
  emoji: t.field(12, t.binary()).optional(),
  background_color: t.field(13, t.binary()).optional(),
  theme_color: t.field(14, t.binary()).optional(),
  outgoing_bubble_color: t.field(15, t.binary()).optional(),
  incoming_bubble_color: t.field(16, t.binary()).optional(),
  entry_point: t.field(17, t.binary()),
  should_fetch_thread_info: t.field(18, t.boolean()),
  create_group_mutation_params: t.field(
    19,
    t.struct({
      managingNeosCount: t.field(1, t.int64()).optional(),
      eventCount: t.field(2, t.int64()).optional(),
      fetchUsersSeparately: t.field(3, t.boolean()).optional(),
      fullScreenHeight: t.field(4, t.int64()),
      fullScreenWidth: t.field(5, t.int64()),
      hashKey: t.field(6, t.int64()).optional(),
      includeFullUserInfo: t.field(7, t.boolean()),
      includeMessageInfo: t.field(8, t.boolean()),
      itemCount: t.field(9, t.int64()).optional(),
      largePreviewWidth: t.field(10, t.int64()),
      largePreviewHeight: t.field(11, t.int64()),
      mediumPreviewWidth: t.field(12, t.int64()),
      mediumPreviewHeight: t.field(13, t.int64()),
      msgCount: t.field(14, t.int64()),
      profilePicLargeSize: t.field(15, t.int64()),
      profilePicMediumSize: t.field(16, t.int64()),
      profilePicSmallSize: t.field(17, t.int64()),
      smallPreviewWidth: t.field(18, t.int64()),
      smallPreviewHeight: t.field(19, t.int64()),
      includeCustomerData: t.field(20, t.boolean()),
      customerTagCount: t.field(21, t.int64()).optional(),
      includeBookingRequests: t.field(22, t.boolean()),
      pollVotersCount: t.field(23, t.int64()).optional(),
      verificationType: t.field(24, t.binary()).optional(),
    }),
  ),
  use_existing_group: t.field(20, t.boolean()),
  attempt_id: t.field(21, t.int64()),
  opt_in_group_sync: t.field(22, t.boolean()).optional(),
  extra: t.field(99, t.map(t.binary(), t.binary())).optional(),
})

export const groupCreationMutationResponseSchema = t.struct({
  threadId: t.field(1, t.int64()),
  blockedParticiants: t.field(2, t.list(t.int64())).optional(),
  threadInfo: t.field(3, t.binary()),
  isRetryable: t.field(4, t.boolean()).optional(),
  errorCode: t.field(5, t.int32()).optional(), // createGroupErrorMap
  errorMessage: t.field(6, t.binary()).optional(),
  extra: t.field(99, t.map(t.binary(), t.binary())).optional(),
})

export const createGroupSchema = t.struct({
  offline_threading_id: t.field(1, t.int64()),
  mutation_request: t.field(2, t.binary(t.TBinaryType.BINARY)),
})

export const createGroupResponseSchema = t.struct({
  offline_threading_id: t.field(1, t.int64()),
  mutation_response: t.field(2, t.binary(t.TBinaryType.BINARY)),
})

const groupParticipant = t.struct({
  fbid: t.field(1, t.int64()),
  phoneNumber: t.field(2, t.binary()),
  name: t.field(3, t.binary()),
})

export const addParticipantsToGroupUnwrappedSchema = t.struct({
  threadId: t.field(1, t.int64()),
  participantsToAdd: t.field(2, t.list(groupParticipant)),
  supportPartialSuccess: t.field(3, t.boolean()),
  extra: t.field(99, t.map(t.binary(), t.binary())).optional(),
})

export const addParticipantsToGroupResponseUnwrappedSchema = t.struct({
  success: t.field(1, t.boolean()),
  addedParticipants: t.field(2, t.list(groupParticipant)).optional(),
  errorCode: t.field(3, t.int32()).optional(),
  errorMessage: t.field(4, t.binary()).optional(),
  extra: t.field(99, t.map(t.binary(), t.binary())).optional(),
})

export const addParticipantsToGroupSchema = t.struct({
  threadId: t.field(1, t.int64()),
  payload: t.field(2, t.binary(t.TBinaryType.BINARY)),
})

export const addParticipantsToGroupResponseSchema = t.struct({
  threadId: t.field(1, t.int64()),
  payload: t.field(2, t.binary(t.TBinaryType.BINARY)),
})

export const sendForegroundStateSchema = t.struct({
  inForegroundApp: t.field(1, t.boolean()).optional(),
  inForegroundDevice: t.field(2, t.boolean()).optional(),
  keepAliveTimeout: t.field(3, t.int32()).optional(), // 60
  subscribeTopics: t.field(4, t.list(t.int32())).optional(),
  subscribeGenericTopics: t.field(5, t.list(t.int32())).optional(),
  unsubscribeTopics: t.field(6, t.list(t.int32())).optional(),
  unsubscribeGenericTopics: t.field(7, t.list(t.int32())).optional(),
  requestId: t.field(8, t.int64()).optional(),
  clientRequestId: t.field(9, t.binary()).optional(),
})

export const sendThreadPresenceSchema = t.struct({
  recipient_id: t.field(1, t.int64()),
  sender_id: t.field(2, t.int64()), // always 0
  state: t.field(3, t.int32()), // ThreadPresenceState | 8
})

export const presenceUpdateSchema = t.struct({
  uid: t.field(1, t.int64()),
  state: t.field(2, t.int32()),
  lastActiveTimeSec: t.field(3, t.int64()),
  detailedClientPresence: t.field(4, t.int16()).optional(),
  voipCapabilities: t.field(5, t.int64()).optional(),
  allCapabilities: t.field(6, t.int64()).optional(),
  alohaProxyUserId: t.field(7, t.int64()).optional(),
})

export const presenceUpdateBatchSchema = t.struct({
  isIncrementalUpdate: t.field(1, t.boolean()),
  updates: t.field(2, t.list(presenceUpdateSchema)),
  requestID: t.field(4, t.binary()).optional(),
})

export const typingAtrtibutionSchema = t.struct({
  inThreadAppId: t.field(1, t.int64()),
  pageId: t.field(2, t.int64()),
  extensionType: t.field(3, t.binary()),
  genericAttributionType: t.field(3, t.binary()),
})

export const sendTypingSchema = t.struct({
  recipient: t.field(1, t.int64()),
  sender: t.field(2, t.int64()),
  state: t.field(3, t.int32()), // TypingState
  attribution: t.field(4, typingAtrtibutionSchema).optional(),
  threadKey: t.field(5, threadKeySchema),
})

export const typingNotificationSchema = t.struct({
  sender: t.field(1, t.int64()),
  state: t.field(2, t.int32()), // TypingState
  attribution: t.field(3, typingAtrtibutionSchema).optional(),
  threadKey: t.field(4, threadKeySchema).optional(),
  persona: t
    .field(
      5,
      t.struct({
        id: t.field(1, t.int64()),
        name: t.field(2, t.binary()),
        profilePictureURL: t.field(3, t.binary()),
      }),
    )
    .optional(),
})

export const sendAdditionalContactsSchema = t.struct({
  additionalContacts: t.field(1, t.list(t.int64())),
  requestId: t.field(2, t.binary(t.TBinaryType.STRING)),
})

export const markThreadSchema = t.struct({
  mark: t.field(1, t.binary()), // MarkThreadState
  state: t.field(2, t.boolean()), // flag read/unread, archived/unarchived
  threadId: t.field(3, t.binary()).optional(), // not present in 1-1
  actionId: t.field(4, t.int64()).optional(),
  syncSeqId: t.field(5, t.int64()).optional(),
  threadFbId: t.field(6, t.int64()).optional(),
  otherUserFbId: t.field(7, t.int64()), // 1-1
  actorFbId: t.field(8, t.int64()).optional(),
  watermarkTimestamp: t.field(9, t.int64()).optional(),
  titanOriginatedThreadId: t.field(10, t.binary()).optional(),
  shouldSendReadReceipt: t.field(11, t.boolean()).optional(),
  adPageMessageType: t.field(12, t.binary()).optional(),
  attemptId: t.field(13, t.int64()),
})

export const markThreadResponseSchema = t.struct({})

export const deliveryReceiptBatchSchema = t.struct({
  deliveryReceipts: t.field(
    1,
    t.list(
      t.struct({
        messageSenderFbid: t.field(1, t.int64()),
        watermarkTimestamp: t.field(2, t.int64()),
        threadFbid: t.field(3, t.int64()),
        messageIds: t.field(4, t.list(t.binary())),
        messageRecipientFbid: t.field(5, t.int64()),
        isGroupThread: t.field(6, t.boolean()),
        adPageMessageTypes: t.field(7, t.list(t.binary())),
        traceId: t.field(8, t.binary()).optional(),
        edgeId: t.field(9, t.binary()).optional(),
        requestId: t.field(10, t.binary()).optional(),
      }),
    ),
  ),
  batchId: t.field(2, t.int64()), // random
  source: t.field(3, t.binary()), // FETCH_THREAD_LIST
})

export const deliveryReceiptBatchResponseSchema = t.struct({
  batchId: t.field(1, t.int64()),
  isSuccess: t.field(2, t.boolean()),
  isRetryable: t.field(3, t.boolean()).optional(),
  errorMessage: t.field(4, t.binary()).optional(),
})

const coordinatesSchema = t.struct({
  latitude: t.field(1, t.binary(t.TBinaryType.STRING)),
  longitude: t.field(2, t.binary(t.TBinaryType.STRING)),
  accuracy: t.field(3, t.binary(t.TBinaryType.STRING)),
})

export const sendMessageSchema = t.struct({
  to: t.field(1, t.binary()),
  body: t.field(2, t.binary()).optional(),
  offlineThreadingId: t.field(3, t.int64()),
  coordinates: t.field(4, coordinatesSchema).optional(),
  clientTags: t.field(5, t.map(t.binary(), t.binary())),
  objectAttachment: t.field(6, t.binary()).optional(),
  copyMessageId: t.field(7, t.binary()).optional(),
  copyAttachmentId: t.field(8, t.binary()).optional(),
  mediaAttachmentIds: t.field(9, t.list(t.binary())).optional(),
  fbTraceMeta: t.field(10, t.binary()).optional(),
  imageType: t.field(11, t.int32()).optional(), // ImageType
  senderFbid: t.field(12, t.int64()),
  broadcastRecipients: t.field(13, t.map(t.binary(), t.binary())).optional(),
  attributionAppId: t.field(14, t.int64()).optional(),
  iosBundleId: t.field(15, t.binary()).optional(),
  androidKeyHash: t.field(16, t.binary()).optional(),
  locationAttachment: t
    .field(
      17,
      t.struct({
        coordinates: t.field(1, coordinatesSchema),
        isCurrentLocation: t.field(2, t.boolean()),
        placeId: t.field(3, t.int64()),
      }),
    )
    .optional(),
  ttl: t.field(18, t.int32()),
  refCode: t.field(19, t.int32()).optional(),
  genericMetadata: t.field(20, t.map(t.binary(), t.binary())).optional(),
  markReadWatermarkTimestamp: t.field(21, t.int64()),
  attemptId: t.field(22, t.binary()).optional(),
  isDialtone: t.field(23, t.boolean()),
  msgAttemptId: t.field(24, t.int64()), // random
  externalAttachmentUrl: t.field(25, t.binary()).optional(),
  skipAndroidHashCheck: t.field(26, t.boolean()).optional(),
  originalCopyMessageId: t.field(27, t.binary()).optional(),
  repliedToMessageId: t.field(28, t.binary()).optional(),
  logInfo: t.field(29, t.map(t.binary(), t.binary())).optional(),
  isSelfForwarded: t.field(30, t.boolean()).optional(),
  messagePowerUpData: t
    .field(
      31,
      t.struct({
        powerUpStyle: t.field(1, t.int32()), // MessagePowerUpStyle
      }),
    )
    .optional(),
})

export const sendMessageResponseSchema = t.struct({
  offline_threading_id: t.field(1, t.int64()),
  send_succeeded: t.field(2, t.boolean()),
  error_no: t.field(3, t.int32()).optional().optional(),
  error_message: t.field(4, t.binary()).optional().optional(),
  is_retryable: t.field(5, t.boolean()).optional().optional(),
})

export const requestStreamCreatePresenceStreamSchema = t.struct({
  payload: t.field(
    2,
    t.list(
      t.struct({
        unknownInt: t.field(1, t.int64()), // 1
        nested: t.field(
          2,
          t.struct({
            unknownLong: t.field(1, t.int64()), // 2 or 1 also unsure
            unknownInt: t.field(2, t.int32()), // 1,
            options: t.field(3, t.binary()), // json string {"pollingMode":1,"appFamily":2,"publishEncoding":1} 1 = thrift, 2 = json
            moreOptions: t.field(5, t.binary()), // json string {"Accept-Ack":"RSAck","method":"Presence"}
          }),
        ),
      }),
    ),
  ),
})

export const requestStreamAmendPresenceStreamRequestSchema = t.struct({
  payload: t.field(
    2,
    t.list(
      t.struct({
        unknownInt: t.field(1, t.int64()), // 2
        nested: t.field(
          3,
          t.struct({
            unknownLong: t.field(1, t.int64()), // 2
            data: t.field(3, t.binary()), // json {"additionalContacts":["100001693400880","102192172054498","100001073779453"],"requestId":"","task":1}
          }),
        ),
      }),
    ),
  ),
})

export const requestStreamPresenceUpdateResponseSchema = t.struct({
  unknownLong: t.field(1, t.int64()), // 3
  payload: t.field(
    2,
    t.list(
      t.struct({
        unknownInt: t.field(1, t.int32()), // 2
        nested: t
          .field(
            3,
            t.struct({
              unknownLong: t.field(1, t.int64()), // 1
              presenceUpdateData: t.field(3, t.binary(t.TBinaryType.BINARY)),
            }),
          )
          .optional(),
        unknownStruct: t.field(
          5,
          t.struct({
            unknownLong: t.field(1, t.int64()), // 1
            unknownInt: t.field(2, t.int32()), // 3
          }),
        ),
      }),
    ),
  ),
})

export const requestStreamPresenceUpdateResponseUnwrappedSchema = t.struct({
  updates: t.field(1, t.list(presenceUpdateSchema)),
  publishType: t.field(2, t.int32()), // PresencePublishType
  publishId: t.field(3, t.binary()),
})
