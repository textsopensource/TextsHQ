export interface LoginResponse {
  uid: number
  secret: string
  access_token: string
  machine_id: string
  session_cookies: Record<string, any>[]
  analytics_claim: string
  user_storage_key: string
  identifier?: string
}
export interface PasswordKeyResponse {
  public_key: string
  key_id: number
  seconds_to_live: number
}

export interface MobileConfigResponse {
  configs: Record<string, MobileConfigItem>
  query_hash: string
  one_query_hash: string
  ts: number
  bins: any
  ep_hash: string
}

/*
{
  fields: [
    { k: 1, i64: 131 },
    {
      k: 2,
      str: '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyD5wxR1BYF2nGjpRTUwmlx136/i9bLoi4g62R6HiYVG+jcgAoyFV0xcfYwgUudIox47saHCfHhxV0kTuO/K/tYvW1Rl4ZBerGLuJQnHrkvUymL41BUeH50Eq4wLscJXB8emN45gT6J9YYfzaECT78Th7lFJ2FlsbRU4Q8yYu+OPXRlqaTLs4cXbUayiH67t7yJG0Bu5D4DL5dl72pIICBhyrXXG3douOSzsFQ98m4WpktTXh+nMfGIrQp7QI8lfkWaPz9pt8+6r4wJI74Ul7/xKTGAjqBDOG17GDw4saq4z0FVIQgpj2b/WnyedtGbzNLIGPO2l89QoTvwQvCkpd7wIDAQAB
        '-----END PUBLIC KEY-----\n'
    },
    { k: 3, bln: 1 },
    { k: 5, bln: 1 },
    { k: 6, bln: 1 },
    { k: 7, bln: 1 },
    { k: 8, bln: 1 },
    { k: 9, bln: 1 }
  ],
  hash: '+RyUVdXnIH5hegK54yfUKLXKEUk='
}
*/
export interface MobileConfigItem {
  fields: MobileConfigField[]
  hash: string
}

export interface MobileConfigField {
  k: number
  str: string
  i64: number
  bln: number
}
