import { MessageUnsendability, ThreadCannotReplyReason, ThreadFolder, ThreadType } from '../common'

export interface ParticipantID {
  id: string
}

export interface ReadReceipt {
  action_timestamp_precise: string
  timestamp_precise: string
  actor: ParticipantID
}

export interface DeliveryReceipt {
  timestamp_precise: string
  actor: ParticipantID
}

export interface ReadReceiptList {
  nodes: ReadReceipt[]
}

export interface DeliveryReceiptList {
  nodes: DeliveryReceipt[]
}

export interface Picture {
  uri: string
  width?: number
  height?: number
}

export enum StructuredNamePart {
  FIRST = 'first',
  MIDDLE = 'middle',
  LAST = 'last',
}

export interface StructuredNameChunk {
  length: number
  offset: number
  part: StructuredNamePart
}

export interface StructuredName {
  parts: StructuredNameChunk[]
  phonetic_name?: string
  text: string
}

export interface FriendCount {
  count: number
}

export interface MinimalParticipant extends ParticipantID {
  name?: string
}

export enum FriendshipStatus {
  ARE_FRIENDS = 'ARE_FRIENDS',
  CAN_REQUEST = 'CAN_REQUEST',
  CANNOT_REQUEST = 'CANNOT_REQUEST',
  INCOMING_REQUEST = 'INCOMING_REQUEST',
  OUTGOING_REQUEST = 'OUTGOING_REQUEST',
}

export enum ReachabilityStatus {
  REACHABLE = 'REACHABLE',
  UNREACHABLE_USER_TYPE = 'UNREACHABLE_USER_TYPE',
}

export enum ParticipantType {
  USER = 'User',
  PAGE = 'Page',
}

export interface Participant extends MinimalParticipant {
  username?: string
  structured_name?: StructuredName
  nickname_for_viewer?: string

  profile_pic_small?: Picture
  profile_pic_medium?: Picture
  profile_pic_large?: Picture

  friends?: FriendCount
  friendship_status?: FriendshipStatus
  mutual_friends?: FriendCount
  reachability_status_type?: ReachabilityStatus
  registration_time?: number

  is_aloha_proxy_confirmed: boolean
  is_blocked_by_viewer: boolean
  is_banned_by_page_viewer: boolean
  is_deactivated_allowed_on_messenger: boolean
  is_managing_parent_approved_user: boolean
  is_memorialized: boolean
  is_message_blocked_by_viewer: boolean
  is_message_ignored_by_viewer: boolean
  is_pseudo_blocked_by_viewer: boolean
  is_messenger_user: boolean
  is_partial?: boolean
  is_verified: boolean
  is_viewer_friend: boolean
  can_viewer_message: boolean
}

export interface ParticipantNode {
  id: string
  messaging_actor: Participant
}

export interface ParticipantList {
  nodes: ParticipantNode[]
}

export interface MessageSender {
  id: string
  messaging_actor: MinimalParticipant
}

export interface GetUsersResponse {
  messaging_actors: Participant[]
}

export interface MessageRange {
  length: number
  offset: number
  entity?: ParticipantID
}

export enum MessagePowerUpType {
  NONE = 'NONE',
  LOVE = 'LOVE',
  GIFT_WRAP = 'GIFT_WRAP',
  CELEBRATION = 'CELEBRATION',
  FIRE = 'FIRE',
}

export interface MessagePowerUp {
  style: MessagePowerUpType
}

export interface MessageText {
  text: string
  ranges: MessageRange[]
}

export interface Reaction {
  reaction: string
  reaction_timestamp: string
  user: ParticipantID
}

export interface Dimensions {
  x: number
  y: number
}

export enum AttachmentType {
  IMAGE = 'MessageImage',
  ANIMATED_IMAGE = 'MessageAnimatedImage',
  FILE = 'MessageFile',
  AUDIO = 'MessageAudio',
  VIDEO = 'MessageVideo',
  LOCATION = 'MessageLocation',
  LIVE_LOCATION = 'MessageLiveLocation',
  EXTERNAL_URL = 'ExternalUrl',
  STORY = 'Story',
}

export enum ImageType {
  FILE_ATTACHMENT = 'FILE_ATTACHMENT',
  MESSENGER_CAM = 'MESSENGER_CAM',
  TRANSPARENT = 'TRANSPARENT',
}

export enum VideoType {
  FILE_ATTACHMENT = 'FILE_ATTACHMENT',
  RECORDED_VIDEO = 'RECORDED_VIDEO',
  SPEAKING_STICKER = 'SPEAKING_STICKER',
  RECORDED_STICKER = 'RECORDED_STICKER',
  VIDEO_MAIL = 'VIDEO_MAIL',
  IG_SELFIE_STICKER = 'IG_SELFIE_STICKER',
}

export interface Attachment {
  id: string
  attachment_fbid: string
  filename: string
  mimetype: string
  filesize?: number
  render_as_sticker: boolean

  image_type?: ImageType
  original_dimensions?: Dimensions
  image_blurred_preview?: Picture
  image_full_screen?: Picture
  image_large_preview?: Picture
  image_medium_preview?: Picture
  image_small_preview?: Picture

  //     # For animated images
  animated_image_render_as_sticker: boolean
  animated_image_original_dimensions?: Dimensions
  animated_image_full_screen?: Picture
  animated_image_large_preview?: Picture
  animated_image_medium_preview?: Picture
  animated_image_small_preview?: Picture
  animated_static_image_full_screen?: Picture
  animated_static_image_large_preview?: Picture
  animated_static_image_medium_preview?: Picture
  animated_static_image_small_preview?: Picture

  //     # For audio files
  is_voicemail: boolean
  playable_url?: string

  //     # For audio and video files
  playable_duration_in_ms?: number

  //     # For video files
  video_type?: VideoType
  video_filesize?: number
  attachment_video_url?: string
}

export interface MinimalSticker {
  //     # 369239263222822 = "like"
  id: string
  url?: string
}

export interface StickerPackMeta {
  id: string
  is_comments_capable: boolean
  is_composer_capable: boolean
  is_messenger_capable: boolean
  is_messenger_kids_capable: boolean
  is_montage_capable: boolean
  is_posts_capable: boolean
  is_sms_capable: boolean
}

export enum StickerType {
  REGULAR = 'REGULAR',
  AVATAR = 'AVATAR',
  CUSTOM = 'CUSTOM',
}

export interface Sticker extends MinimalSticker {
  pack: StickerPackMeta
  animated_image: Picture
  preview_image: Picture
  thread_image: Picture
  sticker_type: StickerType
  label?: string
}

export interface ExtensibleText {
  text: string
}

export interface Coordinates {
  latitude: number
  longitude: number
}

export interface StoryTarget {
  typename: AttachmentType
  id?: string
  url?: string
  coordinates?: Coordinates
}

export interface StoryMediaAttachment {
  id?: string
  width?: number
  height?: number
  owner?: MinimalParticipant
  title?: ExtensibleText
  image?: Picture
  playable_url?: string
  is_looping: boolean
  is_playable: boolean
}

export interface StoryAttachment {
  title: string
  url: string
  style_list: string[]
  title_with_entities?: ExtensibleText
  description?: ExtensibleText
  source?: ExtensibleText
  subtitle?: string
  target?: StoryTarget
  deduplication_key?: string
  media?: StoryMediaAttachment
}

export interface ExtensibleAttachment {
  id: string
  is_forwardable: boolean
  story_attachment?: StoryAttachment
}

export interface MinimalMessage {
  id?: string
  message_id?: string
  message?: MessageText
  message_sender: MessageSender
  sticker?: MinimalSticker
  blob_attachments: Attachment[]
  extensible_attachment: ExtensibleAttachment
}

export enum ReplyStatus {
  VALID = 'VALID',
  DELETED = 'DELETED',
}

export interface Reply {
  message: MinimalMessage
  status: ReplyStatus
}

export interface Message extends MinimalMessage {
  snippet: string
  timestamp_precise: string
  unsent_timestamp_precise?: string
  offline_threading_id?: string
  tags_list: string[]
  message_reactions: Reaction[]
  replied_to_message?: Reply
  message_unsendability_status: MessageUnsendability
  is_sponsored: boolean
  is_user_generated: boolean
  unread: boolean
  ttl?: number
}

export interface PageInfo {
  has_next_page: boolean
  has_previous_page: boolean

  end_cursor?: string
  start_cursor?: string
}

export interface MessageList {
  nodes: Message[]
  page_info: PageInfo
}

export interface ThreadKey {
  other_user_id?: string
  thread_fbid?: string
}

export interface ThreadParticipantCustomization {
  participant_id: string
  nickname: string
}

export interface ThreadCustomizationInfo {
  custom_like_emoji?: string
  participant_customizations: ThreadParticipantCustomization[]
}
export interface Thread {
  id: string
  folder: ThreadFolder
  has_viewer_archived: boolean // user archived threada
  name?: string
  thread_key: ThreadKey
  thread_category_as_string: ThreadType
  image?: Picture

  messages_count: number
  unread_count: number
  unsend_limit: number
  mute_until?: number
  privacy_mode: number
  thread_pin_timestamp: number
  thread_queue_enabled: boolean
  thread_unsendability_status: MessageUnsendability
  updated_time_precise: string

  last_message: Pick<MessageList, 'nodes'>
  messages: MessageList
  read_receipts: ReadReceiptList
  delivery_receipts: DeliveryReceiptList
  all_participants: ParticipantList
  customization_info: ThreadCustomizationInfo

  thread_admins: ParticipantID[]
  is_admin_supported: boolean
  is_business_page_active: boolean
  is_disappearing_mode: boolean
  is_fuss_red_page: boolean
  is_group_thread: boolean
  group_approval_mode?: number // 0 = invite only?
  is_ignored_by_viewer: boolean // user has left group
  is_pinned: boolean
  is_viewer_allowed_to_add_members: boolean
  is_viewer_subscribed: boolean // user left or removed from group
  can_viewer_reply: boolean
  cannot_reply_reason?: ThreadCannotReplyReason
  can_participants_claim_admin: boolean
}

export interface ThreadListResponse {
  count: number
  unread_count: number
  unseen_count: number
  mute_until: number
  nodes: Thread[]
  page_info: PageInfo
  sync_sequence_id: string
}

export interface ThreadQueryResponse {
  message_threads: Thread[]
}

export interface StickerPreviewResponse {
  nodes: Sticker[]
}

export interface MessageUnsendResponse {
  did_succeed: boolean
  error_code: string
  error_message: string
}

export interface ImageFragment {
  id: string
  animated_gif?: Picture
  image?: Picture
}

export interface SubsequentMediaNode {
  id: string
  legacy_attachment_id: string
  creation_time: number
  creator: MinimalParticipant
  adjusted_size?: Picture
  original_dimensions?: Dimensions
}

export interface SubsequentMediaResponse {
  nodes: SubsequentMediaNode[]
  page_info: PageInfo
}

export interface FileAttachmentWithURL {
  attachment_fbid: string
  id: string
  url: string
}

export interface FileAttachmentURLResponse {
  id: string
  blob_attachments: FileAttachmentWithURL[]
}

export interface OwnInfo {
  id: string
  birthday?: string
  gender?: string
  locale?: string
  email?: string
  name: string
  first_name?: string
  middle_name?: string
  last_name?: string
  username?: string
  link?: string
  is_employee: boolean
  verified: boolean
  published_timeline: boolean
  timezone: number
  updated_time?: string
}

export interface MessageSearchResult {
  thread_id: string
  name?: string
}

export type SearchResultNode = MessageSearchResult | Participant | Object

export interface SearchResult {
  node: SearchResultNode
}

export interface SearchResults {
  edges: SearchResult[]
}

export interface SearchEntitiesResponse {
  cache_id: string
  search_results: SearchResults
}

export interface UpdateGroupAdminsResponse {
  success: boolean
}

export type UpdateGroupParticipantsResponse = boolean

export interface ModifyThreadResponse {}

export interface LocationSharingReverseGeocodeResponse {
  reverse_geocode: {
    reverse_geocode_data: {
      nodes: [
        {
          full_address_single_line: string
        },
      ]
    }
  }
}
