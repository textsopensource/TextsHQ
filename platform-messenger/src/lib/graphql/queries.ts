import clone from 'clone-deep'
import {
  SerializableEntity,
  RequiredAttributes,
  ReservedAsPartial,
  ThreadFolder,
  ReactionAction,
  _serialize_,
  serializeObject,
} from '../common'

export interface GraphQLQuery extends SerializableEntity {
  _kind_: 'GraphQLQuery'
  _caller_class_: string
  _doc_id_?: string
  _sub_kind_?: string
}

export interface GraphQLMutation extends SerializableEntity {
  _kind_: 'GraphQLMutation'
  _caller_class_: string
  _doc_id_?: string
  _sub_kind_?: string
}

export const basicGraphQLQuery: GraphQLQuery = {
  _kind_: 'GraphQLQuery',
  _caller_class_: 'graphservice',
  _serialize_() {
    const {
      _kind_,
      _hash_key_,
      _caller_class_,
      _doc_id_,
      _serialize_,
      ...attributes
    } = this
    return serializeObject(attributes)
  },
}

export const basicGraphQLMutation: GraphQLMutation = {
  _kind_: 'GraphQLMutation',
  _caller_class_: 'graphservice',
  _serialize_() {
    const {
      _kind_,
      _hash_key_,
      _caller_class_,
      _doc_id_,
      _serialize_,
      ...attributes
    } = this
    return serializeObject(attributes)
  },
}

export interface NTContext extends SerializableEntity {
  _hash_key_: 'NTContext'
  styles_id: string
  using_white_navbar: boolean
  pixel_ratio: number
  bloks_version: string
}

export const ntContext: NTContext = {
  _kind_: 'NTContext',
  _hash_key_: 'NTContext',
  styles_id: '2c223dbb808ad6e5a12a84d5206cdced',
  using_white_navbar: true,
  pixel_ratio: 3,
  bloks_version:
    'a3f9c6739b6e161ea3a6d42127d93f59d501287d05b1608be647a818b229bafa',
  _serialize_,
}

export interface ThreadQuery extends GraphQLQuery {
  _doc_id_: '3837215039696289'
  _hash_key_: 'ThreadQuery'
  thread_ids: string[]
  msg_count: number
  blur: number
  nt_context: NTContext
  include_full_user_info: boolean
  include_message_info: boolean
  include_booking_requests: boolean
  full_screen_width: number
  full_screen_height: number
  large_preview_width: number
  large_preview_height: number
  medium_preview_width: number
  medium_preview_height: number
  small_preview_width: number
  small_preview_height: number
  profile_pic_large_size: number
  profile_pic_small_size: number
}

export const threadQuery = (
  q: RequiredAttributes<ThreadQuery, 'thread_ids'>,
): ThreadQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3837215039696289',
  _hash_key_: 'ThreadQuery',
  msg_count: 20,
  blur: 0,
  nt_context: clone(ntContext),
  include_full_user_info: true,
  include_message_info: true,
  include_booking_requests: true,
  full_screen_width: 4096,
  full_screen_height: 4096,
  large_preview_width: 1500,
  large_preview_height: 750,
  medium_preview_width: 962,
  medium_preview_height: 481,
  small_preview_width: 716,
  small_preview_height: 358,
  profile_pic_large_size: 880,
  profile_pic_small_size: 138,
  ...q,
})

export interface ThreadListQuery extends GraphQLQuery {
  _doc_id_: '4008197475905235'
  _hash_key_: 'ThreadListQuery'
  msg_count: number
  thread_count: number
  include_thread_info: boolean
  include_message_info: boolean
  fetch_users_separately: boolean
  filter_to_groups: boolean
  include_booking_requests: boolean
  nt_context: NTContext
  folder_tag?: ThreadFolder[]
  theme_icon_size_small: number
  reaction_static_asset_size_small: number
  reaction_static_asset_size_large: number
  profile_pic_medium_size: number
  profile_pic_large_size: number
  profile_pic_small_size: number
  theme_background_size: number
  theme_icon_size_large: number
}

export const threadListQuery = (
  q: RequiredAttributes<ThreadListQuery> = {},
): ThreadListQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '4008197475905235',
  _hash_key_: 'ThreadListQuery',
  msg_count: 1,
  thread_count: 20,
  include_thread_info: true,
  include_message_info: true,
  fetch_users_separately: false,
  filter_to_groups: false,
  include_booking_requests: true,
  nt_context: clone(ntContext),
  theme_icon_size_small: 66,
  reaction_static_asset_size_small: 39,
  reaction_static_asset_size_large: 83,
  profile_pic_medium_size: 220,
  profile_pic_large_size: 880,
  profile_pic_small_size: 138,
  theme_background_size: 2048,
  theme_icon_size_large: 138,
  ...q,
})

export interface MoreThreadsQuery extends GraphQLQuery {
  _doc_id_: '4256034191073780'
  _hash_key_: 'MoreThreadsQuery'
  after_time_ms: string
  include_full_user_info: boolean
  include_message_info: boolean
  fetch_users_separately: boolean
  nt_context: NTContext
  folder_tag?: ThreadFolder[]
  msg_count: number
  thread_count: number
  filter_to_groups: boolean
  profile_pic_small_size: number
  profile_pic_medium_size: number
  profile_pic_large_size: number
}

export const moreThreadsQuery = (
  q: RequiredAttributes<MoreThreadsQuery, 'after_time_ms'>,
): MoreThreadsQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '4256034191073780',
  _hash_key_: 'MoreThreadsQuery',
  include_full_user_info: true,
  include_message_info: true,
  fetch_users_separately: false,
  nt_context: clone(ntContext),
  msg_count: 1,
  thread_count: 20,
  filter_to_groups: false,
  profile_pic_small_size: 138,
  profile_pic_medium_size: 220,
  profile_pic_large_size: 880,
  ...q,
})

export interface MoreMessagesQuery extends GraphQLQuery {
  _doc_id_: '3447218621980314'
  _hash_key_: 'MoreMessagesQuery'
  before_time_ms: string
  thread_id: string
  msg_count: number
  blur: number
  nt_context: NTContext
  full_screen_width: number
  full_screen_height: number
  large_preview_width: number
  large_preview_height: number
  medium_preview_width: number
  medium_preview_height: number
  small_preview_width: number
  small_preview_height: number
}

export const moreMessagesQuery = (
  q: RequiredAttributes<MoreMessagesQuery, 'thread_id' | 'before_time_ms'>,
): MoreMessagesQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3447218621980314',
  _hash_key_: 'MoreMessagesQuery',
  msg_count: 20,
  blur: 0,
  nt_context: clone(ntContext),
  full_screen_width: 4096,
  full_screen_height: 4096,
  large_preview_width: 1500,
  large_preview_height: 750,
  medium_preview_width: 962,
  medium_preview_height: 481,
  small_preview_width: 716,
  small_preview_height: 358,
  ...q,
})

export interface FetchStickersWithPreviewsQuery extends GraphQLQuery {
  _doc_id_: '3154119451330002'
  _caller_class_: 'com.facebook.messaging.sync.delta.NewMessageHandlerHelper'
  _hash_key_: 'FetchStickersWithPreviewsQuery'
  sticker_ids: string[]
  preview_size: number
  animated_media_type: string
  media_type: string
  scaling_factor: string
  sticker_labels_enabled: boolean
  sticker_state_enabled: boolean
}

export const fetchStickersWithPreviewsQuery = (
  q: RequiredAttributes<FetchStickersWithPreviewsQuery, 'sticker_ids'>,
): FetchStickersWithPreviewsQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3154119451330002',
  _caller_class_: 'com.facebook.messaging.sync.delta.NewMessageHandlerHelper',
  _hash_key_: 'FetchStickersWithPreviewsQuery',
  preview_size: 165,
  animated_media_type: 'image/webp',
  media_type: 'image/webp',
  scaling_factor: '2.75',
  sticker_labels_enabled: false,
  sticker_state_enabled: false,
  ...q,
})

export interface MessageUndoSend extends GraphQLMutation {
  _doc_id_: '1015037405287590'
  _hash_key_: 'MessageUndoSend'
  message_id: string
  client_mutation_id: string
  actor_id: string
}

export const messageUndoSend = (
  q: ReservedAsPartial<MessageUndoSend>,
): MessageUndoSend => ({
  ...clone(basicGraphQLMutation),
  _doc_id_: '1015037405287590',
  _hash_key_: 'MessageUndoSend',
  ...q,
})

export interface MessageReactionMutation extends GraphQLMutation {
  _doc_id_: '1415891828475683'
  _hash_key_: 'MessageReactionMutation'
  message_id: string
  client_mutation_id: string
  actor_id: string
  action: ReactionAction
  reaction?: string
}

export const messageReactionMutation = (
  q: ReservedAsPartial<MessageReactionMutation>,
): MessageReactionMutation => ({
  ...clone(basicGraphQLMutation),
  _doc_id_: '1415891828475683',
  _hash_key_: 'MessageReactionMutation',
  ...q,
})

export interface DownloadImageFragment extends GraphQLQuery {
  _doc_id_: '3063616537053520'
  _hash_key_: 'DownloadImageFragment'
  fbid: string
  img_size: string
}

export const downloadImageFragmnet = (
  q: RequiredAttributes<DownloadImageFragment, 'fbid'>,
): DownloadImageFragment => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3063616537053520',
  _hash_key_: 'DownloadImageFragment',
  img_size: '0',
  ...q,
})

export interface FbIdToCursorQuery extends GraphQLQuery {
  _doc_id_: '2015407048575350'
  _hash_key_: 'FbIdToCursorQuery'
  fbid: string
  thread_id: string
}

export const fbIdToCursorQuery = (
  q: ReservedAsPartial<FbIdToCursorQuery>,
): FbIdToCursorQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '2015407048575350',
  _hash_key_: 'FbIdToCursorQuery',
  ...q,
})

export interface SubsequentMediaQuery extends GraphQLQuery {
  _doc_id_: '2948398158550055'
  _hash_key_: 'SubsequentMediaQuery'
  thread_id: string
  cursor_id: string
  fetch_size: number
  thumbnail_size: number
  height: number
  width: number
}

export const subsequentMediaQuery = (
  q: RequiredAttributes<SubsequentMediaQuery, 'thread_id' | 'cursor_id'>,
): SubsequentMediaQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '2948398158550055',
  _hash_key_: 'SubsequentMediaQuery',
  fetch_size: 99,
  thumbnail_size: 540,
  height: 2088,
  width: 1080,
  ...q,
})

export interface ThreadMessageID extends SerializableEntity {
  _hash_key_: 'ThreadMessageID'
  thread_id: string
  message_id: string
}

export const threadMessageID = (
  q: ReservedAsPartial<ThreadMessageID>,
): ThreadMessageID => ({
  _kind_: 'ThreadMessageID',
  _hash_key_: 'ThreadMessageID',
  _serialize_,
  ...q,
})

export interface FileAttachmentUrlQuery extends GraphQLQuery {
  _doc_id_: '3200288700012393'
  _hash_key_: 'FileAttachmentUrlQuery'
  thread_msg_id: ThreadMessageID
}

export const fileAttachmentUrlQuery = (
  q: RequiredAttributes<FileAttachmentUrlQuery, 'thread_msg_id'>,
): FileAttachmentUrlQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3200288700012393',
  _hash_key_: 'FileAttachmentUrlQuery',
  ...q,
})

export interface UsersQuery extends GraphQLQuery {
  _doc_id_: '4250664144953532'
  _hash_key_: 'UsersQuery'
  profile_pic_large_size: number
  profile_pic_medium_size: number
  profile_pic_small_size: number
  user_fbids: string[]
}

export const usersQuery = (
  q: RequiredAttributes<UsersQuery, 'user_fbids'>,
): UsersQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '4250664144953532',
  _hash_key_: 'UsersQuery',
  profile_pic_large_size: 880,
  profile_pic_medium_size: 220,
  profile_pic_small_size: 138,
  ...q,
})

export interface SearchEntitiesNamedQuery extends GraphQLQuery {
  _doc_id_: '3414226858659179'
  _hash_key_: 'SearchEntitiesNamedQuery'
  search_query: string
  session_id?: string
  results_limit: number
  num_users_query: number
  num_group_threads_query: number
  num_pages_query: number
  unified_config: string
  search_surface: string
  user_types: string[]
  entity_types: string[]
  include_pages: boolean
  include_games: boolean
  profile_pic_large_size: number
  profile_pic_medium_size: number
  profile_pic_small_size: number
}

export const searchEntitiesNamedQuery = (
  q: RequiredAttributes<SearchEntitiesNamedQuery, 'search_query'>,
): SearchEntitiesNamedQuery => ({
  ...clone(basicGraphQLQuery),
  _doc_id_: '3414226858659179',
  _hash_key_: 'SearchEntitiesNamedQuery',
  results_limit: 20,
  num_users_query: 20,
  num_group_threads_query: 20,
  num_pages_query: 6,
  unified_config: 'DOUBLE_SERVER_QUERY_PRIMARY',
  search_surface: 'UNIVERSAL_ALL',
  user_types: ['CONTACT', 'NON_FRIEND_NON_CONTACT'],
  entity_types: [
    'user',
    'group_thread',
    'page',
    'game',
    'matched_message_thread',
  ],
  include_pages: true,
  include_games: true,
  profile_pic_large_size: 880,
  profile_pic_medium_size: 220,
  profile_pic_small_size: 138,
  ...q,
})

export interface AddAdminsToGroupQuery extends GraphQLQuery {
  _hash_key_: 'addAdminsToGroup'
  _caller_class_: 'MultiCacheThreadsQueue'
  _thread_id_: string

  admin_ids: string[]
}

export const addAdminsToGroupQuery = (
  q: RequiredAttributes<AddAdminsToGroupQuery, 'admin_ids' | '_thread_id_'>,
): AddAdminsToGroupQuery => ({
  ...clone(basicGraphQLQuery),
  _hash_key_: 'addAdminsToGroup',
  _caller_class_: 'MultiCacheThreadsQueue',
  ...q,
})

export interface RemoveAdminsFromGroupQuery extends GraphQLQuery {
  _hash_key_: 'removeAdminsFromGroup'
  _caller_class_: 'MultiCacheThreadsQueue'
  _thread_id_: string

  admin_ids: string[]
  method: 'DELETE'
}

export const removeAdminsFromGroupQuery = (
  q: RequiredAttributes<RemoveAdminsFromGroupQuery, 'admin_ids' | '_thread_id_'>,
): RemoveAdminsFromGroupQuery => ({
  ...clone(basicGraphQLQuery),
  _hash_key_: 'removeAdminsFromGroup',
  _caller_class_: 'MultiCacheThreadsQueue',
  method: 'DELETE',
  ...q,
})

export interface AddParticipantsToGroupQuery extends GraphQLQuery {
  _hash_key_: 'addMembers'
  _caller_class_: 'MultiCacheThreadsQueue'
  id: string

  method: 'POST'
  to: string[]
}

export const addParticipantsToGroupQuery = (
  q: RequiredAttributes<AddParticipantsToGroupQuery, 'to' | 'id'>,
): AddParticipantsToGroupQuery => ({
  ...clone(basicGraphQLQuery),
  _hash_key_: 'addMembers',
  _caller_class_: 'MultiCacheThreadsQueue',
  method: 'POST',
  ...q,
})

export interface RemoveParticipantsFromGroupQuery extends GraphQLQuery {
  _hash_key_: 'removeMembers'
  _caller_class_: 'MultiCacheThreadsQueue'
  id: string
  to: string[]
  method: 'DELETE'
}

export const removeParticipantsFromGroupQuery = (
  q: RequiredAttributes<RemoveParticipantsFromGroupQuery, 'to' | 'id'>,
): RemoveParticipantsFromGroupQuery => ({
  ...clone(basicGraphQLQuery),
  _hash_key_: 'removeMembers',
  _caller_class_: 'MultiCacheThreadsQueue',
  method: 'DELETE',
  ...q,
})

export interface ModifyThreadQuery extends GraphQLQuery {
  _hash_key_: 'modifyThread'
  _caller_class_: 'WebServiceHandler'
  _operation_:
  | UpdateParticipantNicknameQuery
  | UpdateThreadThemeEmojiQuery
  | UpdateThreadImageQuery
  | MuteThreadQuery
}

export const modifyThreadQuery = (
  _operation_: ModifyThreadQuery['_operation_'],
): ModifyThreadQuery => ({
  ...clone(basicGraphQLQuery),
  _hash_key_: 'modifyThread',
  _caller_class_: 'WebServiceHandler',
  _operation_,
})

export interface UpdateParticipantNicknameQuery extends GraphQLQuery {
  _hash_key_: 'setThreadParticipantNickname'
  _sub_kind_: 'UpdateParticipantNicknameQuery'
  _thread_id_: string

  participant_id: string
  nickname: string
  source: 'thread_settings'
  omit_response_on_success: false
  relative_url: string
}

export const updateParticipantNicknameQuery = (
  q: RequiredAttributes<
  UpdateParticipantNicknameQuery,
  'participant_id' | 'nickname' | '_thread_id_'
  >,
): ModifyThreadQuery =>
  modifyThreadQuery({
    ...clone(basicGraphQLQuery),
    _hash_key_: 'setThreadParticipantNickname',
    _sub_kind_: 'UpdateParticipantNicknameQuery',
    source: 'thread_settings',
    omit_response_on_success: false,
    relative_url: `t_${q._thread_id_}/nicknames`,
    ...q,
  })

export interface UpdateThreadThemeEmojiQuery extends GraphQLQuery {
  _hash_key_: 'setThreadTheme'
  _sub_kind_: 'UpdateThreadThemeEmojiQuery'
  _thread_id_: string

  emoji: string
  source: 'thread_settings'
  omit_response_on_success: false
  relative_url: string
}

export const updateThreadThemeEmojiQuery = (
  q: RequiredAttributes<UpdateThreadThemeEmojiQuery, 'emoji' | '_thread_id_'>,
): ModifyThreadQuery =>
  modifyThreadQuery({
    ...clone(basicGraphQLQuery),
    _hash_key_: 'setThreadTheme',
    _sub_kind_: 'UpdateThreadThemeEmojiQuery',
    source: 'thread_settings',
    omit_response_on_success: false,
    relative_url: `t_${q._thread_id_}/threadcustomization`,
    ...q,
  })

export interface ThreadNameMutation extends GraphQLMutation {
  _hash_key: 'ThreadNameMutation'
  _doc_id_: '3090707060965997'
  new_thread_name: string
  thread_id: string
  source: 'SETTINGS'
  client_mutation_id: string
  actor_id: string
}

export const threadNameMutation = (
  q: RequiredAttributes<
  ThreadNameMutation,
  'new_thread_name' | 'thread_id' | 'client_mutation_id' | 'actor_id'
  >,
): ThreadNameMutation => ({
  ...clone(basicGraphQLMutation),
  _hash_key: 'ThreadNameMutation',
  _doc_id_: '3090707060965997',
  source: 'SETTINGS',
  ...q,
})

export interface UpdateThreadImageQuery extends GraphQLQuery {
  _hash_key_: 'setThreadImage'
  _sub_kind_: 'UpdateThreadImageQuery'

  tid: string

  omit_response_on_success: false
  relative_url: string
  attached_files: { image: {} }
  _image_: Buffer
  _filename_: string
}

export const updateThreadImageQuery = (
  q: RequiredAttributes<
  UpdateThreadImageQuery,
  'tid' | '_image_' | '_filename_'
  >,
): ModifyThreadQuery =>
  modifyThreadQuery({
    ...clone(basicGraphQLQuery),
    _hash_key_: 'setThreadImage',
    _sub_kind_: 'UpdateThreadImageQuery',
    omit_response_on_success: false,
    relative_url: 'method/messaging.setthreadimage',
    attached_files: { image: {} },
    ...q,
  })

/*
[{"method":"POST","body":"format=json&mute_until=-1&locale=en_US&client_country_code=US&fb_api_req_friendly_name=muteThread","name":"muteThread","omit_response_on_success":false,"relative_url":"t_6141738925840077/mute"}]
*/

// NOTE: need to do both call and messages if user so wishes

// only for messages
export interface MuteThreadQuery extends GraphQLQuery {
  _hash_key_: 'muteThread'
  _sub_kind_: 'MuteThreadQuery'
  mute_until: string // -1 = mute until changed || 0 = unmute || timestamp in seconds
  omit_response_on_success: false
  relative_url: string
}

export const muteThreadQuery = (q: RequiredAttributes<MuteThreadQuery, 'mute_until'>, threadID: string): ModifyThreadQuery =>
  modifyThreadQuery({
    ...clone(basicGraphQLQuery),
    _hash_key_: 'muteThread',
    _sub_kind_: 'MuteThreadQuery',
    omit_response_on_success: false,
    relative_url: `t_${threadID}/mute`,
    ...q,
  })

// only for call
export interface MuteThreadCallNotificationMutation extends GraphQLMutation {
  _hash_key_: 'MuteThreadCallNotificationMutation'
  _doc_id_: '4689814717711206'

  thread_fbid: string
  mute_end_timestamp: string // -1 = mute until changed || 0 = unmute || timestamp in seconds
  request_id: string
  client_mutation_id: string
  actor_id: string
}

export const muteThreadCallNotificationMutation = (
  q: RequiredAttributes<
  MuteThreadCallNotificationMutation,
  | 'thread_fbid'
  | 'mute_end_timestamp'
  | 'request_id'
  | 'client_mutation_id'
  | 'actor_id'
  >,
): MuteThreadCallNotificationMutation => ({
  ...clone(basicGraphQLMutation),
  _hash_key_: 'MuteThreadCallNotificationMutation',
  _doc_id_: '4689814717711206',
  ...q,
})

export interface PollDetailQuery extends GraphQLQuery {
  _doc_id_: '3484039508273182'
  _hash_key_: 'PollDetailQuery'
  id: string
  poll_voters_count: 250
}

export interface PollUpdateVoteBatchMutation extends GraphQLMutation {
  _hash_key_: 'PollUpdateVoteBatchMutation'
  _doc_id_: '1769330456420841'
  selected_options: string[]
  selected_new_options: string[] // text content of new option if it is also selected
  new_option_text: string[] // any new option created
  question_id: string
  client_mutation_id: string
  actor_id: string
}

export interface MessengerViewerLiveLocationUpdateMutation
  extends GraphQLMutation {
  _hash_key_: 'MessengerViewerLiveLocationUpdateMutation'
  _doc_id_: '2490363141040184'
  thread_ids: string[] // ['100067883871130']
  client_mutation_id: string // '92fd941c-0bd6-4cd1-b086-278061c39051'
  location: {
    speed_meters_per_second: number // 0
    longitude: number // -18.5333
    latitude: number // 65.9667
    timestamp_milliseconds: number // '1622920233517'
    bearing_degrees: number // 0
    altitude_meters: number // 0
    altitude_accuracy_meters: number // 0
    accuracy_meters: number // 0
  }
  actor_id: string // '100067883871130'
}

export interface MessageLiveLocationUpdateMutation extends GraphQLMutation {
  _hash_key_: 'MessageLiveLocationUpdateMutation'
  _doc_id_: '2245699965536252'
  stop_reason: 'EXPIRED' | 'CANCELED' | 'ARRIVED'
  message_live_location_id: string // '119850020287793'
  client_mutation_id: string // '5f038725-ab26-40b0-9f7a-4fbab78b9d46'
  actor_id: string // '100067883871130'
}

export interface LocationSharingReverseGeocodeQuery extends GraphQLQuery {
  _doc_id_: '3266623283362602'
  _hash_key_: 'LocationSharingReverseGeocodeQuery'
  params: {
    gps_points: [
      {
        longitude: number // -18.533291816711426
        latitude: number // 65.96670185505867
      },
    ]
    caller: 'MESSENGER_ANDROID_LOCATION_SHARING'
  }
}

export interface PlaceTypeaheadSearchQuery extends GraphQLMutation {
  _doc_id_: '3350112678450263'
  _hash_key_: 'PlaceTypeaheadSearchQuery'
  viewer_coordinates: {
    stale_time: number // 0.649
    longitude: number // -18.5333
    latitude: number // 65.9667
    accuracy: number // 52
  }
  query: string // 'sector+31+'
  caller_platform: 'FB_LOCATION_PICKER'
}
