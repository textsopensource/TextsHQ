import Chance from 'chance'
import clone from 'clone-deep'
import merge from 'deepmerge'
import { JsonObject, SerializableEntity, _serialize_ } from './common'

export interface AndroidApplication extends SerializableEntity {
  _kind_: 'AndroidApplication'
  name: string
  version: string
  id: string
  locale: string
  build: string
  version_id: string
  client_id: string
  client_secret: string
  access_token: string
}

export const defaultApplication: AndroidApplication = {
  _kind_: 'AndroidApplication',
  name: 'Orca-Android',
  version: '362.0.0.8.108',
  id: 'com.facebook.orca',
  locale: 'en_US',
  build: '302410839',
  version_id: '1653290564000',
  client_id: '256002347743983',
  client_secret: '374e60f8b9bb6b8cbb30f78030438895',
  get access_token() {
    return `${this.client_id}|${this.client_secret}`
  },
  _serialize_,
}

export interface AndroidDevice extends SerializableEntity {
  _kind_: 'AndroidDevice'
  manufacturer: string
  builder: string
  name: string
  software: string
  architecture: string
  dimensions: string
  user_agent: string
  connection_type: string
  connection_quality: string
  language: string
  country_code: string
  uuid?: string
  adid?: string
  device_group?: string
}

export const defaultDevice: AndroidDevice = {
  _kind_: 'AndroidDevice',
  manufacturer: 'Google',
  builder: 'google',
  name: 'Pixel 2',
  software: '11',
  architecture: 'x86:',
  dimensions: '{density=2.625,width=1080,height=1794}',
  user_agent:
    'Dalvik/2.1.0 (Linux; U; Android 11; Pixel 2 Build/RSR1.201013.001)',
  connection_type: 'WIFI',
  connection_quality: 'EXCELLENT',
  language: 'en_US',
  country_code: 'US',
  _serialize_,
}

export interface AndroidCarrier extends SerializableEntity {
  _kind_: 'AndroidCarrier'
  name: string
  hni: number
}

export const defaultCarrier: AndroidCarrier = {
  _kind_: 'AndroidCarrier',
  name: 'Verizon',
  hni: 311390,
  _serialize_,
}

export interface AndroidSession extends SerializableEntity {
  _kind_: 'AndroidSession'
  access_token?: string
  uid?: number
  password_encryption_pubkey?: string
  password_encryption_key_id?: number
  password_encryption_prefix?: string
  seconds_to_live?: number
  machine_id?: string
  transient_auth_token?: string
  login_first_factor?: string
  region_hint: string
}

export const defaultSession: AndroidSession = {
  _kind_: 'AndroidSession',
  region_hint: 'ODN',
  _serialize_,
}

export interface AndroidState extends SerializableEntity {
  _kind_: 'AndroidState'
  application: AndroidApplication
  device: AndroidDevice
  carrier: AndroidCarrier
  session: AndroidSession
  _common_user_agent_meta_ua_parts_: Record<string, string>
  mqtt_user_agent_meta: string
  http_user_agent: string
}

export const defaultState: AndroidState = {
  _kind_: 'AndroidState',
  application: clone(defaultApplication),
  device: clone(defaultDevice),
  carrier: clone(defaultCarrier),
  session: clone(defaultSession),

  get _common_user_agent_meta_ua_parts_() {
    return {
      FBAN: this.application.name,
      FBAV: this.application.version,
      FBPN: this.application.id,
      FBLC: this.device.language,
      FBBV: this.application.build,
      FBCR: this.carrier.name,
      FBMF: this.device.manufacturer,
      FBBD: this.device.builder,
      FBDV: this.device.name,
      FBSV: this.device.software,
      FBCA: this.device.architecture,
      FBDM: this.device.dimensions,
    }
  },

  get mqtt_user_agent_meta() {
    const ua_meta = Object.entries({
      ...this._common_user_agent_meta_ua_parts_,
      FBLR: '0', // LOW RAM INDICATOR: FALSE
      FBBK: '1', // UNKNOWN HARDCODED 1
    })
      .map(([key, value]) => `${key}/${value}`)
      .join(';')
    return `[${ua_meta};]`
  },

  get http_user_agent() {
    const ua_meta = Object.entries({
      ...this._common_user_agent_meta_ua_parts_,
      FBCA: `${this.device.architecture}armeabi-v7a`,
      FB_FW: '1',
    })
      .map(([key, value]) => `${key}/${value}`)
      .join(';')
    return `${this.device.user_agent} [${ua_meta};]`
  },

  _serialize_,
}

export const generateState = (seed: string) => {
  const state = clone(defaultState)
  const chance = new Chance(seed)
  state.device.adid = chance.guid()
  state.device.uuid = chance.guid()
  return state
}

export const hydrateState = (savedState?: JsonObject) => {
  const state = clone(defaultState)
  return merge(state, savedState) as AndroidState
}
