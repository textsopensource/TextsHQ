import { t } from '@f0rr0/thrift-compact-protocol'
import { threadKeySchema } from './descriptors'

export type JsonObject = { [k in string]?: JsonValue }

export interface JsonArray extends Array<JsonValue> {}

export type JsonValue =
  | string
  | number
  | boolean
  | null
  | JsonObject
  | JsonArray

export type UnwrapOptional<T> = Exclude<T, undefined>

export type ArrayElement<T> = T extends Array<infer R> ? R : T

export type UnwrapPromise<T> = T extends Promise<infer R> ? R : T

export interface ReservedAttributes {
  // Internal
  _kind_: string
  _hash_key_: string
  _serialize_(): JsonObject

  // FB
  _caller_class_: string
  _doc_id_: string
}

export interface SerializableEntity {
  _kind_: string
  _hash_key_?: string
  _serialize_(): JsonObject
}

export type ReservedAsPartial<T> = Omit<T, keyof ReservedAttributes> &
([keyof (T | ReservedAttributes)] extends [never]
  ? {}
  : Partial<{ [k in keyof (T | ReservedAttributes)]: T[k] }>)

// NOTE: https://www.typescriptlang.org/docs/handbook/2/conditional-types.html#distributive-conditional-types
export type RequiredAttributes<
  T,
  K extends keyof T | undefined = undefined,
> = ReservedAsPartial<
[K] extends [keyof T]
  ? Required<Pick<T, K>> & Partial<Omit<T, K>>
  : Partial<T>
>

export enum ThreadType {
  ONE_TO_ONE = 'ONE_TO_ONE',
  GROUP = 'GROUP',
  TINCAN = 'TINCAN',
  TINCAN_MULTI_ENDPOINT = 'TINCAN_MULTI_ENDPOINT',
  PENDING_THREAD = 'PENDING_THREAD',
  PENDING_GENERAL_THREAD = 'PENDING_GENERAL_THREAD',
  SMS = 'SMS',
  WHATSAPP_ONE_TO_ONE = 'WHATSAPP_ONE_TO_ONE',
  OPTIMISTIC_GROUP_THREAD = 'OPTIMISTIC_GROUP_THREAD',
  MONTAGE = 'MONTAGE',
  ENCRYPTED_ONE_TO_ONE_DISAPPEARING = 'ENCRYPTED_ONE_TO_ONE_DISAPPEARING',
  CARRIER_MESSAGING_ONE_TO_ONE = 'CARRIER_MESSAGING_ONE_TO_ONE',
  CARRIER_MESSAGING_GROUP = 'CARRIER_MESSAGING_GROUP',
  ADVANCED_CRYPTO_ONE_TO_ONE = 'ADVANCED_CRYPTO_ONE_TO_ONE',
  ADVANCED_CRYPTO_GROUP = 'ADVANCED_CRYPTO_GROUP',
  COMMUNITY_FOLDER = 'COMMUNITY_FOLDER',
}

export enum ReactionAction {
  ADD = 'ADD_REACTION',
  REMOVE = 'REMOVE_REACTION',
}

export enum ThreadFolder {
  INBOX = 'INBOX',
  PENDING = 'PENDING',
  ARCHIVED = 'ARCHIVED',
  OTHER = 'OTHER',
}

export enum MessageUnsendability {
  DENY_FOR_NON_SENDER = 'deny_for_non_sender',
  DENY_LOG_MESSAGE = 'deny_log_message',
  DENY_TOMBSTONE_MESSAGE = 'deny_tombstone_message',
  DENY_IF_PAGE_THREAD = 'deny_if_page_thread',
  DENY_IF_CANNOT_REPLY_TO_VIEWER_THREAD = 'deny_if_cannot_reply_to_viewer_thread',
  DENY_IF_MARKETPLACE_THREAD = 'deny_if_marketplace_thread',
  CAN_UNSEND = 'can_unsend',
}

export enum ThreadCannotReplyReason {
  BLOCKED = 'BLOCKED',
  MESSENGER_BLOCKEE = 'MESSENGER_BLOCKEE',
  COMPOSER_DISABLED_BOT = 'COMPOSER_DISABLED_BOT',
  HAS_EMAIL_PARTICIPANT = 'HAS_EMAIL_PARTICIPANT',
  OBJECT_ORIGINATED = 'OBJECT_ORIGINATED',
  READ_ONLY = 'READ_ONLY',
  VIEWER_NOT_SUBSCRIBED = 'VIEWER_NOT_SUBSCRIBED',
  RECIPIENTS_NOT_LOADABLE = 'RECIPIENTS_NOT_LOADABLE',
  RECIPIENTS_UNAVAILABLE = 'RECIPIENTS_UNAVAILABLE',
  RECIPIENTS_INVALID = 'RECIPIENTS_INVALID',
  RECIPIENTS_INACTIVE_WORK_ACC = 'RECIPIENTS_INACTIVE_WORK_ACC',
  MONTAGE_NOT_AUTHOR = 'MONTAGE_NOT_AUTHOR',
  VIEWER_MUTED_IN_FBGROUP = 'VIEWER_MUTED_IN_FBGROUP',
  VIEWER_IS_LIMITED_WORK_ACCOUNT = 'VIEWER_IS_LIMITED_WORK_ACCOUNT',
  RECIPIENTS_IS_LIMITED_WORK_ACCOUNT = 'RECIPIENTS_IS_LIMITED_WORK_ACCOUNT',
  WORK_GARDEN_ARCHIVED = 'WORK_GARDEN_ARCHIVED',
  PARTICIPANTS_IN_RESTRICTED_JURISDICTION = 'PARTICIPANTS_IN_RESTRICTED_JURISDICTION',
  BROADCAST_CHAT_READ_ONLY = 'BROADCAST_CHAT_READ_ONLY',

}
export enum ThreadReadStateEffect {
  MARK_READ = 1,
  MARK_UNREAD,
  KEEP_AS_IS,
}

export enum MessageLiveLocationStopReason {
  EXPIRED = 1,
  CANCELED,
  ARRIVED,
}

export enum MessageReactionAction {
  ADD_REACTION = 0,
  REMOVE_REACTION,
}

export enum MessageReplyStatus {
  VALID = 0,
  DELETED,
  TEMPORARILY_UNAVAILABLE,
}

export enum AddMemberSource {
  ADD = 0,
  JOIN_THROUGH_LINK,
}

export enum AppState {
  BACKGROUND = 0,
  FOREGROUND,
  UNKNOWN,
}

export enum AttachmentType {
  NONE = 0,
  STICKER,
  IMAGE,
  ANIMATED_IMAGE,
  VIDEO,
  AUDIO,
  FILE,
  XMA,
  EPHEMERAL_IMAGE,
  EPHEMERAL_VIDEO,
  SELFIE_STICKER,
}

export enum ImageType {
  FILE_ATTACHMENT = 1,
  MESSENGER_CAM,
}

export enum ImageSource {
  FILE = 1,
  QUICKCAM_FRONT,
  QUICKCAM_BACK,
}

export enum VideoSource {
  NON_QUICKCAM = 1,
  QUICKCAM,
  SPEAKING_STICKER,
  RECORDED_STICKER,
  VIDEO_MAIL,
}

export enum MessagePowerUpStyle {
  NONE = 0,
  LOVE,
  GIFT_WRAP,
  CELEBRATION,
  FIRE,
}

export enum PresencePublishType {
  FULL = 1,
  INCREMENTAL,
}

export enum ThreadPresenceState {
  NOT_IN_THREAD = 0,
  IN_THREAD,
}

export enum TypingState {
  IDLE = 0,
  TYPING,
}

export enum MarkThreadState {
  archived = 'archived',
  spam = 'spam',
  other = 'other',
  inbox = 'inbox',
  pageFollowUp = 'pageFollowUp',
  read = 'read',
}

export const createGroupErrorMap = {
  0: { type: 'NONE', message: '' },
  900: { type: 'MQTT_EXCEPTION', message: 'MQTT exception' },
  901: { type: 'MQTT_REMOTEEXCEPTION', message: 'MQTT remote exception' },
  902: { type: 'MQTT_IOEXCEPTION', message: 'MQTT IO exception' },
  911: { type: 'FAILED_NOT_CONNECTED', message: 'MQTT not connected' },
  912: {
    type: 'FAILED_TIMED_OUT_AFTER_PUBLISH',
    message: 'Timed out after publish',
  },
  913: {
    type: 'FAILED_TIMED_OUT_WAITING_FOR_RESPONSE',
    message: 'Timed out waiting for response',
  },
  914: {
    type: 'FAILED_SERVER_RETURNED_FAILURE',
    message: 'Server returned failure',
  },
  915: { type: 'FAILED_PUBLISH_FAILED', message: 'Publish failed' },
  916: {
    type: 'FAILED_UNKNOWN_EXCEPTION',
    message: 'Failed to send via MQTT',
  },
  917: { type: 'FAILED_NO_RETRY', message: 'Failure, no retry' },
  918: { type: 'FAILED_THRIFT_EXCEPTION', message: 'Thrift serialize failed' },
  919: {
    type: 'FAILED_PUBLISH_FAILED_WITH_EXCEPTION',
    message: 'Publish failed with exception',
  },
  928: {
    type: 'FAILED_PUBLISH_FAILED_TIMEOUT',
    message: 'Publish failed with timeout',
  },
  929: { type: 'SKIPPED_PHOTO', message: 'Group has a photo' },
  930: {
    type: 'SKIPPED_MQTT_DISABLED',
    message: 'MQTT group creation disabled',
  },
  931: {
    type: 'SKIPPED_FACEBOOK_GROUP',
    message: 'Group is associated with a Facebook group',
  },
  932: {
    type: 'FAILED_BLOCKED_PARTICIPANTS',
    message: 'Group includes blocked participants',
  },
  933: {
    type: 'SKIPPED_NO_OFFLINE_THREADING_ID',
    message: 'Group has no offline threading ID',
  },
}

export const serializeObject = (object: object) =>
  Object.entries(object).reduce(
    (prev, [key, val]) => ({
      ...prev,
      ...(key.startsWith('_') && key.endsWith('_')
        ? {}
        : {
          [`${key}`]:
              typeof val?._serialize_ === 'function' ? val._serialize_() : val,
        }),
    }),
    {},
  )

export function _serialize_(this: SerializableEntity) {
  return serializeObject(this)
}

export const getThreadIdFromThread = (
  thread:
  | t.TypeOf<typeof threadKeySchema>
  | { [K in keyof t.TypeOf<typeof threadKeySchema>]: string },
) => String(thread.thread_fbid || thread.other_user_id)
