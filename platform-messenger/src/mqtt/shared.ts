import { CompressCallback, deflate, InputType, unzip, ZlibOptions } from 'zlib'
import debug, { Debugger } from 'debug'
import { promisify } from 'util'
import { thriftWriteFromObject } from '@f0rr0/thrift-compact-protocol'
import { texts } from '@textshq/platform-sdk'
import { mqttThriftHeaderSchema } from '../lib/descriptors'

// only BEST_COMPRESSION is used
const ZLIB_HEADER_BEST_COMPRESSION = 0x78da

export type Parser<T> = (payload: Buffer) => T

const deflatePromise = promisify(
  deflate as (
    buffer: InputType,
    options: ZlibOptions,
    callback: CompressCallback
  ) => void,
)

const unzipPromise = promisify(
  unzip as (buffer: InputType, callback: CompressCallback) => void,
)

export function compressDeflate(data: string | Buffer): Promise<Buffer> {
  return deflatePromise(data, { level: 9 })
}

export function unzipAsync(data: string | Buffer): Promise<Buffer> {
  return unzipPromise(data)
}

export async function tryUnzipAsync(data: Buffer): Promise<Buffer> {
  try {
    if (
      data.byteLength <= 2
      || data.readUInt16BE() !== ZLIB_HEADER_BEST_COMPRESSION
    ) {
      // texts.log('Not compressed', data)
      return data
    }
    // texts.log('Compressed', data)
    return unzipAsync(data)
  } catch (e) {
    texts.log('Problem with zlib', data.readUInt16BE(), e.message)
    return data
  }
}

export const tryRemoveLeadingZeroByte = (data: Buffer) =>
  (data.byteLength && data.readUInt8(0) === 0x00 ? data.slice(1) : data)

export function debugChannel(...path: string[]): Debugger {
  return debug(['fb', ...path].join(':'))
}

export const emptyTracingHeader: Buffer = ((): Buffer =>
  thriftWriteFromObject({ traceInfo: '' }, mqttThriftHeaderSchema))()
