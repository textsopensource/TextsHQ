/* eslint-disable class-methods-use-this */
import { texts } from '@textshq/platform-sdk'
import EventEmitter from 'eventemitter3'
import { MqttMessage } from 'mqtts'
import { inspect } from 'util'
import {
  REALTIME,
  SubscribeTopics,
  SubscribeTopicsArray,
  SubscribeTopicsIdMap,
} from './constants'
import {
  compressDeflate,
  debugChannel,
  tryRemoveLeadingZeroByte,
  tryUnzipAsync,
} from './shared'
import {
  MQTToTClient,
  MQTToTConnection,
  MQTToTClientInitOptions,
} from './mqttot'
import { ClientDisconnectedError } from './errors'
import { AndroidState } from '../lib/state'
import Commands from './commands'
import RequestResponseOverMQTT from './request-respose-mqtt'
import { stringifyWithBigInt } from '../lib/utils'

const realtimeDebug = debugChannel('realtime')
const messageDebug = realtimeDebug.extend('message')

export interface AndroidMQTTEvents {
  safe_disconnect: () => void
  unsafe_disconnect: () => void
  region_hint: (
    payload: ReturnType<typeof SubscribeTopics.REGION_HINT.parser>
  ) => void
  typing: (
    payload: ReturnType<typeof SubscribeTopics.TYPING_NOTIFICATION.parser>
  ) => void
  presence: (
    payload: ReturnType<typeof SubscribeTopics.ORCA_PRESENCE.parser>
  ) => void
  message_sync: (
    payload: ReturnType<typeof SubscribeTopics.MESSAGE_SYNC.parser>
  ) => void
}

export default class AndroidMQTT extends EventEmitter<AndroidMQTTEvents> {
  protected state: AndroidState

  public sync_token?: string

  public sync_sequence_id?: string

  // This are public to access them from PlatformAPI and update retry behaviour
  public safeDisconnect = false

  public mqtt?: MQTToTClient

  public commands?: Commands

  public requestResponseState: RequestResponseOverMQTT

  public constructor(state: AndroidState) {
    super()
    this.state = state
    this.requestResponseState = new RequestResponseOverMQTT()
    this.handleRequestResponseOverMQTT = this.handleRequestResponseOverMQTT.bind(this)
  }

  private resetSyncState = () => {
    this.sync_sequence_id = undefined
    this.sync_token = undefined
  }

  public resyncConnection(sequence_id: string) {
    this.sync_sequence_id = sequence_id
    if (this.sync_token) {
      this.commands.diffMessageSyncQueue(this.sync_sequence_id, this.sync_token)
      texts.log('Synced connection with broker')
    } else {
      texts.log('No sync token to use for syncing')
    }
  }

  public async connect(
    initOptions: Partial<MQTToTClientInitOptions> = {},
  ): Promise<void> {
    realtimeDebug('Connecting to realtime-broker...')

    this.mqtt?.disconnect()
    this.mqtt = new MQTToTClient({
      url: REALTIME.HOST_NAME_V6,
      payloadProvider: () => compressDeflate(this.constructConnection()),
      autoReconnect: false,
      requirePayload: false,
      ...initOptions,
    })

    this.mqtt.on('message', this.onMessage.bind(this))
    this.mqtt.on('error', e => {
      this.resetSyncState()
      this.emitError(e)
    })
    this.mqtt.on('warning', w => this.emitWarning(w))
    this.mqtt.on('disconnect', () => {
      if (!this.safeDisconnect) {
        this.emit('unsafe_disconnect')
        this.emitError(
          new ClientDisconnectedError('MQTToTClient got disconnected.'),
        )
      } else {
        this.emit('safe_disconnect')
      }
    })

    /*
      0: Connection successful
      1: Connection refused – incorrect protocol version
      2: Connection refused – invalid client identifier
      3: Connection refused – server unavailable
      4: Connection refused – bad username or password
      5: Connection refused – not authorised
    */
    return new Promise((resolve, reject) => {
      this.mqtt?.on('connect', async ({ returnCode }) => {
        if (returnCode !== 0) {
          texts.log(`Non zero return code ${returnCode}`)
          texts.Sentry.captureException({
            message: 'Non zero return code',
            returnCode,
          })
        }
        await this.postConnect()
        resolve()
      })
      this.mqtt
        ?.connect({
          keepAlive: 60,
          protocolLevel: 3,
          clean: true,
          connectDelay: 60 * 1000,
        })
        .catch(e => {
          this.resetSyncState()
          this.emitError(e)
          reject(e)
        })
    })
  }

  public async disconnect(force = false): Promise<void> {
    if (this.mqtt) {
      this.safeDisconnect = true
      // don't disconnect already disconnected client
      if (!this.mqtt.disconnected) {
        await this.mqtt.disconnect(force)
      }
      this.commands = undefined
    }
  }

  protected async onMessage(message: MqttMessage) {
    const unzipped = await tryUnzipAsync(message.payload)
    const payload = tryRemoveLeadingZeroByte(unzipped)
    const topic = SubscribeTopicsIdMap[message.topic]
    const debugMessageParsing = () => {
      const firstNullByteIndex = unzipped.indexOf(0x00)
      texts.log(
        `##### Previously missing data for topic ${message.topic}, null byte at index ${firstNullByteIndex}`,
      )
      if (topic.parser) {
        const parsedMessageOld = topic.parser(
          Buffer.from(unzipped.slice(firstNullByteIndex + 1)),
        )
        const parsedMessage = topic.parser(payload)
        texts.log(`##### With missing data: ${inspect(parsedMessageOld)}`)
        texts.log(`##### Without missing data: ${inspect(parsedMessage)}`)
      }
    }
    // log if old implementation was slicing part of data for a topic
    if (texts.IS_DEV && unzipped.indexOf(0x00) > 0) {
      debugMessageParsing()
    }
    if (topic) {
      switch (topic.id) {
        case SubscribeTopics.REGION_HINT.id: {
          const parsedMessage = topic.parser(payload)
          this.emit('region_hint', parsedMessage)
          break
        }
        case SubscribeTopics.MESSAGE_SYNC.id: {
          const parsedMessage = topic.parser(payload)
          // break down futher
          this.emit('message_sync', parsedMessage)
          break
        }
        case SubscribeTopics.TYPING_NOTIFICATION.id: {
          const parsedMessage = topic.parser(payload)
          this.emit('typing', parsedMessage)
          break
        }
        case SubscribeTopics.ORCA_PRESENCE.id: {
          const parsedMessage = topic.parser(payload)
          this.emit('presence', parsedMessage)
          break
        }
        default: {
          messageDebug(
            `We received unhandled topic id ${topic?.id} with path ${topic?.path}]`,
          )
        }
      }
      const parsedMessage = topic.parser(payload)
      messageDebug(
        `Received on ${topic.path}: ${stringifyWithBigInt(
          parsedMessage,
          2,
        )}`,
      )
    } else {
      messageDebug(
        `Received raw on ${message.topic}: (${
          unzipped.byteLength
        } bytes) ${unzipped.toString()}`,
      )
      // new BufferReader(payload).pretty_print()
    }
  }

  public handleRequestResponseOverMQTT(
    responseTopic: typeof SubscribeTopicsIdMap[string],
    id: string,
    parsedMessage: any,
  ) {
    if (responseTopic.isRequestResponseOverMQTT && responseTopic.requestTopic) {
      this.requestResponseState.handleResponse(
        responseTopic.requestTopic.id,
        id,
        parsedMessage,
      )
      this.requestResponseState.clearRequest(responseTopic.requestTopic.id, id)
    }
  }

  protected emitError(e: Error) {
    texts.log(e)
    texts.Sentry.captureException(e)
  }

  protected emitWarning(e: Error) {
    texts.log(e)
    texts.Sentry.captureException(e)
  }

  protected constructConnection() {
    const userAgent = this.state.mqtt_user_agent_meta
    const deviceId = this.state.device.uuid!
    const password = this.state.session.access_token!
    const connection = new MQTToTConnection({
      clientIdentifier: deviceId.substring(0, 20),
      clientInfo: {
        userId: BigInt(this.state.session.uid!),
        userAgent,
        clientCapabilities: BigInt(439),
        endpointCapabilities: BigInt(90),
        publishFormat: 2, // ZLIB_OPTIONAL
        noAutomaticForeground: true,
        makeUserAvailableInForeground: true,
        deviceId,
        isInitiallyForeground: true,
        networkType: 1,
        networkTypeInfo: 7, // WIFI
        networkSubtype: 0,
        clientMqttSessionId: BigInt(Date.now()) & BigInt(0xffffffff),
        subscribeTopics: SubscribeTopicsArray.map(({ id }) =>
          Number.parseInt(id, 10)),
        clientType: '',
        appId: BigInt(this.state.application.client_id),
        deviceSecret: '',
        regionPreference: this.state.session.region_hint || 'ODN',
        clientStack: 3,
      },
      password,
      appSpecificInfo: {
        ls_sv: this.state.application.version_id,
        ls_fdid: deviceId,
      },
    })
    return connection.toThrift()
  }

  protected async postConnect() {
    this.commands = new Commands(
      this.mqtt!,
      this.state,
      this.requestResponseState,
    )
    return Promise.all([
      this.commands?.toggleAppForeground(true),
      this.sync_token ? this.commands?.diffMessageSyncQueue(this.sync_sequence_id, this.sync_token) : this.commands?.createMessageSyncQueue(this.sync_sequence_id!),
    ])
  }
}
