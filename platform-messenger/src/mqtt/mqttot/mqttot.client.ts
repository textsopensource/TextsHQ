import { texts } from '@textshq/platform-sdk'
import {
  ConnectRequestOptions,
  DefaultPacketReadMap,
  DefaultPacketReadResultMap,
  DefaultPacketWriteMap,
  DefaultPacketWriteOptions,
  isConnAck,
  MqttClient,
  MqttMessage,
  MqttMessageOutgoing,
  PacketFlowFunc,
  PacketType,
  TlsTransport,
} from 'mqtts'
import { ConnectionOptions } from 'tls'
import { compressDeflate, debugChannel } from '../shared'
import {
  MQTToTConnectPacketOptions,
  writeConnectRequestPacket,
} from './mqttot.connect.request.packet'
import { ConnectionFailedError, EmptyPacketError } from '../errors'
import {
  MQTToTConnectResponsePacket,
  readConnectResponsePacket,
} from './mqttot.connect.response.packet'

export type MQTToTReadMap = Omit<
DefaultPacketReadResultMap,
PacketType.ConnAck
> & {
  [PacketType.ConnAck]: MQTToTConnectResponsePacket
}

export type MQTToTWriteMap = Omit<
DefaultPacketWriteOptions,
PacketType.Connect
> & {
  [PacketType.Connect]: MQTToTConnectPacketOptions
}

export interface MQTToTClientInitOptions {
  url: string
  payloadProvider: () => Promise<Buffer>
  enableTrace?: boolean
  autoReconnect: boolean
  requirePayload: boolean
  additionalOptions?: ConnectionOptions
}

export class MQTToTClient extends MqttClient<MQTToTReadMap, MQTToTWriteMap> {
  protected connectPayloadProvider: MQTToTClientInitOptions['payloadProvider']

  protected connectPayload?: Buffer

  protected requirePayload: MQTToTClientInitOptions['requirePayload']

  protected mqttotDebug: (msg: string) => void

  public constructor(options: MQTToTClientInitOptions) {
    super({
      autoReconnect: options.autoReconnect,
      readMap: {
        ...DefaultPacketReadMap,
        [PacketType.ConnAck]: readConnectResponsePacket,
      },
      writeMap: {
        ...DefaultPacketWriteMap,
        [PacketType.Connect]: writeConnectRequestPacket,
      },
      transport: new TlsTransport({
        host: options.url,
        port: 443,
        additionalOptions: options.additionalOptions,
      }),
    })
    this.mqttotDebug = (msg: string, ...args: string[]) =>
      debugChannel('mqttot')(`${options.url}: ${msg}`, ...args)
    this.connectPayloadProvider = options.payloadProvider
    this.mqttotDebug('Creating client')
    this.registerListeners()
    this.requirePayload = options.requirePayload
  }

  protected registerListeners() {
    const printErrorOrWarning = (type: string) => (e: Error | string) => {
      if (typeof e === 'string') {
        this.mqttotDebug(`${type}: ${e}`)
        texts.Sentry.captureException(e)
      } else {
        this.mqttotDebug(`${type}: ${e.message}\n\tStack: ${e.stack}`)
        texts.Sentry.captureException(e)
      }
    }
    this.on('error', printErrorOrWarning('Error'))
    this.on('warning', printErrorOrWarning('Warning'))
    this.on('disconnect', e =>
      this.mqttotDebug(`Disconnected.\n\n${JSON.stringify(e, null, 2)}`))
  }

  async connect(options?: ConnectRequestOptions): Promise<any> {
    this.connectPayload = await this.connectPayloadProvider()
    return super.connect(options)
  }

  protected getConnectFlow(): PacketFlowFunc<
  MQTToTReadMap,
  MQTToTWriteMap,
  any
  > {
    return mqttotConnectFlow(this.connectPayload!, this.requirePayload)
  }

  public async mqttotPublish(
    message: MqttMessage,
  ): Promise<MqttMessageOutgoing> {
    this.mqttotDebug(
      `Publishing ${message.payload.byteLength}bytes to topic ${message.topic}`,
    )
    return this.publish({
      topic: message.topic,
      payload: await compressDeflate(message.payload),
      qosLevel: message.qosLevel,
    })
  }
}

export function mqttotConnectFlow(
  payload: Buffer,
  requirePayload: MQTToTClientInitOptions['requirePayload'],
): PacketFlowFunc<MQTToTReadMap, MQTToTWriteMap, MQTToTConnectResponsePacket> {
  return (success, error) => ({
    start: () => ({
      type: PacketType.Connect,
      options: {
        payload,
        keepAlive: 60,
      },
    }),
    accept: isConnAck,
    next: (packet: MQTToTConnectResponsePacket) => {
      if (packet.isSuccess) {
        if (packet.payload?.length || !requirePayload) success(packet)
        else {
          error(
            new EmptyPacketError(
              `CONNACK: no payload (payloadExpected): ${packet.payload}`,
            ),
          )
        }
      } else {
        error(
          new ConnectionFailedError(
            `CONNACK returnCode: ${packet.returnCode} errorName: ${packet.errorName}`,
          ),
        )
      }
    },
  })
}
