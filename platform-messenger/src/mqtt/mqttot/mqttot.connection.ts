import { thriftWriteFromObject, t } from '@f0rr0/thrift-compact-protocol'

export class MQTToTConnection {
  private fbnsConnectionData: t.TypeOf<typeof MQTToTConnection.thriftConfig>

  public static thriftConfig = t.struct({
    clientIdentifier: t.field(1, t.binary()),
    willTopic: t.field(2, t.binary()).optional(),
    willMessage: t.field(3, t.binary()).optional(),
    clientInfo: t.field(
      4,
      t.struct({
        userId: t.field(1, t.int64()),
        userAgent: t.field(2, t.binary()),
        clientCapabilities: t.field(3, t.int64()),
        endpointCapabilities: t.field(4, t.int64()),
        publishFormat: t.field(5, t.int32()),
        noAutomaticForeground: t.field(6, t.boolean()),
        makeUserAvailableInForeground: t.field(7, t.boolean()),
        deviceId: t.field(8, t.binary()),
        isInitiallyForeground: t.field(9, t.boolean()),
        networkType: t.field(10, t.int32()),
        networkSubtype: t.field(11, t.int32()),
        clientMqttSessionId: t.field(12, t.int64()),
        clientIpAddress: t.field(13, t.binary()).optional(),
        subscribeTopics: t.field(14, t.list(t.int32())),
        clientType: t.field(15, t.binary()),
        appId: t.field(16, t.int64()),
        overrideNectarLogging: t.field(17, t.boolean()).optional(),
        connectTokenHash: t.field(18, t.binary()).optional(),
        regionPreference: t.field(19, t.binary()),
        deviceSecret: t.field(20, t.binary()),
        clientStack: t.field(21, t.byte()),
        fbnsConnectionKey: t.field(22, t.int64()).optional(),
        fbnsConnectionSecret: t.field(23, t.binary()).optional(),
        fbnsDeviceId: t.field(24, t.binary()).optional(),
        fbnsDeviceSecret: t.field(25, t.binary()).optional(),
        unknown: t.field(26, t.int64()).optional(),
        networkTypeInfo: t.field(27, t.int32()),
        sslFingerprint: t.field(28, t.binary(t.TBinaryType.STRING)).optional(),
        tcpFingerPrint: t.field(29, t.binary(t.TBinaryType.STRING)).optional(),
      }),
    ),
    password: t.field(5, t.binary()),
    getDiffsRequests: t.field(6, t.list(t.binary())).optional(),
    zeroRatingTokenHash: t.field(9, t.binary()).optional(),
    appSpecificInfo: t.field(10, t.map(t.binary(), t.binary())),
  })

  public constructor(
    connectionData: t.TypeOf<typeof MQTToTConnection.thriftConfig>,
  ) {
    this.fbnsConnectionData = connectionData
  }

  public toThrift(): Buffer {
    return thriftWriteFromObject(
      this.fbnsConnectionData,
      MQTToTConnection.thriftConfig,
    )
  }

  public toString(): string {
    return this.toThrift().toString()
  }
}
