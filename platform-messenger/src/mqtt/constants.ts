import {
  regionHintParser,
  messageSyncParser,
  presenceUpdateListParser,
  typingNotificationParser,
  sendMessageResponseParser,
} from './parsers'

export const PublishTopics = {
  FOREGROUND_STATE: {
    id: '102' as const,
    path: '/t_fs' as const,
    isRequestResponseOverMQTT: false,
  },
  LIGHTSPEED_REQUEST: {
    id: '178' as const,
    path: '/ls_req' as const,
    isRequestResponseOverMQTT: false,
  },
  SEND_ADDITIONAL_CONTACTS: {
    id: '109' as const,
    path: '/t_sac' as const,
    isRequestResponseOverMQTT: false,
  },
  SEND_THREAD_PRESENCE: {
    id: '104' as const,
    path: '/t_stp' as const,
    isRequestResponseOverMQTT: false,
  },
  SEND_TYPING: {
    id: '105' as const,
    path: '/t_st' as const,
    isRequestResponseOverMQTT: false,
  },
  SEND_MESSAGE: {
    id: '69' as const,
    path: '/t_sm' as const,
    isRequestResponseOverMQTT: true,
  },
  DELIVERY_RECEIPT_BATCH: {
    id: '78' as const,
    path: '/t_dr_batch' as const,
    isRequestResponseOverMQTT: true,
  },
  MARK_THREAD: {
    id: '121' as const,
    path: '/t_mt_req' as const,
  },
  CREATE_GROUP: {
    id: '156' as const,
    path: '/t_create_group' as const,
    isRequestResponseOverMQTT: true,
  },
  ADD_PARTICIPANTS_TO_GROUP: {
    id: '181' as const,
    path: '/t_add_participants_to_group' as const,
    isRequestResponseOverMQTT: true,
  },
  REQUEST_STREAM_REQUEST: {
    id: '244' as const,
    path: '/rs_req' as const,
    isRequestResponseOverMQTT: false,
  },
}

export const PublishTopicsArray = Object.values(PublishTopics)

export const PublishTopicsIdMap = Object.fromEntries(
  Object.values(PublishTopics).map(topic => [topic.id, topic]),
)

export const SubscribeTopics = {
  ORCA_PRESENCE: {
    id: '35' as const,
    path: '/orca_presence' as const,
    parser: presenceUpdateListParser,
    isRequestResponseOverMQTT: false,
    requestTopic: undefined,
  },

  REGION_HINT: {
    id: '150' as const,
    path: '/t_region_hint' as const,
    parser: regionHintParser,
    isRequestResponseOverMQTT: false,
    requestTopic: undefined,
  },
  MESSAGE_SYNC: {
    id: '59' as const,
    path: '/t_ms' as const,
    parser: messageSyncParser,
    isRequestResponseOverMQTT: false,
    requestTopic: undefined,
  },
  // GENERIC_PRESENCE: {
  //   id: '65' as const,
  //   path: '/t_p' as const,
  //   parser: presenceUpdateBatchParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
  // SPECIAL_PRESENCE: {
  //   id: '92' as const,
  //   path: '/t_sp' as const,
  //   parser: presenceUpdateBatchParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
  // THREAD_PRESENCE: {
  //   id: '103' as const,
  //   path: '/t_tp' as const,
  //   parser: presenceUpdateBatchParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
  TYPING_NOTIFICATION: {
    id: '100' as const,
    path: '/t_tn',
    parser: typingNotificationParser,
    isRequestResponseOverMQTT: false,
    requestTopic: undefined,
  },
  // THREAD_TYPING: {
  //   id: '195' as const,
  //   path: '/t_thread_typing' as const,
  //   parser: typingNotificationParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
  SEND_MESSAGE_RESPONSE: {
    id: '70' as const,
    path: '/t_sm_rp' as const,
    parser: sendMessageResponseParser,
    isRequestResponseOverMQTT: true,
    requestTopic: PublishTopics.SEND_MESSAGE,
  },
  // DELIVERY_RECEIPT_BATCH_RESPONSE: {
  //   id: '85' as const,
  //   path: '/t_dr_response' as const,
  //   parser: deliveryReceiptBatchResponseParser,
  //   isRequestResponseOverMQTT: true,
  //   requestTopic: PublishTopics.DELIVERY_RECEIPT_BATCH,
  // },
  // MARK_THREAD_RESPONSE: {
  //   id: '122' as const,
  //   path: '/t_mt_resp' as const,
  //   parser: markThreadResponseParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
  // CREATE_GROUP_RESPONSE: {
  //   id: '157' as const,
  //   path: '/t_create_group_rp' as const,
  //   parser: createGroupResponseParser,
  //   isRequestResponseOverMQTT: true,
  //   requestTopic: PublishTopics.CREATE_GROUP,
  // },
  // ADD_PARTICIPANTS_TO_GROUP_RESPONSE: {
  //   id: '182' as const,
  //   path: '/t_add_participants_to_group_rp' as const,
  //   parser: addParticipantsToGroupResponseParser,
  //   isRequestResponseOverMQTT: true,
  //   requestTopic: PublishTopics.ADD_PARTICIPANTS_TO_GROUP,
  // },
  // REQUEST_STREAM_RESPONSE: {
  //   id: '245' as const,
  //   path: '/rs_resp' as const,
  //   parser: requestStreamResponseParser,
  //   isRequestResponseOverMQTT: false,
  //   requestTopic: undefined,
  // },
}

export const SubscribeTopicsArray = Object.values(SubscribeTopics)

export const SubscribeTopicsIdMap = Object.fromEntries(
  Object.values(SubscribeTopics).map(topic => [topic.id, topic]),
)

export const REALTIME = {
  HOST_NAME_V6: 'edge-mqtt.facebook.com',
}
