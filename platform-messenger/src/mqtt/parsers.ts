import {
  t,
  thriftReadToObject,
} from '@f0rr0/thrift-compact-protocol'
import { texts } from '@textshq/platform-sdk'
import { debugChannel, Parser } from './shared'
import {
  messageSyncPayloadSchema,
  messageSyncPayloadUnwrappedSchema,
  messageSyncInnerEventPayloadUnwrappedSchema,
  messageSyncEventUnwrappedSchema,
  regionHintPayloadSchema,
  regionHintUnwrappedSchema,
  typingNotificationSchema,
  deliveryReceiptBatchResponseSchema,
  sendMessageResponseSchema,
  markThreadResponseSchema,
  createGroupResponseSchema,
  addParticipantsToGroupResponseSchema,
  groupCreationMutationResponseSchema,
  requestStreamPresenceUpdateResponseUnwrappedSchema,
  requestStreamPresenceUpdateResponseSchema,
  messageSyncDeltas,
} from '../lib/descriptors'
import { ArrayElement, UnwrapOptional } from '../lib/common'
import { stringifyWithBigInt } from '../lib/utils'

const realtimeDebug = debugChannel('realtime')
const parserDebug = realtimeDebug.extend('parser')

export const jsonParser: Parser<any> = (payload: Buffer) =>
  (payload.length > 0 ? JSON.parse(payload.toString()) : {})

export const regionHintParser: Parser<
t.TypeOf<typeof regionHintUnwrappedSchema>
> = (payload: Buffer) =>
  thriftReadToObject(
    thriftReadToObject(payload, regionHintPayloadSchema).region_hint,
    regionHintUnwrappedSchema,
  )

export interface PresenceItem {
  userId: number
  activeStatus: boolean
  lastSeen: number
}

interface PresenceItemSchema {
  u: number
  p: number
  l: number
  vc: number
  c: number
}

export const presenceUpdateListParser = (payload: Buffer): PresenceItem[] => {
  const parsed = JSON.parse(payload.toString())
  return parsed.list.map((u: PresenceItemSchema) => ({
    userId: u.u,
    activeStatus: u.p === 2 ? 'online' : 'offline',
    lastSeen: u.p === 2 || u.p === 0 ? u.l * 1000 : undefined,
  }))
}

type UnwrappedInnerEvents = ArrayElement<
UnwrapOptional<
t.TypeOf<typeof messageSyncEventUnwrappedSchema>['inner_items']
>['items']
>

type MessageSyncPayload = t.TypeOf<typeof messageSyncPayloadUnwrappedSchema>

export type FlattenedMessageSyncPayload = Omit<MessageSyncPayload, 'items'> & {
  items?: Array<
  Omit<
  ArrayElement<UnwrapOptional<MessageSyncPayload['items']>>,
  'inner_items'
  > & { inner_items?: UnwrappedInnerEvents[] }
  >
}

export type FlattenedMessageSyncPayloadItems = Required<
ArrayElement<UnwrapOptional<FlattenedMessageSyncPayload['items']>>
>

export const messageSyncParser: Parser<FlattenedMessageSyncPayload> = (
  payload: Buffer,
) => {
  let parsed

  // TODO - fix hack and investigate where else thrift parsing is incorrect
  try {
    parsed = thriftReadToObject(payload, messageSyncPayloadSchema)
  } catch (e) {
    parsed = thriftReadToObject(payload, messageSyncDeltas)
    if (!(parsed.items?.find(i => i.mark_read_own))) {
      // this isn't just a message sent from another device
      texts.Sentry.captureMessage('Thrift parse error on messageSyncParser')
    }
  }

  const transformed: FlattenedMessageSyncPayload = {
    ...parsed,
    items: parsed.items?.map(({ inner_items, ...rest }) => ({
      ...rest,
      ...(inner_items?.items
        ? {
          inner_items: thriftReadToObject(
            inner_items.items,
            messageSyncInnerEventPayloadUnwrappedSchema,
          ).items,
        }
        : {}),
    })),
  }
  return transformed
}

export const typingNotificationParser: Parser<
t.TypeOf<typeof typingNotificationSchema>
> = (payload: Buffer) => thriftReadToObject(payload, typingNotificationSchema)
export const sendMessageResponseParser: Parser<
t.TypeOf<typeof sendMessageResponseSchema>
> = (payload: Buffer) => thriftReadToObject(payload, sendMessageResponseSchema)

export const markThreadResponseParser: Parser<
t.TypeOf<typeof markThreadResponseSchema>
> = (payload: Buffer) => thriftReadToObject(payload, markThreadResponseSchema)

export const deliveryReceiptBatchResponseParser: Parser<
t.TypeOf<typeof deliveryReceiptBatchResponseSchema>
> = (payload: Buffer) =>
  thriftReadToObject(payload, deliveryReceiptBatchResponseSchema)

export type FlattenedCreateGroupResponse = Omit<
t.TypeOf<typeof createGroupResponseSchema>,
'mutation_response'
> & { mutation_response: t.TypeOf<typeof groupCreationMutationResponseSchema> }

export const createGroupResponseParser: Parser<FlattenedCreateGroupResponse> = (
  payload: Buffer,
) => {
  const { mutation_response, ...rest } = thriftReadToObject(
    payload,
    createGroupResponseSchema,
  )
  const parsed_mutation_response = thriftReadToObject(
    mutation_response,
    groupCreationMutationResponseSchema,
  )
  return { ...rest, mutation_response: parsed_mutation_response }
}

export const addParticipantsToGroupResponseParser: Parser<
t.TypeOf<typeof addParticipantsToGroupResponseSchema>
> = (payload: Buffer) =>
  thriftReadToObject(payload, addParticipantsToGroupResponseSchema)

export const requestStreamResponseParser: Parser<
| t.TypeOf<typeof requestStreamPresenceUpdateResponseUnwrappedSchema>
| undefined
> = (payload: Buffer) => {
  const { payload: wrappedPayload = [] } = thriftReadToObject(
    payload,
    requestStreamPresenceUpdateResponseSchema,
  )
  if (wrappedPayload.length > 0) {
    if (wrappedPayload.length > 1) {
      parserDebug('Unknown payload received')
      // parserDebug(new BufferReader(payload).pretty_print())
      return undefined
    }
    if (wrappedPayload[0].nested?.presenceUpdateData) {
      return thriftReadToObject(
        wrappedPayload[0].nested.presenceUpdateData,
        requestStreamPresenceUpdateResponseUnwrappedSchema,
      )
    }
    return undefined
  }
  return undefined
}
