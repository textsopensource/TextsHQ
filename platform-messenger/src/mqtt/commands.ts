import {
  thriftWriteFromObject,
  t,
  BufferReader,
} from '@f0rr0/thrift-compact-protocol'
import { MqttMessage } from 'mqtts'
import { MQTToTClient } from './mqttot'
import {
  sendAdditionalContactsSchema,
  deliveryReceiptBatchSchema,
  sendThreadPresenceSchema,
  sendTypingSchema,
  markThreadSchema,
  sendMessageSchema,
  createGroupSchema,
  addParticipantsToGroupSchema,
  deliveryReceiptBatchResponseSchema,
  sendMessageResponseSchema,
  createGroupResponseSchema,
  addParticipantsToGroupResponseSchema,
  ClientAppSettings,
  requestStreamCreatePresenceStreamSchema,
  requestStreamAmendPresenceStreamRequestSchema,
  typingNotificationSchema,
} from '../lib/descriptors'
import { AndroidState } from '../lib/state'
import { AppState } from '../lib/common'
import RequestResponseOverMQTT from './request-respose-mqtt'
import { PublishTopics } from './constants'
import { emptyTracingHeader } from './shared'
import { FlattenedMessageSyncPayload } from './parsers'

export default class Commands {
  protected mqtt: MQTToTClient

  protected state: AndroidState

  public requestResponseState: RequestResponseOverMQTT

  public constructor(
    mqtt: MQTToTClient,
    state: AndroidState,
    requestResponseState: RequestResponseOverMQTT,
  ) {
    this.mqtt = mqtt
    this.state = state
    this.requestResponseState = requestResponseState
  }

  protected async sendCommand(message: MqttMessage): Promise<void> {
    await this.mqtt.mqttotPublish(message)
  }

  public async sendAdditionalContacts(
    data: t.TypeOf<typeof sendAdditionalContactsSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.SEND_ADDITIONAL_CONTACTS.id,
      payload: thriftWriteFromObject(data, sendAdditionalContactsSchema),
      qosLevel: 0,
    })
  }

  public async createPresenceRequestStream(
    data: t.TypeOf<typeof requestStreamCreatePresenceStreamSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.REQUEST_STREAM_REQUEST.id,
      payload: thriftWriteFromObject(
        data,
        requestStreamCreatePresenceStreamSchema,
      ),
      qosLevel: 1,
    })
  }

  public async amendPresenceRequestStreamWithAdditionalContacts(
    data: t.TypeOf<typeof requestStreamAmendPresenceStreamRequestSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.REQUEST_STREAM_REQUEST.id,
      payload: thriftWriteFromObject(
        data,
        requestStreamAmendPresenceStreamRequestSchema,
      ),
      qosLevel: 1,
    })
  }

  public async sendThreadPresence(
    data: t.TypeOf<typeof sendThreadPresenceSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.SEND_THREAD_PRESENCE.id,
      payload: thriftWriteFromObject(data, sendThreadPresenceSchema),
      qosLevel: 0,
    })
  }

  public async sendThreadTyping(
    data: t.TypeOf<typeof sendTypingSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.SEND_TYPING.id,
      payload: thriftWriteFromObject(data, sendTypingSchema),
      qosLevel: 0,
    })
  }

  public async sendTyping(
    data: t.TypeOf<typeof sendTypingSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.SEND_TYPING.id,
      payload: thriftWriteFromObject(data, sendTypingSchema),
      qosLevel: 0,
    })
  }

  public async sendDeliveryReceiptBatch(
    data: t.TypeOf<typeof deliveryReceiptBatchSchema>,
  ) {
    await this.sendCommand({
      topic: PublishTopics.DELIVERY_RECEIPT_BATCH.id,
      payload: thriftWriteFromObject(data, deliveryReceiptBatchSchema),
      qosLevel: 1,
    })
    return this.requestResponseState.registerRequest<
    t.TypeOf<typeof deliveryReceiptBatchResponseSchema>
    >(PublishTopics.DELIVERY_RECEIPT_BATCH, data.batchId.toString())
  }

  public async markThread(
    data: t.TypeOf<typeof markThreadSchema>,
  ): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.MARK_THREAD.id,
      payload: Buffer.concat([
        Buffer.from([0]),
        thriftWriteFromObject(data, markThreadSchema),
      ]),
      qosLevel: 1,
    })
  }

  public async sendMessage(data: t.TypeOf<typeof sendMessageSchema>) {
    await this.sendCommand({
      topic: PublishTopics.SEND_MESSAGE.id,
      payload: Buffer.concat([
        emptyTracingHeader,
        thriftWriteFromObject(data, sendMessageSchema),
      ]),
      qosLevel: 1,
    })
    return this.requestResponseState.registerRequest<
    FlattenedMessageSyncPayload,
    t.TypeOf<typeof sendMessageSchema>
    >(PublishTopics.SEND_MESSAGE, data.offlineThreadingId.toString(), data)
  }

  public async createGroup(data: t.TypeOf<typeof createGroupSchema>) {
    await this.sendCommand({
      topic: PublishTopics.CREATE_GROUP.id,
      payload: thriftWriteFromObject(data, createGroupSchema),
      qosLevel: 1,
    })
    return this.requestResponseState.registerRequest<
    t.TypeOf<typeof createGroupResponseSchema>
    >(PublishTopics.CREATE_GROUP, data.offline_threading_id.toString())
  }

  public async addParticipantsToGroup(
    data: t.TypeOf<typeof addParticipantsToGroupSchema>,
  ) {
    await this.sendCommand({
      topic: PublishTopics.ADD_PARTICIPANTS_TO_GROUP.id,
      payload: thriftWriteFromObject(data, addParticipantsToGroupSchema),
      qosLevel: 1,
    })
    return this.requestResponseState.registerRequest<
    t.TypeOf<typeof addParticipantsToGroupResponseSchema>
    >(PublishTopics.ADD_PARTICIPANTS_TO_GROUP, data.threadId.toString())
  }

  public async toggleAppForeground(foreground: boolean): Promise<void> {
    await this.sendCommand({
      topic: PublishTopics.LIGHTSPEED_REQUEST.id,
      payload: Buffer.from(
        JSON.stringify({
          label: '1',
          payload: {
            app_state: foreground ? AppState.FOREGROUND : AppState.BACKGROUND,
            request_id: 'android_request_id',
          },
          version: this.state.application.version_id,
        }),
      ),
      qosLevel: 1,
    })
  }

  public async toggleAppActiveStatus(data: ClientAppSettings) {
    await this.sendCommand({
      topic: '',
      payload: Buffer.from(JSON.stringify(data)),
      qosLevel: 0,
    })
  }

  public async diffMessageSyncQueue(sync_sequence_id: string, sync_token: string): Promise<void> {
    await this.mqtt.mqttotPublish({
      topic: '/messenger_sync_get_diffs',
      payload: Buffer.from(
        JSON.stringify({
          last_seq_id: sync_sequence_id,
          sync_token,
        }),
      ),
      qosLevel: 1 })
  }

  public async createMessageSyncQueue(sync_sequence_id: string): Promise<void> {
    await this.mqtt.mqttotPublish({
      topic: '/messenger_sync_create_queue',
      payload: Buffer.from(
        JSON.stringify({
          initial_titan_sequence_id: Number(sync_sequence_id),
          delta_batch_size: 125,
          device_params: {
            image_sizes: {
              0: '4096x4096',
              4: '358x358',
              1: '750x750',
              2: '481x481',
              3: '358x358',
            },
            animated_image_format: 'WEBP,GIF',
            animated_image_sizes: {
              0: '4096x4096',
              4: '358x358',
              1: '750x750',
              2: '481x481',
              3: '358x358',
            },
            thread_theme_background_sizes: {
              0: '2048x2048',
            },
            thread_theme_icon_sizes: {
              1: '138x138',
              3: '66x66',
            },
            thread_theme_reaction_sizes: {
              1: '83x83',
              3: '39x39',
            },
          },
          entity_fbid: this.state.session.uid,
          sync_api_version: 10,
          queue_params: {
            client_delta_sync_bitmask: 'H/p8Ym+r0YAFf7LNgA',
            graphql_query_hashes: {
              xma_query_id: '3564170710353387',
            },
            graphql_query_params: {
              3564170710353387: {
                xma_id: '<ID>',
                small_preview_width: 716,
                small_preview_height: 358,
                large_preview_width: 1500,
                large_preview_height: 750,
                full_screen_width: 4096,
                full_screen_height: 4096,
                blur: 0,
                nt_context: {
                  styles_id: '2c223dbb808ad6e5a12a84d5206cdced',
                  pixel_ratio: 3,
                },
                use_oss_id: true,
              },
            },
          },
        }),
      ),
      qosLevel: 1,
    })
  }
}
