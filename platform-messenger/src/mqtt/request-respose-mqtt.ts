import { texts } from '@textshq/platform-sdk'
import { PublishTopicsIdMap } from './constants'
import { RequestResponseOverMQTTTimeoutError } from './errors'

export default class RequestResponseOverMQTT {
  protected promises: Record<
  string,
  Record<string, ResolvablePromise<any, any>>
  > = {}

  register(topics: string[]) {
    topics.forEach(topic => {
      if (!this.promises[topic]) {
        this.promises[topic] = {}
      }
    })
  }

  registerRequest<R, E = never>(
    topic: typeof PublishTopicsIdMap[string],
    id: string,
    extra: E = undefined,
    timeoutMs = 60000,
  ) {
    if (!this.promises[topic.id]) {
      this.register([topic.id])
    }
    const resolvablePromise = createResolvablePromise<R, E>(timeoutMs, extra)
    this.promises[topic.id][id] = resolvablePromise as any
    return resolvablePromise.promise
  }

  handleResponse(topic: string, id: string, response: any) {
    if (this.promises[topic] && this.promises[topic][id]) {
      this.promises[topic][id].resolve({
        data: response,
        extra: this.promises[topic][id].extra,
      })
    } else {
      texts.log(`No pending promises for topic:${topic} - id:${id}`)
    }
  }

  clearRequest(topic: string, id: string) {
    if (this.promises[topic] && this.promises[topic][id]) {
      delete this.promises[topic][id]
    }
  }
}

export interface ResolvablePromise<T, E> {
  promise: Promise<T>
  extra: E
  resolve: (response: T) => void
}

export const createResolvablePromise = <T, E = never>(
  timeout: number,
  extra: E = undefined,
): ResolvablePromise<{ data: T, extra: E }, E> => {
  const timeoutPromise = new Promise<never>((_, reject) => {
    setTimeout(() => reject(new RequestResponseOverMQTTTimeoutError()), timeout)
  })

  let promiseResolveFn: ((value: { data: T, extra: E }) => void) | undefined

  const externalResolver = (response: { data: T, extra: E }) => {
    if (promiseResolveFn) {
      promiseResolveFn(response)
    }
  }

  const resolvablePromise = new Promise<{ data: T, extra: E }>(resolve => {
    promiseResolveFn = resolve
  })

  return {
    promise: Promise.race([timeoutPromise, resolvablePromise]),
    resolve: externalResolver,
    extra,
  }
}
