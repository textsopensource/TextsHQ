class BaseError extends Error {
  constructor(message?: string) {
    super(message)
    // @ts-expect-error -- set the name to the class's actual name
    this.name = this.__proto__.constructor.name
  }
}

export class ClientDisconnectedError extends BaseError {}

export class EmptyPacketError extends BaseError {}

export class ConnectionFailedError extends BaseError {}

export class RequestResponseOverMQTTTimeoutError extends BaseError {}
