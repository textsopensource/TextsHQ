import React from 'react'
import type { AuthProps } from '@textshq/platform-sdk'

const auth: React.FC<AuthProps> = ({ login, api, children }) => {
  React.useEffect(() => {
    api!.onLoginEvent(done => {
      if (done) login()
    })
  }, [api, login])

  return (
    <div>
      {children}
      <footer>
        <div>Facebook may lock your account since it doesn't recognize Texts app and make you reset your password.</div>
        <div>You'll also get a login notification from Facebook saying Texts has logged in as a Google Pixel 2. There is no physical/simulated device used but how Texts identifies itself. All data stays local.</div>
      </footer>
    </div>
  )
}

export default auth
