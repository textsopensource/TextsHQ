import { randomBytes, publicEncrypt, createCipheriv, constants } from 'crypto'
import { texts } from '@textshq/platform-sdk'
import BaseAndroidAPI from './base'
import { LoginResponse, MobileConfigResponse } from '../lib/login'
import {
  isIncorrectPasswordError,
  isInvalidEmailError,
  isOAuthExceptionError,
  isTwoFactorRequiredError,
} from '../lib/errors'
import { objPropIfTruthyValue, objPropIfExclusiveValue } from '../lib/utils'

export default class LoginAPI extends BaseAndroidAPI {
  approvalTimer?: NodeJS.Timeout

  updatePasswordEncryption(pubKey: string, keyId: number, isMSGR2 = true) {
    this.state.session.password_encryption_pubkey = pubKey
    this.state.session.password_encryption_key_id = keyId
    this.state.session.password_encryption_prefix = `#PWD_MSGR:${isMSGR2 ? '2' : '1'}`
    this.state.session.seconds_to_live = new Date().getSeconds() + 1000
  }

  async mobile_config_sessionless() {
    const req_data = {
      ...this._params,
      query_hash: '8da87e6b43dd4ad3b10a29f975439527068855d1d539bf1451e2ff1a303acb0e',
      one_query_hash: '47a537df9564077c3ae6f7eb16a497d7474219cbc8aa70b0a3cd5e4721d2b8af&',
      bool_opt_policy: '3',
      value_hashes: '[]',
      device_id: this.state.device.uuid,
      ts: ~~(Date.now() / 1000),
      batch_size: 400,
      bln_li_hashes: '[]',
      api_version: '8',
      fetch_type: 'ASYNC_FULL',
      unit_type: '1',
      access_token: this.state.application.access_token,
      client_country_code: 'US',
      locale: 'en_US',
    }
    const response = await this.post<MobileConfigResponse>(
      `${this.b_graph_url}/mobileconfigsessionless`,
      {
        form: req_data,
        headers: this._headers,
      },
    )
    try {
      if (response?.data) {
        const { configs } = response.data
        const config = configs['15712']
        if (config?.fields) {
          this.updatePasswordEncryption(config.fields.find(f => f.k === 2).str, config.fields.find(f => f.k === 1).i64, false)
        }
      }
    } catch {
      texts.Sentry.captureMessage('Error getting configurations from mobile_config_sessionless')
    }
  }

  get _jazoest() {
    const buf = Buffer.from(this.state.device.uuid!, 'ascii')
    let sum = 0
    for (let i = 0; i < buf.byteLength; i++) {
      sum += buf.readUInt8(i)
    }
    return `2${sum}`
  }

  encryptPassword(password: string) {
    const randKey = randomBytes(32)
    const iv = randomBytes(12)

    const rsaEncrypted = publicEncrypt(
      {
        key: this.state.session.password_encryption_pubkey,
        padding: constants.RSA_PKCS1_PADDING,
      },
      randKey,
    )
    const cipher = createCipheriv('aes-256-gcm', randKey, iv)
    const time = Math.floor(Date.now() / 1000).toString()
    cipher.setAAD(Buffer.from(time))
    const aesEncrypted = Buffer.concat([
      cipher.update(password, 'utf8'),
      cipher.final(),
    ])
    const sizeBuffer = Buffer.alloc(2, 0)
    sizeBuffer.writeInt16LE(rsaEncrypted.byteLength, 0)
    const authTag = cipher.getAuthTag()
    const encrypted = Buffer.concat([
      Buffer.from([1, this.state.session.password_encryption_key_id]),
      iv,
      sizeBuffer,
      rsaEncrypted,
      authTag,
      aesEncrypted,
    ]).toString('base64')
    return `${this.state.session.password_encryption_prefix}:${time}:${encrypted}`
  }

  async _login(
    email: string,
    password: string,
    extra?: Record<string, string>,
  ) {
    const req = {
      // try to keep everything in sync with official app inc. order
      // should help prevent locked account
      email,
      password,
      credentials_type: 'password',
      error_detail_type: 'button_with_disabled',
      format: 'json',
      ...objPropIfTruthyValue('device_id', this.state.device.uuid),
      generate_session_cookies: '1',
      generate_analytics_claim: '1',
      ...objPropIfTruthyValue('machine_id', this.state.session.machine_id),
      method: 'POST',
      ...(extra || {}),
    }

    const req_data = this.sign(req, {
      access_token: this.state.application.access_token,
    })

    const headers = {
      ...this._headers,
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    const response = await this.post<LoginResponse>(
      `${this.a_graph_url}/auth/login`,
      {
        body: this.urlEncode(req_data),
        headers,
      },
    )

    this.state.session.access_token = response.data.access_token
    this.state.session.uid = response.data.uid
    this.state.session.machine_id = response.data.machine_id
    this.state.session.login_first_factor = undefined
    return response
  }

  async login_2fa(email: string, code: string) {
    clearInterval(this.approvalTimer)
    try {
      if (!this.state.session.login_first_factor) {
        throw new Error('No two-factor login in progress')
      }
      const response = await this._login(email, code, {
        twofactor_code: code,
        userid: `${this.state.session.uid}`,
        first_factor: this.state.session.login_first_factor,
        credentials_type: 'two_factor',
      })
      return response
    } catch (error) {
      const data = error?.response?.data?.error
      if (isIncorrectPasswordError(data)) {
        throw data
      } else if (isOAuthExceptionError(data)) {
        throw data
      } else {
        throw error
      }
    }
  }

  async login_approved() {
    if (!this.state.session.transient_auth_token) {
      throw new Error('No two-factor login in progress')
    }
    const response = await this._login(
      `${this.state.session.uid}!`,
      this.state.session.transient_auth_token!,
      {
        credentials_type: 'transient_token',
      },
    )
    return response
  }

  async check_approved_machine() {
    const req_data = {
      u: `${this.state.session.uid!}`,
      m: this.state.session.machine_id!,
      ...this._params,
      method: 'GET',
      fb_api_req_friendly_name: 'checkApprovedMachine',
      fb_api_caller_class:
        'TwoFacServiceHandler',
      access_token: this.state.application.access_token!,
    }
    const headers = {
      ...this._headers,
      'content-type': 'application/x-www-form-urlencoded',
      'x-fb-friendly-name': req_data.fb_api_req_friendly_name,
    }
    const response = await this.post(
      `${this.a_graph_url}/check_approved_machine`,
      { body: this.urlEncode(req_data), headers },
    )
    return response?.data?.data
  }

  async login(email: string, password: string, cb: () => void) {
    try {
      const response = await this._login(email, password)
      return response
    } catch (error) {
      texts.log(JSON.stringify(error, null, 2))
      const data = error?.response?.data?.error

      if (isTwoFactorRequiredError(data)) {
        // two-factor
        this.state.session.machine_id = data.error_data.machine_id
        this.state.session.uid = data.error_data.uid
        this.state.session.login_first_factor = data.error_data.login_first_factor
        this.state.session.transient_auth_token = data.error_data.auth_token

        clearInterval(this.approvalTimer)
        this.approvalTimer = setInterval(async () => {
          try {
            const approval: any[] = await this.check_approved_machine()
            const was_approved = approval.find(obj => obj.approved !== undefined)?.approved
            if (was_approved) {
              clearInterval(this.approvalTimer)
              try {
                const response = await this.login_approved()
                this.state.session.access_token = response.data.access_token
                this.state.session.uid = response.data.uid
                this.state.session.machine_id = response.data.machine_id
                this.state.session.login_first_factor = undefined
                cb?.()
              } catch (after_approval_error) {
                const afterApprovalErrorData = after_approval_error?.response?.data?.error
                if (isTwoFactorRequiredError(afterApprovalErrorData)) {
                  console.log(
                    'Login approved from another device, but Facebook decided that you need to enter the 2FA code anyway.',
                  )
                } else {
                  throw after_approval_error
                }
              }
            }
          } catch (approval_error) {
            clearInterval(this.approvalTimer)
            throw new Error(
              `Error checking if login was approved from another device: ${JSON.stringify(
                approval_error.response.data,
              )}`,
            )
          }
        }, 5_000)
        // Throw 2FA error so that it can be handled by platform-sdk
        throw data
      } else if (isIncorrectPasswordError(data)) {
        throw data
      } else if (isInvalidEmailError(data)) {
        throw data
      } else if (isOAuthExceptionError(data)) {
        throw data
      } else {
        throw error
      }
    }
  }

  clear_login_state() {
    // this.state.session.password_encryption_pubkey = undefined
    // this.state.session.password_encryption_key_id = undefined
    this.state.session.access_token = undefined
    this.state.session.uid = undefined
    this.state.session.machine_id = undefined
    this.state.session.login_first_factor = undefined
    this.state.session.transient_auth_token = undefined
  }

  async logout() {
    const response = await this.post<boolean>(
      `${this.b_graph_url}/auth/expire_session`,
      {
        body: this.urlEncode({
          locale: 'en_US',
          client_country_code: 'US',
          fb_api_req_friendly_name: 'logout',
          fb_api_caller_class: 'AuthOperations',
        }),
        headers: this._headers,
      },
    )
    this.clear_login_state()
    return response.data
  }
}
