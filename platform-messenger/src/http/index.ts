import { texts } from '@textshq/platform-sdk'
import FormData from 'form-data'

import {
  downloadImageFragmnet,
  fbIdToCursorQuery,
  fetchStickersWithPreviewsQuery,
  messageReactionMutation,
  messageUndoSend,
  moreMessagesQuery,
  searchEntitiesNamedQuery,
  subsequentMediaQuery,
  threadListQuery,
  threadQuery,
  moreThreadsQuery,
  usersQuery,
  muteThreadQuery,
  AddParticipantsToGroupQuery,
  RemoveParticipantsFromGroupQuery,
  removeParticipantsFromGroupQuery,
  addParticipantsToGroupQuery,
} from '../lib/graphql/queries'
import {
  GetUsersResponse,
  ImageFragment,
  MessageList,
  MessageUnsendResponse,
  OwnInfo,
  PageInfo,
  SearchEntitiesResponse,
  StickerPreviewResponse,
  SubsequentMediaResponse,
  ThreadListResponse,
  ThreadQueryResponse,
} from '../lib/graphql/responses'
import LoginAPI from './login'

export default class AndroidAPI extends LoginAPI {
  async fetch_thread_list(q?: Parameters<typeof threadListQuery>[0]) {
    const response = await this.graphql(threadListQuery(q))
    return response?.data?.data?.viewer?.message_threads as ThreadListResponse
  }

  async fetch_more_threads(q: Parameters<typeof moreThreadsQuery>[0]) {
    const response = await this.graphql(moreThreadsQuery(q))
    return response?.data?.data?.viewer?.message_threads as ThreadListResponse
  }

  async fetch_thread_info(q: Parameters<typeof threadQuery>[0]) {
    const response = await this.graphql(threadQuery(q))
    return response?.data?.data as ThreadQueryResponse
  }

  async fetch_messages(q: Parameters<typeof moreMessagesQuery>[0]) {
    const response = await this.graphql(moreMessagesQuery(q))
    return response?.data?.data?.message_thread?.messages as MessageList
  }

  async fetch_stickers(
    q: Parameters<typeof fetchStickersWithPreviewsQuery>[0],
  ) {
    const response = await this.graphql(fetchStickersWithPreviewsQuery(q))
    return response?.data?.data as StickerPreviewResponse
  }

  // Convenience method
  async fetch_single_sticker_url(stickerId: string) {
    const stickerResponse = await this.fetch_stickers({
      sticker_ids: [stickerId],
    })
    if (stickerResponse?.nodes?.length == 1) {
      const [sticker] = stickerResponse.nodes
      return (
        sticker?.animated_image?.uri
        || sticker?.thread_image?.uri
        || sticker?.preview_image?.uri
        || ''
      )
    }
  }

  async unsend(q: Parameters<typeof messageUndoSend>[0]) {
    const response = await this.graphql(messageUndoSend(q))
    return response?.data?.data?.message_undo_send as MessageUnsendResponse
  }

  async react(q: Parameters<typeof messageReactionMutation>[0]) {
    const response = await this.graphql(messageReactionMutation(q))
    return response
  }

  async fetch_image(q: Parameters<typeof downloadImageFragmnet>[0]) {
    const response = await this.graphql(downloadImageFragmnet(q))
    return response?.data?.data?.node as ImageFragment
  }

  async fbid_to_cursor(q: Parameters<typeof fbIdToCursorQuery>[0]) {
    const response = await this.graphql(fbIdToCursorQuery(q))
    return response?.data?.message_thread?.message_shared_media
      ?.page_info as PageInfo
  }

  async media_query(q: Parameters<typeof subsequentMediaQuery>[0]) {
    const response = await this.graphql(subsequentMediaQuery(q))
    return response?.data?.data?.message_thread
      ?.mediaResult as SubsequentMediaResponse
  }

  async search(q: Parameters<typeof searchEntitiesNamedQuery>[0]) {
    const response = await this.graphql(searchEntitiesNamedQuery(q))
    return response?.data?.data?.entities_named as SearchEntitiesResponse
  }

  async get_self() {
    if (this.state.session.uid) {
      const response = await this.get(`${this.a_graph_url}/${this.state.session.uid}`)
      return response?.data as OwnInfo
    }
  }

  async register_push_tokens(token: string, register = true) {
    const body = new FormData()
    body.append('format', 'json')

    if (register) {
      body.append('return_structure', 1)
      // body.append('subtype', 0) unknown
      body.append('protocol_params', JSON.stringify({
        url: 'https://fcm.googleapis.com/fcm/send',
        token,
        device_id: this.state.device.uuid,
      }))
    } else {
      body.append('token', token)
    }

    body.append('locale', 'en_US')
    body.append('client_country_code', 'US')
    body.append('fb_api_req_friendly_name', register ? 'registerPush' : 'unregisterPush')
    body.append('fb_api_caller_class', 'FacebookPushServerRegistrar')

    await this.post(`${this.b_graph_url}/me/${register ? '' : 'un'}register_push_tokens`, {
      body,
      headers: this._headers,
    })
  }

  async get_users(q: Parameters<typeof usersQuery>[0]) {
    const response = await this.graphql(usersQuery(q))
    return response?.data?.data as GetUsersResponse
  }

  async mute_thread(threadID: string, mutedUntil: Date | 'forever' | undefined) {
    const response = await this.modifyThread(muteThreadQuery({
      mute_until: mutedUntil ? '-1' : '0',
    }, threadID))
    return response?.data
  }

  async modify_participant(threadID: string, participantID: string, remove: boolean) {
    const queryParams = {
      id: `t_${threadID}`,
      to: [participantID],
    }
    const query: AddParticipantsToGroupQuery | RemoveParticipantsFromGroupQuery = remove
      ? removeParticipantsFromGroupQuery(queryParams) : addParticipantsToGroupQuery(queryParams)
    await this.updateGroupParticipants(query)
  }

  async create_group(participantIds: string[]) {
    const form = {
      format: 'json',
      method: 'POST',
      fb_api_req_friendly_name: 'createGroup',
      ...this._params,
      recipients: JSON.stringify(participantIds.map(userId => ({
        type: 'id',
        id: userId,
      }))),
    }

    await this.post(`${this.b_graph_url}/me/group_threads`, {
      form,
      headers: this._headers,
    })
  }
}
