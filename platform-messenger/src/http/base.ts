import { FetchOptions, ReAuthError, texts } from '@textshq/platform-sdk'
import FormData from 'form-data'
import clone from 'clone-deep'
import Chance from 'chance'
import { createHash, randomBytes } from 'crypto'

import {
  AddAdminsToGroupQuery,
  AddParticipantsToGroupQuery,
  GraphQLMutation,
  GraphQLQuery,
  ModifyThreadQuery,
  RemoveAdminsFromGroupQuery,
  RemoveParticipantsFromGroupQuery,
} from '../lib/graphql/queries'
import { AndroidState } from '../lib/state'
import { JsonObject } from '../lib/common'
import { isStatusCodeError } from './utils'

export interface SendMediaRequest {
  data: Buffer
  fileName: string
  mimetype: string
  offlineThreadingId: string
  timestamp: number
  chatId?: string
  isGroup?: boolean
  replyTo?: string
}

export default class BaseAndroidAPI {
  // Seems to be a per-minute request identifier
  private _cid?: string

  private _cid_ts: number

  private freeze_cid: boolean

  // Seems to be a per-session request identifier
  private nid: string

  // Seems to be a per-request incrementing integer
  private _tid: number

  public a_url = 'https://api.facebook.com'

  public b_url = 'https://b-api.facebook.com'

  public a_graph_url = 'https://graph.facebook.com'

  public b_graph_url = 'https://b-graph.facebook.com'

  public rupload_url = 'https://rupload.facebook.com'

  protected state: AndroidState

  protected _file_url_cache: Record<string, any>

  protected http = texts.createHttpClient()

  constructor(state: AndroidState) {
    this.state = state

    this._cid_ts = 0
    this.freeze_cid = false
    this.nid = randomBytes(9).toString('base64')
    this._tid = 0

    this._file_url_cache = {}
  }

  get tid() {
    this._tid += 1
    return this._tid
  }

  get cid() {
    const new_ts = Math.round(new Date().getTime() / (1000 * 60))

    if (!this._cid || (this._cid_ts != new_ts && !this.freeze_cid)) {
      this._cid_ts = new_ts
      const chance = new Chance(`${this.state.device.uuid}${new_ts}`)
      this._cid = chance.hash({ length: 32, casing: 'lower' })
    }

    return this._cid
  }

  get session_id() {
    return `nid=${this.nid};pid=Main;tid=${this.tid};nc=0;fc=0;bc=0,cid=${this.cid}`
  }

  urlEncode = (params: JsonObject) => {
    const stringified = (
      o: JsonObject,
    ): Record<string, string | number | boolean> =>
      Object.entries(o).reduce(
        (prev, [key, val]) => ({
          ...prev,
          ...(val != null && val !== undefined
            ? { [key]: typeof val === 'object' ? JSON.stringify(val) : val }
            : {}),
        }),
        {},
      )
    return Object.entries(stringified(params))
      .map(
        ([key, val]) => `${encodeURIComponent(key)}=${encodeURIComponent(val)}`,
      )
      .join('&')
  }

  sign(req: Record<string, string>, extra?: Record<string, any>) {
    const cloned = clone(req)
    const sig_data = Object.entries(cloned)
      .sort(([a], [b]) => a.localeCompare(b))
      .reduce((prev, [key, val]) => `${prev}${key}=${val}`, '')
    const sig_data_bytes = sig_data + this.state.application.client_secret
    cloned.sig = createHash('md5').update(sig_data_bytes).digest('hex')
    const merged = Object.assign(cloned, extra)
    return merged
  }

  get _headers(): Record<string, string> {
    return Object.fromEntries(
      Object.entries({
        'X-Fb-Connection-Quality': this.state.device.connection_quality,
        'X-Fb-Connection-Type': this.state.device.connection_type,
        'User-Agent': this.state.http_user_agent,
        'X-Tigon-Is-Retry': 'False',
        'X-Fb-Http-Engine': 'Liger',
        'X-Fb-Client-Ip': 'True',
        'X-Fb-Connection-Token': this.cid,
        'X-Fb-Session-Id': this.session_id,
        'X-Fb-Device-Group': this.state.device.device_group,
        'X-Fb-SIM-HNI': String(this.state.carrier.hni),
        'X-Fb-Net-HNI': String(this.state.carrier.hni),
        Authorization: this.state.session.access_token
          ? `OAuth ${this.state.session.access_token}`
          : null,
      }).filter(([, val]) => !!val),
    )
  }

  get _params() {
    return {
      locale: this.state.device.language,
      client_country_code: this.state.device.country_code,
    }
  }

  async get<ResponseType = any>(
    url: string,
    include_auth = true,
    config: FetchOptions = {},
  ) {
    const { headers: extraHeaders, ...restConfig } = config
    const headers: Record<string, string> = {
      ...(extraHeaders || {}),
      ...this._headers,
    }

    if (url.endsWith('.facebook.com') || !include_auth) {
      delete headers.authorization
    }

    const res = await this.http.requestAsString(url, {
      headers,
      method: 'GET',
      ...restConfig,
    })

    const body = JSON.parse(res.body || '{}')
    const isError = isStatusCodeError(res.statusCode) || body?.error

    /* {
      error: {
        message: 'Error validating access token: The session has been invalidated because the user changed their password or Facebook has changed the session for security reasons.',
        type: 'OAuthException',
        code: 190,
        error_subcode: 452,
        fbtrace_id: 'AK-t1TcsFegc4m1KMxmIXQ_'
      }
    } */
    if (isError) {
      if (body?.error?.type === 'OAuthException') throw new ReAuthError(body?.error?.message)
      throw { response: { data: { error: body?.error } } }
    }
    if (!res.body?.length) return {}

    return { data: JSON.parse(res.body) as ResponseType }
  }

  async post<ResponseType = any>(url: string, config: FetchOptions = {}) {
    const res = await this.http.requestAsString(url, {
      method: 'POST',
      ...config,
    })

    const body = JSON.parse(res.body || '{}')
    const isError = isStatusCodeError(res.statusCode) || body?.error

    if (isError) throw { response: { data: { error: body?.error } } }
    if (!res.body?.length) return {}

    return {
      statusCode: res.statusCode,
      data: JSON.parse(res.body) as ResponseType,
    }
  }

  async graphql(
    req: GraphQLQuery | GraphQLMutation,
    config: FetchOptions = {},
    b = false,
  ) {
    const { headers: extraHeaders, ...restConfig } = config
    const headers: Record<string, string> = {
      ...(extraHeaders || {}),
      ...this._headers,
      'content-type': 'application/x-www-form-urlencoded',
      'X-FB-Friendly-Name': req._hash_key_,
    }

    let variables = req._serialize_()

    if (req._kind_ === 'GraphQLMutation') {
      variables = { input: variables }
    }

    const params = {
      ...this._params,
      variables,
      method: 'post',
      doc_id: req._doc_id_,
      format: 'json',
      pretty: false,
      fb_api_req_friendly_name: req._hash_key_,
      fb_api_caller_class: req._caller_class_,
    }

    return this.post(`${b ? this.b_graph_url : this.a_graph_url}/graphql`, {
      body: this.urlEncode(params),
      headers,
      ...restConfig,
    })
  }

  async updateGroupAdmins(
    req: AddAdminsToGroupQuery | RemoveAdminsFromGroupQuery,
    config: FetchOptions = {},
    b = false,
  ) {
    const { headers: extraHeaders, ...restConfig } = config
    const headers: Record<string, string> = {
      ...(extraHeaders || {}),
      ...this._headers,
      'content-type': 'application/x-www-form-urlencoded',
      'X-FB-Friendly-Name': req._hash_key_,
    }

    const variables = req._serialize_()

    const params = {
      ...this._params,
      ...variables,
      fb_api_req_friendly_name: req._hash_key_,
      fb_api_caller_class: req._caller_class_,
    }

    return this.post(
      `${b ? this.b_graph_url : this.a_graph_url}/graphql/t_${
        req._thread_id_
      }/admins`,
      {
        body: this.urlEncode(params),
        headers,
        ...restConfig,
      },
    )
  }

  async updateGroupParticipants(
    req: AddParticipantsToGroupQuery | RemoveParticipantsFromGroupQuery,
    config: FetchOptions = {},
    b = false,
  ) {
    const { headers: extraHeaders, ...restConfig } = config
    const headers: Record<string, string> = {
      ...(extraHeaders || {}),
      ...this._headers,
      'content-type': 'application/x-www-form-urlencoded',
      'X-FB-Friendly-Name': req._hash_key_,
    }

    const variables = req._serialize_()

    const params = {
      ...this._params,
      to: `t_${req.id}`,
      fb_api_req_friendly_name: req._hash_key_,
      fb_api_caller_class: req._caller_class_,
      ...variables,
    }

    return this.post(
      `${b ? this.b_graph_url : this.a_graph_url}//participants`, // double slash is intended

      {
        body: this.urlEncode(params),
        headers,
        ...restConfig,
      },
    )
  }

  async modifyThread(
    req: ModifyThreadQuery,
    config: FetchOptions = {},
    b = false,
  ) {
    const { headers: extraHeaders, ...restConfig } = config

    const headers: Record<string, string> = {
      ...(extraHeaders || {}),
      ...this._headers,
      'Content-Type': 'application/x-www-form-urlencoded',
      'X-FB-Friendly-Name': req._hash_key_,
    }

    const params = {
      include_headers: false,
      decode_body_json: false,
      streamable_json_response: true,
      ...this._params,
      // @ts-expect-error
      ...restConfig.params,
    }

    if (req._operation_._sub_kind_ === 'UpdateThreadImageQuery') {
      const {
        _hash_key_,
        relative_url,
        omit_response_on_success,
        attached_files,
        ...restOperation
      } = req._operation_

      const batchOperation = {
        omit_response_on_success,
        name: _hash_key_,
        relative_url,
        method: 'POST',
        attached_files,
        body: this.urlEncode({
          format: 'json',
          ...this._params,
          ...restOperation._serialize_(),
          fb_api_req_friendly_name: _hash_key_,
        }),
      }

      const payload = {
        batch: [batchOperation],
        fb_api_req_friendly_name: req._hash_key_,
        fb_api_caller_class: req._caller_class_,
      }

      const formData = new FormData()
      formData.append('batch', JSON.stringify(payload.batch))
      formData.append('fb_api_req_friendly_name', req._hash_key_)
      formData.append('fb_api_caller_class', req._caller_class_)
      formData.append('image', req._operation_._image_, {
        filename: req._operation_._filename_,
        contentType: 'image/jpeg',
      })

      return this.post(`${b ? this.b_graph_url : this.a_graph_url}`, {
        body: formData,
        headers: {
          ...headers,
          ...formData.getHeaders(),
        },
        searchParams: params,
        ...restConfig,
      })
    }
    const {
      _hash_key_,
      relative_url,
      omit_response_on_success,
      ...restOperation
    } = req._operation_

    const batchOperation = {
      method: 'POST',
      body: this.urlEncode({
        format: 'json',
        ...restOperation._serialize_(),
        ...this._params,
        fb_api_req_friendly_name: _hash_key_,
      }),
      name: _hash_key_,
      omit_response_on_success,
      relative_url,
    }

    const payload = this.urlEncode({
      batch: [batchOperation],
      fb_api_caller_class: req._caller_class_,
      fb_api_req_friendly_name: req._hash_key_,
    })

    // texts.log(JSON.stringify(payload, null, 2))

    return this.post(`${b ? this.b_graph_url : this.a_graph_url}`, {
      body: payload as any,
      headers,
      searchParams: params,
      ...restConfig,
    })
  }

  async sendMedia({
    data,
    fileName,
    mimetype,
    offlineThreadingId,
    chatId,
    isGroup = false,
    replyTo,
    timestamp,
  }: SendMediaRequest) {
    const headers: Record<string, string> = {
      ...this._headers,
      app_id: this.state.application.client_id,
      device_id: this.state.device.uuid!,
      attempt_id: offlineThreadingId,
      offset: '0',
      'x-entity-length': `${data.length}`,
      'x-entity-name': fileName,
      'x-entity-type': mimetype,
      'content-type': 'application/octet-stream',
      client_tags: JSON.stringify({
        trigger: '2:thread_list:thread',
        is_in_chatheads: false,
      }),
      original_timestamp: `${timestamp}`,
      'X-fb-rmd': 'state=NO_MATCH',
      'x-msgr-region': this.state.session.region_hint,
      'x-fb-friendly-name': 'post_resumable_upload_session',
      ...(chatId
        ? {
          send_message_by_server: '1',
          sender_fbid: `${this.state.session.uid!}`,
          to: isGroup ? `tfbid_${chatId}` : chatId,
          offline_threading_id: offlineThreadingId,
          ttl: '0',
        }
        : {
          send_message_by_server: '0',
          thread_type_hint: 'thread',
        }),
      ...(replyTo ? { replied_to_message_id: replyTo } : {}),
    }

    let pathType = 'messenger_file'

    if (mimetype.startsWith('image/')) {
      headers.image_type = 'FILE_ATTACHMENT'
      pathType = mimetype == 'image/gif' ? 'messenger_gif' : 'messenger_image'
    } else if (mimetype.startsWith('video/')) {
      headers.video_type = 'FILE_ATTACHMENT'
      pathType = 'messenger_video'
    } else if (mimetype.startsWith('audio/')) {
      headers.audio_type = 'VOICE_MESSAGE'
      headers.is_voicemail = '0'
      pathType = 'messenger_audio'
    } else {
      headers.file_type = 'FILE_ATTACHMENT'
    }

    const fileId = `${createHash('md5')
      .update(data)
      .digest('hex')}${offlineThreadingId}`

    const response = await this.post(
      `${this.rupload_url}/${pathType}/${fileId}`,
      {
        body: data,
        headers,
      },
    )
    return response
  }

  async get_image_url(
    message_id: string,
    attachment_id: number | string,
    preview = false,
    max_width = 384,
    max_height = 480,
  ) {
    try {
      const response = await this.http.requestAsBuffer(
        `${this.a_graph_url}/messaging_get_attachment`,
        {
          headers: {
            referer: `fbapp://${this.state.application.client_id}/messenger_thread_photo`,
            'x-fb-friendly-name': 'image',
          },
          searchParams: {
            method: 'POST',
            redirect: 'true',
            access_token: this.state.session.access_token,
            mid: `m_${message_id}`,
            aid: String(attachment_id),
            ...(preview
              ? {
                preview: '1',
                max_width,
                max_height,
              }
              : {}),
            maxRedirects: 0,
          },
        },
      )

      return response.body
    } catch (error) {
      return error?.response?.headers?.location
    }
  }
}
