export const isStatusCodeError = (status: number): boolean => status !== 304 && status >= 400
