import fs from 'fs/promises'
import path from 'path'
import {
  texts,
  PlatformAPI,
  ReAuthError,
  OnServerEventCallback,
  MessageSendOptions,
  InboxName,
  Thread,
  LoginResult,
  MessageContent,
  PaginationArg,
  ActivityType,
  LoginCreds,
  CodeRequiredReason,
  CurrentUser,
  AccountInfo,
  ServerEventType,
  ThreadFolderName,
  ServerEvent,
  GetAssetOptions,
  Message,
} from '@textshq/platform-sdk'
import { t } from '@f0rr0/thrift-compact-protocol'
import bluebird from 'bluebird'
import {
  mapMessages,
  mapThreads,
  mapSearchResultUser,
  mapParticipant,
  REACTION_MAP_TO_MESSENGER,
  REACTION_MAP_TO_NORMALIZED,
  mapMQTTMessages,
  mapUserPresence,
  mapThread,
} from './mappers'
import { stringifyWithBigInt, uuid } from './lib/utils'
import { AndroidState, generateState, hydrateState } from './lib/state'
import { OwnInfo, Participant } from './lib/graphql/responses'
import AndroidMQTT from './mqtt'
import AndroidHTTP from './http'
import {
  getThreadIdFromThread,
  MarkThreadState,
  MessageReactionAction,
  ReactionAction,
  ThreadFolder,
  // TypingState,
} from './lib/common'
import { isTwoFactorRequiredError } from './lib/errors'
import { PublishTopics, SubscribeTopics } from './mqtt/constants'
import { FlattenedMessageSyncPayload } from './mqtt/parsers'
import { sendMessageSchema } from './lib/descriptors'
import { genClientContext, PasswordKeyParams } from './util'

const MESSAGE_PAGE_SIZE = 20

export default class MessengerAPI implements PlatformAPI {
  onEvent: OnServerEventCallback = () => {}

  private loginEventCallback?: any

  private currentUser: OwnInfo & Participant

  private groupThreadsCache = new Set<string>()

  // private mqttConnectRetryCount = 0

  private loginMetadata: any = {}

  public state: AndroidState

  private mqtt: AndroidMQTT

  private http: AndroidHTTP

  private pushAuth = false

  init = async (session?: any, accountInfo?: AccountInfo) => {
    if (session) {
      if (session.savedState) {
        // Is latest version
        this.state = hydrateState(session.savedState)
        this.mqtt = new AndroidMQTT(this.state)
        this.http = new AndroidHTTP(this.state)
        await this.afterLogin()
      } else if (
        session.userCredentials?.access_token
        && session.userCredentials?.uid
        && session.deviceId
      ) {
        // is older version
        if (accountInfo?.accountID) {
          this.state = generateState(accountInfo.accountID)
        } else {
          this.state = generateState(Math.random().toString())
        }
        this.state.session.access_token = session.userCredentials.access_token
        this.state.session.uid = session.userCredentials.uid
        this.state.device.uuid = session.deviceId
        this.mqtt = new AndroidMQTT(this.state)
        this.http = new AndroidHTTP(this.state)
        await this.afterLogin()
      } else {
        // unknown version
        if (accountInfo?.accountID) {
          this.state = generateState(accountInfo.accountID)
        } else {
          this.state = generateState(Math.random().toString())
        }
        this.mqtt = new AndroidMQTT(this.state)
        throw new ReAuthError('You must login again because this integration has been rebuilt.')
      }
    } else if (accountInfo?.accountID) {
      // no previous session
      this.state = generateState(accountInfo.accountID)
      this.mqtt = new AndroidMQTT(this.state)
      this.http = new AndroidHTTP(this.state)
    } else {
      // no previous session or accountID
      this.state = generateState(Math.random().toString())
      this.mqtt = new AndroidMQTT(this.state)
      this.http = new AndroidHTTP(this.state)
    }
  }

  dispose = () => this.mqtt.disconnect()

  onLoginEvent = (onEvent: Function) => {
    this.loginEventCallback = onEvent
  }

  private onLoginApproved = () => {
    this.pushAuth = true
    this.loginEventCallback?.({ done: true })
  }

  private async loginWithUserPass(username: string, password: string) {
    try {
      await this.http.login(username, password, this.onLoginApproved)
    } catch (err) {
      const pwdKey = err?.error_data?.pwd_enc_key_pkg
      if (pwdKey) {
        texts.Sentry.captureMessage('pwd_enc_key_pkg was found on http.login error')
        this.http.updatePasswordEncryption(pwdKey.public_key, pwdKey.key_id)
        return this.loginWithUserPass(username, password)
      }
      throw err
    }
  }

  login = async ({
    username,
    password,
    lastLoginResult,
    code,
  }: LoginCreds = {}): Promise<LoginResult> => {
    try {
      if (this.pushAuth) {
        await this.afterLogin()
        return { type: 'success' }
      }
      if (lastLoginResult) {
        await this.http.login_2fa(this.loginMetadata.username, code)
      } else {
        this.http.clear_login_state()
        this.http.updatePasswordEncryption(PasswordKeyParams.pubKey, PasswordKeyParams.keyId)
        await this.http.mobile_config_sessionless()
        await this.loginWithUserPass(username, password)
      }
    } catch (error) {
      texts.error(error)
      if (isTwoFactorRequiredError(error)) {
        this.loginMetadata = { username }
        return {
          type: 'code_required',
          reason: CodeRequiredReason.TWO_FACTOR,
          title: error.error_user_msg,
        }
      }
      const humanMessage = [error.error_user_title, error.error_user_msg].filter(Boolean).join('\n')
      if (!humanMessage) {
        texts.Sentry.captureException(error)
      }
      return {
        type: 'error',
        errorMessage: humanMessage || error.message,
      }
    }
    await this.afterLogin()
    return { type: 'success' }
  }

  logout = async () => {
    clearInterval(this.http.approvalTimer)
    await this.dispose()
    await this.http.logout()
  }

  private afterLogin = async () => {
    const selfResponse = await this.http.get_self()
    const infoResponse = await this.http.get_users({ user_fbids: [selfResponse.id] })
    this.currentUser = {
      ...selfResponse,
      ...infoResponse?.messaging_actors[0],
    }
    await this.connectMQTT()
  }

  setInitialSequenceId = async () => {
    texts.log('Setting initial sequence id')
    const { sync_sequence_id } = await this.http.fetch_thread_list()
    this.mqtt.sync_sequence_id = sync_sequence_id
  }

  connectMQTT = async () => {
    if (!this.mqtt.sync_sequence_id) {
      await this.setInitialSequenceId()
    }
    try {
      await this.mqtt.connect()
      // this.mqttConnectRetryCount = 0
    } catch (error) {
      texts.error(error)
      texts.Sentry.captureException(error)
    }

    this.mqtt.on('region_hint', ({ code }) => {
      this.state.session.region_hint = code
    })

    this.mqtt.on('presence', presenceItems => {
      const events = presenceItems.map<ServerEvent>(item => ({
        type: ServerEventType.USER_PRESENCE_UPDATED,
        presence: mapUserPresence(item),
      }))
      this.onEvent(events)
    })

    this.mqtt.on('typing', typing => {
      // texts.log('mqtt.on.typing', stringifyWithBigInt(typing, 2))
      const threadID = typing.threadKey
        ? getThreadIdFromThread(typing.threadKey)
        : String(typing.sender)
      if (!typing.state) return
      const event: ServerEvent = {
        type: ServerEventType.USER_ACTIVITY,
        threadID,
        participantID: String(typing.sender),
        durationMs: 5_000,
        activityType: ActivityType.TYPING,
      }
      this.onEvent([event])
    })

    this.mqtt.on('message_sync', async message => {
      if (message.firstDeltaSeqId && message.syncToken) {
        texts.log('Received first message with sync info')
        this.mqtt.sync_token = message.syncToken ?? this.mqtt.sync_token
        this.mqtt.sync_sequence_id = message.firstDeltaSeqId?.toString() || this.mqtt.sync_sequence_id
      } else {
        this.mqtt.sync_sequence_id = message.lastIssuedSeqId?.toString() || this.mqtt.sync_sequence_id
      }

      if (!this.mqtt.sync_sequence_id) {
        texts.log('Sequence id was reset')
        await this.setInitialSequenceId()
        await this.mqtt.connect()
      }

      const mappedMessages = await mapMQTTMessages(
        message,
        this.currentUser.id,
        this.http,
      )

      // @ts-expect-error
      const serverEvents: ServerEvent[] = (
        await bluebird.map(message.items || [], async item => {
          if (item.add_member) {
            texts.log(stringifyWithBigInt(item.add_member, 2))
            const threadID = getThreadIdFromThread(
              item.add_member.metadata.thread,
            )
            texts.log(threadID)
            const response = await this.http.get_users({
              user_fbids: item.add_member.users.map(user => String(user.id)),
            })
            const participants = response?.messaging_actors.map(mapParticipant)
            return {
              type: ServerEventType.STATE_SYNC,
              mutationType: 'upsert',
              objectName: 'participant',
              objectIDs: { threadID },
              entries: participants,
            }
          }
          if (item.forced_fetch) {
            const threadID = getThreadIdFromThread(item.forced_fetch.thread)
            return {
              type: ServerEventType.THREAD_MESSAGES_REFRESH,
              threadID,
            }
          }
          if (item.message) {
            // texts.log(stringifyWithBigInt(item.message, 2))
            const threadID = getThreadIdFromThread(item.message.metadata.thread)
            texts.log(threadID)
            if (String(item.message.metadata.sender) == this.currentUser.id) {
              this.mqtt.handleRequestResponseOverMQTT(
                SubscribeTopics.SEND_MESSAGE_RESPONSE,
                String(item.message.metadata.offline_threading_id),
                message,
              )
            }
            const mappedMessage = mappedMessages.find(
              mapped => mapped.id == item.message.metadata.id,
            )

            return {
              type: ServerEventType.THREAD_MESSAGES_REFRESH,
              threadID,
            }
          }
          if (item.sent_message) {
            this.mqtt.handleRequestResponseOverMQTT(
              SubscribeTopics.SEND_MESSAGE_RESPONSE,
              String(item.sent_message.messageMetadata.offline_threading_id),
              message,
            )
            return undefined
          }

          if (item.read_receipt) {
            const reads: { [participantID: string]: Date } = {}
            reads[String(item.read_receipt.user_id.toString)] = new Date(
              Number(item.read_receipt.read_at),
            )
            return {
              type: ServerEventType.STATE_SYNC,
              mutationType: 'update',
              objectName: 'message',
              objectIDs: {
                threadID: getThreadIdFromThread(item.read_receipt.thread),
              },
              entries: [
                {
                  id: String(item.read_receipt.read_to),
                  seen: reads,
                },
              ],
            }
          }
          if (item.mark_read_own) {
            return {
              type: ServerEventType.STATE_SYNC,
              mutationType: 'update',
              objectName: 'thread',
              objectIDs: {},
              entries: item.mark_read_own.threads.map(thread => ({
                id: getThreadIdFromThread(thread),
                isUnread: false,
              })),
            }
          }

          return bluebird.map(item.inner_items || [], async innerItem => {
            if (innerItem.message_reply) {
              const threadID = getThreadIdFromThread(
                innerItem.message_reply.message.metadata.thread,
              )
              if (
                String(innerItem.message_reply.message.metadata.sender)
                == this.currentUser.id
              ) {
                this.mqtt.handleRequestResponseOverMQTT(
                  SubscribeTopics.SEND_MESSAGE_RESPONSE,
                  String(
                    innerItem.message_reply.message.metadata
                      .offline_threading_id,
                  ),
                  message,
                )
              }

              const mappedMessage = mappedMessages.find(
                mapped =>
                  mapped.id == innerItem.message_reply.message.metadata.id,
              )
              // if (mappedMessage) {
              //   return {
              //     type: ServerEventType.STATE_SYNC,
              //     mutationType: 'upsert',
              //     objectName: 'message',
              //     objectIDs: { threadID },
              //     entries: [mappedMessage],
              //   }
              // }
              // return undefined
              return {
                type: ServerEventType.THREAD_MESSAGES_REFRESH,
                threadID,
              }
            }
            if (innerItem.reaction) {
              const threadID = getThreadIdFromThread(innerItem.reaction.thread)
              if (
                innerItem.reaction.action == MessageReactionAction.ADD_REACTION
              ) {
                return {
                  type: ServerEventType.STATE_SYNC,
                  mutationType: 'upsert',
                  objectName: 'message_reaction',
                  objectIDs: {
                    threadID,
                    messageID: innerItem.reaction.message_id,
                  },
                  entries: [
                    {
                      participantID: String(
                        innerItem.reaction.reaction_sender_id,
                      ),
                      id: String(innerItem.reaction.reaction_sender_id),
                      emoji: true,
                      reactionKey:
                        REACTION_MAP_TO_NORMALIZED[
                          innerItem.reaction.reaction
                        ] || innerItem.reaction.reaction,
                    },
                  ],
                }
              }
              return {
                type: ServerEventType.STATE_SYNC,
                mutationType: 'delete',
                objectName: 'message_reaction',
                objectIDs: {
                  threadID: String(threadID),
                  messageID: innerItem.reaction.message_id,
                },
                entries: [String(innerItem.reaction.reaction_sender_id)],
              }
            }
            if (innerItem.unsend_message) {
              const threadID = getThreadIdFromThread(
                innerItem.unsend_message.thread,
              )

              return {
                type: ServerEventType.THREAD_MESSAGES_REFRESH,
                threadID,
              }
            }
            return undefined
          })
        })
      )
        .flat()
        .filter(event => !!event)

      if (serverEvents.length > 0) {
        texts.log(stringifyWithBigInt(serverEvents, 2))
        this.onEvent(serverEvents)
      }
    })

    this.mqtt.mqtt?.on('disconnect', async (...args) => {
      texts.log('mqtt disconnected', ...args)
      await this.reconnectMqtt()
    })
  }

  reconnectRealtime = async () => {
    texts.log('reconnectRealtime')
    await this.mqtt.disconnect(true)
    await this.connectMQTT()
  }

  getCurrentUser = (): CurrentUser => ({
    id: this.currentUser.id,
    fullName: this.currentUser.name,
    displayText: this.currentUser.username || this.currentUser.name,
    imgURL: this.currentUser.profile_pic_large?.uri,
    isVerified: this.currentUser.verified,
    email: this.currentUser.email,
    username: this.currentUser.username,
    isSelf: true,
  })

  addParticipant = (threadID: string, participantID: string) =>
    this.http.modify_participant(threadID, participantID, false)

  removeParticipant = (threadID: string, participantID: string) =>
    this.http.modify_participant(threadID, participantID, true)

  serializeSession = () => ({ savedState: this.state._serialize_() })

  subscribeToEvents = (onEvent: OnServerEventCallback) => {
    this.onEvent = onEvent
  }

  searchUsers = async (typed: string): Promise<any[]> => {
    const {
      search_results: { edges },
    } = await this.http.search({
      search_query: typed,
      entity_types: ['user'],
      num_group_threads_query: 0,
      num_pages_query: 0,
      include_pages: false,
      include_games: false,
    })
    const mapped = await Promise.all(
      // NOTE: we can narrow down to Participant here because we are only searching for users
      (edges || []).map(({ node }) => mapSearchResultUser(node as Participant)),
    )
    return mapped
  }

  createThread = async (userIDs: string[]): Promise<Thread | boolean> => {
    if (userIDs.length === 1) {
      const response = await this.http.get_users({ user_fbids: [userIDs[0], this.currentUser.id] })
      const otherUser = response.messaging_actors.find(user => user.id === userIDs[0])
      return {
        id: otherUser.id,
        title: otherUser.name,
        imgURL: otherUser.profile_pic_large.uri,
        isUnread: false,
        isReadOnly: false,
        messages: {
          items: [],
          hasMore: false,
        },
        participants: {
          items: response.messaging_actors.map(mapParticipant),
          hasMore: false,
        },
        timestamp: new Date(),
        type: 'single',
      } as Thread
    }
    await this.http.create_group(userIDs)
    // we'll receive the update via MQTT
    return true
  }

  getThread = async (threadID: string) => {
    const res = await this.http.fetch_thread_info({ thread_ids: [threadID] })
    if (res?.message_threads?.length) {
      const thread_info = res.message_threads[0]
      return mapThread(thread_info)
    }
  }

  getThreads = async (
    inboxName: ThreadFolderName,
    pagination: PaginationArg,
  ) => {
    if (inboxName === InboxName.NORMAL) {
      const cursor = pagination?.cursor
        ? JSON.parse(pagination.cursor)
        : {
          [ThreadFolder.INBOX]: null,
          [ThreadFolder.ARCHIVED]: null,
        }
      const [{ nodes: inboxThreads }, { nodes: archivedThreads }] = cursor
        ? await Promise.all([
          this.http.fetch_more_threads({
            after_time_ms: cursor[ThreadFolder.INBOX],
            folder_tag: [ThreadFolder.INBOX],
          }),
          this.http.fetch_more_threads({
            after_time_ms: cursor[ThreadFolder.ARCHIVED],
            folder_tag: [ThreadFolder.ARCHIVED],
          }),
        ])
        : await Promise.all([
          this.http.fetch_thread_list({
            folder_tag: [ThreadFolder.INBOX],
          }),
          this.http.fetch_thread_list({
            folder_tag: [ThreadFolder.ARCHIVED],
          }),
        ])

      const threads = [...inboxThreads, ...archivedThreads]

      threads.forEach(thread => {
        if (thread.is_group_thread) {
          this.groupThreadsCache.add(getThreadIdFromThread(thread.thread_key))
        }
      })

      return {
        items: threads.length ? mapThreads(threads) : [],
        hasMore: !(threads.length % 20) || true,
        oldestCursor: JSON.stringify({
          [ThreadFolder.INBOX]: inboxThreads.length
            ? inboxThreads[inboxThreads.length - 1]?.updated_time_precise
            : null,
          [ThreadFolder.ARCHIVED]: archivedThreads.length
            ? archivedThreads[archivedThreads.length - 1]?.updated_time_precise
            : null,
        }),
      }
    }

    const { cursor } = pagination || { cursor: null, direction: null }

    const { nodes: threads } = cursor
      ? await this.http.fetch_more_threads({
        after_time_ms: cursor,
        folder_tag: [ThreadFolder.PENDING],
      })
      : await this.http.fetch_thread_list({
        folder_tag: [ThreadFolder.PENDING],
      })

    threads.forEach(thread => {
      if (thread.is_group_thread) {
        this.groupThreadsCache.add(getThreadIdFromThread(thread.thread_key))
      }
    })

    return {
      items: threads.length ? mapThreads(threads) : [],
      hasMore: !(threads.length % 20) || true,
      oldestCursor: threads.length
        ? threads[threads.length - 1]?.updated_time_precise
        : null,
    }
  }

  getMessages = async (threadID: string, pagination: PaginationArg) => {
    const { cursor } = pagination || { cursor: null, direction: null }

    const response = await this.http.fetch_messages({
      thread_id: threadID,
      before_time_ms: cursor,
    })

    if (Array.isArray(response?.nodes)) {
      for (const message of response?.nodes) {
        if (message?.sticker?.id) {
          const stickerResponse = await this.http.fetch_stickers({
            sticker_ids: [message?.sticker?.id],
          })
          if (
            Array.isArray(stickerResponse?.nodes)
            && stickerResponse?.nodes?.length == 1
          ) {
            const [sticker] = stickerResponse.nodes
            message.sticker.url = sticker?.animated_image?.uri
              || sticker?.thread_image?.uri
              || sticker?.preview_image?.uri
              || ''
          }
        }
      }
    }

    const filteredNodes = response?.nodes.filter(n => n.timestamp_precise !== cursor)

    // texts.log(filteredNodes.length)
    // texts.log(response?.nodes?.length)

    return {
      items: Array.isArray(response?.nodes)
        ? mapMessages(filteredNodes, threadID)
        : [],
      hasMore:
        response?.page_info?.has_previous_page
        || response?.nodes?.length >= MESSAGE_PAGE_SIZE,
    }
  }

  sendMessage = async (
    threadID: string,
    content: MessageContent,
    options?: MessageSendOptions,
  ): Promise<boolean | Message[]> => {
    let response: {
      data: FlattenedMessageSyncPayload
      extra: t.TypeOf<typeof sendMessageSchema>
    } | undefined

    if (!this.mqtt.commands) {
      await this.reconnectMqtt()
      texts.Sentry.captureMessage('Had to manually reconnect MQTT for sendMessage')
    }

    const offlineThreadingId = options.pendingMessageID.includes('-')
      ? genClientContext().toString() // for ios
      : options.pendingMessageID
    const offlineThreadingIdInt = BigInt(offlineThreadingId)

    if (content.filePath || content.fileBuffer) {
      const data = content.filePath
        ? await fs.readFile(content.filePath)
        : content.fileBuffer
      const fileName = content.filePath
        ? path.basename(content.filePath)
        : content.fileName
      this.http.sendMedia({
        data,
        fileName,
        mimetype: content.mimeType,
        offlineThreadingId,
        chatId: threadID,
        timestamp: Date.now(),
        ...(options.quotedMessageID
          ? { replyTo: options.quotedMessageID }
          : {}),
      })
      const mqttRegisterRequest = () =>
        this.mqtt.commands?.requestResponseState.registerRequest<
        FlattenedMessageSyncPayload,
        Partial<t.TypeOf<typeof sendMessageSchema>>
        >(PublishTopics.SEND_MESSAGE, offlineThreadingId, { offlineThreadingId: offlineThreadingIdInt })

      try {
        response = await mqttRegisterRequest()
      } catch (e) {
        texts.error('Error occurred during mqttRegisterRequest', JSON.stringify(e, null, 2))
        await this.reconnectMqtt()
        response = await mqttRegisterRequest()
        texts.Sentry.captureMessage('Had to manually reconnect MQTT for mqttRegisterRequest')
      }
    } else {
      const mqttSendMessage = () =>
        this.mqtt.commands.sendMessage({
          to: this.groupThreadsCache.has(threadID)
            ? `tfbid_${threadID}`
            : threadID,
          senderFbid: BigInt(this.currentUser.id),
          body: content.text,
          offlineThreadingId: offlineThreadingIdInt,
          clientTags: {
            trigger: 'thread_view_messages_fragment_unknown',
            is_in_chatheads: 'false',
            entrypoint: 'messenger_inbox:in_thread',
          },
          isDialtone: false,
          markReadWatermarkTimestamp: BigInt(0),
          ttl: 0,
          msgAttemptId: genClientContext(),
          ...(options.quotedMessageID
            ? { repliedToMessageId: options.quotedMessageID }
            : {}),
        })
      try {
        response = await mqttSendMessage()
      } catch (e) {
        texts.error('Error occurred during mqttSendMessage', e)
        await this.reconnectMqtt()
        response = await mqttSendMessage()
        texts.Sentry.captureMessage('Had to manually reconnect MQTT for mqttSendMessage')
      }
    }

    if (response) {
      let sentMessageId: string | undefined

      response.data.items?.find(item => {
        if (
          item.message
          && item.message.metadata.offline_threading_id
            == response.extra.offlineThreadingId
        ) {
          sentMessageId = item.message.metadata.id
          return true
        }
        if (
          item.sent_message
          && item.sent_message.messageMetadata.offline_threading_id
            == response.extra.offlineThreadingId
        ) {
          sentMessageId = item.sent_message.messageMetadata.id
          return true
        }

        if (item.inner_items) {
          item.inner_items.forEach(innerItem => {
            if (
              innerItem.message_reply
              && innerItem.message_reply.message.metadata.offline_threading_id
                == response.extra.offlineThreadingId
            ) {
              sentMessageId = innerItem.message_reply.message.metadata.id
              return true
            }
          })
        }
      })
      if (sentMessageId) {
        const mappedMessages = await mapMQTTMessages(
          response.data,
          this.currentUser.id,
          this.http,
          response.extra,
        )
        const mappedSentMessage = mappedMessages.find(
          mapped => mapped.id == sentMessageId,
        )
        // texts.log(JSON.stringify(mappedSentMessage))
        return [mappedSentMessage]
      }
      return true
    }
    return true
  }

  addReaction = async (
    threadID: string,
    messageID: string,
    reactionKey: string,
  ) => {
    await this.http.react({
      message_id: messageID,
      client_mutation_id: uuid(),
      actor_id: this.currentUser.id,
      action: ReactionAction.ADD,
      reaction: REACTION_MAP_TO_MESSENGER[reactionKey] || reactionKey,
    })
  }

  removeReaction = async (
    threadID: string,
    messageID: string,
    reactionKey: string,
  ) => {
    await this.http.react({
      message_id: messageID,
      client_mutation_id: uuid(),
      actor_id: this.currentUser.id,
      action: ReactionAction.REMOVE,
      reaction: REACTION_MAP_TO_MESSENGER[reactionKey] || reactionKey,
    })
  }

  deleteMessage = async (
    threadID: string,
    messageID: string,
    forEveryone: boolean,
  ) => {
    if (forEveryone) {
      await this.http.unsend({
        message_id: messageID,
        client_mutation_id: uuid(),
        actor_id: this.currentUser.id,
      })
      return true
    }
  }

  getAsset = async (
    options: GetAssetOptions,
    category: string,
    threadID: string,
    messageID: string,
    attachmentID: string,
  ) => {
    if (category !== 'attachment') return
    const uri = await this.http.get_image_url(messageID, attachmentID)
    return uri
  }

  sendActivityIndicator = async (type: ActivityType, threadID: string) => {
    if (type === ActivityType.TYPING) {
      await this.mqtt.commands?.sendTyping({
        recipient: BigInt(threadID),
        sender: BigInt(this.currentUser.id),
        state: 1,
      })
    } else if (type === ActivityType.ONLINE) {
      await this.mqtt.commands?.toggleAppForeground(true)
    } else if (type === ActivityType.OFFLINE) {
      await this.mqtt.commands?.toggleAppForeground(false)
    }
    // texts.log('activity', type, threadID)
  }

  sendReadReceipt = async (threadID: string) => {
    await this.mqtt.commands?.markThread({
      mark: MarkThreadState.read,
      state: true,
      ...(this.groupThreadsCache.has(threadID)
        ? {
          threadFbId: BigInt(threadID),
        }
        : {
          otherUserFbId: BigInt(threadID),
        }),
      watermarkTimestamp: BigInt(Date.now()),
    })
  }

  markAsUnread = async (threadID: string) => {
    await this.mqtt.commands?.markThread({
      mark: MarkThreadState.read,
      state: false,
      ...(this.groupThreadsCache.has(threadID)
        ? {
          threadFbId: BigInt(threadID),
        }
        : {
          otherUserFbId: BigInt(threadID),
        }),
      watermarkTimestamp: BigInt(Date.now()),
      attemptId: genClientContext(),
    })
  }

  updateThread = async (threadID: string, updates: Partial<Thread>) => {
    if ('mutedUntil' in updates) {
      return this.http.mute_thread(threadID, updates.mutedUntil)
    }
  }

  archiveThread = async (threadID: string, archived: boolean) => {
    await this.mqtt.commands?.markThread({
      mark: MarkThreadState.archived,
      state: archived,
      ...(this.groupThreadsCache.has(threadID)
        ? {
          threadFbId: BigInt(threadID),
        }
        : {
          otherUserFbId: BigInt(threadID),
        }),
      attemptId: genClientContext(),
    })
  }

  private reconnectMqtt = async () => {
    await this.mqtt.disconnect()
    await this.connectMQTT()
  }

  registerForPushNotifications = async (type: 'android', token: string) => {
    if (type !== 'android') throw Error('invalid type')
    await this.http.register_push_tokens(token, true)
  }

  unregisterForPushNotifications = async (type: 'android', token: string) => {
    if (type !== 'android') throw Error('invalid type')
    await this.http.register_push_tokens(token, false)
  }
}
