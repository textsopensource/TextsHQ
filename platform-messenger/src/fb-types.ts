export interface FBMessageAttachment {
  __typename: 'MessageImage' | 'MessageVideo' | 'MessageAnimatedImage'
  attribution_app?: any
  attribution_metadata?: any
  filename: string
  preview?: Preview
  large_preview?: Preview
  thumbnail?: Preview
  photo_encodings: any[]
  legacy_attachment_id: string
  original_dimensions: { x: number, y: number }

  animated_image?: Preview
  preview_image?: Preview

  playable_url?: string
  large_image?: Preview

  url?: string
  preview_url?: string
  mimetype?: string

  original_extension: string
  render_as_sticker: boolean
  blurred_image_uri?: any
}

export interface GraphQLData<T> {
  nodes: T[]
}

export interface Preview {
  uri: string
  height?: number
  width?: number
}

export interface Target {
  __typename: string
  live_location_id: string
  is_expired: boolean
  expiration_time: number
  sender: { id: string }
  coordinate: { latitude: number, longitude: number }
  location_title: string
  sender_destination?: any
  stop_reason?: any
  business_items: GraphQLData<any>
  message?: string
  text?: string
}

export interface FBMedia {
  animated_image?: any
  image: Preview
  playable_duration_in_ms: number
  is_playable: boolean
  playable_url?: any
}

export interface StoryAttachment {
  description: { text: string }
  media: FBMedia
  source?: any
  image?: any
  style_list: string[]
  title_with_entities: { text: string }
  properties: any[]
  url: string
  deduplication_key: string
  action_links: any[]
  messaging_attribution?: any
  messenger_call_to_actions: any[]
  xma_layout_info?: any
  target: Target
  subattachments: any[]
}

export interface FBMessageSticker {
  id: string
  pack: any
  label: string
  frame_count: number
  frame_rate: number
  frames_per_row: number
  frames_per_column: number
  sprite_image_2x?: any
  sprite_image?: any
  padded_sprite_image?: any
  padded_sprite_image_2x?: any
  url: string
  height: number
  width: number
}

export interface FBExtensibleAttachment {
  legacy_attachment_id: string
  story_attachment: StoryAttachment
  genie_attachment: any
}

export interface FBMessage {
  __typename: 'UserMessage' | 'GenericAdminTextMessage' | string
  message_id?: string
  offline_threading_id?: string
  message_sender: { id: string, email: string }
  ttl?: number
  timestamp_precise: string
  unread: boolean
  is_sponsored: boolean
  ad_id?: any
  ad_client_token?: any
  commerce_message_type?: any
  participants_added?: { id: string }[]
  participants_removed?: { id: string }[]
  thread_name?: string
  tags_list: string[]
  platform_xmd_encoded?: any
  message_source_data?: any
  montage_reply_data?: any
  message_reactions: any[]
  unsent_timestamp_precise: string
  unsender?: any
  verse_group_role_xmd?: any
  message_unsendability_status: string
  message?: { text: string }
  snippet?: string
  extensible_attachment?: FBExtensibleAttachment
  sticker?: FBMessageSticker
  blob_attachments?: FBMessageAttachment[]
  page_admin_sender?: any
  replied_to_message?: {
    status: 'VALID' | 'INVALID'
    message: FBMessage
  }
}

export enum CannotReplyReason {
  BLOCKED = 'blocked',
  MESSENGER_BLOCKEE = 'messenger_blockee',
  COMPOSER_DISABLED_BOT = 'composer_disabled_bot',
  HAS_EMAIL_PARTICIPANT = 'has_email_participant',
  OBJECT_ORIGINATED = 'object_originated',
  READ_ONLY = 'read_only',
  VIEWER_NOT_SUBSCRIBED = 'viewer_not_subscribed',
  RECIPIENTS_NOT_LOADABLE = 'recipients_not_loadable',
  RECIPIENTS_UNAVAILABLE = 'recipients_unavailable',
  RECIPIENTS_INVALID = 'recipients_invalid',
  RECIPIENTS_INACTIVE_WORK_ACC = 'recipients_inactive_work_account',
  MONTAGE_NOT_AUTHOR = 'montage_not_author',
  VIEWER_MUTED_IN_FBGROUP = 'viewer_muted_in_fbgroup',
}

export interface FBUser {
  id: string
  name: string
  gender: 'MALE' | 'FEMALE' | string
  url: string
  big_image_src: { uri: string }
  short_name: string
  username: string
  is_viewer_friend: boolean
  is_messenger_user: boolean
}

export interface FBParticipant {
  admin_type: any
  node: { messaging_actor: FBUser }
}

export interface FBReadReceipt {
  watermark: string
  action: string
  actor: { id: string }
}

export interface FBDeliveryReceipt {
  timestamp_precise: string
}

export interface FBMessageThread {
  id: string
  thread_key: { thread_fbid: string, other_user_id: string }
  name: string
  last_message: GraphQLData<FBMessage>
  unread_count: number
  messages_count: 4
  mute_until: number
  is_pinned: boolean
  image?: { uri: string }
  folder: 'INBOX' | string
  cannot_reply_reason: CannotReplyReason
  has_viewer_archived: boolean
  thread_admins: { id: string }[]
  updated_time_precise: string
  last_read_receipt: GraphQLData<{ timestamp_precise: string }>
  thread_type: 'ONE_TO_ONE' | 'GROUP'
  all_participants: { edges: FBParticipant[] }
  read_receipts: GraphQLData<FBReadReceipt>
  delivery_receipts: GraphQLData<FBDeliveryReceipt>

  messages?: GraphQLData<FBMessage>
}

export interface FBUserSearchResult {
  display_name: string
  node: {
    id: string
    __typename: string
    name: string
    username: string
    is_memorialized: boolean
    is_viewer_friend: boolean
    is_viewer_coworker: boolean
    work_foreign_entity_info?: any
    profile_picture: { uri: string }
    messenger_contact: { is_on_viewer_contact_list: boolean }
    url: string
    is_messenger_user: boolean
    is_work_user: boolean
    is_verified: boolean
    viewer_affinity: number
    scim_company_user?: any
    work_info?: any
  }
}
