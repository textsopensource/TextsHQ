import {
  Participant,
  Message,
  Thread,
  MessageAttachment,
  MessageAttachmentType,
  MessageActionType,
  MessageAction,
  MessageReaction,
  TextEntity,
  UserPresence,
  texts,
} from '@textshq/platform-sdk'
import { forEach as forEachAsync } from 'p-iteration'
import { t } from '@f0rr0/thrift-compact-protocol'
import {
  Participant as FBParticipant,
  Thread as FBThread,
  ReadReceipt as FBReadReceipt,
  DeliveryReceipt as FBDeliveryReceipt,
} from './lib/graphql/responses'
import { FlattenedMessageSyncPayload, PresenceItem } from './mqtt/parsers'
import { FBMessage } from './fb-types'
import { getThreadIdFromThread, ThreadCannotReplyReason } from './lib/common'
import {
  attachmentSchema,
  parseExtensibleAttachment,
  sendMessageSchema,
} from './lib/descriptors'
import AndroidAPI from './http'
import { stringifyWithBigInt } from './lib/utils'

export const REACTION_MAP_TO_NORMALIZED = {
  '❤': 'heart',
  '😆': 'laugh',
  '😮': 'surprised',
  '😢': 'cry',
  '😠': 'angry',
  '👍': 'like',
  '👎': 'dislike',
}

export const REACTION_MAP_TO_MESSENGER = {
  heart: '❤',
  laugh: '😆',
  surprised: '😮',
  cry: '😢',
  angry: '😠',
  like: '👍',
  dislike: '👎',
}

const ATTACHMENT_MAP = {
  MessageImage: MessageAttachmentType.IMG,
  MessageVideo: MessageAttachmentType.VIDEO,
  MessageAnimatedImage: MessageAttachmentType.IMG,
  MessageAudio: MessageAttachmentType.AUDIO,
  MessageFile: MessageAttachmentType.UNKNOWN,
}

const MESSENGER_LINK_SHIM_PREFIX = 'https://l.messenger.com/l.php?u='
const FACEBOOK_LINK_SHIM_PREFIX = 'https://l.facebook.com/l.php?u='

export type FBExtendedMessage = FBMessage & {
  misc?: {
    deliveries: FBDeliveryReceipt[]
    reads: FBReadReceipt[]
  }
} & any

export function mapParticipant(user: FBParticipant): Participant {
  return {
    id: user.id,
    fullName: user.name,
    username: user.username,
    imgURL: user.profile_pic_large?.uri,
  }
}

export function mapSearchResultUser(user: FBParticipant): Participant {
  return {
    id: user.id,
    fullName: user.name || user.structured_name?.text,
    phoneNumber: null,
    imgURL: user.profile_pic_large?.uri,
  }
}

export function mapAction(message: FBMessage): MessageAction {
  switch (message.__typename) {
    case 'ParticipantsAddedMessage':
      return {
        type: MessageActionType.THREAD_PARTICIPANTS_ADDED,
        participantIDs: message.participants_added.map(p => p.id),
        actorParticipantID: message.message_sender?.id,
      }
    case 'ParticipantLeftMessage':
      return {
        type: MessageActionType.THREAD_PARTICIPANTS_REMOVED,
        participantIDs: message.participants_removed.map(p => p.id),
        actorParticipantID: message.message_sender?.id,
      }
    case 'ThreadNameMessage':
      return {
        type: MessageActionType.THREAD_TITLE_UPDATED,
        title: message.thread_name,
        actorParticipantID: message.message_sender?.id,
      }
  }
}

export async function mapMQTTAttachments(
  mqttAttachments: t.TypeOf<typeof attachmentSchema>[],
  messageID: string,
) {
  const attachments: MessageAttachment[] = []
  mqttAttachments?.forEach(attachment => {
    if (attachment.xmaGraphQL) {
      const extensibleAttachment = parseExtensibleAttachment(attachment)
      if (extensibleAttachment.story_attachment) {
        if (extensibleAttachment.story_attachment.media?.playable_url) {
          attachments.push({
            id: messageID,
            type: MessageAttachmentType.VIDEO,
            posterImg: extensibleAttachment.story_attachment.media?.image?.uri,
            srcURL: extensibleAttachment.story_attachment.media?.playable_url,
          })
        } else if (extensibleAttachment.story_attachment.media?.image) {
          attachments.push({
            id: messageID,
            type: MessageAttachmentType.IMG,
            srcURL: extensibleAttachment.story_attachment.media.image.uri,
            size: {
              width: extensibleAttachment.story_attachment.media.image.width,
              height: extensibleAttachment.story_attachment.media.image.height,
            },
          })
        }
      }
    } else {
      const attachmentMetadata: Partial<MessageAttachment> &
      Pick<MessageAttachment, 'type'> = (() => {
        if (
          (attachment.mimeType == 'image/jpeg'
            || attachment.mimeType == 'image/png')
          && attachment.imageMetadata
        ) {
          return {
            type: MessageAttachmentType.IMG,
            posterImg: attachment.imageMetadata.imageURIMap[0],
            srcURL: attachment.imageMetadata.imageURIMap[0],
            size: {
              width: attachment.imageMetadata.width,
              height: attachment.imageMetadata.height,
            },
          }
        }
        if (attachment.mimeType == 'image/gif' && attachment.imageMetadata) {
          return {
            type: MessageAttachmentType.IMG,
            isGif: true,
            posterImg: attachment.imageMetadata.animatedImageURIMap[0],
            srcURL: attachment.imageMetadata.animatedImageURIMap[0],
            size: {
              width: attachment.imageMetadata.width,
              height: attachment.imageMetadata.height,
            },
          }
        }
        if (attachment.mimeType == 'video/mp4' && attachment.videoMetadata) {
          return {
            type: MessageAttachmentType.VIDEO,
            posterImg: attachment.videoMetadata.thumbnailUri,
            srcURL: attachment.videoMetadata.videoUri,
            size: {
              width: attachment.videoMetadata.width,
              height: attachment.videoMetadata.height,
            },
          }
        }
        if (attachment.mimeType == 'audio/mp4' && attachment.audioMetadata) {
          return {
            type: MessageAttachmentType.AUDIO,
            srcURL: attachment.audioMetadata.url,
            isVoiceNote: true,
          }
        }
        return {
          type: MessageAttachmentType.UNKNOWN,
        }
      })()
      attachments.push({
        id: attachment.id || attachment.filename,
        fileName: attachment.filename,
        mimeType: attachment.mimeType,
        ...attachmentMetadata,
      })
    }
  })
  return attachments
}

export async function mapMQTTSticker(
  stickerID: BigInt,
  messageID: string,
  http: AndroidAPI,
) {
  const attachments: MessageAttachment[] = []
  if (stickerID) {
    const url = await http.fetch_single_sticker_url(String(stickerID))
    attachments.push({
      id: messageID + '-' + String(stickerID),
      type: MessageAttachmentType.IMG,
      fileName: 'sticker.webp',
      isSticker: true,
      mimeType: 'image/webp',
      srcURL: url,
      size: { width: 64, height: 64 },
    })
  }
  return attachments
}
function getOriginalURL(linkURL: string) {
  if (
    !linkURL?.startsWith(MESSENGER_LINK_SHIM_PREFIX)
    && !linkURL?.startsWith(FACEBOOK_LINK_SHIM_PREFIX)
  ) {
    return linkURL
  }

  const parsed = new URL(linkURL)
  if (!parsed?.searchParams) return
  // {
  //   u: 'https://texts.com/',
  //   h: 'randomness',
  //   s: '1'
  // }
  const u = parsed.searchParams.getAll('u')
  if (u.length > 1) return linkURL
  return u[0]
}
export function mapAttachments(message: FBExtendedMessage, threadID: string) {
  const attachments: MessageAttachment[] = []
  if (message.sticker) {
    attachments.push({
      id: message.message_id + '-' + message.sticker.id,
      type: MessageAttachmentType.IMG,
      fileName: 'sticker.webp',
      isSticker: true,
      mimeType: 'image/webp',
      srcURL: message.sticker.url,
      size: { width: 64, height: 64 },
    })
  }
  const story = message.extensible_attachment?.story_attachment
  if (story) {
    if (story.media?.playable_url) {
      attachments.push({
        id: message.message_id,
        type: MessageAttachmentType.VIDEO,
        posterImg: story.image?.uri,
        srcURL: story.media?.playable_url,
      })
    } else if (story.media?.image) {
      const { width, height } = story.media.image
      // prevent an error on attachments with only URI and size as 0
      const size = (width && height) ? { width, height } : undefined
      attachments.push({
        id: message.message_id,
        type: MessageAttachmentType.IMG,
        srcURL: story.media.image.uri,
        size,
      })
    }

    attachments.push(
      ...(story.target?.business_items?.nodes
        ?.map(node => {
          if (!node.image_url) return null
          return {
            id: node.id,
            type: MessageAttachmentType.IMG,
            srcURL: node.image_url,
          }
        })
        .filter(Boolean) || []),
    )
  }

  const mappedAttachments = (
    message.blob_attachments as any[]
  )?.map<MessageAttachment>(attachment => {
    const type = ATTACHMENT_MAP[attachment.__typename] || MessageAttachmentType.UNKNOWN
    return {
      id: attachment.attachment_fbid || attachment.filename,
      type,
      isGif: attachment.__typename === 'MessageAnimatedImage',
      fileName: attachment.filename,
      mimeType: attachment.mimetype,
      posterImg:
        attachment?.preview_url
        || attachment?.large_image?.uri
        || attachment.preview?.uri
        || attachment?.streamingImageThumbnail,
      srcURL:
        attachment.__typename === 'MessageImage'
          ? `asset://$accountID/attachment/${threadID}/${message.message_id}/${attachment.attachment_fbid}` // dynamically load if it's an image
          : attachment.__typename === 'MessageVideo'
            ? attachment.attachment_video_url
            : getOriginalURL(
              attachment.url
                || attachment.playable_url
                || attachment.animated_image_full_screen?.uri
                || attachment.animated_image?.uri
                || attachment.large_preview?.uri,
            ),
      size: {
        // 0 should be null
        width: attachment.original_dimensions?.x || null,
        height: attachment.original_dimensions?.y || null,
      },
    }
  })
  attachments.push(...(mappedAttachments || []))
  return attachments
}

function getMessageTextHeading(tags: Set<string>) {
  if (tags.has('filtered_content')) {
    if (tags.has('filtered_content_bh')) {
      return 'This message was removed because it includes a link that goes against our Community Standards.'
    }
    if (tags.has('filtered_content_account')) {
      return "This message has been temporarily removed because the sender's account requires verification."
    }
    if (tags.has('filtered_content_quasar')) {
      return 'This message was removed because it contains blocked or harmful content.'
    }
    return tags.has('filtered_content_invalid_app')
      ? "This message was removed because the app it's being sent from is blocked from sending messages."
      : "This message has been temporarily removed because the sender's account requires verification, or it was identified as abusive."
  }
}

export const mapMessageReactions = (reactions: any[] = []) =>
  reactions.map<MessageReaction>(({ reaction, user }) => {
    const participantID = user.id
    const reactionKey = REACTION_MAP_TO_NORMALIZED[reaction] || reaction
    return {
      id: participantID,
      participantID,
      reactionKey,
    }
  })

export function mapMessage(
  message: FBExtendedMessage,
  threadID: string,
): Message {
  const reads: { [participantID: string]: Date } = {}
  message.misc?.reads.forEach(read => {
    reads[read.actor.id] = new Date(+read.watermark)
  })

  const attachments = mapAttachments(message, threadID)
  const action = mapAction(message)
  const quoted = message.replied_to_message?.message
  const isDeleted = message.unsent_timestamp_precise !== '0'
  const sa = message.extensible_attachment?.story_attachment
  const saType = sa?.target?.__typename
  const { tags_list = [] } = message
  const tags = new Set<string>(tags_list)

  const m: Message = {
    _original: stringifyWithBigInt([message]),
    id: message.message_id,
    cursor: message.timestamp_precise,
    textHeading: isDeleted ? null : getMessageTextHeading(tags),
    forwardedCount: tags.has('action:copy_message') ? 1 : 0,
    text:
      message.__typename === 'GenericAdminTextMessage'
      || isDeleted
      || message.is_user_generated === false
        ? message.snippet
        : message.message?.text,
    textFooter: null,
    timestamp: new Date(+message.timestamp_precise),
    senderID: message.message_sender?.messaging_actor.id,
    isSender: tags.has('sent'),
    isDeleted,
    attachments,
    reactions: mapMessageReactions(message.message_reactions),
    seen: reads,
    linkedMessageID: quoted ? quoted.message_id : undefined,
    action,
    isAction: message.__typename !== 'UserMessage', // prevent deleted messages from becoming an action
  }

  const { ranges } = message.message || {}
  if (ranges) {
    m.textAttributes = {
      entities: (ranges as any[]).map<TextEntity>(range => {
        if (range.entity.__typename !== 'User') return
        return {
          from: range.offset,
          to: range.offset + range.length,
          mentionedUser: {
            id: range.entity.id,
          },
        }
      }),
    }
  }
  if (saType === 'ExternalUrl') {
    m.attachments.pop()
    const mappedUrl = sa.url.startsWith('fbrpc://')
      ? new URL(sa.url).searchParams.get('target_url')
      : sa.url
    const ogURL = getOriginalURL(mappedUrl)

    m.links = [
      {
        title: sa.title_with_entities.text,
        summary: sa.description?.text,
        img:
          sa.media?.is_playable && sa.media?.animated_image
            ? sa.media?.animated_image?.uri
            : sa.media?.image?.uri,
        imgSize: {
          height: sa.media?.image?.height,
          width: sa.media?.image?.width,
        },
        originalURL: ogURL === mappedUrl ? undefined : ogURL,
        url: mappedUrl,
      },
    ]
  } else if (saType === 'MessageLocation') {
    m.attachments.pop()
    m.links = [
      {
        title: sa.title_with_entities?.text,
        img: sa.media?.image?.uri,
        url: sa.url,
      },
    ]
  } else if (saType === 'MessageLiveLocation') {
    m.textHeading = sa.title_with_entities?.text
    m.textFooter = sa.description?.text
  } else if (saType === 'Video') {
    // todo
  } else if (saType === 'MessengerBusinessMessage') {
    m.textHeading = sa.target.message
  } else if (sa && !isDeleted) {
    if (sa.style_list.includes('unavailable')) {
      m.textHeading = sa.title_with_entities?.text
      m.textFooter = sa.description?.text
    } else if (!isDeleted) {
      m.textFooter = [
        sa.target?.text,
        sa.title_with_entities?.text,
        sa.description?.text,
      ]
        .filter(Boolean)
        .join('\n')
    }
  }
  return m
}

export async function mapMQTTMessages(
  message: FlattenedMessageSyncPayload,
  currentUserID: string,
  http: AndroidAPI,
  sentMessage?: t.TypeOf<typeof sendMessageSchema>,
): Promise<Message[] | undefined> {
  const mappedMessages: Message[] = []

  if (message.items?.length > 0) {
    await forEachAsync(message.items, async item => {
      if (item.add_member) {
        const messageID = String(item.add_member.metadata.id)
        const threadID = getThreadIdFromThread(item.add_member.metadata.thread)
        mappedMessages.push({
          _original: stringifyWithBigInt([message]),
          id: messageID,
          threadID,
          text: item.add_member.metadata.admin_text,
          timestamp: new Date(+String(item.add_member.metadata.timestamp)),
          senderID: String(item.add_member.metadata.sender),
          isSender: String(item.add_member.metadata.sender) == currentUserID,
          cursor: String(item.add_member.metadata.timestamp),
        })
      } else if (item.remove_member) {
        const messageID = String(item.remove_member.metadata.id)
        const threadID = getThreadIdFromThread(
          item.remove_member.metadata.thread,
        )
        mappedMessages.push({
          _original: stringifyWithBigInt([message]),
          id: messageID,
          threadID,
          text: item.remove_member.metadata.admin_text,
          timestamp: new Date(+String(item.remove_member.metadata.timestamp)),
          senderID: String(item.remove_member.metadata.sender),
          isSender: String(item.remove_member.metadata.sender) == currentUserID,
          cursor: String(item.remove_member.metadata.timestamp),
        })
      } else if (item.message) {
        const messageID = String(item.message.metadata.id)
        const threadID = getThreadIdFromThread(item.message.metadata.thread)
        const attachments = [
          ...(await mapMQTTAttachments(
            item.message.attachments,
            item.message.metadata.id,
          )),
          ...(await mapMQTTSticker(
            item.message.sticker,
            item.message.metadata.id,
            http,
          )),
        ]
        mappedMessages.push({
          _original: stringifyWithBigInt([message]),
          id: messageID,
          threadID,
          text: item.message.text || item.message.metadata.admin_text,
          timestamp: new Date(+String(item.message.metadata.timestamp)),
          senderID: String(item.message.metadata.sender),
          isSender: String(item.message.metadata.sender) == currentUserID,
          attachments,
          cursor: String(item.message.metadata.timestamp),
        })
      } else if (item.sent_message && sentMessage) {
        const messageID = String(item.sent_message.messageMetadata.id)
        const threadID = getThreadIdFromThread(
          item.sent_message.messageMetadata.thread,
        )
        const attachments = [
          ...(await mapMQTTAttachments(
            item.sent_message.attachments,
            item.sent_message.messageMetadata.id,
          )),
        ]
        mappedMessages.push({
          _original: stringifyWithBigInt([message]),
          id: messageID,
          threadID,
          text: sentMessage.body,
          timestamp: new Date(
            +String(item.sent_message.messageMetadata.timestamp),
          ),
          senderID: currentUserID,
          isSender: true,
          attachments,
          cursor: String(item.sent_message.messageMetadata.timestamp),
        })
      }

      if (item.inner_items) {
        await forEachAsync(item.inner_items, async innerItem => {
          if (innerItem.message_reply) {
            const messageID = String(
              innerItem.message_reply.message.metadata.id,
            )
            const threadID = getThreadIdFromThread(
              innerItem.message_reply.message.metadata.thread,
            )
            const attachments = [
              ...(await mapMQTTAttachments(
                innerItem.message_reply.message.attachments,
                innerItem.message_reply.message.metadata.id,
              )),
              ...(await mapMQTTSticker(
                innerItem.message_reply.message.sticker,
                innerItem.message_reply.message.metadata.id,
                http,
              )),
            ]
            mappedMessages.push({
              _original: stringifyWithBigInt([message]),
              id: messageID,
              threadID,
              text:
                innerItem.message_reply.message.text
                || innerItem.message_reply.message.metadata.admin_text,
              timestamp: new Date(
                +String(innerItem.message_reply.message.metadata.timestamp),
              ),
              senderID: String(innerItem.message_reply.message.metadata.sender),
              isSender:
                String(innerItem.message_reply.message.metadata.sender)
                == currentUserID,
              attachments,
              cursor: String(
                innerItem.message_reply.message.metadata.timestamp,
              ),
              linkedMessage: {
                id: innerItem.message_reply.replied_to_message.metadata.id,
                threadID,
                text:
                  innerItem.message_reply.replied_to_message.text
                  || innerItem.message_reply.replied_to_message.metadata
                    .admin_text,
                senderID: String(
                  innerItem.message_reply.replied_to_message.metadata.sender,
                ),
                attachments: [
                  ...(await mapMQTTAttachments(
                    innerItem.message_reply.replied_to_message.attachments,
                    innerItem.message_reply.replied_to_message.metadata.id,
                  )),
                  ...(await mapMQTTSticker(
                    innerItem.message_reply.replied_to_message.sticker,
                    innerItem.message_reply.replied_to_message.metadata.id,
                    http,
                  )),
                ],
              },
            })
          }
        })
      }
    })
  }

  return mappedMessages.length > 0 ? mappedMessages : []

  // const m: Message = {
  //   _original: JSON.stringify([message]),
  //   id: message.,
  //   cursor: message.timestamp_precise,
  //   textHeading: isDeleted ? null : getMessageTextHeading(tags),
  //   forwardedCount: tags.has('action:copy_message') ? 1 : 0,
  //   text:
  //     message.__typename === 'GenericAdminTextMessage' || isDeleted
  //       ? message.snippet
  //       : message.message?.text,
  //   textFooter: null,
  //   timestamp: new Date(+message.timestamp_precise),
  //   senderID: message.message_sender?.messaging_actor.id,
  //   isSender: tags.has('sent'),
  //   isDeleted,
  //   attachments,
  //   reactions: mapMessageReactions(message.message_reactions),
  //   seen: reads,
  //   linkedMessage: quoted
  //     ? mapMessage(quoted as FBExtendedMessage, threadID)
  //     : undefined,
  //   action,
  //   isAction: message.__typename !== 'UserMessage', // prevent deleted messages from becoming an action
  // }
  // const { ranges } = message.message || {}
  // if (ranges) {
  //   m.textAttributes = {
  //     entities: (ranges as any[]).map<TextEntity>((range) => {
  //       if (range.entity.__typename !== 'User') return
  //       return {
  //         from: range.offset,
  //         to: range.offset + range.length,
  //         mentionedUser: {
  //           id: range.entity.id,
  //         },
  //       }
  //     }),
  //   }
  // }
  // if (saType === 'ExternalUrl') {
  //   m.attachments.pop()
  //   const ogURL = getOriginalURL(sa.url)
  //   m.links = [
  //     {
  //       title: sa.title_with_entities.text,
  //       summary: sa.description?.text,
  //       img: sa.media?.image?.uri,
  //       imgSize: {
  //         height: sa.media?.image?.height,
  //         width: sa.media?.image?.width,
  //       },
  //       originalURL: ogURL === sa.url ? undefined : ogURL,
  //       url: sa.url,
  //     },
  //   ]
  // } else if (saType === 'MessageLocation') {
  //   m.attachments.pop()
  //   m.links = [
  //     {
  //       title: sa.title_with_entities?.text,
  //       img: sa.media?.image?.uri,
  //       url: sa.url,
  //     },
  //   ]
  // } else if (saType === 'MessageLiveLocation') {
  //   m.textHeading = sa.title_with_entities?.text
  //   m.textFooter = sa.description?.text
  // } else if (saType === 'Video') {
  //   // todo
  // } else if (saType === 'MessengerBusinessMessage') {
  //   m.textHeading = sa.target.message
  // } else if (sa && !isDeleted) {
  //   if (sa.style_list.includes('unavailable')) {
  //     m.textHeading = sa.title_with_entities?.text
  //     m.textFooter = sa.description?.text
  //   } else if (!isDeleted) {
  //     m.textFooter = [
  //       sa.target?.text,
  //       sa.title_with_entities?.text,
  //       sa.description?.text,
  //     ]
  //       .filter(Boolean)
  //       .join('\n')
  //   }
  // }
  // return m
}

export function mapThread(thread: FBThread): Thread {
  const participants = thread.all_participants?.nodes.map(e => {
    const p = mapParticipant(e.messaging_actor)
    p.isAdmin = !!thread.thread_admins?.find(item => item.id === p.id)
    return p
  }) || []

  const left_group = !thread.has_viewer_archived && thread.cannot_reply_reason === ThreadCannotReplyReason.VIEWER_NOT_SUBSCRIBED
  // user can send messages to archived chats unless they left
  const is_read_only = left_group
  || ((thread.cannot_reply_reason === ThreadCannotReplyReason.VIEWER_NOT_SUBSCRIBED)
  || thread.cannot_reply_reason === ThreadCannotReplyReason.BLOCKED
  || thread.cannot_reply_reason === ThreadCannotReplyReason.READ_ONLY)
  return {
    _original: stringifyWithBigInt(thread),
    id: getThreadIdFromThread(thread.thread_key),
    title: thread.name,
    description: null,
    imgURL: thread.image?.uri,
    isUnread: thread.unread_count > 0 && !(thread.has_viewer_archived && !thread.can_viewer_reply && thread.cannot_reply_reason === 'VIEWER_NOT_SUBSCRIBED'),
    isReadOnly: is_read_only,
    isArchived: thread.has_viewer_archived,
    isPinned: thread.is_pinned,
    messages: {
      items: [],
      hasMore: true,
    },
    participants: {
      items: participants,
      hasMore: false,
    },
    lastMessageSnippet: thread.last_message?.nodes?.[0]?.snippet,
    timestamp: new Date(+thread.updated_time_precise),
    type: thread.is_group_thread ? 'group' : 'single',
  }
}

export const mapUserPresence = (item: PresenceItem) => {
  const presence: UserPresence = {
    userID: item.userId.toString(),
    status: item.activeStatus ? 'online' : 'offline',
    lastActive: new Date(item.lastSeen),
  }
  return presence
}

export const mapMessages = (messages: FBExtendedMessage[], threadID: string) =>
  messages
    .map(m => mapMessage(m, threadID))
    .sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime())

export const mapThreads = (threads: FBThread[]) => threads.map(mapThread)
