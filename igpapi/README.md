# NodeJS Instagram private API client

---

# Install

The compiled npm libraries are hosted on gemfury (private npm repositories provider).
In order to install libraries from gemfury you need to execute 2 commands.

```bash
npm config set @textshq:registry https://npm.fury.io/igpapi/
npm login --scope=@textshq
```

After that you should be able to install gemfury packages just like any other packages.
If you need to install android package

```bash
npm install @textshq/android
```

If you need chrome

```bash
npm install @textshq/chrome
```

And so on. You can see the full list of libraries [here](https://manage.fury.io/dashboard/igpapi)

# License token

You should provide license token to start the library.
The token should be provided as the `IGPAPI_LICENCE_KEY` environment variable like this

```
IGPAPI_LICENCE_KEY=token
```

You could read more about environment variables [here](https://medium.com/the-node-js-collection/making-your-node-js-work-everywhere-with-environment-variables-2da8cdf6e786)

# Debugging

In order to get debug infos provided by the library, you can enable debugging.
The prefix for this library is `ig`.
To get all debug logs (_recommended_) set the namespace to `ig:*`.

#### Node

In node you only have to set the environment variable `DEBUG` to the desired namespace.
[Further information](https://github.com/visionmedia/debug#environment-variables)

#### Browser

In the browser you have to set `localStorage.debug` to the desired namespace.
[Further information](https://github.com/visionmedia/debug#browser-support)

# Contribution

If you need features that is not implemented - feel free to implement and create PRs!

Plus we need some documentation, so if you are good in it - you are welcome.

Setting up your environment is described [here](CONTRIBUTING.md).

# Useful links

[instagram-id-to-url-segment](https://www.npmjs.com/package/instagram-id-to-url-segment) - convert the image url fragment to the media ID
