import 'dotenv/config';
import { sign } from 'jsonwebtoken';
import inquirer from 'inquirer';
import clipboardy from 'clipboardy';

(async () => {
  const { subject, expiresIn, issuer } = await inquirer.prompt([
    {
      type: 'input',
      name: 'subject',
      message: 'Enter name',
    },
    {
      type: 'input',
      name: 'expiresIn',
      message: 'Enter expiresIn (human format)',
    },
    {
      type: 'input',
      name: 'issuer',
      message: 'Enter license checker server url (or none)',
      default: 'http://license.igpapi.com/check',
      choices: ['none', 'http://license.igpapi.com/check'],
    },
  ]);
  const key = sign({}, process.env.IGPAPI_LICENSE_PRIVATE_KEY, {
    algorithm: 'ES256',
    ...(issuer === 'none' ? {} : { issuer }),
    expiresIn,
    subject,
  });
  console.log(key);
  clipboardy.writeSync(key);
})();
