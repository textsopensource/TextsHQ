import { writeFile } from 'fs/promises';
import { json2ts } from 'json-ts/dist';
import { camelCase, merge, upperFirst } from 'lodash';

export class JsonTs {
  json: any = {};

  postfix = 'response';

  constructor(private dirname: string, private name: string) {}

  static async generate(json: any, dirname: string, name: string) {
    const jsonTs = new JsonTs(dirname, name);
    jsonTs.add(json);
    await jsonTs.generate();
  }

  add(json: any) {
    merge(this.json, json);
  }

  async generate() {
    const interfaceName = upperFirst(camelCase(`${this.name}-${this.postfix}`));
    const interfaces = json2ts(JSON.stringify(this.json), {
      prefix: interfaceName,
      rootName: ' ',
    }).replace(/interface/g, 'export interface');
    await writeFile(`${this.dirname}/${this.name}.${this.postfix}.ts`, interfaces);
  }
}
