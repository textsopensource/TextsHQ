/* eslint-disable no-console */
import { promises } from 'fs';
import { WebIgpapi } from '@textshq/web';
import { WebMqttClient } from '@textshq/web-mqtt';

const ig = new WebIgpapi();
const mqtt = new WebMqttClient(ig);

const STATE_FILE = 'web.state.json';

(async () => {
  await ig.simulate.bootstrap({
    login: {
      username: process.env.IG_USERNAME,
      password: process.env.IG_PASSWORD,
    },
    saveState: ({ application, ...state }) => promises.writeFile(`./tools/${STATE_FILE}`, JSON.stringify(state)),
    state: promises.readFile(`./tools/${STATE_FILE}`, { encoding: 'utf8' }).catch(() => undefined),
    isMobile: true,
  });
  mqtt.message$.subscribe(logEvent('message'));
  mqtt.activityIndicator$.subscribe(logEvent('activityIndicator'));
  mqtt.appPresence$.subscribe(logEvent('appPresence'));
  mqtt.realtimeSub$.subscribe(logEvent('realtimeSub'));
  mqtt.realtimeDirect$.subscribe(logEvent('realtimeDirect'));
  mqtt.zeroProvision$.subscribe(logEvent('zeroProvision'));
  await mqtt.connect({});
  console.log('post');

  const {
    items: [{ id: media_id }],
  } = await ig.feed.timeline().next();
  const {
    items: [{ thread_id }],
  } = await ig.feed.inbox().next();

  setTimeout(async () => {
    console.log(await mqtt.directCommands.indicateActivity({ thread_id }));
    console.log(await mqtt.directCommands.sendText({ thread_id, text: 'hi :)' }));
    const ack = await mqtt.directCommands.sendMedia({ thread_id, media_id });
    console.log(await mqtt.directCommands.sendReaction({ thread_id, item_id: ack.payload.item_id }));
  }, 1000);
})();

function logEvent(name) {
  return data => console.log(`${name}: ${JSON.stringify(data, undefined, 2)}`);
}
