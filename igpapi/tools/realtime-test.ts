/* tslint:disable:no-console */
import { AndroidIgpapi } from '@textshq/android';
import 'dotenv/config';
import { AndroidRealtimeClient } from '@textshq/android-realtime';
import { promises } from 'fs';
import { FbnsClient } from '@textshq/fbns';
import { GraphQLSubscriptions, SkywalkerSubscriptions } from '@textshq/realtime-core';

const STATE_FILE = 'state2.json';

const ig = new AndroidIgpapi();

async function login() {
  // ig.state.device.generate(process.env.IG_USERNAME);
  // await ig.simulate.preLoginFlow();
  // await ig.account.login(process.env.IG_USERNAME, process.env.IG_PASSWORD);
  await ig.simulate.bootstrap({
    credentials: {
      username: process.env.IG_USERNAME,
      password: process.env.IG_PASSWORD,
    },
    state: promises.readFile(`./tools/${STATE_FILE}`, { encoding: 'utf8' }).catch(() => undefined),
    saveState: ({ ...state }) => promises.writeFile(`./tools/${STATE_FILE}`, JSON.stringify(state)),
  });
}

(async () => {
  await login();
  // const a = await ig.highlights.highlightsTray(await ig.user.getIdByUsername('instagram'));
  // console.log(a);

  //await setupFbns();
  await setupRealtime();
})();

// @ts-ignore - may be unused
async function setupRealtime() {
  const mqtt = new AndroidRealtimeClient(ig);
  mqtt.message$.subscribe(logEvent('message'));
  mqtt.activityIndicator$.subscribe(logEvent('activityIndicator'));
  mqtt.appPresence$.subscribe(logEvent('appPresence'));
  mqtt.realtimeSub$.subscribe(logEvent('realtimeSub'));
  mqtt.realtimeDirect$.subscribe(logEvent('realtimeDirect'));
  mqtt.zeroProvision$.subscribe(logEvent('zeroProvision'));
  mqtt.error$.subscribe(logEvent('error'));
  mqtt.warning$.subscribe(logEvent('warning'));
  await mqtt.connect({
    graphQlSubs: [
      GraphQLSubscriptions.getAppPresenceSubscription(),
      GraphQLSubscriptions.getClientConfigUpdateSubscription(),
      GraphQLSubscriptions.getDirectStatusSubscription(),
      //GraphQLSubscriptions.getZeroProvisionSubscription(ig.state.device.phoneId),
      GraphQLSubscriptions.getDirectTypingSubscription(ig.state.cookies.userId + 'aaaaaaa'),
      //GraphQLSubscriptions.getAsyncAdSubscription(ig.state.cookies.userId),
    ],
    skywalkerSubs: [
      SkywalkerSubscriptions.directSub(ig.state.cookies.userId),
      SkywalkerSubscriptions.liveSub(ig.state.cookies.userId),
    ],
    irisData: await ig.feed.directInbox().request(),
  });
  console.log('post');

  /*const {
    raw: {
      broadcasts: [broadcast],
    },
  } = await ig.feed.reelsTray().next();
    await mqtt.graphQlSub([
      //GraphQLSubscriptions.getLiveRealtimeCommentsSubscription(broadcast.id),
      //GraphQLSubscriptions.getLiveTypingIndicatorSubscription(broadcast.id),
    ]),*/
}

// @ts-ignore - may be unused
async function setupFbns() {
  const fbns = new FbnsClient(ig);
  fbns.push$.subscribe(logEvent('push'));
  fbns.auth$.subscribe(logEvent('auth'));
  fbns.error$.subscribe(logRawEvent('error'));
  fbns.warning$.subscribe(logRawEvent('warning'));
  console.log('fbns:pre');
  await fbns.connect();
  console.log('fbns:post');
}

function logEvent(name) {
  return data => console.log(`${name}: ${JSON.stringify(data, undefined, 2)}`);
}

function logRawEvent(name) {
  return data => console.log(name, data);
}
