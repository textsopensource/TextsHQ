export const INSTAGRAM_PACKAGE_NAME = 'com.instagram.android';

export const FbnsTopics = {
  FBNS_MESSAGE: {
    id: '76',
    path: '/fbns_msg',
  },
  FBNS_REG_REQ: {
    id: '79',
    path: '/fbns_reg_req',
  },
  FBNS_REG_RESP: {
    id: '80',
    path: '/fbns_reg_resp',
  },
  FBNS_EXP_LOGGING: {
    id: '231',
    path: '/fbns_exp_logging',
  },
  PP: {
    id: '34',
    path: '/pp',
  },
};

export const FBNS = {
  PACKAGE: 'com.instagram.android',
  APP_ID: '567310203415052',
  HOST_NAME_V6: 'mqtts://mqtt-mini.facebook.com:443',
  CLIENT_CAPABILITIES: 439,
  ENDPOINT_CAPABILITIES: 128,
  CLIENT_STACK: 3,
  PUBLISH_FORMAT: 1,
};
