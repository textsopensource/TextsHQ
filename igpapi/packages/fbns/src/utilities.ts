import { AndroidIgpapi } from '@textshq/android';

export function notUndefined<T>(a: T | undefined): a is T {
  return typeof a !== 'undefined';
}

export function createUserAgent(ig: AndroidIgpapi) {
  const [androidVersion, , resolution, manufacturer, deviceName] = ig.state.device.descriptor.split('; ');
  const [width, height] = resolution.split('x');
  const params = {
    FBAN: 'MQTT',
    FBAV: ig.state.application.APP_VERSION,
    FBBV: ig.state.application.APP_VERSION,
    FBDM: `{density=4.0,width=${width},height=${height}`,
    FBLC: ig.state.device.language,
    FBCR: 'Android',
    FBMF: manufacturer.trim(),
    FBBD: 'Android',
    FBPN: 'com.instagram.android',
    FBDV: deviceName.trim(),
    FBSV: androidVersion.split('/')[1],
    FBLR: '0',
    FBBK: '1',
    FBCA: 'x86:armeabi-v7a',
  };
  return `[${Object.entries(params)
    .map(p => p.join('/'))
    .join(';')}]`;
}
