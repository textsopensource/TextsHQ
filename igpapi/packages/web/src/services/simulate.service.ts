import { Service } from 'typedi';
import { Resolvable, SimulateBootstrapOptions } from '../types';
import debug from 'debug';
import { AccountRepository } from '../repositories/account.repository';
import { DataRepository } from '../repositories/data.repository';
import { WebState } from '../core/web.state';
import { WebHttp } from '../core/web.http';
import { CheckpointRequiredError } from '../errors/checkpoint-required.error';

@Service()
export class SimulateService {
  private static simulateDebug = debug('ig:simulate');

  constructor(
    private state: WebState,
    private http: WebHttp,
    private account: AccountRepository,
    private data: DataRepository,
  ) {}

  private static async resolve<T>(res: Resolvable<T>): Promise<T> {
    if (typeof res === 'function') {
      // @ts-ignore - T should not be of type T & Function
      return res();
    }
    return res;
  }

  public async bootstrap(options: SimulateBootstrapOptions) {
    if (options.saveState) {
      this.http.end$.subscribe(async () => {
        try {
          await options.saveState?.(this.state.toJSON());
        } catch (e) {
          SimulateService.simulateDebug(`saveState() failed! ${e.toString()}\n${e.stack}`);
        }
      });
    }
    this.state.device.isMobile = options.isMobile;
    if (options.device) {
      const device = await SimulateService.resolve(options.device);
      if (typeof device === 'string') {
        this.state.device.generate(device);
      } else {
        this.state.deserialize({ device });
      }
    } else if (options.login) {
      this.state.device.generate(options.login.username);
    } else {
      throw new Error('Device generation failed (pass device or login)');
    }
    if (options.state) {
      this.state.deserialize((await SimulateService.resolve(options.state)) ?? {});
    }
    try {
      await this.data.sharedData();
      if (options.login) {
        if (!this.state.session.sharedData?.config?.viewer) {
          await this.account.preLogin();
          await this.account.login(options.login.username, options.login.password ?? '');
        }
      }
    } catch (e) {
      if (e instanceof CheckpointRequiredError) {
        const challenge = await this.http.body<any>({
          url: e.response.body.checkpoint_url,
          form: {
            choice: '1',
          },
        });
        if (!options.onChallenge) throw e;

        const code = await options.onChallenge({ challenge, error: e });
        await this.http.body({
          url: challenge.navigation.forward,
          form: {
            security_code: code,
          },
        });
      }
    }

    if (!this.state.session.sharedData) {
      await this.data.sharedData();
    }
  }
}
