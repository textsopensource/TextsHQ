import { Feed } from '@textshq/core';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';
import { DiscoverFeedNode, DiscoverFeedRootObject } from '../responses';

@Service({ transient: true, global: true })
export class DiscoverFeed extends Feed<DiscoverFeedRootObject, DiscoverFeedNode> {
  first?: number;
  after?: string;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: DiscoverFeedRootObject): DiscoverFeedNode[] {
    return raw.data.user.edge_web_discover_media.edges.map(edge => edge.node);
  }

  protected handle(response: DiscoverFeedRootObject): boolean {
    const { has_next_page, end_cursor } = response.data.user.edge_web_discover_media.page_info;
    this.after = end_cursor;
    return has_next_page;
  }

  protected async request(): Promise<DiscoverFeedRootObject> {
    return this._graphql.query({
      queryHash: QueryId.DiscoverFeed,
      variables: {
        first: this.first,
        after: this.after,
        // rank_token?
      },
    });
  }
}
