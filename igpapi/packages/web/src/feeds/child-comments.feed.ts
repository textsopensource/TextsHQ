import { Service } from 'typedi';
import { Feed } from '@textshq/core';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { FeedChildCommentsNode, FeedChildCommentsRootObject } from '../responses';
import { QueryId } from '../core/web.constants';

@Service({ transient: true, global: true })
export class ChildCommentsFeed extends Feed<FeedChildCommentsRootObject, FeedChildCommentsNode> {
  commentId: string;
  cursor?: string;
  first?: number = 6;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: FeedChildCommentsRootObject) {
    return raw.data.comment.edge_threaded_comments.edges.map(edge => edge.node);
  }

  protected async request() {
    return this._graphql.query<FeedChildCommentsRootObject>({
      queryHash: QueryId.ChildComments,
      variables: {
        comment_id: this.commentId,
        first: this.first,
        after: this.cursor,
      },
    });
  }

  protected handle(response: any) {
    const { end_cursor, has_next_page } = response.data.comment.edge_threaded_comments.page_info;
    this.cursor = end_cursor;
    return has_next_page;
  }
}
