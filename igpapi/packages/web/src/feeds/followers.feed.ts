import { Service } from 'typedi';
import { FollowFeed } from './follow.feed';
import { QueryId } from '../core/web.constants';
import { FeedFollowEdge, FeedFollowRootObject } from '../responses';

@Service({ transient: true, global: true })
export class FollowersFeed extends FollowFeed {
  protected get queryId() {
    return QueryId.UserFollowers;
  }

  protected getEdge(response: FeedFollowRootObject): FeedFollowEdge {
    return response.data.user.edge_followed_by as FeedFollowEdge;
  }
}
