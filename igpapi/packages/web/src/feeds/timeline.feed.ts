import { Feed } from '@textshq/core';

import { GraphqlRepository } from '../repositories/graphql.repository';
import { GraphqlFeedNode, GraphqlFeedRootObject } from '../responses/graphql.feed';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class TimelineFeed extends Feed<GraphqlFeedRootObject, GraphqlFeedNode> {
  cachedFeedItemIds?: string[] = [];
  fetchCommentCount?: number = 4;
  fetchLike?: number = 3;
  fetchMediaItemCount?: number = 12;
  fetchMediaItemCursor?: string | null = null;
  hasStories?: boolean = false;
  hasThreadedComments?: boolean = true;

  constructor(private _graphQl: GraphqlRepository) {
    super();
  }

  items(raw: GraphqlFeedRootObject): GraphqlFeedNode[] {
    return raw.data.user.edge_web_feed_timeline?.edges.map(edge => edge.node) ?? [];
  }

  protected async request(): Promise<GraphqlFeedRootObject> {
    return this._graphQl.query({
      queryHash: QueryId.Feed,
      variables: {
        cached_feed_item_ids: this.cachedFeedItemIds,
        fetch_comment_count: this.fetchCommentCount,
        fetch_like: this.fetchLike,
        fetch_media_item_count: this.fetchMediaItemCount,
        fetch_media_item_cursor: this.fetchMediaItemCursor,
        has_stories: this.hasStories,
        has_threaded_comments: this.hasThreadedComments,
      },
    });
  }

  protected handle(response: GraphqlFeedRootObject) {
    this.fetchMediaItemCursor = response.data.user.edge_web_feed_timeline?.page_info.end_cursor;
    return !!response.data.user.edge_web_feed_timeline?.page_info.has_next_page;
  }
}
