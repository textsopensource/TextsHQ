import { Feed } from '@textshq/core';
import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { DirectInboxFeedRootObject, DirectInboxFeedThreadsItem } from '../responses';

@Service({ transient: true, global: true })
export class DirectInboxFeed extends Feed<DirectInboxFeedRootObject, DirectInboxFeedThreadsItem> {
  persistentBadging?: boolean = true;
  folder? = '';
  limit? = 20;
  threadMessageLimit? = 10;
  cursor?: string;

  constructor(private _http: WebHttp) {
    super();
  }

  items(raw: DirectInboxFeedRootObject): DirectInboxFeedThreadsItem[] {
    return raw.inbox.threads;
  }

  protected handle(response: DirectInboxFeedRootObject): boolean {
    const { has_older, oldest_cursor } = response.inbox;
    this.cursor = oldest_cursor;
    return has_older;
  }

  protected request(): Promise<DirectInboxFeedRootObject> {
    return this._http.body({
      url: '/direct_v2/web/inbox/',
      searchParams: {
        persistentBadging: this.persistentBadging ?? null,
        // if the cursor exists, only send the cursor (probably contains the information)
        ...(this.cursor
          ? {
              cursor: this.cursor,
            }
          : {
              folder: this.folder ?? null,
              limit: this.limit ?? null,
              thread_message_limit: this.threadMessageLimit ?? null,
            }),
      },
    });
  }
}
