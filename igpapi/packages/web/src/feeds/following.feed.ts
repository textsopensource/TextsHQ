import { Service } from 'typedi';
import { FollowFeed } from './follow.feed';
import { QueryId } from '../core/web.constants';
import { FeedFollowEdge, FeedFollowRootObject } from '../responses';

@Service({ transient: true, global: true })
export class FollowingFeed extends FollowFeed {
  protected get queryId() {
    return QueryId.UserFollowing;
  }

  protected getEdge(response: FeedFollowRootObject): FeedFollowEdge {
    return response.data.user.edge_follow as FeedFollowEdge;
  }
}
