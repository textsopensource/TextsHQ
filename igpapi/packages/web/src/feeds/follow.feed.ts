import { Feed } from '@textshq/core';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { Service } from 'typedi';
import { QueryId } from '../core/web.constants';
import { FeedFollowEdge, FeedFollowNode, FeedFollowRootObject } from '../responses/feed.follow';

@Service({ transient: true, global: true })
export abstract class FollowFeed extends Feed<FeedFollowRootObject, FeedFollowNode> {
  id: string;
  includeReel?: boolean = true;
  fetchMutual?: boolean = true;
  first?: number = 24;
  after?: string;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  protected abstract get queryId(): QueryId;

  items(raw: FeedFollowRootObject): any[] {
    return this.getEdge(raw).edges.map(edge => edge.node);
  }

  protected abstract getEdge(response: FeedFollowRootObject): FeedFollowEdge;

  protected handle(response: FeedFollowRootObject): boolean {
    const { has_next_page, end_cursor } = this.getEdge(response).page_info;
    this.after = end_cursor;
    return has_next_page;
  }

  protected async request(): Promise<FeedFollowRootObject> {
    return this._graphql.query({
      queryHash: this.queryId,
      variables: {
        id: this.id,
        include_reel: this.includeReel,
        fetch_mutual: this.fetchMutual,
        first: this.first,
        after: this.after,
      },
    });
  }
}
