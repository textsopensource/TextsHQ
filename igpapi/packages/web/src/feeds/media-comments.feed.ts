import { Feed } from '@textshq/core';
import { FeedMediaCommentsNode, FeedMediaCommentsRootObject } from '../responses';

import { GraphqlRepository } from '../repositories/graphql.repository';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class MediaCommentsFeed extends Feed<FeedMediaCommentsRootObject, FeedMediaCommentsNode> {
  shortcode: string;
  cursor?: string;
  /**
   * This value is original one. The maximum is 50.
   */
  first?: number = 12;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: FeedMediaCommentsRootObject) {
    return raw.data.shortcode_media.edge_media_to_parent_comment.edges.map(edge => edge.node);
  }

  protected async request() {
    return this._graphql.query<FeedMediaCommentsRootObject>({
      queryHash: QueryId.CommentRequest,
      variables: {
        shortcode: this.shortcode,
        first: this.first,
        after: this.cursor,
      },
    });
  }

  protected handle(response: any) {
    const { end_cursor, has_next_page } = response.data.shortcode_media.edge_media_to_parent_comment.page_info;
    this.cursor = end_cursor;
    return has_next_page;
  }
}
