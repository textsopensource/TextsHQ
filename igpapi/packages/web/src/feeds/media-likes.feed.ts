import { Feed } from '@textshq/core';
import { FeedMediaLikesNode, FeedMediaLikesRootObject } from '../responses';

import { GraphqlRepository } from '../repositories/graphql.repository';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class MediaLikesFeed extends Feed<FeedMediaLikesRootObject, FeedMediaLikesNode> {
  shortcode: string;
  cursor?: string;
  /**
   * This value is original one. The maximum is 50.
   */
  first?: number = 24;
  includeReel?: boolean = true;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: FeedMediaLikesRootObject) {
    return raw.data.shortcode_media.edge_liked_by.edges.map(edge => edge.node);
  }

  protected async request() {
    return this._graphql.query<FeedMediaLikesRootObject>({
      queryHash: QueryId.RequestLiked,
      variables: {
        shortcode: this.shortcode,
        include_ree: this.includeReel,
        first: this.first,
        after: this.cursor,
      },
    });
  }

  protected handle(response: FeedMediaLikesRootObject) {
    const { end_cursor, has_next_page } = response.data.shortcode_media.edge_liked_by.page_info;
    this.cursor = end_cursor;
    return has_next_page;
  }
}
