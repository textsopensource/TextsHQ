import { Feed } from '@textshq/core';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';
import { TagFeedNode, TagFeedRootObject } from '../responses/tag.feed';

@Service({ transient: true, global: true })
export class TagFeed extends Feed<TagFeedRootObject, TagFeedNode> {
  tagName: string;
  first?: number = 12;
  after?: string;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: TagFeedRootObject): TagFeedNode[] {
    return raw.data.hashtag.edge_hashtag_to_media.edges.map(edge => edge.node);
  }

  protected handle(response: TagFeedRootObject): boolean {
    const { has_next_page, end_cursor } = response.data.hashtag.edge_hashtag_to_media.page_info;
    this.after = end_cursor;
    return has_next_page;
  }

  protected async request(): Promise<TagFeedRootObject> {
    return this._graphql.query({
      queryHash: QueryId.TagFeed,
      variables: {
        tag_name: this.tagName,
        first: this.first,
        after: this.after,
      },
    });
  }
}
