import { Feed } from '@textshq/core';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { QueryId } from '../core/web.constants';
import { Service } from 'typedi';
import { FeedTaggedPostsNode, FeedTaggedPostsRootObject } from '../responses';

@Service({ transient: true, global: true })
export class TaggedPostsFeed extends Feed<FeedTaggedPostsRootObject, FeedTaggedPostsNode> {
  id: string;
  first?: number = 12;
  after?: string;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: FeedTaggedPostsRootObject): FeedTaggedPostsNode[] {
    return raw.data.user.edge_user_to_photos_of_you.edges.map(edge => edge.node);
  }

  protected handle(response: FeedTaggedPostsRootObject): boolean {
    const { has_next_page, end_cursor } = response.data.user.edge_user_to_photos_of_you.page_info;
    this.after = end_cursor;
    return has_next_page;
  }

  protected async request(): Promise<FeedTaggedPostsRootObject> {
    return this._graphql.query({
      queryHash: QueryId.TaggedPosts,
      variables: {
        id: this.id,
        first: this.first,
        after: this.after,
      },
    });
  }
}
