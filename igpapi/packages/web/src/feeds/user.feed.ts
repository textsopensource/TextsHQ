import { Feed } from '@textshq/core';
import { FeedUserNode, FeedUserRootObject } from '../responses';

import { GraphqlRepository } from '../repositories/graphql.repository';
import { Service } from 'typedi';
import { QueryId } from '../core/web.constants';

@Service({ transient: true, global: true })
export class UserFeed extends Feed<FeedUserRootObject, FeedUserNode> {
  id: string;
  after?: string;
  first?: number = 12;

  constructor(private _graphql: GraphqlRepository) {
    super();
  }

  items(raw: FeedUserRootObject) {
    return raw.data.user.edge_owner_to_timeline_media?.edges.map(edge => edge.node) ?? [];
  }

  protected async request() {
    return this._graphql.query<FeedUserRootObject>({
      queryHash: QueryId.RequestProfilePosts,
      variables: {
        id: this.id,
        cursor: this.after,
        first: this.first,
      },
    });
  }

  protected handle(response: FeedUserRootObject) {
    const { end_cursor, has_next_page } = response.data.user.edge_owner_to_timeline_media?.page_info ?? {};
    this.after = end_cursor;
    return !!has_next_page;
  }
}
