import { Feed } from '@textshq/core';
import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { DirectThreadFeedItemsItem, DirectThreadFeedRootObject } from '../responses';

@Service({ transient: true, global: true })
export class DirectThreadFeed extends Feed<DirectThreadFeedRootObject, DirectThreadFeedItemsItem> {
  threadId: string;
  cursor?: string;

  constructor(private _http: WebHttp) {
    super();
  }

  items(raw: DirectThreadFeedRootObject): DirectThreadFeedItemsItem[] {
    return raw.thread.items;
  }

  protected handle(response: DirectThreadFeedRootObject): boolean {
    // TODO: pagination in the other direction
    const { has_older, oldest_cursor } = response.thread;
    this.cursor = oldest_cursor;
    return has_older;
  }

  protected request(): Promise<DirectThreadFeedRootObject> {
    return this._http.body({
      url: `/direct_v2/web/threads/${this.threadId}/`,
      searchParams: {
        cursor: this.cursor ?? null,
      },
    });
  }
}
