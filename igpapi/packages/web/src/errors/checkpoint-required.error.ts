import { IgpapiChallengeRequiredError, IgResponseError } from '@textshq/core';
import { CheckpointErrorRootObject } from '../responses';

export class CheckpointRequiredError extends IgResponseError<CheckpointErrorRootObject>
  implements IgpapiChallengeRequiredError {
  url(): string {
    return this.response.body.checkpoint_url;
  }
}
