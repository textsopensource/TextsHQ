export interface RepositoryPublicUserRootObject {
  logging_page_id: string;
  show_suggested_profiles: boolean;
  show_follow_dialog: boolean;
  graphql: RepositoryPublicUserGraphql;
  toast_content_on_load: null;
}

export interface RepositoryPublicUserGraphql {
  user: RepositoryPublicUserUser;
}

export interface RepositoryPublicUserUser {
  biography: string;
  blocked_by_viewer: boolean;
  country_block: boolean;
  external_url: null;
  external_url_linkshimmed: null;
  edge_followed_by: RepositoryPublicUserEdge_followed_by;
  followed_by_viewer: boolean;
  edge_follow: RepositoryPublicUserEdge_follow;
  follows_viewer: boolean;
  full_name: string;
  has_channel: boolean;
  has_blocked_viewer: boolean;
  highlight_reel_count: number;
  has_requested_viewer: boolean;
  id: string;
  is_business_account: boolean;
  is_joined_recently: boolean;
  business_category_name: null;
  is_private: boolean;
  is_verified: boolean;
  edge_mutual_followed_by: RepositoryPublicUserEdge_mutual_followed_by;
  profile_pic_url: string;
  profile_pic_url_hd: string;
  requested_by_viewer: boolean;
  username: string;
  connected_fb_page: null;
  edge_felix_video_timeline: RepositoryPublicUserEdge_felix_video_timeline;
  edge_owner_to_timeline_media: RepositoryPublicUserEdge_owner_to_timeline_media;
  edge_saved_media: RepositoryPublicUserEdge_saved_media;
  edge_media_collections: RepositoryPublicUserEdge_media_collections;
}

export interface RepositoryPublicUserEdge_followed_by {
  count: number;
}

export interface RepositoryPublicUserEdge_follow {
  count: number;
}

export interface RepositoryPublicUserEdge_mutual_followed_by {
  count: number;
  edges: any[];
}

export interface RepositoryPublicUserEdge_felix_video_timeline {
  count: number;
  page_info: RepositoryPublicUserPage_info;
  edges: RepositoryPublicUserEdgesItem[];
}

export interface RepositoryPublicUserPage_info {
  has_next_page: boolean;
  end_cursor: null | string;
}

export interface RepositoryPublicUserEdgesItem {
  node: RepositoryPublicUserNode;
}

export interface RepositoryPublicUserNode {
  __typename?: string;
  id?: string;
  edge_media_to_caption?: RepositoryPublicUserEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: RepositoryPublicUserEdge_media_to_comment;
  comments_disabled?: boolean;
  taken_at_timestamp?: number;
  dimensions?: RepositoryPublicUserDimensions;
  display_url?: string;
  edge_liked_by?: RepositoryPublicUserEdge_liked_by;
  edge_media_preview_like?: RepositoryPublicUserEdge_media_preview_like;
  location?: null | RepositoryPublicUserLocation;
  gating_info?: null;
  fact_check_overall_rating?: null;
  fact_check_information?: null;
  media_preview?: string | null;
  owner?: RepositoryPublicUserOwner;
  thumbnail_src?: string;
  thumbnail_resources?: RepositoryPublicUserThumbnailResourcesItem[];
  is_video?: boolean;
  felix_profile_grid_crop?: null | RepositoryPublicUserFelix_profile_grid_crop;
  encoding_status?: null;
  is_published?: boolean;
  product_type?: string;
  title?: string;
  video_duration?: number;
  video_view_count?: number;
  text?: string;
  accessibility_caption?: string;
}

export interface RepositoryPublicUserEdge_media_to_caption {
  edges: RepositoryPublicUserEdgesItem[];
}

export interface RepositoryPublicUserEdge_media_to_comment {
  count: number;
}

export interface RepositoryPublicUserDimensions {
  height: number;
  width: number;
}

export interface RepositoryPublicUserEdge_liked_by {
  count: number;
}

export interface RepositoryPublicUserEdge_media_preview_like {
  count: number;
}

export interface RepositoryPublicUserOwner {
  id: string;
  username: string;
}

export interface RepositoryPublicUserThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface RepositoryPublicUserFelix_profile_grid_crop {
  crop_left: number;
  crop_right: number;
  crop_top: number;
  crop_bottom: number;
}

export interface RepositoryPublicUserEdge_owner_to_timeline_media {
  count: number;
  page_info: RepositoryPublicUserPage_info;
  edges: RepositoryPublicUserEdgesItem[];
}

export interface RepositoryPublicUserLocation {
  id: string;
  has_public_page: boolean;
  name: string;
  slug: string;
}

export interface RepositoryPublicUserEdge_saved_media {
  count: number;
  page_info: RepositoryPublicUserPage_info;
  edges: any[];
}

export interface RepositoryPublicUserEdge_media_collections {
  count: number;
  page_info: RepositoryPublicUserPage_info;
  edges: any[];
}
