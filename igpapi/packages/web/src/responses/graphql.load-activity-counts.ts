export interface GraphqlLoadActivityCountsRootObject {
  data: GraphqlLoadActivityCountsData;
  status: string;
}

export interface GraphqlLoadActivityCountsData {
  user: GraphqlLoadActivityCountsUser;
}

export interface GraphqlLoadActivityCountsUser {
  edge_activity_count: GraphqlLoadActivityCountsEdge_activity_count;
}

export interface GraphqlLoadActivityCountsEdge_activity_count {
  edges: GraphqlLoadActivityCountsEdgesItem[];
}

export interface GraphqlLoadActivityCountsEdgesItem {
  node: GraphqlLoadActivityCountsNode;
}

export interface GraphqlLoadActivityCountsNode {
  comment_likes: number;
  comments: number;
  likes: number;
  relationships: number;
  usertags: number;
}
