export interface FeedChildCommentsRootObject {
  data: FeedChildCommentsData;
  status: string;
}

export interface FeedChildCommentsData {
  comment: FeedChildCommentsComment;
}

export interface FeedChildCommentsComment {
  id: string;
  edge_threaded_comments: FeedChildCommentsEdge_threaded_comments;
}

export interface FeedChildCommentsEdge_threaded_comments {
  count: number;
  page_info: FeedChildCommentsPage_info;
  edges: FeedChildCommentsEdgesItem[];
}

export interface FeedChildCommentsPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface FeedChildCommentsEdgesItem {
  node: FeedChildCommentsNode;
}

export interface FeedChildCommentsNode {
  id: string;
  text: string;
  created_at: number;
  owner: FeedChildCommentsOwner;
  viewer_has_liked: boolean;
  edge_liked_by: FeedChildCommentsEdge_liked_by;
  is_restricted_pending: boolean;
}

export interface FeedChildCommentsOwner {
  id: string;
  profile_pic_url: string;
  username: string;
}

export interface FeedChildCommentsEdge_liked_by {
  count: number;
}
