export interface DirectThreadFeedRootObject {
  thread: DirectThreadFeedThread;
  status: string;
}

export interface DirectThreadFeedThread {
  thread_id: string;
  thread_v2_id: string;
  users: DirectThreadFeedUsersItem[];
  left_users: any[];
  admin_user_ids: any[];
  items: DirectThreadFeedItemsItem[];
  last_activity_at: number;
  muted: boolean;
  is_pin: boolean;
  named: boolean;
  canonical: boolean;
  pending: boolean;
  archived: boolean;
  thread_type: string;
  viewer_id: number;
  thread_title: string;
  folder: number;
  vc_muted: boolean;
  is_group: boolean;
  mentions_muted: boolean;
  approval_required_for_new_members: boolean;
  input_mode: number;
  business_thread_folder: number;
  read_state: number;
  last_non_sender_item_at: number;
  assigned_admin_id: number;
  inviter: DirectThreadFeedInviter;
  has_older: boolean;
  has_newer: boolean;
  last_seen_at: DirectThreadFeedLast_seen_at;
  newest_cursor: string;
  oldest_cursor: string;
  next_cursor: string;
  prev_cursor: string;
  last_permanent_item: DirectThreadFeedLast_permanent_item;
}

export interface DirectThreadFeedUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectThreadFeedFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectThreadFeedFriendship_status {
  following: boolean;
  blocking: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
  followed_by?: boolean;
  muting?: boolean;
}

export interface DirectThreadFeedItemsItem {
  item_id: string;
  user_id: number;
  timestamp: number;
  hide_in_thread?: number;
  item_type: string;
  action_log?: DirectThreadFeedAction_log;
  client_context: string;
  reactions?: DirectThreadFeedReactions;
  raven_media?: DirectThreadFeedRaven_media;
  reply_type?: string;
  expiring_media_action_summary?: DirectThreadFeedExpiring_media_action_summary;
  seen_user_ids?: string[];
  view_mode?: string;
  seen_count?: number;
  replay_expiring_at_us?: number;
  text?: string;
  voice_media?: DirectThreadFeedVoice_media;
  animated_media?: DirectThreadFeedAnimated_media;
  media?: DirectThreadFeedMedia;
  text_entities?: DirectThreadFeedText_entities;
  link?: DirectThreadFeedLink;
  like?: string;
}

export interface DirectThreadFeedAction_log {
  description: string;
  bold: DirectThreadFeedBoldItem[];
}

export interface DirectThreadFeedBoldItem {
  start: number;
  end: number;
}

export interface DirectThreadFeedReactions {
  likes: DirectThreadFeedLikesItem[];
  likes_count: number;
}

export interface DirectThreadFeedLikesItem {
  sender_id: number;
  timestamp: number;
  client_context: string;
}

export interface DirectThreadFeedRaven_media {
  media_type: number;
}

export interface DirectThreadFeedExpiring_media_action_summary {
  type: string;
  timestamp: number;
  count: number;
}

export interface DirectThreadFeedVoice_media {
  media: DirectThreadFeedMedia;
  expiring_media_action_summary: DirectThreadFeedExpiring_media_action_summary;
  seen_user_ids: any[];
  view_mode: string;
  seen_count: number;
  replay_expiring_at_us: null;
}

export interface DirectThreadFeedMedia {
  id: string | number;
  media_type: number;
  product_type?: string;
  audio?: DirectThreadFeedAudio;
  organic_tracking_token?: string;
  user?: DirectThreadFeedUser;
  image_versions2?: DirectThreadFeedImage_versions2;
  original_width?: number;
  original_height?: number;
  video_versions?: DirectThreadFeedVideoVersionsItem[];
}

export interface DirectThreadFeedAudio {
  audio_src: string;
  duration: number;
  waveform_data: number[];
  waveform_sampling_frequency_hz: number;
}

export interface DirectThreadFeedUser {
  pk?: number;
  friendship_status?: DirectThreadFeedFriendship_status;
  username: string;
  is_verified?: boolean;
}

export interface DirectThreadFeedAnimated_media {
  id: string;
  images: DirectThreadFeedImages;
  is_random: boolean;
  is_sticker: boolean;
  user: DirectThreadFeedUser;
}

export interface DirectThreadFeedImages {
  fixed_height: DirectThreadFeedFixed_height;
}

export interface DirectThreadFeedFixed_height {
  height: string;
  mp4: string;
  mp4_size: string;
  size: string;
  url: string;
  webp: string;
  webp_size: string;
  width: string;
}

export interface DirectThreadFeedImage_versions2 {
  candidates: DirectThreadFeedCandidatesItem[];
}

export interface DirectThreadFeedCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface DirectThreadFeedText_entities {
  mentioned_user_ids: number[];
}

export interface DirectThreadFeedLink {
  text: string;
  link_context: DirectThreadFeedLink_context;
  client_context: string;
  mutation_token: string;
}

export interface DirectThreadFeedLink_context {
  link_url: string;
  link_title: string;
  link_summary: string;
  link_image_url: string;
}

export interface DirectThreadFeedVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}

export interface DirectThreadFeedInviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
}

export type DirectThreadFeedLast_seen_at = {
  [x: string]: DirectThreadFeedLastSeenAtUser;
};

export interface DirectThreadFeedLastSeenAtUser {
  timestamp: string;
  item_id: string;
}

export interface DirectThreadFeedLast_permanent_item {
  item_id: string;
  user_id: number;
  timestamp: number;
  hide_in_thread: number;
  item_type: string;
  action_log: DirectThreadFeedAction_log;
  client_context: string;
}
