export interface GraphqlFeedPageExtrasRootObject {
  data: GraphqlFeedPageExtrasData;
  status: string;
}

export interface GraphqlFeedPageExtrasData {
  user: GraphqlFeedPageExtrasUser;
}

export interface GraphqlFeedPageExtrasUser {
  feed_reels_tray?: GraphqlFeedPageExtrasFeed_reels_tray;
  id?: string;
  profile_pic_url?: string;
  username?: string;
  followed_by_viewer?: boolean;
  requested_by_viewer?: boolean;
}

export interface GraphqlFeedPageExtrasFeed_reels_tray {
  edge_reels_tray_to_reel: GraphqlFeedPageExtrasEdge_reels_tray_to_reel;
}

export interface GraphqlFeedPageExtrasEdge_reels_tray_to_reel {
  edges: GraphqlFeedPageExtrasEdgesItem[];
}

export interface GraphqlFeedPageExtrasEdgesItem {
  node: GraphqlFeedPageExtrasNode;
}

export interface GraphqlFeedPageExtrasNode {
  __typename: string;
  has_besties_media: boolean;
  has_pride_media: boolean;
  id: string;
  can_reply: boolean;
  can_reshare: boolean;
  expiring_at: number;
  latest_reel_media: number;
  muted: boolean;
  supports_reel_reactions: null;
  items: GraphqlFeedPageExtrasItemsItem[] | null;
  prefetch_count: number;
  ranked_position: number;
  seen: null;
  seen_ranked_position: number;
  user: GraphqlFeedPageExtrasUser;
  owner: GraphqlFeedPageExtrasOwner;
}

export interface GraphqlFeedPageExtrasItemsItem {
  __typename: string;
  audience: string;
  id: string;
  dimensions: GraphqlFeedPageExtrasDimensions;
  story_view_count: null;
  edge_story_media_viewers: GraphqlFeedPageExtrasEdge_story_media_viewers;
  display_resources: GraphqlFeedPageExtrasDisplayResourcesItem[];
  display_url: string;
  media_preview: string;
  gating_info: null;
  fact_check_overall_rating: null;
  fact_check_information: null;
  taken_at_timestamp: number;
  expiring_at_timestamp: number;
  story_cta_url: string;
  is_video: boolean;
  owner: GraphqlFeedPageExtrasOwner;
  tracking_token: string;
  has_audio: boolean;
  overlay_image_resources: null;
  video_duration: number;
  video_resources: GraphqlFeedPageExtrasVideoResourcesItem[];
  tappable_objects: any[];
  story_app_attribution: null;
  edge_media_to_sponsor_user: GraphqlFeedPageExtrasEdge_media_to_sponsor_user;
}

export interface GraphqlFeedPageExtrasDimensions {
  height: number;
  width: number;
}

export interface GraphqlFeedPageExtrasEdge_story_media_viewers {
  count: number;
  edges: any[];
  page_info: GraphqlFeedPageExtrasPage_info;
}

export interface GraphqlFeedPageExtrasPage_info {
  has_next_page: boolean;
  end_cursor: null;
}

export interface GraphqlFeedPageExtrasDisplayResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface GraphqlFeedPageExtrasOwner {
  id: string;
  profile_pic_url: string;
  username: string;
  followed_by_viewer: boolean;
  requested_by_viewer: boolean;
  __typename?: string;
}

export interface GraphqlFeedPageExtrasVideoResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
  mime_type: string;
  profile: string;
}

export interface GraphqlFeedPageExtrasEdge_media_to_sponsor_user {
  edges: any[];
}
