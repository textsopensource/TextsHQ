export interface DataSharedDataRootObject {
  config: DataSharedDataConfig;
  country_code: string;
  language_code: string;
  locale: string;
  entry_data: DataSharedDataEntry_data;
  hostname: string;
  is_whitelisted_crawl_bot: boolean;
  deployment_stage: string;
  platform: string;
  nonce: string;
  mid_pct: number;
  zero_data: DataSharedDataZero_data;
  cache_schema_version: number;
  server_checks: DataSharedDataServer_checks;
  knobx: DataSharedDataKnobx;
  to_cache: DataSharedDataTo_cache;
  device_id: string;
  encryption: DataSharedDataEncryption;
  rollout_hash: string;
  bundle_variant: string;
  is_canary: boolean;
}

export interface DataSharedDataConfig {
  csrf_token: string;
  viewer: DataSharedDataViewer;
  viewerId: string;
}

export interface DataSharedDataViewer {
  biography: string;
  external_url: null;
  full_name: string;
  has_phone_number: boolean;
  has_profile_pic: boolean;
  has_tabbed_inbox: boolean;
  id: string;
  is_joined_recently: boolean;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_url_hd: string;
  username: string;
  badge_count: string;
}

export interface DataSharedDataEntry_data {}

export interface DataSharedDataZero_data {}

export interface DataSharedDataServer_checks {
  hfe: boolean;
}

export interface DataSharedDataKnobx {
  4: boolean;
  17: boolean;
  20: boolean;
  22: boolean;
  23: boolean;
  24: boolean;
}

export interface DataSharedDataTo_cache {
  gatekeepers: { [x: number]: boolean };
  qe: DataSharedDataQe;
  probably_has_app: boolean;
  cb: boolean;
}

export interface DataSharedDataQe {
  0: DataSharedData0;
  2: DataSharedData2;
  4: DataSharedData4;
  5: DataSharedData5;
  6: DataSharedData6;
  10: DataSharedData10;
  12: DataSharedData12;
  13: DataSharedData13;
  16: DataSharedData16;
  17: DataSharedData17;
  19: DataSharedData19;
  21: DataSharedData21;
  22: DataSharedData22;
  23: DataSharedData23;
  25: DataSharedData25;
  26: DataSharedData26;
  28: DataSharedData28;
  29: DataSharedData29;
  30: DataSharedData30;
  31: DataSharedData31;
  33: DataSharedData33;
  34: DataSharedData34;
  35: DataSharedData35;
  36: DataSharedData36;
  37: DataSharedData37;
  39: DataSharedData39;
  41: DataSharedData41;
  42: DataSharedData42;
  43: DataSharedData43;
  44: DataSharedData44;
  45: DataSharedData45;
  46: DataSharedData46;
  47: DataSharedData47;
  49: DataSharedData49;
  50: DataSharedData50;
  53: DataSharedData53;
  54: DataSharedData54;
  55: DataSharedData55;
  56: DataSharedData56;
  58: DataSharedData58;
  59: DataSharedData59;
  62: DataSharedData62;
  64: DataSharedData64;
  65: DataSharedData65;
  66: DataSharedData66;
  67: DataSharedData67;
  68: DataSharedData68;
  69: DataSharedData69;
  70: DataSharedData70;
  71: DataSharedData71;
  72: DataSharedData72;
  73: DataSharedData73;
  74: DataSharedData74;
  75: DataSharedData75;
  76: DataSharedData76;
  77: DataSharedData77;
  78: DataSharedData78;
  80: DataSharedData80;
  81: DataSharedData81;
  82: DataSharedData82;
  83: DataSharedData83;
  84: DataSharedData84;
  app_upsell: DataSharedDataApp_upsell;
  igl_app_upsell: DataSharedDataIgl_app_upsell;
  notif: DataSharedDataNotif;
  onetaplogin: DataSharedDataOnetaplogin;
  multireg_iter: DataSharedDataMultireg_iter;
  felix_clear_fb_cookie: DataSharedDataFelix_clear_fb_cookie;
  felix_creation_duration_limits: DataSharedDataFelix_creation_duration_limits;
  felix_creation_fb_crossposting: DataSharedDataFelix_creation_fb_crossposting;
  felix_creation_fb_crossposting_v2: DataSharedDataFelix_creation_fb_crossposting_v2;
  felix_creation_validation: DataSharedDataFelix_creation_validation;
  mweb_topical_explore: DataSharedDataMweb_topical_explore;
  post_options: DataSharedDataPost_options;
  iglscioi: DataSharedDataIglscioi;
  sticker_tray: DataSharedDataSticker_tray;
  web_sentry: DataSharedDataWeb_sentry;
}

export interface DataSharedData0 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedDataP {
  4?: boolean | number;
  7?: boolean;
  8?: boolean;
  9?: boolean;
  0?: boolean | number | string;
  1?: boolean | string;
  5?: boolean;
  6?: boolean;
  10?: boolean | number;
  2?: boolean | number | string;
  3?: number | boolean | string;
  11?: number | boolean;
  12?: number | boolean;
  13?: boolean;
  14?: boolean;
  17?: number;
  18?: boolean;
  19?: number;
  22?: boolean;
  23?: string;
  24?: boolean;
  25?: string;
  26?: string;
  has_back_removed?: string;
  is_enabled?: string;
  blacklist?: string;
  maximum_length_seconds?: string;
  minimum_length_seconds?: string;
  display_version?: string;
  edit_video_controls?: string;
  description_maximum_length?: string;
  max_video_size_in_bytes?: string;
  minimum_length_for_feed_preview_seconds?: string;
  title_maximum_length?: string;
  valid_cover_mime_types?: string;
  valid_video_extensions?: string;
  valid_video_mime_types?: string;
  enable_igtv_embed?: string;
  use_refactor?: string;
}

export interface DataSharedData2 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData4 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData5 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData6 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData10 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData12 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData13 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData16 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData17 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData19 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData21 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData22 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData23 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData25 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData26 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData28 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData29 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData30 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData31 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData33 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData34 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData35 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData36 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData37 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData39 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData41 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData42 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData43 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData44 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData45 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData46 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData47 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData49 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData50 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData53 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData54 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData55 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData56 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData58 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData59 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData62 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData64 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData65 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData66 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData67 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData68 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData69 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData70 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData71 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData72 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData73 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData74 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData75 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData76 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData77 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData78 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData80 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData81 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData82 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData83 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedData84 {
  p: DataSharedDataP;
  qex: boolean;
}

export interface DataSharedDataApp_upsell {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataIgl_app_upsell {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataNotif {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataOnetaplogin {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataMultireg_iter {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataFelix_clear_fb_cookie {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataFelix_creation_duration_limits {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataFelix_creation_fb_crossposting {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataFelix_creation_fb_crossposting_v2 {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataFelix_creation_validation {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataMweb_topical_explore {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataPost_options {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataIglscioi {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataSticker_tray {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataWeb_sentry {
  g: string;
  p: DataSharedDataP;
}

export interface DataSharedDataEncryption {
  key_id: string;
  public_key: string;
}
