export interface GraphqlFetchReelsMediaRootObject {
  data: GraphqlFetchReelsMediaData;
  status: string;
}

export interface GraphqlFetchReelsMediaData {
  reels_media: GraphqlFetchReelsMediaReelsMediaItem[];
}

export interface GraphqlFetchReelsMediaReelsMediaItem {
  __typename: string;
  id: string;
  latest_reel_media: number;
  can_reply: boolean;
  owner: GraphqlFetchReelsMediaOwner;
  can_reshare: boolean;
  expiring_at: number;
  has_besties_media: null;
  has_pride_media: boolean;
  seen: null;
  user: GraphqlFetchReelsMediaUser;
  items: GraphqlFetchReelsMediaItemsItem[];
}

export interface GraphqlFetchReelsMediaOwner {
  __typename?: string;
  id: string;
  profile_pic_url: string;
  username: string;
  followed_by_viewer: boolean;
  requested_by_viewer: boolean;
}

export interface GraphqlFetchReelsMediaUser {
  id: string;
  profile_pic_url: string;
  username: string;
  followed_by_viewer: boolean;
  requested_by_viewer: boolean;
}

export interface GraphqlFetchReelsMediaItemsItem {
  audience: string;
  edge_story_media_viewers: GraphqlFetchReelsMediaEdge_story_media_viewers;
  __typename: string;
  id: string;
  dimensions: GraphqlFetchReelsMediaDimensions;
  display_resources: GraphqlFetchReelsMediaDisplayResourcesItem[];
  display_url: string;
  media_preview: string | null;
  gating_info: null;
  fact_check_overall_rating: null;
  fact_check_information: null;
  taken_at_timestamp: number;
  expiring_at_timestamp: number;
  story_cta_url: null;
  story_view_count: null;
  is_video: boolean;
  owner: GraphqlFetchReelsMediaOwner;
  tracking_token: string;
  tappable_objects: GraphqlFetchReelsMediaTappableObjectsItem[];
  story_app_attribution: null;
  edge_media_to_sponsor_user: GraphqlFetchReelsMediaEdge_media_to_sponsor_user;
  has_audio?: boolean;
  overlay_image_resources?: null;
  video_duration?: number;
  video_resources?: GraphqlFetchReelsMediaVideoResourcesItem[];
}

export interface GraphqlFetchReelsMediaEdge_story_media_viewers {
  count: number;
  page_info: GraphqlFetchReelsMediaPage_info;
  edges: any[];
}

export interface GraphqlFetchReelsMediaPage_info {
  has_next_page: boolean;
  end_cursor: null;
}

export interface GraphqlFetchReelsMediaDimensions {
  height: number;
  width: number;
}

export interface GraphqlFetchReelsMediaDisplayResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface GraphqlFetchReelsMediaTappableObjectsItem {
  __typename: string;
  x: number;
  y: number;
  width: number;
  height: number;
  rotation: number;
  custom_title: null;
  attribution: null;
  media?: GraphqlFetchReelsMediaMedia;
  username?: string;
  full_name?: string;
  is_private?: boolean;
  tappable_type?: string;
}

export interface GraphqlFetchReelsMediaMedia {
  id: string;
  shortcode: string;
}

export interface GraphqlFetchReelsMediaEdge_media_to_sponsor_user {
  edges: any[];
}

export interface GraphqlFetchReelsMediaVideoResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
  mime_type: string;
  profile: string;
}
