export interface GraphqlFeedRootObject {
  data: GraphqlFeedData;
  status: string;
}

export interface GraphqlFeedData {
  user: GraphqlFeedUser;
}

export interface GraphqlFeedUser {
  id: string;
  profile_pic_url: string;
  username: string;
  edge_web_feed_timeline?: GraphqlFeedEdge_web_feed_timeline;
  full_name?: string;
  is_verified?: boolean;
  biography?: string;
  edge_followed_by?: GraphqlFeedEdge_followed_by;
  is_private?: boolean;
  is_viewer?: boolean;
}

export interface GraphqlFeedEdge_web_feed_timeline {
  page_info: GraphqlFeedPage_info;
  edges: GraphqlFeedEdgesItem[];
}

export interface GraphqlFeedPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface GraphqlFeedEdgesItem {
  node: GraphqlFeedNode;
}

export interface GraphqlFeedNode {
  __typename?: string;
  id?: string;
  dimensions?: GraphqlFeedDimensions;
  display_url?: string;
  display_resources?: GraphqlFeedDisplayResourcesItem[];
  follow_hashtag_info?: null;
  is_video?: boolean;
  tracking_token?: string;
  edge_media_to_tagged_user?: GraphqlFeedEdge_media_to_tagged_user;
  accessibility_caption?: string;
  attribution?: null;
  shortcode?: string;
  edge_media_to_caption?: GraphqlFeedEdge_media_to_caption;
  edge_media_preview_comment?: GraphqlFeedEdge_media_preview_comment;
  gating_info?: null;
  fact_check_overall_rating?: null;
  fact_check_information?: null;
  media_preview?: string;
  comments_disabled?: boolean;
  taken_at_timestamp?: number;
  edge_media_preview_like?: GraphqlFeedEdge_media_preview_like;
  edge_media_to_sponsor_user?: GraphqlFeedEdge_media_to_sponsor_user;
  location?: GraphqlFeedLocation | null;
  viewer_has_liked?: boolean;
  viewer_has_saved?: boolean;
  viewer_has_saved_to_collection?: boolean;
  viewer_in_photo_of_you?: boolean;
  viewer_can_reshare?: boolean;
  owner?: GraphqlFeedOwner;
  user?: GraphqlFeedUser;
  x?: number;
  y?: number;
  text?: string;
  created_at?: number;
  did_report_as_spam?: boolean;
  aysf?: GraphqlFeedAysfItem[];
  type?: string;
  profile_pic_url?: string;
  username?: string;
}

export interface GraphqlFeedDimensions {
  height: number;
  width: number;
}

export interface GraphqlFeedDisplayResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface GraphqlFeedEdge_media_to_tagged_user {
  edges: GraphqlFeedEdgesItem[];
}

export interface GraphqlFeedEdge_media_to_caption {
  edges: GraphqlFeedEdgesItem[];
}

export interface GraphqlFeedEdge_media_preview_comment {
  count: number;
  page_info: GraphqlFeedPage_info;
  edges: GraphqlFeedEdgesItem[];
}

export interface GraphqlFeedOwner {
  id: string;
  profile_pic_url: string;
  username: string;
  followed_by_viewer?: boolean;
  full_name?: string;
  is_private?: boolean;
  requested_by_viewer?: boolean;
  blocked_by_viewer?: boolean;
  has_blocked_viewer?: boolean;
  restricted_by_viewer?: boolean;
}

export interface GraphqlFeedEdge_media_preview_like {
  count: number;
  edges: GraphqlFeedEdgesItem[];
}

export interface GraphqlFeedEdge_media_to_sponsor_user {
  edges: any[];
}

export interface GraphqlFeedLocation {
  id: string;
  has_public_page: boolean;
  name: string;
  slug: string;
}

export interface GraphqlFeedAysfItem {
  description: string;
  user: GraphqlFeedUser;
}

export interface GraphqlFeedEdge_followed_by {
  count: number;
}
