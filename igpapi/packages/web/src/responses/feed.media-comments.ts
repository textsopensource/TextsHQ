export interface FeedMediaCommentsRootObject {
  data: FeedMediaCommentsData;
  status: string;
}

export interface FeedMediaCommentsData {
  shortcode_media: FeedMediaCommentsShortcode_media;
}

export interface FeedMediaCommentsShortcode_media {
  edge_media_to_parent_comment: FeedMediaCommentsEdge_media_to_parent_comment;
}

export interface FeedMediaCommentsEdge_media_to_parent_comment {
  count: number;
  page_info: FeedMediaCommentsPage_info;
  edges: FeedMediaCommentsEdgesItem[];
}

export interface FeedMediaCommentsPage_info {
  has_next_page: boolean;
  end_cursor: string | null;
}

export interface FeedMediaCommentsEdgesItem {
  node: FeedMediaCommentsNode;
}

export interface FeedMediaCommentsNode {
  id: string;
  text: string;
  created_at: number;
  did_report_as_spam: boolean;
  owner: FeedMediaCommentsOwner;
  viewer_has_liked: boolean;
  edge_liked_by: FeedMediaCommentsEdge_liked_by;
  is_restricted_pending: boolean;
  edge_threaded_comments: FeedMediaCommentsEdge_threaded_comments;
}

export interface FeedMediaCommentsOwner {
  id: string;
  is_verified: boolean;
  profile_pic_url: string;
  username: string;
}

export interface FeedMediaCommentsEdge_liked_by {
  count: number;
}

export interface FeedMediaCommentsEdge_threaded_comments {
  count: number;
  page_info: FeedMediaCommentsPage_info;
  edges: any[];
}
