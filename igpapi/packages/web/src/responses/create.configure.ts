export interface CreateConfigureRootObject {
  media: CreateConfigureMedia;
  status: string;
}

export interface CreateConfigureMedia {
  taken_at: number;
  pk: number;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: CreateConfigureImage_versions2;
  original_width: number;
  original_height: number;
  location: CreateConfigureLocation;
  lat: number;
  lng: number;
  user: CreateConfigureUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  photo_of_you: boolean;
  usertags: CreateConfigureUsertags;
  caption: CreateConfigureCaption;
  fb_user_tags: CreateConfigureFb_user_tags;
  can_viewer_save: boolean;
  organic_tracking_token: string;
}

export interface CreateConfigureImage_versions2 {
  candidates: CreateConfigureCandidatesItem[];
}

export interface CreateConfigureCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface CreateConfigureLocation {
  pk: number;
  name: string;
  address: string;
  city: string;
  short_name: string;
  lng: number;
  lat: number;
  external_source: string;
  facebook_places_id: number;
}

export interface CreateConfigureUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  has_anonymous_profile_picture?: boolean;
  can_boost_post?: boolean;
  can_see_organic_insights?: boolean;
  show_insights_terms?: boolean;
  reel_auto_archive?: string;
  is_unpublished?: boolean;
  allowed_commenter_type?: string;
  is_verified?: boolean;
}

export interface CreateConfigureUsertags {
  in: CreateConfigureInItem[];
}

export interface CreateConfigureInItem {
  user: CreateConfigureUser;
  position: number[];
  start_time_in_video_in_sec: null;
  duration_in_video_in_sec: null;
}

export interface CreateConfigureCaption {
  pk: number;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: CreateConfigureUser;
  media_id: number;
}

export interface CreateConfigureFb_user_tags {
  in: any[];
}
