export interface AccountLoginRootObject {
  authenticated: boolean;
  user: boolean;
  userId: string;
  oneTapPrompt: boolean;
  status: string;
}
