export interface FeedFollowRootObject {
  data: FeedFollowData;
  status: string;
}

export interface FeedFollowData {
  user: FeedFollowUser;
}

export interface FeedFollowUser {
  edge_follow?: FeedFollowEdge;
  edge_followed_by?: FeedFollowEdge;
  edge_mutual_followed_by?: FeedFollowEdge_mutual_followed_by;
}

export interface FeedFollowEdge {
  count: number;
  page_info: FeedFollowPage_info;
  edges: FeedFollowEdgesItem[];
}

export interface FeedFollowPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface FeedFollowEdgesItem {
  node: FeedFollowNode;
}

export interface FeedFollowNode {
  id: string;
  username: string;
  full_name: string;
  profile_pic_url: string;
  is_private: boolean;
  is_verified: boolean;
  followed_by_viewer: boolean;
  requested_by_viewer: boolean;
  reel: FeedFollowReel;
}

export interface FeedFollowReel {
  id: string;
  expiring_at: number;
  has_pride_media: null;
  latest_reel_media: null | number;
  seen: null;
  owner: FeedFollowOwner;
}

export interface FeedFollowOwner {
  __typename: string;
  id: string;
  profile_pic_url: string;
  username: string;
}

export interface FeedFollowEdge_mutual_followed_by {
  count: number;
  edges: any[];
}
