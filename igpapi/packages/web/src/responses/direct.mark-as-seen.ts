export interface DirectMarkAsSeenRootObject {
  unseen_count: number;
  unseen_count_ts: number;
  status: string;
}
