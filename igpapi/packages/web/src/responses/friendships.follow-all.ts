export interface FriendshipsFollowAllRootObject {
  friendship_statuses: FriendshipsFollowAllFriendship_statuses;
  status: string;
}

export interface FriendshipsFollowAllFriendship_statuses {}
