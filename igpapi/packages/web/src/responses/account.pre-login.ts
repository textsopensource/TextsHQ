export interface AccountPreLoginRootObject {
  captcha: AccountPreLoginCaptcha;
  gdpr_required: boolean;
  tos_version: string;
  username_hint: string;
}

export interface AccountPreLoginCaptcha {
  enabled: boolean;
  key: string;
}
