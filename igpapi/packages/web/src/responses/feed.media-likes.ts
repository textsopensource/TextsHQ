export interface FeedMediaLikesRootObject {
  data: FeedMediaLikesData;
  status: string;
}

export interface FeedMediaLikesData {
  shortcode_media: FeedMediaLikesShortcode_media;
}

export interface FeedMediaLikesShortcode_media {
  id: string;
  shortcode: string;
  edge_liked_by: FeedMediaLikesEdge_liked_by;
}

export interface FeedMediaLikesEdge_liked_by {
  count: number;
  page_info: FeedMediaLikesPage_info;
  edges: FeedMediaLikesEdgesItem[];
}

export interface FeedMediaLikesPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface FeedMediaLikesEdgesItem {
  node: FeedMediaLikesNode;
}

export interface FeedMediaLikesNode {
  id: string;
  username: string;
  full_name: string;
  profile_pic_url: string;
  is_private: boolean;
  is_verified: boolean;
  followed_by_viewer: boolean;
  requested_by_viewer: boolean;
}
