export interface TagFeedRootObject {
  data: TagFeedData;
  status: string;
}

export interface TagFeedData {
  hashtag: TagFeedHashtag;
}

export interface TagFeedHashtag {
  id: string;
  name: string;
  allow_following: boolean;
  is_following: boolean;
  is_top_media_only: boolean;
  profile_pic_url: string;
  edge_hashtag_to_media: TagFeedEdge_hashtag_to_media;
  edge_hashtag_to_top_posts: TagFeedEdge_hashtag_to_top_posts;
  edge_hashtag_to_content_advisory: TagFeedEdge_hashtag_to_content_advisory;
  edge_hashtag_to_null_state: TagFeedEdge_hashtag_to_null_state;
}

export interface TagFeedEdge_hashtag_to_media {
  count: number;
  page_info: TagFeedPage_info;
  edges: TagFeedEdgesItem[];
}

export interface TagFeedPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface TagFeedEdgesItem {
  node: TagFeedNode;
}

export interface TagFeedNode {
  comments_disabled?: boolean;
  __typename?: string;
  id?: string;
  edge_media_to_caption?: TagFeedEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: TagFeedEdge_media_to_comment;
  taken_at_timestamp?: number;
  dimensions?: TagFeedDimensions;
  display_url?: string;
  edge_liked_by?: TagFeedEdge_liked_by;
  edge_media_preview_like?: TagFeedEdge_media_preview_like;
  owner?: TagFeedOwner;
  thumbnail_src?: string;
  thumbnail_resources?: TagFeedThumbnailResourcesItem[];
  is_video?: boolean;
  product_type?: string;
  video_view_count?: number;
  text?: string;
  accessibility_caption?: string;
}

export interface TagFeedEdge_media_to_caption {
  edges: TagFeedEdgesItem[];
}

export interface TagFeedEdge_media_to_comment {
  count: number;
}

export interface TagFeedDimensions {
  height: number;
  width: number;
}

export interface TagFeedEdge_liked_by {
  count: number;
}

export interface TagFeedEdge_media_preview_like {
  count: number;
}

export interface TagFeedOwner {
  id: string;
}

export interface TagFeedThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface TagFeedEdge_hashtag_to_top_posts {
  edges: TagFeedEdgesItem[];
}

export interface TagFeedEdge_hashtag_to_content_advisory {
  count: number;
  edges: any[];
}

export interface TagFeedEdge_hashtag_to_null_state {
  edges: any[];
}
