export interface FeedUserRootObject {
  data: FeedUserData;
  status: string;
}

export interface FeedUserData {
  user: FeedUserUser;
}

export interface FeedUserUser {
  edge_owner_to_timeline_media?: FeedUserEdge_owner_to_timeline_media;
  full_name?: string;
  id?: string;
  is_verified?: boolean;
  profile_pic_url?: string;
  username?: string;
}

export interface FeedUserEdge_owner_to_timeline_media {
  count: number;
  page_info: FeedUserPage_info;
  edges: FeedUserEdgesItem[];
}

export interface FeedUserPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface FeedUserEdgesItem {
  node: FeedUserNode;
}

export interface FeedUserNode {
  __typename?: string;
  id?: string;
  dimensions?: FeedUserDimensions;
  display_url?: string;
  display_resources?: FeedUserDisplayResourcesItem[];
  is_video?: boolean;
  tracking_token?: string;
  edge_media_to_tagged_user?: FeedUserEdge_media_to_tagged_user;
  accessibility_caption?: null;
  edge_media_to_caption?: FeedUserEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: FeedUserEdge_media_to_comment;
  edge_media_to_sponsor_user?: FeedUserEdge_media_to_sponsor_user;
  comments_disabled?: boolean;
  taken_at_timestamp?: number;
  edge_media_preview_like?: FeedUserEdge_media_preview_like;
  gating_info?: null;
  fact_check_overall_rating?: null;
  fact_check_information?: null;
  media_preview?: null | string;
  owner?: FeedUserOwner;
  location?: FeedUserLocation | null;
  viewer_has_liked?: boolean;
  viewer_has_saved?: boolean;
  viewer_has_saved_to_collection?: boolean;
  viewer_in_photo_of_you?: boolean;
  viewer_can_reshare?: boolean;
  thumbnail_src?: string;
  thumbnail_resources?: FeedUserThumbnailResourcesItem[];
  text?: string;
  created_at?: number;
  did_report_as_spam?: boolean;
  dash_info?: FeedUserDash_info;
  video_url?: string;
  video_view_count?: number;
  user?: FeedUserUser;
  x?: number;
  y?: number;
}

export interface FeedUserDimensions {
  height: number;
  width: number;
}

export interface FeedUserDisplayResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface FeedUserEdge_media_to_tagged_user {
  edges: FeedUserEdgesItem[];
}

export interface FeedUserEdge_media_to_caption {
  edges: FeedUserEdgesItem[];
}

export interface FeedUserEdge_media_to_comment {
  count: number;
  page_info: FeedUserPage_info;
  edges: FeedUserEdgesItem[];
}

export interface FeedUserOwner {
  id: string;
  is_verified?: boolean;
  profile_pic_url?: string;
  username: string;
}

export interface FeedUserEdge_media_to_sponsor_user {
  edges: any[];
}

export interface FeedUserEdge_media_preview_like {
  count: number;
  edges: any[];
}

export interface FeedUserLocation {
  id: string;
  has_public_page: boolean;
  name: string;
  slug: string;
}

export interface FeedUserThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface FeedUserDash_info {
  is_dash_eligible: boolean;
  video_dash_manifest: null;
  number_of_qualities: number;
}
