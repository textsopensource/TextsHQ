export interface DirectCreateGroupThreadRootObject {
  thread_id: string;
  thread_v2_id: string;
  users: DirectCreateGroupThreadUsersItem[];
  left_users: any[];
  admin_user_ids: number[];
  items: DirectCreateGroupThreadItemsItem[];
  last_activity_at: number;
  muted: boolean;
  is_pin: boolean;
  named: boolean;
  canonical: boolean;
  pending: boolean;
  archived: boolean;
  thread_type: string;
  viewer_id: number;
  thread_title: string;
  folder: number;
  vc_muted: boolean;
  is_group: boolean;
  mentions_muted: boolean;
  approval_required_for_new_members: boolean;
  input_mode: number;
  business_thread_folder: number;
  read_state: number;
  last_non_sender_item_at: number;
  assigned_admin_id: number;
  inviter: DirectCreateGroupThreadInviter;
  has_older: boolean;
  has_newer: boolean;
  last_seen_at: DirectCreateGroupThreadLast_seen_at;
  newest_cursor: string;
  oldest_cursor: string;
  next_cursor: string;
  prev_cursor: string;
  last_permanent_item: DirectCreateGroupThreadLast_permanent_item;
  status: string;
}

export interface DirectCreateGroupThreadUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectCreateGroupThreadFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectCreateGroupThreadFriendship_status {
  following: boolean;
  blocking: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
  followed_by?: boolean;
  muting?: boolean;
}

export interface DirectCreateGroupThreadItemsItem {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  video_call_event?: DirectCreateGroupThreadVideo_call_event;
  voice_media?: DirectCreateGroupThreadVoice_media;
  client_context?: string;
  media?: DirectCreateGroupThreadMedia;
  text?: string;
  action_log?: DirectCreateGroupThreadAction_log;
}

export interface DirectCreateGroupThreadVideo_call_event {
  action: string;
  vc_id: number;
  encoded_server_data_info: null | string;
  description: string;
  text_attributes: any[];
  did_join: null;
}

export interface DirectCreateGroupThreadVoice_media {
  media: DirectCreateGroupThreadMedia;
  seen_user_ids: any[];
  view_mode: string;
  seen_count: number;
  replay_expiring_at_us: null;
  expiring_media_action_summary?: DirectCreateGroupThreadExpiring_media_action_summary;
}

export interface DirectCreateGroupThreadMedia {
  id: string | number;
  media_type: number;
  product_type?: string;
  audio?: DirectCreateGroupThreadAudio;
  organic_tracking_token?: string;
  user?: DirectCreateGroupThreadUser;
  image_versions2?: DirectCreateGroupThreadImage_versions2;
  original_width?: number;
  original_height?: number;
}

export interface DirectCreateGroupThreadAudio {
  audio_src: string;
  duration: number;
  waveform_data: number[] | null[];
  waveform_sampling_frequency_hz: number;
}

export interface DirectCreateGroupThreadUser {
  pk: number;
  friendship_status: DirectCreateGroupThreadFriendship_status;
  username: string;
}

export interface DirectCreateGroupThreadImage_versions2 {
  candidates: DirectCreateGroupThreadCandidatesItem[];
}

export interface DirectCreateGroupThreadCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface DirectCreateGroupThreadExpiring_media_action_summary {
  type: string;
  timestamp: number;
  count: number;
}

export interface DirectCreateGroupThreadAction_log {
  description: string;
  bold: DirectCreateGroupThreadBoldItem[];
  text_attributes: DirectCreateGroupThreadTextAttributesItem[];
}

export interface DirectCreateGroupThreadBoldItem {
  start: number;
  end: number;
}

export interface DirectCreateGroupThreadTextAttributesItem {
  start: number;
  end: number;
  bold: number;
  color: string;
  intent: string;
}

export interface DirectCreateGroupThreadInviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  reel_auto_archive: string;
  allowed_commenter_type: string;
}

export interface DirectCreateGroupThreadLast_seen_at {
  [x: string]: DirectCreateGroupThreadLastSeenAtUser;
}

export interface DirectCreateGroupThreadLastSeenAtUser {
  timestamp: string;
  item_id: string;
}

export interface DirectCreateGroupThreadLast_permanent_item {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  video_call_event: DirectCreateGroupThreadVideo_call_event;
}
