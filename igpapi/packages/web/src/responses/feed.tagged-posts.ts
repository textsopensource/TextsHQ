export interface FeedTaggedPostsRootObject {
  data: FeedTaggedPostsData;
  status: string;
}

export interface FeedTaggedPostsData {
  user: FeedTaggedPostsUser;
}

export interface FeedTaggedPostsUser {
  edge_user_to_photos_of_you: FeedTaggedPostsEdge_user_to_photos_of_you;
}

export interface FeedTaggedPostsEdge_user_to_photos_of_you {
  count: number;
  page_info: FeedTaggedPostsPage_info;
  edges: FeedTaggedPostsEdgesItem[];
}

export interface FeedTaggedPostsPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface FeedTaggedPostsEdgesItem {
  node: FeedTaggedPostsNode;
}

export interface FeedTaggedPostsNode {
  id?: string;
  __typename?: string;
  edge_media_to_caption?: FeedTaggedPostsEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: FeedTaggedPostsEdge_media_to_comment;
  comments_disabled?: boolean;
  taken_at_timestamp?: number;
  dimensions?: FeedTaggedPostsDimensions;
  display_url?: string;
  edge_liked_by?: FeedTaggedPostsEdge_liked_by;
  edge_media_preview_like?: FeedTaggedPostsEdge_media_preview_like;
  owner?: FeedTaggedPostsOwner;
  thumbnail_src?: string;
  is_video?: boolean;
  accessibility_caption?: string;
  text?: string;
  video_view_count?: number;
}

export interface FeedTaggedPostsEdge_media_to_caption {
  edges: FeedTaggedPostsEdgesItem[];
}

export interface FeedTaggedPostsEdge_media_to_comment {
  count: number;
}

export interface FeedTaggedPostsDimensions {
  height: number;
  width: number;
}

export interface FeedTaggedPostsEdge_liked_by {
  count: number;
}

export interface FeedTaggedPostsEdge_media_preview_like {
  count: number;
}

export interface FeedTaggedPostsOwner {
  id: string;
  username: string;
}
