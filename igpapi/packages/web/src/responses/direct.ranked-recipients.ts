export interface DirectRankedRecipientsRootObject {
  ranked_recipients: DirectRankedRecipientsRankedRecipientsItem[];
  expires: number;
  filtered: boolean;
  request_id: string;
  rank_token: string;
  status: string;
}

export interface DirectRankedRecipientsRankedRecipientsItem {
  user: DirectRankedRecipientsUser;
}

export interface DirectRankedRecipientsUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectRankedRecipientsFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectRankedRecipientsFriendship_status {
  following: boolean;
  followed_by: boolean;
  blocking: boolean;
  muting: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
