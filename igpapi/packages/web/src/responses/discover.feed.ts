export interface DiscoverFeedRootObject {
  data: DiscoverFeedData;
}

export interface DiscoverFeedData {
  user: DiscoverFeedUser;
}

export interface DiscoverFeedUser {
  id: string;
  profile_pic_url: string;
  username: string;
  edge_web_discover_media: DiscoverFeedEdge_web_discover_media;
}

export interface DiscoverFeedEdge_web_discover_media {
  page_info: DiscoverFeedPage_info;
  edges: DiscoverFeedEdgesItem[];
}

export interface DiscoverFeedPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface DiscoverFeedEdgesItem {
  node: DiscoverFeedNode;
}

export interface DiscoverFeedNode {
  __typename?: string;
  id?: string;
  comments_disabled?: boolean;
  dimensions?: DiscoverFeedDimensions;
  display_url?: string;
  edge_liked_by?: DiscoverFeedEdge_liked_by;
  edge_media_preview_like?: DiscoverFeedEdge_media_preview_like;
  edge_media_to_caption?: DiscoverFeedEdge_media_to_caption;
  edge_media_to_comment: DiscoverFeedEdge_media_to_comment | string;
  is_video: boolean | string;
  owner: DiscoverFeedOwner | string;
  shortcode?: string;
  taken_at_timestamp: number | string;
  thumbnail_src?: string;
  thumbnail_resources?: DiscoverFeedThumbnailResourcesItem[];
  accessibility_caption?: string;
  edge_sidecar_to_children?: DiscoverFeedEdge_sidecar_to_children;
  text?: string;
  persönlich?: number;
  erfolgistkeinglück?: string;
  count?: string;
  1618601494?: string;
  B8W2BQUIvPJ?: string;
}

export interface DiscoverFeedDimensions {
  height: number;
  width: number;
}

export interface DiscoverFeedEdge_liked_by {
  count: number;
}

export interface DiscoverFeedEdge_media_preview_like {
  count: number;
}

export interface DiscoverFeedEdge_media_to_caption {
  edges: DiscoverFeedEdgesItem[];
}

export interface DiscoverFeedEdge_media_to_comment {
  count: number;
}

export interface DiscoverFeedOwner {
  id: string;
}

export interface DiscoverFeedThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface DiscoverFeedEdge_sidecar_to_children {
  edges: DiscoverFeedEdgesItem[];
}
