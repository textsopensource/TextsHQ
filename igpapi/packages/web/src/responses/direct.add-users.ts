export interface DirectAddUsersRootObject {
  thread: DirectAddUsersThread;
  status: string;
}

export interface DirectAddUsersThread {
  thread_id: string;
  thread_v2_id: string;
  users: DirectAddUsersUsersItem[];
  left_users: any[];
  admin_user_ids: number[];
  items: DirectAddUsersItemsItem[];
  last_activity_at: number;
  muted: boolean;
  is_pin: boolean;
  named: boolean;
  canonical: boolean;
  pending: boolean;
  archived: boolean;
  thread_type: string;
  viewer_id: number;
  thread_title: string;
  folder: number;
  vc_muted: boolean;
  is_group: boolean;
  mentions_muted: boolean;
  approval_required_for_new_members: boolean;
  input_mode: number;
  business_thread_folder: number;
  read_state: number;
  last_non_sender_item_at: number;
  assigned_admin_id: number;
  inviter: DirectAddUsersInviter;
  has_older: boolean;
  has_newer: boolean;
  last_seen_at: DirectAddUsersLast_seen_at;
  newest_cursor: string;
  oldest_cursor: string;
  next_cursor: string;
  prev_cursor: string;
  last_permanent_item: DirectAddUsersLast_permanent_item;
}

export interface DirectAddUsersUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectAddUsersFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectAddUsersFriendship_status {
  following: boolean;
  blocking: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
  followed_by?: boolean;
  muting?: boolean;
}

export interface DirectAddUsersItemsItem {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  video_call_event?: DirectAddUsersVideo_call_event;
  voice_media?: DirectAddUsersVoice_media;
  client_context?: string;
  media?: DirectAddUsersMedia;
  text?: string;
  action_log?: DirectAddUsersAction_log;
}

export interface DirectAddUsersVideo_call_event {
  action: string;
  vc_id: number;
  encoded_server_data_info: null | string;
  description: string;
  text_attributes: any[];
  did_join: null;
}

export interface DirectAddUsersVoice_media {
  media: DirectAddUsersMedia;
  seen_user_ids: any[];
  view_mode: string;
  seen_count: number;
  replay_expiring_at_us: null;
  expiring_media_action_summary?: DirectAddUsersExpiring_media_action_summary;
}

export interface DirectAddUsersMedia {
  id: string | number;
  media_type: number;
  product_type?: string;
  audio?: DirectAddUsersAudio;
  organic_tracking_token?: string;
  user?: DirectAddUsersUser;
  image_versions2?: DirectAddUsersImage_versions2;
  original_width?: number;
  original_height?: number;
}

export interface DirectAddUsersAudio {
  audio_src: string;
  duration: number;
  waveform_data: number[] | null[];
  waveform_sampling_frequency_hz: number;
}

export interface DirectAddUsersUser {
  pk: number;
  friendship_status: DirectAddUsersFriendship_status;
  username: string;
}

export interface DirectAddUsersImage_versions2 {
  candidates: DirectAddUsersCandidatesItem[];
}

export interface DirectAddUsersCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface DirectAddUsersExpiring_media_action_summary {
  type: string;
  timestamp: number;
  count: number;
}

export interface DirectAddUsersAction_log {
  description: string;
  bold: DirectAddUsersBoldItem[];
  text_attributes: DirectAddUsersTextAttributesItem[];
}

export interface DirectAddUsersBoldItem {
  start: number;
  end: number;
}

export interface DirectAddUsersTextAttributesItem {
  start: number;
  end: number;
  bold: number;
  color: string;
  intent: string;
}

export interface DirectAddUsersInviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  reel_auto_archive: string;
  allowed_commenter_type: string;
}

export interface DirectAddUsersLast_seen_at {
  [x: string]: DirectAddUsersLastSeenAtUser;
}

export interface DirectAddUsersLastSeenAtUser {
  timestamp: string;
  item_id: string;
}

export interface DirectAddUsersLast_permanent_item {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  video_call_event: DirectAddUsersVideo_call_event;
}
