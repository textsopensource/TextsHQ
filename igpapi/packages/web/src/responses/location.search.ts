export interface LocationSearchRootObject {
  venues: LocationSearchVenuesItem[];
  request_id: string;
  rank_token: string;
  status: string;
}

export interface LocationSearchVenuesItem {
  name: string;
  external_id: string;
  external_id_source: string;
  address: string;
  lat: number;
  lng: number;
  minimum_age: number;
}
