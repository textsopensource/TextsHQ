export interface RepositoryPublicMediaRootObject {
  graphql: RepositoryPublicMediaGraphql;
}

export interface RepositoryPublicMediaGraphql {
  shortcode_media: RepositoryPublicMediaShortcode_media;
}

export interface RepositoryPublicMediaShortcode_media {
  __typename: string;
  id: string;
  shortcode: string;
  dimensions: RepositoryPublicMediaDimensions;
  gating_info: null;
  fact_check_overall_rating: null;
  fact_check_information: null;
  media_preview: string;
  display_url: string;
  display_resources: RepositoryPublicMediaDisplayResourcesItem[];
  dash_info: RepositoryPublicMediaDash_info;
  video_url: string;
  video_view_count: number;
  is_video: boolean;
  tracking_token: string;
  edge_media_to_tagged_user: RepositoryPublicMediaEdge_media_to_tagged_user;
  edge_media_to_caption: RepositoryPublicMediaEdge_media_to_caption;
  caption_is_edited: boolean;
  has_ranked_comments: boolean;
  edge_media_to_parent_comment: RepositoryPublicMediaEdge_media_to_parent_comment;
  edge_media_preview_comment: RepositoryPublicMediaEdge_media_preview_comment;
  comments_disabled: boolean;
  commenting_disabled_for_viewer: boolean;
  taken_at_timestamp: number;
  edge_media_preview_like: RepositoryPublicMediaEdge_media_preview_like;
  edge_media_to_sponsor_user: RepositoryPublicMediaEdge_media_to_sponsor_user;
  location: null;
  viewer_has_liked: boolean;
  viewer_has_saved: boolean;
  viewer_has_saved_to_collection: boolean;
  viewer_in_photo_of_you: boolean;
  viewer_can_reshare: boolean;
  owner: RepositoryPublicMediaOwner;
  is_ad: boolean;
  edge_web_media_to_related_media: RepositoryPublicMediaEdge_web_media_to_related_media;
  encoding_status: null;
  is_published: boolean;
  product_type: string;
  title: string;
  video_duration: number;
  thumbnail_src: string;
}

export interface RepositoryPublicMediaDimensions {
  height: number;
  width: number;
}

export interface RepositoryPublicMediaDisplayResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface RepositoryPublicMediaDash_info {
  is_dash_eligible: boolean;
  video_dash_manifest: null;
  number_of_qualities: number;
}

export interface RepositoryPublicMediaEdge_media_to_tagged_user {
  edges: any[];
}

export interface RepositoryPublicMediaEdge_media_to_caption {
  edges: RepositoryPublicMediaEdgesItem[];
}

export interface RepositoryPublicMediaEdgesItem {
  node: RepositoryPublicMediaNode;
}

export interface RepositoryPublicMediaNode {
  text: string;
  id?: string;
  created_at?: number;
  did_report_as_spam?: boolean;
  owner?: RepositoryPublicMediaOwner;
  viewer_has_liked?: boolean;
  edge_liked_by?: RepositoryPublicMediaEdge_liked_by;
  is_restricted_pending?: boolean;
  edge_threaded_comments?: RepositoryPublicMediaEdge_threaded_comments;
}

export interface RepositoryPublicMediaEdge_media_to_parent_comment {
  count: number;
  page_info: RepositoryPublicMediaPage_info;
  edges: RepositoryPublicMediaEdgesItem[];
}

export interface RepositoryPublicMediaPage_info {
  has_next_page: boolean;
  end_cursor: string | null;
}

export interface RepositoryPublicMediaOwner {
  id: string;
  is_verified: boolean;
  profile_pic_url: string;
  username: string;
  blocked_by_viewer?: boolean;
  followed_by_viewer?: boolean;
  full_name?: string;
  has_blocked_viewer?: boolean;
  is_private?: boolean;
  is_unpublished?: boolean;
  requested_by_viewer?: boolean;
}

export interface RepositoryPublicMediaEdge_liked_by {
  count: number;
}

export interface RepositoryPublicMediaEdge_threaded_comments {
  count: number;
  page_info: RepositoryPublicMediaPage_info;
  edges: any[];
}

export interface RepositoryPublicMediaEdge_media_preview_comment {
  count: number;
  edges: RepositoryPublicMediaEdgesItem[];
}

export interface RepositoryPublicMediaEdge_media_preview_like {
  count: number;
  edges: any[];
}

export interface RepositoryPublicMediaEdge_media_to_sponsor_user {
  edges: any[];
}

export interface RepositoryPublicMediaEdge_web_media_to_related_media {
  edges: any[];
}
