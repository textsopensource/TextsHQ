export interface DirectGetBadgeCountRootObject {
  user_id: number;
  badge_count: number;
  seq_id: number;
  badge_count_at_ms: number;
  status: string;
}
