export interface DirectPendingInboxRootObject {
  viewer: DirectPendingInboxViewer;
  inbox: DirectPendingInboxInbox;
  seq_id: number;
  pending_requests_total: number;
  most_recent_inviter: DirectPendingInboxMost_recent_inviter;
  status: string;
}

export interface DirectPendingInboxViewer {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  reel_auto_archive: string;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectPendingInboxInbox {
  threads: DirectPendingInboxThreadsItem[];
  has_older: boolean;
  unseen_count: number;
  unseen_count_ts: number;
}

export interface DirectPendingInboxThreadsItem {
  thread_id: string;
  thread_v2_id: string;
  users: DirectPendingInboxUsersItem[];
  left_users: any[];
  admin_user_ids: any[];
  items: DirectPendingInboxItemsItem[];
  last_activity_at: number;
  muted: boolean;
  is_pin: boolean;
  named: boolean;
  canonical: boolean;
  pending: boolean;
  archived: boolean;
  thread_type: string;
  viewer_id: number;
  thread_title: string;
  folder: number;
  vc_muted: boolean;
  is_group: boolean;
  mentions_muted: boolean;
  approval_required_for_new_members: boolean;
  input_mode: number;
  business_thread_folder: number;
  read_state: number;
  last_non_sender_item_at: number;
  assigned_admin_id: number;
  inviter: DirectPendingInboxInviter;
  has_older: boolean;
  has_newer: boolean;
  newest_cursor: string;
  oldest_cursor: string;
  next_cursor: string;
  prev_cursor: string;
  is_spam: boolean;
  last_permanent_item: DirectPendingInboxLast_permanent_item;
}

export interface DirectPendingInboxUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectPendingInboxFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectPendingInboxFriendship_status {
  following: boolean;
  blocking: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}

export interface DirectPendingInboxItemsItem {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  media_share: DirectPendingInboxMedia_share;
  client_context: string;
}

export interface DirectPendingInboxMedia_share {
  taken_at: number;
  pk: number;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  carousel_media_count: number;
  carousel_media: DirectPendingInboxCarouselMediaItem[];
  can_see_insights_as_brand: boolean;
  user: DirectPendingInboxUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  photo_of_you: boolean;
  caption: DirectPendingInboxCaption;
  can_viewer_save: boolean;
  organic_tracking_token: string;
}

export interface DirectPendingInboxCarouselMediaItem {
  id: string;
  media_type: number;
  image_versions2: DirectPendingInboxImage_versions2;
  original_width: number;
  original_height: number;
  pk: number;
  carousel_parent_id: string;
}

export interface DirectPendingInboxImage_versions2 {
  candidates: DirectPendingInboxCandidatesItem[];
}

export interface DirectPendingInboxCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface DirectPendingInboxUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  has_anonymous_profile_picture: boolean;
  is_unpublished: boolean;
  is_favorite: boolean;
  latest_reel_media: number;
}

export interface DirectPendingInboxCaption {
  pk: number;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: DirectPendingInboxUser;
  media_id: number;
}

export interface DirectPendingInboxInviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  media_count: number;
  follower_count: number;
  following_count: number;
  following_tag_count: number;
  mutual_followers_count: number;
}

export interface DirectPendingInboxLast_permanent_item {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  media_share: DirectPendingInboxMedia_share;
  client_context: string;
}

export interface DirectPendingInboxMost_recent_inviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  media_count: number;
  follower_count: number;
  following_count: number;
  following_tag_count: number;
  mutual_followers_count: number;
}
