export interface CheckpointErrorRootObject {
  status: string;
  lock: boolean;
  checkpoint_url: string;
  message: string;
}
