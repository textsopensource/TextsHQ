export interface GraphqlSuggestedUsersRootObject {
  data: GraphqlSuggestedUsersData;
  status: string;
}

export interface GraphqlSuggestedUsersData {
  user: GraphqlSuggestedUsersUser;
}

export interface GraphqlSuggestedUsersUser {
  connected_fbid?: string;
  edge_facebook_friends?: GraphqlSuggestedUsersEdge_facebook_friends;
  edge_suggested_users?: GraphqlSuggestedUsersEdge_suggested_users;
  edge_followed_by?: GraphqlSuggestedUsersEdge_followed_by;
  followed_by_viewer?: boolean;
  full_name?: string;
  id?: string;
  is_private?: boolean;
  is_verified?: boolean;
  is_viewer?: boolean;
  profile_pic_url?: string;
  requested_by_viewer?: boolean;
  username?: string;
  edge_owner_to_timeline_media?: GraphqlSuggestedUsersEdge_owner_to_timeline_media;
  reel?: GraphqlSuggestedUsersReel;
}

export interface GraphqlSuggestedUsersEdge_facebook_friends {
  count: number;
}

export interface GraphqlSuggestedUsersEdge_suggested_users {
  page_info: GraphqlSuggestedUsersPage_info;
  edges: GraphqlSuggestedUsersEdgesItem[];
}

export interface GraphqlSuggestedUsersPage_info {
  has_next_page: boolean;
}

export interface GraphqlSuggestedUsersEdgesItem {
  node: GraphqlSuggestedUsersNode;
}

export interface GraphqlSuggestedUsersNode {
  user: GraphqlSuggestedUsersUser;
  description: string;
}

export interface GraphqlSuggestedUsersEdge_followed_by {
  count: number;
}

export interface GraphqlSuggestedUsersEdge_owner_to_timeline_media {
  edges: any[];
}

export interface GraphqlSuggestedUsersReel {
  id: string;
  expiring_at: number;
  has_pride_media: boolean;
  latest_reel_media: number | null;
  seen: null;
  owner: GraphqlSuggestedUsersOwner;
}

export interface GraphqlSuggestedUsersOwner {
  __typename: string;
  id: string;
  profile_pic_url: string;
  username: string;
}
