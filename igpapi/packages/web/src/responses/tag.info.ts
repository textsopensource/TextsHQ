export interface TagInfoRootObject {
  graphql: TagInfoGraphql;
}

export interface TagInfoGraphql {
  hashtag: TagInfoHashtag;
}

export interface TagInfoHashtag {
  id: string;
  name: string;
  allow_following: boolean;
  is_following: boolean;
  is_top_media_only: boolean;
  profile_pic_url: string;
  edge_hashtag_to_media: TagInfoEdge_hashtag_to_media;
  edge_hashtag_to_top_posts: TagInfoEdge_hashtag_to_top_posts;
  edge_hashtag_to_content_advisory: TagInfoEdge_hashtag_to_content_advisory;
  edge_hashtag_to_related_tags: TagInfoEdge_hashtag_to_related_tags;
  edge_hashtag_to_null_state: TagInfoEdge_hashtag_to_null_state;
}

export interface TagInfoEdge_hashtag_to_media {
  count: number;
  page_info: TagInfoPage_info;
  edges: TagInfoEdgesItem[];
}

export interface TagInfoPage_info {
  has_next_page: boolean;
  end_cursor: string;
}

export interface TagInfoEdgesItem {
  node: TagInfoNode;
}

export interface TagInfoNode {
  comments_disabled?: boolean;
  __typename?: string;
  id?: string;
  edge_media_to_caption?: TagInfoEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: TagInfoEdge_media_to_comment;
  taken_at_timestamp?: number;
  dimensions?: TagInfoDimensions;
  display_url?: string;
  edge_liked_by?: TagInfoEdge_liked_by;
  edge_media_preview_like?: TagInfoEdge_media_preview_like;
  owner?: TagInfoOwner;
  thumbnail_src?: string;
  thumbnail_resources?: TagInfoThumbnailResourcesItem[];
  is_video?: boolean;
  accessibility_caption?: string;
  text?: string;
  product_type?: string;
  video_view_count?: number;
  name?: string;
}

export interface TagInfoEdge_media_to_caption {
  edges: TagInfoEdgesItem[];
}

export interface TagInfoEdge_media_to_comment {
  count: number;
}

export interface TagInfoDimensions {
  height: number;
  width: number;
}

export interface TagInfoEdge_liked_by {
  count: number;
}

export interface TagInfoEdge_media_preview_like {
  count: number;
}

export interface TagInfoOwner {
  id: string;
}

export interface TagInfoThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface TagInfoEdge_hashtag_to_top_posts {
  edges: TagInfoEdgesItem[];
}

export interface TagInfoEdge_hashtag_to_content_advisory {
  count: number;
  edges: any[];
}

export interface TagInfoEdge_hashtag_to_related_tags {
  edges: TagInfoEdgesItem[];
}

export interface TagInfoEdge_hashtag_to_null_state {
  edges: any[];
}
