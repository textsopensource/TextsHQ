export interface DirectItemAckRootObject {
  action: string;
  status_code: string;
  payload: DirectItemAckPayload;
  status: string;
}

export interface DirectItemAckPayload {
  client_context: string;
  item_id: string;
  timestamp: string;
  thread_id: string;
}
