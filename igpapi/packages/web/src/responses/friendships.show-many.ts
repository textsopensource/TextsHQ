export interface FriendshipsShowManyRootObject {
  friendship_statuses: FriendshipsShowManyFriendship_statuses;
  status: string;
}

export type FriendshipsShowManyFriendship_statuses = { [x: string]: FriendshipsShowManyStatus };

export interface FriendshipsShowManyStatus {
  following: boolean;
  incoming_request: boolean;
  is_bestie: boolean;
  is_private: boolean;
  is_restricted: boolean;
  outgoing_request: boolean;
}
