export interface DirectGetPresenceRootObject {
  user_presence: DirectGetPresenceUser_presence;
  status: string;
}

export type DirectGetPresenceUser_presence = { [x: string]: DirectGetPresenceItem };

export interface DirectGetPresenceItem {
  is_active: boolean;
  last_activity_at_ms: number;
}
