export interface StatusResponse {
  status: 'ok' | string;
}
