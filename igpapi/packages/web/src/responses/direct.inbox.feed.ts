export interface DirectInboxFeedRootObject {
  viewer: DirectInboxFeedViewer;
  inbox: DirectInboxFeedInbox;
  seq_id: number;
  pending_requests_total: number;
  has_pending_top_requests: boolean;
  most_recent_inviter: DirectInboxFeedMost_recent_inviter;
  status: string;
}

export interface DirectInboxFeedViewer {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  reel_auto_archive: string;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectInboxFeedInbox {
  threads: DirectInboxFeedThreadsItem[];
  has_older: boolean;
  oldest_cursor?: string;
  unseen_count: number;
  unseen_count_ts: number;
  prev_cursor: DirectInboxFeedPrev_cursor;
  next_cursor: DirectInboxFeedNext_cursor;
  blended_inbox_enabled: boolean;
}

export interface DirectInboxFeedThreadsItem {
  thread_id: string;
  thread_v2_id: string;
  users: DirectInboxFeedUsersItem[];
  left_users: DirectInboxFeedLeftUsersItem[];
  admin_user_ids: number[];
  items: DirectInboxFeedItemsItem[];
  last_activity_at: number;
  muted: boolean;
  is_pin: boolean;
  named: boolean;
  canonical: boolean;
  pending: boolean;
  archived: boolean;
  thread_type: string;
  viewer_id: number;
  thread_title: string;
  folder: number;
  vc_muted: boolean;
  is_group: boolean;
  mentions_muted: boolean;
  approval_required_for_new_members: boolean;
  input_mode: number;
  business_thread_folder: number;
  read_state: number;
  last_non_sender_item_at: number;
  assigned_admin_id: number;
  inviter: DirectInboxFeedInviter;
  has_older: boolean;
  has_newer: boolean;
  last_seen_at: DirectInboxFeedLast_seen_at;
  newest_cursor: string;
  oldest_cursor: string;
  next_cursor: string;
  prev_cursor: string;
  last_permanent_item: DirectInboxFeedLast_permanent_item;
}

export interface DirectInboxFeedUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectInboxFeedFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectInboxFeedFriendship_status {
  following: boolean;
  blocking: boolean;
  is_private: boolean;
  incoming_request: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
  followed_by?: boolean;
  muting?: boolean;
}

export interface DirectInboxFeedItemsItem {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  reel_share?: DirectInboxFeedReel_share;
  placeholder?: DirectInboxFeedPlaceholder;
  client_context?: string;
  text?: string;
  story_share?: DirectInboxFeedStory_share;
  video_call_event?: DirectInboxFeedVideo_call_event;
  voice_media?: DirectInboxFeedVoice_media;
  media?: DirectInboxFeedMedia;
  action_log?: DirectInboxFeedAction_log;
  hide_in_thread?: number;
  reactions?: DirectInboxFeedReactions;
  raven_media?: DirectInboxFeedRaven_media;
  reply_type?: string;
  expiring_media_action_summary?: DirectInboxFeedExpiring_media_action_summary;
  seen_user_ids?: string[];
  view_mode?: string;
  seen_count?: number;
  replay_expiring_at_us?: number;
  animated_media?: DirectInboxFeedAnimated_media;
  text_entities?: DirectInboxFeedText_entities;
}

export interface DirectInboxFeedReel_share {
  text: string;
  type: string;
  reel_owner_id: number;
  is_reel_persisted: boolean;
  mentioned_user_id: number;
  reel_type: null;
  media: DirectInboxFeedMedia;
}

export interface DirectInboxFeedMedia {
  taken_at?: number;
  pk?: number;
  id: string | number;
  device_timestamp?: number;
  media_type?: number;
  code?: string;
  client_cache_key?: string;
  filter_type?: number;
  image_versions2?: DirectInboxFeedImage_versions2;
  original_width?: number;
  original_height?: number;
  user?: DirectInboxFeedUser;
  caption_is_edited?: boolean;
  comment_likes_enabled?: boolean;
  comment_threading_enabled?: boolean;
  has_more_comments?: boolean;
  max_num_visible_preview_comments?: number;
  preview_comments?: any[];
  comments?: any[];
  can_view_more_preview_comments?: boolean;
  comment_count?: number;
  caption_position?: number;
  is_reel_media?: boolean;
  timezone_offset?: number;
  like_count?: number;
  has_liked?: boolean;
  likers?: any[];
  photo_of_you?: boolean;
  caption?: null;
  fb_user_tags?: DirectInboxFeedFb_user_tags;
  can_viewer_save?: boolean;
  organic_tracking_token?: string;
  expiring_at?: number;
  story_is_saved_to_archive?: boolean;
  imported_taken_at?: number;
  product_type?: string;
  audio?: DirectInboxFeedAudio;
}

export interface DirectInboxFeedImage_versions2 {
  candidates: DirectInboxFeedCandidatesItem[];
}

export interface DirectInboxFeedCandidatesItem {
  width: number;
  height: number;
  url: string;
}

export interface DirectInboxFeedUser {
  pk?: number;
  username: string;
  full_name?: string;
  is_private?: boolean;
  profile_pic_url?: string;
  profile_pic_id?: string;
  has_anonymous_profile_picture?: boolean;
  can_boost_post?: boolean;
  can_see_organic_insights?: boolean;
  show_insights_terms?: boolean;
  reel_auto_archive?: string;
  is_unpublished?: boolean;
  allowed_commenter_type?: string;
  is_verified?: boolean;
  friendship_status?: DirectInboxFeedFriendship_status;
}

export interface DirectInboxFeedFb_user_tags {
  in: any[];
}

export interface DirectInboxFeedPlaceholder {
  is_linked: boolean;
  title: string;
  message: string;
}

export interface DirectInboxFeedStory_share {
  is_linked: boolean;
  title: string;
  message: string;
  reason: number;
  text: string | null;
  media?: DirectInboxFeedMedia;
  reel_id?: number;
  reel_type?: string;
  is_reel_persisted?: boolean;
  story_share_type?: string;
}

export interface DirectInboxFeedInviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  reel_auto_archive?: string;
  allowed_commenter_type?: string;
}

export type DirectInboxFeedLast_seen_at = {
  [x: string]: DirectInboxFeedLastSeenAtUser;
};

export interface DirectInboxFeedLastSeenAtUser {
  timestamp: string;
  item_id: string;
}

export interface DirectInboxFeedLast_permanent_item {
  item_id: string;
  user_id: number;
  timestamp: number;
  item_type: string;
  reel_share?: DirectInboxFeedReel_share;
  video_call_event?: DirectInboxFeedVideo_call_event;
  action_log?: DirectInboxFeedAction_log;
  hide_in_thread?: number;
  client_context?: string;
}

export interface DirectInboxFeedVideo_call_event {
  action: string;
  vc_id: number;
  encoded_server_data_info: null | string;
  description: string;
  text_attributes: any[];
  did_join: null;
}

export interface DirectInboxFeedVoice_media {
  media: DirectInboxFeedMedia;
  seen_user_ids: any[];
  view_mode: string;
  seen_count: number;
  replay_expiring_at_us: null;
  expiring_media_action_summary?: DirectInboxFeedExpiring_media_action_summary;
}

export interface DirectInboxFeedAudio {
  audio_src: string;
  duration: number;
  waveform_data: number[] | null[];
  waveform_sampling_frequency_hz: number;
}

export interface DirectInboxFeedExpiring_media_action_summary {
  type: string;
  timestamp: number;
  count: number;
}

export interface DirectInboxFeedLeftUsersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  friendship_status: DirectInboxFeedFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_using_unified_inbox_for_direct: boolean;
  interop_messaging_user_fbid: number;
}

export interface DirectInboxFeedAction_log {
  description: string;
  bold: DirectInboxFeedBoldItem[];
  text_attributes?: DirectInboxFeedTextAttributesItem[];
}

export interface DirectInboxFeedBoldItem {
  start: number;
  end: number;
}

export interface DirectInboxFeedTextAttributesItem {
  start: number;
  end: number;
  bold: number;
  color: string;
  intent: string;
}

export interface DirectInboxFeedReactions {
  likes: DirectInboxFeedLikesItem[];
  likes_count: number;
}

export interface DirectInboxFeedLikesItem {
  sender_id: number;
  timestamp: number;
  client_context: string;
}

export interface DirectInboxFeedRaven_media {
  media_type: number;
}

export interface DirectInboxFeedAnimated_media {
  id: string;
  images: DirectInboxFeedImages;
  is_random: boolean;
  is_sticker: boolean;
  user: DirectInboxFeedUser;
}

export interface DirectInboxFeedImages {
  fixed_height: DirectInboxFeedFixed_height;
}

export interface DirectInboxFeedFixed_height {
  height: string;
  mp4: string;
  mp4_size: string;
  size: string;
  url: string;
  webp: string;
  webp_size: string;
  width: string;
}

export interface DirectInboxFeedText_entities {
  mentioned_user_ids: number[];
}

export interface DirectInboxFeedPrev_cursor {
  cursor_timestamp_seconds: number;
  cursor_thread_v2_id: number;
}

export interface DirectInboxFeedNext_cursor {
  cursor_timestamp_seconds: number;
  cursor_thread_v2_id: number;
}

export interface DirectInboxFeedMost_recent_inviter {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
}
