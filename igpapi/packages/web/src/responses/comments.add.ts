export interface CommentsAddRootObject {
  id: string;
  from: CommentsAddFrom;
  text: string;
  created_time: number;
  status: string;
}

export interface CommentsAddFrom {
  id: string;
  username: string;
  full_name: string;
  profile_picture: string;
}
