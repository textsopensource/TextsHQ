export interface UsersInfoRootObject {
  logging_page_id: string;
  show_suggested_profiles: boolean;
  show_follow_dialog: boolean;
  graphql: UsersInfoGraphql;
  toast_content_on_load: null;
}

export interface UsersInfoGraphql {
  user: UsersInfoUser;
}

export interface UsersInfoUser {
  biography: string;
  blocked_by_viewer: boolean;
  restricted_by_viewer: boolean;
  country_block: boolean;
  external_url: string;
  external_url_linkshimmed: string;
  edge_followed_by: UsersInfoEdge_followed_by;
  followed_by_viewer: boolean;
  edge_follow: UsersInfoEdge_follow;
  follows_viewer: boolean;
  full_name: string;
  has_channel: boolean;
  has_blocked_viewer: boolean;
  highlight_reel_count: number;
  has_requested_viewer: boolean;
  id: string;
  is_business_account: boolean;
  is_joined_recently: boolean;
  business_category_name: string;
  category_id: string;
  overall_category_name: null;
  is_private: boolean;
  is_verified: boolean;
  edge_mutual_followed_by: UsersInfoEdge_mutual_followed_by;
  profile_pic_url: string;
  profile_pic_url_hd: string;
  requested_by_viewer: boolean;
  username: string;
  connected_fb_page: null;
  edge_felix_video_timeline: UsersInfoEdge_felix_video_timeline;
  edge_owner_to_timeline_media: UsersInfoEdge_owner_to_timeline_media;
  edge_saved_media: UsersInfoEdge_saved_media;
  edge_media_collections: UsersInfoEdge_media_collections;
}

export interface UsersInfoEdge_followed_by {
  count: number;
}

export interface UsersInfoEdge_follow {
  count: number;
}

export interface UsersInfoEdge_mutual_followed_by {
  count: number;
  edges: UsersInfoEdgesItem[];
}

export interface UsersInfoEdgesItem {
  node: UsersInfoNode;
}

export interface UsersInfoNode {
  username?: string;
  __typename?: string;
  id?: string;
  edge_media_to_caption?: UsersInfoEdge_media_to_caption;
  shortcode?: string;
  edge_media_to_comment?: UsersInfoEdge_media_to_comment;
  comments_disabled?: boolean;
  taken_at_timestamp?: number;
  dimensions?: UsersInfoDimensions;
  display_url?: string;
  edge_liked_by?: UsersInfoEdge_liked_by;
  edge_media_preview_like?: UsersInfoEdge_media_preview_like;
  location?: null | UsersInfoLocation;
  gating_info?: null;
  fact_check_overall_rating?: null;
  fact_check_information?: null;
  media_preview?: null | string;
  owner?: UsersInfoOwner;
  thumbnail_src?: string;
  thumbnail_resources?: UsersInfoThumbnailResourcesItem[];
  is_video?: boolean;
  felix_profile_grid_crop?: null;
  encoding_status?: null;
  is_published?: boolean;
  product_type?: string;
  title?: string;
  video_duration?: number;
  video_view_count?: number;
  text?: string;
  accessibility_caption?: string;
}

export interface UsersInfoEdge_felix_video_timeline {
  count: number;
  page_info: UsersInfoPage_info;
  edges: UsersInfoEdgesItem[];
}

export interface UsersInfoPage_info {
  has_next_page: boolean;
  end_cursor: string | null;
}

export interface UsersInfoEdge_media_to_caption {
  edges: UsersInfoEdgesItem[];
}

export interface UsersInfoEdge_media_to_comment {
  count: number;
}

export interface UsersInfoDimensions {
  height: number;
  width: number;
}

export interface UsersInfoEdge_liked_by {
  count: number;
}

export interface UsersInfoEdge_media_preview_like {
  count: number;
}

export interface UsersInfoOwner {
  id: string;
  username: string;
}

export interface UsersInfoThumbnailResourcesItem {
  src: string;
  config_width: number;
  config_height: number;
}

export interface UsersInfoEdge_owner_to_timeline_media {
  count: number;
  page_info: UsersInfoPage_info;
  edges: UsersInfoEdgesItem[];
}

export interface UsersInfoLocation {
  id: string;
  has_public_page: boolean;
  name: string;
  slug: string;
}

export interface UsersInfoEdge_saved_media {
  count: number;
  page_info: UsersInfoPage_info;
  edges: any[];
}

export interface UsersInfoEdge_media_collections {
  count: number;
  page_info: UsersInfoPage_info;
  edges: any[];
}
