import {
  IgvideoEntity,
  RuploadIgphotoRequest,
  RuploadIgvideoSegmentedRequest,
  SectionSizeBufferSegmentDivider,
} from '@textshq/core';
import { Service } from 'typedi';
import { IgtvWebRepository } from '../repositories/igtv.web.repository';
import Chance from 'chance';

const chance = new Chance();

@Service({ transient: true, global: true })
export class IgtvWebVideoEntity extends IgvideoEntity {
  title: string;
  caption?: string;
  sharePreviewToFeed: boolean;
  protected divider = new SectionSizeBufferSegmentDivider(10000000);
  private now = Date.now().toString();

  constructor(
    protected ruploadIgvideo: RuploadIgvideoSegmentedRequest,
    protected ruploadIgphotoRepository: RuploadIgphotoRequest,
    private igtvWebRepository: IgtvWebRepository,
  ) {
    super();
  }

  entityName(segmentByteLength?: number, segmentIndex?: number) {
    return `igtv_${this.now}`;
  }

  uploadId() {
    return this.now;
  }

  // full example - {"is_igtv_video":true,"media_type":2,"for_album":false,"video_format":"video/mp4","upload_id":"1591209063061","upload_media_duration_ms":125952,"upload_media_height":240,"upload_media_width":320,"video_transform":null}
  ruploadParams() {
    return {
      is_igtv_video: true,
      media_type: 2,
      for_album: false,
    };
  }

  headers() {
    return {
      referer: 'https://www.instagram.com/tv/upload',
    };
  }

  async upload() {
    await this.ruploadIgvideo.execute(this);
    await this.ruploadIgphotoRepository.execute({
      entityName: this.entityName(),
      file: this.cover,
      ruploadParams: {
        media_type: 2,
        upload_id: this.uploadId(),
      },
    });
  }

  async execute() {
    await this.upload();
    return this.igtvWebRepository.configureToIgtv({
      title: this.title,
      caption: this.caption,
      igtv_share_preview_to_feed: this.sharePreviewToFeed,
      upload_id: this.uploadId(),
      igtv_composer_session_id: chance.guid({ version: 4 }),
    });
  }
}
