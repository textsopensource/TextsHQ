export interface RequestOneTapLoginNonceResponse {
  login_nonce: string;
  status: string;
}
