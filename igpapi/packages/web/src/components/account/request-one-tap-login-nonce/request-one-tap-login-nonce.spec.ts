import { WebIgpapi } from '../../..';
import { RequestOneTapLoginNonceCommand } from './request-one-tap-login-nonce.command';
import { JsonTs } from '@textshq/tools/json-ts';

test('request-one-tap-login-nonce.command', async () => {
  const igpapi = await WebIgpapi.create();
  const nonce = await igpapi.execute(RequestOneTapLoginNonceCommand, {});
  await JsonTs.generate(nonce, __dirname, 'request-one-tap-login-nonce');
  expect(nonce).toHaveProperty('login_nonce');
});
