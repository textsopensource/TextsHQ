import { Command, IgpapiCommand, IgpapiHttp } from '@textshq/core';
import { RequestOneTapLoginNonceResponse } from './request-one-tap-login-nonce.response';

@Command()
export class RequestOneTapLoginNonceCommand implements IgpapiCommand {
  constructor(private http: IgpapiHttp) {}

  execute() {
    return this.http.body<RequestOneTapLoginNonceResponse>({
      url: '/accounts/request_one_tap_login_nonce/',
      form: {},
    });
  }
}
