import { Command, IgpapiCommand } from '@textshq/core';
import crypto from 'crypto';
import { WebHttp, WebState } from '../../../core';
import { SharedDataRequest } from '../../data/shared-data/shared-data.request';
import { SharedDataRootObject } from '../../data/shared-data/shared-data.response';
import { LoginResponse } from './login.response';
// tweetnacl-sealedbox-js has no types
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { seal } = require('tweetnacl-sealedbox-js');

@Command()
export class LoginCommand implements IgpapiCommand {
  username?: string = process.env.IG_USERNAME;
  password?: string = process.env.IG_PASSWORD;
  sharedData?: SharedDataRootObject;

  constructor(private http: WebHttp, private state: WebState, private sharedDataRequest: SharedDataRequest) {}

  public async execute(): Promise<LoginResponse> {
    if (!this.state.device.userAgent) {
      this.state.device.generate(this.username);
    }
    if (!this.sharedData) {
      this.sharedData = await this.sharedDataRequest.execute();
    }
    const { encrypted, time } = this.encryptPassword();
    return this.http.body<LoginResponse>({
      url: `/accounts/login/ajax/`,
      method: 'POST',
      form: {
        username: this.username,
        enc_password: `#PWD_INSTAGRAM_BROWSER:10:${time}:${encrypted}`,
        query_params: '{}',
        optIntoOneTap: false,
      },
    });
  }

  private encryptPassword() {
    const password = this.password;
    if (!this.sharedData) throw new Error('SharedData is undefined');
    if (!password) throw new Error('password is undefined');
    const time = Math.round(Date.now() / 1000).toString();
    const key = crypto.pseudoRandomBytes(32);
    const iv = Buffer.alloc(12, 0);
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv, { authTagLength: 16 }).setAAD(Buffer.from(time));
    const aesEncrypted = Buffer.concat([cipher.update(Buffer.from(password)), cipher.final()]);
    const authTag = cipher.getAuthTag();
    const encryptedKey = seal(key, Buffer.from(this.sharedData.encryption.public_key, 'hex'));
    return {
      encrypted: Buffer.concat([
        Buffer.from([
          1,
          Number(this.sharedData.encryption.key_id),
          encryptedKey.byteLength & 255,
          (encryptedKey.byteLength >> 8) & 255,
        ]),
        encryptedKey,
        authTag,
        aesEncrypted,
      ]).toString('base64'),
      time,
    };
  }
}
