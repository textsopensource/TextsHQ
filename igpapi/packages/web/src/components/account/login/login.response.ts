export interface LoginResponse {
  user: boolean;
  userId: string;
  authenticated: boolean;
  oneTapPrompt: boolean;
  status: string;
}
