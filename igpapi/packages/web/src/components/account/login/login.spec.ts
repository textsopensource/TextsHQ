import { WebIgpapi } from '../../..';
import { LoginCommand } from './login.command';
import { JsonTs } from '@textshq/tools';

test('login.command', async () => {
  const igpapi = new WebIgpapi();
  const response = await igpapi.execute(LoginCommand, {});
  await JsonTs.generate(response, __dirname, 'login');
  expect(response).toBeDefined();
});
