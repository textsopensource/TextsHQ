import { IgpapiRequest, IgRequest } from '@textshq/core';
import { WebHttp } from '../../../core';
import { SharedDataRootObject } from './shared-data.response';

@IgRequest()
export class SharedDataRequest implements IgpapiRequest {
  constructor(private http: WebHttp) {}

  public execute() {
    return this.http.body<SharedDataRootObject>({
      url: `/data/shared_data/`,
    });
  }
}
