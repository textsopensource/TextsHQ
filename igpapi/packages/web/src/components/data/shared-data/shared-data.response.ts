export interface SharedDataRootObject {
  config: SharedDataConfig;
  country_code: string;
  language_code: string;
  locale: string;
  entry_data: SharedDataEntry_data;
  hostname: string;
  is_whitelisted_crawl_bot: boolean;
  deployment_stage: string;
  platform: string;
  nonce: string;
  mid_pct: number;
  zero_data: SharedDataZero_data;
  cache_schema_version: number;
  server_checks: SharedDataServer_checks;
  knobx: SharedDataKnobx;
  to_cache: SharedDataTo_cache;
  device_id: string;
  encryption: SharedDataEncryption;
  rollout_hash: string;
  bundle_variant: string;
  is_canary: boolean;
}

export interface SharedDataConfig {
  csrf_token: string;
  viewer: SharedDataViewer;
  viewerId: string;
}

export interface SharedDataViewer {
  biography: string;
  external_url: null;
  full_name: string;
  has_phone_number: boolean;
  has_profile_pic: boolean;
  has_tabbed_inbox: boolean;
  id: string;
  is_joined_recently: boolean;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_url_hd: string;
  username: string;
  badge_count: string;
}

export interface SharedDataEntry_data {}

export interface SharedDataZero_data {}

export interface SharedDataServer_checks {
  hfe: boolean;
}

export interface SharedDataKnobx {
  4: boolean;
  17: boolean;
  20: boolean;
  22: boolean;
  23: boolean;
  24: boolean;
}

export interface SharedDataTo_cache {
  gatekeepers: { [x: number]: boolean };
  qe: SharedDataQe;
  probably_has_app: boolean;
  cb: boolean;
}

export interface SharedDataQe {
  0: SharedData0;
  2: SharedData2;
  4: SharedData4;
  5: SharedData5;
  6: SharedData6;
  10: SharedData10;
  12: SharedData12;
  13: SharedData13;
  16: SharedData16;
  17: SharedData17;
  19: SharedData19;
  21: SharedData21;
  22: SharedData22;
  23: SharedData23;
  25: SharedData25;
  26: SharedData26;
  28: SharedData28;
  29: SharedData29;
  30: SharedData30;
  31: SharedData31;
  33: SharedData33;
  34: SharedData34;
  35: SharedData35;
  36: SharedData36;
  37: SharedData37;
  39: SharedData39;
  41: SharedData41;
  42: SharedData42;
  43: SharedData43;
  44: SharedData44;
  45: SharedData45;
  46: SharedData46;
  47: SharedData47;
  49: SharedData49;
  50: SharedData50;
  53: SharedData53;
  54: SharedData54;
  55: SharedData55;
  56: SharedData56;
  58: SharedData58;
  59: SharedData59;
  62: SharedData62;
  64: SharedData64;
  65: SharedData65;
  66: SharedData66;
  67: SharedData67;
  68: SharedData68;
  69: SharedData69;
  70: SharedData70;
  71: SharedData71;
  72: SharedData72;
  73: SharedData73;
  74: SharedData74;
  75: SharedData75;
  76: SharedData76;
  77: SharedData77;
  78: SharedData78;
  80: SharedData80;
  81: SharedData81;
  82: SharedData82;
  83: SharedData83;
  84: SharedData84;
  app_upsell: SharedDataApp_upsell;
  igl_app_upsell: SharedDataIgl_app_upsell;
  notif: SharedDataNotif;
  onetaplogin: SharedDataOnetaplogin;
  multireg_iter: SharedDataMultireg_iter;
  felix_clear_fb_cookie: SharedDataFelix_clear_fb_cookie;
  felix_creation_duration_limits: SharedDataFelix_creation_duration_limits;
  felix_creation_fb_crossposting: SharedDataFelix_creation_fb_crossposting;
  felix_creation_fb_crossposting_v2: SharedDataFelix_creation_fb_crossposting_v2;
  felix_creation_validation: SharedDataFelix_creation_validation;
  mweb_topical_explore: SharedDataMweb_topical_explore;
  post_options: SharedDataPost_options;
  iglscioi: SharedDataIglscioi;
  sticker_tray: SharedDataSticker_tray;
  web_sentry: SharedDataWeb_sentry;
}

export interface SharedData0 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedDataP {
  4?: boolean | number;
  7?: boolean;
  8?: boolean;
  9?: boolean;
  0?: boolean | number | string;
  1?: boolean | string;
  5?: boolean;
  6?: boolean;
  10?: boolean | number;
  2?: boolean | number | string;
  3?: number | boolean | string;
  11?: number | boolean;
  12?: number | boolean;
  13?: boolean;
  14?: boolean;
  17?: number;
  18?: boolean;
  19?: number;
  22?: boolean;
  23?: string;
  24?: boolean;
  25?: string;
  26?: string;
  has_back_removed?: string;
  is_enabled?: string;
  blacklist?: string;
  maximum_length_seconds?: string;
  minimum_length_seconds?: string;
  display_version?: string;
  edit_video_controls?: string;
  description_maximum_length?: string;
  max_video_size_in_bytes?: string;
  minimum_length_for_feed_preview_seconds?: string;
  title_maximum_length?: string;
  valid_cover_mime_types?: string;
  valid_video_extensions?: string;
  valid_video_mime_types?: string;
  enable_igtv_embed?: string;
  use_refactor?: string;
}

export interface SharedData2 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData4 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData5 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData6 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData10 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData12 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData13 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData16 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData17 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData19 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData21 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData22 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData23 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData25 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData26 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData28 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData29 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData30 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData31 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData33 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData34 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData35 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData36 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData37 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData39 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData41 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData42 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData43 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData44 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData45 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData46 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData47 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData49 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData50 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData53 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData54 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData55 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData56 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData58 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData59 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData62 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData64 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData65 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData66 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData67 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData68 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData69 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData70 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData71 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData72 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData73 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData74 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData75 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData76 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData77 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData78 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData80 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData81 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData82 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData83 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedData84 {
  p: SharedDataP;
  qex: boolean;
}

export interface SharedDataApp_upsell {
  g: string;
  p: SharedDataP;
}

export interface SharedDataIgl_app_upsell {
  g: string;
  p: SharedDataP;
}

export interface SharedDataNotif {
  g: string;
  p: SharedDataP;
}

export interface SharedDataOnetaplogin {
  g: string;
  p: SharedDataP;
}

export interface SharedDataMultireg_iter {
  g: string;
  p: SharedDataP;
}

export interface SharedDataFelix_clear_fb_cookie {
  g: string;
  p: SharedDataP;
}

export interface SharedDataFelix_creation_duration_limits {
  g: string;
  p: SharedDataP;
}

export interface SharedDataFelix_creation_fb_crossposting {
  g: string;
  p: SharedDataP;
}

export interface SharedDataFelix_creation_fb_crossposting_v2 {
  g: string;
  p: SharedDataP;
}

export interface SharedDataFelix_creation_validation {
  g: string;
  p: SharedDataP;
}

export interface SharedDataMweb_topical_explore {
  g: string;
  p: SharedDataP;
}

export interface SharedDataPost_options {
  g: string;
  p: SharedDataP;
}

export interface SharedDataIglscioi {
  g: string;
  p: SharedDataP;
}

export interface SharedDataSticker_tray {
  g: string;
  p: SharedDataP;
}

export interface SharedDataWeb_sentry {
  g: string;
  p: SharedDataP;
}

export interface SharedDataEncryption {
  key_id: string;
  public_key: string;
}
