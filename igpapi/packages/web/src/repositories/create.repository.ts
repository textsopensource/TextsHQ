import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { CreateConfigureOptions, CreateConfigureToStoryOptions } from '../types';
import { CreateConfigureRootObject } from '../responses';

@Service()
export class CreateRepository {
  constructor(private http: WebHttp) {}

  public configure(options: CreateConfigureOptions): Promise<CreateConfigureRootObject> {
    return this.http.body({
      url: '/create/configure/',
      method: 'POST',
      form: {
        upload_id: options.uploadId,
        caption: options.caption ?? '',
        ...(options.location && {
          geotag_enabled: true,
          location: JSON.stringify({
            lat: options.location.lat,
            lng: options.location.lng,
            facebook_places_id: options.location.external_id,
          }),
        }),
        usertags:
          options.usertags &&
          JSON.stringify({ in: options.usertags.map(user => ({ user_id: user.userId, position: user.position })) }),
        custom_accessibility_caption: options.customAccessibilityCaption ?? '',
        retry_timeout: options.retryTimeout ?? '',
      },
    });
  }

  public configureToStory(options: CreateConfigureToStoryOptions) {
    return this.http.body({
      url: '/create/configure_to_story/',
      form: {
        upload_id: options.uploadId,
        caption: options.caption ?? '',
      },
    });
  }
}
