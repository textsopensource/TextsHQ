import { WebHttp } from '../core/web.http';
import { CommentsAddRootObject, StatusResponse } from '../responses';
import { Service } from 'typedi';

@Service()
export class CommentsRepository {
  constructor(private http: WebHttp) {}

  public async like(commentId: string): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/web/comments/like/${commentId}/`,
      method: 'POST',
    });
    return body;
  }
  public async unlike(commentId: string): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/web/comments/unlike/${commentId}/`,
      method: 'POST',
    });
    return body;
  }
  public async add(options: {
    mediaId: string;
    text: string;
    repliedToCommentId?: string;
  }): Promise<CommentsAddRootObject> {
    const { body } = await this.http.full<CommentsAddRootObject>({
      url: `/web/comments/${options.mediaId}/add/`,
      method: 'POST',
      form: {
        comment_text: options.text,
        replied_to_comment_id: options.repliedToCommentId,
      },
    });
    return body;
  }
  public async delete(options: { mediaId: string; commentId: string }): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/web/comments/${options.mediaId}/delete/${options.commentId}/`,
      method: 'POST',
    });
    return body;
  }
}
