import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import {
  DirectAddUsersRootObject,
  DirectCreateGroupThreadRootObject,
  DirectGetBadgeCountRootObject,
  DirectGetPresenceItem,
  DirectItemAckRootObject,
  DirectMarkAsSeenRootObject,
  DirectPendingInboxRootObject,
  DirectRankedRecipientsRootObject,
  DirectUpdateTitleRootObject,
  StatusResponse,
} from '../responses';
import {
  DirectConfigurePhotoOptions,
  DirectLinkShareOptions,
  DirectReelReactOptions,
  DirectReelShareOptions,
  DirectStoryShareOptions,
} from '../types';
import { Chance } from 'chance';

@Service()
export class DirectRepository {
  protected static linkRegex = new RegExp(
    // see MobileDirectPage.js / DesktopDirectPage.js 'e.getLinkRegex'
    // eslint-disable-next-line max-len
    '(?:(?:(?:[a-z]+:)?//)?|www\\.)(?:\\S+(?::\\S*)?@)?(?:localhost|(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*(?:\\.(?:((?:[a-z\\u00a1-\\uffff]{2,}))))\\.?)(?::\\d{2,5})?(?:[/?#][^\\s"]*)?',
    'ig',
  );

  protected chance = new Chance();

  constructor(private http: WebHttp) {}

  /**
   * Gets the number displayed as a badge on the "direct-plane"
   * @param {number} noRaven - hard-coded 1
   * @returns {Promise<DirectGetBadgeCountRootObject>}
   */
  public getBadgeCount(noRaven = 1): Promise<DirectGetBadgeCountRootObject> {
    return this.http.body({
      url: '/direct_v2/web/get_badge_count/',
      searchParams: {
        no_raven: noRaven,
      },
    });
  }

  /**
   * Gets the currently active (or not active) users
   * @returns {Promise<DirectGetPresenceItem>}
   */
  public getPresence(): Promise<DirectGetPresenceItem> {
    return this.http.body({
      url: '/direct_v2/web/get_presence/',
    });
  }

  public pendingInbox(): Promise<DirectPendingInboxRootObject> {
    /**
     * TODO: feed?
     *
     * Consumer.js
     *     (e.getPendingInbox = function() {
     *      return get('/direct_v2/web/pending_inbox/');
     *    }),
     */
    return this.http.body({
      url: '/direct_v2/web/pending_inbox/',
    });
  }

  public rankedRecipients(
    query: string,
    mode: 'raven' | 'reshare' = 'reshare',
  ): Promise<DirectRankedRecipientsRootObject> {
    return this.http.body({
      url: '/direct_v2/web/ranked_recipients/',
      searchParams: {
        mode,
        query,
        show_threads: mode === 'raven',
      },
    });
  }

  public markAsSeen(threadId: string, itemId: string): Promise<DirectMarkAsSeenRootObject> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/items/${itemId}/seen/`,
      method: 'POST',
    });
  }

  public approveAll(): Promise<StatusResponse> {
    return this.http.body({
      url: '/direct_v2/web/threads/approve_all/',
      method: 'POST',
    });
  }

  public approveMultiple(threadIds: string[], folder = ''): Promise<StatusResponse> {
    return this.http.body({
      url: '/direct_v2/web/threads/approve_multiple/',
      method: 'POST',
      form: {
        thread_ids: JSON.stringify(threadIds),
        folder,
      },
    });
  }

  public declineAll(): Promise<StatusResponse> {
    return this.http.body({
      url: '/direct_v2/web/threads/decline_all/',
      method: 'POST',
    });
  }

  public declineMultiple(threadIds: string[], folder = ''): Promise<StatusResponse> {
    return this.http.body({
      url: '/direct_v2/web/threads/decline_multiple/',
      method: 'POST',
      form: {
        thread_ids: JSON.stringify(threadIds),
        folder,
      },
    });
  }

  public deleteItem(threadId: string, itemId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/items/${itemId}/delete/`,
      method: 'POST',
    });
  }

  public createGroupThread(recipientUsers: string[]): Promise<DirectCreateGroupThreadRootObject> {
    return this.http.body({
      url: '/direct_v2/web/create_group_thread/',
      method: 'POST',
      form: {
        recipient_users: JSON.stringify(recipientUsers),
      },
    });
  }

  public addUsers(threadId: string, userIds: string[]): Promise<DirectAddUsersRootObject> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/add_user/`,
      method: 'POST',
      form: {
        user_ids: JSON.stringify(userIds),
      },
    });
  }

  public updateTitle(threadId: string, title: string): Promise<DirectUpdateTitleRootObject> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/update_title/`,
      method: 'POST',
      form: { title },
    });
  }

  public mute(threadId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/mute/`,
      method: 'POST',
    });
  }

  public unmute(threadId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/unmute/`,
      method: 'POST',
    });
  }

  public leave(threadId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/leave/`,
      method: 'POST',
    });
  }

  public hide(threadId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/hide/`,
      method: 'POST',
    });
  }

  public move(threadId: string, folder: string) {
    return this.http.body({
      url: `/direct_v2/web/threads/${threadId}/move/`,
      method: 'POST',
      form: {
        folder,
      },
    });
  }

  public configurePhoto(options: DirectConfigurePhotoOptions): Promise<DirectItemAckRootObject> {
    return this.http.body({
      url: '/direct_v2/web/threads/broadcast/configure_photo/',
      method: 'POST',
      form: {
        action: 'send_item',
        allow_full_aspect_ratio: '1',
        content_type: 'photo',
        mutation_token: options.mutationToken ?? this.chance.guid({ version: 4 }),
        sampled: '1',
        thread_id: options.threadId,
        upload_id: options.uploadId,
      },
    });
  }

  public linkShare(options: DirectLinkShareOptions): Promise<DirectItemAckRootObject> {
    return this.http.body({
      url: '/direct_v2/web/threads/broadcast/link/',
      method: 'POST',
      form: {
        action: 'send_item',
        client_context: this.chance.guid({ version: 4 }),
        link_text: options.linkText,
        link_urls: JSON.stringify(options.linkUrls ?? options.linkText.match(DirectRepository.linkRegex)),
        mutation_token: options.mutationToken ?? this.chance.guid({ version: 4 }),
        thread_id: options.threadId,
      },
    });
  }

  public reelShare(options: DirectReelShareOptions): Promise<DirectItemAckRootObject> {
    return this.http.body({
      url: '/direct_v2/web/threads/broadcast/reel_share/',
      method: 'POST',
      form: {
        action: 'send_item',
        client_context: this.chance.guid({ version: 4 }),
        thread_id: options.threadId,
        reel_id: options.reelId,
        media_id: options.mediaId,
        text: options.text ?? '',
      },
    });
  }

  public reelReact(options: DirectReelReactOptions): Promise<DirectItemAckRootObject> {
    return this.http.body({
      url: '/direct_v2/web/threads/broadcast/reel_react/',
      method: 'POST',
      form: {
        action: 'send_item',
        client_context: this.chance.guid({ version: 4 }),
        thread_id: options.threadId,
        reel_id: options.reelId,
        media_id: options.mediaId,
        reaction_emoji: options.reactionEmoji,
      },
    });
  }

  public storyShare(options: DirectStoryShareOptions): Promise<DirectItemAckRootObject> {
    return this.http.body({
      url: '/direct_v2/web/threads/broadcast/story_share/',
      method: 'POST',
      form: {
        action: 'send_item',
        client_context: this.chance.guid({ version: 4 }),
        thread_id: options.threadId,
        reel_id: options.reelId,
        story_media_id: options.storyMediaId,
      },
    });
  }
}
