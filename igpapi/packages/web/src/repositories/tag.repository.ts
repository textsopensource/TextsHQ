import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { TagInfoRootObject } from '../responses';

@Service()
export class TagRepository {
  constructor(private http: WebHttp) {}

  public async follow(name: string) {
    return this.http.body({
      url: `/web/tags/follow/${name}/`,
      method: 'POST',
    });
  }

  public async unfollow(name: string) {
    return this.http.body({
      url: `/web/tags/unfollow/${name}/`,
      method: 'POST',
    });
  }

  public async info(name: string): Promise<TagInfoRootObject> {
    return this.http.body({
      url: `/explore/tags/${name}/`,
      searchParams: { __a: 1 },
    });
  }
}
