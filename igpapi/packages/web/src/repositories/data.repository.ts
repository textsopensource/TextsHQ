import { DataSharedDataRootObject } from '../responses';
import { Service } from 'typedi';
import { WebHttp } from '../core/web.http';
import { WebState } from '../core/web.state';

@Service()
export class DataRepository {
  constructor(private http: WebHttp, private state: WebState) {}

  public async sharedData(): Promise<DataSharedDataRootObject> {
    const { body } = await this.http.full<DataSharedDataRootObject>({
      url: `/data/shared_data/`,
    });
    this.state.session.sharedData = body;
    return body;
  }
}
