import { FetchReelsMediaOptions, LoadPostPageExtrasOptions, SuggestedUsersOptions } from '../types';
import {
  GraphqlFeedPageExtrasRootObject,
  GraphqlFetchReelsMediaRootObject,
  GraphqlSuggestedUsersRootObject,
} from '../responses';
import { Service } from 'typedi';
import { WebHttp } from '../core/web.http';
import { WebState } from '../core/web.state';
import { QueryHash, QueryId } from '../core/web.constants';

@Service()
export class GraphqlRepository {
  constructor(private http: WebHttp, private state: WebState) {}

  /**
   *
   * @param {{queryHash: QueryHash; variables: any; requestOptions?: RequestOptions}} options
   *  queryHash is a constant value for a request,
   *  variables contains the variables for the query as an object
   * @returns {Promise<T>}
   */
  async query<T>(options: {
    queryHash: QueryHash;
    variables: object;
    requestOptions?: Partial<{ headers: { [x: string]: string } }>;
  }): Promise<T> {
    const { body } = await this.http.full<T>({
      url: '/graphql/query/',
      searchParams: {
        query_hash: options.queryHash,
        variables: JSON.stringify(options.variables),
      },
      ...options.requestOptions,
    });
    return body;
  }

  loadFeedPageExtras(options: LoadPostPageExtrasOptions = {}) {
    options = {
      only_stories: true,
      stories_prefetch: true,
      stories_video_dash_manifest: false,
      ...options,
    };
    return this.query<GraphqlFeedPageExtrasRootObject>({
      queryHash: QueryId.FeedPageExtras,
      variables: options,
    });
  }

  loadActivityCounts(id?: string) {
    return this.query({
      queryHash: QueryId.LoadActivityCounts,
      variables: { id: id ?? this.state.cookies.userId },
    });
  }

  suggestedUsers(options: SuggestedUsersOptions = {}) {
    options = {
      fetch_media_count: 0,
      fetch_suggested_count: 30,
      filter_followed_friends: true,
      ignore_cache: true,
      include_reel: true,
      ...options,
    };
    return this.query<GraphqlSuggestedUsersRootObject>({
      queryHash: QueryId.SuggestedUsers,
      variables: options,
    });
  }

  /**
   * ONLY ONE ARRAY IS ALLOWED TO BE POPULATED!
   * @param {FetchReelsMediaOptions} options
   * @returns {Promise<GraphqlFetchReelsMediaRootObject>}
   */
  fetchReelsMedia(options: FetchReelsMediaOptions) {
    options = {
      reel_ids: [],
      tag_names: [],
      location_ids: [],
      highlight_reel_ids: [],
      precomposed_overlay: false,
      show_story_viewer_list: true,
      story_viewer_fetch_count: 50,
      story_viewer_cursor: '',
      stories_video_dash_manifest: false,
      ...options,
    };
    return this.query<GraphqlFetchReelsMediaRootObject>({
      queryHash: QueryId.FetchReelsMedia,
      variables: options,
    });
  }
}
