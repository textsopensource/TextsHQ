import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { UsersInfoRootObject } from '../responses';

@Service()
export class UsersRepository {
  constructor(private http: WebHttp) {}

  public infoByName(name: string): Promise<UsersInfoRootObject> {
    return this.http.body({
      url: `/${name}/`,
      searchParams: {
        __a: 1,
      },
    });
  }
}
