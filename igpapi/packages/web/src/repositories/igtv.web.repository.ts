import { Service } from 'typedi';
import { IgpapiHttp } from '@textshq/core';

@Service()
export class IgtvWebRepository {
  constructor(private http: IgpapiHttp) {}
  configureToIgtv({
    caption,
    igtv_share_preview_to_feed,
    title,
    upload_id,
    igtv_composer_session_id,
  }: {
    caption?: string;
    igtv_share_preview_to_feed: boolean;
    title: string;
    upload_id: string | number;
    igtv_composer_session_id: string;
  }) {
    return this.http.body({
      url: '/igtv/configure_to_igtv/',
      form: {
        async_configure: 1,
        caption: caption ?? '',
        igtv_composer_session_id,
        igtv_share_preview_to_feed: igtv_share_preview_to_feed || '',
        publish_mode: 'igtv',
        title,
        upload_id,
      },
    });
  }
}
