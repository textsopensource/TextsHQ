import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { LocationSearchRootObject } from '../responses';

@Service()
export class LocationRepository {
  constructor(private http: WebHttp) {}

  public search({ latitude, longitude }: { latitude: number; longitude: number }): Promise<LocationSearchRootObject> {
    return this.http.body({
      url: '/location_search/',
      form: {
        latitude,
        longitude,
      },
    });
  }
}
