import { Service } from 'typedi';
import { WebState } from '../core/web.state';
import { WebHttp } from '../core/web.http';

@Service()
export class ChallengeRepository {
  private sharedData = /<script type="text\/javascript">window\._sharedData = (.+);<\/script>/;

  constructor(private http: WebHttp, private localState: WebState) {}

  private get checkpointUrl() {
    return this.localState.getChallengePath();
  }

  async state() {
    const { body, headers } = await this.http.full<any>({
      url: this.checkpointUrl,
    });
    if (headers['content-type']?.includes('json')) {
      return body;
    }
    const sharedDataExec = this.sharedData.exec(body)?.[1];
    if (!sharedDataExec) {
      throw new Error('Something went wrong while parsing the challenge HTML page');
    }
    try {
      const state = JSON.parse(sharedDataExec);
      return state.entry_data.Challenge[0];
    } catch (e) {
      throw new Error('Something went wrong while parsing the challenge HTML page');
    }
  }

  /**
   * Select verification method.
   * @param choice Verification method. Phone number = 0, email = 1
   */
  selectVerifyMethod(choice: string) {
    return this.post({ choice });
  }

  /**
   * Send the code received in the message
   */
  sendSecurityCode(code: string) {
    return this.post({ security_code: code });
  }

  private async post(params?: object) {
    const { body } = await this.http.full({
      url: this.checkpointUrl,
      method: 'POST',
      form: {
        ...params,
      },
    });
    return body;
  }
}
