import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { StoriesReelSeenOptions } from '../types';
import { StatusResponse } from '../responses';

@Service()
export class StoriesRepository {
  constructor(private http: WebHttp) {}

  public reelSeen(options: StoriesReelSeenOptions): Promise<StatusResponse> {
    options = {
      viewSeenAt: options.reelMediaTakenAt,
      ...options,
    };
    return this.http.body({
      url: '/stories/reel/seen',
      method: 'POST',
      form: options,
    });
  }
}
