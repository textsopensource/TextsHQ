import { WebHttp } from '../core/web.http';
import { imageSize } from 'image-size';
import { MediaPublishMode, MediaType, UploadMode, UploadPhotoOptions, UploadVideoOptions, VideoData } from '../types';
import { Service } from 'typedi';
import { StatusResponse } from '../responses';

@Service()
export class UploadRepository {
  constructor(private http: WebHttp) {}

  /**
   * Gets duration in ms, width and height info for a video in the mp4 container
   * @param buffer Buffer, containing the video-file
   * @returns duration in ms, width and height in px
   */
  public static getVideoInfo(buffer: Buffer): VideoData {
    const width = UploadRepository.read16(buffer, ['moov', 'trak', 'stbl', 'avc1'], 24);
    const height = UploadRepository.read16(buffer, ['moov', 'trak', 'stbl', 'avc1'], 26);
    return {
      durationMs: UploadRepository.getMP4Duration(buffer),
      width,
      height,
    };
  }

  /**
   * Reads the duration in ms from any MP4 file with at least one stream (a/v)
   * @param buffer
   */
  public static getMP4Duration(buffer: Buffer): number {
    const timescale = UploadRepository.read32(buffer, ['moov', 'mvhd'], 12);
    const length = UploadRepository.read32(buffer, ['moov', 'mvhd'], 12 + 4);
    return Math.floor((length / timescale) * 1000);
  }

  private static formatName(uploadId?: string, mode?: UploadMode): string {
    switch (mode) {
      case UploadMode.Direct:
        return `direct_${uploadId}`;
      case UploadMode.FbUploader:
        return `fb_uploader_${uploadId}`;
      case UploadMode.Feed:
        return `feed_${uploadId}`;
      case UploadMode.Story:
        return `story_${uploadId}`;
    }
    return uploadId ?? '';
  }

  /**
   * Reads a 32bit unsigned integer from a given Buffer by walking along the keys and getting the value with the given offset
   * ref: https://gist.github.com/OllieJones/5ffb011fa3a11964154975582360391c#file-streampeek-js-L9
   * @param buffer  The buffer to read from
   * @param keys  Keys the 'walker' should pass (stopping at the last key)
   * @param offset  Offset from the ast key to read the uint32
   */
  private static read32(buffer: Buffer, keys: string[], offset: number) {
    let start = 0;
    for (const key of keys) {
      start = buffer.indexOf(Buffer.from(key), start) + key.length;
    }
    return buffer.readUInt32BE(start + offset);
  }

  /**
   * Reads a 16bit unsigned integer from a given Buffer by walking along the keys and getting the value with the given offset
   * ref: https://gist.github.com/OllieJones/5ffb011fa3a11964154975582360391c#file-streampeek-js-L25
   * @param buffer  The buffer to read from
   * @param keys  Keys the 'walker' should pass (stopping at the last key)
   * @param offset  Offset from the ast key to read the uint16
   */
  private static read16(buffer: Buffer, keys: string[], offset: number) {
    let start = 0;
    for (const key of keys) {
      start = buffer.indexOf(Buffer.from(key), start) + key.length;
    }
    return buffer.readUInt16BE(start + offset);
  }

  public async photo(options: UploadPhotoOptions): Promise<StatusResponse & { upload_id: string }> {
    options = {
      uploadId: Date.now().toString(),
      mode: UploadMode.Regular,
      mediaType: MediaType.Image,
      ...options,
    };
    const name = UploadRepository.formatName(options.uploadId, options.mode);
    const { width, height, type } = options.mediaInfo ?? imageSize(options.file);
    return this.http.body({
      url: `/rupload_igphoto/${name}`,
      method: 'POST',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify({
          media_type: options.mediaType,
          upload_id: options.uploadId,
          upload_media_height: height,
          upload_media_width: width,
        }),
        'X-Entity-Name': name,
        'X-Entity-Length': options.file.byteLength.toString(),
        Offset: '0',
        'Content-Type': `image/${type === 'jpg' ? 'jpeg' : type}`,
        'Content-Length': options.file.byteLength.toString(),
      },
      body: options.file,
    });
  }

  public async video(options: UploadVideoOptions) {
    options = {
      uploadId: Date.now().toString(),
      videoTransform: null,
      ...options,
    };
    const name = UploadRepository.formatName(options.uploadId, options.mode);
    const videoInfo = options.videoData ?? UploadRepository.getVideoInfo(options.file);
    return this.http.body({
      url: `/rupload_igvideo/${name}`,
      method: 'POST',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify({
          is_igtv_video: options.isIgtvVideo ?? false,
          media_type: MediaType.Video,
          for_album: options.publishMode === MediaPublishMode.Reel,
          video_format: 'mp4',
          upload_id: options.uploadId,
          upload_media_duration_ms: videoInfo.durationMs,
          upload_media_height: videoInfo.height,
          upload_media_width: videoInfo.width,
          video_transform: options.videoTransform,
        }),
        'X-Entity-Name': name,
        'X-Entity-Length': options.file.byteLength.toString(),
        Offset: '0',
        'Content-Type': 'video/mp4',
        'Content-Length': options.file.byteLength.toString(),
      },
      body: options.file,
    });
  }
}
