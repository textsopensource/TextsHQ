import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import { StatusResponse } from '../responses';

@Service()
export class LikesRepository {
  constructor(private http: WebHttp) {}

  public async like(mediaId: string): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/web/likes/${mediaId}/like/`,
      method: 'POST',
    });
    return body;
  }

  public async unlike(mediaId: string): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/web/likes/${mediaId}/unlike/`,
      method: 'POST',
    });
    return body;
  }
}
