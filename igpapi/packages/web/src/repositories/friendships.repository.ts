import { WebHttp } from '../core/web.http';
import { Service } from 'typedi';
import {
  FriendshipsChangeRootObject,
  FriendshipsFollowAllRootObject,
  FriendshipsShowManyRootObject,
} from '../responses';

@Service()
export class FriendshipsRepository {
  constructor(private http: WebHttp) {}

  private async change(userId: string, action: string): Promise<FriendshipsChangeRootObject> {
    const { body } = await this.http.full<FriendshipsChangeRootObject>({
      url: `/web/friendships/${userId}/${action}/`,
      method: 'POST',
    });
    return body;
  }

  public follow(userId: string) {
    return this.change(userId, 'follow');
  }
  public unfollow(userId: string) {
    return this.change(userId, 'unfollow');
  }
  public block(userId: string) {
    return this.change(userId, 'block');
  }
  public unblock(userId: string) {
    return this.change(userId, 'unblock');
  }
  public approve(userId: string) {
    return this.change(userId, 'approve');
  }
  public ignore(userId: string) {
    return this.change(userId, 'ignore');
  }

  public async followAll(userIds: string[]): Promise<FriendshipsFollowAllRootObject> {
    const { body } = await this.http.full<FriendshipsFollowAllRootObject>({
      url: '/web/friendships/follow_all/',
      method: 'POST',
      form: {
        user_ids: userIds,
      },
    });
    return body;
  }
  public async showMany(userIds: string[]): Promise<FriendshipsShowManyRootObject> {
    const { body } = await this.http.full<FriendshipsShowManyRootObject>({
      url: '/web/friendships/show_many/',
      method: 'POST',
      form: {
        user_ids: userIds.join(','),
      },
    });
    return body;
  }
}
