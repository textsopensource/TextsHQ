import crypto from 'crypto';
import { Service } from 'typedi';
import { WebHttp } from '../core/web.http';
import { WebState } from '../core/web.state';
// tweetnacl-sealedbox-js has no types
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { seal } = require('tweetnacl-sealedbox-js');

@Service()
export class AccountRepository {
  constructor(private http: WebHttp, private state: WebState) {}

  public preLogin(): Promise<any> {
    return this.http.body({
      url: `/accounts/login/?__a=1`,
    });
  }

  public async login(username: string, password: string): Promise<any> {
    const { encrypted, time } = this.encryptPassword(password);
    return this.http.body({
      url: `/accounts/login/ajax/`,
      method: 'POST',
      form: {
        username,
        enc_password: `#PWD_INSTAGRAM_BROWSER:10:${time}:${encrypted}`,
        query_params: '{}',
        optIntoOneTap: false,
      },
    });
  }

  private encryptPassword(password: string) {
    // TODO: assert()
    if (!this.state.session.sharedData) throw new Error('SharedData is undefined');
    const time = Math.round(Date.now() / 1000).toString();
    const key = crypto.pseudoRandomBytes(32);
    const iv = Buffer.alloc(12, 0);
    const cipher = crypto.createCipheriv('aes-256-gcm', key, iv, { authTagLength: 16 }).setAAD(Buffer.from(time));
    const aesEncrypted = Buffer.concat([cipher.update(Buffer.from(password)), cipher.final()]);
    const authTag = cipher.getAuthTag();
    const encryptedKey = seal(key, Buffer.from(this.state.session.sharedData.encryption.public_key, 'hex'));
    return {
      encrypted: Buffer.concat([
        Buffer.from([
          1,
          Number(this.state.session.sharedData.encryption.key_id),
          encryptedKey.byteLength & 255,
          (encryptedKey.byteLength >> 8) & 255,
        ]),
        encryptedKey,
        authTag,
        aesEncrypted,
      ]).toString('base64'),
      time,
    };
  }
}
