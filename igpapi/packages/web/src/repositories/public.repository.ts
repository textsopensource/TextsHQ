import { RepositoryPublicMediaRootObject, RepositoryPublicUserRootObject } from '../responses';
import { Service } from 'typedi';
import { WebHttp } from '../core/web.http';

@Service()
export class PublicRepository {
  constructor(private http: WebHttp) {}

  async user(username: string) {
    const { body } = await this.http.full<RepositoryPublicUserRootObject>({ url: `/${username}/?__a=1` });
    return body.graphql.user;
  }

  async media(shortcode: string) {
    const { body } = await this.http.full<RepositoryPublicMediaRootObject>({ url: `/p/${shortcode}/?__a=1` });
    return body.graphql.shortcode_media;
  }
}
