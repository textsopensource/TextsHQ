import { Chance } from 'chance';
import { USERAGENT_PLATFORM, USERAGENT_SYSTEMS } from './web.constants';
import debug from 'debug';

export class WebDevice {
  private static debug = debug('ig:state:device');
  userAgent = '';
  isMobile?: boolean;

  generate(seed?: string) {
    const chance = new Chance(seed ?? Math.random().toString());
    const userAgentData = {
      platform: chance.pickone(Object.entries(USERAGENT_PLATFORM)),
      system: chance.pickone(Object.entries(USERAGENT_SYSTEMS)),
    };
    WebDevice.debug(`Generated User-Agent: ${JSON.stringify(userAgentData)}`);
    this.userAgent = `Mozilla/5.0 (${userAgentData.system[1]}) ${userAgentData.platform[1]}`;
  }
}
