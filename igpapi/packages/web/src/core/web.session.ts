import { DataSharedDataRootObject } from '../responses';

export class WebSession {
  wwwClaim?: string;
  sharedData?: DataSharedDataRootObject;
}
