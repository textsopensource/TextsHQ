import { Igpapi, IgpapiHttp } from '@textshq/core';
import { WebState } from './web.state';
import { WebHttp } from './web.http';
import { WebFeedFactory } from './web.feed.factory';
import { PublicRepository } from '../repositories/public.repository';
import { DataRepository } from '../repositories/data.repository';
import { AccountRepository } from '../repositories/account.repository';
import { GraphqlRepository } from '../repositories/graphql.repository';
import { SimulateService } from '../services/simulate.service';
import { CommentsRepository } from '../repositories/comments.repository';
import { LikesRepository } from '../repositories/likes.repository';
import { FriendshipsRepository } from '../repositories/friendships.repository';
import { TagRepository } from '../repositories/tag.repository';
import { StoriesRepository } from '../repositories/stories.repository';
import { ContainerInstance } from 'typedi';
import { DirectRepository } from '../repositories/direct.repository';
import { UploadRepository } from '../repositories/upload.repository';
import { CreateRepository } from '../repositories/create.repository';
import { LocationRepository } from '../repositories/location.repository';
import { UsersRepository } from '../repositories/users.repository';
import { ChallengeRepository } from '../repositories/challenge.repository';
import { WebEntityFactory } from './web.entity.factory';
import { LoginCommand } from '../components/account/login';

export class WebIgpapi extends Igpapi {
  public container: ContainerInstance = new ContainerInstance('igpapi');

  constructor() {
    super();
    this.container.set(ContainerInstance, this.container);
    this.container.set(WebState, new WebState());
    this.container.set(IgpapiHttp, this.container.get(WebHttp));
    this.container.set(WebHttp, this.container.get(WebHttp));
  }

  get state() {
    return this.resolve(WebState);
  }

  get feed() {
    return this.resolve(WebFeedFactory);
  }

  get entity() {
    return this.resolve(WebEntityFactory);
  }

  get http() {
    return this.resolve(WebHttp);
  }

  get public() {
    return this.resolve(PublicRepository);
  }

  /* REPOSITORIES */

  get data() {
    return this.resolve(DataRepository);
  }

  get account() {
    return this.resolve(AccountRepository);
  }

  get graphql() {
    return this.resolve(GraphqlRepository);
  }

  get comments() {
    return this.resolve(CommentsRepository);
  }

  get likes() {
    return this.resolve(LikesRepository);
  }

  get friendships() {
    return this.resolve(FriendshipsRepository);
  }

  get tag() {
    return this.resolve(TagRepository);
  }

  get stories() {
    return this.resolve(StoriesRepository);
  }

  get direct() {
    return this.resolve(DirectRepository);
  }

  get upload() {
    return this.resolve(UploadRepository);
  }

  get create() {
    return this.resolve(CreateRepository);
  }

  get location() {
    return this.resolve(LocationRepository);
  }

  get users() {
    return this.resolve(UsersRepository);
  }

  get challenge() {
    return this.resolve(ChallengeRepository);
  }

  get simulate() {
    return this.resolve(SimulateService);
  }

  /* SERVICES */

  /**
   * Primarily for testing purposes, but feel free to use for whatever you want
   */
  static async create() {
    const igpapi = new WebIgpapi();
    await igpapi.execute(LoginCommand, {});
    return igpapi;
  }
}
