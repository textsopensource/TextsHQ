import { IgpapiState } from '@textshq/core';
import { WebApplication } from './web.application';
import { Type } from 'class-transformer';
import { WebDevice } from './web.device';
import { WebSession } from './web.session';
import { Service } from 'typedi';

@Service()
export class WebState extends IgpapiState {
  @Type(() => WebApplication)
  application = new WebApplication();

  @Type(() => WebDevice)
  device = new WebDevice();

  @Type(() => WebSession)
  session = new WebSession();

  get csrfToken(): string {
    try {
      return this.cookies.value('csrftoken');
    } catch {
      return this.session.sharedData?.config?.csrf_token ?? '';
    }
  }

  get userId(): string {
    return this.session.sharedData?.config?.viewer?.id ?? this.cookies.userId;
  }

  get appId(): number {
    return this.device.isMobile ? this.application.WEB_FB_APP_ID : this.application.WEB_DESKTOP_FB_APP_ID;
  }
}
