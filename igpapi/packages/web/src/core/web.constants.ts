/**
 * most of the constants are in ConsumerLibCommons.js/*
 */

export enum AppPlatformType {
  UNKNOWN = 'unknown',
  IOS = 'ios',
  ANDROID = 'android',
  BLACKBERRY = 'blackberry',
  WINDOWSPHONE = 'windows_phone',
  WEB = 'web',
  WINDOWSPHONE10 = 'windows_phone_10',
  WINDOWSNT10 = 'windows_nt_10',
  OSMETA_WINDOWS_PHONE = 'osmeta_windows_phone',
  OSMETA_WINDOWS_TABLET = 'osmeta_windows_tablet',
  OSMETA_TIZEN = 'osmeta_tizen',
  OSMETA_DEFAULT = 'osmeta_default',
}

export enum AppPlatformIndex {
  UNKNOWN = -1,
  IOS,
  ANDROID,
}

/** YOUR USERAGENTS GO HERE:
 * They are split into the OS and Platform (browser)
 *
 * Mozilla/<version> (<system-information>) <platform> (<platform-details>) <extensions>
 *                    ([      SYSTEM      ])  [          PLATFORM                   ...]
 *
 *  All useragents are named for debugging purposes.
 *
 *  SYSTEM:
 *    Naming: {OS}_{OS-Version}_{additional information, separated by '_'}
 */
export const USERAGENT_SYSTEMS: { [x: string]: string } = {
  Windows_10_64bit_Firefox: 'Windows NT 10.0; Win64; x64; rv:74.0',
  Windows_10_64bit: 'Windows NT 10.0; Win64; x64',
  FirefoxMobileEmulated_Android_7: 'Linux; Android 7.0; SM-G930V Build/NRD90M',
};

/**
 * PLATFORM:
 *    Naming: {Browser}_{branch: stable, beta, nightly/canary}_{version}
 */
export const USERAGENT_PLATFORM: { [x: string]: string } = {
  Firefox_Nightly_74: 'Gecko/20100101 Firefox/74.0',
  Chrome_Stable_78: 'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36',
  FirefoxMobileEmulated_Nightly_75: 'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36',
};

/**
 * GraphQl query Ids
 * Found in ConsumerLibCommons.js and Consumer.js
 * Useful: query hash regex ([a-f0-9]){32}
 *
 * Progress: Consumer.js 12/16
 *
 * This requires constant updates
 */
export enum QueryId {
  Feed = '6b838488258d7a4820e48d209ef79eb1',
  FeedPageExtras = '5931dcfc7f4fb5b410e38269227fa0e8',
  SuggestedUserCount = '09bb2c060bd093088daac1906a1f1d53',
  SuggestedUsers = 'bd90987150a65578bc0dd5d4e60f113d',
  SUL = 'bd90987150a65578bc0dd5d4e60f113d',
  LoadActivityCounts = '0f318e8cfff9cc9ef09f88479ff571fb',
  SuggestedSearches = '7616ef507122e253e5c7806f7976f05c',
  HashtagFollowList = 'e6306cc3dbe69d6a82ef8b5f8654c50b',
  LocationPageExtras = '13893662e2e7e5b01f81fe1c4eb3fdb5',
  UserFollowers = 'c76146de99bb02f6415203be841dd25a',
  UserFollowing = 'd04b0a864b4b54837c0d870b0e77e076',
  TagFeed = 'bd33792e9f52a56ae8fa0985521d141d',
  DiscoverFeed = 'ecd67af449fb6edab7c69a205413bfa7',

  // not sure about names
  LoadPost = '06f8942777d97c874d3d88066e5e3824',
  LoadPostPageExtras = '6ff3f5c474a240353993056428fb851e',
  LoadPostShareIds = 'ba5c3def9f75f43213da3d428f4c783a',
  LoadTagPageExtras = '12ae035b8131ec43c0a0a2ec7088c777',
  StoryViewers = '42c6ec100f5e57a1fe09be16cd3a7021',
  ReelsRefresh = '5931dcfc7f4fb5b410e38269227fa0e8',
  RequestNextCommentPage = 'f0986789a5c5d17c2400faebf16efd0d',
  TaggedPosts = 'ff260833edf142911047af6024eb634a',
  ProfilePageExtras = 'e74d51c10ecc0fe6250a295b9bb9db74',
  RequestProfilePosts = 'e769aa130647d2354c40ea6a439bfc08',
  SavedPosts = '8c86fed24fa03a8a2eea2a70a80c7b6b',
  CommentRequest = 'bc3296d1ce80a24b1b6e40b1e72903f5',
  RequestLiked = 'd5d763b1e2acf209d62d22d184488e57',

  // name unclear, tag_names?
  FetchReelsMedia = 'ba71ba2fcb5655e7e2f37b05aec0ff98',

  // these are labeled 'query_id' instead of 'query_hash' not sure if they fit here
  CommentLikeLists = '5f0b1f6281e72053cbc07909c8d154ae',
  ChildComments = '1ee91c32fc020d44158a3192eda98247',
}

// just a wrapper to 'enforce' a format
export type QueryHash = QueryId;
