import { HeaderStatusCheckerStrategy, IgpapiHttp, IgResponseError } from '@textshq/core';
import got, { Response } from 'got';
import { Service } from 'typedi';
import debug from 'debug';
import { CheckpointRequiredError } from '../errors';
import { WebState } from './web.state';

@Service()
export class WebHttp extends IgpapiHttp {
  client = got.extend({
    prefixUrl: this.state.application.HOST,
    responseType: 'json',
    decompress: true,
    mutableDefaults: true,
    hooks: {
      beforeRequest: [
        request => {
          if (request.http2) {
            delete request.headers.host;
            delete request.headers.connection;
          }
        },
      ],
    },
  });
  protected debug = debug('ig:web:http');
  protected statusCheckerStrategy = new HeaderStatusCheckerStrategy();

  constructor(protected state: WebState) {
    super();
  }

  protected error(response: Response<any>): Error {
    this.debug(
      `Request ${response.request.options.method} ${response.request.options.url.pathname} failed: ${
        typeof response.body === 'object' ? JSON.stringify(response.body) : response.body
      }`,
    );
    const json = response.body;
    if (typeof json.message === 'string') {
      if (json.message === 'checkpoint_required') {
        const err = new CheckpointRequiredError(response);
        this.state.challengePath = err.url();
        return new CheckpointRequiredError(response);
      }
    }
    return new IgResponseError(response);
  }

  protected headers() {
    return {
      Accept: '*/*',
      'Accept-Encoding': 'gzip, deflate, br',
      'Accept-Language': 'en-GB,en;q=0.5',
      Connection: 'keep-alive',
      // do not track
      DNT: '1',
      Host: 'www.instagram.com',
      Referer: 'https://www.instagram.com/',
      TE: 'Trailers',
      'User-Agent': this.state.device.userAgent,
      'X-CSRFToken': this.state.csrfToken,
      'X-IG-App-ID': this.state.appId,
      'X-IG-WWW-Claim': this.state.session.wwwClaim ?? '',
      'X-Instagram-AJAX': this.state.session.sharedData?.rollout_hash,
      'X-Requested-With': 'XMLHttpRequest',
    };
  }

  protected finally(response: Response<any>) {
    const { 'x-ig-set-www-claim': wwwClaim } = response.headers;
    if (typeof wwwClaim === 'string') {
      this.state.session.wwwClaim = wwwClaim;
    }
    return response;
  }
}
