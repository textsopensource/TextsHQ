import { MediaCommentsFeed, MediaLikesFeed, TimelineFeed, UserFeed } from '../feeds';
import { Service } from 'typedi';
import { FeedProperties, IgpapiFactory } from '@textshq/core';
import { ChildCommentsFeed } from '../feeds/child-comments.feed';
import { TaggedPostsFeed } from '../feeds/tagged-posts.feed';
import { TagFeed } from '../feeds/tag.feed';
import { DiscoverFeed } from '../feeds/discover.feed';
import { DirectInboxFeed } from '../feeds/direct-inbox.feed';
import { DirectThreadFeed } from '../feeds/direct-thread.feed';
import { FollowersFeed } from '../feeds/followers.feed';
import { FollowingFeed } from '../feeds/following.feed';

@Service()
export class WebFeedFactory {
  constructor(private factory: IgpapiFactory) {}

  mediaComments(input: FeedProperties<MediaCommentsFeed>) {
    return this.factory.instantiate(MediaCommentsFeed, input);
  }

  childComments(input: FeedProperties<ChildCommentsFeed>) {
    return this.factory.instantiate(ChildCommentsFeed, input);
  }

  mediaLikes(input: FeedProperties<MediaLikesFeed>) {
    return this.factory.instantiate(MediaLikesFeed, input);
  }

  user(input: FeedProperties<UserFeed>) {
    return this.factory.instantiate(UserFeed, input);
  }

  timeline(input: FeedProperties<TimelineFeed> = {}) {
    return this.factory.instantiate(TimelineFeed, input);
  }

  taggedPosts(input: FeedProperties<TaggedPostsFeed>) {
    return this.factory.instantiate(TaggedPostsFeed, input);
  }

  userFollowers(input: FeedProperties<FollowersFeed>) {
    return this.factory.instantiate(FollowersFeed, input);
  }

  userFollowing(input: FeedProperties<FollowingFeed>) {
    return this.factory.instantiate(FollowingFeed, input);
  }

  tag(input: FeedProperties<TagFeed>) {
    return this.factory.instantiate(TagFeed, input);
  }

  discover(input: FeedProperties<DiscoverFeed> = {}) {
    return this.factory.instantiate(DiscoverFeed, input);
  }

  inbox(input: FeedProperties<DirectInboxFeed> = {}) {
    return this.factory.instantiate(DirectInboxFeed, input);
  }

  directThread(input: FeedProperties<DirectThreadFeed>) {
    return this.factory.instantiate(DirectThreadFeed, input);
  }
}
