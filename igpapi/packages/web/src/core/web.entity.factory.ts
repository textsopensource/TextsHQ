import { IgpapiFactory, IgpapiFactoryInput } from '@textshq/core';
import { Service } from 'typedi';
import { IgtvWebVideoEntity } from '../entities';

@Service()
export class WebEntityFactory {
  constructor(protected factory: IgpapiFactory) {}

  igtvVideo(input: IgpapiFactoryInput<IgtvWebVideoEntity>) {
    return this.factory.instantiate(IgtvWebVideoEntity, input);
  }
}
