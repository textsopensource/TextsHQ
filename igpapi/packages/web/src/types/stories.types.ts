export interface StoriesReelSeenOptions {
  reelMediaId: string;
  reelMediaOwnerId?: string;
  reelId: string;
  reelMediaTakenAt: string;
  viewSeenAt?: string;
}
