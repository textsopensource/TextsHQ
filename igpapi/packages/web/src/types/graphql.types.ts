export interface LoadPostPageExtrasOptions {
  only_stories?: boolean;
  stories_prefetch?: boolean;
  stories_video_dash_manifest?: boolean;
}

export interface SuggestedUsersOptions {
  fetch_media_count?: number;
  fetch_suggested_count?: number;
  filter_followed_friends?: boolean;
  ignore_cache?: boolean;
  include_reel?: boolean;
  seen_ids?: string[];
}

export interface FetchReelsMediaOptions {
  reel_ids?: string[];
  tag_names?: string[];
  location_ids?: string[];
  highlight_reel_ids?: string[];
  precomposed_overlay?: boolean;
  show_story_viewer_list?: boolean;
  story_viewer_fetch_count?: number;
  story_viewer_cursor?: string;
  stories_video_dash_manifest?: boolean;
}
