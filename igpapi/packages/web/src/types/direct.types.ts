export interface DirectConfigurePhotoOptions {
  mutationToken?: string;
  threadId: string;
  uploadId: string;
}

export interface DirectLinkShareOptions {
  linkUrls?: string;
  linkText: string;
  threadId: string;
  mutationToken?: string;
}

export interface DirectReelShareOptions {
  reelId: string;
  mediaId: string;
  threadId: string;
  text?: string;
}

export interface DirectStoryShareOptions {
  reelId: string;
  storyMediaId: string;
  threadId: string;
}

export interface DirectReelReactOptions {
  reelId: string;
  mediaId: string;
  threadId: string;
  reactionEmoji: string;
}
