export interface UploadPhotoOptions {
  file: Buffer;
  uploadId?: string;
  mode?: UploadMode;
  mediaType?: MediaType;
  mediaInfo?: { width: number; height: number; type?: string };
}

export interface UploadVideoOptions {
  file: Buffer;
  mode?: UploadMode;
  uploadId?: string;
  isIgtvVideo?: boolean;
  publishMode?: MediaPublishMode;
  videoTransform?: null | VideoTransform;
  videoData?: VideoData;
}

export interface VideoData {
  durationMs: number;
  height: number;
  width: number;
}

export enum VideoTransform {
  CenterCrop = 'center_crop',
}

export enum MediaPublishMode {
  Feed = 'default',
  Reel = 'reel',
  Album = 'album',
  ProfilePic = 'profile_pic',
  LiveReaction = 'live_reaction',
  Draft = 'draft',
  Profile = 'profile',
  NametagSelfie = 'nametag_selfie',
  Igtv = 'igtv',
  IgtvDraft = 'igtv_draft',
  IgtvWithFeed = 'igtv_with_feed',
}

export enum UploadMode {
  Regular,
  Direct,
  FbUploader,
  Feed,
  Story,
  Reel,
}

export enum MediaType {
  Image = 1,
  Video,
  Album,
  Webview,
  Bundle,
  Map,
  Broadcast,
  CarouselV2,
  Collection = 10,
  Audio,
  AnimatedMedia,
  StaticSticker,
}
