export interface CreateConfigureOptions {
  uploadId: string;
  caption?: string;
  location?: { lat: number; lng: number; external_id: string };
  usertags?: Array<{ userId: string; position?: [number, number] }>;
  customAccessibilityCaption?: string;
  retryTimeout?: number;
}

export interface CreateConfigureToStoryOptions {
  uploadId: string;
  caption: string;
}
