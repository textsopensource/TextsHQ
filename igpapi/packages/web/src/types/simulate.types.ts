import { WebState } from '..';

export interface SimulateBootstrapOptions {
  login?: {
    username: string;
    password?: string;
    // pre login etc.
  };
  device?: Resolvable<string | unknown>;
  state?: Resolvable<string | any>;
  saveState?: Callback<Partial<WebState>, void>;
  onChallenge?: Callback<any, { code: string }>;
  isMobile?: boolean;
}

export type PromiseOr<T> = Promise<T> | PromiseLike<T> | T;
export type Resolvable<T> = PromiseOr<T> | (() => PromiseOr<T>);
export type Callback<TInput, TReturn> = (input: TInput) => PromiseOr<TReturn>;
