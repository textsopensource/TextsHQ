import { WebIgpapi } from '../src';

describe('web account', () => {
  const igpapi = new WebIgpapi();
  it('should bootstrap', async () => {
    await igpapi.simulate.bootstrap({
      login: {
        username: String(process.env.IG_USERNAME),
        password: String(process.env.IG_PASSWORD),
      },
    });
    await igpapi.data.sharedData();
    expect(igpapi.state.session.sharedData?.config.viewer.username).toBeDefined();
  });
});
