import { WebIgpapi } from '../src';
import { readFileSync } from 'fs';

const file = readFileSync(`${__dirname}/samples/big-buck-bunny.mp4`);
const cover = readFileSync(`${__dirname}/samples/nature.jpg`);

jest.setTimeout(30000);

describe('igtv.web.video.entity', () => {
  const igpapi = new WebIgpapi();
  it('should publish', async () => {
    await igpapi.simulate.bootstrap({
      login: {
        username: String(process.env.IG_USERNAME),
        password: String(process.env.IG_PASSWORD),
      },
    });
    await igpapi.entity
      .igtvVideo({
        file,
        cover,
        title: 'test',
        sharePreviewToFeed: false,
      })
      .publish();
  });
});
