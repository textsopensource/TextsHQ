# `mqtt`

This package contains the foundation for both FBNS and Realtime.
It consists of the following parts:

- **MQTT Client** as the communication base
- **MQTToT** to comply with Facebooks adjustments to MQTT
- **Basic Thrift Handling** for reading and writing packets

The MQTT client is based on concepts found in [net-mqtt](https://github.com/binsoul/net-mqtt) by binsoul.

> TODO: add more
