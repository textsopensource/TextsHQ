import { deflate, unzip } from 'zlib';
import { promisify } from 'util';

export async function compressDeflate(data: string | Buffer): Promise<Buffer> {
  return new Promise((resolve, reject) =>
    deflate(data, { level: 9 }, (error, result) => (error ? reject(error) : resolve(result))),
  );
}
export const unzipAsync = promisify<string | Buffer, Buffer>(unzip);

/**
 * A large integer (usually userIds)
 */
export type BigInteger = string | bigint | number;
