export enum RealtimeTopics {
  SubIris = '/ig_sub_iris',
  SubIrisResponse = '/ig_sub_iris_response',
  MessageSync = '/ig_message_sync',
  SendMessage = '/ig_send_message',
  SendMessageResponse = '/ig_send_message_response',
  RealtimeSub = '/ig_realtime_sub',
  Pubsub = '/pubsub',
  ForegroundState = '/t_fs',
  GraphQl = '/graphql',
  RegionHint = '/t_region_hint',
}
