import { IrisSubData, RealtimeCoreConnectOptions, RealtimeCoreInitOptions, RealtimeCoreOptions } from './types';
import { MqttClient } from 'mqtts';
import {
  decompressTransformer,
  directBaseTransformer,
  DirectBaseTransformerResult,
  jsonTransformer,
} from './transfromers';
import { RealtimeTopics } from './topics';
import {
  ActivityIndicatorPayload,
  AppPresenceEventPayload,
  ClientConfigUpdatePayload,
  IrisPayload,
  LiveVideoCommentPayload,
  // MessageSyncPayload,
  PubsubPayload,
  PubsubPayloadData,
  RealtimeDirectPayload,
  RealtimeZeroProvisionPayload,
  SubIrisPayload,
} from './payloads';
import { GraphQlQueryId, GraphqlSubscription, SkywalkerSubscription } from './subscriptions';
import { Subject } from 'rxjs';
import debug from 'debug';
import { matchAndExtract, tryUnzipAsync } from './utilities';
import { ActivityIndicatorRegex, DirectThreadRegex } from './constants';
import { pull, pick } from 'lodash';
import { multiTopicPublish } from './flows';
import { DirectCommands } from './commands';

export abstract class RealtimeCoreClient<T = {}> {
  get mqttClient(): MqttClient {
    return this._mqttClient;
  }
  // Subjects

  //graphql subs
  appPresence$ = new Subject<AppPresenceEventPayload>();
  realtimeDirect$ = new Subject<RealtimeDirectPayload>();
  zeroProvision$ = new Subject<RealtimeZeroProvisionPayload>();
  clientConfigUpdate$ = new Subject<ClientConfigUpdatePayload>();
  liveComments$ = new Subject<LiveVideoCommentPayload>();

  /**
   * All realtime messages
   * @type {Subject<DirectBaseTransformerResult<any>>}
   */
  realtimeSub$ = new Subject<DirectBaseTransformerResult<any>>();

  activityIndicator$ = new Subject<ActivityIndicatorPayload>();
  message$ = new Subject<IrisPayload>();
  regionHint$ = new Subject<[string]>();

  error$ = new Subject<Error>();
  warning$ = new Subject<Error>();

  public directCommands: DirectCommands = new DirectCommands(this);

  private realtimeCoreDebug = debug('ig:realtime:core');

  public graphQlSubs: GraphqlSubscription[] = [];
  public skywalkerSubs: SkywalkerSubscription[] = [];
  public safeDisconnect = false;

  private reconnecting = false;
  protected connectOptions: RealtimeCoreConnectOptions & T;
  private _mqttClient: MqttClient;
  protected coreOptions: RealtimeCoreOptions;
  protected constructor(coreOptions: RealtimeCoreInitOptions) {
    this.coreOptions = {
      compress: async data => data,
      decompress: data => tryUnzipAsync(data),
      ...coreOptions,
    };
  }

  public async connect(options: RealtimeCoreConnectOptions & T) {
    this.realtimeCoreDebug('Connecting...');
    this.connectOptions = options;
    this._mqttClient = await this.createMqttClient();
    this.setupListeners();
    await this.connectToBroker();
    await this.postConnect();
    // do this so the promise only resolves once the client is fully connected, but support multiple connect attempts
    this._mqttClient.$connect.subscribe(() => this.postConnect().catch(e => this.error$.next(e)));
    this.realtimeCoreDebug('Connected.');
  }

  protected abstract createMqttClient(): Promise<MqttClient>;
  protected abstract connectToBroker(): Promise<any>;
  protected async postConnect(): Promise<any> {}

  /**
   * Prepares the data to be sent. This method "converts" the input to a Buffer to be sent as a payload.
   * @param {object | any[] | string | Buffer} payload
   * @returns {Buffer}
   */
  protected async preparePayload(payload: object | any[] | string | Buffer): Promise<Buffer> {
    const buf =
      payload instanceof Buffer
        ? payload
        : typeof payload === 'string'
        ? Buffer.from(payload)
        : Buffer.from(JSON.stringify(payload));
    if (this.coreOptions.compressData) {
      return this.coreOptions.compress(buf);
    }
    return buf;
  }

  protected encodeTopic(topic: RealtimeTopics | string): string {
    return this.coreOptions.topicMap ? this.coreOptions.topicMap[topic] : topic;
  }

  protected decodeTopic(topic: string): RealtimeTopics {
    if (this.coreOptions.topicMap) {
      // @ts-ignore
      return Object.entries(this.coreOptions.topicMap).find(([, value]) => value === topic)[0] ?? topic;
    }
    // @ts-ignore
    return topic;
  }

  public async publishToMqtt(topic: RealtimeTopics | string, payload: object | any[] | string | Buffer) {
    const prepared = await this.preparePayload(payload);
    this.realtimeCoreDebug(`Publish: ${prepared.byteLength} bytes to ${topic}`);
    return this._mqttClient.publish({
      topic: this.encodeTopic(topic),
      payload: prepared,
      qosLevel: 1,
    });
  }

  public async request<T = Buffer>(options: {
    topic: RealtimeTopics | string;
    payload: object | any[] | string | Buffer;
    responseTopic: RealtimeTopics | string;
    transformer?: (data: Buffer) => T;
  }): Promise<T> {
    this.realtimeCoreDebug(`Requesting from ${options.topic}`);
    const res = await tryUnzipAsync(
      await this.mqttClient.startFlow(
        multiTopicPublish(
          this.encodeTopic(options.topic),
          this.encodeTopic(options.responseTopic),
        )(await this.preparePayload(options.payload)),
      ),
    );
    // @ts-ignore -- this yields the correct value
    return options.transformer?.(res) ?? res;
  }

  public async irisSubscribe(payload: IrisSubData): Promise<SubIrisPayload> {
    const res = await this.request<SubIrisPayload>({
      topic: RealtimeTopics.SubIris,
      responseTopic: RealtimeTopics.SubIrisResponse,
      payload: pick(payload, 'seq_id', 'snapshot_at_ms', 'snapshot_app_version'),
      transformer: data => JSON.parse(data.toString()),
    });
    this.realtimeCoreDebug(`Iris: SubResponse ${JSON.stringify(res)}`);
    return res;
  }

  public graphQlSub(subs: GraphqlSubscription | GraphqlSubscription[]) {
    this.graphQlSubs.push(...subs);
    this.realtimeCoreDebug(`GraphQl: Subscribe to ${JSON.stringify(subs)}`);
    return this.publishToMqtt(RealtimeTopics.RealtimeSub, { sub: typeof subs === 'string' ? [subs] : subs });
  }

  public graphQlUnsub(subs: GraphqlSubscription | GraphqlSubscription[]) {
    this.graphQlSubs = pull(this.graphQlSubs, ...subs);
    this.realtimeCoreDebug(`GraphQl: Unsubscribe from ${JSON.stringify(subs)}`);
    return this.publishToMqtt(RealtimeTopics.RealtimeSub, { unsub: typeof subs === 'string' ? [subs] : subs });
  }

  public skywalkerSub(subs: SkywalkerSubscription | SkywalkerSubscription[]) {
    this.skywalkerSubs.push(...subs);
    this.realtimeCoreDebug(`Skywalker: Subscribe to ${JSON.stringify(subs)}`);
    return this.publishToMqtt(RealtimeTopics.Pubsub, { sub: typeof subs === 'string' ? [subs] : subs });
  }

  public skywalkerUnsub(subs: SkywalkerSubscription | SkywalkerSubscription[]) {
    this.skywalkerSubs = pull(this.skywalkerSubs, ...subs);
    this.realtimeCoreDebug(`Skywalker: Unsubscribe from ${JSON.stringify(subs)}`);
    return this.publishToMqtt(RealtimeTopics.Pubsub, { unsub: typeof subs === 'string' ? [subs] : subs });
  }

  protected setupListeners() {
    this.realtimeCoreDebug(`Setting up listeners`);
    this._mqttClient
      .listen({
        topic: this.encodeTopic(RealtimeTopics.MessageSync),
        transformer: decompressTransformer(jsonTransformer<IrisPayload[]>(), this.coreOptions.decompress),
      })
      .subscribe(async data => (await data).forEach(x => this.handleMessageSync(x)));
    this._mqttClient
      .listen({
        topic: this.encodeTopic(RealtimeTopics.Pubsub),
        transformer: decompressTransformer(directBaseTransformer<PubsubPayload>(), this.coreOptions.decompress),
      })
      .subscribe(async _message => {
        const { message } = await _message;
        message.data.forEach(x => this.handlePubsubMessage(x, message));
      });
    this._mqttClient
      .listen({
        topic: this.encodeTopic(RealtimeTopics.RealtimeSub),
        transformer: decompressTransformer(
          directBaseTransformer<AppPresenceEventPayload>(),
          this.coreOptions.decompress,
        ),
      })
      .subscribe(async data => this.readRealtimeSub(await data));
    this._mqttClient.$message.subscribe(m => {
      this.realtimeCoreDebug.extend('message')(
        `topic: ${this.decodeTopic(m.topic)} QoS: ${m.qosLevel} length: ${m.payload.byteLength} bytes`,
      );
    });

    this._mqttClient.$error.subscribe(e => {
      this.realtimeCoreDebug(`MQTTS-Error: ${e.message}\n${e.stack}`);
      this.error$.next(e);
    });
    this._mqttClient.$warning.subscribe(e => {
      this.realtimeCoreDebug(`MQTTS-Warning: ${e.message}\n${e.stack}`);
      this.warning$.next(e);
    });
    this._mqttClient.$disconnect.subscribe(() => {
      this.realtimeCoreDebug('MQTT: Disconnected.' + Date.now().toString());
      if (!this.safeDisconnect) {
        if (this.connectOptions.autoReconnect && !this.reconnecting) {
          this.warning$.next(new Error('MQTT: Disconnected.'));
          this.reconnecting = true;
          this.connect(this.connectOptions)
            .then(() => (this.reconnecting = false))
            .catch(e => {
              this.realtimeCoreDebug(`Reconnecting failed: ${e.message}\n${e.stack}`);
              this.error$.next(e);
            });
        }
      } else {
        this.realtimeCoreDebug('Session ended.');
        this.safeDisconnect = false;
      }
    });
  }

  public disconnect(): Promise<any> {
    this.safeDisconnect = true;
    return this.mqttClient.disconnect();
  }

  protected async mqttSubscribe(...topics: string[]): Promise<void> {
    this.realtimeCoreDebug(`RawMQTT: Subscribe to ${JSON.stringify(topics)}`);
    for (const topic of topics) {
      await this._mqttClient.subscribe({ topic: this.encodeTopic(topic), qosLevel: 1 });
    }
  }

  protected readRealtimeSub(result: DirectBaseTransformerResult<any>) {
    this.realtimeCoreDebug(`Realtime: Reading ${result.topic}`);
    if (isAppPresencePayload(result)) {
      this.appPresence$.next(result.message);
    } else if (isDirectPayload(result)) {
      this.realtimeDirect$.next(result.message);
    } else if (isZeroProvisioningPayload(result)) {
      this.zeroProvision$.next(result.message);
    } else if (isConfigUpdatePayload(result)) {
      this.clientConfigUpdate$.next(result.message);
    } else if (isLiveVideoCommentPayload(result)) {
      this.liveComments$.next(result.message);
    } else {
      this.realtimeCoreDebug(`Realtime: Unknown message with topic ${result.topic}: ${JSON.stringify(result.message)}`);
    }
    this.realtimeSub$.next(result);
  }

  protected handlePubsubMessage(data: PubsubPayloadData, _message: PubsubPayload) {
    const match = matchAndExtract(ActivityIndicatorRegex, data.path);
    if (match) {
      const message: any = {
        ..._message,
        ...data,
        ...match,
      };
      delete message.value;
      this.activityIndicator$.next(message);
    } else if (!data.doublePublish) {
      this.realtimeCoreDebug(`Pubsub: no activity indicator on data. Data: ${JSON.stringify(data)}`);
    } else {
      this.realtimeCoreDebug(`Pubsub: double publish, ${data.path}`);
    }
  }

  protected handleMessageSync(payload: IrisPayload) {
    this.message$.next(payload);
  }
}

export const isAppPresencePayload = isRealtimePayload<AppPresenceEventPayload>(GraphQlQueryId.appPresence);
export const isDirectPayload = isRealtimePayload<RealtimeDirectPayload>('direct');
export const isZeroProvisioningPayload = isRealtimePayload<RealtimeZeroProvisionPayload>(GraphQlQueryId.zeroProvision);
export const isConfigUpdatePayload = isRealtimePayload<ClientConfigUpdatePayload>(GraphQlQueryId.clientConfigUpdate);
export const isLiveVideoCommentPayload = isRealtimePayload<LiveVideoCommentPayload>(
  GraphQlQueryId.liveRealtimeComments,
);

export function isRealtimePayload<T>(topic: GraphQlQueryId | string) {
  return (data: DirectBaseTransformerResult<any>): data is DirectBaseTransformerResult<T> => data.topic === topic;
}
