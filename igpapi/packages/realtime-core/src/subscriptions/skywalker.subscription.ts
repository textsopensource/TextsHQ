export class SkywalkerSubscriptions {
  public static directSub(userId: string | number | bigint): SkywalkerSubscription {
    return `ig/u/v1/${userId}`;
  }
  public static liveSub(userId: string | number | bigint): SkywalkerSubscription {
    return `ig/live_notification_subscribe/${userId}`;
  }
}

export type SkywalkerSubscription = string;
