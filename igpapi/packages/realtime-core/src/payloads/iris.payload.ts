export interface SubIrisPayload {
  succeeded: boolean;
  seq_id: number;
  error_type: null | string | any;
  error_message: null | string | any;
  subscribed_at_ms: number;
  latest_seq_id: number;
}

export interface IrisPayload {
  event: 'patch' | string;
  data: any[];
  message_type: number;
  seq_id: number;
  mutation_token: null | string;
  realtime?: boolean;
  sampled?: boolean;
}
