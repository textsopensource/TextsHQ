export interface PubsubBasePayload {
  publish_metadata?: PubsubPublishMetadata;
  lazy: boolean;
  event: 'patch' | string;
  num_endpoints?: number;
}

export interface PubsubPayload extends PubsubBasePayload {
  data: PubsubPayloadData[];
}

export interface PubsubPayloadData {
  doublePublish: boolean;
  value: string;
  path: string;
  op: 'add' | string;
}

export interface PubsubPublishMetadata {
  publish_time_ms: string;
  topic_publish_id: number;
}
