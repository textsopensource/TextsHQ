import { PubsubBasePayload } from './pubsub.payload';

export interface ActivityIndicatorPayload extends PubsubBasePayload {
  op: string;
  path: string;
  doublePublish: boolean;
  data: ActivityIndicatorData;
  threadId: string;
  activityIndicatorId: string;
}

export interface ActivityIndicatorData {
  timestamp: string;
  sender_id: string;
  ttl: number;
  activity_status: number;
}
