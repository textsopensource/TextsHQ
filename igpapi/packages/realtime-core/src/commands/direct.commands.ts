import Chance from 'chance';
import debug from 'debug';
import { RealtimeTopics } from '../topics';
import { compressDeflate, thriftDescriptors, ThriftPacketDescriptor, thriftWriteFromObject } from '@textshq/mqttot';
import { RealtimeCoreClient } from '../realtime-core.client';
import { DirectItemAckResponse, ThreadItemType, TypingStatus } from '../types';

interface ItemBaseType {
  thread_id: string;
  client_context?: string;
  offline_threading_id?: string;
  is_shh_mode?: string;
}

export interface ForegroundState {
  inForegroundApp?: boolean;
  inForegroundDevice?: boolean;
  keepAliveTimeout?: number;
  subscribeTopics?: string[];
  subscribeGenericTopics?: string[];
  unsubscribeTopics?: string[];
  unsubscribeGenericTopics?: string[];
  requestId?: bigint;
}

export class DirectCommands {
  private directDebug = debug('ig:mqtt:realtime:direct');
  private chance: Chance.Chance;

  public foregroundStateConfig: ThriftPacketDescriptor[] = [
    thriftDescriptors.boolean('inForegroundApp', 1),
    thriftDescriptors.boolean('inForegroundDevice', 2),
    thriftDescriptors.int32('keepAliveTimeout', 3),
    thriftDescriptors.listOfBinary('subscribeTopics', 4),
    thriftDescriptors.listOfBinary('subscribeGenericTopics', 5),
    thriftDescriptors.listOfBinary('unsubscribeTopics', 6),
    thriftDescriptors.listOfBinary('unsubscribeGenericTopics', 7),
    thriftDescriptors.int64('requestId', 8),
  ];

  public constructor(private client: RealtimeCoreClient) {
    this.chance = new Chance();
  }

  public async sendForegroundState(state: ForegroundState) {
    this.directDebug(`Updated foreground state: ${JSON.stringify(state)}`);
    return this.client
      .publishToMqtt(
        RealtimeTopics.ForegroundState,
        await compressDeflate(
          Buffer.concat([Buffer.alloc(1, 0), thriftWriteFromObject(state, this.foregroundStateConfig)]),
        ),
      )
      .then(res => {
        // updating the keepAlive to match the shared value
        if (state.keepAliveTimeout != null) {
          this.client.mqttClient.keepAlive = state.keepAliveTimeout;
        }
        return res;
      });
  }

  private async sendCommand(options: ItemBaseType & any): Promise<DirectItemAckResponse> {
    const clientContext = options.client_context ?? this.chance.guid({ version: 4 });
    const json = JSON.stringify({
      client_context: clientContext,
      offline_threading_id: clientContext,
      // TODO
      // device_id: this.ig.state.cookies.value('ig_did'),
      ...options,
    });
    return this.client.request<DirectItemAckResponse>({
      topic: RealtimeTopics.SendMessage,
      responseTopic: RealtimeTopics.SendMessageResponse,
      payload: json,
      transformer: buffer => JSON.parse(buffer.toString()),
    });
  }

  private async sendItem(options: { item_type: ThreadItemType } & any & ItemBaseType) {
    return this.sendCommand({
      is_shh_mode: '0',
      ...options,
      action: 'send_item',
    });
  }

  public async sendHashtag(options: { text?: string; hashtag: string } & ItemBaseType) {
    return this.sendItem({
      text: '',
      item_id: options.hashtag,
      ...options,
      item_type: ThreadItemType.HASHTAG,
    });
  }

  public async sendLike(options: ItemBaseType) {
    return this.sendItem({
      ...options,
      item_type: ThreadItemType.LIKE,
    });
  }

  public async sendLocation(options: { text?: string; venue_id: string } & ItemBaseType) {
    return this.sendItem({
      text: '',
      item_id: options.venue_id,
      ...options,
      item_type: ThreadItemType.LOCATION,
    });
  }

  public async sendMedia(options: { text?: string; media_id: string } & ItemBaseType) {
    return this.sendItem({
      text: '',
      ...options,
      item_type: ThreadItemType.MEDIA_SHARE,
    });
  }

  public async sendProfile(options: { text?: string; profile_user_id: string } & ItemBaseType) {
    return this.sendItem({
      item_id: options.profile_user_id,
      text: '',
      ...options,
      itemType: 'profile',
    });
  }

  public async sendReaction(
    options: {
      item_id: string;
      reaction_status?: 'created' | 'deleted';
      target_item_type?: ThreadItemType | string;
      emoji?: string; // defaults to ❤
    } & ItemBaseType,
  ) {
    return this.sendItem({
      reaction_status: 'created',
      target_item_type: ThreadItemType.TEXT,
      ...options,
      action: 'send_item',
      item_type: ThreadItemType.REACTION,
      node_type: 'item',
      reaction_type: 'like',
      reaction_action_source: 'double_tap',
    });
  }

  public async sendUserStory(options: { text?: string; media_id: string } & ItemBaseType) {
    return this.sendItem({
      text: '',
      item_id: options.media_id,
      ...options,
      item_type: ThreadItemType.REEL_SHARE,
    });
  }

  public async sendText(options: { text: string } & ItemBaseType) {
    return this.sendItem({
      ...options,
      item_type: ThreadItemType.TEXT,
    });
  }

  public async markAsSeen(options: { item_id: string } & ItemBaseType) {
    return this.sendCommand({
      ...options,
      action: 'mark_seen',
    });
  }

  public markVisualItemSeen(options: { itemId: string } & ItemBaseType) {
    const item = options.itemId;
    delete options.itemId;
    return this.sendCommand({
      ...options,
      item_ids: JSON.stringify([item]),
      action: 'mark_visual_item_seen',
      // hard coded ?!
      target_item_type: 'voice_media',
    });
  }

  public async indicateActivity(options: { activity_status?: TypingStatus } & ItemBaseType) {
    return this.sendCommand({
      activity_status: TypingStatus.Text,
      ...options,
      action: 'indicate_activity',
    });
  }
}
