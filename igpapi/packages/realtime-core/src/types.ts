export interface RealtimeCoreOptionsMandatory {
  compressData: boolean;
}

export interface RealtimeCoreOptionsOptional {
  compress: (data: Buffer) => Promise<Buffer>;
  decompress: (data: Buffer) => Promise<Buffer>;
  topicMap?: { [x: string]: string };
}

export type RealtimeCoreOptions = RealtimeCoreOptionsMandatory & RealtimeCoreOptionsOptional;
export type RealtimeCoreInitOptions = RealtimeCoreOptionsMandatory & Partial<RealtimeCoreOptionsOptional>;

export interface RealtimeCoreConnectOptions {
  graphQlSubs?: string[];
  skywalkerSubs?: string[];
  irisData?: IrisSubData;
  autoReconnect?: boolean;
}

export type BigInteger = string | number | bigint;

export interface IrisSubData {
  seq_id: number;
  snapshot_at_ms: number;
  snapshot_app_version?: 'badge_count_only' | 'message' | string;
}

export interface RegexInfo<T> {
  regex: RegExp;
  matches: Array<keyof T>;
}

export enum ThreadItemType {
  DELETION = 'deletion',
  MEDIA = 'media',
  TEXT = 'text',
  LIKE = 'like',
  HASHTAG = 'hashtag',
  PROFILE = 'profile',
  MEDIA_SHARE = 'media_share',
  LOCATION = 'location',
  ACTION_LOG = 'action_log',
  TITLE = 'title',
  USER_REACTION = 'user_reaction',
  HISTORY_EDIT = 'history_edit',
  REACTION_LOG = 'reaction_log',
  REEL_SHARE = 'reel_share',
  DEPRECATED_CHANNEL = 'deprecated_channel',
  LINK = 'link',
  RAVEN_MEDIA = 'raven_media',
  LIVE_VIDEO_SHARE = 'live_video_share',
  TEST = 'test',
  STORY_SHARE = 'story_share',
  REEL_REACT = 'reel_react',
  LIVE_INVITE_GUEST = 'live_invite_guest',
  LIVE_VIEWER_INVITE = 'live_viewer_invite',
  TYPE_MAX = 'type_max',
  PLACEHOLDER = 'placeholder',
  PRODUCT = 'product',
  PRODUCT_SHARE = 'product_share',
  VIDEO_CALL_EVENT = 'video_call_event',
  POLL_VOTE = 'poll_vote',
  FELIX_SHARE = 'felix_share',
  ANIMATED_MEDIA = 'animated_media',
  CTA_LINK = 'cta_link',
  VOICE_MEDIA = 'voice_media',
  STATIC_STICKER = 'static_sticker',
  AR_EFFECT = 'ar_effect',
  SELFIE_STICKER = 'selfie_sticker',
  REACTION = 'reaction',
}

export interface DirectItemAckResponse {
  action: string;
  status_code: string;
  payload: DirectItemAckResponsePayload;
  status: string;
}
export interface DirectItemAckResponsePayload {
  client_context: string;
  item_id: string;
  timestamp: string;
  thread_id: string;
}

export enum TypingStatus {
  Off,
  Text,
  Visual,
}
