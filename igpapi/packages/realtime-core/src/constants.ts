import { RegexInfo } from './types';

export const ActivityIndicatorRegex: RegexInfo<{
  threadId: string;
  activityIndicatorId: string;
}> = {
  regex: /\/direct_v2\/threads\/([\w_]+)\/activity_indicator_id\/([\w_]+)/,
  matches: ['threadId', 'activityIndicatorId'],
};

export const DirectThreadRegex: RegexInfo<Array<string>> = {
  regex: /\/direct_v2\/threads\/?([\w_]*)*\/?([\w_]*)*\/?([\w_]*)*\/?([\w_]*)*\/?([\w_]*)*\/?([\w_]*)*/,
  matches: [0, 1, 2, 3, 4, 5], // max for /threadId/items/itemId/reactions/likes/userId
};
