import { isPubAck, isPublish, MqttPacket, PacketFlowFunc, PublishAckPacket, PublishRequestPacket } from 'mqtts';
import { RealtimeTopics } from './topics';

export const irisSubscribeFlow = multiTopicPublish(RealtimeTopics.SubIris, RealtimeTopics.SubIrisResponse);

export function multiTopicPublish(
  requestTopic: string,
  responseTopic: string,
): (payload: Buffer) => PacketFlowFunc<Buffer> {
  const id = MqttPacket.generateIdentifier() && MqttPacket.generateIdentifier();
  return (payload: Buffer) => success => ({
    start: () => {
      const packet = new PublishRequestPacket(requestTopic, payload, 1);
      packet.identifier = id;
      return packet;
    },
    accept: (packet: PublishRequestPacket | PublishAckPacket) =>
      (isPublish(packet) && packet.topic === responseTopic) || (isPubAck(packet) && packet.identifier === id),
    next: (packet: PublishRequestPacket | PublishAckPacket) => {
      if (isPubAck(packet)) return null;
      success((packet as PublishRequestPacket).payload);
    },
  });
}
