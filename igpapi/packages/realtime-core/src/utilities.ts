import { deflate, unzip, unzipSync, ZlibOptions } from 'zlib';
import { promisify } from 'util';
import { RegexInfo } from './types';
const deflateAsync = promisify(
  (buffer: string | Buffer, options: ZlibOptions, cb: (error: Error | null, res: Buffer) => void) =>
    deflate(buffer, options, cb),
);
const unzipAsync = promisify((buffer: Buffer, cb: (err: Error | null, res: Buffer) => void) => unzip(buffer, cb));

export function isJson(buffer: Buffer) {
  return String.fromCharCode(buffer[0]).match(/[{[]/);
}

export async function compressDeflate(data: string | Buffer): Promise<Buffer> {
  return deflateAsync(data, { level: 9 });
}

export async function tryUnzipAsync(data: Buffer): Promise<Buffer> {
  try {
    if (data.readInt8(0) !== 0x78) return data;

    return unzipAsync(data);
  } catch (e) {
    return data;
  }
}

export function tryUnzipSync(data: Buffer): Buffer {
  try {
    if (data.readInt8(0) !== 0x78) return data;

    return unzipSync(data);
  } catch (e) {
    return data;
  }
}

export function matchAndExtract<T>(info: RegexInfo<T>, target: string): T | null {
  const match = target.match(info.regex);
  if (!match) return null;
  const result: any = {};
  for (let i = 0; i < Math.min(info.matches.length, match.length - 1); i++) {
    result[info.matches[i]] = match[i + 1];
  }
  return result;
}
