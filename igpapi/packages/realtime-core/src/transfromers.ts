import { MqttMessage } from 'mqtts';
import { thriftDescriptors, thriftReadToObject } from '@textshq/mqttot';

export interface DirectBaseTransformerResult<T> {
  topic: string;
  message: T;
}

export function directBaseTransformer<T>() {
  return (data: Buffer): DirectBaseTransformerResult<T> => {
    const parsed = thriftReadToObject<{ message: string; topic: string }>(data, [
      thriftDescriptors.binary('topic', 1),
      thriftDescriptors.binary('message', 2),
    ]);
    return {
      topic: parsed.topic ?? '',
      message: JSON.parse(parsed.message ?? ''),
    };
  };
}

export function jsonTransformer<T>() {
  return (data: Buffer): T => JSON.parse(data.toString());
}

export function decompressTransformer<T>(
  transformer: (data: Buffer) => T,
  decompress: (data: Buffer) => Promise<Buffer>,
): (data: MqttMessage) => Promise<T> {
  return async (data: MqttMessage) => transformer(await decompress(data.payload));
}
