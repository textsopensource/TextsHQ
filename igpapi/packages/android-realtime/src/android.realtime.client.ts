/* tslint:disable:no-bitwise */
import { AndroidIgpapi } from '@textshq/android';
import { compressDeflate, MQTToTClient, MQTToTConnection, MQTToTConnectionClientInfo } from '@textshq/mqttot';
import debug from 'debug';
import { MqttClient } from 'mqtts';
import { IrisSubData, GraphQlQueryId, RealtimeCoreClient, RealtimeTopics } from '@textshq/realtime-core';

export interface AndroidRealtimeClientInitOptions {
  graphQlSubs?: string[];
  skywalkerSubs?: string[];
  irisData?: IrisSubData;
  connectOverrides?: MQTToTConnectionClientInfo;
  enableTrace?: boolean;
}

export class AndroidRealtimeClient extends RealtimeCoreClient<AndroidRealtimeClientInitOptions> {
  private debug = debug('ig:android:realtime');

  private connection: MQTToTConnection;

  public constructor(private readonly ig: AndroidIgpapi) {
    super({
      compressData: true,
      compress: compressDeflate,
      topicMap: {
        '/pp': '34',
        [RealtimeTopics.GraphQl]: '9',
        [RealtimeTopics.Pubsub]: '88',
        [RealtimeTopics.SendMessage]: '132',
        [RealtimeTopics.SendMessageResponse]: '133',
        [RealtimeTopics.SubIris]: '134',
        [RealtimeTopics.SubIrisResponse]: '135',
        [RealtimeTopics.MessageSync]: '146',
        [RealtimeTopics.RealtimeSub]: '149',
        [RealtimeTopics.RegionHint]: '150',
        [RealtimeTopics.ForegroundState]: '102',
        '/mqtt_health_stats': '/mqtt_health_stats',
      },
    });
  }

  private constructConnection() {
    const userAgent = this.ig.state.userAgent;
    const deviceId = this.ig.state.device.phoneId;
    const password = `sessionid=${this.ig.state.cookies.value('sessionid')}`;
    this.connection = new MQTToTConnection({
      clientIdentifier: deviceId.substring(0, 20),
      clientInfo: {
        userId: BigInt(Number(this.ig.state.extractUserId())),
        userAgent,
        clientCapabilities: 183,
        endpointCapabilities: 0,
        publishFormat: 1,
        noAutomaticForeground: true,
        makeUserAvailableInForeground: false,
        deviceId,
        isInitiallyForeground: true,
        networkType: 1,
        networkSubtype: 0,
        clientMqttSessionId: BigInt(Date.now()) & BigInt(0xffffffff),
        subscribeTopics: [88, 135, 149, 150, 133, 146, 179, 34],
        clientType: 'cookie_auth',
        appId: BigInt(567067343352427),
        regionPreference: this.ig.state.session.regionHint ?? 'LLA',
        deviceSecret: '',
        clientStack: 3,
        ...(this.connectOptions.connectOverrides || {}),
      },
      password,
      appSpecificInfo: {
        app_version: this.ig.state.application.APP_VERSION,
        'X-IG-Capabilities': this.ig.state.application.CAPABILITIES,
        everclear_subscriptions: JSON.stringify({
          async_ads_subscribe: GraphQlQueryId.asyncAdSub,
          inapp_notification_subscribe_default: GraphQlQueryId.inappNotification,
          inapp_notification_subscribe_comment: GraphQlQueryId.inappNotification,
          inapp_notification_subscribe_comment_mention_and_reply: GraphQlQueryId.inappNotification,
          business_import_page_media_delivery_subscribe: GraphQlQueryId.businessDelivery,
          video_call_participant_state_delivery: GraphQlQueryId.videoCallParticipantDelivery,
        }),
        'User-Agent': userAgent,
        'Accept-Language': this.ig.state.device.language.replace('_', '-'),
        platform: 'android',
        ig_mqtt_route: 'django',
        pubsub_msg_type_blacklist: 'direct, typing_type',
        auth_cache_enabled: '0',
      },
    });
  }

  protected async createMqttClient(): Promise<MqttClient> {
    this.constructConnection();
    return new MQTToTClient({
      url: 'mqtts://edge-mqtt.facebook.com:443',
      payloadProvider: () => {
        this.constructConnection();
        return compressDeflate(this.connection.toThrift());
      },
      enableTrace: this.connectOptions.enableTrace,
      autoReconnect: this.connectOptions.autoReconnect ?? true,
      requirePayload: false,
    });
  }

  protected connectToBroker(): Promise<any> {
    return this.mqttClient.connect({
      keepAlive: 60,
      protocolLevel: 3,
      clean: true,
      connectDelay: 60 * 1000,
    });
  }

  protected async postConnect(): Promise<any> {
    this.debug('Connected. Checking initial subs.');
    const { graphQlSubs, skywalkerSubs, irisData } = this.connectOptions;
    graphQlSubs && graphQlSubs.length > 0 && (await this.graphQlSub(graphQlSubs));
    skywalkerSubs && skywalkerSubs.length > 0 && (await this.skywalkerSub(skywalkerSubs));
    irisData && (await this.irisSubscribe(irisData));
  }
}
