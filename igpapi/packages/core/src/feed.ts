import { classToPlain, plainToClassFromExist } from 'class-transformer';
import { NonFunctionKeys } from 'utility-types';

export type FeedProperties<T extends object> = Pick<T, NonFunctionKeys<T>>;

interface FeedPage<Response, Item> {
  raw: Response;
  readonly items: Item[];
}

export abstract class Feed<Response, Item> {
  private _hasMore = true;

  /**
   * @description indicates whether feed reached the end or not.
   */
  hasMore() {
    return this._hasMore;
  }

  public abstract items(raw: Response): Item[];

  public async next(): Promise<{ raw: Response; items: Item[] }> {
    const raw = await this.request();
    this._hasMore = this.handle(raw);
    // required here - no arrow function possible
    // eslint-disable-next-line @typescript-eslint/no-this-alias
    const feed = this;
    return {
      raw,
      get items() {
        return feed.items(raw);
      },
    };
  }

  public serialize() {
    return JSON.stringify(this);
  }

  public deserialize(data: string | FeedProperties<this>): void {
    if (typeof data === 'string') {
      plainToClassFromExist(this, JSON.parse(data));
    } else {
      plainToClassFromExist(this, data);
    }
  }

  public toJSON(): FeedProperties<this> {
    return classToPlain(this, { excludePrefixes: ['_'] }) as any;
  }

  [Symbol.asyncIterator]() {
    return {
      next: async (): Promise<{
        value: FeedPage<Response, Item>;
        done: boolean;
      }> => {
        if (this.hasMore()) {
          const value = await this.next();
          return { value, done: false };
        } else {
          return { done: true, value: (void 0 as unknown) as FeedPage<Response, Item> };
        }
      },
    };
  }

  /**
   * @description This function automatically calls after each successful request.
   * Usually mutates the feed state.
   * @return boolean whether next page exists or not
   */
  protected abstract handle(response: Response): boolean;

  protected abstract async request(...args: any[]): Promise<Response>;
}
