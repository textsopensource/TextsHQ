import { Cookie, CookieJar, MemoryCookieStore } from 'tough-cookie';
import _ from 'lodash';
import { IgCookieNotFoundError } from './errors';

export class Cookies {
  private store = new MemoryCookieStore();
  public jar = new CookieJar(this.store);

  get csrfToken() {
    try {
      return this.value('csrftoken');
    } catch {
      return 'missing';
    }
  }

  get userId() {
    return this.value('ds_user_id');
  }

  get username() {
    return this.value('ds_user');
  }

  deserialize(cookies: string | CookieJar.Serialized) {
    this.jar = CookieJar.deserializeSync(cookies, this.store);
  }

  get(key: string): Cookie | null {
    const cookies = this.jar.getCookiesSync('https://instagram.com');
    return _.find(cookies, { key }) || null;
  }

  value(key: string) {
    const cookie = this.get(key);
    if (cookie === null) {
      throw new IgCookieNotFoundError(key);
    }
    return cookie.value;
  }

  toJSON(): CookieJar.Serialized {
    return this.jar.toJSON();
  }
}
