import { Service } from 'typedi';
import { IgpapiHttp } from '../../igpapi.http';
import { RuploadIgphotoRepositoryResponseRootObject } from './rupload-igphoto.response';

export interface UploadPhotoInput {
  file: Buffer;
  ruploadParams: any;
  entityName: string;
}

@Service()
export class RuploadIgphotoRequest {
  constructor(private http: IgpapiHttp) {}

  execute(input: UploadPhotoInput) {
    const contentLength = input.file.byteLength;
    return this.http.body<RuploadIgphotoRepositoryResponseRootObject>({
      url: `/rupload_igphoto/${input.entityName}`,
      method: 'POST',
      headers: {
        'X-Entity-Type': 'image/jpeg',
        Offset: '0',
        'X-Instagram-Rupload-Params': JSON.stringify(input.ruploadParams),
        'X-Entity-Name': input.entityName,
        'X-Entity-Length': contentLength.toString(),
        'Content-Type': 'application/octet-stream',
        'Content-Length': contentLength.toString(),
        'Accept-Encoding': 'gzip',
      },
      body: input.file,
    });
  }
}
