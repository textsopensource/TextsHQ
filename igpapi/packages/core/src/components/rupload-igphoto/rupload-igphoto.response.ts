export interface RuploadIgphotoRepositoryResponseRootObject {
  upload_id: string;
  xsharing_nonces: RuploadIgphotoRepositoryResponseXsharing_nonces;
  status: string;
}
export interface RuploadIgphotoRepositoryResponseXsharing_nonces {}
