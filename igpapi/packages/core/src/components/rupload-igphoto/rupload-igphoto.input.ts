export interface RuploadIgphotoInput {
  uploadId?: string;
  file: Buffer;
  isSidecar?: boolean;
  waterfallId?: string;
}
