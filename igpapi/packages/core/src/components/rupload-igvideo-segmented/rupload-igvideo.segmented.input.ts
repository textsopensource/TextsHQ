export interface RuploadIgvideoSegmentInitInput {
  entityName: string;
  headers: Record<string, any>;
}

export interface UploadVideoSegmentTransferOptions extends RuploadIgvideoSegmentInitInput {
  // Size of the whole video, not the segment
  entityLength: number;
  ruploadParams: any;
  offset: number;
  segment: Buffer;
}
