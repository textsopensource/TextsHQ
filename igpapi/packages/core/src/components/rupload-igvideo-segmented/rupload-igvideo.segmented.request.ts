import { Service } from 'typedi';
import debug from 'debug';
import { IgpapiHttp } from '../../igpapi.http';
import { RuploadIgvideoSegmentInitInput, UploadVideoSegmentTransferOptions } from './rupload-igvideo.segmented.input';
import { HeaderStatusCheckerStrategy } from '../../status-checker-strategies';
import { IgvideoEntity } from './igvideo.entity';
import { IgpapiRequest } from '../../invokers';

const statusCheckerStrategy = new HeaderStatusCheckerStrategy();

@Service()
export class RuploadIgvideoSegmentedRequest implements IgpapiRequest {
  private static debug = debug('ig:rupload-igvideo-segmented');

  constructor(private http: IgpapiHttp) {}

  async init(options: RuploadIgvideoSegmentInitInput) {
    RuploadIgvideoSegmentedRequest.debug(`Initializing segmented video upload: ${JSON.stringify(options)}`);
    return this.http.body<{ offset: number }>({
      url: `/rupload_igvideo/${options.entityName}`,
      method: 'GET',
      statusCheckerStrategy,
      headers: {
        ...options.headers,
      },
    });
  }

  transfer(options: UploadVideoSegmentTransferOptions) {
    return this.http.body({
      url: `/rupload_igvideo/${options.entityName}`,
      method: 'POST',
      statusCheckerStrategy,
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(options.ruploadParams),
        'X-Entity-Length': options.entityLength.toString(),
        'X-Entity-Name': options.entityName,
        'X-Entity-Type': 'video/mp4',
        'Segment-Start-Offset': options.offset.toString(),
        'Segment-Type': '2',
        Offset: options.offset.toString(),
        'Content-Length': options.segment.byteLength.toString(),
      },
      body: options.segment,
    });
  }

  async execute(input: IgvideoEntity) {
    for (const [i, segment] of input.segments().entries()) {
      const { offset } = await this.init({
        entityName: input.entityName(segment.byteLength, i),
        headers: input.headers(),
      });
      await this.transfer({
        offset,
        ruploadParams: {
          ...input.ruploadParams(),
          video_format: 'video/mp4',
          upload_id: input.uploadId(),
          upload_media_duration_ms: input.dimensions().duration,
          upload_media_height: input.dimensions().height,
          upload_media_width: input.dimensions().width,
          video_transform: null,
        },
        entityName: input.entityName(segment.byteLength, i),
        entityLength: input.file.byteLength,
        headers: input.headers(),
        segment,
      });
    }
  }
}
