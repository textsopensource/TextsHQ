import { Memoize } from 'typescript-memoize';
import { BufferSegmentDivider } from '../../buffer-segment-divider';

export abstract class IgvideoEntity {
  /**
   * Cover image
   */
  readonly cover: Buffer;
  /**
   * Video file
   */
  readonly file: Buffer;
  protected abstract divider: BufferSegmentDivider;

  /**
   * Gets duration in ms, width and height info for a video in the mp4 container
   * @param buffer Buffer, containing the video-file
   * @returns duration in ms, width and height in px
   */
  public static dimensions(buffer: Buffer): { duration: number; width: number; height: number } {
    const width = IgvideoEntity.read16(buffer, ['moov', 'trak', 'stbl', 'avc1'], 24);
    const height = IgvideoEntity.read16(buffer, ['moov', 'trak', 'stbl', 'avc1'], 26);
    return {
      duration: IgvideoEntity.getMP4Duration(buffer),
      width,
      height,
    };
  }

  /**
   * Reads the duration in ms from any MP4 file with at least one stream (a/v)
   * @param buffer
   */
  public static getMP4Duration(buffer: Buffer): number {
    const timescale = IgvideoEntity.read32(buffer, ['moov', 'mvhd'], 12);
    const length = IgvideoEntity.read32(buffer, ['moov', 'mvhd'], 12 + 4);
    return Math.floor((length / timescale) * 1000);
  }

  /**
   * Reads a 16bit unsigned integer from a given Buffer by walking along the keys and getting the value with the given offset
   * ref: https://gist.github.com/OllieJones/5ffb011fa3a11964154975582360391c#file-streampeek-js-L25
   * @param buffer  The buffer to read from
   * @param keys  Keys the 'walker' should pass (stopping at the last key)
   * @param offset  Offset from the ast key to read the uint16
   */
  private static read16(buffer: Buffer, keys: string[], offset: number) {
    let start = 0;
    for (const key of keys) {
      start = buffer.indexOf(Buffer.from(key), start) + key.length;
    }
    return buffer.readUInt16BE(start + offset);
  }

  /**
   * Reads a 32bit unsigned integer from a given Buffer by walking along the keys and getting the value with the given offset
   * ref: https://gist.github.com/OllieJones/5ffb011fa3a11964154975582360391c#file-streampeek-js-L9
   * @param buffer  The buffer to read from
   * @param keys  Keys the 'walker' should pass (stopping at the last key)
   * @param offset  Offset from the ast key to read the uint32
   */
  private static read32(buffer: Buffer, keys: string[], offset: number) {
    let start = 0;
    for (const key of keys) {
      start = buffer.indexOf(Buffer.from(key), start) + key.length;
    }
    return buffer.readUInt32BE(start + offset);
  }

  abstract uploadId(): string;

  abstract entityName(segmentByteLength: number, segmentIndex: number): string;

  abstract ruploadParams(): Record<string, any>;
  abstract upload(): any;

  @Memoize()
  dimensions() {
    const width = IgvideoEntity.read16(this.file, ['moov', 'trak', 'stbl', 'avc1'], 24);
    const height = IgvideoEntity.read16(this.file, ['moov', 'trak', 'stbl', 'avc1'], 26);
    return {
      duration: IgvideoEntity.getMP4Duration(this.file),
      width,
      height,
    };
  }

  headers() {
    return {};
  }

  segments() {
    return this.divider.execute(this.file);
  }
}
