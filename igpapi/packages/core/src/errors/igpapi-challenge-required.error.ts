export interface IgpapiChallengeRequiredError {
  url(): string;
}
