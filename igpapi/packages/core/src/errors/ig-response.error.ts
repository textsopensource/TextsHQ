import { IgClientError } from './ig-client.error';
import { Enumerable } from '../decorators';
import { Response } from 'got';

export class IgResponseError<TBody extends { [x: string]: any } = any> extends IgClientError {
  @Enumerable(false)
  public text: string;
  @Enumerable(false)
  public response: Response<TBody>;

  constructor(response: Response<TBody>) {
    super(
      `${response.request.options.method} ${response.requestUrl} - ${response.statusCode} ${
        response.statusMessage
      }; ${JSON.stringify(response.body.message, null, 2) || ''}`,
    );
    this.response = response;
    if (response.body.message) {
      this.text = response.body.message;
    }
  }
}
