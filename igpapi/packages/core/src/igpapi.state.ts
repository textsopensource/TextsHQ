import { classToPlain, plainToClassFromExist, Transform, TransformationType } from 'class-transformer';
import { Cookies } from './cookies';
import { IgNoCheckpointError } from './errors';

export class IgpapiState {
  challengePath?: string;
  proxyUrl?: string;
  @Transform((value, state, transformationType) => {
    switch (transformationType) {
      case TransformationType.CLASS_TO_PLAIN:
        return (value as Cookies).toJSON();
      case TransformationType.PLAIN_TO_CLASS: {
        const cookies = new Cookies();
        cookies.deserialize(value);
        return cookies;
      }
    }
  })
  cookies: Cookies = new Cookies();

  getChallengePath(): string {
    if (!this.challengePath) {
      throw new IgNoCheckpointError();
    }
    return this.challengePath;
  }

  getProxy() {
    return this.proxyUrl || process.env.IGPAPI_PROXY;
  }

  public toJSON(): this {
    return classToPlain(this) as this;
  }

  public deserialize(state: string | Buffer | any) {
    if (Buffer.isBuffer(state)) {
      state = state.toString('utf8');
    }
    const obj = typeof state === 'string' ? JSON.parse(state) : state;
    if (typeof obj !== 'object') {
      throw new TypeError("AndroidState isn't an object or serialized JSON");
    }
    plainToClassFromExist(this, obj);
  }
}
