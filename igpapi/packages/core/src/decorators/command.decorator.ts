import { Service } from 'typedi';

export function Command() {
  return Service({ transient: true, global: true });
}
