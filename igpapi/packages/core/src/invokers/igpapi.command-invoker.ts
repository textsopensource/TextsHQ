import { plainToClassFromExist } from 'class-transformer';
import { ContainerInstance } from 'typedi';
import { IgpapiFactoryInput, Cls } from '../igpapi.factory';

export interface IgpapiCommand {
  execute(): any;
}

export class IgpapiCommandInvoker {
  constructor(private container: ContainerInstance) {}
  execute<T extends IgpapiCommand>(cls: Cls<T>, input: IgpapiFactoryInput<T>): ReturnType<T['execute']> {
    const classObj = this.container.get(cls);
    plainToClassFromExist(classObj, input, { ignoreDecorators: true });
    return classObj.execute();
  }
}
