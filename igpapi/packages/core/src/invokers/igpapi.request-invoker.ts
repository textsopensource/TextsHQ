import { ContainerInstance } from 'typedi';
import { Cls } from '../igpapi.factory';

export interface IgpapiRequest {
  execute(...args: any[]): any;
}

type ArgumentTypes<F extends Function> = F extends (...args: infer A) => any ? A : never;

export class IgpapiRequestInvoker {
  constructor(private container: ContainerInstance) {}

  execute<T extends IgpapiRequest>(cls: Cls<T>, input: ArgumentTypes<T['execute']>[0]): ReturnType<T['execute']> {
    const classObj = this.container.get(cls);
    return classObj.execute(input);
  }
}
