import { ContainerInstance, ObjectType } from 'typedi';
import { IgpapiCommandInvoker, IgpapiFactory, IgpapiState } from './index';
import { IgpapiRequestInvoker } from './invokers/igpapi.request-invoker';

export abstract class Igpapi {
  public abstract readonly container: ContainerInstance;
  public abstract readonly state: IgpapiState;

  get execute() {
    return this.command.execute;
  }

  get factory() {
    return this.container.get(IgpapiFactory);
  }

  get command() {
    return this.container.get(IgpapiCommandInvoker);
  }

  get request() {
    return this.container.get(IgpapiRequestInvoker);
  }

  public resolve<T>(dependency: ObjectType<T>): T {
    return this.container.get(dependency);
  }
}
