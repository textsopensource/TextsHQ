import { BufferSegmentDivider } from './buffer-segment-divider';

export class SectionSizeBufferSegmentDivider implements BufferSegmentDivider {
  constructor(private sectionSizeBytes: number) {}
  execute(buffer: Buffer): Buffer[] {
    const sections = [];
    for (let i = 0; i < buffer.byteLength; ) {
      const section = buffer.slice(i, Math.min(i + this.sectionSizeBytes, buffer.byteLength));
      sections.push(section);
      i += section.byteLength;
    }
    return sections;
  }
}
