import { BufferSegmentDivider } from './buffer-segment-divider';

export class TotalSectionsBufferSegmentDivider implements BufferSegmentDivider {
  constructor(private totalSections: number) {}
  execute(buffer: Buffer): Buffer[] {
    const sections = [];
    const sectionSize = Math.floor(buffer.byteLength / this.totalSections);
    for (let i = 0; i < this.totalSections - 1; i++) {
      sections.push(buffer.slice(i * sectionSize, Math.min((i + 1) * sectionSize, buffer.byteLength)));
    }
    sections.push(buffer.slice(sectionSize * (this.totalSections - 1)));
    return sections;
  }
}
