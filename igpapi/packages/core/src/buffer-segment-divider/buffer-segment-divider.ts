export interface BufferSegmentDivider {
  execute(buffer: Buffer): Buffer[];
}
