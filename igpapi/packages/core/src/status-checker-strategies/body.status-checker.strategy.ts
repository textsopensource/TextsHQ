import { Response } from 'got';
import { StatusCheckerStrategy } from './status-checker.strategy';

export class BodyStatusCheckerStrategy implements StatusCheckerStrategy {
  execute(response: Response<any>): boolean {
    return response.body.status === 'ok';
  }
}
