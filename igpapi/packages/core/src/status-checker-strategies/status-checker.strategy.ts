import { Response } from 'got';

/**
 * Indicates whether HTTP request was successful or not
 */
export interface StatusCheckerStrategy {
  execute(response: Response): boolean;
}
