import { Response } from 'got';
import { StatusCheckerStrategy } from './status-checker.strategy';

export class HeaderStatusCheckerStrategy implements StatusCheckerStrategy {
  execute(response: Response): boolean {
    return response.statusCode >= 200 && response.statusCode < 300;
  }
}
