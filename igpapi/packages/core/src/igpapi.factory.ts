import { plainToClassFromExist } from 'class-transformer';
import { NonFunctionKeys } from 'utility-types';
import { ContainerInstance } from 'typedi';

export type IgpapiFactoryInput<T extends object> = Pick<T, NonFunctionKeys<T>>;
export type Cls<T> = { new (...args: any[]): T };

export class IgpapiFactory {
  constructor(private container: ContainerInstance) {}
  instantiate<T extends object>(cls: { new (...args: any[]): T }, plain: IgpapiFactoryInput<T>): T {
    const classObj = this.container.get(cls);
    return plainToClassFromExist(classObj, plain, { ignoreDecorators: true });
  }
}
