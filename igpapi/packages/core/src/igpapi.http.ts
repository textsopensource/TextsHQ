import { defaultsDeep, isUndefined, omitBy } from 'lodash';
import { Got, OptionsOfJSONResponseBody, RequiredRetryOptions, Response } from 'got';
import { Subject } from 'rxjs';
import { Debugger } from 'debug';
import { Memoize } from 'typescript-memoize';
import { IgClientError } from './errors';
import { StatusCheckerStrategy } from './status-checker-strategies';
import { IgpapiState } from './igpapi.state';
import { SocksProxyAgent } from 'socks-proxy-agent';
import { HttpsProxyAgent } from 'https-proxy-agent';

const tsDefaultsDeep = <T>(target: Partial<T>, ...defaults: Array<Partial<T>>): T => defaultsDeep(target, ...defaults);

export interface IgpapiHttpRequestParameters extends Omit<OptionsOfJSONResponseBody, 'searchParams'> {
  statusCheckerStrategy?: StatusCheckerStrategy;
  searchParams?: string | Record<string, string | number | boolean | null | undefined>;
}

export abstract class IgpapiHttp {
  abstract client: Got;
  end$ = new Subject();
  error$ = new Subject<IgClientError>();
  protected abstract debug: Debugger;
  protected abstract state: IgpapiState;
  protected abstract statusCheckerStrategy: StatusCheckerStrategy;

  async full<T = unknown>(input: IgpapiHttpRequestParameters): Promise<Response<T>> {
    if (typeof input.searchParams === 'object') {
      input.searchParams = omitBy(input.searchParams, isUndefined);
    }
    if (typeof input.form === 'object') {
      input.form = omitBy(input.form, isUndefined);
    }
    const options = tsDefaultsDeep<IgpapiHttpRequestParameters>(input, {
      headers: this.headers(),
      method: input.form || input.body ? 'POST' : 'GET',
      agent: {
        // @ts-ignore - fucking wrong external typings
        https: this.agent(this.state.proxyUrl || process.env.IGPAPI_PROXY),
      },
      cookieJar: this.state.cookies.jar,
      ignoreInvalidCookies: true,
      https: {
        rejectUnauthorized: false,
      },
      throwHttpErrors: false,
      statusCheckerStrategy: this.statusCheckerStrategy,
    });
    this.debug(`Requesting ${options.method} ${options.url || '[could not find url]'}`);
    // in got, using `prefixUrl`, you can't have a '/' at hte beginning, so we remove it here
    // this should be done throughout the repo at some point, but is now not really worth it
    if (options.url?.toString().startsWith('/')) options.url = options.url.toString().substring(1);
    const response = await this.client<T>(options as OptionsOfJSONResponseBody);
    this.finally(response);
    process.nextTick(() => this.end$.next());
    if (!input.statusCheckerStrategy || input.statusCheckerStrategy.execute(response)) {
      return response;
    }
    const error = this.error(response);
    process.nextTick(() => this.error$.next(error));
    throw error;
  }

  public async body<T = unknown>(options: IgpapiHttpRequestParameters): Promise<T> {
    const { body } = await this.full<T>(options);
    return body;
  }

  @Memoize()
  public agent(proxyUrl?: string) {
    if (!proxyUrl) {
      return void 0;
    }
    if (proxyUrl.startsWith('socks')) {
      return new SocksProxyAgent(proxyUrl);
    }
    if (proxyUrl.startsWith('http')) {
      return new HttpsProxyAgent(proxyUrl);
    }
    throw new Error(`Unknown proxy protocol of "${proxyUrl}"`);
  }

  setRetryOptions(options: Partial<RequiredRetryOptions>) {
    Object.assign(this.client.defaults.options.retry, options);
  }

  protected abstract headers(): Record<string, any>;

  protected abstract error(response: Response): Error;

  protected abstract finally(response: Response): any;
}
