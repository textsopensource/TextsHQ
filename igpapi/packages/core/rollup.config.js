import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import externals from 'rollup-plugin-node-externals';

export default {
  input: 'src/index.ts',
  output: {
    dir: 'dist',
    format: 'cjs',
  },
  plugins: [
    typescript({
      tsconfig: 'tsconfig.build.json',
    }),
    externals({
      deps: true,
      exclude: ['jsonwebtoken'],
    }),
    resolve(),
    commonjs(),
  ],
};
