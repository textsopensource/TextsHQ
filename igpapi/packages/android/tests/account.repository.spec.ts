import { AndroidIgpapi } from '../src';

describe('android account', () => {
  const igpapi = new AndroidIgpapi();
  it('should bootstrap', async () => {
    await igpapi.simulate.bootstrap({
      credentials: {
        username: String(process.env.IG_USERNAME),
        password: String(process.env.IG_PASSWORD),
      },
    });
    const currentUser = await igpapi.account.currentUser();
    console.log(currentUser);
    expect(currentUser).toBeDefined();
  });
});
