import { Service } from 'typedi';
import { MediaEntityOembedResponse } from '../responses';
import got from 'got';

@Service({ transient: true, global: true })
export class MediaEntity {
  static async oembed(url: string): Promise<MediaEntityOembedResponse> {
    return got({
      url: 'https://api.instagram.com/oembed/',
      searchParams: {
        url,
      },
    }).json();
  }
}
