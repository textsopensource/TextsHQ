import { Service } from 'typedi';
import { FriendshipRepository } from '../repositories/friendship.repository';

@Service({ transient: true, global: true })
export class ProfileEntity {
  pk: string | number;

  constructor(private friendship: FriendshipRepository) {}

  public async checkFollow() {
    const friendshipStatus = await this.friendship.show(this.pk);
    if (friendshipStatus.following === true) {
      return friendshipStatus;
    }
    return await this.friendship.create(this.pk);
  }

  public async checkUnfollow() {
    const friendshipStatus = await this.friendship.show(this.pk);
    if (friendshipStatus.following === false) {
      return friendshipStatus;
    }
    return await this.friendship.destroy(this.pk);
  }
}
