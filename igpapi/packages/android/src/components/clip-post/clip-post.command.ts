import {
  IgpapiCommand,
  IgvideoEntity,
  RuploadIgphotoRequest,
  RuploadIgvideoSegmentedRequest,
  SectionSizeBufferSegmentDivider,
} from '@textshq/core';
import Chance from 'chance';
import { Memoize } from 'typescript-memoize';

import { random } from 'lodash';
import { Service } from 'typedi';
import {
  MediaConfigureToClipsOptions,
  MediaConfigureToClipsRequest,
  MediaRepositoryConfigureToClipsResponseRootObject,
} from '../media/configure-to-clips';

const chance = new Chance();

export interface ClipMusicOptions {
  assetId: string;
  clusterId: string;
  start: number;
  sessionId?: string;
}

export enum ClipAudioSource {
  music = 'music_selection',
  remix = 'remix',
  original = 'original_remix',
}

@Service({ transient: true, global: true })
export class ClipPostCommand extends IgvideoEntity implements IgpapiCommand {
  hasOverlay? = false;
  audioMuted? = false;
  music?: ClipMusicOptions;
  remixMediaId?: string;
  audioSource?: ClipAudioSource;
  caption? = '';
  additional?: Partial<MediaConfigureToClipsOptions>;

  protected divider = new SectionSizeBufferSegmentDivider(2e13);

  constructor(
    protected ruploadIgvideo: RuploadIgvideoSegmentedRequest,
    protected ruploadIgphotoRepository: RuploadIgphotoRequest,
    protected mediaConfigureToClipsRepository: MediaConfigureToClipsRequest,
  ) {
    super();
  }

  entityName(segmentByteLength: number, segmentIndex: number): string {
    return `${chance.guid({ version: 4 }).replace('-', '')}-0-${segmentByteLength}`;
  }

  ruploadParams(): Record<string, any> {
    return {
      is_clips_video: '1',
      media_type: 2,
      retry_context: JSON.stringify({ num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 }),
      xsharing_user_ids: JSON.stringify([]),
      content_tags: this.hasOverlay ? 'has-overlay' : '',
      music_burnin_params: this.music
        ? JSON.stringify({
            asset_fbid: this.music.assetId,
            offset_ms: this.music.start,
          })
        : undefined,
    };
  }

  @Memoize()
  uploadId(): string {
    return Date.now().toString();
  }

  audioSourceName(): ClipAudioSource {
    return (
      this.audioSource ||
      (this.music ? ClipAudioSource.music : this.remixMediaId ? ClipAudioSource.remix : ClipAudioSource.original)
    );
  }

  async upload() {
    await this.ruploadIgvideo.execute(this);
    await this.ruploadIgphotoRepository.execute({
      entityName: `${this.uploadId()}_0_${random(1000000000, 9999999999)}`,
      file: this.cover,
      ruploadParams: {
        retry_context: JSON.stringify({ num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 }),
        media_type: '2',
        upload_id: this.uploadId(),
        xsharing_user_ids: JSON.stringify([]),
        image_compression: JSON.stringify({ lib_name: 'moz', lib_version: '3.1.m', quality: '80' }),
      },
    });
  }

  async execute(): Promise<MediaRepositoryConfigureToClipsResponseRootObject> {
    await this.upload();
    return this.mediaConfigureToClipsRepository.execute({
      length: this.dimensions().duration / 1000,
      uploadId: this.uploadId(),
      clips: [
        {
          length: this.dimensions().duration / 1000,
          source_type: '3',
          camera_position: 'back',
        },
      ],
      extra: {
        width: this.dimensions().width,
        height: this.dimensions().height,
      },
      audioMuted: this.audioMuted,
      segments: [
        {
          index: 0,
          face_effect_id: null,
          speed: 100,
          source: '3',
          duration_ms: this.dimensions().duration,
          audio_type: this.audioSourceName(),
          from_draft: '0',
          media_type: 'video',
          original_media_type: 'video',
        },
      ],
      musicParams: this.music
        ? {
            audio_asset_id: this.music.assetId,
            audio_cluster_id: this.music.clusterId,
            audio_asset_start_time_in_ms: this.music.start,
            derived_content_start_time_in_ms: this.music.start,
            overlap_duration_in_ms: 15000,
            browse_session_id: this.music.sessionId ?? chance.guid({ version: 4 }),
            product: 'story_camera_clips_v2',
          }
        : undefined,
      remixedParams: this.remixMediaId ? { original_media_id: this.remixMediaId } : undefined,
      caption: this.caption,
      ...this.additional,
    });
  }
}
