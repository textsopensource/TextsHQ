import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp } from '../../core';
import { FriendshipsShowResponse } from './friendships-show.response';

@Command()
export class FriendshipsShowCommand implements IgpapiCommand {
  id: string;

  constructor(private _http: AndroidHttp) {}

  execute(): Promise<FriendshipsShowResponse> {
    return this._http.body<FriendshipsShowResponse>({
      url: `/api/v1/friendships/show/${this.id}/`,
    });
  }
}
