import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp, AndroidState } from '../../core';
import { FriendshipsShowResponse } from './friendships-show.response';

@Command()
export class FriendshipsShowManyCommand implements IgpapiCommand {
  ids: string[];

  constructor(private _http: AndroidHttp, private _state: AndroidState) {}

  async execute(): Promise<FriendshipsShowResponse[]> {
    const { body } = await this._http.full<{ friendship_statuses: FriendshipsShowResponse[] }>({
      url: `/api/v1/friendships/show_many/`,
      method: 'POST',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        user_ids: this.ids.join(),
        _uuid: this._state.device.uuid,
      },
    });
    return body.friendship_statuses;
  }
}
