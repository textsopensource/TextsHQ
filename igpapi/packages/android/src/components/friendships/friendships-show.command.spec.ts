import { AndroidIgpapi } from '../../core/android.igpapi';
import { AccountLoginCommand } from '../account/account-login.command';
import { FriendshipsShowCommand } from './friendships-show.command';

describe('android friendships show', () => {
  const igpapi = new AndroidIgpapi();
  beforeAll(async () => await igpapi.execute(AccountLoginCommand, {}));
  it('execute', () => {
    return expect(
      igpapi.execute(FriendshipsShowCommand, {
        id: '100',
      }),
    ).resolves.toBeDefined();
  });
});
