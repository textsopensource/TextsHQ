import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp, AndroidState } from '../../core';
import { TagsSearchResponse } from './tags-search.response';

@Command()
export class TagsSearchCommand implements IgpapiCommand {
  q: string;

  constructor(private http: AndroidHttp, private state: AndroidState) {}

  execute(): Promise<TagsSearchResponse> {
    return this.http.body<TagsSearchResponse>({
      url: `/api/v1/tags/search/`,
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        q: this.q,
        count: 30,
      },
    });
  }
}
