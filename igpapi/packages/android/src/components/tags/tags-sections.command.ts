import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp, AndroidState } from '../../core';
import { TagsSectionsResponse } from './tags-sections.response';

export enum TagsSectionsTab {
  top = 'top',
  recent = 'recent',
  places = 'places',
  discover = 'discover',
}

@Command()
export class TagsSectionsCommand implements IgpapiCommand {
  q: string;
  tab: TagsSectionsTab;

  constructor(private http: AndroidHttp, private state: AndroidState) {}

  execute(): Promise<TagsSectionsResponse> {
    return this.http.body<TagsSectionsResponse>({
      url: `/api/v1/tags/${encodeURI(this.q)}/sections/`,
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        tab: this.tab,
        count: 30,
      },
    });
  }
}
