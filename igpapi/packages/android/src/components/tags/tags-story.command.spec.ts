import { AndroidIgpapi } from '../../core/android.igpapi';
import { AccountLoginCommand } from '../account/account-login.command';
import { TagsStoryCommand } from './tags-story.command';

describe('android tags story', () => {
  const igpapi = new AndroidIgpapi();
  beforeAll(async () => await igpapi.execute(AccountLoginCommand, {}));
  it('execute', () => {
    return expect(
      igpapi.execute(TagsStoryCommand, {
        tag: 'instagram',
      }),
    ).resolves.toBeDefined();
  });
});
