import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp } from '../../core';
import { TagsStoryResponse } from './tags-story.response';

@Command()
export class TagsStoryCommand implements IgpapiCommand {
  tag: string;

  constructor(private http: AndroidHttp) {}

  execute(): Promise<TagsStoryResponse> {
    return this.http.body<TagsStoryResponse>({
      url: `/api/v1/tags/${encodeURI(this.tag)}/story/`,
    });
  }
}
