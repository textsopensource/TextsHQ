import { AndroidIgpapi } from '../../core/android.igpapi';
import { AccountLoginCommand } from '../account/account-login.command';
import { TagsSearchCommand } from './tags-search.command';

describe('android tags search', () => {
  const igpapi = new AndroidIgpapi();
  beforeAll(async () => await igpapi.execute(AccountLoginCommand, {}));
  it('execute', () => {
    return expect(
      igpapi.execute(TagsSearchCommand, {
        q: 'instagram',
      }),
    ).resolves.toBeDefined();
  });
});
