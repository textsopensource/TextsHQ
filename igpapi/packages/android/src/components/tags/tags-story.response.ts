export interface TagsStoryResponse {
  story: TagsStoryResponseStory;
  status: string;
}
export interface TagsStoryResponseStory {
  id: string;
  latest_reel_media: number;
  expiring_at: number;
  seen: number;
  can_reply: boolean;
  can_gif_quick_reply: boolean;
  can_reshare: boolean;
  reel_type: string;
  is_sensitive_vertical_ad: boolean;
  owner: TagsStoryResponseOwner;
  items: TagsStoryResponseItemsItem[];
  prefetch_count: number;
  unique_integer_reel_id: string;
  has_pride_media: boolean;
}
export interface TagsStoryResponseOwner {
  type: string;
  pk: number;
  name: string;
  profile_pic_url: string;
  profile_pic_username: string;
}
export interface TagsStoryResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: TagsStoryResponseImageVersions2;
  original_width: number;
  original_height: number;
  user: TagsStoryResponseUser;
  caption_is_edited: boolean;
  caption_position: number;
  is_reel_media: boolean;
  photo_of_you: boolean;
  caption: null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  expiring_at: number;
  imported_taken_at: number;
  is_in_profile_grid: boolean;
  profile_grid_control_enabled: boolean;
  can_reshare: boolean;
  can_reply: boolean;
  is_pride_media: boolean;
  supports_reel_reactions: boolean;
  can_send_custom_emojis: boolean;
  show_one_tap_fb_share_tooltip: boolean;
  is_dash_eligible?: number;
  video_dash_manifest?: string;
  video_codec?: string;
  number_of_qualities?: number;
  video_versions?: TagsStoryResponseVideoVersionsItem[];
  has_audio?: boolean;
  video_duration?: number;
  location?: TagsStoryResponseLocation;
  lat?: number;
  lng?: number;
  story_hashtags?: TagsStoryResponseStoryHashtagsItem[];
  story_feed_media?: TagsStoryResponseStoryFeedMediaItem[];
  story_locations?: TagsStoryResponseStoryLocationsItem[];
  story_music_stickers?: TagsStoryResponseStoryMusicStickersItem[];
  story_music_lyrics_stickers?: StoryPositionnedItem[];
  story_polls?: TagsStoryResponsePollsItem[];
  reel_mentions?: TagsStoryResponseReelMentionsItem[];
  has_shared_to_fb?: number;
}
export interface TagsStoryResponseImageVersions2 {
  candidates: TagsStoryResponseCandidatesItem[];
}
export interface TagsStoryResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  estimated_scans_sizes?: number[];
}
export interface TagsStoryResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  is_unpublished?: boolean;
  is_favorite?: boolean;
}
export interface TagsStoryResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface TagsStoryResponseLocation {
  pk: number;
  name: string;
  address: string;
  city: string;
  short_name: string;
  lng: number;
  lat: number;
  external_source: string;
  facebook_places_id: number;
}
export interface TagsStoryResponseStoryHashtagsItem extends StoryPositionnedItem {
  hashtag: TagsStoryResponseHashtag;
}
export interface TagsStoryResponseHashtag {
  name: string;
  id: string;
}
export interface TagsStoryResponseReelMentionsItem extends StoryPositionnedItem {
  display_type: string;
  user: TagsStoryResponseUser;
}
export interface TagsStoryResponseStoryFeedMediaItem extends StoryPositionnedItem {
  media_id: string;
  product_type: string;
}
export interface TagsStoryResponseStoryLocationsItem extends StoryPositionnedItem {
  location: TagsStoryResponseLocation;
}
export interface TagsStoryResponseStoryMusicStickersItem extends StoryPositionnedItem {
  attribution: string;
  display_type: string;
  music_asset_info: TagsStoryResponseStoryMusicStickersMusicAssetInfo;
}
export interface TagsStoryResponseStoryMusicStickersMusicAssetInfo {
  audio_cluster_id: null;
  id: string;
  title: string;
  subtitle: string;
  display_artist: string;
  cover_artwork_uri: string;
  cover_artwork_thumbnail_uri: string;
  progressive_download_url: string;
  highlight_start_times_in_ms: number[];
  is_explicit: boolean;
  dash_manifest: string;
  has_lyrics: boolean;
  audio_asset_id: string;
  duration_in_ms: number;
  dark_message: null;
  allows_saving: boolean;
  ig_artist: TagsStoryResponseUser;
  placeholder_profile_pic_url: string;
  should_mute_audio: boolean;
  should_mute_audio_reason: string;
  is_bookmarked: boolean;
  overlap_duration_in_ms: number;
  audio_asset_start_time_in_ms: number;
}
export interface TagsStoryResponsePollsItem extends StoryPositionnedItem {
  poll_sticker: TagsStoryResponsePollSticker;
}
export interface TagsStoryResponsePollSticker {
  id: string;
  poll_id: string;
  question: string;
  tallies: TagsStoryResponsePollStickerTally[];
  promotion_tallies: null;
  viewer_can_vote: boolean;
  is_shared_result: boolean;
  finished: boolean;
}
export interface TagsStoryResponsePollStickerTally {
  text: string;
  font_size: number;
  count: number;
}
export interface StoryPositionnedItem {
  x: number | string;
  y: number | string;
  z: number | string;
  width: number | string;
  height: number | string;
  rotation: number | string;
  is_pinned: number;
  is_hidden: number;
  is_sticker: number;
  is_fb_sticker: number;
}
