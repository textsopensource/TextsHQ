import { AndroidIgpapi } from '../../core/android.igpapi';
import { AccountLoginCommand } from '../account/account-login.command';
import { TagsSectionsCommand, TagsSectionsTab } from './tags-sections.command';

describe('android tags sections', () => {
  const igpapi = new AndroidIgpapi();
  beforeAll(async () => await igpapi.execute(AccountLoginCommand, {}));
  it('execute', () => {
    return expect(
      igpapi.execute(TagsSectionsCommand, {
        q: 'instagram',
        tab: TagsSectionsTab.top,
      }),
    ).resolves.toBeDefined();
  });
});
