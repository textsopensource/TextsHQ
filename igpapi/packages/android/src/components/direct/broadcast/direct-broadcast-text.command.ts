import { Command } from '@textshq/core';
import { DirectBroadcastDestination } from './direct-broadcast-destination';
import { DirectBroadcastRequest } from './direct-broadcast.request';
import { DirectBroadcastCommand } from './direct-broadcast.command';

@Command()
export class DirectBroadcastTextCommand implements DirectBroadcastCommand {
  destination: DirectBroadcastDestination;
  ids: string[];
  text: string;
  constructor(private directBroadcastRequest: DirectBroadcastRequest) {}
  execute() {
    return this.directBroadcastRequest.execute({
      item: 'text',
      destination: this.destination,
      ids: this.ids,
      form: {
        text: this.text,
      },
    });
  }
}
