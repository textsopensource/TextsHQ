export interface DirectBroadcastResponsePayload {
  client_context: string;
  item_id: string;
  timestamp: string;
  thread_id: string;
}

export interface DirectBroadcastResponse {
  action: string;
  status_code: string;
  payload: DirectBroadcastResponsePayload;
  status: string;
}
