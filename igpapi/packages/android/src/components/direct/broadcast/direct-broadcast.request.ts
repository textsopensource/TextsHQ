import { IgRequest } from '@textshq/core';
import Chance = require('chance');
import { AndroidHttp, AndroidState } from '../../../core';
import { DirectBroadcastDestination } from './direct-broadcast-destination';
import { DirectBroadcastResponse } from './direct-broadcast.response';

export interface DirectBroadcastInput {
  destination: DirectBroadcastDestination;
  ids: string[];
  item: string;
  searchParams?: Record<string, any>;
  form?: Record<string, any>;
  signed?: boolean;
  isShhMode?: boolean;
}

@IgRequest()
export class DirectBroadcastRequest {
  constructor(private state: AndroidState, private http: AndroidHttp) {}

  execute(input: DirectBroadcastInput) {
    const mutationToken = new Chance().guid();
    const form = {
      action: 'send_item',
      is_shh_mode: Number(!!input.isShhMode),
      send_attribution: 'inbox',
      [input.destination]: JSON.stringify(
        input.destination === DirectBroadcastDestination.Thread ? input.ids : [input.ids],
      ),
      client_context: mutationToken,
      _csrftoken: this.state.cookies.csrfToken,
      device_id: this.state.device.id,
      mutation_token: mutationToken,
      _uuid: this.state.device.uuid,
      ...input.form,
      offline_threading_id: mutationToken,
    };

    return this.http.body<DirectBroadcastResponse>({
      url: `/api/v1/direct_v2/threads/broadcast/${input.item}/`,
      method: 'POST',
      form: input.signed ? this.http.sign(form) : form,
      searchParams: input.searchParams,
    });
  }
}
