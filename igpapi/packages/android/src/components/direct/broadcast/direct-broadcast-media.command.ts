import { Command } from '@textshq/core';
import { DirectBroadcastDestination } from './direct-broadcast-destination';
import { DirectBroadcastCommand } from './direct-broadcast.command';
import { DirectBroadcastRequest } from './direct-broadcast.request';

/**
 * There are apparently only 'photo' and 'video', sidecar/album/shopping = 'video'
 */
export enum BroadcastMediaType {
  Photo = 'photo',
  Video = 'video',
}

@Command()
export class DirectBroadcastMediaCommand implements DirectBroadcastCommand {
  destination: DirectBroadcastDestination;
  ids: string[];

  mediaType?: BroadcastMediaType;
  mediaId: string;
  carouselShareChildMediaId?: string;

  constructor(private directBroadcastRequest: DirectBroadcastRequest) {}

  execute() {
    return this.directBroadcastRequest.execute({
      ids: this.ids,
      destination: this.destination,
      item: 'media_share',
      searchParams: {
        media_type:
          this.mediaType ?? (this.carouselShareChildMediaId ? BroadcastMediaType.Video : BroadcastMediaType.Photo),
      },
      form: {
        media_id: this.mediaId,
        carousel_share_child_media_id: this.carouselShareChildMediaId,
      },
    });
  }
}
