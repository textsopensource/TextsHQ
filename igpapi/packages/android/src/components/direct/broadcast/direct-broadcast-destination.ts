export enum DirectBroadcastDestination {
  Thread = 'thread_ids',
  User = 'recipient_users',
}
