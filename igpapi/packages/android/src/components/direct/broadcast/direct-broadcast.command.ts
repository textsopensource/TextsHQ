import { DirectBroadcastDestination } from './direct-broadcast-destination';
import { IgpapiCommand } from '@textshq/core';

export interface DirectBroadcastCommand extends IgpapiCommand {
  destination: DirectBroadcastDestination;
  ids: string[];
}
