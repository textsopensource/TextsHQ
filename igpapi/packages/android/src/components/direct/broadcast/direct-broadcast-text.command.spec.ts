import { AndroidIgpapi } from '../../../core/android.igpapi';
import { AccountLoginCommand } from '../../account/account-login.command';
import { DirectBroadcastTextCommand } from './direct-broadcast-text.command';
import { DirectBroadcastDestination } from './direct-broadcast-destination';

describe('android direct broadcast text', () => {
  const igpapi = new AndroidIgpapi();
  beforeAll(async () => await igpapi.execute(AccountLoginCommand, {}));
  it('execute', () => {
    return expect(
      igpapi.execute(DirectBroadcastTextCommand, {
        destination: DirectBroadcastDestination.User,
        ids: [igpapi.state.cookies.userId],
        text: 'Hello world',
      }),
    ).resolves.toBeDefined();
  });
});
