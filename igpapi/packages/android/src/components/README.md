# Executable components concept

Every **component** should have its own eponymous directory.
This directory constitutes the **component**.

The **component** _**MAY**_ contain such files as

- _component-name_**.input.ts** - input classes/interfaces for the request (not necessary should be in separate file)
- _component-name_**.response.ts** - response classes/interfaces
- _component-name_**.generator.ts** - Scripts to generate response classes/interfaces (maybe it will be better to implement it right in automated tests file)
- _component-name_**.spec.ts** - automated tests
- ...Anything else

## Request component

**Request component** _**SHOULD**_ have the `component-name.request.ts` file.

This file should export a class that implements

```typescript
interface IgpapiRequest<Response> {
  execute(...args: any[]): Promise<Response>;
}
```

The `execute` method should execute _**EXACTLY**_ one http request and return the response.

## Command component

**Command component** _**SHOULD**_ have the `component-name.command.ts` file.

This file should export a class that implements

```typescript
interface IgpapiCommand<Response> {
  execute(): Promise<Response>;
}
```

**Command** is disposable and instantiated everytime you want to execute it.

**Command** **SHOULD NOT** be executed inside another executable component
because it leads to an implicit dependency.
Component class declares it only needs an invoker, but in fact it depends on other commands.
Generally, it's the only reason why we have separated `command` and `request` components.

## Difference

The only difference between `request` and `command` is that request is singleton (among the container) and the command is transient.

Both of them got its own invokers.

```typescript
const igpapi = new Igpapi();
igpapi.command.execute(SomeCommand);
igpapi.request.execute(SomeRequest);
```

# Services

Services is just shapeless classes with some logic.

It is still possible to instantiate any service

```typescript
const igpapi = new Igpapi();
igpapi.factory.instantiate(SomeService).someMethod();
```
