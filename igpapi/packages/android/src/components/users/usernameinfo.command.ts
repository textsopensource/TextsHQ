import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp } from '../../core';
import { UserinfoResponse } from './userinfo.response';

@Command()
export class UsernameinfoCommand implements IgpapiCommand {
  username: string;

  constructor(private _http: AndroidHttp) {}

  async execute() {
    const body = await this._http.body<UserinfoResponse>({
      url: `/api/v1/users/${this.username}/usernameinfo/`,
    });
    return body.user;
  }
}
