import { AndroidIgpapi } from '../../../core/android.igpapi';
import { AccountLoginCommand } from '../../account/account-login.command';
import { TimelinePhotoPostCommand } from './timeline-photo-post.command';
import got from 'got';

describe('android timeline post photo', () => {
  const igpapi = new AndroidIgpapi();
  let file: Buffer;
  beforeAll(async () => {
    file = await got('https://picsum.photos/200').buffer();
    await igpapi.execute(AccountLoginCommand, {});
  });
  it('execute', () => {
    return expect(
      igpapi.execute(TimelinePhotoPostCommand, {
        file,
        caption: 'Lorem ipsum',
      }),
    ).resolves.toBeDefined();
  });
});
