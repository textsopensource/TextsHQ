import { Command, IgpapiCommand, RuploadIgphotoRequest } from '@textshq/core';
import { random } from 'lodash';
import { imageSize } from 'image-size';
import { MediaConfigureRequest } from '../media-configure.request';

@Command()
/**
 * Uploads a single photo to the timeline-feed
 */
export class TimelinePhotoPostCommand implements IgpapiCommand {
  /**
   * @file must be a JPG or PNG
   */
  file: Buffer;

  caption?: string;

  private uploadId?: string = Date.now().toString();

  constructor(protected ruploadIgphotoRepository: RuploadIgphotoRequest, private configure: MediaConfigureRequest) {}

  async execute() {
    const uploadedPhoto = await this.ruploadIgphotoRepository.execute({
      file: this.file,
      entityName: `${this.uploadId}_0_${random(1000000000, 9999999999)}`,
      ruploadParams: {
        upload_id: this.uploadId,
        media_type: '1',
      },
    });
    const size = imageSize(this.file);

    return this.configure.execute({
      upload_id: uploadedPhoto.upload_id,
      width: size.width,
      height: size.height,
      caption: this.caption,
    });
  }
}
