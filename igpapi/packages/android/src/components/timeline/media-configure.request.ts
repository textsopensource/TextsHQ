import { IgpapiRequest, IgRequest } from '@textshq/core';
import { AndroidHttp } from '../../core';
import { MediaConfigureResponseRootObject } from './media-configure.response';

@IgRequest()
export class MediaConfigureRequest implements IgpapiRequest {
  constructor(private http: AndroidHttp) {}

  execute(input: Record<string, any>) {
    return this.http.body<MediaConfigureResponseRootObject>({
      url: '/api/v1/media/configure/',
      method: 'POST',
      form: this.http.sign(input),
    });
  }
}
