import { Command, IgpapiCommand, IgResponseError } from '@textshq/core';
import Bluebird from 'bluebird';
import { Response } from 'got';
import { AndroidHttp, AndroidState } from '../../core';
import { AccountPasswordEncryptorService } from './account-password-encryptor.service';
import { IgLoginBadPasswordError, IgLoginInvalidUserError, IgLoginTwoFactorRequiredError } from '../../errors';
import { AccountLoginResponse } from './account-login.response';
import { AccountRepositoryLoginErrorResponse } from '../../responses/account.repository.login.error.response';
import { QeRepository } from '../../repositories/qe.repository';

@Command()
export class AccountLoginCommand implements IgpapiCommand {
  username?: string = String(process.env.IG_USERNAME);
  password?: string = String(process.env.IG_PASSWORD);
  constructor(
    private http: AndroidHttp,
    private state: AndroidState,
    private encryptor: AccountPasswordEncryptorService,
    private qe: QeRepository,
  ) {}
  async execute() {
    if (!this.state.device.id) {
      this.state.device.generate(this.username);
    }
    await this.qe.syncLoginExperiments();
    const response = await Bluebird.try(() =>
      this.http.full<AccountLoginResponse>({
        method: 'POST',
        url: '/api/v1/accounts/login/',
        form: this.http.sign({
          username: this.username,
          enc_password: this.encryptor.encrypt(this.password),
          guid: this.state.device.uuid,
          phone_id: this.state.device.phoneId,
          _csrftoken: this.state.cookies.csrfToken,
          device_id: this.state.device.id,
          adid: '' /*this.state.device.adid ? not set on pre-login*/,
          google_tokens: '[]',
          login_attempt_count: 0,
          country_codes: JSON.stringify([{ country_code: '1', source: 'default' }]),
          jazoest: this.jazoest(),
        }),
      }),
    ).catch(IgResponseError, error => {
      if (error.response.body.two_factor_required) {
        throw new IgLoginTwoFactorRequiredError(error.response as Response<AccountRepositoryLoginErrorResponse>);
      }
      switch (error.response.body.error_type) {
        case 'bad_password': {
          throw new IgLoginBadPasswordError(error.response as Response<AccountRepositoryLoginErrorResponse>);
        }
        case 'invalid_user': {
          throw new IgLoginInvalidUserError(error.response as Response<AccountRepositoryLoginErrorResponse>);
        }
        default: {
          throw error;
        }
      }
    });
    return response.body.logged_in_user;
  }

  private jazoest(): string {
    const buf = Buffer.from(this.state.device.phoneId, 'ascii');
    let sum = 0;
    for (let i = 0; i < buf.byteLength; i++) {
      sum += buf.readUInt8(i);
    }
    return `2${sum}`;
  }
}
