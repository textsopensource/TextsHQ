import { IgAppModule } from '../../../types/common.types';

export interface MediaConfigureToClipsOptions {
  toFeed?: boolean;
  filterType?: string;
  length: number;
  uploadId: string;
  clips: Array<{ length: number; source_type: string; camera_position?: string }>;
  extra: {
    width: number;
    height: number;
  };
  overlay_data?: Array<MediaClipOverlay>;
  audioMuted: boolean;
  posterFrameIndex?: number;
  segments: Array<MediaClipSegment>;
  musicParams?: MediaMusicParams;
  remixedParams?: MusicRemixParams;
  caption?: string;
}

export interface MediaClipSegment {
  index: number;
  face_effect_id: number | null;
  speed: number;
  source: string;
  duration_ms: number;
  audio_type: 'music_selection' | 'original_remix' | 'original' | string;
  from_draft: '0' | '1' | string;
  media_type: 'video' | string;
  original_media_type: 'video' | string;
}

export interface MusicRemixParams {
  original_media_id: string | number;
}

export interface MediaMusicParams {
  audio_asset_id: string;
  audio_cluster_id: string;
  audio_asset_start_time_in_ms: number;
  derived_content_start_time_in_ms: number;
  overlap_duration_in_ms: number;
  browse_session_id?: string;
  product: IgAppModule;
}

export interface MediaClipOverlay {
  type: 'text' | 'sticker' | string;
  sticker_id?: string;
  text?: string;
  start: number;
  end: number;
}
