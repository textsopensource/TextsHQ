import { IgpapiRequest } from '@textshq/core';
import { Service } from 'typedi';
import { AndroidHttp, AndroidState } from '../../../core';
import { MediaConfigureToClipsOptions } from './media-configure-to-clips.input';
import { MediaRepositoryConfigureToClipsResponseRootObject } from './media-configure-to-clips.response';

@Service()
export class MediaConfigureToClipsRequest implements IgpapiRequest {
  constructor(private _http: AndroidHttp, private _state: AndroidState) {}

  execute(input: MediaConfigureToClipsOptions) {
    return this._http.body<MediaRepositoryConfigureToClipsResponseRootObject>({
      url: '/api/v1/media/configure_to_clips/',
      searchParams: { video: 1 },
      headers: {
        is_clips_video: '1',
      },
      form: this._http.sign({
        _uuid: this._state.device.uuid,
        _uid: this._state.cookies.userId,
        _csrftoken: this._state.cookies.csrfToken,
        clips_share_preview_to_feed: '1', // TODO
        internal_features: 'clips_launch',
        filter_type: input.filterType ?? '0',
        timezone_offset: this._state.device.timezoneOffset,
        source_type: '3',
        video_result: '',
        device_id: this._state.device.id,
        caption: input.caption ?? '',
        // TODO: date_time_original: '19040101T000000.000Z',
        capture_type: 'clips_v2',
        upload_id: input.uploadId,
        device: this._state.device.payload,
        length: input.length,
        clips: input.clips,
        extra: {
          source_width: input.extra.width,
          source_height: input.extra.height,
        },
        overlay_data: input.overlay_data,
        audio_muted: !!input.audioMuted,
        poster_frame_index: input.posterFrameIndex ?? 0,
        clips_segments_metadata: {
          num_segments: input.segments.length,
          clips_segments: input.segments,
        },
        music_params: input.musicParams,
        remixed_original_sound_params: input.remixedParams,
      }),
    });
  }
}
