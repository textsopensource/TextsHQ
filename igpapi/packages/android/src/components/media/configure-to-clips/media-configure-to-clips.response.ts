export interface MediaRepositoryConfigureToClipsResponseRootObject {
  media: MediaRepositoryConfigureToClipsResponseMedia;
  upload_id: string;
  status: string;
}
export interface MediaRepositoryConfigureToClipsResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: MediaRepositoryConfigureToClipsResponseImage_versions2;
  original_width: number;
  original_height: number;
  video_versions: MediaRepositoryConfigureToClipsResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  user: MediaRepositoryConfigureToClipsResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  likers: any[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  caption: MediaRepositoryConfigureToClipsResponseCaption;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  product_type: string;
  clips_metadata: MediaRepositoryConfigureToClipsResponseClips_metadata;
}
export interface MediaRepositoryConfigureToClipsResponseImage_versions2 {
  candidates: MediaRepositoryConfigureToClipsResponseCandidatesItem[];
  additional_candidates: MediaRepositoryConfigureToClipsResponseAdditional_candidates;
}
export interface MediaRepositoryConfigureToClipsResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaRepositoryConfigureToClipsResponseAdditional_candidates {
  igtv_first_frame: MediaRepositoryConfigureToClipsResponseIgtv_first_frame;
  first_frame: MediaRepositoryConfigureToClipsResponseFirst_frame;
}
export interface MediaRepositoryConfigureToClipsResponseIgtv_first_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaRepositoryConfigureToClipsResponseFirst_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaRepositoryConfigureToClipsResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface MediaRepositoryConfigureToClipsResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  can_boost_post: boolean;
  can_see_organic_insights: boolean;
  show_insights_terms: boolean;
  reel_auto_archive: string;
  is_unpublished: boolean;
  allowed_commenter_type: string;
}
export interface MediaRepositoryConfigureToClipsResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: MediaRepositoryConfigureToClipsResponseUser;
  media_id: string;
}
export interface MediaRepositoryConfigureToClipsResponseClips_metadata {
  music_info: null | MediaRepositoryConfigureToClipsResponseMusic_info;
  original_sound_info: MediaRepositoryConfigureToClipsResponseOriginal_sound_info | null;
  featured_label: null;
}
export interface MediaRepositoryConfigureToClipsResponseOriginal_sound_info {
  audio_asset_id: number;
  progressive_download_url: string;
  dash_manifest: string;
  ig_artist: MediaRepositoryConfigureToClipsResponseIg_artist;
  should_mute_audio: boolean;
  original_media_id: string;
  hide_remixing: boolean;
  duration_in_ms: number;
  time_created: number;
}
export interface MediaRepositoryConfigureToClipsResponseIg_artist {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface MediaRepositoryConfigureToClipsResponseMusic_info {
  music_asset_info: MediaRepositoryConfigureToClipsResponseMusic_asset_info;
  music_consumption_info: MediaRepositoryConfigureToClipsResponseMusic_consumption_info;
}
export interface MediaRepositoryConfigureToClipsResponseMusic_asset_info {
  audio_cluster_id: string;
  id: string;
  title: string;
  subtitle: string;
  display_artist: string;
  cover_artwork_uri: string;
  cover_artwork_thumbnail_uri: string;
  progressive_download_url: string;
  highlight_start_times_in_ms: number[];
  is_explicit: boolean;
  dash_manifest: string;
  has_lyrics: boolean;
  audio_asset_id: string;
  duration_in_ms: number;
  dark_message: null;
  allows_saving: boolean;
}
export interface MediaRepositoryConfigureToClipsResponseMusic_consumption_info {
  ig_artist: null;
  placeholder_profile_pic_url: string;
  should_mute_audio: boolean;
  should_mute_audio_reason: string;
  is_bookmarked: boolean;
  overlap_duration_in_ms: number;
  audio_asset_start_time_in_ms: number;
}
