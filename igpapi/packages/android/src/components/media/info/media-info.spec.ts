import { AndroidIgpapi } from '../../..';
import { MediaInfoCommand } from './media-info.command';
import { JsonTs } from '@textshq/tools/json-ts';

test('media-info.command', async () => {
  const igpapi = await AndroidIgpapi.create();
  const picture = await igpapi.execute(MediaInfoCommand, { id: '2322681149551708126' });
  await JsonTs.generate(picture, __dirname, 'media-info.picture');
  const video = await igpapi.execute(MediaInfoCommand, { id: '2302971092417260011' });
  await JsonTs.generate(video, __dirname, 'media-info.video');
  const igtv = await igpapi.execute(MediaInfoCommand, { id: '2339956521855070904' });
  await JsonTs.generate(igtv, __dirname, 'media-info.igtv');
  const album = await igpapi.execute(MediaInfoCommand, { id: '2353013041721943035' });
  await JsonTs.generate(album, __dirname, 'media-info.album');
});
