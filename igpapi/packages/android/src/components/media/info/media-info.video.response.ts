export interface MediaInfoVideoResponse {
  items: MediaInfoVideoResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  auto_load_more_enabled: boolean;
  status: string;
}
export interface MediaInfoVideoResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  user: MediaInfoVideoResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  image_versions2: MediaInfoVideoResponseImage_versions2;
  original_width: number;
  original_height: number;
  like_count: number;
  has_liked: boolean;
  top_likers: any[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  is_dash_eligible: number;
  video_dash_manifest: string;
  video_codec: string;
  number_of_qualities: number;
  video_versions: MediaInfoVideoResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  view_count: number;
  caption: MediaInfoVideoResponseCaption;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  sharing_friction_info: MediaInfoVideoResponseSharing_friction_info;
  is_in_profile_grid: boolean;
  profile_grid_control_enabled: boolean;
  is_shop_the_look_eligible: boolean;
  deleted_reason: number;
}
export interface MediaInfoVideoResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  friendship_status: MediaInfoVideoResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_unpublished: boolean;
  is_favorite: boolean;
  latest_reel_media: number;
  account_badges: any[];
}
export interface MediaInfoVideoResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface MediaInfoVideoResponseImage_versions2 {
  candidates: MediaInfoVideoResponseCandidatesItem[];
}
export interface MediaInfoVideoResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoVideoResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface MediaInfoVideoResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: MediaInfoVideoResponseUser;
  is_covered: boolean;
  media_id: string;
  has_translation: boolean;
  private_reply_status: number;
}
export interface MediaInfoVideoResponseSharing_friction_info {
  should_have_sharing_friction: boolean;
  bloks_app_url: null;
}
