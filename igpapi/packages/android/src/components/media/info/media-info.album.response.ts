export interface MediaInfoAlbumResponse {
  items: MediaInfoAlbumResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  auto_load_more_enabled: boolean;
  status: string;
}
export interface MediaInfoAlbumResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  carousel_media_count: number;
  carousel_media: MediaInfoAlbumResponseCarouselMediaItem[];
  can_see_insights_as_brand: boolean;
  user: MediaInfoAlbumResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  next_max_id: string;
  max_num_visible_preview_comments: number;
  preview_comments: MediaInfoAlbumResponsePreviewCommentsItem[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  like_count: number;
  has_liked: boolean;
  top_likers: any[];
  photo_of_you: boolean;
  usertags: MediaInfoAlbumResponseUsertags;
  caption: MediaInfoAlbumResponseCaption;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  sharing_friction_info: MediaInfoAlbumResponseSharing_friction_info;
  is_in_profile_grid: boolean;
  profile_grid_control_enabled: boolean;
  is_shop_the_look_eligible: boolean;
  deleted_reason: number;
}
export interface MediaInfoAlbumResponseCarouselMediaItem {
  id: string;
  media_type: number;
  image_versions2: MediaInfoAlbumResponseImage_versions2;
  original_width: number;
  original_height: number;
  pk: string;
  carousel_parent_id: string;
  can_see_insights_as_brand: boolean;
  usertags: MediaInfoAlbumResponseUsertags;
  creative_config?: MediaInfoAlbumResponseCreative_config;
  sharing_friction_info: MediaInfoAlbumResponseSharing_friction_info;
  video_versions?: MediaInfoAlbumResponseVideoVersionsItem[];
  video_duration?: number;
  is_dash_eligible?: number;
  video_dash_manifest?: string;
  video_codec?: string;
  number_of_qualities?: number;
}
export interface MediaInfoAlbumResponseImage_versions2 {
  candidates: MediaInfoAlbumResponseCandidatesItem[];
}
export interface MediaInfoAlbumResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoAlbumResponseUsertags {
  "'in'"?: MediaInfoAlbumResponseInItem[];
  in?: MediaInfoAlbumResponseInItem[];
}
export interface MediaInfoAlbumResponseCreative_config {
  camera_facing: string;
  face_effect_id: number;
  capture_type: string;
  should_render_try_it_on: boolean;
  failure_reason: string;
  effect_preview: MediaInfoAlbumResponseEffect_preview;
}
export interface MediaInfoAlbumResponseEffect_preview {
  name: string;
  id: string;
  gatekeeper: null;
  gatelogic: null;
  attribution_user_id: string;
  attribution_user: MediaInfoAlbumResponseAttribution_user;
  thumbnail_image: MediaInfoAlbumResponseThumbnail_image;
  effect_actions: string[];
  effect_action_sheet: MediaInfoAlbumResponseEffect_action_sheet;
  save_status: string;
  device_position: null;
}
export interface MediaInfoAlbumResponseAttribution_user {
  instagram_user_id: string;
  username: string;
  profile_picture: MediaInfoAlbumResponseProfile_picture;
}
export interface MediaInfoAlbumResponseProfile_picture {
  uri: string;
}
export interface MediaInfoAlbumResponseThumbnail_image {
  uri: string;
}
export interface MediaInfoAlbumResponseEffect_action_sheet {
  primary_actions: string[];
  secondary_actions: string[];
}
export interface MediaInfoAlbumResponseSharing_friction_info {
  should_have_sharing_friction: boolean;
  bloks_app_url: null;
}
export interface MediaInfoAlbumResponseInItem {
  user: MediaInfoAlbumResponseUser;
  position: (number | string)[] | number[];
  start_time_in_video_in_sec: null;
  duration_in_video_in_sec: null;
}
export interface MediaInfoAlbumResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  friendship_status?: MediaInfoAlbumResponseFriendship_status;
  has_anonymous_profile_picture?: boolean;
  is_unpublished?: boolean;
  is_favorite?: boolean;
  latest_reel_media?: number;
  account_badges?: any[];
}
export interface MediaInfoAlbumResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface MediaInfoAlbumResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface MediaInfoAlbumResponsePreviewCommentsItem {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: MediaInfoAlbumResponseUser;
  is_covered: boolean;
  media_id: string;
  has_liked_comment: boolean;
  comment_like_count: number;
  private_reply_status: number;
}
export interface MediaInfoAlbumResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: MediaInfoAlbumResponseUser;
  is_covered: boolean;
  media_id: string;
  has_translation: boolean;
  private_reply_status: number;
}
