export interface MediaInfoIgtvResponse {
  items: MediaInfoIgtvResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  auto_load_more_enabled: boolean;
  status: string;
}
export interface MediaInfoIgtvResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  user: MediaInfoIgtvResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  title: string;
  product_type: string;
  nearly_complete_copyright_match: boolean;
  media_cropping_info: MediaInfoIgtvResponseMedia_cropping_info;
  thumbnails: MediaInfoIgtvResponseThumbnails;
  is_post_live: boolean;
  image_versions2: MediaInfoIgtvResponseImage_versions2;
  original_width: number;
  original_height: number;
  like_count: number;
  has_liked: boolean;
  top_likers: any[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  is_dash_eligible: number;
  video_dash_manifest: string;
  video_codec: string;
  number_of_qualities: number;
  video_versions: MediaInfoIgtvResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  view_count: number;
  caption: MediaInfoIgtvResponseCaption;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  sharing_friction_info: MediaInfoIgtvResponseSharing_friction_info;
  is_in_profile_grid: boolean;
  profile_grid_control_enabled: boolean;
  is_shop_the_look_eligible: boolean;
  deleted_reason: number;
}
export interface MediaInfoIgtvResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  friendship_status: MediaInfoIgtvResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_unpublished: boolean;
  is_favorite: boolean;
  latest_reel_media: number;
  account_badges: any[];
}
export interface MediaInfoIgtvResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface MediaInfoIgtvResponseMedia_cropping_info {}
export interface MediaInfoIgtvResponseThumbnails {
  video_length: number;
  thumbnail_width: number;
  thumbnail_height: number;
  thumbnail_duration: string;
  sprite_urls: string[];
  thumbnails_per_row: number;
  max_thumbnails_per_sprite: number;
  sprite_width: number;
  sprite_height: number;
  rendered_width: number;
}
export interface MediaInfoIgtvResponseImage_versions2 {
  candidates: MediaInfoIgtvResponseCandidatesItem[];
  additional_candidates: MediaInfoIgtvResponseAdditional_candidates;
}
export interface MediaInfoIgtvResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoIgtvResponseAdditional_candidates {
  igtv_first_frame: MediaInfoIgtvResponseIgtv_first_frame;
  first_frame: MediaInfoIgtvResponseFirst_frame;
}
export interface MediaInfoIgtvResponseIgtv_first_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoIgtvResponseFirst_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoIgtvResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface MediaInfoIgtvResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: MediaInfoIgtvResponseUser;
  is_covered: boolean;
  media_id: string;
  has_translation: boolean;
  private_reply_status: number;
}
export interface MediaInfoIgtvResponseSharing_friction_info {
  should_have_sharing_friction: boolean;
  bloks_app_url: null;
}
