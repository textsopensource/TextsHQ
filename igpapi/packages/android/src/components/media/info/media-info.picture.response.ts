export interface MediaInfoPictureResponse {
  items: MediaInfoPictureResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  auto_load_more_enabled: boolean;
  status: string;
}
export interface MediaInfoPictureResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  user: MediaInfoPictureResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comments_disabled: boolean;
  image_versions2: MediaInfoPictureResponseImage_versions2;
  original_width: number;
  original_height: number;
  like_count: number;
  has_liked: boolean;
  top_likers: any[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  caption: null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  sharing_friction_info: MediaInfoPictureResponseSharing_friction_info;
  is_in_profile_grid: boolean;
  profile_grid_control_enabled: boolean;
  is_shop_the_look_eligible: boolean;
  deleted_reason: number;
}
export interface MediaInfoPictureResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  friendship_status: MediaInfoPictureResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  is_unpublished: boolean;
  is_favorite: boolean;
  latest_reel_media: number;
  account_badges: any[];
}
export interface MediaInfoPictureResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface MediaInfoPictureResponseImage_versions2 {
  candidates: MediaInfoPictureResponseCandidatesItem[];
}
export interface MediaInfoPictureResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface MediaInfoPictureResponseSharing_friction_info {
  should_have_sharing_friction: boolean;
  bloks_app_url: null;
}
