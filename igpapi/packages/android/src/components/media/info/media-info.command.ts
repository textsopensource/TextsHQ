import { Command, IgpapiCommand } from '@textshq/core';
import { AndroidHttp, AndroidState } from '../../../core';
import { MediaInfoAlbumResponse } from './media-info.album.response';
import { MediaInfoIgtvResponse } from './media-info.igtv.response';
import { MediaInfoPictureResponse } from './media-info.picture.response';
import { MediaInfoVideoResponse } from './media-info.video.response';

export type MediaInfoResponse =
  | MediaInfoAlbumResponse
  | MediaInfoIgtvResponse
  | MediaInfoPictureResponse
  | MediaInfoVideoResponse;

@Command()
export class MediaInfoCommand implements IgpapiCommand {
  id!: string;
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  execute() {
    return this.http.body<MediaInfoResponse>({
      url: `/api/v1/media/${this.id}/info/`,
      method: 'GET',
      allowGetBody: true,
      form: this.http.sign({
        igtv_feed_preview: false,
        media_id: this.id,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }
}
