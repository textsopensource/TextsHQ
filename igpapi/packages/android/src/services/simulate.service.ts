import { shuffle } from 'lodash';
import { Service } from 'typedi';
import { AccountRepository } from '../repositories/account.repository';
import { ZrRepository } from '../repositories/zr.repository';
import { LauncherRepository } from '../repositories/launcher.repository';
import { QeRepository } from '../repositories/qe.repository';
import { AttributionRepository } from '../repositories/attribution.repository';
import { LoomRepository } from '../repositories/loom.repository';
import { LinkedAccountRepository } from '../repositories/linked-account.repository';
import { FbsearchRepository } from '../repositories/fbsearch.repository';
import { DirectRepository } from '../repositories/direct.repository';
import { MediaRepository } from '../repositories/media.repository';
import { QpRepository } from '../repositories/qp.repository';
import { UserRepository } from '../repositories/user.repository';
import { DiscoverRepository } from '../repositories/discover.repository';
import { StatusRepository } from '../repositories/status.repository';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { FeedFactory } from '../core/feed.factory';
import { Resolvable, SimulateBootstrapOptions } from '../types/';
import debug from 'debug';
import { ChallengeRepository } from '../repositories/challenge.repository';
import { AccountRepositoryCurrentUserResponseUser, AccountRepositoryLoginResponseLogged_in_user } from '../responses';
import { IgCheckpointError, IgLoginTwoFactorRequiredError, IgNotResolvableError } from '../errors';
import { TimelineFeedReason } from '../feeds/timeline.feed';
import Bluebird = require('bluebird');

@Service()
export class SimulateService {
  private static simulateDebug = debug('ig:simulate');
  constructor(
    private http: AndroidHttp,
    private state: AndroidState,
    private account: AccountRepository,
    private zr: ZrRepository,
    private launcher: LauncherRepository,
    private qe: QeRepository,
    private attribution: AttributionRepository,
    private loom: LoomRepository,
    private linkedAccount: LinkedAccountRepository,
    private fbsearch: FbsearchRepository,
    private direct: DirectRepository,
    private media: MediaRepository,
    private qp: QpRepository,
    private user: UserRepository,
    private discover: DiscoverRepository,
    private status: StatusRepository,
    private feed: FeedFactory,
    private challenge: ChallengeRepository,
  ) {}
  private get preLoginFlowRequests(): (() => any)[] {
    return [
      () => this.account.readMsisdnHeader(),
      () => this.account.msisdnHeaderBootstrap('ig_select_app'),
      () => this.zr.tokenResult(),
      () => this.account.contactPointPrefill('prefill'),
      () => this.launcher.preLoginSync(),
      () => this.qe.syncLoginExperiments(),
      () => this.attribution.logAttribution(),
      () => this.account.getPrefillCandidates(),
    ];
  }

  private get postLoginFlowRequests(): (() => any)[] {
    return [
      () => this.zr.tokenResult(),
      () => this.launcher.postLoginSync(),
      () => this.qe.syncExperiments(),
      () => this.attribution.logAttribution(),
      () => this.attribution.logResurrectAttribution(),
      () => this.loom.fetchConfig(),
      () => this.linkedAccount.getLinkageStatus(),
      // () => this.creatives.writeSupportedCapabilities(),
      // () => this.account.processContactPointSignals(),
      () => this.feed.timeline().request({ recoveredFromCrash: '1', reason: TimelineFeedReason.cold_start_fetch }),
      () => this.fbsearch.suggestedSearches('users'),
      () => this.fbsearch.suggestedSearches('blended'),
      () => this.fbsearch.recentSearches(),
      () => this.direct.rankedRecipients('reshare'),
      () => this.direct.rankedRecipients('raven'),
      () => this.direct.getPresence(),
      () => this.feed.directInbox().request(),
      () => this.media.blocked(),
      () => this.qp.batchFetch(),
      () => this.qp.getCooldowns(),
      () => this.user.arlinkDownloadInfo(),
      () => this.discover.topicalExplore(),
      () => this.discover.markSuSeen(),
      () => this.facebookOta(),
      () => this.status.getViewableStatuses(),
    ];
  }

  private static async executeRequestsFlow({
    requests,
    concurrency = 1,
    toShuffle = true,
  }: {
    requests: (() => any)[];
    concurrency?: number;
    toShuffle?: boolean;
  }) {
    if (toShuffle) {
      requests = shuffle(requests);
    }
    await Bluebird.map(requests, request => request(), { concurrency });
  }

  public async preLoginFlow(concurrency?: number, toShuffle?: boolean) {
    return SimulateService.executeRequestsFlow({
      requests: this.preLoginFlowRequests,
      concurrency,
      toShuffle,
    });
  }

  public async postLoginFlow(concurrency?: number, toShuffle?: boolean) {
    return SimulateService.executeRequestsFlow({
      requests: this.postLoginFlowRequests,
      concurrency,
      toShuffle,
    });
  }

  private async facebookOta() {
    const uid = this.state.cookies.userId;
    const { body } = await this.http.full({
      url: '/api/v1/facebook_ota/',
      searchParams: {
        fields: this.state.application.FACEBOOK_OTA_FIELDS,
        custom_user_id: uid,
        signed_body: this.http.signature() + '.',
        version_code: this.state.application.APP_VERSION_CODE,
        version_name: this.state.application.APP_VERSION,
        custom_app_id: this.state.application.FACEBOOK_ORCA_APPLICATION_ID,
        custom_device_id: this.state.device.uuid,
      },
    });
    return body;
  }

  async bootstrap(
    options: SimulateBootstrapOptions,
  ): Promise<AccountRepositoryLoginResponseLogged_in_user | AccountRepositoryCurrentUserResponseUser> {
    if (options.saveState) {
      this.http.end$.subscribe(async () => {
        try {
          await options.saveState(await this.state.toJSON());
        } catch (e) {
          SimulateService.simulateDebug(`saveState() failed! ${e.toString()}\n${e.stack}`);
        }
      });
    }
    if (options.device) {
      const device = await SimulateService.resolve(options.device);
      if (typeof device === 'string') {
        this.state.device.generate(device);
      } else {
        // TODO: if state is any & {device: any} -> wrap in object
        await this.state.deserialize({ device });
      }
    } else {
      this.state.device.generate(options.credentials.username);
    }
    if (options.state) {
      await this.state.deserialize((await SimulateService.resolve(options.state)) ?? {});
    }
    if (options.preLoginFlow) {
      try {
        await this.preLoginFlow();
      } catch (e) {
        SimulateService.simulateDebug(`preLoginFlow failed! ${e.toString()}\\n${e.stack}`);
      }
    } else {
      await this.qe.syncLoginExperiments();
    }

    if (!(await this.checkLogin())) {
      await Bluebird.try(() => this.account.login(options.credentials.username, options.credentials.password ?? ''))
        .catch(IgLoginTwoFactorRequiredError, async e => {
          if (!options.onTwoFactor) {
            SimulateService.simulateDebug('No TwoFactor resolver provided.');
            throw new Error('Two Factor required, onTwoFactor() not provided!');
          }
          const twoFactorInfo = e.response.body.two_factor_info;
          const result = await options.onTwoFactor({ twoFactorInfo });
          if (typeof result.method === 'string') result.method = result.method === 'SMS' ? 1 : 0;
          if (!result.method) {
            result.method = twoFactorInfo.totp_two_factor_on ? 0 : 1;
          }
          if (typeof result.trustThisDevice === 'boolean') {
            // @ts-ignore - has to be passed as string
            result.trustThisDevice = result.trustThisDevice ? '1' : '0';
          }
          return this.account.twoFactorLogin({
            username: twoFactorInfo.username,
            verificationMethod: result.method.toString(),
            trustThisDevice: result.trustThisDevice,
            twoFactorIdentifier: twoFactorInfo.two_factor_identifier,
            verificationCode: result.code,
          });
        })
        .catch(IgCheckpointError, async e => {
          if (!options.onChallenge) {
            SimulateService.simulateDebug('No challenge resolver provided.');
            throw new Error('No challenge callback provided');
          }
          try {
            await this.challenge.auto(true);
          } catch (e) {
            SimulateService.simulateDebug('Challenge info failed, most likely a web-challenge.');
            throw new IgNotResolvableError(e.response);
          }
          const { code } = await options.onChallenge({ challenge: e.response.body.challenge });
          return this.challenge.sendSecurityCode(code);
        });
      if (options.postLoginFlow) {
        try {
          await this.postLoginFlow();
        } catch (e) {
          SimulateService.simulateDebug(`postLoginFlow failed! ${e.toString()}\n${e.stack}`);
        }
      }
    }
    return await this.account.currentUser();
  }

  async upgrade() {
    const userId = this.state.cookies.userId;
    const { login_nonce } = await this.account.logout(true);
    return this.account.oneTapAppLogin(userId, login_nonce);
  }

  private async checkLogin(): Promise<boolean> {
    try {
      await this.account.currentUser();
      return true;
    } catch (e) {
      SimulateService.simulateDebug(`Not logged in, logging in (${e.message})`);
      return false;
    }
  }

  private static async resolve<T>(res: Resolvable<T>): Promise<T> {
    if (typeof res === 'function') {
      // @ts-ignore - T should not be of type T & Function
      return res();
    }
    return res;
  }
}
