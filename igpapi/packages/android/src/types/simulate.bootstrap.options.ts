import { AccountRepositoryLoginErrorResponseTwoFactorInfo, CheckpointResponseChallenge } from '../responses';
import { AndroidState } from '../core/android.state';

export interface SimulateBootstrapOptions {
  credentials: Credentials;
  preLoginFlow?: boolean;
  device?: Resolvable<string | unknown>;
  state?: Resolvable<string | any>;
  saveState?: Callback<Partial<AndroidState>, void>;
  onChallenge?: Callback<{ challenge: CheckpointResponseChallenge }, { code: string }>;
  onTwoFactor?: Callback<
    { twoFactorInfo: AccountRepositoryLoginErrorResponseTwoFactorInfo },
    { code: string; method?: 'SMS' | 'TOTP' | 1 | 0; trustThisDevice?: boolean | '1' | '0' }
  >;
  postLoginFlow?: boolean;
}

export type Resolvable<T> = T | Promise<T> | (() => T) | (() => Promise<T>);
export type Callback<TInput, TReturn> = (input: TInput) => PromiseLike<TReturn>;
export interface Credentials {
  username: string;
  password?: string; // if state is present this can be left out
}
