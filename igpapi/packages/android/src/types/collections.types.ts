import { IgAppModule } from '../types';

export interface MediaSaveOptions {
  mediaId: string;
  moduleName?: IgAppModule;
  removedCollections?: string[];
  addedCollections?: string[];
}
