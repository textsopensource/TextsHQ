import { LoggingSequence, LoggingSequenceFactory } from '../core/android.logging';
import { Service } from 'typedi';
import { IgAppModule, IgMediaType } from '../types';
import { AndroidState } from '../core';

@Service({ transient: true, global: true })
export class CarouselSequence implements LoggingSequenceFactory {
  trackingToken: string;
  /**
   * mediaId_userId
   * @type {string}
   */
  mediaId: string;
  authorId: string;
  takenAtTs: number;
  followStatus: 'following' | string; // probably not_following
  carouselItems: Array<{ type: IgMediaType; mediaId: string; taggedUseIds?: string[] }>;
  carouselIndex: number;

  timeSpent: number;

  module?: IgAppModule;
  feedPosition?: number;
  feedRequestId?: string;
  timeSinceLastItem?: number;

  constructor(private state: AndroidState) {}

  execute(): LoggingSequence {
    const mostBasicChunk = {
      m_pk: this.mediaId,
      m_t: IgMediaType.Carousel,
      tracking_token: this.trackingToken,
      m_ix: this.feedPosition,
      carousel_index: this.carouselIndex,
      carousel_media_id: this.mediaId,
      carousel_cover_media_id: this.mediaId,
      carousel_m_t: this.carouselItems[this.carouselIndex].type,
      carousel_size: this.carouselItems.length,
      inventory_source: 'media_or_ad',
      feed_request_id: this.feedRequestId,
      delivery_flags: 'c,n',
      imp_logger_ver: 24,
      pk: this.state.extractUserId(),
      release_channel: 'prod',
      radio_type: this.state.device.radioType,
    };
    const baseChunk = {
      ...mostBasicChunk,
      a_pk: this.authorId,
      m_ts: this.takenAtTs,
      source_of_action: this.module ?? 'feed_timeline',
      follow_status: this.followStatus,
      elapsed_time_since_last_item: this.timeSinceLastItem ?? -1,
      tagged_user_ids: this.carouselItems[this.carouselIndex].taggedUseIds,
      is_eof: false,
      is_ad: false,
      is_acp_delivered: false,
    };

    return [
      [
        {
          name: 'instagram_organic_carousel_sub_viewed_impression',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
        },
        {
          ...baseChunk,
          post_impression_column_override: true,
        },
      ],
      [
        {
          name: 'instagram_organic_carousel_time_spent',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
          tags: 32, // TODO
        },
        {
          ...baseChunk,
          timespent: this.timeSpent,
        },
      ],
      [
        {
          name: 'instagram_organic_carousel_vpvd_imp',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
        },
        {
          ...mostBasicChunk,
          max_duration_ms: this.timeSpent,
          sum_duration_ms: this.timeSpent,
          legacy_duration_ms: this.timeSpent,
          session_id: this.state.clientSessionId, // not sure which id
        },
      ],
      [
        {
          name: 'instagram_organic_carousel_sub_viewed_impression',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
        },
        { ...baseChunk, session_id: this.state.clientSessionId },
      ],
      [
        {
          name: 'instagram_organic_time_spent',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
          tags: 32,
        },
        {
          ...baseChunk,
          timespent: this.timeSpent,
          avgViewpercent: 1.0,
          maxViewPercent: 1.0,
          session_id: this.state.clientSessionId,
        },
      ],
      [
        {
          name: 'instagram_organic_vpvd_imp',
          module: this.module ?? 'feed_timeline',
          sampling_rate: 1,
        },
        {
          ...mostBasicChunk,
          max_duration_ms: this.timeSpent,
          sum_duration_ms: this.timeSpent,
          legacy_duration_ms: this.timeSpent,
          session_id: this.state.clientSessionId, // not sure which id
        },
      ],
    ];
  }
}
