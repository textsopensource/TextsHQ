import { plainToClassFromExist } from 'class-transformer';
import Chance from 'chance';
import descriptors from '../samples/devices.json';
import builds from '../samples/builds.json';

export class AndroidDevice {
  id: string;
  descriptor: string;
  uuid: string;
  phoneId: string;
  /**
   * Google Play Advertising ID.
   *
   * The advertising ID is a unique ID for advertising, provided by Google
   * Play services for use in Google Play apps. Used by Instagram.
   *
   * @see https://support.google.com/googleplay/android-developer/answer/6048248?hl=en
   */
  adid: string;
  build: string;

  language = 'en_US';
  radioType = 'wifi-none';
  connectionType = 'WIFI';
  timezoneOffset = String(new Date().getTimezoneOffset() * -60);
  isLayoutRTL = false;

  get batteryLevel() {
    const chance = new Chance(this.id);
    const percentTime = chance.integer({ min: 200, max: 600 });
    return 100 - (Math.round(Date.now() / 1000 / percentTime) % 100);
  }

  get isCharging() {
    const chance = new Chance(`${this.id}${Math.round(Date.now() / 10800000)}`);
    return chance.bool();
  }

  get payload() {
    const deviceParts = this.descriptor.split(';');
    const [android_version, android_release] = deviceParts[0].split('/');
    const [manufacturer] = deviceParts[3].split('/');
    const model = deviceParts[4];
    return {
      android_version,
      android_release,
      manufacturer,
      model,
    };
  }

  generate(seed: string) {
    const chance = new Chance(seed);
    const data: Partial<AndroidDevice> = {
      id: `android-${chance.hash({ length: 16 })}`,
      descriptor: chance.pickone(descriptors),
      uuid: chance.guid(),
      phoneId: chance.guid(),
      adid: chance.guid(),
      build: chance.pickone(builds),
    };
    plainToClassFromExist(this, data);
  }
}
