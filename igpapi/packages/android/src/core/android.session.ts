export class AndroidSession {
  // set by the app

  euDCEnabled?: boolean = undefined;
  thumbnailCacheBustingValue = 1000;
  adsOptOut = false;

  // sent by Instagram

  igWWWClaim?: string;
  authorization?: string;
  passwordEncryptionPubKey?: string;
  passwordEncryptionKeyId?: string | number;
  regionHint?: string;
}
