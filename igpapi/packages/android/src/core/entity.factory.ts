import { IgpapiFactory, IgpapiFactoryInput } from '@textshq/core';
import { Service } from 'typedi';
import { DirectThreadEntity, ProfileEntity } from '../entities';

@Service()
export class EntityFactory {
  constructor(private factory: IgpapiFactory) {}

  public directThread(input: IgpapiFactoryInput<DirectThreadEntity>): DirectThreadEntity {
    return this.factory.instantiate(DirectThreadEntity, input);
  }

  public profile(input: IgpapiFactoryInput<ProfileEntity>): ProfileEntity {
    return this.factory.instantiate(ProfileEntity, input);
  }
}
