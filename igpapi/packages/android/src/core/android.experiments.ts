import { QeSyncResponseExperimentsItem, QeSyncResponseRootObject } from '../responses';

export class AndroidExperiments {
  public experiments: { [x: string]: AndroidExperiment } = {};

  public get(name: string | Experiment): AndroidExperiment {
    return this.experiments[name] ?? { igGroup: '' };
  }

  /**
   * Update the experiments
   * @param {object} updated - the parsed experiments
   */
  public update(updated: QeSyncResponseRootObject) {
    this.experiments = Object.fromEntries(
      updated.experiments.map(item => [item.name, { ...this.experiments[item.name], ...this.parse(item) }]),
    );
  }

  protected parse(experiment: QeSyncResponseExperimentsItem): AndroidExperiment {
    const simpleParse = (value: string): any => {
      try {
        return JSON.parse(value);
      } catch {
        return value;
      }
    };
    return {
      igGroup: experiment.group,
      igAdditional: experiment.additional_params,
      igLoggingId: experiment.logging_id,
      ...Object.fromEntries(experiment.params.map(item => [item.name, simpleParse(item.value)])),
    };
  }
}

export interface AndroidExperiment {
  igGroup: string;
  igAdditional?: any[];
  igLoggingId?: string;
  [x: string]: boolean | number | string | any[]; // any[] only for igAdditional
}

export enum Experiment {
  // this is right; imagine spelling this encrytpion
  pwdEncryption = 'ig_android_pwd_encrytpion',
}
