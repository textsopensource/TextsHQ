import { Exclude, Type } from 'class-transformer';
import { IgpapiState } from '@textshq/core';
import Chance from 'chance';
import { IgUserIdNotFoundError } from '../errors';
import { ChallengeStateResponse } from '../responses';
import { AndroidDevice } from './android.device';
import { AndroidApplication } from './android.application';
import { AndroidSession } from './android.session';
import { AndroidExperiments } from '../core/android.experiments';

export class AndroidState extends IgpapiState {
  @Type(() => AndroidDevice)
  device: AndroidDevice = new AndroidDevice();

  @Type(() => AndroidSession)
  session: AndroidSession = new AndroidSession();

  @Type(() => AndroidApplication)
  application: AndroidApplication = new AndroidApplication();

  @Exclude()
  experiments = new AndroidExperiments();

  challenge: ChallengeStateResponse | null = null;

  clientSessionIdLifetime = 1200000;
  pigeonSessionIdLifetime = 1200000;

  /**
   * The current application session ID.
   *
   * This is a temporary ID which changes in the official app every time the
   * user closes and re-opens the Instagram application or switches account.
   *
   * We will update it once an hour
   */
  public get clientSessionId(): string {
    return this.generateTemporaryGuid('clientSessionId', this.clientSessionIdLifetime);
  }

  public get pigeonSessionId(): string {
    return this.generateTemporaryGuid('pigeonSessionId', this.pigeonSessionIdLifetime);
  }

  public get userAgent() {
    // eslint-disable-next-line max-len
    return `Instagram ${this.application.APP_VERSION} Android (${this.device.descriptor}; ${this.device.language}; ${this.application.APP_VERSION_CODE})`;
  }

  public isExperimentEnabled(experiment: string) {
    return this.application.EXPERIMENTS.includes(experiment);
  }

  public extractUserId(): string {
    try {
      return this.cookies.userId;
    } catch (e) {
      if (this.challenge === null || !this.challenge.user_id) {
        throw new IgUserIdNotFoundError();
      }
      return String(this.challenge.user_id);
    }
  }

  private generateTemporaryGuid(seed: string, lifetime: number) {
    return new Chance(`${seed}${this.device.id}${Math.round(Date.now() / lifetime)}`).guid();
  }
}
