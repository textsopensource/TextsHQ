import { ContainerInstance, Service } from 'typedi';
import { AndroidHttp, AndroidState } from '../core';
import { CookieJar } from 'tough-cookie';
import { Cls, IgpapiFactoryInput } from '@textshq/core';
import { plainToClassFromExist } from 'class-transformer';
import { LoggingSendStrategy, UrlencodedLoggingSendStrategy } from '../logging-send-strategies';

export enum LoggingSendMode {
  UrlEncoded,
  Form,
}

export enum LoggingChannel {
  ZeroLatency = 'zero_latency',
  Regular = 'regular',
}

export interface LoggingChunkHeader {
  name: string;
  time?: string;
  module?: string;
  sampling_rate?: number;
  tags?: number;
}

export interface LoggingResponse {
  checksum: string;
  config: string;
  config_owner_id: string;
  app_data: string;
}

export type LoggingSequence = Array<[LoggingChunkHeader, Record<string, unknown>]>;

@Service()
export class AndroidLogging {
  chunks: Array<LoggingChunkHeader & { extra: Record<string, unknown> }> = [];
  sequenceId = 1;
  configChecksum?: string;
  channel = LoggingChannel.ZeroLatency;

  constructor(private http: AndroidHttp, private state: AndroidState, private containerInstance: ContainerInstance) {}

  async sequence<T extends LoggingSequenceFactory>(seq: LoggingSequence | Cls<T>, plain?: IgpapiFactoryInput<T>) {
    if (typeof seq === 'function') {
      const instance = plainToClassFromExist(this.containerInstance.get(seq), plain, { ignoreDecorators: true });
      seq = await instance.execute();
    }
    for (const [header, data] of seq) this.push(header, data);
    return this.flush();
  }

  push(header: LoggingChunkHeader, data: Record<string, unknown>): this {
    this.chunks.push({
      time: String(Date.now() / 1000),
      ...header,
      extra: data,
    });
    return this;
  }

  async flush(strategy: LoggingSendStrategy = new UrlencodedLoggingSendStrategy()): Promise<LoggingResponse> {
    const chunks = this.chunks;
    this.chunks = [];

    const res = await strategy.send(
      {
        chunks,
        channel: this.channel,
        configChecksum: this.configChecksum,
        sequenceId: this.sequenceId++,
      },
      this.state,
      this.http,
    );

    if (res.checksum) this.configChecksum = res.checksum;

    return res;
  }
}

export interface LoggingSequenceFactory {
  execute(): LoggingSequence | Promise<LoggingSequence>;
}
