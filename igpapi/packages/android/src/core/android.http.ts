import { random } from 'lodash';
import { createHmac } from 'crypto';
import {
  IgActionSpamError,
  IgCheckpointError,
  IgInactiveUserError,
  IgLoginRequiredError,
  IgNotFoundError,
  IgPrivateUserError,
  IgSentryBlockError,
  IgUserHasLoggedOutError,
} from '../errors';
import debug from 'debug';
import { AndroidState } from './android.state';
import { Service } from 'typedi';
import { BodyStatusCheckerStrategy, IgResponseError, IgClientError, IgpapiHttp } from '@textshq/core';
import got, { Response } from 'got';
// no @types for json-bigint
// eslint-disable-next-line @typescript-eslint/no-var-requires
const JSONBigInt = require('json-bigint');

const jsonBigString = JSONBigInt({ storeAsString: true });

type Payload = { [key: string]: any } | string;

/**
 * This represents a kind of bitfield, so (except for precomputed values) this should contain values of log2(n) % 1 === 0
 */
export enum RequestDefaults {
  CSRFToken = 0x1,
  UserId = 0x2,
  UUID = 0x4,

  // precomputed
  Base = CSRFToken | UserId | UUID,
}

/**
 *  This class now uses 'got'
 *  There's a migration guide here:
 *  https://github.com/sindresorhus/got/blob/master/documentation/migration-guides.md
 *
 *  As a SOCKS-Proxy, use 'socks-proxy-agent'
 *  https://github.com/TooTallNate/node-socks-proxy-agent
 *  Note: typescript will complain, bot you can `@ts-ignore` it
 *  @example igpapi.http.defaults.agent = {
      https: new SocksProxyAgent({
        host,
        port
      }),
    }
 *
 *  As a HTTP(S) proxy, use 'tunnel'
 *  https://github.com/koichik/node-tunnel
 *  @example https://github.com/sindresorhus/got#proxies
 *
 */

@Service()
export class AndroidHttp extends IgpapiHttp {
  client = got.extend({
    prefixUrl: 'https://i.instagram.com',
    decompress: true,
    mutableDefaults: true,
    hooks: {
      beforeRequest: [
        request => {
          if (request.http2) {
            delete request.headers.host;
            delete request.headers.connection;
          }
        },
      ],
      afterResponse: [
        response => {
          // application/json; charset=utf8
          if (
            ((response.headers['content-type'] && response.headers['content-type'].includes('application/json')) ||
              (typeof response.body === 'string' && response.body.startsWith('{'))) &&
            typeof response.body === 'string'
          ) {
            response.body = jsonBigString.parse(response.body);
          }
          return response;
        },
      ],
    },
  });
  protected debug = debug('ig:android:http');
  protected statusCheckerStrategy = new BodyStatusCheckerStrategy();

  constructor(protected state: AndroidState) {
    super();
  }

  public getDefaults(targets: RequestDefaults): object {
    return {
      _csrftoken: targets & RequestDefaults.CSRFToken ? this.state.cookies.csrfToken : void 0,
      _uid: targets & RequestDefaults.UserId ? this.state.extractUserId() : void 0,
      _uuid: targets & RequestDefaults.UUID ? this.state.device.uuid : void 0,
    };
  }

  public signature() {
    return 'SIGNATURE';
  }

  public sign(payload: Payload) {
    const json = typeof payload === 'object' ? JSON.stringify(payload) : payload;
    const signature = this.signature();
    return {
      signed_body: `${signature}.${json}`,
    };
  }

  public userBreadcrumb(size: number) {
    const term = random(2, 3) * 1000 + size + random(15, 20) * 1000;
    const textChangeEventCount = Math.round(size / random(2, 3)) || 1;
    const data = `${size} ${term} ${textChangeEventCount} ${Date.now()}`;
    const signature = Buffer.from(
      createHmac('sha256', this.state.application.BREADCRUMB_KEY)
        .update(data)
        .digest('hex'),
    ).toString('base64');
    const body = Buffer.from(data).toString('base64');
    return `${signature}\n${body}\n`;
  }

  error(response: Response<any>): IgClientError {
    this.debug(
      `Request ${response.request.options.method} ${response.request.options.url.pathname} failed: ${
        typeof response.body === 'object' ? JSON.stringify(response.body) : response.body
      }`,
    );

    const json = response.body;
    if (json.spam) {
      return new IgActionSpamError(response);
    }
    if (response.statusCode === 404) {
      return new IgNotFoundError(response);
    }
    if (typeof json.message === 'string') {
      if (json.message === 'challenge_required') {
        const err = new IgCheckpointError(response);
        this.state.challengePath = err.url();
        return err;
      }
      if (json.message === 'user_has_logged_out') {
        return new IgUserHasLoggedOutError(response);
      }
      if (json.message === 'login_required') {
        return new IgLoginRequiredError(response);
      }
      if (json.message.toLowerCase() === 'not authorized to view user') {
        return new IgPrivateUserError(response);
      }
    }
    if (json.error_type === 'sentry_block') {
      return new IgSentryBlockError(response);
    }
    if (json.error_type === 'inactive user') {
      return new IgInactiveUserError(response);
    }
    return new IgResponseError(response);
  }

  public headers() {
    return {
      'User-Agent': this.state.userAgent,
      'X-Ads-Opt-Out': this.state.session.adsOptOut ? '1' : '0',
      // needed? 'X-DEVICE-ID': this.state.device.uuid,
      'X-CM-Bandwidth-KBPS': '-1.000',
      'X-CM-Latency': '-1.000',
      'X-IG-App-Locale': this.state.device.language,
      'X-IG-Device-Locale': this.state.device.language,
      'X-Pigeon-Session-Id': this.state.pigeonSessionId,
      'X-Pigeon-Rawclienttime': (Date.now() / 1000).toFixed(3),
      'X-IG-Connection-Speed': `${random(1000, 3700)}kbps`,
      'X-IG-Bandwidth-Speed-KBPS': '-1.000',
      'X-IG-Bandwidth-TotalBytes-B': '0',
      'X-IG-Bandwidth-TotalTime-MS': '0',
      'X-IG-EU-DC-ENABLED':
        typeof this.state.session.euDCEnabled === 'undefined' ? void 0 : this.state.session.euDCEnabled?.toString(),
      'X-IG-Extended-CDN-Thumbnail-Cache-Busting-Value': this.state.session.thumbnailCacheBustingValue?.toString(),
      'X-Bloks-Version-Id': this.state.application.BLOKS_VERSION_ID,
      'X-MID': this.state.cookies.get('mid')?.value,
      'X-IG-WWW-Claim': this.state.session.igWWWClaim || '0',
      'X-Bloks-Is-Layout-RTL': this.state.device.isLayoutRTL.toString(),
      'X-IG-Connection-Type': this.state.device.connectionType,
      'X-IG-Capabilities': this.state.application.CAPABILITIES,
      'X-IG-App-ID': this.state.application.FACEBOOK_ANALYTICS_APPLICATION_ID,
      'X-IG-Device-ID': this.state.device.uuid,
      'X-IG-Android-ID': this.state.device.id,
      'Accept-Language': this.state.device.language.replace('_', '-'),
      'X-FB-HTTP-Engine': 'Liger',
      Authorization: this.state.session.authorization,
      Host: 'i.instagram.com',
      'Accept-Encoding': 'gzip',
      Connection: 'close',
    };
  }

  protected finally(response: Response<any>) {
    const {
      'x-ig-set-www-claim': wwwClaim,
      'ig-set-authorization': auth,
      'ig-set-password-encryption-key-id': pwKeyId,
      'ig-set-password-encryption-pub-key': pwPubKey,
      'ig-set-ig-u-ig-direct-region-hint': regionHint,
    } = response.headers;
    if (typeof wwwClaim === 'string') {
      this.state.session.igWWWClaim = wwwClaim;
    }
    if (typeof auth === 'string' && !auth.endsWith(':')) {
      this.state.session.authorization = auth;
    }
    if (typeof pwKeyId === 'string') {
      this.state.session.passwordEncryptionKeyId = pwKeyId;
    }
    if (typeof pwPubKey === 'string') {
      this.state.session.passwordEncryptionPubKey = pwPubKey;
    }
    if (typeof regionHint === 'string') {
      this.state.session.regionHint = regionHint;
    }
  }
}
