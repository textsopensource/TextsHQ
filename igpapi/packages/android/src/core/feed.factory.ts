import {
  AccountFollowersFeed,
  AccountFollowingFeed,
  BlockedUsersFeed,
  DirectInboxFeed,
  DirectPendingInboxFeed,
  DirectThreadFeed,
  DiscoverFeed,
  PostsInsightsFeed,
  LocationFeed,
  MediaCommentsFeed,
  MusicGenreFeed,
  MusicMoodFeed,
  MusicSearchFeed,
  MusicTrendingFeed,
  NewsFeed,
  PendingFriendshipsFeed,
  ReelsMediaFeed,
  ReelsTrayFeed,
  SavedFeed,
  StoriesInsightsFeed,
  TagFeed,
  TagsFeed,
  TimelineFeed,
  UserFeed,
  UsertagsFeed,
  IgtvBrowseFeed,
  IgtvChannelFeed,
  LikedFeed,
  CollectionsListFeed,
  CollectionFeed,
  OnlyMeFeed,
  ArchiveDayShellsFeed,
  ClipsUserFeed,
  ClipsHomeFeed,
  ClipsMusicFeed,
  UserStoryFeed,
  ListReelMediaViewerFeed,
  MediaInlineChildCommentsFeed,
  TopicalExploreFeed,
} from '../feeds';
import { AndroidState } from './android.state';
import { Service } from 'typedi';
import { FeedProperties, IgpapiFactory } from '@textshq/core';

@Service()
export class FeedFactory {
  // TODO: @Service({transient: true, global: true}) -> @Service({transient: true}) whenever typedi gets updated
  constructor(private factory: IgpapiFactory, private state: AndroidState) {}

  public accountFollowers(input: Partial<FeedProperties<AccountFollowersFeed>> = {}): AccountFollowersFeed {
    return this.factory.instantiate(AccountFollowersFeed, {
      id: this.state.cookies.userId,
      ...input,
    });
  }

  public accountFollowing(input: Partial<FeedProperties<AccountFollowingFeed>> = {}): AccountFollowingFeed {
    return this.factory.instantiate(AccountFollowingFeed, {
      id: this.state.cookies.userId,
      ...input,
    });
  }

  /**
   * Activity Feed
   * @returns {NewsFeed}
   */
  public news(input: FeedProperties<NewsFeed> = {}): NewsFeed {
    return this.factory.instantiate(NewsFeed, input);
  }

  /**
   * List saved collections
   * @returns {CollectionsListFeed}
   */
  public collectionsList(input: FeedProperties<CollectionsListFeed> = {}): CollectionsListFeed {
    return this.factory.instantiate(CollectionsListFeed, input);
  }

  /**
   * Saved collection
   * @returns {CollectionFeed}
   */
  public collection(input: FeedProperties<CollectionFeed>): CollectionFeed {
    return this.factory.instantiate(CollectionFeed, input);
  }

  public discover(input: FeedProperties<DiscoverFeed> = {}): DiscoverFeed {
    return this.factory.instantiate(DiscoverFeed, input);
  }

  public pendingFriendships(input: FeedProperties<PendingFriendshipsFeed> = {}): PendingFriendshipsFeed {
    return this.factory.instantiate(PendingFriendshipsFeed, input);
  }

  public blockedUsers(input: FeedProperties<BlockedUsersFeed> = {}): BlockedUsersFeed {
    return this.factory.instantiate(BlockedUsersFeed, input);
  }

  public directInbox(input: FeedProperties<DirectInboxFeed> = {}): DirectInboxFeed {
    return this.factory.instantiate(DirectInboxFeed, input);
  }

  /**
   * Pending - not yet accepted threads
   * @returns {DirectPendingInboxFeed}
   */
  public directPending(input: FeedProperties<DirectPendingInboxFeed> = {}): DirectPendingInboxFeed {
    return this.factory.instantiate(DirectPendingInboxFeed, input);
  }

  public directThread(input: FeedProperties<DirectThreadFeed>): DirectThreadFeed {
    return this.factory.instantiate(DirectThreadFeed, input);
  }

  public user(input: FeedProperties<UserFeed>): UserFeed {
    return this.factory.instantiate(UserFeed, input);
  }

  public tag(input: FeedProperties<TagFeed>): TagFeed {
    return this.factory.instantiate(TagFeed, input);
  }

  public tags(input: FeedProperties<TagsFeed>): TagsFeed {
    return this.factory.instantiate(TagsFeed, input);
  }

  public location(input: FeedProperties<LocationFeed>): LocationFeed {
    return this.factory.instantiate(LocationFeed, input);
  }

  public mediaComments(input: FeedProperties<MediaCommentsFeed>): MediaCommentsFeed {
    return this.factory.instantiate(MediaCommentsFeed, input);
  }

  /**
   * Story of **multiple** users
   * @returns {ReelsMediaFeed}
   */
  public reelsMedia(input: FeedProperties<ReelsMediaFeed>): ReelsMediaFeed {
    return this.factory.instantiate(ReelsMediaFeed, input);
  }

  public userStory(input: FeedProperties<UserStoryFeed>): UserStoryFeed {
    return this.factory.instantiate(UserStoryFeed, input);
  }

  /**
   * Tray on the Timeline
   * @returns {ReelsTrayFeed}
   */
  public reelsTray(input: FeedProperties<ReelsTrayFeed> = {}): ReelsTrayFeed {
    return this.factory.instantiate(ReelsTrayFeed, input);
  }

  public timeline(input: FeedProperties<TimelineFeed> = {}): TimelineFeed {
    return this.factory.instantiate(TimelineFeed, input);
  }

  public musicTrending(input: FeedProperties<MusicTrendingFeed> = {}): MusicTrendingFeed {
    return this.factory.instantiate(MusicTrendingFeed, input);
  }

  public musicSearch(input: FeedProperties<MusicSearchFeed>): MusicSearchFeed {
    return this.factory.instantiate(MusicSearchFeed, input);
  }

  public musicGenre(input: FeedProperties<MusicGenreFeed>): MusicGenreFeed {
    return this.factory.instantiate(MusicGenreFeed, input);
  }

  public musicMood(input: FeedProperties<MusicMoodFeed>): MusicMoodFeed {
    return this.factory.instantiate(MusicMoodFeed, input);
  }

  /**
   * Tagged posts of a user. (3rd. Item on the Profile Page)
   * @returns {UsertagsFeed}
   */
  public usertags(input: FeedProperties<UsertagsFeed>): UsertagsFeed {
    return this.factory.instantiate(UsertagsFeed, input);
  }

  public postsInsightsFeed(input: FeedProperties<PostsInsightsFeed>): PostsInsightsFeed {
    return this.factory.instantiate(PostsInsightsFeed, input);
  }

  public storiesInsights(input: FeedProperties<StoriesInsightsFeed>): StoriesInsightsFeed {
    return this.factory.instantiate(StoriesInsightsFeed, input);
  }

  public saved(input: FeedProperties<SavedFeed> = {}): SavedFeed {
    return this.factory.instantiate(SavedFeed, input);
  }

  /**
   * Story (Reel) viewers
   * @returns {ListReelMediaViewerFeed}
   */
  public listReelMediaViewers(input: FeedProperties<ListReelMediaViewerFeed>): ListReelMediaViewerFeed {
    return this.factory.instantiate(ListReelMediaViewerFeed, input);
  }

  /**
   * Comment responses
   * @returns {MediaInlineChildCommentsFeed}
   */
  public mediaInlineChildComments(input: FeedProperties<MediaInlineChildCommentsFeed>): MediaInlineChildCommentsFeed {
    return this.factory.instantiate(MediaInlineChildCommentsFeed, input);
  }

  public igtvBrowse(input: FeedProperties<IgtvBrowseFeed> = {}): IgtvBrowseFeed {
    return this.factory.instantiate(IgtvBrowseFeed, input);
  }

  /**
   * Explore Grid
   * @returns {TopicalExploreFeed}
   */
  public topicalExplore(input: FeedProperties<TopicalExploreFeed> = {}): TopicalExploreFeed {
    return this.factory.instantiate(TopicalExploreFeed, input);
  }

  /**
   * The Archive
   * @returns {OnlyMeFeed}
   */
  public onlyMe(input: FeedProperties<OnlyMeFeed> = {}): OnlyMeFeed {
    return this.factory.instantiate(OnlyMeFeed, input);
  }

  /**
   * Stories-Archive
   * @returns {ArchiveDayShellsFeed}
   */
  public archiveDayShells(input: FeedProperties<ArchiveDayShellsFeed> = {}): ArchiveDayShellsFeed {
    return this.factory.instantiate(ArchiveDayShellsFeed, input);
  }

  public clipsUser(input: FeedProperties<ClipsUserFeed>): ClipsUserFeed {
    return this.factory.instantiate(ClipsUserFeed, input);
  }

  public clipsHome(input: FeedProperties<ClipsHomeFeed> = {}): ClipsHomeFeed {
    return this.factory.instantiate(ClipsHomeFeed, input);
  }

  /**
   * The Music Feed (all clips for a given sound)
   * originalAssetId => 'original'
   * assetId, clusterId => 'music'
   *
   * @returns {ClipsMusicFeed}
   */
  public clipsMusic(input: FeedProperties<ClipsMusicFeed>): ClipsMusicFeed {
    return this.factory.instantiate(ClipsMusicFeed, input);
  }

  // TODO: nerix
  // public storyQuestionResponses(
  //   mediaId: string,
  //   stickerId: string | number,
  // ): MediaStickerResponsesFeed<
  //   StoryQuestionResponsesFeedResponseRootObject,
  //   StoryQuestionResponsesFeedResponseRespondersItem
  // > {
  //   return plainToClassFromExist(this.container.resolve(MediaStickerResponsesFeed), {
  //     mediaId,
  //     stickerId,
  //     name: 'story_question_responses',
  //     rootName: 'responder_info',
  //     itemName: 'responders',
  //   });
  // }
  //
  // public storyPollVoters(
  //   mediaId: string,
  //   stickerId: string | number,
  // ): MediaStickerResponsesFeed<StoryPollVotersFeedResponseRootObject, StoryPollVotersFeedResponseVotersItem> {
  //   return plainToClassFromExist(this.container.get(MediaStickerResponsesFeed<any, any>), {
  //     mediaId,
  //     stickerId,
  //     name: 'story_poll_voters',
  //     rootName: 'voter_info',
  //     itemName: 'voters',
  //   });
  // }
  //
  // public storyQuizParticipants(
  //   mediaId: string,
  //   stickerId: string | number,
  // ): MediaStickerResponsesFeed<
  //   StoryQuizParticipantsFeedResponseRootObject,
  //   StoryQuizParticipantsFeedResponseParticipantsItem
  // > {
  //   return plainToClassFromExist(this.container.get(MediaStickerResponsesFeed<any, any>), {
  //     mediaId,
  //     stickerId,
  //     name: 'story_quiz_participants',
  //     rootName: 'participant_info',
  //     itemName: 'participants',
  //   });
  // }
  //
  // public storySliderVoters(
  //   mediaId: string,
  //   stickerId: string | number,
  // ): MediaStickerResponsesFeed<
  //   StorySliderVotersFeedResponseResponseRootObject,
  //   StorySliderVotersFeedResponseResponseVotersItem
  // > {
  //   return plainToClassFromExist(this.container.get(MediaStickerResponsesFeed<any, any>), {
  //     mediaId,
  //     stickerId,
  //     name: 'story_slider_voters',
  //     rootName: 'voter_info',
  //     itemName: 'voters',
  //   });
  // }

  public igtvChannel(input: FeedProperties<IgtvChannelFeed>): IgtvChannelFeed {
    return this.factory.instantiate(IgtvChannelFeed, input);
  }

  public liked(input: FeedProperties<LikedFeed> = {}): LikedFeed {
    return this.factory.instantiate(LikedFeed, input);
  }
}
