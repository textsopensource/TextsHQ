import { IgResponseError } from '@textshq/core';

export class IgInactiveUserError extends IgResponseError {}
