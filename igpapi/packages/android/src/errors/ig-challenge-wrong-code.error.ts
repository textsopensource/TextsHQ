import { IgClientError } from '@textshq/core';

export class IgChallengeWrongCodeError extends IgClientError {}
