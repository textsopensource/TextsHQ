import { LoginRequiredResponse } from '../responses';
import { IgResponseError } from '@textshq/core';

export class IgLoginRequiredError extends IgResponseError<LoginRequiredResponse> {}
