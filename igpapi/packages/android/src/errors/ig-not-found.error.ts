import { IgResponseError } from '@textshq/core';

export class IgNotFoundError extends IgResponseError {}
