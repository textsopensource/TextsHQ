import { IgResponseError } from '@textshq/core';
import { SpamResponse } from '../responses';

export class IgSignupBlockError extends IgResponseError<SpamResponse> {}
