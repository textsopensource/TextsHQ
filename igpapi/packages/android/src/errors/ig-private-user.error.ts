import { IgResponseError } from '@textshq/core';

export class IgPrivateUserError extends IgResponseError {}
