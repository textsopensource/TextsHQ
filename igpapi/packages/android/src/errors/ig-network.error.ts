import { IgClientError } from '@textshq/core';

export class IgNetworkError extends IgClientError {
  constructor(e: Error) {
    super();
    delete e.name;
    Object.assign(this, e);
  }
}
