import { IgResponseError } from '@textshq/core';
import { Response } from 'got';
import { UploadRepositoryVideoResponseRootObject } from '../responses';

export class IgUploadVideoError extends IgResponseError {
  constructor(response: Response<UploadRepositoryVideoResponseRootObject>, public videoInfo: any) {
    super(response);
  }
}
