import { LoginRequiredResponse } from '../responses';
import { IgResponseError } from '@textshq/core';

export class IgUserHasLoggedOutError extends IgResponseError<LoginRequiredResponse> {}
