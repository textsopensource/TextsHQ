import { IgClientError } from '@textshq/core';

export class IgRequestsLimitError extends IgClientError {
  constructor() {
    super('You just made too many request to instagram API');
  }
}
