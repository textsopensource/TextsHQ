import { IgResponseError } from '@textshq/core';

export class IgSentryBlockError extends IgResponseError {}
