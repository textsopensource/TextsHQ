import { IgResponseError } from '@textshq/core';
import { AccountRepositoryLoginErrorResponse } from '../responses';

export class IgLoginBadPasswordError extends IgResponseError<AccountRepositoryLoginErrorResponse> {}
