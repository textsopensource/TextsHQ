import { IgClientError } from '@textshq/core';

export class IgParseError extends IgClientError {
  constructor(public body: string) {
    super('Not possible to parse API response');
  }
}
