import { IgResponseError } from '@textshq/core';
import { CheckpointResponse } from '../responses';

export class IgNotResolvableError extends IgResponseError<CheckpointResponse> {}
