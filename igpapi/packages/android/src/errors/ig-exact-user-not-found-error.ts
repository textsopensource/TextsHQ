import { IgClientError } from '@textshq/core';

export class IgExactUserNotFoundError extends IgClientError {
  constructor() {
    super('User with exact username not found.');
  }
}
