import { IgResponseError } from '@textshq/core';
import { AccountRepositoryLoginErrorResponse } from '../responses';

export class IgLoginTwoFactorRequiredError extends IgResponseError<AccountRepositoryLoginErrorResponse> {}
