import { IgClientError } from '@textshq/core';

export class IgUserIdNotFoundError extends IgClientError {
  constructor() {
    super(`Could not extract userid (pk)`);
  }
}
