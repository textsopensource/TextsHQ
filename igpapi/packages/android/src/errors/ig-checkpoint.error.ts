import { IgpapiChallengeRequiredError } from '@textshq/core';
import { IgResponseError } from '@textshq/core';
import { CheckpointResponse } from '../responses';

export class IgCheckpointError extends IgResponseError<CheckpointResponse> implements IgpapiChallengeRequiredError {
  url() {
    return this.response.body.challenge.api_path;
  }
}
