import { IgResponseError } from '@textshq/core';
import { AccountRepositoryLoginErrorResponse } from '../responses';

export class IgLoginInvalidUserError extends IgResponseError<AccountRepositoryLoginErrorResponse> {}
