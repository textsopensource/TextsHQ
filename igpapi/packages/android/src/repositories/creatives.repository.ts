import { Service } from 'typedi';
import { AndroidHttp, RequestDefaults } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { IgAppModule } from '../types';

@Service()
export class CreativesRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async writeSupportedCapabilities() {
    const { body } = await this.http.full({
      url: '/api/v1/creatives/write_supported_capabilities/',
      method: 'POST',
      form: this.http.sign({
        supported_capabilities_new: JSON.stringify(this.state.application.SUPPORTED_CAPABILITIES),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
    return body;
  }

  public clipsAssets(
    options: {
      horizontalAccuracy?: number | string;
      camera_entry_point?: IgAppModule;
      alt?: number | string;
      lat?: number | string;
      lng?: number | string;
      type?: 'static_stickers' | string;
      speed?: number | string;
    } = {},
  ) {
    return this.http.body({
      url: '/api/v1/creatives/clip_assets/',
      form: this.http.sign({
        ...this.http.getDefaults(RequestDefaults.Base),
        horizontalAccuracy: (options.horizontalAccuracy ?? 10.0).toString(),
        camera_entry_point: options.camera_entry_point ?? 'your_story_placeholder',
        alt: (options.alt ?? 50.0).toString(),
        lat: (options.lat ?? 53.0).toString(),
        lng: (options.lng ?? 10.0).toString(),
        type: options.type ?? 'static_stickers',
        speed: (options.speed ?? 0.0).toString(),
      }),
    });
  }
}
