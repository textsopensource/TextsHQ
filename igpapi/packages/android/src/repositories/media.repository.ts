import { defaultsDeep, omit } from 'lodash';
import { DateTime } from 'luxon';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import {
  MediaEditResponseRootObject,
  MediaInfoResponseRootObject,
  MediaRepositoryBlockedResponse,
  MediaRepositoryCheckOffensiveCommentResponseRootObject,
  MediaRepositoryCommentResponse,
  MediaRepositoryConfigureResponseRootObject,
  MediaRepositoryLikersResponseRootObject,
  MediaUpdatedMediaResponseRootObject,
  StatusResponse,
} from '../responses';
import {
  IgAppModule,
  LikeRequestOptions,
  MediaCommentLikeOptions,
  MediaConfigureOptions,
  MediaConfigureSidecarItem,
  MediaConfigureSidecarOptions,
  MediaConfigureSidecarVideoItem,
  MediaConfigureStoryBaseOptions,
  MediaConfigureStoryPhotoOptions,
  MediaConfigureStoryVideoOptions,
  MediaConfigureTimelineOptions,
  MediaConfigureTimelineVideoOptions,
  MediaConfigureToIgtvOptions,
  MediaLikeOrUnlikeOptions,
  MediaSaveOptions,
  StoryMusicQuestionResponse,
  StoryTextQuestionResponse,
  UnlikeRequestOptions,
} from '../types';
import Chance = require('chance');

@Service()
export class MediaRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}

  private static stringifyStoryStickers(form: MediaConfigureStoryBaseOptions) {
    const serialize = (obj: any | undefined) => {
      if (typeof obj !== 'undefined' && Array.isArray(obj) && obj.length > 0 && typeof obj[0] !== 'string') {
        return JSON.stringify(obj);
      }
      return obj;
    };
    form.story_hashtags = serialize(form.story_hashtags);
    form.story_locations = serialize(form.story_locations);
    form.reel_mentions = serialize(form.reel_mentions);
    form.story_polls = serialize(form.story_polls);
    form.story_sliders = serialize(form.story_sliders);
    form.story_questions = serialize(form.story_questions);
    form.story_countdowns = serialize(form.story_countdowns);
    form.attached_media = serialize(form.attached_media);
    form.story_cta = serialize(form.story_cta);
    form.story_chats = serialize(form.story_chats);
    form.story_quizs = serialize(form.story_quizs);
  }

  public async info(mediaId: string): Promise<MediaInfoResponseRootObject> {
    return this.http.body<MediaInfoResponseRootObject>({
      url: `/api/v1/media/${mediaId}/info/`,
      method: 'GET',
      allowGetBody: true,
      form: this.http.sign({
        igtv_feed_preview: false,
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async editMedia({
    mediaId,
    captionText,
  }: {
    mediaId: string;
    captionText: string;
  }): Promise<MediaEditResponseRootObject> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/edit_media/`,
      method: 'POST',
      form: this.http.sign({
        igtv_feed_preview: false,
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        caption_text: captionText,
      }),
    });
  }

  public async delete({
    mediaId,
    mediaType = 'PHOTO',
  }: {
    mediaId: string;
    mediaType?: 'PHOTO' | 'VIDEO' | 'CAROUSEL';
  }) {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/delete/`,
      method: 'POST',
      searchParams: {
        media_type: mediaType,
      },
      form: this.http.sign({
        igtv_feed_preview: false,
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async like(options: LikeRequestOptions) {
    return this.likeAction({
      action: 'like',
      ...options,
    });
  }

  public async unlike(options: UnlikeRequestOptions) {
    return this.likeAction({
      action: 'unlike',
      ...options,
    });
  }

  /**
   * Normally, this is requested before each comment is sent to ensure it isn't spam or hateful
   * @param commentText
   * @param mediaId - The mediaId of the post
   */
  public async checkOffensiveComment(
    commentText: string,
    mediaId?: string,
  ): Promise<MediaRepositoryCheckOffensiveCommentResponseRootObject> {
    return this.http.body<MediaRepositoryCheckOffensiveCommentResponseRootObject>({
      url: '/api/v1/media/comment/check_offensive_comment/',
      method: 'POST',
      form: this.http.sign({
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        comment_text: commentText,
      }),
    });
  }

  public async commentsBulkDelete(mediaId: string, commentIds: string[]): Promise<StatusResponse> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/comment/bulk_delete/`,
      method: 'POST',
      form: this.http.sign({
        comment_ids_to_delete: commentIds.join(','),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async comment({
    mediaId,
    text,
    replyToCommentId,
    module = 'self_comments_v2',
  }: {
    mediaId: string;
    text: string;
    replyToCommentId?: string;
    module?: string;
  }) {
    const { body } = await this.http.full<MediaRepositoryCommentResponse>({
      url: `/api/v1/media/${mediaId}/comment/`,
      method: 'POST',
      form: this.http.sign({
        user_breadcrumb: this.http.userBreadcrumb(text.length),
        idempotence_token: new Chance().guid(),
        _csrftoken: this.state.cookies.csrfToken,
        radio_type: this.state.device.radioType,
        _uid: this.state.cookies.userId,
        device_id: this.state.device.id,
        _uuid: this.state.device.uuid,
        comment_text: text,
        containermodule: module,
        replied_to_comment_id: replyToCommentId,
      }),
    });
    return body.comment;
  }

  async commentsDisable(mediaId: string | number) {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/disable_comments/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  async commentsEnable(mediaId: string | number) {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/enable_comments/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async likers(id: string): Promise<MediaRepositoryLikersResponseRootObject> {
    return this.http.body<MediaRepositoryLikersResponseRootObject>({
      url: `/api/v1/media/${id}/likers/`,
    });
  }

  public async blocked() {
    const { body } = await this.http.full<MediaRepositoryBlockedResponse>({
      url: `/api/v1/media/blocked/`,
    });
    return body.media_ids;
  }

  public async uploadFinish(options: {
    upload_id: string;
    source_type: string;
    video?: {
      length: number;
      clips?: Array<{ length: number; source_type: string }>;
      poster_frame_index?: number;
      audio_muted?: boolean;
    };
  }) {
    if (options.video) {
      options.video = defaultsDeep(options.video, {
        clips: [{ length: options.video.length, source_type: options.source_type }],
        poster_frame_index: 0,
        audio_muted: false,
      });
    }
    return this.http.body({
      url: '/api/v1/media/upload_finish/',
      method: 'POST',
      headers: {
        retry_context: JSON.stringify({ num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 }),
      },
      form: this.http.sign({
        timezone_offset: this.state.device.timezoneOffset,
        _csrftoken: this.state.cookies.csrfToken,
        source_type: options.source_type,
        _uid: this.state.cookies.userId,
        device_id: this.state.device.id,
        _uuid: this.state.device.uuid,
        upload_id: options.upload_id,
        device: this.state.device.payload,
        ...options.video,
      }),
      searchParams: options.video ? { video: '1' } : {},
    });
  }

  /**
   * Configures an upload (indicated by {upload_id} in the options) for the timeline
   * @param options - configuration-options
   */
  public async configure(options: MediaConfigureTimelineOptions): Promise<MediaRepositoryConfigureResponseRootObject> {
    const devicePayload = this.state.device.payload;
    const now = DateTime.local().toFormat('yyyy:mm:dd HH:mm:ss');
    const width = options.width || 1520;
    const height = options.height || 2048;

    const form = this.applyConfigureDefaults<MediaConfigureTimelineOptions>(options, {
      width,
      height,

      upload_id: Date.now().toString(),
      timezone_offset: this.state.device.timezoneOffset,
      date_time_original: now,
      date_time_digitalized: now,
      caption: '',
      source_type: '4',
      media_folder: 'Camera',
      edits: {
        crop_original_size: [width, height],
        crop_center: [0.0, -0.0],
        crop_zoom: 1.0,
      },

      // needed?!
      camera_model: devicePayload.model,
      scene_capture_type: 'standard',
      device_id: this.state.device.id,
      creation_logger_session_id: this.state.clientSessionId,
      software: '1',
      camera_make: devicePayload.manufacturer,
    });
    if (typeof form.usertags !== 'undefined') {
      form.usertags = JSON.stringify(form.usertags);
    }
    if (typeof form.location !== 'undefined') {
      form.location = JSON.stringify(form.location);
    }

    return this.http.body<MediaRepositoryConfigureResponseRootObject>({
      url: '/api/v1/media/configure/',
      method: 'POST',
      form: this.http.sign(form),
    });
  }

  public async configureVideo(options: MediaConfigureTimelineVideoOptions) {
    const now = DateTime.local().toFormat('yyyy:mm:dd HH:mm:ss');

    const form = this.applyConfigureDefaults<MediaConfigureTimelineVideoOptions>(options, {
      width: options.width,
      height: options.height,

      upload_id: Date.now().toString(),
      timezone_offset: this.state.device.timezoneOffset,
      date_time_original: now,
      caption: '',
      source_type: '4',
      device_id: this.state.device.id,
      filter_type: '0',
      audio_muted: false,
      poster_frame_index: 0,
    });

    if (typeof form.usertags !== 'undefined') {
      form.usertags = JSON.stringify(form.usertags);
    }
    if (typeof form.location !== 'undefined') {
      form.location = JSON.stringify(form.location);
    }

    return this.http.body<MediaRepositoryConfigureResponseRootObject>({
      url: '/api/v1/media/configure/',
      method: 'POST',
      searchParams: {
        video: '1',
      },
      form: this.http.sign(form),
    });
  }

  public async configureToStory(options: MediaConfigureStoryPhotoOptions) {
    const now = Date.now();
    const width = options.width || 1520;
    const height = options.height || 2048;
    const form = this.applyConfigureDefaults<MediaConfigureStoryPhotoOptions>(options, {
      width,
      height,

      source_type: '3',
      configure_mode: '1',
      client_shared_at: now.toString(),
      edits: {
        crop_original_size: [width, height],
        crop_center: [0.0, -0.0],
        crop_zoom: 1.0,
      },
    });
    // make sure source_type = 3
    form.source_type = '3';

    if (form.configure_mode === '1') {
      MediaRepository.stringifyStoryStickers(form);
    } else if (form.configure_mode === '2') {
      if (typeof form.recipient_users !== 'string') {
        form.recipient_users = JSON.stringify(form.recipient_users ? [form.recipient_users.map(x => Number(x))] : []);
      }
      form.thread_ids = JSON.stringify(form.thread_ids || []);
    }

    return this.http.body({
      url: '/api/v1/media/configure_to_story/',
      method: 'POST',
      form: this.http.sign(form),
    });
  }

  public async configureToStoryVideo(options: MediaConfigureStoryVideoOptions) {
    const now = Date.now();
    const devicePayload = this.state.device.payload;
    const form = defaultsDeep(options, {
      supported_capabilities_new: JSON.stringify(this.state.application.SUPPORTED_CAPABILITIES),
      timezone_offset: '0',
      _csrftoken: this.state.cookies.csrfToken,
      client_shared_at: now.toString(),
      configure_mode: '1',
      source_type: '3',
      video_result: '',
      _uid: this.state.cookies.userId,
      date_time_original: new Date().toISOString().replace(/[-:]/g, ''),
      device_id: this.state.device.id,
      _uuid: this.state.device.uuid,
      device: devicePayload,
      clips: [
        {
          length: options.length,
          source_type: '3',
        },
      ],
      extra: {
        source_width: options.width,
        source_height: options.height,
      },
      audio_muted: false,
      poster_frame_index: 0,
    });
    // make sure source_type = 3
    form.source_type = '3';

    if (form.configure_mode === '1') {
      MediaRepository.stringifyStoryStickers(form);
    } else if (form.configure_mode === '2') {
      if (typeof form.recipient_users !== 'string') {
        form.recipient_users = JSON.stringify(
          form.recipient_users ? [form.recipient_users.map((x: string) => Number(x))] : [],
        );
      }
      form.thread_ids = JSON.stringify(form.thread_ids || []);
    }
    return this.http.body({
      url: '/api/v1/media/configure_to_story/',
      method: 'POST',
      searchParams: {
        video: '1',
      },
      form: this.http.sign(form),
    });
  }

  public async configureSidecar(options: MediaConfigureSidecarOptions) {
    const isVideo = (arg: MediaConfigureSidecarItem): arg is MediaConfigureSidecarVideoItem =>
      (arg as MediaConfigureSidecarVideoItem).length !== undefined;

    const devicePayload = this.state.device.payload;
    const sidecarId = options.upload_id || Date.now().toString();
    const now = DateTime.local().toFormat('yyyy:mm:dd HH:mm:ss');
    options = defaultsDeep(options, {
      _csrftoken: this.state.cookies.csrfToken,
      _uid: this.state.cookies.userId,
      _uuid: this.state.device.uuid,
      timezone_offset: '0',
      source_type: '4',
      device_id: this.state.device.id,
      caption: '',
      client_sidecar_id: sidecarId,
      upload_id: sidecarId,
      device: devicePayload,
    });
    options.children_metadata = options.children_metadata.map(item => {
      const { width, height } = item;
      item = defaultsDeep(item, {
        timezone_offset: '0',
        caption: null,
        source_type: '4',
        extra: { source_width: width, source_height: height },
        edits: { crop_original_size: [width, height], crop_center: [0.0, -0.0], crop_zoom: 1.0 },
        device: devicePayload,
      });
      if (typeof item.extra !== 'string') {
        item.extra = JSON.stringify(item.extra);
      }
      if (typeof item.edits !== 'string') {
        item.edits = JSON.stringify(item.edits);
      }
      if (typeof item.device !== 'string') {
        item.device = JSON.stringify(item.device);
      }
      if (item.usertags && typeof item.usertags !== 'string') {
        item.usertags = JSON.stringify(item.usertags);
      }
      if (isVideo(item)) {
        item = defaultsDeep(item, {
          filter_type: '0',
          video_result: '',
          date_time_original: now,
          audio_muted: 'false',
          clips: [{ length: item.length, source_type: '4' }],
          poster_frame_index: '0',
        });
        const clips = item as MediaConfigureSidecarVideoItem;
        if (typeof clips !== 'string') {
          (item as MediaConfigureSidecarVideoItem).clips = JSON.stringify(clips);
        }
      }
      return item;
    });

    if (typeof options.location !== 'string') {
      options.location = JSON.stringify(options.location);
    }

    return this.http.body({
      url: '/api/v1/media/configure_sidecar/',
      method: 'POST',
      form: this.http.sign(options),
    });
  }

  public async configureToIgtv(options: MediaConfigureToIgtvOptions) {
    const form: MediaConfigureToIgtvOptions = defaultsDeep(options, {
      caption: '',
      date_time_original: new Date().toISOString().replace(/[-:]/g, ''),
      igtv_share_preview_to_feed: '0',
      clips: [
        {
          length: options.length,
          source_type: options.source_type || '4',
        },
      ],
      audio_muted: false,
      poster_frame_index: 0,
      filter_type: '0',
      timezone_offset: this.state.device.timezoneOffset,
      media_folder: options.source_type !== '4' ? 'Camera' : undefined,
      source_type: '4',
      device: this.state.device.payload,
      retryContext: { num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 },
    });
    const retryContext = options.retryContext;
    delete form.retryContext;
    return this.http.body({
      url: '/api/v1/media/configure_to_igtv/',
      method: 'POST',
      searchParams: {
        video: '1',
      },
      headers: {
        is_igtv_video: '1',
        retry_context: JSON.stringify(retryContext),
      },
      form: this.http.sign({
        ...form,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async onlyMe(mediaId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/only_me/`,
      method: 'POST',
      form: this.http.sign({
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async undoOnlyMe(mediaId: string): Promise<StatusResponse> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/undo_only_me/`,
      method: 'POST',
      form: this.http.sign({
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  async seen(
    reels: {
      [item: string]: [string];
    },
    module: IgAppModule = 'feed_timeline',
  ): Promise<StatusResponse> {
    return this.http.body<StatusResponse>({
      url: `/api/v2/media/seen/`,
      method: 'POST',
      searchParams: {
        reel: 1,
        live_vod: 0,
      },
      // TODO: gzip
      form: this.http.sign({
        reels,
        container_module: module,
        reel_media_skipped: [],
        live_vods: [],
        live_vods_skipped: [],
        nuxes: [],
        nuxes_skipped: [],
        _uuid: this.state.device.uuid,
        _uid: this.state.cookies.userId,
        _csrftoken: this.state.cookies.csrfToken,
        device_id: this.state.device.id,
      }),
    });
  }

  /**
   * Last update: 5. Apr. 2020
   * @param {string | MediaSaveOptions} userOptions or id
   * @returns {Promise<any>}
   */
  public save(userOptions: string | MediaSaveOptions) {
    const options: MediaSaveOptions = typeof userOptions === 'string' ? { mediaId: userOptions } : userOptions;
    return this.http.body({
      url: `/api/v1/media/${options.mediaId}/save/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.extractUserId(),
        __uuid: this.state.device.uuid,
        radio_type: this.state.device.radioType,
        module_name: options.moduleName ?? 'feed_timeline',
        removed_collection_ids: options.removedCollections ? JSON.stringify(options.removedCollections) : void 0,
        added_collection_ids: options.addedCollections ? JSON.stringify(options.addedCollections) : void 0,
      }),
    });
  }

  async unsave(mediaId: string) {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/unsave/`,
      method: 'POST',
    });
  }

  public async storyPollVote(
    mediaId: string,
    pollId: string | number,
    vote: '0' | '1',
  ): Promise<MediaUpdatedMediaResponseRootObject> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/${pollId}/story_poll_vote/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        radio_type: this.state.device.radioType,
        _uid: this.state.cookies.userId,
        vote,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async storyQuestionResponse(
    mediaId: string,
    questionId: string | number,
    options: StoryTextQuestionResponse | StoryMusicQuestionResponse,
  ): Promise<StatusResponse> {
    const chance = new Chance();
    // @ts-ignore
    if (typeof options.response === 'undefined') {
      options = defaultsDeep(options, { music_browse_session_id: chance.guid({ version: 4 }) });
    }

    return this.http.body({
      url: `/api/v1/media/${mediaId}/${questionId}/story_question_response/`,
      method: 'POST',
      form: this.http.sign({
        client_context: chance.guid({ version: 4 }),
        mutation_token: chance.guid({ version: 4 }),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        ...options,
      }),
    });
  }

  public async storySliderVote(
    mediaId: string,
    sliderId: string | number,
    vote: number,
  ): Promise<MediaUpdatedMediaResponseRootObject> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/${sliderId}/story_slider_vote/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        vote: vote.toFixed(8),
      }),
    });
  }

  /**
   * Answers a story quiz
   * @param mediaId storyId
   * @param quizId id of the quiz
   * @param answer index (string is only for compatibility)
   */
  public async storyQuizAnswer(
    mediaId: string,
    quizId: string | number,
    answer: '0' | '1' | '2' | '3' | string,
  ): Promise<MediaUpdatedMediaResponseRootObject> {
    return this.http.body({
      url: `/api/v1/media/${mediaId}/${quizId}/story_quiz_answer/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
        answer,
      }),
    });
  }

  public commentLike(options: MediaCommentLikeOptions): Promise<StatusResponse> {
    return this.updateCommentLike({
      ...options,
      unlike: false,
    });
  }

  public commentUnlike(options: MediaCommentLikeOptions): Promise<StatusResponse> {
    return this.updateCommentLike({
      ...options,
      unlike: true,
    });
  }

  private async likeAction(options: MediaLikeOrUnlikeOptions) {
    const signedFormData = this.http.sign({
      module_name: options.moduleInfo.module_name,
      media_id: options.mediaId,
      _csrftoken: this.state.cookies.csrfToken,
      ...omit(options.moduleInfo, 'module_name'),
      radio_type: this.state.device.radioType,
      _uid: this.state.cookies.userId,
      device_id: this.state.device.id,
      _uuid: this.state.device.uuid,
    });

    return this.http.body({
      url: `/api/v1/media/${options.mediaId}/${options.action}/`,
      method: 'POST',
      form: {
        ...signedFormData,
        d: options.d,
      },
    });
  }

  /**
   * Adds default values to the MediaConfigureOptions
   * @param options - user submitted options
   * @param defaults - default values
   */
  private applyConfigureDefaults<T extends MediaConfigureOptions>(options: T, defaults: Partial<T>): T {
    const width = options.width || 1520;
    const height = options.height || 2048;
    const devicePayload = this.state.device.payload;
    return defaultsDeep(options, {
      _csrftoken: this.state.cookies.csrfToken,
      _uid: this.state.cookies.userId,
      _uuid: this.state.device.uuid,
      device: devicePayload,
      extra: { source_width: width, source_height: height },

      ...defaults,
    });
  }

  /**
   * Last update: 5. Apr. 2020
   * @param {MediaCommentLikeOptions} options
   * @returns {Promise<StatusResponse>}
   */
  private updateCommentLike(options: MediaCommentLikeOptions): Promise<StatusResponse> {
    return this.http.body({
      url: `/api/v1/media/${options.mediaId}/comment_${options.unlike ? 'unlike' : 'like'}/`,
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
        _uid: this.state.extractUserId(),
        inventory_source: options.inventorySource ?? 'media_or_ad',
        is_carousel_bumped_post: (options.isCarouselBumpedPost ?? false).toString(),
        container_module: options.containerModule ?? 'comments_v2_feed_timeline',
        feed_position: options.feedPosition ?? void 0,
        carousel_index: options.carouselIndex ?? void 0,
      }),
    });
  }
}
