import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import {
  FbsearchRepositoryPlacesResponseRootObject,
  FbsearchRepositoryTopsearchFlatResponseRootObject,
} from '../responses';

@Service()
export class FbsearchRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  async suggestedSearches(type: 'blended' | 'users' | 'hashtags' | 'places') {
    const { body } = await this.http.full({
      url: '/api/v1/fbsearch/suggested_searches/',
      searchParams: {
        type,
      },
    });
    return body;
  }
  async recentSearches() {
    const { body } = await this.http.full({
      url: '/api/v1/fbsearch/recent_searches/',
    });
    return body;
  }

  async topsearchFlat(query: string): Promise<FbsearchRepositoryTopsearchFlatResponseRootObject> {
    const { body } = await this.http.full<FbsearchRepositoryTopsearchFlatResponseRootObject>({
      url: '/api/v1/fbsearch/topsearch_flat/',
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        count: 30,
        query,
        context: 'blended',
      },
    });
    return body;
  }
  async places(query: string) {
    const { body } = await this.http.full<FbsearchRepositoryPlacesResponseRootObject>({
      url: '/api/v1/fbsearch/places/',
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        count: 30,
        query,
      },
    });
    return body;
  }
}
