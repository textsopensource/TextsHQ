import { Service } from 'typedi';
import { AndroidHttp, RequestDefaults } from '../core/android.http';
import { StatusResponse } from '../responses';

@Service()
export class ClipsRepository {
  constructor(private http: AndroidHttp) {}

  public writeSeenState(impressions: string | string[]): Promise<StatusResponse> {
    return this.http.body({
      url: '/api/v1/clips/write_seen_state/',
      form: this.http.sign({
        ...this.http.getDefaults(RequestDefaults.Base),
        impressions: Array.isArray(impressions) ? impressions : [impressions],
      }),
    });
  }
}
