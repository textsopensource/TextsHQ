import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

@Service()
export class ZrRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public tokenResult() {
    return this.http.full({
      url: '/api/v1/zr/token/result/',
      searchParams: {
        device_id: this.state.device.id,
        token_hash: '',
        custom_device_id: this.state.device.uuid,
        fetch_reason: 'token_expired',
      },
    });
  }
}
