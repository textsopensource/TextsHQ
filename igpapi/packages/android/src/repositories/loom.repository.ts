import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

@Service()
export class LoomRepository {
  constructor(private http: AndroidHttp) {}
  public async fetchConfig() {
    const { body } = await this.http.full({
      url: '/api/v1/loom/fetch_config/',
    });
    return body;
  }
}
