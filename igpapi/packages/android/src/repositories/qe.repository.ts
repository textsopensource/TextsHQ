import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { QeSyncResponseRootObject } from '../responses';

@Service()
export class QeRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public syncExperiments() {
    return this.sync(this.state.application.EXPERIMENTS);
  }
  public async syncLoginExperiments() {
    return this.sync(this.state.application.LOGIN_EXPERIMENTS);
  }
  public async sync(experiments: string) {
    let data;
    try {
      const uid = this.state.cookies.userId;
      data = {
        _csrftoken: this.state.cookies.csrfToken,
        id: uid,
        _uid: uid,
        _uuid: this.state.device.uuid,
      };
    } catch {
      data = {
        id: this.state.device.uuid,
      };
    }
    data = Object.assign(data, { experiments });
    const { body } = await this.http.full<QeSyncResponseRootObject>({
      method: 'POST',
      url: '/api/v1/qe/sync/',
      headers: {
        'X-DEVICE-ID': this.state.device.uuid,
      },
      form: this.http.sign(data),
    });
    this.state.experiments.update(body);
    return body;
  }
}
