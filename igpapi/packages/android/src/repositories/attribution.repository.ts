import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

@Service()
export class AttributionRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async logAttribution() {
    const { body } = await this.http.full({
      method: 'POST',
      url: '/api/v1/attribution/log_attribution/',
      form: this.http.sign({
        adid: this.state.device.adid,
      }),
    });
    return body;
  }
  // This method participates in post-login flow
  // And it crashes in official IG app, so we just catch it and return error
  public async logResurrectAttribution() {
    try {
      const { body } = await this.http.full({
        method: 'POST',
        url: '/api/v1/attribution/log_resurrect_attribution/',
        form: this.http.sign({
          _csrftoken: this.state.cookies.csrfToken,
          _uid: this.state.cookies.userId,
          adid: this.state.device.adid,
          _uuid: this.state.device.uuid,
        }),
      });
      return body;
    } catch (e) {
      return e;
    }
  }
}
