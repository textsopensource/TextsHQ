import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { TagRepositorySearchResponseRootObject } from '../responses';

@Service()
export class TagRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async search(q: string) {
    const { body } = await this.http.full<TagRepositorySearchResponseRootObject>({
      url: '/api/v1/tags/search/',
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        q,
        count: 30,
      },
    });
    return body;
  }

  public async section(q: string, tab: string) {
    const { body } = await this.http.full<TagRepositorySearchResponseRootObject>({
      url: `/api/v1/tags/${encodeURI(q)}/sections/`,
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        tab: tab,
        count: 30,
      },
    });
    return body;
  }
}
