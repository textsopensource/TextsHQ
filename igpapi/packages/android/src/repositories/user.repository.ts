import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import {
  UserRepositoryInfoResponseRootObject,
  UserRepositoryInfoResponseUser,
  UserRepositorySearchResponseRootObject,
  UserRepositorySearchResponseUsersItem,
} from '../responses';
import { IgExactUserNotFoundError } from '../errors';
import { UserLookupOptions } from '../types/user.lookup.options';
import { defaults } from 'lodash';
import Chance from 'chance';

@Service()
export class UserRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  async info(id: string | number): Promise<UserRepositoryInfoResponseUser> {
    const { body } = await this.http.full<UserRepositoryInfoResponseRootObject>({
      url: `/api/v1/users/${id}/info/`,
    });
    return body.user;
  }

  async arlinkDownloadInfo() {
    const { body } = await this.http.full<any>({
      url: `/api/v1/users/arlink_download_info/`,
      searchParams: {
        version_override: '2.0.2',
      },
    });
    return body.user;
  }

  async search(username: string): Promise<UserRepositorySearchResponseRootObject> {
    const { body } = await this.http.full<UserRepositorySearchResponseRootObject>({
      url: `/api/v1/users/search/`,
      searchParams: {
        timezone_offset: this.state.device.timezoneOffset,
        q: username,
        count: 30,
      },
    });
    return body;
  }

  async searchExact(username: string): Promise<UserRepositorySearchResponseUsersItem> {
    username = username.toLowerCase();
    const result = await this.search(username);
    const users = result.users;
    const account = users.find(user => user.username === username);
    if (!account) {
      throw new IgExactUserNotFoundError();
    }
    return account;
  }

  async accountDetails(id?: string | number) {
    id = id || this.state.cookies.userId;
    const { body } = await this.http.full({
      url: `/api/v1/users/${id}/account_details/`,
    });
    return body;
  }

  async formerUsernames(id?: string | number) {
    id = id || this.state.cookies.userId;
    const { body } = await this.http.full({
      url: `/api/v1/users/${id}/former_usernames/`,
    });
    return body;
  }

  async sharedFollowerAccounts(id: string | number) {
    const { body } = await this.http.full({
      url: `/api/v1/users/${id}/shared_follower_accounts/`,
    });
    return body;
  }

  async flagUser(id: string | number) {
    const { body } = await this.http.full({
      url: `/api/v1/users/${id}/flag_user/`,
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        reason_id: 1,
        user_id: id,
        source_name: 'profile',
        is_spam: true,
      },
    });
    return body;
  }

  async usernameInfo(username: string): Promise<UserRepositoryInfoResponseRootObject> {
    return this.http.body({
      url: `/api/v1/users/${username}/usernameinfo/`,
    });
  }

  async getIdByUsername(username: string): Promise<number> {
    const { user } = await this.usernameInfo(username);
    return user.pk;
  }

  public async lookup(options: UserLookupOptions) {
    options = defaults(options, {
      waterfallId: new Chance().guid({ version: 4 }),
      directlySignIn: true,
      countryCodes: [{ country_code: '1', source: ['default'] }],
    });
    const { body } = await this.http.full({
      url: '/api/v1/users/lookup/',
      method: 'POST',
      form: this.http.sign({
        country_codes: JSON.stringify(options.countryCodes),
        _csrftoken: this.state.cookies.csrfToken,
        q: options.query,
        guid: this.state.device.uuid,
        device_id: this.state.device.id,
        waterfall_id: options.waterfallId,
        directly_sign_in: options.directlySignIn.toString(),
      }),
    });
    return body;
  }
}
