import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import {
  DirectRepositoryCreateGroupThreadResponseRootObject,
  DirectRepositoryGetPresenceResponseRootObject,
  DirectRepositoryRankedRecipientsResponseRootObject,
} from '../responses';

@Service()
export class DirectRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public createGroupThread(recipientUsers: string[], threadTitle: string) {
    return this.http.body<DirectRepositoryCreateGroupThreadResponseRootObject>({
      url: '/api/v1/direct_v2/create_group_thread/',
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
        _uid: this.state.cookies.userId,
        recipient_users: JSON.stringify(recipientUsers),
        thread_title: threadTitle,
      }),
    });
  }

  /**
   * Get user suggestions for direct messages
   * @param {"raven" | "reshare"} mode
   * @param {string} query
   * @returns {Promise<DirectRepositoryRankedRecipientsResponseRootObject>}
   */
  public async rankedRecipients(
    mode: 'raven' | 'reshare' = 'raven',
    query = '',
  ): Promise<DirectRepositoryRankedRecipientsResponseRootObject> {
    const { body } = await this.http.full<DirectRepositoryRankedRecipientsResponseRootObject>({
      url: '/api/v1/direct_v2/ranked_recipients/',
      method: 'GET',
      searchParams: {
        mode,
        query,
        show_threads: true,
      },
    });
    return body;
  }

  public async getPresence(): Promise<DirectRepositoryGetPresenceResponseRootObject> {
    const { body } = await this.http.full<DirectRepositoryGetPresenceResponseRootObject>({
      url: '/api/v1/direct_v2/get_presence/',
      method: 'GET',
    });
    return body;
  }
}
