import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { NewsRepositoryInboxResponseRootObject } from '../responses';

@Service()
export class NewsRepository {
  constructor(private http: AndroidHttp) {}
  public async inbox(): Promise<NewsRepositoryInboxResponseRootObject> {
    const { body } = await this.http.full<NewsRepositoryInboxResponseRootObject>({
      url: '/api/v1/news/inbox',
      method: 'GET',
    });
    return body;
  }
}
