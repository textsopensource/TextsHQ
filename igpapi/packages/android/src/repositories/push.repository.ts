import { AndroidHttp } from '../core/android.http';
import { Service } from 'typedi';
import { Chance } from 'chance';
import { StatusResponse } from '../responses';
import { AndroidState } from '../core/android.state';

@Service()
export class PushRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}

  public register(token: string): Promise<StatusResponse> {
    return this.http.body<StatusResponse>({
      url: `/api/v1/push/register/`,
      method: 'POST',
      form: {
        device_type: 'android_mqtt',
        is_main_push_channel: true,
        device_sub_type: 2,
        device_token: token,
        _csrftoken: this.state.cookies.csrfToken,
        guid: this.state.device.uuid,
        uuid: this.state.device.uuid,
        users: this.state.cookies.userId,
        family_device_id: new Chance().guid({ version: 4 }),
      },
    });
  }
}
