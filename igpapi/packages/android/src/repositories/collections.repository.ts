import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { IgAppModule } from '../types';
import { CollectionsCreateResponseRootObject } from '../responses';

@Service()
export class CollectionsRepository {
  constructor(private http: AndroidHttp) {}

  public create(options: {
    name: string;
    moduleName?: IgAppModule;
    added?: string[];
    collectionVisibility?: boolean;
    coverMediaId?: string;
  }): Promise<CollectionsCreateResponseRootObject> {
    return this.http.body({
      url: '/api/v1/collections/create/',
      form: this.http.sign({
        module_name: options.moduleName ?? 'feed_timeline',
        added_media_ids: options.added ?? void 0,
        collection_visibility: options.collectionVisibility ? '1' : '0',
        name: options.name,
        cover_media_id: options.coverMediaId,
      }),
    });
  }
}
