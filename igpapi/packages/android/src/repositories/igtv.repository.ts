import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { IgtvWriteSeenStateOptions } from '../types';
import { defaults } from 'lodash';
import { StatusResponse, IgtvSearchResponseRootObject } from '../responses';
import Chance from 'chance';

@Service()
export class IgtvRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public writeSeenState(options: IgtvWriteSeenStateOptions): Promise<StatusResponse> {
    return this.http.body<StatusResponse>({
      url: '/api/v1/igtv/write_seen_state/',
      method: 'POST',
      form: this.http.sign({
        seen_state: JSON.stringify(defaults(options, { impressions: {}, grid_impressions: [] })),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
  }

  public async search(query = ''): Promise<IgtvSearchResponseRootObject> {
    const { body } = await this.http.full<IgtvSearchResponseRootObject>({
      // this is the same method in the app
      url: `/api/v1/igtv/${query && query.length > 0 ? 'search' : 'suggested_searches'}/`,
      method: 'GET',
      searchParams: {
        query,
      },
    });
    return body;
  }

  public async allUserSeries(user: string | number, data: object = {}) {
    const { body } = await this.http.full({
      url: `/api/v1/igtv/series/all_user_series/${user}/`,
      method: 'GET',
      searchParams: this.http.sign(data),
    });
    return body;
  }

  public async createSeries(title: string, description = '') {
    const { body } = await this.http.full({
      url: `/api/v1/igtv/series/create/`,
      method: 'POST',
      form: this.http.sign({
        title,
        description,
        igtv_composer_session_id: new Chance().guid({ version: 4 }),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
    return body;
  }

  public async seriesAddEpisode(series: string | number, mediaId: string) {
    const { body } = await this.http.full({
      url: `/api/v1/igtv/series/${series}/add_episode/`,
      method: 'POST',
      form: {
        media_id: mediaId,
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
      },
    });
    return body;
  }
}
