import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

@Service()
export class StatusRepository {
  constructor(private http: AndroidHttp) {}
  public async getViewableStatuses() {
    const { body } = await this.http.full({
      url: '/api/v1/status/get_viewable_statuses/',
      method: 'GET',
    });
    return body;
  }
}
