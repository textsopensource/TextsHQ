import { random } from 'lodash';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import Chance = require('chance');
import { UploadRepositoryPhotoResponseRootObject } from '../responses';
import {
  UploadRetryContext,
  UploadVideoOptions,
  UploadPhotoOptions,
  UploadVideoSegmentInitOptions,
  UploadVideoSegmentTransferOptions,
} from '../types';
import debug from 'debug';
import { HeaderStatusCheckerStrategy } from '@textshq/core';

@Service()
export class UploadRepository {
  constructor(private http: AndroidHttp) {}
  private static uploadDebug = debug('ig:upload');
  private chance = new Chance();

  public async photo(options: UploadPhotoOptions): Promise<UploadRepositoryPhotoResponseRootObject> {
    const uploadId = options.uploadId || Date.now();
    const name = `${uploadId}_0_${random(1000000000, 9999999999)}`;
    const contentLength = options.file.byteLength;
    UploadRepository.uploadDebug(`Uploading ${options.file.byteLength}b as ${uploadId} (photo, jpeg)`);
    const { body } = await this.http.full<UploadRepositoryPhotoResponseRootObject>({
      url: `/rupload_igphoto/${name}`,
      method: 'POST',
      headers: {
        X_FB_PHOTO_WATERFALL_ID: options.waterfallId || this.chance.guid(),
        'X-Entity-Type': 'image/jpeg',
        Offset: '0',
        'X-Instagram-Rupload-Params': JSON.stringify(UploadRepository.createPhotoRuploadParams(options, uploadId)),
        'X-Entity-Name': name,
        'X-Entity-Length': contentLength.toString(),
        'Content-Type': 'application/octet-stream',
        'Content-Length': contentLength.toString(),
        'Accept-Encoding': 'gzip',
      },
      body: options.file,
    });
    return body;
  }

  /**
   * Uploads a video (container: mp4 with h264 and aac)
   */
  public async video(options: UploadVideoOptions) {
    const video = options.video;
    const uploadId = options.uploadId || Date.now();
    const name = options.uploadName || `${uploadId}_0_${random(1000000000, 9999999999)}`;
    const contentLength = video.byteLength;
    const waterfallId = options.waterfallId || this.chance.guid({ version: 4 });
    const ruploadParams = UploadRepository.createVideoRuploadParams(options, uploadId);

    UploadRepository.uploadDebug(
      `Uploading ${options.video.byteLength}b as ${uploadId} (video, mp4, not segmented). Info: ${JSON.stringify(
        ruploadParams,
      )}`,
    );
    const { body } = await this.http.full({
      url: `/rupload_igvideo/${name}`,
      method: 'POST',
      searchParams: {
        // target: this.state.extractCookieValue('rur'),
      },
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(ruploadParams),
        X_FB_VIDEO_WATERFALL_ID: waterfallId,
        'X-Entity-Type': 'video/mp4',
        Offset: options.offset?.toString() || '0',
        'X-Entity-Name': name,
        'X-Entity-Length': contentLength.toString(),
        'Content-Type': 'application/octet-stream',
        'Content-Length': contentLength.toString(),
        'Accept-Encoding': 'gzip',
      },
      body: video,
    });
    return body;
  }

  public async initVideo({ name, ruploadParams, waterfallId }: any): Promise<{ offset: number }> {
    UploadRepository.uploadDebug(`Initializing video upload: ${JSON.stringify(ruploadParams)}`);
    return this.http.body({
      url: `/rupload_igvideo/${name}`,
      method: 'GET',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(ruploadParams),
        X_FB_VIDEO_WATERFALL_ID: waterfallId,
        'X-Entity-Type': 'video/mp4',
        'Accept-Encoding': 'gzip',
      },
      statusCheckerStrategy: new HeaderStatusCheckerStrategy(),
    });
  }

  public async startSegmentedVideo(ruploadParams: any) {
    UploadRepository.uploadDebug(`Starting segmented video upload: ${JSON.stringify(ruploadParams)}`);
    return this.http.body<{ stream_id: string }>({
      url: `/rupload_igvideo/${this.chance.guid({ version: 4 })}`,
      searchParams: {
        segmented: true,
        phase: 'start',
      },
      method: 'POST',
      body: '',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(ruploadParams),
        'Accept-Encoding': 'gzip',
        'Content-Length': '0',
      },
    });
  }

  public async videoSegmentInit(options: UploadVideoSegmentInitOptions) {
    UploadRepository.uploadDebug(`Initializing segmented video upload: ${JSON.stringify(options)}`);
    return this.http.body<{ offset: number }>({
      url: `/rupload_igvideo/${options.transferId}`,
      method: 'GET',
      searchParams: {
        segmented: true,
        phase: 'transfer',
      },
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(options.ruploadParams),
        'Stream-Id': options.streamId,
        'Segment-Start-Offset': options.startOffset.toString(),
        X_FB_VIDEO_WATERFALL_ID: options.waterfallId,
        'Segment-Type': options.segmentType ?? '2',
        'Accept-Encoding': 'gzip',
      },
      statusCheckerStrategy: new HeaderStatusCheckerStrategy(),
    });
  }

  public async videoSegmentTransfer(options: UploadVideoSegmentTransferOptions) {
    UploadRepository.uploadDebug(
      `Transfering segmented video: ${options.segment.byteLength}b, stream position: ${options.startOffset}`,
    );
    const { body } = await this.http.full({
      url: `/rupload_igvideo/${options.transferId}`,
      searchParams: {
        segmented: true,
        phase: 'transfer',
      },
      method: 'POST',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(options.ruploadParams),
        'X-Entity-Length': options.segment.byteLength.toString(),
        'X-Entity-Name': options.transferId,
        'Stream-Id': options.streamId,
        'X-Entity-Type': 'video/mp4',
        'Segment-Start-Offset': options.startOffset.toString(),
        'Segment-Type': options.segmentType ?? '2',
        X_FB_VIDEO_WATERFALL_ID: options.waterfallId,
        // TODO: inspect offset
        Offset: '0',
        'Content-Length': options.segment.byteLength.toString(),
      },
      body: options.segment,
    });
    return body;
  }

  public async endSegmentedVideo({ ruploadParams, streamId }: any): Promise<any> {
    UploadRepository.uploadDebug(`Ending segmented video upload of ${streamId}`);
    const { body } = await this.http.full({
      url: `/rupload_igvideo/${this.chance.guid({ version: 4 })}`,
      searchParams: {
        segmented: true,
        phase: 'end',
      },
      method: 'POST',
      body: '',
      headers: {
        'X-Instagram-Rupload-Params': JSON.stringify(ruploadParams),
        'Accept-Encoding': 'gzip',
        'Content-Length': '0',
        'Stream-Id': streamId,
      },
    });
    return body;
  }

  private static createPhotoRuploadParams(options: UploadPhotoOptions, uploadId: number | string) {
    const ruploadParams: any = {
      retry_context: JSON.stringify({ num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 }),
      media_type: options.mediaType ?? '1',
      upload_id: uploadId.toString(),
      xsharing_user_ids: JSON.stringify([]),
      image_compression: JSON.stringify({ lib_name: 'moz', lib_version: '3.1.m', quality: '80' }),
    };
    if (options.isSidecar) {
      ruploadParams.is_sidecar = '1';
    }
    return ruploadParams;
  }

  public static createVideoRuploadParams(
    options: UploadVideoOptions,
    uploadId: number | string,
    retryContext?: UploadRetryContext,
  ) {
    const { duration, width, height, xSharingUserIds, contentTags, musicBurninParams } = options;
    const ruploadParams: any = {
      retry_context: JSON.stringify(
        retryContext || { num_step_auto_retry: 0, num_reupload: 0, num_step_manual_retry: 0 },
      ),
      media_type: options.mediaType || '2',
      xsharing_user_ids: JSON.stringify(xSharingUserIds ?? []),
      upload_id: uploadId.toString(),
      upload_media_height: height?.toString(),
      upload_media_width: width?.toString(),
      upload_media_duration_ms: duration.toString(),
      content_tags: contentTags,
      music_burnin_params: musicBurninParams ? JSON.stringify(musicBurninParams) : undefined,
      is_sidecar: options.isSidecar ? '1' : undefined,
      for_album: options.forAlbum ? '1' : undefined,
      direct_v2: options.isDirect ? '1' : undefined,
      for_direct_story: options.forDirectStory ? '1' : undefined,
      is_igtv_video: options.isIgtvVideo ? '1' : undefined,
      is_direct_voice: options.isDirectVoice ? '1' : undefined,
      is_clips_video: options.isClipsVideo ? '1' : undefined,
    };
    return ruploadParams;
  }
}
