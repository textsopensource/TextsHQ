import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import {
  HighlightsRepositoryCreateReelResponseRootObject,
  HighlightsRepositoryEditReelResponseRootObject,
  HighlightsRepositoryHighlightsTrayResponseRootObject,
  StatusResponse,
} from '../responses';
import { CreateHighlightsReelOptions, EditHighlightsReelOptions } from '../types';

@Service()
export class HighlightsRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async highlightsTray(userId: string | number): Promise<HighlightsRepositoryHighlightsTrayResponseRootObject> {
    const { body } = await this.http.full<HighlightsRepositoryHighlightsTrayResponseRootObject>({
      url: `/api/v1/highlights/${userId}/highlights_tray/`,
      method: 'GET',
      searchParams: {
        supported_capabilities_new: JSON.stringify(this.state.application.SUPPORTED_CAPABILITIES),
        phone_id: this.state.device.phoneId,
        battery_level: this.state.device.batteryLevel,
        is_charging: Number(this.state.device.isCharging),
        will_sound_on: 0,
      },
    });
    return body;
  }
  public async createReel(
    options: CreateHighlightsReelOptions,
  ): Promise<HighlightsRepositoryCreateReelResponseRootObject> {
    const { body } = await this.http.full<HighlightsRepositoryCreateReelResponseRootObject>({
      url: '/api/v1/highlights/create_reel/',
      method: 'POST',
      form: this.http.sign({
        supported_capabilities_new: JSON.stringify(this.state.application.SUPPORTED_CAPABILITIES),
        source: options.source || 'story_viewer_profile',
        creation_id: Date.now().toString(),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        cover: JSON.stringify({
          media_id: options.coverId || options.mediaIds[0],
        }),
        title: options.title,
        media_ids: JSON.stringify(options.mediaIds),
      }),
    });
    return body;
  }
  public async editReel(options: EditHighlightsReelOptions): Promise<HighlightsRepositoryEditReelResponseRootObject> {
    const { body } = await this.http.full<HighlightsRepositoryEditReelResponseRootObject>({
      url: `/api/v1/highlights/${options.highlightId}/edit_reel/`,
      method: 'POST',
      form: this.http.sign({
        supported_capabilities_new: JSON.stringify(this.state.application.SUPPORTED_CAPABILITIES),
        source: options.source || 'story_viewer_default',
        added_media_ids: JSON.stringify(options.added || []),
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
        cover: JSON.stringify({
          media_id: options.coverId,
        }),
        title: options.title,
        removed_media_ids: JSON.stringify(options.removed || []),
      }),
    });
    return body;
  }
  public async deleteReel(highlightId: string): Promise<StatusResponse> {
    const { body } = await this.http.full<StatusResponse>({
      url: `/api/v1/highlights/${highlightId}/delete_reel/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        _uid: this.state.cookies.userId,
        _uuid: this.state.device.uuid,
      }),
    });
    return body;
  }
}
