import { Service } from 'typedi';
import { AndroidHttp, RequestDefaults } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { FriendshipRepositoryChangeResponseRootObject, FriendshipRepositoryShowResponseRootObject } from '../responses';
import { IgAppModule } from '../types';

@Service()
export class FriendshipRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  async show(id: string | number) {
    const { body } = await this.http.full<FriendshipRepositoryShowResponseRootObject>({
      url: `/api/v1/friendships/show/${id}/`,
    });
    return body;
  }

  async showMany(userIds: string[] | number[]) {
    const { body } = await this.http.full<{ friendship_statuses: FriendshipRepositoryShowResponseRootObject[] }>({
      url: `/api/v1/friendships/show_many/`,
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        user_ids: userIds.join(),
        _uuid: this.state.device.uuid,
      },
    });
    return body.friendship_statuses;
  }

  async block(id: string | number, mediaIdAttribution?: string) {
    return this.change('block', id, mediaIdAttribution);
  }

  async unblock(id: string | number, mediaIdAttribution?: string) {
    return this.change('unblock', id, mediaIdAttribution);
  }

  /**
   * Follow a user
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async create(id: string | number, mediaIdAttribution?: string) {
    return this.change('create', id, mediaIdAttribution);
  }

  /**
   * Unfollow a user
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async destroy(id: string | number, mediaIdAttribution?: string) {
    return this.change('destroy', id, mediaIdAttribution);
  }

  async approve(id: string | number, mediaIdAttribution?: string) {
    return this.change('approve', id, mediaIdAttribution);
  }

  async deny(id: string | number, mediaIdAttribution?: string) {
    return this.change('ignore', id, mediaIdAttribution);
  }

  async removeFollower(id: string | number) {
    return this.change('remove_follower', id);
  }

  /**
   * Turn on story notifications
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async favoriteForStories(id: string | number, mediaIdAttribution?: string) {
    return this.change('favorite_for_stories', id, mediaIdAttribution);
  }

  /**
   * Turn off story notifications
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async unfavoriteForStories(id: string | number, mediaIdAttribution?: string) {
    return this.change('unfavorite_for_stories', id, mediaIdAttribution);
  }

  /**
   * Turn on post and story notifications
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async favorite(id: string | number, mediaIdAttribution?: string) {
    return this.change('favorite', id, mediaIdAttribution);
  }

  /**
   * Turn off ost and story notifications
   * @param {string | number} id
   * @param {string} mediaIdAttribution
   * @returns {Promise<FriendshipRepositoryChangeResponseFriendship_status>}
   */
  async unfavorite(id: string | number, mediaIdAttribution?: string) {
    return this.change('unfavorite', id, mediaIdAttribution);
  }

  private async change(action: string, id: string | number, mediaIdAttribution?: string) {
    const { body } = await this.http.full<FriendshipRepositoryChangeResponseRootObject>({
      url: `/api/v1/friendships/${action}/${id}/`,
      method: 'POST',
      form: this.http.sign({
        _csrftoken: this.state.cookies.csrfToken,
        user_id: id,
        radio_type: this.state.device.radioType,
        _uid: this.state.cookies.userId,
        device_id: this.state.device.id,
        _uuid: this.state.device.uuid,
        media_id_attribution: mediaIdAttribution,
      }),
    });
    return body.friendship_status;
  }

  /**
   * Hide Story/Reel from ReelsTray
   * @param {{userId: string | number; source?: string}} options
   * @returns {Promise<unknown>}
   */
  public blockFriendReel(options: { userId: string | number; source?: string }) {
    return this.changeFriendReelVisibility({
      ...options,
      unblock: false,
    });
  }

  /**
   * "Show" Story/Reel in ReelsTray
   * @param {{userId: string | number; source?: string}} options
   * @returns {Promise<unknown>}
   */
  public unblockFriendReel(options: { userId: string | number; source?: string }) {
    return this.changeFriendReelVisibility({
      ...options,
      unblock: true,
    });
  }

  private changeFriendReelVisibility(options: { userId: string | number; source?: string; unblock: boolean }) {
    return this.http.body({
      url: `/api/v1/friendships/${options.unblock ? 'unblock' : 'block'}_friend_reel/${options.userId}/`,
      form: this.http.sign({
        ...this.http.getDefaults(RequestDefaults.Base),
        source: options.source ?? 'profile',
      }),
    });
  }

  public mutePostsAndStoryFromFollow(options: { mediaId?: string; userId: string | number }) {
    return this.changeMutedStatus({
      ...options,
      unmute: false,
      mutePosts: true,
    });
  }

  public muteStoryFromFollow(options: { mediaId?: string; userId: string | number }) {
    return this.changeMutedStatus({
      ...options,
      unmute: false,
      mutePosts: false,
    });
  }

  public unmutePostsOrStoryFromFollow(options: { mediaId?: string; userId: string | number }) {
    return this.changeMutedStatus({
      ...options,
      unmute: true,
      mutePosts: false,
    });
  }

  private changeMutedStatus(options: {
    unmute: boolean;
    mediaId?: string;
    mutePosts: boolean;
    userId: string | number;
  }) {
    return this.http.full({
      url: `/api/v1/friendships/${options.unmute ? 'unmute' : 'mute'}_posts_or_story_from_follow/`,
      form: this.http.sign({
        ...this.http.getDefaults(RequestDefaults.Base),
        target_reel_author_id: options.userId.toString(),
        target_posts_author_id: options.mutePosts ? options.userId.toString() : void 0,
        media_id: options.mediaId,
      }),
    });
  }

  /**
   * Manage close-friends
   * @returns {Promise<unknown>}
   * @param options
   */
  public setBesties(options: {
    add?: Array<string | number>;
    remove?: Array<string | number>;
    module?: IgAppModule;
    source?: string;
    blockOnEmptyThreadCreation?: boolean;
  }) {
    return this.http.body({
      url: '/api/v1/friendships/set_besties/',
      form: this.http.sign({
        ...this.http.getDefaults(RequestDefaults.Base),
        add: options.add?.map(x => x.toString()) ?? [],
        remove: options.remove?.map(x => x.toString()) ?? [],
        module: options.module ?? 'CLOSE_FRIENDS_V2_LIST',
        source: options.source ?? 'audience_manager',
        block_on_empty_thread_creation: (options.blockOnEmptyThreadCreation ?? false).toString(),
      }),
    });
  }
}
