import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { MusicRepositoryGenresResponseRootObject, MusicRepositoryMoodsResponseRootObject } from '../responses';
import { IgAppModule } from '../types/common.types';
import { MusicRepositoryLyricsResponseRootObject } from '../responses/music.repository.lyrics.response';

@Service()
export class MusicRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async moods(product?: IgAppModule): Promise<MusicRepositoryMoodsResponseRootObject> {
    product = product || 'story_camera_music_overlay_post_capture';
    const { body } = await this.http.full<MusicRepositoryMoodsResponseRootObject>({
      url: '/api/v1/music/moods/',
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        product,
        _uuid: this.state.device.uuid,
        browse_session_id: this.state.clientSessionId,
      },
    });
    return body;
  }

  public async genres(product?: IgAppModule): Promise<MusicRepositoryGenresResponseRootObject> {
    product = product || 'story_camera_music_overlay_post_capture';
    const { body } = await this.http.full<MusicRepositoryGenresResponseRootObject>({
      url: '/api/v1/music/genres/',
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        product,
        _uuid: this.state.device.uuid,
        browse_session_id: this.state.clientSessionId,
      },
    });
    return body;
  }

  public async lyrics(trackId: number | string): Promise<MusicRepositoryLyricsResponseRootObject> {
    const { body } = await this.http.full<MusicRepositoryLyricsResponseRootObject>({
      url: `/api/v1/music/track/${trackId}/lyrics/`,
      method: 'GET',
    });
    return body;
  }
}
