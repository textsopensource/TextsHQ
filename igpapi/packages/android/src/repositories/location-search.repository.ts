import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { LocationRepositorySearchResponseRootObject } from '../responses';

@Service()
export class LocationSearch {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async index(
    latitude: number,
    longitude: number,
    searchQuery?: string,
  ): Promise<LocationRepositorySearchResponseRootObject> {
    const queryOrTimestamp =
      typeof searchQuery === 'undefined' ? { timestamp: Date.now() } : { search_query: searchQuery };
    const { body } = await this.http.full<LocationRepositorySearchResponseRootObject>({
      url: '/api/v1/location_search/',
      method: 'GET',
      searchParams: {
        _uuid: this.state.device.uuid,
        _uid: this.state.cookies.userId,
        _csrftoken: this.state.cookies.csrfToken,
        rank_token: '',
        latitude,
        longitude,
        ...queryOrTimestamp,
      },
    });
    return body;
  }
}
