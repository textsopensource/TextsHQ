import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { RestrictActionRepositoryRestrictResponseRootObject } from '../responses';

@Service()
export class RestrictActionRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}

  /**
   * Shadow-Block user
   * @param {number | string} targetUserId
   * @returns {Promise<RestrictActionRepositoryRestrictResponseRootObject>}
   */
  public async restrict(targetUserId: number | string): Promise<RestrictActionRepositoryRestrictResponseRootObject> {
    const { body } = await this.http.full<RestrictActionRepositoryRestrictResponseRootObject>({
      url: '/api/v1/restrict_action/restrict/',
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
        target_user_id: targetUserId,
      },
    });
    return body;
  }

  /**
   * Shadow-Unblock user
   * @param {number | string} targetUserId
   * @returns {Promise<RestrictActionRepositoryRestrictResponseRootObject>}
   */
  public async unrestrict(targetUserId: number | string): Promise<RestrictActionRepositoryRestrictResponseRootObject> {
    const { body } = await this.http.full<RestrictActionRepositoryRestrictResponseRootObject>({
      url: '/api/v1/restrict_action/unrestrict/',
      method: 'POST',
      form: {
        _csrftoken: this.state.cookies.csrfToken,
        _uuid: this.state.device.uuid,
        target_user_id: targetUserId,
      },
    });
    return body;
  }
}
