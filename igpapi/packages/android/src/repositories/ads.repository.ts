import { Service } from 'typedi';
import { HeaderStatusCheckerStrategy } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { GraphQLRequestOptions } from '../types';

@Service()
export class AdsRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}

  /**
   * Probably internal only, used for insights
   * @param {GraphQLRequestOptions} options
   * @returns {Promise<T>}
   */
  public async graphQL<T extends { data: any }>(options: GraphQLRequestOptions): Promise<T> {
    const { body } = await this.http.full<T>({
      url: '/api/v1/ads/graphql/',
      method: 'POST',
      searchParams: {
        locale: this.state.device.language,
        vc_policy: 'insights_policy',
        ...(options.surface.name ? { surface: options.surface.name } : {}),
      },
      form: {
        access_token: options.accessToken,
        fb_api_caller_class: 'RelayModern',
        fb_api_req_friendly_name: options.surface.friendlyName,
        doc_id: options.documentId,
        variables: JSON.stringify(options.variables),
      },
      statusCheckerStrategy: new HeaderStatusCheckerStrategy(),
    });
    return body;
  }
}
