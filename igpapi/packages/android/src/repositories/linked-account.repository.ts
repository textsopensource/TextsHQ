import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

@Service()
export class LinkedAccountRepository {
  constructor(private http: AndroidHttp) {}
  public async getLinkageStatus() {
    const { body } = await this.http.full({
      url: `/api/v1/linked_accounts/get_linkage_status/`,
    });
    return body;
  }
}
