import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';

import { IgAppModule } from '../types';
import { AddressBookRepositoryLinkResponseRootObject } from '../responses/address-book.repository.link.response';
import { StatusResponse } from '../responses';

@Service()
export class AddressBookRepository {
  constructor(private http: AndroidHttp, private state: AndroidState) {}
  public async link(
    contacts: Array<{
      phone_numbers: string[];
      email_addresses: string[];
      first_name: string;
      last_name: string;
    }>,
    module?: IgAppModule,
  ): Promise<AddressBookRepositoryLinkResponseRootObject> {
    const { body } = await this.http.full<AddressBookRepositoryLinkResponseRootObject>({
      url: '/api/v1/address_book/link/',
      method: 'POST',
      form: {
        phone_id: this.state.device.phoneId,
        module: module || 'find_friends_contacts',
        contacts: JSON.stringify(contacts),
        _csrftoken: this.state.cookies.csrfToken,
        device_id: this.state.device.id,
        _uuid: this.state.device.uuid,
      },
    });
    return body;
  }

  public acquireOwnerContacts(me: {
    phone_numbers: string[];
    email_addresses: string[];
    first_name?: string;
    last_name?: string;
  }): Promise<StatusResponse> {
    return this.http.body<StatusResponse>({
      url: '/api/v1/address_book/acquire_owner_contacts/',
      method: 'POST',
      form: {
        phone_id: this.state.device.phoneId,
        _csrftoken: this.state.cookies.csrfToken,
        me: JSON.stringify(me),
        _uuid: this.state.device.uuid,
      },
    });
  }
}
