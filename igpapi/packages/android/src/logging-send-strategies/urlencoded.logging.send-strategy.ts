import { LoggingSendOptions, LoggingSendStrategy } from './logging.send-strategy';
import { AndroidHttp, AndroidState } from '../core';
import { LoggingResponse } from '../core/android.logging';
import { HeaderStatusCheckerStrategy } from '@textshq/core';

// TODO: add 'compressed' option => deflateRaw(json)

export class UrlencodedLoggingSendStrategy implements LoggingSendStrategy {
  send(options: LoggingSendOptions, state: AndroidState, http: AndroidHttp): Promise<LoggingResponse> {
    return http.body<LoggingResponse>({
      prefixUrl: 'https://graph.instagram.com',
      cookieJar: { getCookieString: () => undefined, setCookie: () => undefined },
      url: '/logging_client_events',
      method: 'POST',
      headers: {
        Host: 'graph.instagram.com',
      },
      statusCheckerStrategy: new HeaderStatusCheckerStrategy(),
      form: {
        access_token: `${state.application.FACEBOOK_ANALYTICS_APPLICATION_ID}|f249176f09e26ce54212b472dbab8fa8`,
        format: 'json',
        compressed: '0',
        sent_time: Date.now() / 1000,
        message: {
          seq: options.sequenceId,
          app_id: state.application.FACEBOOK_ANALYTICS_APPLICATION_ID,
          app_ver: state.application.APP_VERSION,
          build_num: state.application.APP_VERSION_CODE,
          device_id: state.device.uuid,
          family_device_id: state.device.phoneId,
          session_id: state.pigeonSessionId,
          channel: options.channel,
          app_uid: state.extractUserId(),
          claims: [state.session.igWWWClaim],
          config_version: 'v2',
          config_checksum: options.configChecksum ?? null,
          data: options.chunks,
          log_type: 'client_event',
        },
      },
    });
  }
}
