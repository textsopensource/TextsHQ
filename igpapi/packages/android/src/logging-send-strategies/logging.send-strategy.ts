import { LoggingChannel, LoggingChunkHeader, LoggingResponse } from '../core/android.logging';
import { AndroidHttp, AndroidState } from '../core';

export interface LoggingSendOptions {
  chunks: Array<LoggingChunkHeader & { extra: Record<string, unknown> }>;
  sequenceId: number;
  configChecksum?: string;
  channel: LoggingChannel;
}
export interface LoggingSendStrategy {
  send(options: LoggingSendOptions, state: AndroidState, http: AndroidHttp): Promise<LoggingResponse>;
}
