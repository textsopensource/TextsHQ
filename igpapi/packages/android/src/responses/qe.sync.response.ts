export interface QeSyncResponseRootObject {
  experiments: QeSyncResponseExperimentsItem[];
  status: string;
}
export interface QeSyncResponseExperimentsItem {
  name: string;
  group: string;
  additional_params: any[];
  params: QeSyncResponseParamsItem[];
  logging_id?: string;
}
export interface QeSyncResponseParamsItem {
  name: string;
  value: string;
}
