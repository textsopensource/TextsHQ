export interface ClipsUserFeedResponseRootObject {
  items: ClipsUserFeedResponseItemsItem[];
  paging_info: ClipsUserFeedResponsePaging_info;
  status: string;
}
export interface ClipsUserFeedResponseItemsItem {
  media: ClipsUserFeedResponseMedia;
}
export interface ClipsUserFeedResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: ClipsUserFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  video_versions: ClipsUserFeedResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  view_count: number;
  play_count: number;
  user: ClipsUserFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: ClipsUserFeedResponsePreviewCommentsItem[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  likers: ClipsUserFeedResponseLikersItem[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  caption: ClipsUserFeedResponseCaption | null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  product_type: string;
  clips_metadata: ClipsUserFeedResponseClips_metadata;
  is_dash_eligible?: number;
  video_dash_manifest?: string;
  video_codec?: string;
  number_of_qualities?: number;
}
export interface ClipsUserFeedResponseImage_versions2 {
  candidates: ClipsUserFeedResponseCandidatesItem[];
  additional_candidates: ClipsUserFeedResponseAdditional_candidates;
}
export interface ClipsUserFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsUserFeedResponseAdditional_candidates {
  igtv_first_frame: ClipsUserFeedResponseIgtv_first_frame;
  first_frame: ClipsUserFeedResponseFirst_frame;
}
export interface ClipsUserFeedResponseIgtv_first_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsUserFeedResponseFirst_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsUserFeedResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface ClipsUserFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  can_boost_post?: boolean;
  can_see_organic_insights?: boolean;
  show_insights_terms?: boolean;
  reel_auto_archive?: string;
  is_unpublished?: boolean;
  allowed_commenter_type?: string;
}
export interface ClipsUserFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsUserFeedResponseUser;
  media_id: string;
}
export interface ClipsUserFeedResponseClips_metadata {
  music_info: null | ClipsUserFeedResponseMusic_info;
  original_sound_info: ClipsUserFeedResponseOriginal_sound_info | null;
  featured_label: null;
}
export interface ClipsUserFeedResponseOriginal_sound_info {
  audio_asset_id: string | number;
  progressive_download_url: string;
  dash_manifest: string;
  ig_artist: ClipsUserFeedResponseIg_artist;
  should_mute_audio: boolean;
  original_media_id: string;
  hide_remixing: boolean;
  duration_in_ms: number;
  time_created: number;
}
export interface ClipsUserFeedResponseIg_artist {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface ClipsUserFeedResponsePreviewCommentsItem {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsUserFeedResponseUser;
  media_id: string;
  restricted_status: number;
}
export interface ClipsUserFeedResponseLikersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface ClipsUserFeedResponseMusic_info {
  music_asset_info: ClipsUserFeedResponseMusic_asset_info;
  music_consumption_info: ClipsUserFeedResponseMusic_consumption_info;
}
export interface ClipsUserFeedResponseMusic_asset_info {
  audio_cluster_id: string;
  id: string;
  title: string;
  subtitle: string;
  display_artist: string;
  cover_artwork_uri: string;
  cover_artwork_thumbnail_uri: string;
  progressive_download_url: string;
  highlight_start_times_in_ms: number[];
  is_explicit: boolean;
  dash_manifest: string;
  has_lyrics: boolean;
  audio_asset_id: string;
  duration_in_ms: number;
  dark_message: null;
  allows_saving: boolean;
}
export interface ClipsUserFeedResponseMusic_consumption_info {
  ig_artist: null;
  placeholder_profile_pic_url: string;
  should_mute_audio: boolean;
  should_mute_audio_reason: string;
  is_bookmarked: boolean;
  overlap_duration_in_ms: number;
  audio_asset_start_time_in_ms: number;
}
export interface ClipsUserFeedResponsePaging_info {
  more_available: boolean;
  max_id: string;
}
