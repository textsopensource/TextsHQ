export interface QeSyncExperimentsRootObject {
  test_1000: QeSyncExperimentsTest_1000;
  rollout: QeSyncExperimentsRollout;
  test: QeSyncExperimentsTest;
  update_on_tranistion: QeSyncExperimentsUpdate_on_tranistion;
  new_warning_content: QeSyncExperimentsNew_warning_content;
  deploy: QeSyncExperimentsDeploy;
  test_no_vyml_new_cover: QeSyncExperimentsTest_no_vyml_new_cover;
  launch: QeSyncExperimentsLaunch;
  test_1: QeSyncExperimentsTest_1;
  test_2: QeSyncExperimentsTest_2;
  test1: QeSyncExperimentsTest1;
  control: QeSyncExperimentsControl;
  rollout_high_volume: QeSyncExperimentsRollout_high_volume;
  no_cache_warmup_shared_thread: QeSyncExperimentsNo_cache_warmup_shared_thread;
  test_v5: QeSyncExperimentsTest_v5;
  pass: QeSyncExperimentsPass;
  test_50: QeSyncExperimentsTest_50;
  test_v3frx: QeSyncExperimentsTest_v3frx;
  test_dedup_async_http: QeSyncExperimentsTest_dedup_async_http;
  test_15_secs_10_seg: QeSyncExperimentsTest_15_secs_10_seg;
  test_with_animation: QeSyncExperimentsTest_with_animation;
  increment_075: QeSyncExperimentsIncrement_075;
  test_4s: QeSyncExperimentsTest_4s;
  full_screen_nux_bottom_done: QeSyncExperimentsFull_screen_nux_bottom_done;
  Test: QeSyncExperimentsTest;
  enabled: QeSyncExperimentsEnabled;
  disable_options: QeSyncExperimentsDisable_options;
  roll: QeSyncExperimentsRoll;
  prod_launch: QeSyncExperimentsProd_launch;
  new_infra: QeSyncExperimentsNew_infra;
  test_json_onpause: QeSyncExperimentsTest_json_onpause;
  button: QeSyncExperimentsButton;
  control_v1: QeSyncExperimentsControl_v1;
  test_activity_status: QeSyncExperimentsTest_activity_status;
  justdoit: QeSyncExperimentsJustdoit;
  use_trustedapp: QeSyncExperimentsUse_trustedapp;
  treatment: QeSyncExperimentsTreatment;
  control_top5: QeSyncExperimentsControl_top5;
  experiment: QeSyncExperimentsExperiment;
  release: QeSyncExperimentsRelease;
  test_40mb: QeSyncExperimentsTest_40mb;
  test_pill: QeSyncExperimentsTest_pill;
  test_entrypoint: QeSyncExperimentsTest_entrypoint;
  with_local_prefill: QeSyncExperimentsWith_local_prefill;
  blacklisted: QeSyncExperimentsBlacklisted;
  test5000ms: QeSyncExperimentsTest5000ms;
  close_friends: QeSyncExperimentsClose_friends;
  test_skip_cover_pic: QeSyncExperimentsTest_skip_cover_pic;
  Rollout: QeSyncExperimentsRollout;
  show_inapp_notif_no_tap_action: QeSyncExperimentsShow_inapp_notif_no_tap_action;
  enable_sso: QeSyncExperimentsEnable_sso;
  allow: QeSyncExperimentsAllow;
  are_you_satisfied: QeSyncExperimentsAre_you_satisfied;
  test2: QeSyncExperimentsTest2;
  test_cap: QeSyncExperimentsTest_cap;
  test_use_timeout: QeSyncExperimentsTest_use_timeout;
  test_v2: QeSyncExperimentsTest_v2;
  test_limit_2: QeSyncExperimentsTest_limit_2;
  test_no_tabs_hide_preview: QeSyncExperimentsTest_no_tabs_hide_preview;
  test_new_definition_persist: QeSyncExperimentsTest_new_definition_persist;
  est_payload_640_1280_story_512: QeSyncExperimentsEst_payload_640_1280_story_512;
  ui: QeSyncExperimentsUi;
}
export interface QeSyncExperimentsTest_1000 {
  ig_android_video_ssim_fix_pts_universe: QeSyncExperimentsIg_android_video_ssim_fix_pts_universe;
}
export interface QeSyncExperimentsIg_android_video_ssim_fix_pts_universe {
  igGroup: string;
  igAdditional: any[];
  ssim_fix_pts: number;
}
export interface QeSyncExperimentsRollout {
  ig_android_stories_seen_state_serialization?: QeSyncExperimentsIg_android_stories_seen_state_serialization;
  ig_android_stories_music_search_typeahead?: QeSyncExperimentsIg_android_stories_music_search_typeahead;
  ig_android_igtv_crop_top?: QeSyncExperimentsIg_android_igtv_crop_top;
  ig_android_stories_music_filters?: QeSyncExperimentsIg_android_stories_music_filters;
  ig_xposting_mention_reshare_stories?: QeSyncExperimentsIg_xposting_mention_reshare_stories;
  ig_android_cover_frame_upload_skip_story_raven_universe?: QeSyncExperimentsIg_android_cover_frame_upload_skip_story_raven_universe;
  ig_android_stories_video_prefetch_kb?: QeSyncExperimentsIg_android_stories_video_prefetch_kb;
  ig_android_nelson_v0_universe?: QeSyncExperimentsIg_android_nelson_v0_universe;
  ig_android_stories_music_overlay?: QeSyncExperimentsIg_android_stories_music_overlay;
  ig_android_viewpoint_occlusion?: QeSyncExperimentsIg_android_viewpoint_occlusion;
  ig_disable_fsync_universe?: QeSyncExperimentsIg_disable_fsync_universe;
  ig_search_hashtag_content_advisory_remove_snooze?: QeSyncExperimentsIg_search_hashtag_content_advisory_remove_snooze;
  ig_android_video_product_specific_abr?: QeSyncExperimentsIg_android_video_product_specific_abr;
  ig_android_ig_personal_account_xpost_eligibility_from_server?: QeSyncExperimentsIg_android_ig_personal_account_xpost_eligibility_from_server;
  ig_android_vc_capture_universe?: QeSyncExperimentsIg_android_vc_capture_universe;
  ig_android_custom_story_import_intent?: QeSyncExperimentsIg_android_custom_story_import_intent;
  ig_android_wab_adjust_resize_universe?: QeSyncExperimentsIg_android_wab_adjust_resize_universe;
  ig_rti_inapp_notifications_universe?: QeSyncExperimentsIg_rti_inapp_notifications_universe;
  ig_android_igtv_cribs_creation_universe?: QeSyncExperimentsIg_android_igtv_cribs_creation_universe;
  ig_android_post_live?: QeSyncExperimentsIg_android_post_live;
  ig_android_stories_question_sticker_music_format?: QeSyncExperimentsIg_android_stories_question_sticker_music_format;
  ig_android_stories_music_lyrics?: QeSyncExperimentsIg_android_stories_music_lyrics;
  ig_android_stories_music_awareness_universe?: QeSyncExperimentsIg_android_stories_music_awareness_universe;
  ig_android_shopping_checkout_signaling?: QeSyncExperimentsIg_android_shopping_checkout_signaling;
  ig_android_stories_share_extension_video_segmentation?: QeSyncExperimentsIg_android_stories_share_extension_video_segmentation;
  ig_android_ads_profile_cta_feed_universe?: QeSyncExperimentsIg_android_ads_profile_cta_feed_universe;
  ig_android_live_qa_viewer_v1_universe?: QeSyncExperimentsIg_android_live_qa_viewer_v1_universe;
  ig_android_shopping_product_metadata_on_product_tiles_universe?: QeSyncExperimentsIg_android_shopping_product_metadata_on_product_tiles_universe;
  ig_android_vc_start_from_direct_inbox_universe?: QeSyncExperimentsIg_android_vc_start_from_direct_inbox_universe;
  ig_android_story_bottom_sheet_clips_single_audio_mas?: QeSyncExperimentsIg_android_story_bottom_sheet_clips_single_audio_mas;
  ig_android_unified_iab_logging_universe?: QeSyncExperimentsIg_android_unified_iab_logging_universe;
  ig_android_igtv_first_frame_cover?: QeSyncExperimentsIg_android_igtv_first_frame_cover;
  ig_android_igtv_cribs?: QeSyncExperimentsIg_android_igtv_cribs;
  ig_android_direct_unread_count_badge?: QeSyncExperimentsIg_android_direct_unread_count_badge;
  ig_android_live_ama_universe?: QeSyncExperimentsIg_android_live_ama_universe;
  ig_android_video_raven_streaming_upload_universe?: QeSyncExperimentsIg_android_video_raven_streaming_upload_universe;
  ig_android_profile_unified_follow_view?: QeSyncExperimentsIg_android_profile_unified_follow_view;
  aymt_instagram_promote_flow_abandonment_ig_universe?: QeSyncExperimentsAymt_instagram_promote_flow_abandonment_ig_universe;
  ig_android_personal_user_xposting_destination_fix?: QeSyncExperimentsIg_android_personal_user_xposting_destination_fix;
  ig_android_felix_video_upload_length?: QeSyncExperimentsIg_android_felix_video_upload_length;
  ig_android_interactions_nav_to_permalink_followup_universe?: QeSyncExperimentsIg_android_interactions_nav_to_permalink_followup_universe;
  ig_android_live_qa_broadcaster_v1_universe?: QeSyncExperimentsIg_android_live_qa_broadcaster_v1_universe;
  ig_android_new_camera_design_universe?: QeSyncExperimentsIg_android_new_camera_design_universe;
  ig_direct_max_participants?: QeSyncExperimentsIg_direct_max_participants;
  ig_android_logging_metric_universe_v2?: QeSyncExperimentsIg_android_logging_metric_universe_v2;
}
export interface QeSyncExperimentsIg_android_stories_seen_state_serialization {
  igGroup: string;
  igAdditional: any[];
  serialize_async: boolean;
  memory_only: boolean;
}
export interface QeSyncExperimentsIg_android_stories_music_search_typeahead {
  igGroup: string;
  igAdditional: any[];
  keywords_count: number;
  default_search_mode: string;
}
export interface QeSyncExperimentsIg_android_igtv_crop_top {
  igGroup: string;
  igAdditional: any[];
  is_profile_crop_enabled: boolean;
  is_feed_preview_crop_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_music_filters {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_xposting_mention_reshare_stories {
  igGroup: string;
  igAdditional: any[];
  enable_xpost_mention_reshares: boolean;
}
export interface QeSyncExperimentsIg_android_cover_frame_upload_skip_story_raven_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  skip_raven: boolean;
  skip_story: boolean;
  skip_story_raven: boolean;
}
export interface QeSyncExperimentsIg_android_stories_video_prefetch_kb {
  igGroup: string;
  igAdditional: any[];
  prefetch_kb: number;
}
export interface QeSyncExperimentsIg_android_nelson_v0_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  nux_show_count: number;
  bloks_learn_more_app_id: string;
  bloks_nux_action_app_id: string;
  endpoint_name_add: string;
  endpoint_name_comment_action_1: string;
  endpoint_name_get_members: string;
  endpoint_name_remove: string;
}
export interface QeSyncExperimentsIg_android_stories_music_overlay {
  igGroup: string;
  igAdditional: any[];
  is_post_capture_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_viewpoint_occlusion {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  new_ad_viewability: boolean;
  pip_enabled: boolean;
}
export interface QeSyncExperimentsIg_disable_fsync_universe {
  igGroup: string;
  igAdditional: any[];
  disable_fsync: boolean;
}
export interface QeSyncExperimentsIg_search_hashtag_content_advisory_remove_snooze {
  igGroup: string;
  igAdditional: any[];
  remove_snooze: boolean;
}
export interface QeSyncExperimentsIg_android_video_product_specific_abr {
  igGroup: string;
  igAdditional: any[];
  remove_video_type: boolean;
}
export interface QeSyncExperimentsIg_android_ig_personal_account_xpost_eligibility_from_server {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_vc_capture_universe {
  igGroup: string;
  igAdditional: any[];
  use_textureview_preview: boolean;
  use_camera_core_renderer: boolean;
}
export interface QeSyncExperimentsIg_android_custom_story_import_intent {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_whitelist_gating_disabled: boolean;
  new_story_holdout_behavior: string;
}
export interface QeSyncExperimentsIg_android_wab_adjust_resize_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_rti_inapp_notifications_universe {
  igGroup: string;
  igAdditional: any[];
  enable_comment_mention_and_reply: boolean;
  enable_comment: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_cribs_creation_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_post_live {
  igGroup: string;
  igAdditional: any[];
  is_updated_cta_enabled: boolean;
  is_delete_primary: boolean;
}
export interface QeSyncExperimentsIg_android_stories_question_sticker_music_format {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_music_lyrics {
  igGroup: string;
  igAdditional: any[];
  is_lyrics_enabled: boolean;
  is_dynamic_phrase_timing_enabled: boolean;
  is_dynamic_phrase_timing_for_dynamic_reveal_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_music_awareness_universe {
  igGroup: string;
  igAdditional: any[];
  is_sound_wave_enabled: boolean;
  is_consumption_sheet_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_shopping_checkout_signaling {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_share_extension_video_segmentation {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ads_profile_cta_feed_universe {
  igGroup: string;
  igAdditional: any[];
  media_enabled: boolean;
  comments_enabled: boolean;
  profile_enabled: boolean;
  bottom_cta_type: string;
}
export interface QeSyncExperimentsIg_android_live_qa_viewer_v1_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_shopping_product_metadata_on_product_tiles_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_vc_start_from_direct_inbox_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_story_bottom_sheet_clips_single_audio_mas {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_unified_iab_logging_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_first_frame_cover {
  igGroup: string;
  igAdditional: any[];
  is_first_frame_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_cribs {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_unread_count_badge {
  igGroup: string;
  igAdditional: any[];
  show_unread_message_digest: boolean;
}
export interface QeSyncExperimentsIg_android_live_ama_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_realtime_submissions_enabled: boolean;
  is_voting_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_video_raven_streaming_upload_universe {
  igGroup: string;
  igAdditional: any[];
  streaming_upload_raven: boolean;
}
export interface QeSyncExperimentsIg_android_profile_unified_follow_view {
  igGroup: string;
  igAdditional: any[];
  display_only_mutual_in_see_all_view: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsAymt_instagram_promote_flow_abandonment_ig_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  show_promote_icon: boolean;
  in_payments_campaign: boolean;
  use_see_how_alt: boolean;
  show_tip: boolean;
}
export interface QeSyncExperimentsIg_android_personal_user_xposting_destination_fix {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_felix_video_upload_length {
  igGroup: string;
  igAdditional: any[];
  max_duration_ms: number;
  min_duration_ms: number;
}
export interface QeSyncExperimentsIg_android_interactions_nav_to_permalink_followup_universe {
  igGroup: string;
  igAdditional: any[];
  post_tag_push_notif_to_permalink: boolean;
  like_push_notif_to_permalink: boolean;
  post_like_push_notif_scroll_down_a_little: boolean;
  from_actvity_feed_scroll_down_50_pct: boolean;
}
export interface QeSyncExperimentsIg_android_live_qa_broadcaster_v1_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_new_camera_design_universe {
  igGroup: string;
  igAdditional: any[];
  display_edit_buttons_in_type_mode: boolean;
  is_viewmaster_neue_enabled: boolean;
  format_ordering: string;
}
export interface QeSyncExperimentsIg_direct_max_participants {
  igGroup: string;
  igAdditional: any[];
  group_size: number;
  max_participants: number;
}
export interface QeSyncExperimentsTest {
  android_ard_ig_use_brotli_effect_universe?: QeSyncExperimentsAndroid_ard_ig_use_brotli_effect_universe;
  ig_android_feedback_required?: QeSyncExperimentsIg_android_feedback_required;
  ig_android_biz_ranked_requests_universe?: QeSyncExperimentsIg_android_biz_ranked_requests_universe;
  ig_android_direct_add_member_dialog_universe?: QeSyncExperimentsIg_android_direct_add_member_dialog_universe;
  ig_android_recyclerview_binder_group_enabled_universe?: QeSyncExperimentsIg_android_recyclerview_binder_group_enabled_universe;
  ig_rn_branded_content_settings_approval_on_select_save?: QeSyncExperimentsIg_rn_branded_content_settings_approval_on_select_save;
  ig_android_fix_ppr_thumbnail_url?: QeSyncExperimentsIg_android_fix_ppr_thumbnail_url;
  ig_interactions_project_daisy_creators_universe?: QeSyncExperimentsIg_interactions_project_daisy_creators_universe;
  ig_android_xposting_feed_to_stories_reshares_universe?: QeSyncExperimentsIg_android_xposting_feed_to_stories_reshares_universe;
  ig_android_explore_reel_loading_state?: QeSyncExperimentsIg_android_explore_reel_loading_state;
  ig_android_stories_disable_highlights_media_preloading?: QeSyncExperimentsIg_android_stories_disable_highlights_media_preloading;
  ig_camera_android_attribution_bottomsheet_universe?: QeSyncExperimentsIg_camera_android_attribution_bottomsheet_universe;
  ig_carousel_bumped_organic_impression_client_universe?: QeSyncExperimentsIg_carousel_bumped_organic_impression_client_universe;
  ig_android_branded_content_upsell_keywords_extension?: QeSyncExperimentsIg_android_branded_content_upsell_keywords_extension;
  ig_android_unify_graph_management_actions?: QeSyncExperimentsIg_android_unify_graph_management_actions;
  ig_challenge_general_v2?: QeSyncExperimentsIg_challenge_general_v2;
  ig_android_live_ama_viewer_universe?: QeSyncExperimentsIg_android_live_ama_viewer_universe;
  ig_android_camera_post_smile_low_end_universe?: QeSyncExperimentsIg_android_camera_post_smile_low_end_universe;
  ig_android_wellbeing_support_frx_igtv_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_igtv_reporting;
  ig_android_branded_content_appeal_states?: QeSyncExperimentsIg_android_branded_content_appeal_states;
  ig_android_mediauri_parse_optimization?: QeSyncExperimentsIg_android_mediauri_parse_optimization;
  ig_android_fix_main_feed_su_cards_size_universe?: QeSyncExperimentsIg_android_fix_main_feed_su_cards_size_universe;
  ig_android_video_outputsurface_handlerthread_universe?: QeSyncExperimentsIg_android_video_outputsurface_handlerthread_universe;
  ig_android_direct_like_animation_onbind_conflict_fix?: QeSyncExperimentsIg_android_direct_like_animation_onbind_conflict_fix;
  ig_android_react_native_universe_kill_switch?: QeSyncExperimentsIg_android_react_native_universe_kill_switch;
  ig_android_interactions_fix_activity_feed_ufi?: QeSyncExperimentsIg_android_interactions_fix_activity_feed_ufi;
  ig_android_xposting_upsell_directly_after_sharing_to_story?: QeSyncExperimentsIg_android_xposting_upsell_directly_after_sharing_to_story;
  ig_android_camera_focus_low_end_universe?: QeSyncExperimentsIg_android_camera_focus_low_end_universe;
  ig_android_vc_background_call_toast_universe?: QeSyncExperimentsIg_android_vc_background_call_toast_universe;
  ig_android_fb_sync_options_universe?: QeSyncExperimentsIg_android_fb_sync_options_universe;
  ig_traffic_routing_universe?: QeSyncExperimentsIg_traffic_routing_universe;
  ig_android_camera_effects_order_universe?: QeSyncExperimentsIg_android_camera_effects_order_universe;
  ig_android_account_story_grid_universe?: QeSyncExperimentsIg_android_account_story_grid_universe;
  ig_payment_checkout_info?: QeSyncExperimentsIg_payment_checkout_info;
  ig_android_feed_defer_on_interactions?: QeSyncExperimentsIg_android_feed_defer_on_interactions;
  ig_android_test_not_signing_address_book_unlink_endpoint?: QeSyncExperimentsIg_android_test_not_signing_address_book_unlink_endpoint;
  ig_android_branding_v2_settings_universe?: QeSyncExperimentsIg_android_branding_v2_settings_universe;
  ig_direct_android_mentions_sender?: QeSyncExperimentsIg_direct_android_mentions_sender;
  ig_android_gallery_minimum_video_length?: QeSyncExperimentsIg_android_gallery_minimum_video_length;
  ig_android_hashtag_remove_share_hashtag?: QeSyncExperimentsIg_android_hashtag_remove_share_hashtag;
  ig_shopping_checkout_2x2_platformization_universe?: QeSyncExperimentsIg_shopping_checkout_2x2_platformization_universe;
  ig_android_show_create_content_pages_universe?: QeSyncExperimentsIg_android_show_create_content_pages_universe;
  ig_android_hashtag_limit?: QeSyncExperimentsIg_android_hashtag_limit;
  ig_android_profile_ppr_fixes?: QeSyncExperimentsIg_android_profile_ppr_fixes;
  ig_android_inline_composer_animation_fix?: QeSyncExperimentsIg_android_inline_composer_animation_fix;
  ig_android_hide_contacts_list?: QeSyncExperimentsIg_android_hide_contacts_list;
  ig_android_wellbeing_support_frx_stories_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_stories_reporting;
  ig_android_business_attribute_sync?: QeSyncExperimentsIg_android_business_attribute_sync;
  ig_android_view_and_likes_cta_universe?: QeSyncExperimentsIg_android_view_and_likes_cta_universe;
  ig_android_tango_cpu_overuse_universe?: QeSyncExperimentsIg_android_tango_cpu_overuse_universe;
  ig_android_branded_content_ads_enable_partner_boost?: QeSyncExperimentsIg_android_branded_content_ads_enable_partner_boost;
  ig_android_direct_remix_visual_messages?: QeSyncExperimentsIg_android_direct_remix_visual_messages;
  ig_quick_story_placement_validation_universe?: QeSyncExperimentsIg_quick_story_placement_validation_universe;
  ig_android_search_impression_logging_viewpoint?: QeSyncExperimentsIg_android_search_impression_logging_viewpoint;
  ig_android_camera_upsell_dialog?: QeSyncExperimentsIg_android_camera_upsell_dialog;
  ig_android_ad_watchbrowse_universe?: QeSyncExperimentsIg_android_ad_watchbrowse_universe;
  ig_android_direct_delete_or_block_from_message_requests?: QeSyncExperimentsIg_android_direct_delete_or_block_from_message_requests;
  ig_android_stories_reshare_reply_msg?: QeSyncExperimentsIg_android_stories_reshare_reply_msg;
  ig_camera_android_facetracker_v12_universe?: QeSyncExperimentsIg_camera_android_facetracker_v12_universe;
  ig_badge_dedup_universe?: QeSyncExperimentsIg_badge_dedup_universe;
  ig_android_direct_rx_thread_update?: QeSyncExperimentsIg_android_direct_rx_thread_update;
  ig_android_account_post_grid_universe?: QeSyncExperimentsIg_android_account_post_grid_universe;
  ard_ig_broti_effect?: QeSyncExperimentsArd_ig_broti_effect;
  ig_android_expanded_xposting_upsell_directly_after_sharing_story_universe?: QeSyncExperimentsIg_android_expanded_xposting_upsell_directly_after_sharing_story_universe;
  ig_promote_post_insights_entry_universe?: QeSyncExperimentsIg_promote_post_insights_entry_universe;
  ig_android_live_subscribe_user_level_universe?: QeSyncExperimentsIg_android_live_subscribe_user_level_universe;
  ig_android_video_call_finish_universe?: QeSyncExperimentsIg_android_video_call_finish_universe;
  ig_biz_growth_insights_universe?: QeSyncExperimentsIg_biz_growth_insights_universe;
  ig_promote_manager_improvements_universe?: QeSyncExperimentsIg_promote_manager_improvements_universe;
  ig_promote_prefill_destination_universe?: QeSyncExperimentsIg_promote_prefill_destination_universe;
  ig_android_dismiss_recent_searches?: QeSyncExperimentsIg_android_dismiss_recent_searches;
  ig_android_feed_camera_size_setter?: QeSyncExperimentsIg_android_feed_camera_size_setter;
  ig_android_fb_link_ui_polish_universe?: QeSyncExperimentsIg_android_fb_link_ui_polish_universe;
  ig_android_camera_gyro_universe?: QeSyncExperimentsIg_android_camera_gyro_universe;
  ig_android_igtv_ssim_report?: QeSyncExperimentsIg_android_igtv_ssim_report;
  ig_android_stories_combined_asset_search?: QeSyncExperimentsIg_android_stories_combined_asset_search;
  instagram_pcp_activity_feed_following_tab_universe?: QeSyncExperimentsInstagram_pcp_activity_feed_following_tab_universe;
  ig_android_optic_photo_cropping_fixes?: QeSyncExperimentsIg_android_optic_photo_cropping_fixes;
  ig_android_optic_new_architecture?: QeSyncExperimentsIg_android_optic_new_architecture;
  ig_android_push_notifications_settings_redesign_universe?: QeSyncExperimentsIg_android_push_notifications_settings_redesign_universe;
  ig_android_show_muted_accounts_page?: QeSyncExperimentsIg_android_show_muted_accounts_page;
  ig_android_vc_service_crash_fix_universe?: QeSyncExperimentsIg_android_vc_service_crash_fix_universe;
  ig_android_ttcp_improvements?: QeSyncExperimentsIg_android_ttcp_improvements;
  ig_share_to_story_toggle_include_shopping_product?: QeSyncExperimentsIg_share_to_story_toggle_include_shopping_product;
  ig_android_interactions_verified_badge_on_comment_details?: QeSyncExperimentsIg_android_interactions_verified_badge_on_comment_details;
  ig_android_fs_new_gallery_hashtag_prompts?: QeSyncExperimentsIg_android_fs_new_gallery_hashtag_prompts;
  ig_android_story_ads_carousel_performance_universe_2?: QeSyncExperimentsIg_android_story_ads_carousel_performance_universe_2;
  ig_android_quick_conversion_universe?: QeSyncExperimentsIg_android_quick_conversion_universe;
  ig_android_stories_vpvd_container_module_fix?: QeSyncExperimentsIg_android_stories_vpvd_container_module_fix;
  ig_android_wellbeing_timeinapp_v1_universe?: QeSyncExperimentsIg_android_wellbeing_timeinapp_v1_universe;
  ig_android_wellbeing_timeinapp_v1_migration?: QeSyncExperimentsIg_android_wellbeing_timeinapp_v1_migration;
  ig_android_shopping_variant_selector_redesign?: QeSyncExperimentsIg_android_shopping_variant_selector_redesign;
  ig_android_video_streaming_upload_universe?: QeSyncExperimentsIg_android_video_streaming_upload_universe;
  ig_android_camera_tti_improvements?: QeSyncExperimentsIg_android_camera_tti_improvements;
  ig_camera_android_release_drawing_view_universe?: QeSyncExperimentsIg_camera_android_release_drawing_view_universe;
  ig_android_music_story_fb_crosspost_universe?: QeSyncExperimentsIg_android_music_story_fb_crosspost_universe;
  ig_android_payments_growth_promote_payments_in_payments?: QeSyncExperimentsIg_android_payments_growth_promote_payments_in_payments;
  ig_android_direct_smoke_animation_universe?: QeSyncExperimentsIg_android_direct_smoke_animation_universe;
  ig_android_camera_platform_effect_share_universe?: QeSyncExperimentsIg_android_camera_platform_effect_share_universe;
  ig_android_audience?: QeSyncExperimentsIg_android_audience;
  ig_android_ig_branding_in_fb_universe?: QeSyncExperimentsIg_android_ig_branding_in_fb_universe;
  ig_android_wellbeing_support_frx_hashtags_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_hashtags_reporting;
  ig_android_churned_find_friends_redirect_to_discover_people?: QeSyncExperimentsIg_android_churned_find_friends_redirect_to_discover_people;
  ig_android_fix_direct_badge_count_universe?: QeSyncExperimentsIg_android_fix_direct_badge_count_universe;
  ig_android_profile_follow_tab_hashtag_row_universe?: QeSyncExperimentsIg_android_profile_follow_tab_hashtag_row_universe;
  ig_android_business_transaction_in_stories_creator?: QeSyncExperimentsIg_android_business_transaction_in_stories_creator;
  ig_android_creator_quick_reply_universe?: QeSyncExperimentsIg_android_creator_quick_reply_universe;
  ig_android_biz_story_to_fb_page_improvement?: QeSyncExperimentsIg_android_biz_story_to_fb_page_improvement;
  ig_android_branded_content_insights_disclosure?: QeSyncExperimentsIg_android_branded_content_insights_disclosure;
  ig_adapter_leak_universe?: QeSyncExperimentsIg_adapter_leak_universe;
  ig_xposting_biz_feed_to_story_reshare?: QeSyncExperimentsIg_xposting_biz_feed_to_story_reshare;
  ig_android_user_url_deeplink_fbpage_endpoint?: QeSyncExperimentsIg_android_user_url_deeplink_fbpage_endpoint;
  ig_android_wellbeing_support_frx_cowatch_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_cowatch_reporting;
  ig_android_explore_use_shopping_endpoint?: QeSyncExperimentsIg_android_explore_use_shopping_endpoint;
  ig_branded_content_settings_unsaved_changes_dialog?: QeSyncExperimentsIg_branded_content_settings_unsaved_changes_dialog;
  ig_android_insights_native_post_universe?: QeSyncExperimentsIg_android_insights_native_post_universe;
  ig_android_dual_destination_quality_improvement?: QeSyncExperimentsIg_android_dual_destination_quality_improvement;
  ig_music_dash?: QeSyncExperimentsIg_music_dash;
  ig_android_insights_activity_tab_native_universe?: QeSyncExperimentsIg_android_insights_activity_tab_native_universe;
  ig_android_aggressive_media_cleanup?: QeSyncExperimentsIg_android_aggressive_media_cleanup;
  ig_android_search_usl?: QeSyncExperimentsIg_android_search_usl;
  ig_android_business_tokenless_stories_xposting?: QeSyncExperimentsIg_android_business_tokenless_stories_xposting;
  ig_android_xposting_newly_fbc_people?: QeSyncExperimentsIg_android_xposting_newly_fbc_people;
  ig_android_do_not_show_social_context_on_follow_list_universe?: QeSyncExperimentsIg_android_do_not_show_social_context_on_follow_list_universe;
  ig_android_list_adapter_prefetch_infra?: QeSyncExperimentsIg_android_list_adapter_prefetch_infra;
  ig_android_fix_inbox_flicker_universe?: QeSyncExperimentsIg_android_fix_inbox_flicker_universe;
  ig_direct_feed_media_sticker_universe?: QeSyncExperimentsIg_direct_feed_media_sticker_universe;
  ig_android_test_remove_button_main_cta_self_followers_universe?: QeSyncExperimentsIg_android_test_remove_button_main_cta_self_followers_universe;
  ig_android_wellbeing_support_frx_live_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_live_reporting;
  android_cameracore_safe_makecurrent_ig?: QeSyncExperimentsAndroid_cameracore_safe_makecurrent_ig;
  ig_android_direct_simple_message_comparator_universe?: QeSyncExperimentsIg_android_direct_simple_message_comparator_universe;
  ig_android_vc_face_effects_universe?: QeSyncExperimentsIg_android_vc_face_effects_universe;
  ig_android_fbpage_on_profile_side_tray?: QeSyncExperimentsIg_android_fbpage_on_profile_side_tray;
  ig_android_feed_delivery_refactor?: QeSyncExperimentsIg_android_feed_delivery_refactor;
  ig_branded_content_tagging_approval_request_flow_brand_side_v2?: QeSyncExperimentsIg_branded_content_tagging_approval_request_flow_brand_side_v2;
  ig_android_wellbeing_support_frx_profile_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_profile_reporting;
  ig_android_search_register_recent_store?: QeSyncExperimentsIg_android_search_register_recent_store;
  ig_android_wellbeing_support_frx_feed_posts_reporting?: QeSyncExperimentsIg_android_wellbeing_support_frx_feed_posts_reporting;
  ig_android_unified_inbox_ephemeral_media_tooltip_universe?: QeSyncExperimentsIg_android_unified_inbox_ephemeral_media_tooltip_universe;
  ig_android_recents_and_edit_flow?: QeSyncExperimentsIg_android_recents_and_edit_flow;
  ig_android_delete_ssim_compare_img_soon?: QeSyncExperimentsIg_android_delete_ssim_compare_img_soon;
  ig_camera_android_boomerang_attribution_universe?: QeSyncExperimentsIg_camera_android_boomerang_attribution_universe;
  ig_android_igtv_browse_long_press?: QeSyncExperimentsIg_android_igtv_browse_long_press;
  ig_android_direct_mark_as_read_notif_action?: QeSyncExperimentsIg_android_direct_mark_as_read_notif_action;
  ig_cameracore_android_new_optic_camera2?: QeSyncExperimentsIg_cameracore_android_new_optic_camera2;
  ig_android_secondary_inbox_universe?: QeSyncExperimentsIg_android_secondary_inbox_universe;
  ig_android_video_live_trace_universe?: QeSyncExperimentsIg_android_video_live_trace_universe;
  ig_android_camera_ar_platform_details_view_universe?: QeSyncExperimentsIg_android_camera_ar_platform_details_view_universe;
  qe_android_direct_vm_view_modes?: QeSyncExperimentsQe_android_direct_vm_view_modes;
  ig_android_flexible_profile_universe?: QeSyncExperimentsIg_android_flexible_profile_universe;
  ig_android_cold_start_logging_for_new_users_fix?: QeSyncExperimentsIg_android_cold_start_logging_for_new_users_fix;
  ig_android_business_transaction_in_stories_consumer?: QeSyncExperimentsIg_android_business_transaction_in_stories_consumer;
  ig_android_business_cross_post_with_biz_id_infra?: QeSyncExperimentsIg_android_business_cross_post_with_biz_id_infra;
  ig_android_fix_push_setting_logging_universe?: QeSyncExperimentsIg_android_fix_push_setting_logging_universe;
  ig_interactions_h2_2019_team_holdout_universe?: QeSyncExperimentsIg_interactions_h2_2019_team_holdout_universe;
  ig_android_reel_tray_item_impression_logging_viewpoint?: QeSyncExperimentsIg_android_reel_tray_item_impression_logging_viewpoint;
  ig_android_ads_history_universe?: QeSyncExperimentsIg_android_ads_history_universe;
  ig_android_location_recyclerview?: QeSyncExperimentsIg_android_location_recyclerview;
  ig_android_sidecar_report_ssim?: QeSyncExperimentsIg_android_sidecar_report_ssim;
  shop_home_hscroll_see_all_button_universe?: QeSyncExperimentsShop_home_hscroll_see_all_button_universe;
  ig_android_ad_view_ads_native_universe?: QeSyncExperimentsIg_android_ad_view_ads_native_universe;
  ig_android_throttled_search_requests?: QeSyncExperimentsIg_android_throttled_search_requests;
  ig_close_friends_v4_global?: QeSyncExperimentsIg_close_friends_v4_global;
  ig_promote_hide_local_awareness_universe?: QeSyncExperimentsIg_promote_hide_local_awareness_universe;
  ig_android_graphql_survey_new_proxy_universe?: QeSyncExperimentsIg_android_graphql_survey_new_proxy_universe;
  ig_android_shopping_bag_optimization_universe?: QeSyncExperimentsIg_android_shopping_bag_optimization_universe;
  ig_android_hashtag_recyclerview?: QeSyncExperimentsIg_android_hashtag_recyclerview;
  ig_android_wishlist_reconsideration_universe?: QeSyncExperimentsIg_android_wishlist_reconsideration_universe;
  ig_camera_android_focus_in_post_universe?: QeSyncExperimentsIg_camera_android_focus_in_post_universe;
  ig_android_ad_iab_qpl_kill_switch_universe?: QeSyncExperimentsIg_android_ad_iab_qpl_kill_switch_universe;
  ig_android_prefetch_logic_infra?: QeSyncExperimentsIg_android_prefetch_logic_infra;
  ig_android_self_story_button_non_fbc_accounts?: QeSyncExperimentsIg_android_self_story_button_non_fbc_accounts;
  ig_android_direct_activator_cards?: QeSyncExperimentsIg_android_direct_activator_cards;
  ig_payment_checkout_cvv?: QeSyncExperimentsIg_payment_checkout_cvv;
  ig_android_scroll_main_feed?: QeSyncExperimentsIg_android_scroll_main_feed;
  ig_android_igtv_player_follow_button?: QeSyncExperimentsIg_android_igtv_player_follow_button;
  ig_android_hec_promote_universe?: QeSyncExperimentsIg_android_hec_promote_universe;
  ig_android_remove_follow_all_fb_list?: QeSyncExperimentsIg_android_remove_follow_all_fb_list;
  ig_android_react_native_email_sms_settings_universe?: QeSyncExperimentsIg_android_react_native_email_sms_settings_universe;
  ig_android_ads_manager_pause_resume_ads_universe?: QeSyncExperimentsIg_android_ads_manager_pause_resume_ads_universe;
}
export interface QeSyncExperimentsAndroid_ard_ig_use_brotli_effect_universe {
  igGroup: string;
  igAdditional: any[];
  use_effect_cache_key: boolean;
}
export interface QeSyncExperimentsIg_android_feedback_required {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_biz_ranked_requests_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_add_member_dialog_universe {
  igGroup: string;
  igAdditional: any[];
  show_new_add_member_dialog_string: boolean;
}
export interface QeSyncExperimentsIg_android_recyclerview_binder_group_enabled_universe {
  igGroup: string;
  igAdditional: any[];
}
export interface QeSyncExperimentsIg_rn_branded_content_settings_approval_on_select_save {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fix_ppr_thumbnail_url {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_interactions_project_daisy_creators_universe {
  igGroup: string;
  igAdditional: any[];
  should_show_creator_insights_section: boolean;
  profile_creator_entrypoint_enabled: boolean;
  profile_creator_bloks_entrypoint: string;
}
export interface QeSyncExperimentsIg_android_xposting_feed_to_stories_reshares_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_explore_reel_loading_state {
  igGroup: string;
  igAdditional: any[];
  show_loading_state: boolean;
}
export interface QeSyncExperimentsIg_android_stories_disable_highlights_media_preloading {
  igGroup: string;
  igAdditional: any[];
  tray_prefetch_disabled: boolean;
  max_media_in_preloaded_reel: number;
  num_reels_to_preload: number;
  preload_small_reels_only: string;
}
export interface QeSyncExperimentsIg_camera_android_attribution_bottomsheet_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  attribution_query_id: number;
}
export interface QeSyncExperimentsIg_carousel_bumped_organic_impression_client_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_branded_content_upsell_keywords_extension {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_unify_graph_management_actions {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_rollout: boolean;
}
export interface QeSyncExperimentsIg_challenge_general_v2 {
  igGroup: string;
  igAdditional: any[];
  eligible_for_hl_unvet: boolean;
}
export interface QeSyncExperimentsIg_android_live_ama_viewer_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_camera_post_smile_low_end_universe {
  igGroup: string;
  igAdditional: any[];
  is_double_buffer_enabled: boolean;
  is_video_transcoding_enabled: boolean;
  is_managing_surfacetexture_enabled: boolean;
  is_dark_test_enabled: boolean;
  is_photo_optimization_enabled: boolean;
  is_enabled: boolean;
  is_multi_upload_enabled: boolean;
  is_video_preview_enabled: boolean;
  is_photo_preview_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_igtv_reporting {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_branded_content_appeal_states {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_mediauri_parse_optimization {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_avoid_alloc_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fix_main_feed_su_cards_size_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_video_outputsurface_handlerthread_universe {
  igGroup: string;
  igAdditional: any[];
  outputsurface_use_handlerthread: boolean;
}
export interface QeSyncExperimentsIg_android_direct_like_animation_onbind_conflict_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_react_native_universe_kill_switch {
  igGroup: string;
  igAdditional: any[];
  ig_android_react_native_push_notification_settings_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_interactions_fix_activity_feed_ufi {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_xposting_upsell_directly_after_sharing_to_story {
  igGroup: string;
  igAdditional: any[];
  check_story_share_type_enabled: boolean;
  fetch_eligibility_in_reel_viewer: boolean;
  fetch_eligibility_every_time_open_camera: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_camera_focus_low_end_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_vc_background_call_toast_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fb_sync_options_universe {
  igGroup: string;
  igAdditional: any[];
  show_fb_sync_options: boolean;
}
export interface QeSyncExperimentsIg_traffic_routing_universe {
  igGroup: string;
  igAdditional: any[];
  route_to_lla: boolean;
  is_in_lla_routing_experiment: boolean;
}
export interface QeSyncExperimentsIg_android_camera_effects_order_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_account_story_grid_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_payment_checkout_info {
  igGroup: string;
  igAdditional: any[];
  checkout_info_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_feed_defer_on_interactions {
  igGroup: string;
  igAdditional: any[];
  defer_on_save: boolean;
  defer_on_carousel_swipe: boolean;
  defer_on_like: boolean;
  include_bug_report: boolean;
  defer_on_single_tap: boolean;
}
export interface QeSyncExperimentsIg_android_test_not_signing_address_book_unlink_endpoint {
  igGroup: string;
  igAdditional: any[];
  do_not_sign: boolean;
}
export interface QeSyncExperimentsIg_android_branding_v2_settings_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_direct_android_mentions_sender {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_in_qe: boolean;
}
export interface QeSyncExperimentsIg_android_gallery_minimum_video_length {
  igGroup: string;
  igAdditional: any[];
  feed_video_minimum_length_ms: number;
}
export interface QeSyncExperimentsIg_android_hashtag_remove_share_hashtag {
  igGroup: string;
  igAdditional: any[];
  remove_share_hashtag: boolean;
}
export interface QeSyncExperimentsIg_shopping_checkout_2x2_platformization_universe {
  igGroup: string;
  igAdditional: any[];
  is_platformized: boolean;
}
export interface QeSyncExperimentsIg_android_show_create_content_pages_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_hashtag_limit {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_profile_ppr_fixes {
  igGroup: string;
  igAdditional: any[];
  enable_profile_shared_ppr_cache: boolean;
}
export interface QeSyncExperimentsIg_android_inline_composer_animation_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_hide_contacts_list {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_stories_reporting {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_business_attribute_sync {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_view_and_likes_cta_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_tango_cpu_overuse_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_branded_content_ads_enable_partner_boost {
  igGroup: string;
  igAdditional: any[];
  enable_carousel: boolean;
}
export interface QeSyncExperimentsIg_android_direct_remix_visual_messages {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_quick_story_placement_validation_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_search_impression_logging_viewpoint {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_camera_upsell_dialog {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ad_watchbrowse_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_delete_or_block_from_message_requests {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_reshare_reply_msg {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_camera_android_facetracker_v12_universe {
  igGroup: string;
  igAdditional: any[];
  in_control: boolean;
  is_exposed_when_downloaded: boolean;
  face_tracker_version_int: number;
}
export interface QeSyncExperimentsIg_badge_dedup_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_rx_thread_update {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_account_post_grid_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsArd_ig_broti_effect {
  igGroup: string;
  igAdditional: any[];
  use_brotli: boolean;
}
export interface QeSyncExperimentsIg_android_expanded_xposting_upsell_directly_after_sharing_story_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_promote_post_insights_entry_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_live_subscribe_user_level_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_video_call_finish_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled_finish_call_feed: boolean;
}
export interface QeSyncExperimentsIg_biz_growth_insights_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_promote_manager_improvements_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_promote_prefill_destination_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_dismiss_recent_searches {
  igGroup: string;
  igAdditional: any[];
  enable_recent_x_out_filtering: boolean;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_feed_camera_size_setter {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fb_link_ui_polish_universe {
  igGroup: string;
  igAdditional: any[];
  unlink_content_update_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_camera_gyro_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_ssim_report {
  igGroup: string;
  igAdditional: any[];
  igtv_report_ssim: boolean;
}
export interface QeSyncExperimentsIg_android_stories_combined_asset_search {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsInstagram_pcp_activity_feed_following_tab_universe {
  igGroup: string;
  igAdditional: any[];
  remove_following: boolean;
}
export interface QeSyncExperimentsIg_android_optic_photo_cropping_fixes {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_optic_new_architecture {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_push_notifications_settings_redesign_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_show_muted_accounts_page {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_vc_service_crash_fix_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_foreground_service_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ttcp_improvements {
  igGroup: string;
  igAdditional: any[];
  prefetch_colors: boolean;
}
export interface QeSyncExperimentsIg_share_to_story_toggle_include_shopping_product {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_interactions_verified_badge_on_comment_details {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fs_new_gallery_hashtag_prompts {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_story_ads_carousel_performance_universe_2 {
  igGroup: string;
  igAdditional: any[];
  opt_in_copy: string;
}
export interface QeSyncExperimentsIg_android_quick_conversion_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_vpvd_container_module_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_timeinapp_v1_universe {
  igGroup: string;
  igAdditional: any[];
  analytics_logging_enabled: boolean;
  instrumentation_enabled: boolean;
  usage_events_logging_enabled: boolean;
  heartbeat_rate_ms: number;
}
export interface QeSyncExperimentsIg_android_wellbeing_timeinapp_v1_migration {
  igGroup: string;
  igAdditional: any[];
  v1_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_shopping_variant_selector_redesign {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_video_streaming_upload_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled_streaming_upload_feed: boolean;
  is_enabled_streaming_upload_story_raven: boolean;
}
export interface QeSyncExperimentsIg_android_camera_tti_improvements {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_camera_android_release_drawing_view_universe {
  igGroup: string;
  igAdditional: any[];
  lazy_initialize_drawing_view: boolean;
}
export interface QeSyncExperimentsIg_android_music_story_fb_crosspost_universe {
  igGroup: string;
  igAdditional: any[];
  ig_android_music_story_fb_crosspost: boolean;
}
export interface QeSyncExperimentsIg_android_payments_growth_promote_payments_in_payments {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_smoke_animation_universe {
  igGroup: string;
  igAdditional: any[];
  fix_smoke_animation: boolean;
}
export interface QeSyncExperimentsIg_android_camera_platform_effect_share_universe {
  igGroup: string;
  igAdditional: any[];
  is_detail_view_enabled: boolean;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_audience {
  igGroup: string;
  igAdditional: any[];
  enable_close_friends_shortcut: boolean;
}
export interface QeSyncExperimentsIg_android_ig_branding_in_fb_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_hashtags_reporting {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_churned_find_friends_redirect_to_discover_people {
  igGroup: string;
  igAdditional: any[];
  is_push_notification: boolean;
  is_activity_feed: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fix_direct_badge_count_universe {
  igGroup: string;
  igAdditional: any[];
  only_trust_push_badge_on_background: boolean;
}
export interface QeSyncExperimentsIg_android_profile_follow_tab_hashtag_row_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_business_transaction_in_stories_creator {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_creator_quick_reply_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_biz_story_to_fb_page_improvement {
  igGroup: string;
  igAdditional: any[];
  fetch_biz_page_token_enabled: boolean;
  check_linking_fb_page_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_branded_content_insights_disclosure {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_adapter_leak_universe {
  igGroup: string;
  igAdditional: any[];
  fix_leak: boolean;
}
export interface QeSyncExperimentsIg_xposting_biz_feed_to_story_reshare {
  igGroup: string;
  igAdditional: any[];
  enable_biz_accounts_feed_to_story_reshares: boolean;
}
export interface QeSyncExperimentsIg_android_user_url_deeplink_fbpage_endpoint {
  igGroup: string;
  igAdditional: any[];
  enable_deeplink: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_cowatch_reporting {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_explore_use_shopping_endpoint {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_branded_content_settings_unsaved_changes_dialog {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_insights_native_post_universe {
  igGroup: string;
  igAdditional: any[];
  show_native: boolean;
}
export interface QeSyncExperimentsIg_android_dual_destination_quality_improvement {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_music_dash {
  igGroup: string;
  igAdditional: any[];
  should_use_dash: boolean;
}
export interface QeSyncExperimentsIg_android_insights_activity_tab_native_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_aggressive_media_cleanup {
  igGroup: string;
  igAdditional: any[];
  fix_audio_file: boolean;
  fix_tmp_file: boolean;
  aggressive_media_cleanup: boolean;
}
export interface QeSyncExperimentsIg_android_search_usl {
  igGroup: string;
  igAdditional: any[];
  is_search_results_page_enabled: boolean;
  is_instagram_search_results_enabled: boolean;
  is_instagram_search_session_initiated_enabled: boolean;
  is_search_results_dismiss_enabled: boolean;
  is_clear_search_history_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_business_tokenless_stories_xposting {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_xposting_newly_fbc_people {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_do_not_show_social_context_on_follow_list_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_list_adapter_prefetch_infra {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fix_inbox_flicker_universe {
  igGroup: string;
  igAdditional: any[];
  fix_inbox_flicker: boolean;
}
export interface QeSyncExperimentsIg_direct_feed_media_sticker_universe {
  igGroup: string;
  igAdditional: any[];
  enable_feed_media_sticker_in_direct: boolean;
}
export interface QeSyncExperimentsIg_android_test_remove_button_main_cta_self_followers_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_rollout: boolean;
  ranking_algo: string;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_live_reporting {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsAndroid_cameracore_safe_makecurrent_ig {
  igGroup: string;
  igAdditional: any[];
  use_safe_egl_make_current: boolean;
}
export interface QeSyncExperimentsIg_android_direct_simple_message_comparator_universe {
  igGroup: string;
  igAdditional: any[];
  test: boolean;
}
export interface QeSyncExperimentsIg_android_vc_face_effects_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_fbpage_on_profile_side_tray {
  igGroup: string;
  igAdditional: any[];
  enable_fbpage_profile_side_tray: boolean;
}
export interface QeSyncExperimentsIg_android_feed_delivery_refactor {
  igGroup: string;
  igAdditional: any[];
  refactor_is_defer_enabled: boolean;
}
export interface QeSyncExperimentsIg_branded_content_tagging_approval_request_flow_brand_side_v2 {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_profile_reporting {
  igGroup: string;
  igAdditional: any[];
  direct_account_enabled: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_search_register_recent_store {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_feed_posts_reporting {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_unified_inbox_ephemeral_media_tooltip_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_recents_and_edit_flow {
  igGroup: string;
  igAdditional: any[];
  recent_search_results_show_vertical_list_on_top: boolean;
  recent_search_results_show_vertical_list_on_top_users: boolean;
  edit_recent_search_results_in_settings_is_enabled: boolean;
  recent_search_truncated_amount: number;
}
export interface QeSyncExperimentsIg_android_delete_ssim_compare_img_soon {
  igGroup: string;
  igAdditional: any[];
  delete_compare_image_soon: boolean;
}
export interface QeSyncExperimentsIg_camera_android_boomerang_attribution_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_browse_long_press {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_mark_as_read_notif_action {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_cameracore_android_new_optic_camera2 {
  igGroup: string;
  igAdditional: any[];
  use_camera2: boolean;
  use_new_optic_if_camera1: boolean;
}
export interface QeSyncExperimentsIg_android_secondary_inbox_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_video_live_trace_universe {
  igGroup: string;
  igAdditional: any[];
}
export interface QeSyncExperimentsIg_android_camera_ar_platform_details_view_universe {
  igGroup: string;
  igAdditional: any[];
  is_direct_enabled: boolean;
  is_enabled: boolean;
  is_save_enabled: boolean;
  saved_effects_before_formats_count: number;
}
export interface QeSyncExperimentsQe_android_direct_vm_view_modes {
  igGroup: string;
  igAdditional: any[];
  show_view_mode_string_variants: boolean;
  remove_view_once: boolean;
}
export interface QeSyncExperimentsIg_android_flexible_profile_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_cold_start_logging_for_new_users_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_business_transaction_in_stories_consumer {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_business_cross_post_with_biz_id_infra {
  igGroup: string;
  igAdditional: any[];
  enable_tooltip: boolean;
  tooltip_impression_cap: number;
  tooltip_impression_delay_days: number;
}
export interface QeSyncExperimentsIg_android_fix_push_setting_logging_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_interactions_h2_2019_team_holdout_universe {
  igGroup: string;
  igAdditional: any[];
  is_h2_2019_holdout: string;
}
export interface QeSyncExperimentsIg_android_reel_tray_item_impression_logging_viewpoint {
  igGroup: string;
  igAdditional: any[];
  use_viewpoint: boolean;
}
export interface QeSyncExperimentsIg_android_ads_history_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_location_recyclerview {
  igGroup: string;
  igAdditional: any[];
  use_contextual_feed: boolean;
  context_feed_delay_initial_update: boolean;
  context_feed_limit_prefetch: boolean;
  use_viewpoint: boolean;
  use_recycler_view: boolean;
}
export interface QeSyncExperimentsIg_android_sidecar_report_ssim {
  igGroup: string;
  igAdditional: any[];
  sidecar_report_ssim: boolean;
}
export interface QeSyncExperimentsShop_home_hscroll_see_all_button_universe {
  igGroup: string;
  igAdditional: any[];
  show_creators_see_all_products: boolean;
}
export interface QeSyncExperimentsIg_android_ad_view_ads_native_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_throttled_search_requests {
  igGroup: string;
  igAdditional: any[];
  should_delay_search_request: boolean;
  search_request_delay_ms: number;
}
export interface QeSyncExperimentsIg_close_friends_v4_global {
  igGroup: string;
  igAdditional: any[];
  is_v4_enabled: boolean;
}
export interface QeSyncExperimentsIg_promote_hide_local_awareness_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_graphql_survey_new_proxy_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  cooldown_time: number;
  whitelist: null;
}
export interface QeSyncExperimentsIg_android_shopping_bag_optimization_universe {
  igGroup: string;
  igAdditional: any[];
  should_push_into_single_merchant_bag: boolean;
}
export interface QeSyncExperimentsIg_android_hashtag_recyclerview {
  igGroup: string;
  igAdditional: any[];
  use_recyclerview: boolean;
  use_contextual_feed: boolean;
  context_feed_delay_initial_update: boolean;
  store_shared_media: boolean;
  context_feed_limit_prefetch: boolean;
  use_viewpoint: boolean;
}
export interface QeSyncExperimentsIg_android_wishlist_reconsideration_universe {
  igGroup: string;
  igAdditional: any[];
  show_reconsideration_hscrolls: boolean;
}
export interface QeSyncExperimentsIg_camera_android_focus_in_post_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  focus_in_post_id: number;
}
export interface QeSyncExperimentsIg_android_ad_iab_qpl_kill_switch_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_prefetch_logic_infra {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  modulestateprovider_clear_onscreen_state: boolean;
}
export interface QeSyncExperimentsIg_android_self_story_button_non_fbc_accounts {
  igGroup: string;
  igAdditional: any[];
  is_expansion_enabled: boolean;
  fetch_eligibility_from_server_enabled: boolean;
  is_old_non_fbc_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_activator_cards {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
  force_show_upsell_in_contacts: boolean;
  max_thread_count_suggestions: number;
}
export interface QeSyncExperimentsIg_payment_checkout_cvv {
  igGroup: string;
  igAdditional: any[];
  checkout_cvv_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_scroll_main_feed {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_player_follow_button {
  igGroup: string;
  igAdditional: any[];
  show_follow_button: boolean;
}
export interface QeSyncExperimentsIg_android_hec_promote_universe {
  igGroup: string;
  igAdditional: any[];
  is_special_requirements_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_remove_follow_all_fb_list {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsUpdate_on_tranistion {
  ig_android_swipe_perf_improvements: QeSyncExperimentsIg_android_swipe_perf_improvements;
}
export interface QeSyncExperimentsIg_android_swipe_perf_improvements {
  igGroup: string;
  igAdditional: any[];
  update_on_transition: boolean;
  register_scroll_when_visible: boolean;
}
export interface QeSyncExperimentsNew_warning_content {
  ig_android_delayed_comments: QeSyncExperimentsIg_android_delayed_comments;
}
export interface QeSyncExperimentsIg_android_delayed_comments {
  igGroup: string;
  igAdditional: any[];
  is_comment_warning_enabled_detail: boolean;
  comment_warning_new_copy: boolean;
  is_comment_warning_enabled_ads: boolean;
  daily_cap: number;
  comment_warning_in_seconds: number;
}
export interface QeSyncExperimentsDeploy {
  ig_direct_holdout_h1_2019: QeSyncExperimentsIg_direct_holdout_h1_2019;
  ig_android_story_import_intent: QeSyncExperimentsIg_android_story_import_intent;
  ig_android_igtv_reshare: QeSyncExperimentsIg_android_igtv_reshare;
  ig_direct_holdout_h2_2018: QeSyncExperimentsIg_direct_holdout_h2_2018;
  ig_company_profile_holdout: QeSyncExperimentsIg_company_profile_holdout;
  ig_timestamp_public_test: QeSyncExperimentsIg_timestamp_public_test;
  ig_interactions_h1_2019_team_holdout_universe: QeSyncExperimentsIg_interactions_h1_2019_team_holdout_universe;
  ig_android_feed_post_sticker: QeSyncExperimentsIg_android_feed_post_sticker;
  igtv_feed_previews: QeSyncExperimentsIgtv_feed_previews;
  ig_android_igtv_autoplay_on_prepare: QeSyncExperimentsIg_android_igtv_autoplay_on_prepare;
  ig_profile_company_holdout_h2_2018: QeSyncExperimentsIg_profile_company_holdout_h2_2018;
}
export interface QeSyncExperimentsIg_direct_holdout_h1_2019 {
  igGroup: string;
  igAdditional: any[];
  behavior: string;
}
export interface QeSyncExperimentsIg_android_story_import_intent {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_reshare {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_direct_holdout_h2_2018 {
  igGroup: string;
  igAdditional: any[];
  behavior: string;
}
export interface QeSyncExperimentsIg_company_profile_holdout {
  igGroup: string;
  igAdditional: any[];
  is_business_profile_holdout: string;
  is_creation_communication_holdout: string;
  is_growth_holdout: string;
  is_home_discovery_holdout: string;
}
export interface QeSyncExperimentsIg_timestamp_public_test {
  igGroup: string;
  igAdditional: any[];
  is_seamless_holdout: boolean;
  is_feed_ui_holdout: boolean;
  behavior: string;
  experiment_flavor: string;
  timestamp_style: string;
}
export interface QeSyncExperimentsIg_interactions_h1_2019_team_holdout_universe {
  igGroup: string;
  igAdditional: any[];
  is_h1_2019_holdout: string;
}
export interface QeSyncExperimentsIg_android_feed_post_sticker {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIgtv_feed_previews {
  igGroup: string;
  igAdditional: any[];
  igtv_feed_preview_read_enabled: boolean;
  igtv_feed_preview_learn_more_url: null;
}
export interface QeSyncExperimentsIg_android_igtv_autoplay_on_prepare {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  visibility_threshold: number;
}
export interface QeSyncExperimentsIg_profile_company_holdout_h2_2018 {
  igGroup: string;
  igAdditional: any[];
  is_community_holdout: string;
  is_interests_holdout: string;
  is_profile_holdout: string;
  is_share_experiences_holdout: string;
}
export interface QeSyncExperimentsTest_no_vyml_new_cover {
  ig_explore_2019_h1_destination_cover: QeSyncExperimentsIg_explore_2019_h1_destination_cover;
}
export interface QeSyncExperimentsIg_explore_2019_h1_destination_cover {
  igGroup: string;
  igAdditional: any[];
  remove_vyml_overlay: boolean;
  use_destination_cover: boolean;
}
export interface QeSyncExperimentsLaunch {
  ig_android_explore_recyclerview_universe: QeSyncExperimentsIg_android_explore_recyclerview_universe;
  ig_android_promote_migration_available_audience: QeSyncExperimentsIg_android_promote_migration_available_audience;
  ig_android_ig_personal_account_to_fb_page_linkage_backfill: QeSyncExperimentsIg_android_ig_personal_account_to_fb_page_linkage_backfill;
  ig_android_country_code_fix_universe: QeSyncExperimentsIg_android_country_code_fix_universe;
  ig_android_save_to_collections_flow: QeSyncExperimentsIg_android_save_to_collections_flow;
  ig_business_integrity_ipc_universe: QeSyncExperimentsIg_business_integrity_ipc_universe;
  ig_android_ad_stories_scroll_perf_universe: QeSyncExperimentsIg_android_ad_stories_scroll_perf_universe;
  ig_android_gap_rule_enforcer_universe: QeSyncExperimentsIg_android_gap_rule_enforcer_universe;
  ig_android_enable_main_feed_reel_tray_preloading: QeSyncExperimentsIg_android_enable_main_feed_reel_tray_preloading;
  ig_android_feed_auto_share_to_facebook_dialog: QeSyncExperimentsIg_android_feed_auto_share_to_facebook_dialog;
  ig_android_skip_button_content_on_connect_fb_universe: QeSyncExperimentsIg_android_skip_button_content_on_connect_fb_universe;
  ig_android_stories_mixed_attribution_universe: QeSyncExperimentsIg_android_stories_mixed_attribution_universe;
  ig_android_promote_migration_gamma_universe: QeSyncExperimentsIg_android_promote_migration_gamma_universe;
  ig_new_eof_demarcator_universe: QeSyncExperimentsIg_new_eof_demarcator_universe;
  ig_android_viewpoint_stories_public_testing: QeSyncExperimentsIg_android_viewpoint_stories_public_testing;
  ig_android_stories_viewer_tall_android_cap_media_universe: QeSyncExperimentsIg_android_stories_viewer_tall_android_cap_media_universe;
  ig_android_stories_viewer_modal_activity: QeSyncExperimentsIg_android_stories_viewer_modal_activity;
  ig_explore_2018_post_chaining_account_recs_dedupe_universe: QeSyncExperimentsIg_explore_2018_post_chaining_account_recs_dedupe_universe;
  ig_android_promote_native_migration_universe: QeSyncExperimentsIg_android_promote_native_migration_universe;
  ig_android_search_without_typed_hashtag_autocomplete: QeSyncExperimentsIg_android_search_without_typed_hashtag_autocomplete;
  ig_android_stories_media_seen_batching_universe: QeSyncExperimentsIg_android_stories_media_seen_batching_universe;
  ig_android_share_publish_page_universe: QeSyncExperimentsIg_android_share_publish_page_universe;
  ig_android_video_pdq_calculation: QeSyncExperimentsIg_android_video_pdq_calculation;
}
export interface QeSyncExperimentsIg_android_explore_recyclerview_universe {
  igGroup: string;
  igAdditional: any[];
  is_grid_partial_updates_enabled: boolean;
  increase_view_pool: boolean;
  prefetch_position_correction: boolean;
  nullify_layout_manager: boolean;
  is_enabled: boolean;
  fast_scroll_to_top: boolean;
  child_prefetching: boolean;
  rebind_reels: boolean;
}
export interface QeSyncExperimentsIg_android_promote_migration_available_audience {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ig_personal_account_to_fb_page_linkage_backfill {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_country_code_fix_universe {
  igGroup: string;
  igAdditional: any[];
  guess: boolean;
}
export interface QeSyncExperimentsIg_android_save_to_collections_flow {
  igGroup: string;
  igAdditional: any[];
  is_snackbar_collection_upsell_enabled: boolean;
  is_media_save_upsell_redesign_enabled: boolean;
}
export interface QeSyncExperimentsIg_business_integrity_ipc_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ad_stories_scroll_perf_universe {
  igGroup: string;
  igAdditional: any[];
  enable_non_9_by_16_text_size_linear_optimization: boolean;
  enable_immediate_ad_warmup: boolean;
  disable_forced_adapter_update: boolean;
}
export interface QeSyncExperimentsIg_android_gap_rule_enforcer_universe {
  igGroup: string;
  igAdditional: any[];
  below_eof_demarcator_check_enabled: boolean;
  is_enabled: boolean;
  highest_position_check_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_enable_main_feed_reel_tray_preloading {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_feed_auto_share_to_facebook_dialog {
  igGroup: string;
  igAdditional: any[];
  dialog_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_skip_button_content_on_connect_fb_universe {
  igGroup: string;
  igAdditional: any[];
  skip_button_content: string;
}
export interface QeSyncExperimentsIg_android_stories_mixed_attribution_universe {
  igGroup: string;
  igAdditional: any[];
  is_mixed_attribution_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_promote_migration_gamma_universe {
  igGroup: string;
  igAdditional: any[];
  is_gamma_enabled: boolean;
}
export interface QeSyncExperimentsIg_new_eof_demarcator_universe {
  igGroup: string;
  igAdditional: any[];
  new_demarcator_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_viewpoint_stories_public_testing {
  igGroup: string;
  igAdditional: any[];
  use_legacy_keys: boolean;
  log_impression_events: boolean;
}
export interface QeSyncExperimentsIg_android_stories_viewer_tall_android_cap_media_universe {
  igGroup: string;
  igAdditional: any[];
  cap_media_size: boolean;
  cap_live_media: boolean;
}
export interface QeSyncExperimentsIg_android_stories_viewer_modal_activity {
  igGroup: string;
  igAdditional: any[];
  keep_bitmap_ref_ig_image_view: boolean;
}
export interface QeSyncExperimentsIg_explore_2018_post_chaining_account_recs_dedupe_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_promote_native_migration_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_beta_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_search_without_typed_hashtag_autocomplete {
  igGroup: string;
  igAdditional: any[];
  include_tagged: boolean;
}
export interface QeSyncExperimentsIg_android_stories_media_seen_batching_universe {
  igGroup: string;
  igAdditional: any[];
  request_compression_enabled: boolean;
  batching_enabled: boolean;
  batch_size: number;
}
export interface QeSyncExperimentsIg_android_share_publish_page_universe {
  igGroup: string;
  igAdditional: any[];
  show_dialog: boolean;
}
export interface QeSyncExperimentsIg_android_video_pdq_calculation {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  fps_num: number;
  image_quality: number;
  memory_threshold: number;
}
export interface QeSyncExperimentsTest_1 {
  ig_camera_android_subtle_filter_universe: QeSyncExperimentsIg_camera_android_subtle_filter_universe;
  ig_android_add_ci_upsell_in_normal_account_chaining_universe: QeSyncExperimentsIg_android_add_ci_upsell_in_normal_account_chaining_universe;
}
export interface QeSyncExperimentsIg_camera_android_subtle_filter_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  brightness: number;
  contrast: number;
  saturation: number;
}
export interface QeSyncExperimentsIg_android_add_ci_upsell_in_normal_account_chaining_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  position: number;
}
export interface QeSyncExperimentsTest_2 {
  ig_android_camera_effect_gallery_prefetch: QeSyncExperimentsIg_android_camera_effect_gallery_prefetch;
}
export interface QeSyncExperimentsIg_android_camera_effect_gallery_prefetch {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  has_visited: boolean;
}
export interface QeSyncExperimentsTest1 {
  ig_android_flexible_contact_and_category_for_creators: QeSyncExperimentsIg_android_flexible_contact_and_category_for_creators;
}
export interface QeSyncExperimentsIg_android_flexible_contact_and_category_for_creators {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsControl {
  ig_android_enable_zero_rating: QeSyncExperimentsIg_android_enable_zero_rating;
  ig_shopping_insights_wc_copy_update_android: QeSyncExperimentsIg_shopping_insights_wc_copy_update_android;
  ig_android_remove_unfollow_dialog_universe: QeSyncExperimentsIg_android_remove_unfollow_dialog_universe;
  ig_android_building_aymf_universe: QeSyncExperimentsIg_android_building_aymf_universe;
  ig_android_downloadable_json_universe: QeSyncExperimentsIg_android_downloadable_json_universe;
  ig_android_vc_cpu_overuse_universe: QeSyncExperimentsIg_android_vc_cpu_overuse_universe;
  ig_android_direct_message_follow_button: QeSyncExperimentsIg_android_direct_message_follow_button;
  instagram_stories_time_fixes: QeSyncExperimentsInstagram_stories_time_fixes;
  ig_android_smplt_universe: QeSyncExperimentsIg_android_smplt_universe;
  ig_android_dropframe_manager: QeSyncExperimentsIg_android_dropframe_manager;
  ig_promote_ctd_post_insights_universe: QeSyncExperimentsIg_promote_ctd_post_insights_universe;
  ig_android_iab_clickid_universe: QeSyncExperimentsIg_android_iab_clickid_universe;
  ig_android_interactions_hide_keyboard_onscroll: QeSyncExperimentsIg_android_interactions_hide_keyboard_onscroll;
  ig_feed_experience: QeSyncExperimentsIg_feed_experience;
  ig_promote_insights_video_views_universe: QeSyncExperimentsIg_promote_insights_video_views_universe;
  ig_android_anr: QeSyncExperimentsIg_android_anr;
  ig_android_story_ads_performance_universe_1: QeSyncExperimentsIg_android_story_ads_performance_universe_1;
  ig_android_mainfeed_generate_prefetch_background: QeSyncExperimentsIg_android_mainfeed_generate_prefetch_background;
  ig_android_edit_location_page_info: QeSyncExperimentsIg_android_edit_location_page_info;
  ig_android_stories_video_seeking_audio_bug_fix: QeSyncExperimentsIg_android_stories_video_seeking_audio_bug_fix;
  ig_android_iab_holdout_universe: QeSyncExperimentsIg_android_iab_holdout_universe;
  ig_android_experimental_onetap_dialogs_universe: QeSyncExperimentsIg_android_experimental_onetap_dialogs_universe;
  ig_android_no_bg_effect_tray_live_universe: QeSyncExperimentsIg_android_no_bg_effect_tray_live_universe;
  ig_android_contact_point_upload_rate_limit_killswitch: QeSyncExperimentsIg_android_contact_point_upload_rate_limit_killswitch;
  ig_android_ar_effect_sticker_consumption_universe: QeSyncExperimentsIg_android_ar_effect_sticker_consumption_universe;
  ig_branded_content_tagging_upsell: QeSyncExperimentsIg_branded_content_tagging_upsell;
  ig_android_igtv_upload_error_messages: QeSyncExperimentsIg_android_igtv_upload_error_messages;
  ig_android_xposting_reel_memory_share_universe: QeSyncExperimentsIg_android_xposting_reel_memory_share_universe;
  ig_android_mainactivity_trim_fix: QeSyncExperimentsIg_android_mainactivity_trim_fix;
  ag_family_bridges_2018_h2_holdout: QeSyncExperimentsAg_family_bridges_2018_h2_holdout;
  ig_android_fbc_upsell_on_dp_first_load: QeSyncExperimentsIg_android_fbc_upsell_on_dp_first_load;
  ig_android_rename_share_option_in_dialog_menu_universe: QeSyncExperimentsIg_android_rename_share_option_in_dialog_menu_universe;
  ufi_share: QeSyncExperimentsUfi_share;
  ig_android_downloadable_fonts_universe: QeSyncExperimentsIg_android_downloadable_fonts_universe;
  ig_android_igtv_whitelisted_for_web: QeSyncExperimentsIg_android_igtv_whitelisted_for_web;
  ig_promote_enter_error_screens_universe: QeSyncExperimentsIg_promote_enter_error_screens_universe;
  ig_android_specific_story_sharing: QeSyncExperimentsIg_android_specific_story_sharing;
}
export interface QeSyncExperimentsIg_android_enable_zero_rating {
  igGroup: string;
  igAdditional: any[];
  is_enabled: number;
}
export interface QeSyncExperimentsIg_shopping_insights_wc_copy_update_android {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_remove_unfollow_dialog_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  is_rollout: boolean;
}
export interface QeSyncExperimentsIg_android_building_aymf_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  show_follow_button: boolean;
  show_welcome_card: boolean;
}
export interface QeSyncExperimentsIg_android_downloadable_json_universe {
  igGroup: string;
  igAdditional: any[];
  use_remote_file: boolean;
}
export interface QeSyncExperimentsIg_android_vc_cpu_overuse_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_direct_message_follow_button {
  igGroup: string;
  igAdditional: any[];
  activity_enabled: boolean;
}
export interface QeSyncExperimentsInstagram_stories_time_fixes {
  igGroup: string;
  igAdditional: any[];
  reelitem_duration_reset_fix_is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_smplt_universe {
  igGroup: string;
  igAdditional: any[];
  always_show_pill: boolean;
}
export interface QeSyncExperimentsIg_android_dropframe_manager {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  should_drop_event: boolean;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_promote_ctd_post_insights_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  show_reach_as_metric_when_no_conversation_yet: boolean;
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_iab_clickid_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_interactions_hide_keyboard_onscroll {
  igGroup: string;
  igAdditional: any[];
  hide_keyboard_on_scroll_with_text: boolean;
  hide_keyboard_on_scroll: boolean;
}
export interface QeSyncExperimentsIg_feed_experience {
  igGroup: string;
  igAdditional: any[];
  early_organic_serialization: boolean;
}
export interface QeSyncExperimentsIg_promote_insights_video_views_universe {
  igGroup: string;
  igAdditional: any[];
}
export interface QeSyncExperimentsIg_android_anr {
  igGroup: string;
  igAdditional: any[];
  is_detector_enabled: boolean;
  is_reporter_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_story_ads_performance_universe_1 {
  igGroup: string;
  igAdditional: any[];
  remove_cta_entry_animation: boolean;
}
export interface QeSyncExperimentsIg_android_mainfeed_generate_prefetch_background {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_edit_location_page_info {
  igGroup: string;
  igAdditional: any[];
  is_edit_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_stories_video_seeking_audio_bug_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_iab_holdout_universe {
  igGroup: string;
  igAdditional: any[];
  in_holdout: boolean;
}
export interface QeSyncExperimentsIg_android_experimental_onetap_dialogs_universe {
  igGroup: string;
  igAdditional: any[];
  is_in_experiment: boolean;
  use_standard_font: boolean;
  show_redesign: boolean;
  design_dialog_type: number;
}
export interface QeSyncExperimentsIg_android_no_bg_effect_tray_live_universe {
  igGroup: string;
  igAdditional: any[];
  is_live_effect_tray_background_disabled: boolean;
}
export interface QeSyncExperimentsIg_android_contact_point_upload_rate_limit_killswitch {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ar_effect_sticker_consumption_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_branded_content_tagging_upsell {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_upload_error_messages {
  igGroup: string;
  igAdditional: any[];
  is_learn_more_link_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_xposting_reel_memory_share_universe {
  igGroup: string;
  igAdditional: any[];
  reel_memory_share_crossposting_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_mainactivity_trim_fix {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsAg_family_bridges_2018_h2_holdout {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  in_holdout: boolean;
}
export interface QeSyncExperimentsIg_android_fbc_upsell_on_dp_first_load {
  igGroup: string;
  igAdditional: any[];
  show_facepile: boolean;
  use_old_dialog: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsIg_android_rename_share_option_in_dialog_menu_universe {
  igGroup: string;
  igAdditional: any[];
  enable_rename: boolean;
}
export interface QeSyncExperimentsUfi_share {
  igGroup: string;
  igAdditional: any[];
  overflow_copy_link: boolean;
  v3_on_private_posts: boolean;
  overflow_everything_else: boolean;
  tooltip_after_overflow: boolean;
  overflow_share_to: boolean;
  overflow_wa_or_share_to: boolean;
  use_direct_share_sheet_actions: boolean;
  use_messenger_action: boolean;
  use_share_to_not_share_link: boolean;
  tooltip_after_dwelling: boolean;
  use_share_to_on_own: boolean;
  use_whatsapp_action: boolean;
  tall_uss: boolean;
  force_direct_reply: boolean;
}
export interface QeSyncExperimentsIg_android_downloadable_fonts_universe {
  igGroup: string;
  igAdditional: any[];
  use_remote_version: boolean;
}
export interface QeSyncExperimentsIg_android_igtv_whitelisted_for_web {
  igGroup: string;
  igAdditional: any[];
  is_whitelisted_for_longer_uploads: boolean;
}
export interface QeSyncExperimentsIg_promote_enter_error_screens_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_specific_story_sharing {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  enable_self_story_copy_link: boolean;
  self_story_show_share_link: boolean;
  enable_self_story_system_share: boolean;
  fetch_user_info: boolean;
  can_copy_link: boolean;
}
export interface QeSyncExperimentsRollout_high_volume {
  ig_android_place_signature_universe: QeSyncExperimentsIg_android_place_signature_universe;
}
export interface QeSyncExperimentsIg_android_place_signature_universe {
  igGroup: string;
  igAdditional: any[];
  is_cancel_on_exit_enabled: boolean;
  is_place_signature_enabled: boolean;
  is_story_and_quick_tag_signature_enabled: boolean;
  num_place_signature_collections: number;
}
export interface QeSyncExperimentsNo_cache_warmup_shared_thread {
  ig_android_direct_inbox_cache_universe: QeSyncExperimentsIg_android_direct_inbox_cache_universe;
}
export interface QeSyncExperimentsIg_android_direct_inbox_cache_universe {
  igGroup: string;
  igAdditional: any[];
  initialize_mutation_manager_on_start: boolean;
  use_independent_thread_for_mutation: boolean;
  remove_cache_warmup: boolean;
  dispatch_to_handler: boolean;
  fix_dispatch_from_disk: boolean;
}
export interface QeSyncExperimentsTest_v5 {
  ig_android_business_promote_tooltip: QeSyncExperimentsIg_android_business_promote_tooltip;
}
export interface QeSyncExperimentsIg_android_business_promote_tooltip {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsPass {
  ig_smb_ads_holdout_2018_h2_universe: QeSyncExperimentsIg_smb_ads_holdout_2018_h2_universe;
  ig_smb_ads_holdout_2019_h2_universe: QeSyncExperimentsIg_smb_ads_holdout_2019_h2_universe;
  ig_smb_ads_holdout_2019_h1_universe: QeSyncExperimentsIg_smb_ads_holdout_2019_h1_universe;
  ig_android_ad_holdout_watchandmore_universe: QeSyncExperimentsIg_android_ad_holdout_watchandmore_universe;
}
export interface QeSyncExperimentsIg_smb_ads_holdout_2018_h2_universe {
  igGroup: string;
  igAdditional: any[];
  show_tips: boolean;
  group_type: string;
}
export interface QeSyncExperimentsIg_smb_ads_holdout_2019_h2_universe {
  igGroup: string;
  igAdditional: any[];
  group_type: string;
}
export interface QeSyncExperimentsIg_smb_ads_holdout_2019_h1_universe {
  igGroup: string;
  igAdditional: any[];
  group_type: string;
}
export interface QeSyncExperimentsIg_android_ad_holdout_watchandmore_universe {
  igGroup: string;
  igAdditional: any[];
  behavior: string;
}
export interface QeSyncExperimentsTest_50 {
  ig_android_video_raven_bitrate_ladder_universe: QeSyncExperimentsIg_android_video_raven_bitrate_ladder_universe;
}
export interface QeSyncExperimentsIg_android_video_raven_bitrate_ladder_universe {
  igGroup: string;
  igAdditional: any[];
  raven_bitrate_percentage: number;
}
export interface QeSyncExperimentsTest_v3frx {
  ig_android_wellbeing_support_frx_comment_reporting: QeSyncExperimentsIg_android_wellbeing_support_frx_comment_reporting;
}
export interface QeSyncExperimentsIg_android_wellbeing_support_frx_comment_reporting {
  igGroup: string;
  igAdditional: any[];
  v3_flow_enabled: boolean;
  redesign_v2: boolean;
  v2_tags_enabled: boolean;
  enabled: boolean;
  spam_tag_param: null;
}
export interface QeSyncExperimentsTest_dedup_async_http {
  ig_android_push_reliability_universe: QeSyncExperimentsIg_android_push_reliability_universe;
}
export interface QeSyncExperimentsIg_android_push_reliability_universe {
  igGroup: string;
  igAdditional: any[];
  use_persistent_deduplication: boolean;
  thread_fetch_mode: string;
}
export interface QeSyncExperimentsTest_15_secs_10_seg {
  ig_android_stories_gallery_video_segmentation: QeSyncExperimentsIg_android_stories_gallery_video_segmentation;
}
export interface QeSyncExperimentsIg_android_stories_gallery_video_segmentation {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  max_num_segments: number;
  max_segment_duration_ms: number;
}
export interface QeSyncExperimentsTest_with_animation {
  ig_android_direct_sticker_search_upgrade_universe: QeSyncExperimentsIg_android_direct_sticker_search_upgrade_universe;
}
export interface QeSyncExperimentsIg_android_direct_sticker_search_upgrade_universe {
  igGroup: string;
  igAdditional: any[];
  should_animate_button: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsIncrement_075 {
  ig_stories_ads_delivery_rules: QeSyncExperimentsIg_stories_ads_delivery_rules;
}
export interface QeSyncExperimentsIg_stories_ads_delivery_rules {
  igGroup: string;
  igAdditional: any[];
}
export interface QeSyncExperimentsTest_4s {
  ig_android_video_upload_iframe_interval: QeSyncExperimentsIg_android_video_upload_iframe_interval;
}
export interface QeSyncExperimentsIg_android_video_upload_iframe_interval {
  igGroup: string;
  igAdditional: any[];
  iframe_interval_secs: number;
}
export interface QeSyncExperimentsFull_screen_nux_bottom_done {
  ig_close_friends_home_v2: QeSyncExperimentsIg_close_friends_home_v2;
}
export interface QeSyncExperimentsIg_close_friends_home_v2 {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  should_show_full_screen_nux: boolean;
  should_show_bottom_done_button: boolean;
}
export interface QeSyncExperimentsIg_android_react_native_email_sms_settings_universe {
  igGroup: string;
  igAdditional: any[];
  ig_android_react_native_email_sms_settings_universe_enabled: boolean;
}
export interface QeSyncExperimentsIg_android_ads_manager_pause_resume_ads_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsEnabled {
  ig_android_save_auto_sharing_to_fb_option_on_server: QeSyncExperimentsIg_android_save_auto_sharing_to_fb_option_on_server;
}
export interface QeSyncExperimentsIg_android_save_auto_sharing_to_fb_option_on_server {
  igGroup: string;
  igAdditional: any[];
  sync_server_in_story_viewer: boolean;
  sync_server_in_camera_enabled: boolean;
  sync_server_in_linked_accounts_page_enabled: boolean;
  enabled: boolean;
}
export interface QeSyncExperimentsDisable_options {
  ig_android_whitehat_options_universe: QeSyncExperimentsIg_android_whitehat_options_universe;
}
export interface QeSyncExperimentsIg_android_whitehat_options_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsRoll {
  ig_android_video_raven_passthrough: QeSyncExperimentsIg_android_video_raven_passthrough;
}
export interface QeSyncExperimentsIg_android_video_raven_passthrough {
  igGroup: string;
  igAdditional: any[];
  enable_raven_streaming_passthrough: boolean;
  enable_raven_passthrough_hint: boolean;
}
export interface QeSyncExperimentsProd_launch {
  ig_stories_ads_media_based_insertion: QeSyncExperimentsIg_stories_ads_media_based_insertion;
}
export interface QeSyncExperimentsIg_stories_ads_media_based_insertion {
  igGroup: string;
  igAdditional: any[];
  cache_layout: boolean;
  is_max_reel_gap_check_enabled: boolean;
  is_client_ad_pool_enabled: boolean;
  is_real_time_media_based_insertion_enabled: boolean;
  disable_django_story_gre: boolean;
  is_media_based_insertion_enabled_backend: boolean;
  min_gap_for_django_gre: number;
}
export interface QeSyncExperimentsNew_infra {
  ig_android_profile_neue_infra_rollout_universe: QeSyncExperimentsIg_android_profile_neue_infra_rollout_universe;
}
export interface QeSyncExperimentsIg_android_profile_neue_infra_rollout_universe {
  igGroup: string;
  igAdditional: any[];
  enable_neue_infra: boolean;
}
export interface QeSyncExperimentsTest_json_onpause {
  ig_android_view_info_universe: QeSyncExperimentsIg_android_view_info_universe;
}
export interface QeSyncExperimentsIg_android_view_info_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  store_adapter: string;
  write_strategy: string;
}
export interface QeSyncExperimentsButton {
  ig_android_pbia_proxy_profile_universe: QeSyncExperimentsIg_android_pbia_proxy_profile_universe;
}
export interface QeSyncExperimentsIg_android_pbia_proxy_profile_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  story_entry_point_enabled: boolean;
  bottom_of_profile_cta_style: string;
}
export interface QeSyncExperimentsControl_v1 {
  ig_android_qp_kill_switch: QeSyncExperimentsIg_android_qp_kill_switch;
}
export interface QeSyncExperimentsIg_android_qp_kill_switch {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncExperimentsTest_activity_status {
  ig_android_direct_left_aligned_navigation_bar: QeSyncExperimentsIg_android_direct_left_aligned_navigation_bar;
}
export interface QeSyncExperimentsIg_android_direct_left_aligned_navigation_bar {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  activity_status_enabled: boolean;
}
export interface QeSyncExperimentsJustdoit {
  ig_android_profile_neue_universe: QeSyncExperimentsIg_android_profile_neue_universe;
}
export interface QeSyncExperimentsIg_android_profile_neue_universe {
  igGroup: string;
  igAdditional: any[];
  enable_chaining_action_button: boolean;
  enable_follow_button_sheet: boolean;
  enable_header_inline_buttons: boolean;
  enable_archive_in_side_tray: boolean;
  enable_follow_similar_tab: boolean;
  enable_contextual_feed: boolean;
  enable_new_header_layout: boolean;
  enable_new_header_ye_olde: boolean;
  enable_context_feed_efficiency_improvements: boolean;
  enable_shopping_tab: boolean;
  ig_enable_contact_sheet: boolean;
  enable_fading_follow_button: boolean;
  enable_tab_swiping: boolean;
  enable_collapsible_bio: boolean;
  enable_igtv_tab: boolean;
}
export interface QeSyncExperimentsUse_trustedapp {
  ig_android_sso_use_trustedapp_universe: QeSyncExperimentsIg_android_sso_use_trustedapp_universe;
}
export interface QeSyncExperimentsIg_android_sso_use_trustedapp_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsTreatment {
  ig_android_vc_webrtc_params: QeSyncExperimentsIg_android_vc_webrtc_params;
}
export interface QeSyncExperimentsIg_android_vc_webrtc_params {
  igGroup: string;
  igAdditional: any[];
  max_video_width: number;
}
export interface QeSyncExperimentsControl_top5 {
  ig_fb_graph_differentiation: QeSyncExperimentsIg_fb_graph_differentiation;
}
export interface QeSyncExperimentsIg_fb_graph_differentiation {
  igGroup: string;
  igAdditional: any[];
  ig_fb_graph_differentiation_feature_limit: number;
  ig_fb_graph_differentiation_source_limit: number;
}
export interface QeSyncExperimentsExperiment {
  ig_android_growth_fci_team_holdout_universe: QeSyncExperimentsIg_android_growth_fci_team_holdout_universe;
}
export interface QeSyncExperimentsIg_android_growth_fci_team_holdout_universe {
  igGroup: string;
  igAdditional: any[];
  behavior: string;
}
export interface QeSyncExperimentsRelease {
  native_contact_invites_universe: QeSyncExperimentsNative_contact_invites_universe;
}
export interface QeSyncExperimentsNative_contact_invites_universe {
  igGroup: string;
  igAdditional: any[];
  filter_high_cost: boolean;
  is_enabled: boolean;
}
export interface QeSyncExperimentsTest_40mb {
  ig_android_image_upload_quality_universe: QeSyncExperimentsIg_android_image_upload_quality_universe;
}
export interface QeSyncExperimentsIg_android_image_upload_quality_universe {
  igGroup: string;
  igAdditional: any[];
  image_upload_ssim_enabled: boolean;
  image_ssim_calc_ram_threshold_mb: number;
}
export interface QeSyncExperimentsTest_pill {
  ig_android_main_feed_refresh_style_universe: QeSyncExperimentsIg_android_main_feed_refresh_style_universe;
}
export interface QeSyncExperimentsIg_android_main_feed_refresh_style_universe {
  igGroup: string;
  igAdditional: any[];
  reload_on_tap_scroll_to_top: boolean;
  reload_on_background: boolean;
  indicator_requires_view_visible: boolean;
  reload_on_view_disappeared: boolean;
  remove_uncacheable_items: boolean;
  indicator_requires_foreground: boolean;
  reload_on_scroll_to_top: boolean;
  use_pill_spring_animation: boolean;
  scroll_to_top_on_feed_refresh_start: boolean;
  defer_new_content_always: boolean;
  allow_defer_on_ptr: boolean;
  hide_indicator_on_stories: boolean;
  defer_enabled: boolean;
  defer_new_content_if_engaged: boolean;
  cached_feed_load_distance: number;
  defer_new_content_if_scrolled_below: string;
  scroll_to_top_action: string;
}
export interface QeSyncExperimentsTest_entrypoint {
  ig_android_rn_ads_manager_universe: QeSyncExperimentsIg_android_rn_ads_manager_universe;
}
export interface QeSyncExperimentsIg_android_rn_ads_manager_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  has_profile_entry_point: boolean;
}
export interface QeSyncExperimentsWith_local_prefill {
  ig_android_inline_editing_local_prefill: QeSyncExperimentsIg_android_inline_editing_local_prefill;
}
export interface QeSyncExperimentsIg_android_inline_editing_local_prefill {
  igGroup: string;
  igAdditional: any[];
  attempt_prefill: boolean;
  show_qp: boolean;
}
export interface QeSyncExperimentsBlacklisted {
  ig_vp9_hd_blacklist: QeSyncExperimentsIg_vp9_hd_blacklist;
}
export interface QeSyncExperimentsIg_vp9_hd_blacklist {
  igGroup: string;
  igAdditional: any[];
  is_blacklisted: boolean;
}
export interface QeSyncExperimentsTest5000ms {
  ig_android_render_output_surface_timeout_universe: QeSyncExperimentsIg_android_render_output_surface_timeout_universe;
}
export interface QeSyncExperimentsIg_android_render_output_surface_timeout_universe {
  igGroup: string;
  igAdditional: any[];
  render_surface_timeout_ms: number;
}
export interface QeSyncExperimentsClose_friends {
  ig_threads_app_close_friends_integration: QeSyncExperimentsIg_threads_app_close_friends_integration;
}
export interface QeSyncExperimentsIg_threads_app_close_friends_integration {
  igGroup: string;
  igAdditional: any[];
  use_close_friends: boolean;
}
export interface QeSyncExperimentsTest_skip_cover_pic {
  ig_android_fb_url_universe: QeSyncExperimentsIg_android_fb_url_universe;
}
export interface QeSyncExperimentsIg_android_fb_url_universe {
  igGroup: string;
  igAdditional: any[];
  show_use_fb_url: boolean;
  skip_has_cover_pic_check: boolean;
  skip_has_profile_pic_check: boolean;
}
export interface QeSyncExperimentsIg_android_logging_metric_universe_v2 {
  igGroup: string;
  igAdditional: any[];
  use_file_based_session: boolean;
  redirect_low_to_zero: boolean;
  use_idle_handler: boolean;
}
export interface QeSyncExperimentsShow_inapp_notif_no_tap_action {
  ig_android_visualcomposer_inapp_notification_universe: QeSyncExperimentsIg_android_visualcomposer_inapp_notification_universe;
}
export interface QeSyncExperimentsIg_android_visualcomposer_inapp_notification_universe {
  igGroup: string;
  igAdditional: any[];
  should_allow_notif_tap: boolean;
  should_show_inapp_notification: boolean;
}
export interface QeSyncExperimentsEnable_sso {
  ig_android_sso_kototoro_app_universe: QeSyncExperimentsIg_android_sso_kototoro_app_universe;
}
export interface QeSyncExperimentsIg_android_sso_kototoro_app_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncExperimentsAllow {
  ig_android_profile_thumbnail_impression: QeSyncExperimentsIg_android_profile_thumbnail_impression;
}
export interface QeSyncExperimentsIg_android_profile_thumbnail_impression {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncExperimentsAre_you_satisfied {
  ig_promote_net_promoter_score_universe: QeSyncExperimentsIg_promote_net_promoter_score_universe;
}
export interface QeSyncExperimentsIg_promote_net_promoter_score_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  question: string;
}
export interface QeSyncExperimentsTest2 {
  ig_android_discover_people_nux: QeSyncExperimentsIg_android_discover_people_nux;
}
export interface QeSyncExperimentsIg_android_discover_people_nux {
  igGroup: string;
  igAdditional: any[];
  variant: number;
}
export interface QeSyncExperimentsTest_cap {
  ig_camera_discovery_surface_universe: QeSyncExperimentsIg_camera_discovery_surface_universe;
}
export interface QeSyncExperimentsIg_camera_discovery_surface_universe {
  igGroup: string;
  igAdditional: any[];
  pre_fetch_enabled: boolean;
  is_discovery_search_enabled: boolean;
  should_show_hero_unit: boolean;
  should_cap_tray: boolean;
  is_enabled: boolean;
  pre_fetch_fresh_in_minutes: number;
  query_id: number;
}
export interface QeSyncExperimentsTest_use_timeout {
  saved_collections_cache_universe: QeSyncExperimentsSaved_collections_cache_universe;
}
export interface QeSyncExperimentsSaved_collections_cache_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
  should_use_timeout: boolean;
}
export interface QeSyncExperimentsTest_v2 {
  ig_android_unfollow_from_main_feed_v2: QeSyncExperimentsIg_android_unfollow_from_main_feed_v2;
}
export interface QeSyncExperimentsIg_android_unfollow_from_main_feed_v2 {
  igGroup: string;
  igAdditional: any[];
  max_number_of_followers: number;
  variant: null;
}
export interface QeSyncExperimentsTest_limit_2 {
  ig_android_self_story_setting_option_in_menu: QeSyncExperimentsIg_android_self_story_setting_option_in_menu;
}
export interface QeSyncExperimentsIg_android_self_story_setting_option_in_menu {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
  setting_dialog_times_limit: number;
}
export interface QeSyncExperimentsTest_no_tabs_hide_preview {
  ig_android_fs_creation_flow_tweaks: QeSyncExperimentsIg_android_fs_creation_flow_tweaks;
}
export interface QeSyncExperimentsIg_android_fs_creation_flow_tweaks {
  igGroup: string;
  igAdditional: any[];
  start_gallery_preview_offscreen: boolean;
  three_grid: boolean;
  remove_feed_camera: boolean;
}
export interface QeSyncExperimentsTest_new_definition_persist {
  ig_android_vpvd_impressions_universe: QeSyncExperimentsIg_android_vpvd_impressions_universe;
}
export interface QeSyncExperimentsIg_android_vpvd_impressions_universe {
  igGroup: string;
  igAdditional: any[];
  should_double_log: boolean;
  is_enabled: boolean;
  log_validation_events: boolean;
  use_new_definition: boolean;
  store_to_disk_max_count: number;
}
export interface QeSyncExperimentsEst_payload_640_1280_story_512 {
  ig_android_payload_based_scheduling: QeSyncExperimentsIg_android_payload_based_scheduling;
}
export interface QeSyncExperimentsIg_android_payload_based_scheduling {
  igGroup: string;
  igAdditional: any[];
  enable_onscreen_byte_fix: boolean;
  should_calculate_bytes_on_response_start: boolean;
  use_payload_size: boolean;
  use_image_pct: boolean;
  should_schedule_request_on_byte_update: boolean;
  cold_start_optimized: boolean;
  is_enabled: boolean;
  use_prefetch_expiration: boolean;
  can_use_default_payload_size_as_hint: boolean;
  should_update_bytes_before_request_callback: boolean;
  use_estimate_payload_hint_size: boolean;
  default_payload_img_kb: number;
  default_payload_other_kb: number;
  default_payload_video_kb: number;
  off_screen_byte_limit_kb: number;
  on_screen_byte_limit_kb: number;
  story_ad_prefetch_kb: number;
}
export interface QeSyncExperimentsUi {
  ig_android_time_spent_dashboard: QeSyncExperimentsIg_android_time_spent_dashboard;
}
export interface QeSyncExperimentsIg_android_time_spent_dashboard {
  igGroup: string;
  igAdditional: any[];
  is_enabled_in_profile_slideout: boolean;
  is_logging_enabled: boolean;
  logging_method: string;
}
