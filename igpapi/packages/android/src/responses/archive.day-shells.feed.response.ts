export interface ArchiveDayShellsFeedResponseRootObject {
  items: ArchiveDayShellsFeedResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  max_id: string | null;
  memories: ArchiveDayShellsFeedResponseMemories;
  status: string;
}
export interface ArchiveDayShellsFeedResponseItemsItem {
  timestamp: number;
  media_count: number;
  id: string;
  reel_type: string;
  latest_reel_media: number;
}
export interface ArchiveDayShellsFeedResponseMemories {
  items: any[];
}
