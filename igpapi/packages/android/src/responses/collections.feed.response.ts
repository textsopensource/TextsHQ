export interface CollectionsFeedResponseRootObject {
  items: CollectionsFeedResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  next_max_id?: string;
  auto_load_more_enabled: boolean;
  collection_id: string;
  collection_name: string;
  has_related_media: boolean;
  status: string;
}
export interface CollectionsFeedResponseItemsItem {
  media: CollectionsFeedResponseMedia;
}
export interface CollectionsFeedResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number | string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: CollectionsFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  user: CollectionsFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: CollectionsFeedResponsePreviewCommentsItem[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  like_count: number;
  has_liked: boolean;
  likers?: CollectionsFeedResponseLikersItem[];
  photo_of_you: boolean;
  caption: null | CollectionsFeedResponseCaption;
  fb_user_tags: CollectionsFeedResponseFb_user_tags;
  can_viewer_save: boolean;
  has_viewer_saved: boolean;
  saved_collection_ids: string[];
  organic_tracking_token: string;
  next_max_id?: string;
  usertags?: CollectionsFeedResponseUsertags;
}
export interface CollectionsFeedResponseImage_versions2 {
  candidates: CollectionsFeedResponseCandidatesItem[];
}
export interface CollectionsFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  estimated_scans_sizes: number[];
}
export interface CollectionsFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id?: string;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  can_boost_post?: boolean;
  can_see_organic_insights?: boolean;
  show_insights_terms?: boolean;
  reel_auto_archive?: string;
  is_unpublished?: boolean;
  allowed_commenter_type?: string;
  latest_reel_media?: number;
  friendship_status?: CollectionsFeedResponseFriendship_status;
  is_favorite?: boolean;
}
export interface CollectionsFeedResponseLikersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface CollectionsFeedResponseFb_user_tags {
  in: any[];
}
export interface CollectionsFeedResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface CollectionsFeedResponsePreviewCommentsItem {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: CollectionsFeedResponseUser;
  media_id: string;
  has_liked_comment: boolean;
  comment_like_count: number;
}
export interface CollectionsFeedResponseUsertags {
  in: CollectionsFeedResponseInItem[];
}
export interface CollectionsFeedResponseInItem {
  user: CollectionsFeedResponseUser;
  position: (string | number)[] | number[];
  start_time_in_video_in_sec: null;
  duration_in_video_in_sec: null;
}
export interface CollectionsFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: CollectionsFeedResponseUser;
  media_id: string;
}
