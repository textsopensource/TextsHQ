export interface TopicalExploreFeedResponseRootObject {
  sectional_items: TopicalExploreFeedResponseSectionalItemsItem[];
  rank_token: string;
  auto_load_more_enabled: boolean;
  more_available: boolean;
  next_max_id: string;
  max_id: string;
  clusters: TopicalExploreFeedResponseClustersItem[];
  status: string;
}
export interface TopicalExploreFeedResponseSectionalItemsItem {
  layout_type: string;
  layout_content: TopicalExploreFeedResponseLayout_content;
  feed_type: string;
  explore_item_info: TopicalExploreFeedResponseExplore_item_info;
}
export interface TopicalExploreFeedResponseLayout_content {
  two_by_two_item?: TopicalExploreFeedResponseTwo_by_two_item;
  fill_items?: TopicalExploreFeedResponseFillItemsItem[];
  medias?: TopicalExploreFeedResponseMediasItem[];
  one_by_two_item?: TopicalExploreFeedResponseOne_by_two_item;
}
export interface TopicalExploreFeedResponseTwo_by_two_item {
  media: TopicalExploreFeedResponseMedia;
}
export interface TopicalExploreFeedResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string | number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2?: TopicalExploreFeedResponseImage_versions2;
  original_width?: number;
  original_height?: number;
  is_dash_eligible?: number;
  video_dash_manifest?: string;
  video_codec?: string;
  number_of_qualities?: number;
  video_versions?: TopicalExploreFeedResponseVideoVersionsItem[];
  has_audio?: boolean;
  video_duration?: number;
  view_count?: number;
  location?: TopicalExploreFeedResponseLocation;
  lat?: number;
  lng?: number;
  user: TopicalExploreFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  top_likers: string[];
  photo_of_you: boolean;
  caption: TopicalExploreFeedResponseCaption | null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  explore_hide_comments: boolean;
  algorithm: string;
  connection_id: string;
  mezql_token: string;
  explore_context: string;
  explore_source_token: string;
  explore: TopicalExploreFeedResponseExplore;
  impression_token: string;
  usertags?: TopicalExploreFeedResponseUsertags;
  carousel_media_count?: number;
  carousel_media?: TopicalExploreFeedResponseCarouselMediaItem[];
  can_see_insights_as_brand?: boolean;
}
export interface TopicalExploreFeedResponseImage_versions2 {
  candidates: TopicalExploreFeedResponseCandidatesItem[];
}
export interface TopicalExploreFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  estimated_scans_sizes: number[];
}
export interface TopicalExploreFeedResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface TopicalExploreFeedResponseLocation {
  pk: number;
  name: string;
  address: string;
  city: string;
  short_name: string;
  lng?: number;
  lat?: number;
  external_source: string;
  facebook_places_id: number;
}
export interface TopicalExploreFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id?: string;
  friendship_status?: TopicalExploreFeedResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  is_unpublished?: boolean;
  is_favorite?: boolean;
  latest_reel_media?: number;
}
export interface TopicalExploreFeedResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
  blocking?: boolean;
  is_private?: boolean;
  incoming_request?: boolean;
}
export interface TopicalExploreFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: TopicalExploreFeedResponseUser;
  media_id: string;
  has_translation?: boolean;
}
export interface TopicalExploreFeedResponseExplore {
  explanation: string;
  actor_id: number;
  source_token: string;
}
export interface TopicalExploreFeedResponseFillItemsItem {
  media: TopicalExploreFeedResponseMedia;
}
export interface TopicalExploreFeedResponseUsertags {
  in: TopicalExploreFeedResponseInItem[];
}
export interface TopicalExploreFeedResponseInItem {
  user: TopicalExploreFeedResponseUser;
  position: number[] | string[];
  start_time_in_video_in_sec: null;
  duration_in_video_in_sec: null;
}
export interface TopicalExploreFeedResponseExplore_item_info {
  num_columns: number;
  total_num_columns: number;
  aspect_ratio: number;
  autoplay: boolean;
}
export interface TopicalExploreFeedResponseMediasItem {
  media: TopicalExploreFeedResponseMedia;
}
export interface TopicalExploreFeedResponseCarouselMediaItem {
  id: string;
  media_type: number;
  image_versions2: TopicalExploreFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  pk: string;
  carousel_parent_id: string;
  usertags?: TopicalExploreFeedResponseUsertags;
  video_versions?: TopicalExploreFeedResponseVideoVersionsItem[];
  video_duration?: number;
  is_dash_eligible?: number;
  video_dash_manifest?: string;
  video_codec?: string;
  number_of_qualities?: number;
}
export interface TopicalExploreFeedResponseOne_by_two_item {
  stories: TopicalExploreFeedResponseStories;
}
export interface TopicalExploreFeedResponseStories {
  id: string;
  is_portrait: boolean;
  design: string;
  type: string;
  seed_reel: TopicalExploreFeedResponseSeed_reel;
  chain_type: string;
}
export interface TopicalExploreFeedResponseSeed_reel {
  id: number;
  latest_reel_media: number;
  expiring_at: number;
  seen: number;
  can_reply: boolean;
  can_gif_quick_reply: boolean;
  can_reshare: boolean;
  reel_type: string;
  user: TopicalExploreFeedResponseUser;
  items: TopicalExploreFeedResponseItemsItem[];
  ranked_position: number;
  seen_ranked_position: number;
  prefetch_count: number;
}
export interface TopicalExploreFeedResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: TopicalExploreFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  is_dash_eligible: number;
  video_dash_manifest: string;
  video_codec: string;
  number_of_qualities: number;
  video_versions: TopicalExploreFeedResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  user: TopicalExploreFeedResponseUser;
  caption_is_edited: boolean;
  caption_position: number;
  is_reel_media: boolean;
  photo_of_you: boolean;
  caption: null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  expiring_at: number;
  imported_taken_at: number;
  can_reshare: boolean;
  can_reply: boolean;
  story_questions: TopicalExploreFeedResponseStoryQuestionsItem[];
  can_see_insights_as_brand: boolean;
  supports_reel_reactions: boolean;
  can_send_custom_emojis: boolean;
  show_one_tap_fb_share_tooltip: boolean;
}
export interface TopicalExploreFeedResponseStoryQuestionsItem {
  x: string;
  y: string;
  z: number;
  width: string;
  height: string;
  rotation: string;
  is_pinned: number;
  is_hidden: number;
  is_sticker: number;
  is_fb_sticker: number;
  question_sticker: TopicalExploreFeedResponseQuestion_sticker;
}
export interface TopicalExploreFeedResponseQuestion_sticker {
  question_type: string;
  question_id: string;
  question: string;
  media_id: string;
  text_color: string;
  background_color: string;
  viewer_can_interact: boolean;
  profile_pic_url: string;
}
export interface TopicalExploreFeedResponseClustersItem {
  id: string;
  title: string;
  context: string;
  description: string;
  labels: string[];
  type: string;
  name: string;
  can_mute: boolean;
  is_muted: boolean;
  debug_info?: string;
  ranked_position: number;
  followed_hashtags?: any[];
}
