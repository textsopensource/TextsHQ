export interface CollectionsListResponseRootObject {
  items: CollectionsListResponseItemsItem[];
  more_available: boolean;
  next_max_id?: string;
  auto_load_more_enabled: boolean;
  status: string;
}
export interface CollectionsListResponseItemsItem {
  collection_id: string;
  collection_name: string;
  collection_type: string;
  collection_media_count: number;
  cover_media_list?: CollectionsListResponseCoverMediaListItem[];
  cover_media?: CollectionsListResponseCover_media;
}
export interface CollectionsListResponseCoverMediaListItem {
  id: string;
  media_type: number;
  image_versions2: CollectionsListResponseImage_versions2;
  original_width: number;
  original_height: number;
}
export interface CollectionsListResponseImage_versions2 {
  candidates: CollectionsListResponseCandidatesItem[];
}
export interface CollectionsListResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  estimated_scans_sizes: number[];
}
export interface CollectionsListResponseCover_media {
  id: string;
  media_type: number;
  image_versions2: CollectionsListResponseImage_versions2;
  original_width: number;
  original_height: number;
}
