export interface AccountLogoutResponse {
  login_nonce: string;
  status: string;
}
