export interface ClipsMusicFeedResponseRootObject {
  items: ClipsMusicFeedResponseItemsItem[];
  paging_info: ClipsMusicFeedResponsePaging_info;
  audio_page_reporting_id: string;
  formatted_media_count: string;
  is_music_page_restricted: boolean;
  status: string;
}
export interface ClipsMusicFeedResponseItemsItem {
  media: ClipsMusicFeedResponseMedia;
}
export interface ClipsMusicFeedResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number | string;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: ClipsMusicFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  is_dash_eligible: number;
  video_dash_manifest: string;
  video_codec: string;
  number_of_qualities: number;
  video_versions: ClipsMusicFeedResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  view_count: number;
  play_count: number;
  user: ClipsMusicFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: ClipsMusicFeedResponsePreviewCommentsItem[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  likers?: ClipsMusicFeedResponseLikersItem[];
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  caption: ClipsMusicFeedResponseCaption | null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  product_type: string;
  clips_metadata: ClipsMusicFeedResponseClips_metadata;
  mezql_token: string;
  next_max_id?: string;
  creative_config?: ClipsMusicFeedResponseCreative_config;
  commenting_disabled_for_viewer?: boolean;
}
export interface ClipsMusicFeedResponseImage_versions2 {
  candidates: ClipsMusicFeedResponseCandidatesItem[];
  additional_candidates: ClipsMusicFeedResponseAdditional_candidates;
}
export interface ClipsMusicFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsMusicFeedResponseAdditional_candidates {
  igtv_first_frame: ClipsMusicFeedResponseIgtv_first_frame;
  first_frame: ClipsMusicFeedResponseFirst_frame;
}
export interface ClipsMusicFeedResponseIgtv_first_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsMusicFeedResponseFirst_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsMusicFeedResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface ClipsMusicFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id?: string;
  friendship_status?: ClipsMusicFeedResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  is_unpublished?: boolean;
  is_favorite?: boolean;
}
export interface ClipsMusicFeedResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface ClipsMusicFeedResponseLikersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface ClipsMusicFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsMusicFeedResponseUser;
  media_id: string;
  has_translation?: boolean;
}
export interface ClipsMusicFeedResponseClips_metadata {
  music_info: ClipsMusicFeedResponseMusic_info | null;
  original_sound_info: null | ClipsMusicFeedResponseOriginal_sound_info;
  featured_label: null | string;
}
export interface ClipsMusicFeedResponseMusic_info {
  music_asset_info: ClipsMusicFeedResponseMusic_asset_info;
  music_consumption_info: ClipsMusicFeedResponseMusic_consumption_info;
}
export interface ClipsMusicFeedResponseMusic_asset_info {
  audio_cluster_id: string;
  id: string;
  title: string;
  subtitle: string;
  display_artist: string;
  cover_artwork_uri: string;
  cover_artwork_thumbnail_uri: string;
  progressive_download_url: string;
  highlight_start_times_in_ms: number[];
  is_explicit: boolean;
  dash_manifest: string;
  has_lyrics: boolean;
  audio_asset_id: string;
  duration_in_ms: number;
  dark_message: null;
  allows_saving: boolean;
}
export interface ClipsMusicFeedResponseMusic_consumption_info {
  ig_artist: null;
  placeholder_profile_pic_url: string;
  should_mute_audio: boolean;
  should_mute_audio_reason: string;
  is_bookmarked: boolean;
  overlap_duration_in_ms: number;
  audio_asset_start_time_in_ms: number;
}
export interface ClipsMusicFeedResponsePreviewCommentsItem {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsMusicFeedResponseUser;
  media_id: string;
  has_translation?: boolean;
  parent_comment_id?: string;
}
export interface ClipsMusicFeedResponseCreative_config {
  capture_type: string;
  effect_ids: number[];
  effect_configs: ClipsMusicFeedResponseEffectConfigsItem[];
}
export interface ClipsMusicFeedResponseEffectConfigsItem {
  name: string;
  id: string;
  failure_reason: string;
  failure_code: string;
  gatekeeper: null;
  attribution_user_id: string;
  attribution_user: ClipsMusicFeedResponseAttribution_user;
  gatelogic: null;
  save_status: string;
  effect_actions: string[];
  is_spot_recognition_effect: boolean;
  is_spot_effect: boolean;
  thumbnail_image: ClipsMusicFeedResponseThumbnail_image;
  effect_action_sheet: ClipsMusicFeedResponseEffect_action_sheet;
  device_position: null;
}
export interface ClipsMusicFeedResponseAttribution_user {
  instagram_user_id: string;
  username: string;
  profile_picture: ClipsMusicFeedResponseProfile_picture;
}
export interface ClipsMusicFeedResponseProfile_picture {
  uri: string;
}
export interface ClipsMusicFeedResponseThumbnail_image {
  uri: string;
}
export interface ClipsMusicFeedResponseEffect_action_sheet {
  primary_actions: string[];
  secondary_actions: string[];
}
export interface ClipsMusicFeedResponsePaging_info {
  max_id: string;
  more_available: boolean;
}
export interface ClipsMusicFeedResponseOriginal_sound_info {
  audio_asset_id: number;
  progressive_download_url: string;
  dash_manifest: string;
  ig_artist: ClipsMusicFeedResponseIg_artist;
  should_mute_audio: boolean;
  original_media_id: string;
  hide_remixing: boolean;
  duration_in_ms: number;
  time_created: number;
}
export interface ClipsMusicFeedResponseIg_artist {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
