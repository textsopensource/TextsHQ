export interface CollectionsCreateResponseRootObject {
  collection_id: string;
  collection_name: string;
  collection_type: string;
  collection_media_count: number;
  status: string;
}
