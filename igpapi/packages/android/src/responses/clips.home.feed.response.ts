export interface ClipsHomeFeedResponseRootObject {
  items: ClipsHomeFeedResponseItemsItem[];
  paging_info: ClipsHomeFeedResponsePaging_info;
  status: string;
}
export interface ClipsHomeFeedResponseItemsItem {
  media: ClipsHomeFeedResponseMedia;
}
export interface ClipsHomeFeedResponseMedia {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: string | number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: ClipsHomeFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  is_dash_eligible: number;
  video_dash_manifest: string;
  video_codec: string;
  number_of_qualities: number;
  video_versions: ClipsHomeFeedResponseVideoVersionsItem[];
  has_audio: boolean;
  video_duration: number;
  view_count: number;
  play_count: number;
  user: ClipsHomeFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  next_max_id?: string;
  max_num_visible_preview_comments: number;
  preview_comments: ClipsHomeFeedResponsePreviewCommentsItem[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  like_count: number;
  has_liked: boolean;
  photo_of_you: boolean;
  can_see_insights_as_brand: boolean;
  caption: ClipsHomeFeedResponseCaption | null;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  product_type: string;
  clips_metadata: ClipsHomeFeedResponseClips_metadata;
  mezql_token: string;
}
export interface ClipsHomeFeedResponseImage_versions2 {
  candidates: ClipsHomeFeedResponseCandidatesItem[];
  additional_candidates: ClipsHomeFeedResponseAdditional_candidates;
}
export interface ClipsHomeFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsHomeFeedResponseAdditional_candidates {
  igtv_first_frame: ClipsHomeFeedResponseIgtv_first_frame;
  first_frame: ClipsHomeFeedResponseFirst_frame;
}
export interface ClipsHomeFeedResponseIgtv_first_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsHomeFeedResponseFirst_frame {
  width: number;
  height: number;
  url: string;
  scans_profile: string;
  estimated_scans_sizes: number[];
}
export interface ClipsHomeFeedResponseVideoVersionsItem {
  type: number;
  width: number;
  height: number;
  url: string;
  id: string;
}
export interface ClipsHomeFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id?: string;
  friendship_status?: ClipsHomeFeedResponseFriendship_status;
  is_verified: boolean;
  has_anonymous_profile_picture?: boolean;
  is_unpublished?: boolean;
  is_favorite?: boolean;
}
export interface ClipsHomeFeedResponseFriendship_status {
  following: boolean;
  outgoing_request: boolean;
  is_bestie: boolean;
  is_restricted: boolean;
}
export interface ClipsHomeFeedResponsePreviewCommentsItem {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsHomeFeedResponseUser;
  media_id: string;
  has_translation?: boolean;
  parent_comment_id?: string;
}
export interface ClipsHomeFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: ClipsHomeFeedResponseUser;
  media_id: string;
  has_translation?: boolean;
}
export interface ClipsHomeFeedResponseClips_metadata {
  music_info: null | ClipsHomeFeedResponseMusic_info;
  original_sound_info: ClipsHomeFeedResponseOriginal_sound_info | null;
  featured_label: null | string;
}
export interface ClipsHomeFeedResponseOriginal_sound_info {
  audio_asset_id: number | string;
  progressive_download_url: string;
  dash_manifest: string;
  ig_artist: ClipsHomeFeedResponseIg_artist;
  should_mute_audio: boolean;
  original_media_id: string;
  hide_remixing: boolean;
  duration_in_ms: number;
  time_created: number;
}
export interface ClipsHomeFeedResponseIg_artist {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface ClipsHomeFeedResponseMusic_info {
  music_asset_info: ClipsHomeFeedResponseMusic_asset_info;
  music_consumption_info: ClipsHomeFeedResponseMusic_consumption_info;
}
export interface ClipsHomeFeedResponseMusic_asset_info {
  audio_cluster_id: string;
  id: string;
  title: string;
  subtitle: string;
  display_artist: string;
  cover_artwork_uri: string;
  cover_artwork_thumbnail_uri: string;
  progressive_download_url: string;
  highlight_start_times_in_ms: number[];
  is_explicit: boolean;
  dash_manifest: string;
  has_lyrics: boolean;
  audio_asset_id: string;
  duration_in_ms: number;
  dark_message: null;
  allows_saving: boolean;
}
export interface ClipsHomeFeedResponseMusic_consumption_info {
  ig_artist: ClipsHomeFeedResponseIg_artist;
  placeholder_profile_pic_url: string;
  should_mute_audio: boolean;
  should_mute_audio_reason: string;
  is_bookmarked: boolean;
  overlap_duration_in_ms: number;
  audio_asset_start_time_in_ms: number;
}
export interface ClipsHomeFeedResponsePaging_info {
  max_id: string;
  more_available: boolean;
}
