export interface QeSyncLoginExperimentsRootObject {
  test: QeSyncLoginExperimentsTest;
  control: QeSyncLoginExperimentsControl;
  var1: QeSyncLoginExperimentsVar1;
  test_do_lookup_but_discard_result: QeSyncLoginExperimentsTest_do_lookup_but_discard_result;
  test_feed_badge: QeSyncLoginExperimentsTest_feed_badge;
  plaintext_only: QeSyncLoginExperimentsPlaintext_only;
  launch: QeSyncLoginExperimentsLaunch;
  rollout_1201: QeSyncLoginExperimentsRollout_1201;
  control_0108: QeSyncLoginExperimentsControl_0108;
  test_one_tap_check_box_hidden: QeSyncLoginExperimentsTest_one_tap_check_box_hidden;
  allusers: QeSyncLoginExperimentsAllusers;
}
export interface QeSyncLoginExperimentsTest {
  ig_android_account_linking_upsell_universe: QeSyncLoginExperimentsIg_android_account_linking_upsell_universe;
  ig_android_direct_add_direct_to_android_native_photo_share_sheet: QeSyncLoginExperimentsIg_android_direct_add_direct_to_android_native_photo_share_sheet;
  ig_android_mas_remove_close_friends_entrypoint: QeSyncLoginExperimentsIg_android_mas_remove_close_friends_entrypoint;
  ig_android_shared_email_reg_universe: QeSyncLoginExperimentsIg_android_shared_email_reg_universe;
  ig_android_push_fcm: QeSyncLoginExperimentsIg_android_push_fcm;
  ig_android_direct_send_like_from_notification: QeSyncLoginExperimentsIg_android_direct_send_like_from_notification;
  ig_android_prefetch_debug_dialog: QeSyncLoginExperimentsIg_android_prefetch_debug_dialog;
  ig_android_video_ffmpegutil_pts_fix: QeSyncLoginExperimentsIg_android_video_ffmpegutil_pts_fix;
  ig_android_multi_tap_login_new: QeSyncLoginExperimentsIg_android_multi_tap_login_new;
  ig_android_hide_contacts_list_in_nux: QeSyncLoginExperimentsIg_android_hide_contacts_list_in_nux;
  ig_android_ingestion_video_support_hevc_decoding: QeSyncLoginExperimentsIg_android_ingestion_video_support_hevc_decoding;
  ig_android_sim_info_upload: QeSyncLoginExperimentsIg_android_sim_info_upload;
  ig_android_targeted_one_tap_upsell_universe: QeSyncLoginExperimentsIg_android_targeted_one_tap_upsell_universe;
  ig_android_family_apps_user_values_provider_universe: QeSyncLoginExperimentsIg_android_family_apps_user_values_provider_universe;
  ig_android_add_account_button_in_profile_mas_universe: QeSyncLoginExperimentsIg_android_add_account_button_in_profile_mas_universe;
  ig_radio_button_universe_2: QeSyncLoginExperimentsIg_radio_button_universe_2;
  ig_android_modularized_dynamic_nux_universe: QeSyncLoginExperimentsIg_android_modularized_dynamic_nux_universe;
  ig_android_fb_account_linking_sampling_freq_universe: QeSyncLoginExperimentsIg_android_fb_account_linking_sampling_freq_universe;
  ig_android_fix_sms_read_lollipop: QeSyncLoginExperimentsIg_android_fix_sms_read_lollipop;
}
export interface QeSyncLoginExperimentsIg_android_account_linking_upsell_universe {
  igGroup: string;
  igAdditional: any[];
  logout_impression_limit: number;
}
export interface QeSyncLoginExperimentsIg_android_direct_add_direct_to_android_native_photo_share_sheet {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_mas_remove_close_friends_entrypoint {
  igGroup: string;
  igAdditional: any[];
  is_remove_entrypoint_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_shared_email_reg_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_push_fcm {
  igGroup: string;
  igAdditional: any[];
  job_scheduler_is_enabled: boolean;
  force_register_login: boolean;
  force_register_new: boolean;
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_direct_send_like_from_notification {
  igGroup: string;
  igAdditional: any[];
  is_send_inline_like_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_prefetch_debug_dialog {
  igGroup: string;
  igAdditional: any[];
}
export interface QeSyncLoginExperimentsIg_android_video_ffmpegutil_pts_fix {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  retry_count: number;
}
export interface QeSyncLoginExperimentsIg_android_multi_tap_login_new {
  igGroup: string;
  igAdditional: any[];
  show_logged_out_only: boolean;
  enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_hide_contacts_list_in_nux {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_ingestion_video_support_hevc_decoding {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  hevc_decoding_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_sim_info_upload {
  igGroup: string;
  igAdditional: any[];
  enable_upload: boolean;
}
export interface QeSyncLoginExperimentsIg_android_targeted_one_tap_upsell_universe {
  igGroup: string;
  igAdditional: any[];
  show_upsell: boolean;
  dialog: number;
}
export interface QeSyncLoginExperimentsIg_android_family_apps_user_values_provider_universe {
  igGroup: string;
  igAdditional: any[];
  should_provide_user_values: boolean;
}
export interface QeSyncLoginExperimentsIg_android_add_account_button_in_profile_mas_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_radio_button_universe_2 {
  igGroup: string;
  igAdditional: any[];
  enable_radio_buttons: boolean;
}
export interface QeSyncLoginExperimentsIg_android_modularized_dynamic_nux_universe {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_fb_account_linking_sampling_freq_universe {
  igGroup: string;
  igAdditional: any[];
  freq: number;
}
export interface QeSyncLoginExperimentsIg_android_fix_sms_read_lollipop {
  igGroup: string;
  igAdditional: any[];
  enabled: boolean;
}
export interface QeSyncLoginExperimentsControl {
  ig_android_prefill_main_account_username_on_login_screen_universe: QeSyncLoginExperimentsIg_android_prefill_main_account_username_on_login_screen_universe;
  ig_android_sign_in_password_visibility_universe: QeSyncLoginExperimentsIg_android_sign_in_password_visibility_universe;
  ig_android_registration_confirmation_code_universe: QeSyncLoginExperimentsIg_android_registration_confirmation_code_universe;
  ig_android_device_verification_fb_signup: QeSyncLoginExperimentsIg_android_device_verification_fb_signup;
}
export interface QeSyncLoginExperimentsIg_android_prefill_main_account_username_on_login_screen_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  is_prefill_main_account_username_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_sign_in_password_visibility_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_registration_confirmation_code_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_device_verification_fb_signup {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsVar1 {
  ig_android_suma_landing_page: QeSyncLoginExperimentsIg_android_suma_landing_page;
}
export interface QeSyncLoginExperimentsIg_android_suma_landing_page {
  igGroup: string;
  igAdditional: any[];
  show_generic_icon: boolean;
  show_title: boolean;
  skip_value_prop: boolean;
}
export interface QeSyncLoginExperimentsTest_do_lookup_but_discard_result {
  ig_activation_global_discretionary_sms_holdout: QeSyncLoginExperimentsIg_activation_global_discretionary_sms_holdout;
}
export interface QeSyncLoginExperimentsIg_activation_global_discretionary_sms_holdout {
  igGroup: string;
  igAdditional: any[];
  hsite_can_stop_lookups: boolean;
  hsite_prefill_holdout_group: string;
}
export interface QeSyncLoginExperimentsTest_feed_badge {
  ig_android_mas_notification_badging_universe: QeSyncLoginExperimentsIg_android_mas_notification_badging_universe;
}
export interface QeSyncLoginExperimentsIg_android_mas_notification_badging_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  show_feed_badge: boolean;
  show_badge: boolean;
  in_experiment: boolean;
}
export interface QeSyncLoginExperimentsPlaintext_only {
  ig_android_pwd_encrytpion: QeSyncLoginExperimentsIg_android_pwd_encrytpion;
}
export interface QeSyncLoginExperimentsIg_android_pwd_encrytpion {
  igGroup: string;
  igAdditional: any[];
  enc_pwd: boolean;
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsLaunch {
  ig_android_account_linking_on_concurrent_user_session_infra_universe: QeSyncLoginExperimentsIg_android_account_linking_on_concurrent_user_session_infra_universe;
  ig_android_account_linking_flow_shorten_universe: QeSyncLoginExperimentsIg_android_account_linking_flow_shorten_universe;
  ig_android_account_linking_universe: QeSyncLoginExperimentsIg_android_account_linking_universe;
}
export interface QeSyncLoginExperimentsIg_android_account_linking_on_concurrent_user_session_infra_universe {
  igGroup: string;
  igAdditional: any[];
  is_enabled: boolean;
}
export interface QeSyncLoginExperimentsIg_android_account_linking_flow_shorten_universe {
  igGroup: string;
  igAdditional: any[];
  show_child_selection: boolean;
}
export interface QeSyncLoginExperimentsIg_android_account_linking_universe {
  igGroup: string;
  igAdditional: any[];
  family_fetch_killswitch: boolean;
  is_enabled: boolean;
  one_tap_upsell_enabled: boolean;
  show_add_account_flow: boolean;
}
export interface QeSyncLoginExperimentsRollout_1201 {
  ig_android_gmail_oauth_in_reg: QeSyncLoginExperimentsIg_android_gmail_oauth_in_reg;
}
export interface QeSyncLoginExperimentsIg_android_gmail_oauth_in_reg {
  igGroup: string;
  igAdditional: any[];
  try_background_confirm: boolean;
}
export interface QeSyncLoginExperimentsControl_0108 {
  ig_android_hsite_prefill_new_carrier: QeSyncLoginExperimentsIg_android_hsite_prefill_new_carrier;
}
export interface QeSyncLoginExperimentsIg_android_hsite_prefill_new_carrier {
  igGroup: string;
  igAdditional: any[];
  prefill_enabled: boolean;
}
export interface QeSyncLoginExperimentsTest_one_tap_check_box_hidden {
  ig_android_passwordless_account_password_creation_universe: QeSyncLoginExperimentsIg_android_passwordless_account_password_creation_universe;
}
export interface QeSyncLoginExperimentsIg_android_passwordless_account_password_creation_universe {
  igGroup: string;
  igAdditional: any[];
  igLoggingId: string;
  upsell_for_mac_flow: boolean;
  show_save_password_checkbox: boolean;
}
export interface QeSyncLoginExperimentsAllusers {
  ig_android_security_intent_switchoff: QeSyncLoginExperimentsIg_android_security_intent_switchoff;
}
export interface QeSyncLoginExperimentsIg_android_security_intent_switchoff {
  igGroup: string;
  igAdditional: any[];
  switch_offs: string;
}
