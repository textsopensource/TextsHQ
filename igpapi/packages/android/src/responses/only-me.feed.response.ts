export interface OnlyMeFeedResponseRootObject {
  items: OnlyMeFeedResponseItemsItem[];
  num_results: number;
  more_available: boolean;
  auto_load_more_enabled: boolean;
  max_id: string | null;
  status: string;
}
export interface OnlyMeFeedResponseItemsItem {
  taken_at: number;
  pk: string;
  id: string;
  device_timestamp: number;
  media_type: number;
  code: string;
  client_cache_key: string;
  filter_type: number;
  image_versions2: OnlyMeFeedResponseImage_versions2;
  original_width: number;
  original_height: number;
  user: OnlyMeFeedResponseUser;
  can_viewer_reshare: boolean;
  caption_is_edited: boolean;
  comment_likes_enabled: boolean;
  comment_threading_enabled: boolean;
  has_more_comments: boolean;
  max_num_visible_preview_comments: number;
  preview_comments: any[];
  can_view_more_preview_comments: boolean;
  comment_count: number;
  inline_composer_display_condition: string;
  inline_composer_imp_trigger_time: number;
  like_count: number;
  has_liked: boolean;
  top_likers: any[];
  likers: OnlyMeFeedResponseLikersItem[];
  photo_of_you: boolean;
  caption: OnlyMeFeedResponseCaption;
  fb_user_tags: OnlyMeFeedResponseFb_user_tags;
  can_viewer_save: boolean;
  organic_tracking_token: string;
  visibility: string;
}
export interface OnlyMeFeedResponseImage_versions2 {
  candidates: OnlyMeFeedResponseCandidatesItem[];
}
export interface OnlyMeFeedResponseCandidatesItem {
  width: number;
  height: number;
  url: string;
  estimated_scans_sizes: number[];
}
export interface OnlyMeFeedResponseUser {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
  has_anonymous_profile_picture: boolean;
  can_boost_post: boolean;
  can_see_organic_insights: boolean;
  show_insights_terms: boolean;
  reel_auto_archive: string;
  is_unpublished: boolean;
  allowed_commenter_type: string;
  latest_reel_media: number;
}
export interface OnlyMeFeedResponseLikersItem {
  pk: number;
  username: string;
  full_name: string;
  is_private: boolean;
  profile_pic_url: string;
  profile_pic_id: string;
  is_verified: boolean;
}
export interface OnlyMeFeedResponseCaption {
  pk: string;
  user_id: number;
  text: string;
  type: number;
  created_at: number;
  created_at_utc: number;
  content_type: string;
  status: string;
  bit_flags: number;
  did_report_as_spam: boolean;
  share_enabled: boolean;
  user: OnlyMeFeedResponseUser;
  media_id: string;
}
export interface OnlyMeFeedResponseFb_user_tags {
  in: any[];
}
