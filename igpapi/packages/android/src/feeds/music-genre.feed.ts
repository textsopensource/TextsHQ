import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { IgAppModule } from '../types';
import { MusicGenreFeedResponseItemsItem, MusicGenreFeedResponseRootObject } from '../responses';

import { AndroidState } from '../core/android.state';

@Service({ transient: true, global: true })
export class MusicGenreFeed extends Feed<MusicGenreFeedResponseRootObject, MusicGenreFeedResponseItemsItem> {
  nextCursor?: string;

  public product?: IgAppModule = 'story_camera_music_overlay_post_capture';

  public id: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: MusicGenreFeedResponseRootObject) {
    this.nextCursor = response.page_info.next_max_id;
    return response.page_info.more_available;
  }

  items(raw: MusicGenreFeedResponseRootObject) {
    return raw.items;
  }

  async request(): Promise<MusicGenreFeedResponseRootObject> {
    const { body } = await this._http.full<MusicGenreFeedResponseRootObject>({
      url: `/api/v1/music/genres/${this.id}/`,
      method: 'POST',
      form: {
        cursor: this.nextCursor || '0',
        _csrftoken: this._state.cookies.csrfToken,
        product: this.product,
        _uuid: this._state.device.uuid,
        browse_session_id: this._state.clientSessionId,
      },
    });

    return body;
  }
}
