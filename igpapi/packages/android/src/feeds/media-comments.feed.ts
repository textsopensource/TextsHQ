import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { MediaCommentsFeedResponse, MediaCommentsFeedResponseCommentsItem } from '../responses';

@Service({ transient: true, global: true })
export class MediaCommentsFeed extends Feed<MediaCommentsFeedResponse, MediaCommentsFeedResponseCommentsItem> {
  id: string;

  nextMaxId?: string;
  nextMinId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: MediaCommentsFeedResponse) {
    this.nextMaxId = body.next_max_id;
    this.nextMinId = body.next_min_id;
    return !!body.next_max_id || !!body.next_min_id;
  }

  async request() {
    const { body } = await this._http.full<MediaCommentsFeedResponse>({
      url: `/api/v1/media/${this.id}/comments/`,
      searchParams: {
        can_support_threading: true,
        max_id: this.nextMaxId,
        min_id: this.nextMinId,
      },
    });

    return body;
  }

  items(raw: MediaCommentsFeedResponse) {
    return raw.comments;
  }
}
