import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

import { LikedFeedResponseItemsItem, LikedFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class LikedFeed extends Feed<LikedFeedResponseRootObject, LikedFeedResponseItemsItem> {
  maxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(response: LikedFeedResponseRootObject) {
    this.maxId = response.next_max_id?.toString();
    return response.more_available;
  }

  items({ items }: LikedFeedResponseRootObject) {
    return items;
  }

  async request(): Promise<LikedFeedResponseRootObject> {
    return this._http.body<LikedFeedResponseRootObject>({
      url: `/api/v1/feed/liked/`,
      method: 'GET',
      searchParams: {
        maxId: this.maxId,
      },
    });
  }
}
