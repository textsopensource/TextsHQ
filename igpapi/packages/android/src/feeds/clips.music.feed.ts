import { Service } from 'typedi';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { ClipsMusicFeedResponseItemsItem, ClipsMusicFeedResponseRootObject } from '../responses';
import { AndroidState } from '../core/android.state';

/**
 * The music feed (clips for a given sound)
 */
@Service({ transient: true, global: true })
export class ClipsMusicFeed extends Feed<ClipsMusicFeedResponseRootObject, ClipsMusicFeedResponseItemsItem> {
  /**
   * 'original' sound
   * See `ClipsMusicFeedResponseItemsItem.media.clips_metadata.original_sound_info.audio_asset_id`
   */
  originalAssetId?: string;

  /**
   * 'music' sound - **requires clusterId to be set**
   * See `ClipsMusicFeedResponseItemsItem.media.clips_metadata.music_info.music_asset_info.audio_asset_id`
   */
  assetId?: string;
  /**
   * 'music' sound - **requires assetId to be set**
   * See `ClipsMusicFeedResponseItemsItem.media.clips_metadata.music_info.music_asset_info.audio_cluster_id`
   */
  clusterId?: string;
  maxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: ClipsMusicFeedResponseRootObject): boolean {
    this.maxId = response.paging_info.max_id;
    return response.paging_info.more_available;
  }

  items(raw: ClipsMusicFeedResponseRootObject): ClipsMusicFeedResponseItemsItem[] {
    return raw.items ?? [];
  }

  protected async request(): Promise<ClipsMusicFeedResponseRootObject> {
    return this._http.body({
      url: '/api/v1/clips/music/',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        _uuid: this._state.extractUserId(),
        ...(this.originalAssetId
          ? {
              original_sound_audio_asset_id: this.originalAssetId,
            }
          : {
              audio_asset_id: this.assetId,
              audio_cluster_id: this.clusterId,
            }),
        max_id: this.maxId,
      },
    });
  }
}
