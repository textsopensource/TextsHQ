import { flatten } from 'lodash';

import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { LocationFeedResponse, LocationFeedResponseMedia } from '../responses';
import { AndroidState } from '../core/android.state';

export enum LocationFeedTab {
  recent = 'recent',
  ranked = 'ranked',
}

@Service({ transient: true, global: true })
export class LocationFeed extends Feed<LocationFeedResponse, LocationFeedResponseMedia> {
  id: string;
  tab?: LocationFeedTab = LocationFeedTab.ranked;

  nextMaxId?: string;
  nextPage?: number;
  nextMediaIds?: Array<string> = [];

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(body: LocationFeedResponse) {
    this.nextMaxId = body.next_max_id;
    this.nextPage = body.next_page;
    this.nextMediaIds = body.next_media_ids;
    return body.more_available;
  }

  public async request() {
    const { body } = await this._http.full<LocationFeedResponse>({
      url: `/api/v1/locations/${this.id}/sections/`,
      method: 'POST',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        tab: this.tab,
        _uuid: this._state.device.uuid,
        session_id: this._state.clientSessionId,
        page: this.nextPage,
        next_media_ids: this.nextPage ? JSON.stringify(this.nextMediaIds) : void 0,
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: LocationFeedResponse) {
    return flatten(raw.sections.map(section => section.layout_content.medias.map(medias => medias.media)));
  }
}
