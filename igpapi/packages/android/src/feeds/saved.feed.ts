import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { SavedFeedResponseMedia, SavedFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class SavedFeed extends Feed<SavedFeedResponseRootObject, SavedFeedResponseMedia> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: SavedFeedResponseRootObject) {
    this.nextMaxId = body.next_max_id;
    return body.more_available;
  }

  async request(): Promise<SavedFeedResponseRootObject> {
    const { body } = await this._http.full<SavedFeedResponseRootObject>({
      url: '/api/v1/feed/saved/',
      method: 'POST',
      searchParams: {
        max_id: this.nextMaxId,
        include_igtv_preview: false,
      },
    });

    return body;
  }

  items(raw: SavedFeedResponseRootObject) {
    const { items } = raw;
    return items.map(i => i.media);
  }
}
