import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

import { IgtvBrowseFeedResponseBrowseItemsItem, IgtvBrowseFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class IgtvBrowseFeed extends Feed<IgtvBrowseFeedResponseRootObject, IgtvBrowseFeedResponseBrowseItemsItem> {
  isPrefetch?: boolean = false;

  maxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(response: any) {
    this.maxId = response.max_id;
    return !!response.more_available;
  }

  items({ browse_items }: IgtvBrowseFeedResponseRootObject) {
    return browse_items;
  }

  async request(): Promise<IgtvBrowseFeedResponseRootObject> {
    return this._http.body<IgtvBrowseFeedResponseRootObject>({
      url: `/api/v1/igtv/${this.isPrefetch ? 'browse_feed' : 'non_prefetch_browse_feed'}/`,
      searchParams: {
        ...(this.isPrefetch ? { prefetch: 1 } : { max_id: this.maxId }),
      },
    });
  }
}
