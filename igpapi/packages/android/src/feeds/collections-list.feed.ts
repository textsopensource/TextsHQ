import { Feed } from '@textshq/core';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { CollectionsListResponseItemsItem, CollectionsListResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class CollectionsListFeed extends Feed<CollectionsListResponseRootObject, CollectionsListResponseItemsItem> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(response: CollectionsListResponseRootObject): boolean {
    this.nextMaxId = response.next_max_id;
    return response.more_available ?? !!this.nextMaxId;
  }

  items({ items }: CollectionsListResponseRootObject): CollectionsListResponseItemsItem[] {
    return items;
  }

  protected async request(): Promise<CollectionsListResponseRootObject> {
    return this._http.body({
      url: '/api/v1/collections/list/',
      searchParams: {
        collection_types: JSON.stringify(['ALL_MEDIA_AUTO_COLLECTION', 'PRODUCT_AUTO_COLLECTION', 'MEDIA']),
        max_id: this.nextMaxId,
      },
    });
  }
}
