import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { Service } from 'typedi';
import { ArchiveDayShellsFeedResponseItemsItem, ArchiveDayShellsFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class ArchiveDayShellsFeed extends Feed<
  ArchiveDayShellsFeedResponseRootObject,
  ArchiveDayShellsFeedResponseItemsItem
> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: ArchiveDayShellsFeedResponseRootObject): boolean {
    this.nextMaxId = response.max_id;
    return response.more_available;
  }

  items(raw: ArchiveDayShellsFeedResponseRootObject): ArchiveDayShellsFeedResponseItemsItem[] {
    return raw.items;
  }

  protected async request(): Promise<ArchiveDayShellsFeedResponseRootObject> {
    return this._http.body({
      url: '/api/v1/archive/reel/day_shells/',
      searchParams: {
        include_suggested_highlights: false,
        is_in_archive_home: true,
        include_cover: 0,
        timezone_offset: this._state.device.timezoneOffset,
        max_id: this.nextMaxId,
      },
    });
  }
}
