import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { Feed } from '@textshq/core';
import { AccountFollowingFeedResponse, AccountFollowingFeedResponseUsersItem } from '../responses';
import Chance from 'chance';

const chance = new Chance();

@Service({ transient: true, global: true })
export class AccountFollowingFeed extends Feed<AccountFollowingFeedResponse, AccountFollowingFeedResponseUsersItem> {
  id: string;

  nextMaxId?: string;

  rankToken?: string = chance.guid();

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: AccountFollowingFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return !!body.next_max_id;
  }

  async request() {
    const { body } = await this._http.full<AccountFollowingFeedResponse>({
      url: `/api/v1/friendships/${this.id}/following/`,
      searchParams: {
        rank_token: this.rankToken,
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items({ users }: AccountFollowingFeedResponse) {
    return users;
  }
}
