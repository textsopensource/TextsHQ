import { Service } from 'typedi';
import Chance from 'chance';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { PendingFriendshipsFeedResponse, PendingFriendshipsFeedResponseUsersItem } from '../responses';

const chance = new Chance();

@Service({ transient: true, global: true })
export class PendingFriendshipsFeed extends Feed<
  PendingFriendshipsFeedResponse,
  PendingFriendshipsFeedResponseUsersItem
> {
  nextMaxId?: string;

  rankToken?: string = chance.guid();

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: PendingFriendshipsFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return !!body.next_max_id;
  }

  async request() {
    const { body } = await this._http.full<PendingFriendshipsFeedResponse>({
      url: `/api/v1/friendships/pending/`,
      searchParams: {
        rank_token: this.rankToken,
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items({ users }: PendingFriendshipsFeedResponse) {
    return users;
  }
}
