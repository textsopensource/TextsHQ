import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { UserFeedResponse, UserFeedResponseItemsItem } from '../responses';

@Service({ transient: true, global: true })
export class UserFeed extends Feed<UserFeedResponse, UserFeedResponseItemsItem> {
  id: string;

  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: UserFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return body.more_available;
  }

  async request() {
    const { body } = await this._http.full<UserFeedResponse>({
      url: `/api/v1/feed/user/${this.id}/`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: UserFeedResponse) {
    return raw.items;
  }
}
