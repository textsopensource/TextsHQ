import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { Service } from 'typedi';
import { OnlyMeFeedResponseItemsItem, OnlyMeFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class OnlyMeFeed extends Feed<OnlyMeFeedResponseRootObject, OnlyMeFeedResponseItemsItem> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(response: OnlyMeFeedResponseRootObject): boolean {
    this.nextMaxId = response.max_id;
    return response.more_available;
  }

  items(raw: OnlyMeFeedResponseRootObject): OnlyMeFeedResponseItemsItem[] {
    return raw.items;
  }

  protected async request(): Promise<OnlyMeFeedResponseRootObject> {
    return this._http.body({
      url: `/api/v1/feed/only_me_feed/`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });
  }
}
