import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

import { UsertagsFeedResponseItemsItem, UsertagsFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class UsertagsFeed extends Feed<UsertagsFeedResponseRootObject, UsertagsFeedResponseItemsItem> {
  id: string;

  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: UsertagsFeedResponseRootObject) {
    this.nextMaxId = body.next_max_id;
    return body.more_available;
  }

  async request() {
    const { body } = await this._http.full<UsertagsFeedResponseRootObject>({
      url: `/api/v1/usertags/${this.id}/feed/`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: UsertagsFeedResponseRootObject) {
    return raw.items;
  }
}
