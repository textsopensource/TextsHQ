import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { DirectThreadFeedResponse, DirectThreadFeedResponseItemsItem } from '../responses';

@Service({ transient: true, global: true })
export class DirectThreadFeed extends Feed<DirectThreadFeedResponse, DirectThreadFeedResponseItemsItem> {
  public id: string;
  public seqId?: number;
  public cursor?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: DirectThreadFeedResponse) {
    this.cursor = body.thread.oldest_cursor;
    return body.thread.has_older;
  }

  async request() {
    const { body } = await this._http.full<DirectThreadFeedResponse>({
      url: `/api/v1/direct_v2/threads/${this.id}/`,
      searchParams: {
        visual_message_return_type: 'unseen',
        cursor: this.cursor,
        direction: 'older',
        seq_id: this.seqId,
        limit: 10,
      },
    });

    return body;
  }

  items({ thread }: DirectThreadFeedResponse) {
    return thread.items;
  }
}
