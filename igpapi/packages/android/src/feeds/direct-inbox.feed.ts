import { Service } from 'typedi';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { DirectInboxFeedResponse, DirectInboxFeedResponseThreadsItem } from '../responses';

@Service({ transient: true, global: true })
export class DirectInboxFeed extends Feed<DirectInboxFeedResponse, DirectInboxFeedResponseThreadsItem> {
  cursor?: string;
  seqId?: number;

  constructor(private _http: AndroidHttp) {
    super();
  }

  request() {
    return this._http.body<DirectInboxFeedResponse>({
      url: `/api/v1/direct_v2/inbox/`,
      searchParams: {
        visual_message_return_type: 'unseen',
        cursor: this.cursor,
        direction: this.cursor ? 'older' : void 0,
        seq_id: this.seqId,
        thread_message_limit: 10,
        persistentBadging: true,
        limit: 20,
      },
    });
  }

  items({ inbox }: DirectInboxFeedResponse) {
    return inbox.threads;
  }

  protected handle(body: DirectInboxFeedResponse) {
    this.seqId = body.seq_id;
    this.cursor = body.inbox.oldest_cursor;
    return body.inbox.has_older;
  }
}
