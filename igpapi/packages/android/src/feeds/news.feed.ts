import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { NewsFeedResponseRootObject, NewsFeedResponseStoriesItem } from '../responses';

@Service({ transient: true, global: true })
export class NewsFeed extends Feed<NewsFeedResponseRootObject, NewsFeedResponseStoriesItem> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: NewsFeedResponseRootObject) {
    this.nextMaxId = String(body.next_max_id);
    return !!body.next_max_id;
  }

  async request() {
    const { body } = await this._http.full<NewsFeedResponseRootObject>({
      url: `/api/v1/news`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: NewsFeedResponseRootObject) {
    return raw.stories;
  }
}
