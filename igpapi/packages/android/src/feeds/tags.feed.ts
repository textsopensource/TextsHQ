import { flatten } from 'lodash';

import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { TagsFeedResponse, TagsFeedResponseMedia } from '../responses';
import { AndroidState } from '../core/android.state';

export enum TagsFeedTab {
  top = 'top',
  recent = 'recent',
  places = 'places',
}

@Service({ transient: true, global: true })
export class TagsFeed extends Feed<TagsFeedResponse, TagsFeedResponseMedia> {
  tag: string;
  tab?: TagsFeedTab = TagsFeedTab.top;

  nextMaxId?: string;

  nextPage?: number;

  nextMediaIds?: Array<string> = [];

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(body: TagsFeedResponse) {
    this.nextMaxId = body.next_max_id;
    this.nextPage = body.next_page;
    this.nextMediaIds = body.next_media_ids;
    return body.more_available;
  }

  public async request() {
    const { body } = await this._http.full<TagsFeedResponse>({
      url: `/api/v1/tags/${encodeURI(this.tag)}/sections/`,
      method: 'POST',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        tab: this.tab,
        _uuid: this._state.device.uuid,
        session_id: this._state.clientSessionId,
        page: this.nextPage,
        next_media_ids: this.nextPage ? JSON.stringify(this.nextMediaIds) : void 0,
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: TagsFeedResponse) {
    return flatten(
      raw.sections.map(section => {
        if (section.layout_type !== 'media_grid') {
          return;
        }

        return section.layout_content.medias.map(medias => medias.media);
      }),
    );
  }
}
