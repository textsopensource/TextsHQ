import { Service } from 'typedi';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { UserStoryFeedResponseItemsItem, UserStoryFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class UserStoryFeed extends Feed<UserStoryFeedResponseRootObject, UserStoryFeedResponseItemsItem> {
  userId: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle() {
    return false;
  }

  items(raw: UserStoryFeedResponseRootObject) {
    return raw.reel.items;
  }

  async request(): Promise<UserStoryFeedResponseRootObject> {
    return this._http.body<UserStoryFeedResponseRootObject>({
      url: `/api/v1/feed/user/${this.userId}/story/`,
      method: 'GET',
      searchParams: {
        supported_capabilities_new: JSON.stringify(this._state.application.SUPPORTED_CAPABILITIES),
      },
    });
  }
}
