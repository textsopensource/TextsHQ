import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { ReelsTrayFeedResponseRootObject, ReelsTrayFeedResponseTrayItem } from '../responses';
import { AndroidState } from '../core/android.state';

export enum ReelsTrayFeedReason {
  cold_start = 'cold_start',
  pull_to_refresh = 'pull_to_refresh',
}

@Service({ transient: true, global: true })
export class ReelsTrayFeed extends Feed<ReelsTrayFeedResponseRootObject, ReelsTrayFeedResponseTrayItem> {
  reason?: ReelsTrayFeedReason = ReelsTrayFeedReason.cold_start;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle() {
    return false;
  }

  /**
   * Returns only the stories (without the broadcasts)
   */
  items(raw: ReelsTrayFeedResponseRootObject) {
    return raw.tray;
  }

  async request(): Promise<ReelsTrayFeedResponseRootObject> {
    const { body } = await this._http.full<ReelsTrayFeedResponseRootObject>({
      url: '/api/v1/feed/reels_tray/',
      method: 'POST',
      form: {
        supported_capabilities_new: this._state.application.SUPPORTED_CAPABILITIES,
        reason: this.reason,
        _csrftoken: this._state.cookies.csrfToken,
        _uuid: this._state.device.uuid,
      },
    });

    return body;
  }
}
