import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { Service } from 'typedi';
import { IgAppModule } from '../types';
import { ClipsHomeFeedResponseItemsItem, ClipsHomeFeedResponseRootObject } from '../responses';
import { AndroidState } from '../core/android.state';

/**
 * Reels Explore Feed
 */
@Service({ transient: true, global: true })
export class ClipsHomeFeed extends Feed<ClipsHomeFeedResponseRootObject, ClipsHomeFeedResponseItemsItem> {
  module?: IgAppModule = 'explore_popular_major_unit';
  maxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  items(raw: ClipsHomeFeedResponseRootObject): ClipsHomeFeedResponseItemsItem[] {
    return raw.items ?? [];
  }

  protected handle(response: ClipsHomeFeedResponseRootObject): boolean {
    this.maxId = response.paging_info.max_id;
    return response.paging_info.more_available;
  }

  protected request(): Promise<ClipsHomeFeedResponseRootObject> {
    return this._http.body({
      url: '/api/v1/clips/home/',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        _uuid: this._state.extractUserId(),
        container_module: this.module,
        max_id: this.maxId,
      },
    });
  }
}
