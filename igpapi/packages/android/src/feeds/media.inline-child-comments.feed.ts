import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

import {
  MediaInlineChildCommentsFeedResponseChildCommentsItem,
  MediaInlineChildCommentsFeedResponseRootObject,
} from '../responses';

@Service({ transient: true, global: true })
export class MediaInlineChildCommentsFeed extends Feed<
  MediaInlineChildCommentsFeedResponseRootObject,
  MediaInlineChildCommentsFeedResponseChildCommentsItem
> {
  mediaId: string;
  commentId: string;

  nextMaxId?: string;
  nextMinId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(state: MediaInlineChildCommentsFeedResponseRootObject) {
    this.nextMaxId = state.next_max_child_cursor;
    this.nextMinId = undefined;
    return !!state.next_max_child_cursor;
  }

  public request() {
    return this._http.body<MediaInlineChildCommentsFeedResponseRootObject>({
      url: `/api/v1/media/${this.mediaId}/comments/${this.commentId}/inline_child_comments/`,
      searchParams: {
        min_id: this.nextMinId,
        max_id: this.nextMaxId,
      },
    });
  }

  items(raw: MediaInlineChildCommentsFeedResponseRootObject) {
    return raw.child_comments;
  }
}
