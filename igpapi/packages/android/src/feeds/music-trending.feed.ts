import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { MusicTrendingFeedResponseItemsItem, MusicTrendingFeedResponseRootObject } from '../responses';
import { IgAppModule } from '../types';

import { AndroidState } from '../core/android.state';

@Service({ transient: true, global: true })
export class MusicTrendingFeed extends Feed<MusicTrendingFeedResponseRootObject, MusicTrendingFeedResponseItemsItem> {
  nextCursor?: string;

  public product?: IgAppModule = 'story_camera_music_overlay_post_capture';

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: MusicTrendingFeedResponseRootObject) {
    this.nextCursor = response.page_info.next_max_id;
    return response.page_info.more_available;
  }

  items(raw: MusicTrendingFeedResponseRootObject) {
    return raw.items;
  }

  async request(): Promise<MusicTrendingFeedResponseRootObject> {
    const { body } = await this._http.full<MusicTrendingFeedResponseRootObject>({
      url: '/api/v1/music/trending/',
      method: 'POST',
      form: {
        cursor: this.nextCursor || '0',
        _csrftoken: this._state.cookies.csrfToken,
        product: this.product,
        _uuid: this._state.device.uuid,
        browse_session_id: this._state.clientSessionId,
      },
    });

    return body;
  }
}
