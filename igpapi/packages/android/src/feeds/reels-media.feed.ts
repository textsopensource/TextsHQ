import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { ReelsMediaFeedResponse, ReelsMediaFeedResponseItem, ReelsMediaFeedResponseRootObject } from '../responses';
import { IgAppModule } from '../types';
import SUPPORTED_CAPABILITIES from '../samples/supported-capabilities.json';
import { AndroidState } from '../core/android.state';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class ReelsMediaFeed extends Feed<ReelsMediaFeedResponseRootObject, ReelsMediaFeedResponseItem> {
  userIds: Array<string>;
  source?: IgAppModule = 'feed_timeline';

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle() {
    return false;
  }

  async request() {
    const { body } = await this._http.full<ReelsMediaFeedResponseRootObject>({
      url: `/api/v1/feed/reels_media/`,
      method: 'POST',
      form: this._http.sign({
        user_ids: this.userIds,
        source: this.source,
        _uuid: this._state.device.uuid,
        _uid: this._state.cookies.userId,
        _csrftoken: this._state.cookies.csrfToken,
        device_id: this._state.device.id,
        supported_capabilities_new: JSON.stringify(SUPPORTED_CAPABILITIES),
      }),
    });
    return body;
  }

  items(raw: ReelsMediaFeedResponseRootObject) {
    return Object.values(raw.reels).reduce(
      (accumulator, current: ReelsMediaFeedResponse) => accumulator.concat(current.items),
      [],
    );
  }
}
