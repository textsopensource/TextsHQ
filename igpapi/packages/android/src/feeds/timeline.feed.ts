import { sample } from 'lodash';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { TimelineFeedResponse, TimelineFeedResponseMedia_or_ad } from '../responses';
import { AndroidState } from '../core/android.state';

// Should be declared before TimelineFeed for @nestjs/swagger/plugin
export enum TimelineFeedReason {
  pagination = 'pagination',
  pull_to_refresh = 'pull_to_refresh',
  warm_start_fetch = 'warm_start_fetch',
  cold_start_fetch = 'cold_start_fetch',
}

@Service({ transient: true, global: true })
export class TimelineFeed extends Feed<TimelineFeedResponse, TimelineFeedResponseMedia_or_ad> {
  // clutch for NestJS swagger plugin
  // https://github.com/nestjs/swagger/issues/815
  static randomReason(): TimelineFeedReason {
    return sample([
      TimelineFeedReason.pull_to_refresh,
      TimelineFeedReason.warm_start_fetch,
      TimelineFeedReason.cold_start_fetch,
    ]);
  }
  tag?: string; // ?

  nextMaxId?: string;
  reason?: TimelineFeedReason = TimelineFeed.randomReason();

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(body: TimelineFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return body.more_available;
  }

  async request(options: TimelineFeedsOptions = {}) {
    let form = {
      is_prefetch: '0',
      feed_view_info: '',
      seen_posts: '',
      phone_id: this._state.device.phoneId,
      is_pull_to_refresh: '0',
      battery_level: this._state.device.batteryLevel,
      timezone_offset: this._state.device.timezoneOffset,
      _csrftoken: this._state.cookies.csrfToken,
      client_session_id: this._state.clientSessionId,
      device_id: this._state.device.uuid,
      _uuid: this._state.device.uuid,
      is_charging: Number(this._state.device.isCharging),
      is_async_ads_in_headload_enabled: 0,
      rti_delivery_backend: 0,
      is_async_ads_double_request: 0,
      will_sound_on: 0,
      is_async_ads_rti: 0,
      recovered_from_crash: options.recoveredFromCrash,
      push_disabled: options.pushDisabled,
      latest_story_pk: options.latestStoryPk,
    };
    if (this.nextMaxId) {
      form = Object.assign(form, {
        max_id: this.nextMaxId,
        reason: options.reason || 'pagination',
      });
    } else {
      form = Object.assign(form, {
        reason: options.reason || this.reason,
        is_pull_to_refresh: this.reason === 'pull_to_refresh' ? '1' : '0',
      });
    }
    const { body } = await this._http.full<TimelineFeedResponse>({
      url: `/api/v1/feed/timeline/`,
      method: 'POST',
      headers: {
        'X-Ads-Opt-Out': '0',
        'X-Google-AD-ID': this._state.device.adid,
        'X-DEVICE-ID': this._state.device.uuid,
        'X-FB': '1',
      },
      form,
    });

    return body;
  }

  items(raw: TimelineFeedResponse) {
    return raw.feed_items.filter(i => i.media_or_ad).map(i => i.media_or_ad);
  }
}

export interface TimelineFeedsOptions {
  reason?: TimelineFeedReason;
  recoveredFromCrash?: string;
  pushDisabled?: boolean;
  latestStoryPk?: string | number;
}
