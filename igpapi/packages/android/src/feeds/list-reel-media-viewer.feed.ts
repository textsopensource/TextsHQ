import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { ListReelMediaViewerFeedResponseRootObject, ListReelMediaViewerFeedResponseUsersItem } from '../responses';

import { AndroidState } from '../core/android.state';

@Service({ transient: true, global: true })
export class ListReelMediaViewerFeed extends Feed<
  ListReelMediaViewerFeedResponseRootObject,
  ListReelMediaViewerFeedResponseUsersItem
> {
  mediaId: string;

  nextMaxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: ListReelMediaViewerFeedResponseRootObject) {
    this.nextMaxId = response.next_max_id;
    return this.nextMaxId !== null;
  }

  items({ users }: ListReelMediaViewerFeedResponseRootObject) {
    return users;
  }

  async request(): Promise<ListReelMediaViewerFeedResponseRootObject> {
    const { body } = await this._http.full<ListReelMediaViewerFeedResponseRootObject>({
      url: `/api/v1/media/${this.mediaId}/list_reel_media_viewer`,
      method: 'GET',
      searchParams: {
        supported_capabilities_new: JSON.stringify(this._state.application.SUPPORTED_CAPABILITIES),
        max_id: this.nextMaxId,
      },
    });

    return body;
  }
}
