import { Service } from 'typedi';
import Chance from 'chance';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { TagFeedResponse, TagFeedResponseItemsItem } from '../responses';

const chance = new Chance();

@Service({ transient: true, global: true })
export class TagFeed extends Feed<TagFeedResponse, TagFeedResponseItemsItem> {
  tag: string;

  nextMaxId?: string;

  rankToken?: string = chance.guid();

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: TagFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return body.more_available;
  }

  async request() {
    const { body } = await this._http.full<TagFeedResponse>({
      url: `/api/v1/feed/tag/${encodeURI(this.tag)}/`,
      searchParams: {
        rank_token: this.rankToken,
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items(raw: TagFeedResponse) {
    return raw.items;
  }
}
