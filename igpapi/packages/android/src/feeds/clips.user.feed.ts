import { Feed } from '@textshq/core';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { ClipsUserFeedResponseItemsItem, ClipsUserFeedResponseRootObject } from '../responses';
import { AndroidState } from '../core/android.state';

/**
 *  The user's Clips/Reels feed
 */
@Service({ transient: true, global: true })
export class ClipsUserFeed extends Feed<ClipsUserFeedResponseRootObject, ClipsUserFeedResponseItemsItem> {
  /**
   * The user's id
   */
  id: string | number;
  maxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  items(raw: ClipsUserFeedResponseRootObject): ClipsUserFeedResponseItemsItem[] {
    return raw.items ?? [];
  }

  protected handle(response: ClipsUserFeedResponseRootObject): boolean {
    this.maxId = response.paging_info.max_id;
    return response.paging_info.more_available;
  }

  protected request(): Promise<ClipsUserFeedResponseRootObject> {
    return this._http.body({
      url: '/api/v1/clips/user/',
      form: {
        _csrftoken: this._state.cookies.csrfToken,
        _uuid: this._state.extractUserId(),
        target_user_id: this.id.toString(),
        max_id: this.maxId,
      },
    });
  }
}
