import { Feed } from '@textshq/core';
import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { CollectionsFeedResponseItemsItem, CollectionsFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class CollectionFeed extends Feed<CollectionsFeedResponseRootObject, CollectionsFeedResponseItemsItem> {
  id: string;
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(response: CollectionsFeedResponseRootObject): boolean {
    this.nextMaxId = response.next_max_id;
    return response.more_available ?? !!this.nextMaxId;
  }

  items({ items }: CollectionsFeedResponseRootObject): CollectionsFeedResponseItemsItem[] {
    return items;
  }

  protected async request(): Promise<CollectionsFeedResponseRootObject> {
    return this._http.body({
      url: `/api/v1/feed/collection/${this.id}/`,
      searchParams: {
        max_id: this.nextMaxId,
        include_igtv_preview: false,
      },
    });
  }
}
