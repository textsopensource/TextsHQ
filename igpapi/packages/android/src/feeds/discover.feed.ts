import { Service } from 'typedi';
import { Feed } from '@textshq/core';
import { AndroidHttp } from '../core/android.http';
import { DiscoverFeedResponseRootObject, DiscoverFeedResponseSuggestionsItem } from '../responses';
import { AndroidState } from '../core/android.state';

@Service({ transient: true, global: true })
export class DiscoverFeed extends Feed<DiscoverFeedResponseRootObject, DiscoverFeedResponseSuggestionsItem> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(body: DiscoverFeedResponseRootObject) {
    this.nextMaxId = body.max_id;
    return body.more_available;
  }

  async request() {
    const { body } = await this._http.full<DiscoverFeedResponseRootObject>({
      url: `/api/v1/discover/ayml/`,
      method: 'POST',
      form: {
        max_id: this.nextMaxId,
        phone_id: this._state.device.phoneId,
        module: 'discover_people',
        _uuid: this._state.device.uuid,
        _csrftoken: this._state.cookies.csrfToken,
        paginate: true,
      },
    });

    return body;
  }

  items({ suggested_users }: DiscoverFeedResponseRootObject) {
    return suggested_users.suggestions;
  }
}
