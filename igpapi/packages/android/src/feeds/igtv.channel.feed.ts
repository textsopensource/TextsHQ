import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

import {
  IgtvChannelFeedResponseItemsItem,
  IgtvChannelFeedResponseRootObject,
} from '../responses/igtv.channel.feed.response';
import { AndroidState } from '../core/android.state';

@Service({ transient: true, global: true })
export class IgtvChannelFeed extends Feed<IgtvChannelFeedResponseRootObject, IgtvChannelFeedResponseItemsItem> {
  /**
   * A users channel id looks like this: `user_{id}`
   * A chaining (suggested videos) id looks like this: `chaining_{videoId}`
   */
  channelId: string;

  maxId?: string;

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: IgtvChannelFeedResponseRootObject) {
    this.maxId = response.max_id;
    return response.more_available;
  }

  async request(): Promise<IgtvChannelFeedResponseRootObject> {
    return this._http.body<IgtvChannelFeedResponseRootObject>({
      url: '/api/v1/igtv/channel/',
      form: {
        id: this.channelId,
        max_id: this.maxId,
        phone_id: this._state.device.phoneId,
        battery_level: this._state.device.batteryLevel,
        _csrftoken: this._state.cookies.csrfToken,
        _uuid: this._state.device.uuid,
        is_charging: this._state.device.isCharging ? '1' : '0',
        will_sound_on: '0',
      },
      method: 'POST',
    });
  }

  items({ items }: IgtvChannelFeedResponseRootObject) {
    return items;
  }
}
