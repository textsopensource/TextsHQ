import { Service } from 'typedi';

import { Feed } from '@textshq/core';
import { StoriesInsightsFeedResponseEdgesItem, StoriesInsightsFeedResponseRootObject } from '../responses';
import { AndroidState } from '../core/android.state';
import { AdsRepository } from '../repositories/ads.repository';

export enum StoriesInsightsFeedTimeframe {
  ONE_DAY = 'ONE_DAY',
  ONE_WEEK = 'ONE_WEEK',
  TWO_WEEKS = 'TWO_WEEKS',
}

@Service({ transient: true, global: true })
export class StoriesInsightsFeed extends Feed<
  StoriesInsightsFeedResponseRootObject,
  StoriesInsightsFeedResponseEdgesItem
> {
  timeframe: StoriesInsightsFeedTimeframe;

  nextCursor?: string;

  constructor(private _ads: AdsRepository, private _state: AndroidState) {
    super();
  }

  protected handle(response: StoriesInsightsFeedResponseRootObject) {
    const { end_cursor, has_next_page } = response.data.user.business_manager.stories_unit.stories.page_info;
    this.nextCursor = end_cursor;
    return has_next_page;
  }

  items(raw: StoriesInsightsFeedResponseRootObject) {
    return raw.data.user.business_manager.stories_unit.stories.edges;
  }

  async request(): Promise<StoriesInsightsFeedResponseRootObject> {
    return await this._ads.graphQL<StoriesInsightsFeedResponseRootObject>({
      surface: { friendlyName: 'IgInsightsStoryGridSurfaceQuery' },
      documentId: '1995528257207653',
      variables: {
        count: 15,
        cursor: this.nextCursor || null,
        IgInsightsGridMediaImage_SIZE: 256,
        queryParams: {
          access_token: '',
          id: this._state.cookies.userId,
        },
        timeframe: this.timeframe,
      },
    });
  }
}
