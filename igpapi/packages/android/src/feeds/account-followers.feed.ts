import { AndroidHttp } from '../core/android.http';
import { Feed } from '@textshq/core';
import { AccountFollowersFeedResponse, AccountFollowersFeedResponseUsersItem } from '../responses';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class AccountFollowersFeed extends Feed<AccountFollowersFeedResponse, AccountFollowersFeedResponseUsersItem> {
  id: string;

  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: AccountFollowersFeedResponse) {
    this.nextMaxId = body.next_max_id;
    return !!body.next_max_id;
  }

  async request() {
    const { body } = await this._http.full<AccountFollowersFeedResponse>({
      url: `/api/v1/friendships/${this.id}/followers/`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items({ users }: AccountFollowersFeedResponse) {
    return users;
  }
}
