import { Service } from 'typedi';

import { Feed } from '@textshq/core';
import { PostsInsightsFeedOptions } from '../types';
import { PostsInsightsFeedResponseEdgesItem, PostsInsightsFeedResponseRootObject } from '../responses';
import { AndroidState } from '../core/android.state';
import { AdsRepository } from '../repositories/ads.repository';

@Service({ transient: true, global: true })
export class PostsInsightsFeed extends Feed<PostsInsightsFeedResponseRootObject, PostsInsightsFeedResponseEdgesItem> {
  options: PostsInsightsFeedOptions;

  nextCursor?: string;

  constructor(private _ads: AdsRepository, private _state: AndroidState) {
    super();
  }

  protected handle(response: PostsInsightsFeedResponseRootObject) {
    const { end_cursor, has_next_page } = response.data.user.business_manager.top_posts_unit.top_posts.page_info;
    this.nextCursor = end_cursor;
    return has_next_page;
  }

  items(raw: PostsInsightsFeedResponseRootObject) {
    return raw.data.user.business_manager.top_posts_unit.top_posts.edges;
  }

  async request(): Promise<PostsInsightsFeedResponseRootObject> {
    return await this._ads.graphQL<PostsInsightsFeedResponseRootObject>({
      surface: { friendlyName: 'IgInsightsPostGridSurfaceQuery' },
      documentId: '1981884911894608',
      variables: {
        count: 15,
        cursor: this.nextCursor ?? null,
        IgInsightsGridMediaImage_SIZE: 256,
        queryParams: {
          access_token: '',
          id: this._state.cookies.userId,
        },
        ...this.options,
      },
    });
  }
}
