import { Feed } from '@textshq/core';
import { IgAppModule } from '../types';
import { AndroidHttp } from '../core/android.http';
import { AndroidState } from '../core/android.state';
import { Service } from 'typedi';
import { TopicalExploreFeedResponseRootObject, TopicalExploreFeedResponseSectionalItemsItem } from '../responses';

@Service({ transient: true, global: true })
export class TopicalExploreFeed extends Feed<
  TopicalExploreFeedResponseRootObject,
  TopicalExploreFeedResponseSectionalItemsItem
> {
  nextMaxId?: string = '0';
  module?: IgAppModule = 'explore_popular';
  clusterId? = 'explore_all:0';

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: TopicalExploreFeedResponseRootObject): boolean {
    this.nextMaxId = response.next_max_id ?? response.max_id;
    return response.more_available;
  }

  items(raw: TopicalExploreFeedResponseRootObject): TopicalExploreFeedResponseSectionalItemsItem[] {
    return raw.sectional_items;
  }

  protected async request(): Promise<TopicalExploreFeedResponseRootObject> {
    return this._http.body({
      url: '/api/v1/discover/topical_explore/',
      searchParams: {
        is_prefetch: false,
        omit_cover_media: true,
        max_id: this.nextMaxId,
        module: this.module,
        use_sectional_payload: true,
        timezone_offset: this._state.device.timezoneOffset,
        cluster_id: this.clusterId,
        session_id: this._state.clientSessionId,
        include_fixed_destinations: true,
      },
    });
  }
}
