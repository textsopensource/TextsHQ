import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';

@Service({ transient: true, global: true })
export class MediaStickerResponsesFeed<T = any, I = any> extends Feed<T, I> {
  name: string;
  rootName: string;
  itemName: string;

  stickerId: string;
  mediaId: string;

  maxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  items(raw: any): I[] {
    return raw[this.rootName][this.itemName];
  }

  protected handle(response: T) {
    this.maxId = (response as any)[this.rootName].max_id;
    return (response as any)[this.rootName].more_available;
  }

  async request(): Promise<T> {
    return this._http.body<T>({
      url: `/api/v1/media/${this.mediaId}/${this.stickerId}/${this.name}/`,
      method: 'GET',
      searchParams: {
        max_id: this.maxId || void 0,
      },
    });
  }
}
