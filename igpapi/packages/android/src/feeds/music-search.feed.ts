import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';

import { Feed } from '@textshq/core';
import { IgAppModule } from '../types';
import { MusicSearchFeedResponseItemsItem, MusicSearchFeedResponseRootObject } from '../responses';

import { AndroidState } from '../core/android.state';
import Chance from 'chance';

@Service({ transient: true, global: true })
export class MusicSearchFeed extends Feed<MusicSearchFeedResponseRootObject, MusicSearchFeedResponseItemsItem> {
  protected nextCursor?: string;

  public product?: IgAppModule = 'story_camera_music_overlay_post_capture';

  public query: string;

  public searchSessionId? = new Chance(this.query).guid({ version: 4 });

  constructor(private _http: AndroidHttp, private _state: AndroidState) {
    super();
  }

  protected handle(response: any) {
    this.nextCursor = response.page_info.next_max_id;
    return response.page_info.more_available;
  }

  items(raw: MusicSearchFeedResponseRootObject) {
    return raw.items;
  }

  async request(): Promise<MusicSearchFeedResponseRootObject> {
    const { body } = await this._http.full<MusicSearchFeedResponseRootObject>({
      url: '/api/v1/music/search/',
      method: 'POST',
      form: {
        cursor: this.nextCursor || '0',
        _csrftoken: this._state.cookies.csrfToken,
        product: this.product,
        _uuid: this._state.device.uuid,
        browse_session_id: this._state.clientSessionId,
        search_session_id: this.searchSessionId,
        q: this.query,
      },
    });

    return body;
  }
}
