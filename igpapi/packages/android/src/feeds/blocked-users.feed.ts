import { Service } from 'typedi';
import { AndroidHttp } from '../core/android.http';
import { Feed } from '@textshq/core';
import { BlockedUsersFeedResponseBlockedListItem, BlockedUsersFeedResponseRootObject } from '../responses';

@Service({ transient: true, global: true })
export class BlockedUsersFeed extends Feed<
  BlockedUsersFeedResponseRootObject,
  BlockedUsersFeedResponseBlockedListItem
> {
  nextMaxId?: string;

  constructor(private _http: AndroidHttp) {
    super();
  }

  protected handle(body: BlockedUsersFeedResponseRootObject) {
    this.nextMaxId = body.next_max_id;
    return !!body.next_max_id;
  }

  async request() {
    const { body } = await this._http.full<BlockedUsersFeedResponseRootObject>({
      url: `/api/v1/users/blocked_list/`,
      searchParams: {
        max_id: this.nextMaxId,
      },
    });

    return body;
  }

  items({ blocked_list }: BlockedUsersFeedResponseRootObject) {
    return blocked_list;
  }
}
