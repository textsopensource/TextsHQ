# `@textshq/chrome`

> Instagram automation using headless chrome browser

## Usage

```typescript
import { AndroidIgpapi } from '@textshq/android';
import { ChromeIgpapiFactory } from '@textshq/chrome';

(async () => {
  const igpapi = new AndroidIgpapi();
  await igpapi.simulate.bootstrap({
    credentials: {
      username: 'username',
      password: 'password',
    },
  });
  const chrome = await ChromeIgpapiFactory.create(igpapi.state);
  const mediaPage = await igpapi.page.profile({ username: 'real_scrip' });
  await mediaPage.follow();
})();
```
