import { Browser as PuppeeterBrowser, BrowserContext, BrowserEventObj, Page, Target, Timeoutable } from 'puppeteer';
import { ChildProcess } from 'child_process';

export abstract class ChromeBrowser implements PuppeeterBrowser {
  abstract addListener(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract browserContexts(): BrowserContext[];

  abstract close(): Promise<void>;

  abstract createIncognitoBrowserContext(): Promise<BrowserContext>;

  abstract defaultBrowserContext(): BrowserContext;

  abstract disconnect(): void;

  abstract emit(event: string | symbol, ...args: any[]): boolean;

  abstract eventNames(): Array<string | symbol>;

  abstract getMaxListeners(): number;

  abstract isConnected(): boolean;

  abstract listenerCount(type: string | symbol): number;

  abstract listeners(event: string | symbol): Function[];

  abstract newPage(): Promise<Page>;

  abstract off(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract on<K extends keyof BrowserEventObj>(
    eventName: K,
    handler: (e: BrowserEventObj[K], ...args: any[]) => void,
  ): this;
  abstract on(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract once<K extends keyof BrowserEventObj>(
    eventName: K,
    handler: (e: BrowserEventObj[K], ...args: any[]) => void,
  ): this;
  abstract once(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract pages(): Promise<Page[]>;

  abstract prependListener(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract prependOnceListener(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract process(): ChildProcess;

  abstract rawListeners(event: string | symbol): Function[];

  abstract removeAllListeners(event?: string | symbol): this;

  abstract removeListener(event: string | symbol, listener: (...args: any[]) => void): this;

  abstract setMaxListeners(n: number): this;

  abstract target(): Target;

  abstract targets(): Promise<Target[]>;

  abstract userAgent(): Promise<string>;

  abstract version(): Promise<string>;

  abstract waitForTarget(predicate: (target: Target) => boolean, options?: Timeoutable): Promise<Target>;

  abstract wsEndpoint(): string;
}
