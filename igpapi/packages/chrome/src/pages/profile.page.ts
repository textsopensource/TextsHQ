import { Service } from 'typedi';
import { Page } from 'puppeteer';
import { IgpapiPage } from '../igpapi.page';

@Service({ transient: true, global: true })
export class ProfilePage implements IgpapiPage {
  page: Page;
  readonly username: string;

  get path() {
    return `/${this.username}/`;
  }

  async follow() {
    await this.page.click('header section div:first-child div span:first-child span button');
  }
}
