import { Service } from 'typedi';
import { Page } from 'puppeteer';
import { IgpapiPage } from '../igpapi.page';

@Service({ transient: true, global: true })
export class MediaPage implements IgpapiPage {
  page: Page;

  readonly shortcode: string;

  get path() {
    return `/p/${this.shortcode}/`;
  }

  async inverseLike() {
    await this.page.click('article section:first-child button');
  }
}
