import { Service } from 'typedi';
import { Page } from 'puppeteer';
import { IgpapiPage } from '../igpapi.page';
import { PagesFactory } from '../pages.factory';
import { ChallengePage } from './challenge';

@Service({ transient: true, global: true })
export class LoginPage implements IgpapiPage {
  public page: Page;

  constructor(private pagesFactory: PagesFactory) {}

  get path() {
    return `/accounts/login/`;
  }

  private static isCheckpoint(body: LoginPageResponse): body is LoginCheckpointResponseRootObject {
    return body.status === 'fail' && body.message === 'checkpoint_required';
  }

  async logIn(credentials: LoginCredentials) {
    await this.page.waitForSelector('[name="username"]');
    await this.page.focus('[name="username"]');
    await this.page.keyboard.type(credentials.username);
    await this.page.focus('[name="password"]');
    await this.page.keyboard.type(credentials.password);
    await this.page.click('form button[type="submit"]');
    const httpResponse = await this.page.waitForResponse(`https://www.instagram.com/accounts/login/ajax/`);
    const json = <LoginPageResponse>await httpResponse.json();
    // If we met checkpoint - return ChallengePage
    if (LoginPage.isCheckpoint(json)) {
      return {
        json,
        next: this.pagesFactory.transfer(ChallengePage, {}, this.page),
      };
    }
    return { json };
  }
}

export interface LoginSuccessResponseRootObject {
  authenticated: boolean;
  user: boolean;
  userId: string;
  oneTapPrompt: boolean;
  status: 'ok';
}

export interface LoginCheckpointResponseRootObject {
  message: string;
  checkpoint_url: string;
  lock: boolean;
  status: 'fail';
}

export type LoginPageResponse = LoginSuccessResponseRootObject | LoginCheckpointResponseRootObject;

export interface LoginCredentials {
  username: string;
  password: string;
}
