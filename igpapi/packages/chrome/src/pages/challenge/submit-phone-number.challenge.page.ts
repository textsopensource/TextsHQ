import { ChallengePage } from './challenge.page';
import { Service } from 'typedi';

@Service({ transient: true, global: true })
export class SubmitPhoneNumberChallengePage extends ChallengePage {
  async submit(phone: string) {
    await this.page.focus('[type="tel"]');
    await this.page.keyboard.type(phone);
    await this.page.click('[type="button"]:nth-child(1)');
    return this.next();
  }
}
