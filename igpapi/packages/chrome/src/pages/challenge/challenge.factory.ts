import { VerifySmsCaptchaCodeRootObject } from './types/verify-sms-captcha-code.challenge.type';
import { SelectVerificationMethodFormChallengeRootObject } from './types/select-verification-method.challenge.type';
import { VerifyCodeChallengePageRootObject } from './types/verify-code.challenge.type';
import { SubmitPhoneNumberChallengeStateRootObject } from './types/submit-phone-number.challenge.type';
import { SubmitPhoneNumberChallengePage } from './submit-phone-number.challenge.page';
import { VerifySmsCaptchaCodeChallengePage } from './verify-sms-captcha-code.challenge.page';
import { SelectVerificationMethodChallengePage } from './select-verification-method.challenge.page';
import { VerifyCodeChallengePage } from './verify-code.challenge.page';

export class ChallengeFactory {
  static type(state: SubmitPhoneNumberChallengeStateRootObject): SubmitPhoneNumberChallengePage;
  static type(state: VerifySmsCaptchaCodeRootObject): VerifySmsCaptchaCodeChallengePage;
  static type(state: SelectVerificationMethodFormChallengeRootObject): SelectVerificationMethodChallengePage;
  static type(state: VerifyCodeChallengePageRootObject): VerifyCodeChallengePage;
  static type(state: any) {
    let page: any;
    switch (state.challengeType) {
      case 'SubmitPhoneNumberForm':
        page = SubmitPhoneNumberChallengePage;
        break;
      case 'VerifySMSCodeFormForSMSCaptcha':
        page = VerifySmsCaptchaCodeChallengePage;
        break;
      case 'SelectVerificationMethodForm':
        page = SelectVerificationMethodChallengePage;
        break;
      case 'VerifyEmailCodeForm':
        page = VerifyCodeChallengePage;
        break;
      default:
        throw new Error(`Unimplemented challenge type: ${state.challengeType}`);
    }
    return page;
  }
}
