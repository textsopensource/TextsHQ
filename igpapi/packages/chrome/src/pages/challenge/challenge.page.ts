import { Service } from 'typedi';
import { Page } from 'puppeteer';
import { IgpapiState } from '@textshq/core';
import { IgpapiPage } from '../../igpapi.page';
import { SubmitPhoneNumberChallengeStateRootObject } from './types/submit-phone-number.challenge.type';
import { SelectVerificationMethodFormChallengeRootObject } from './types/select-verification-method.challenge.type';
import { VerifyCodeChallengePageRootObject } from './types/verify-code.challenge.type';
import { VerifySmsCaptchaCodeRootObject } from './types/verify-sms-captcha-code.challenge.type';

@Service({ transient: true, global: true })
export class ChallengePage implements IgpapiPage {
  page: Page;
  readonly apiPath?: string;

  constructor(protected state: IgpapiState) {}

  get path() {
    return this.apiPath ?? this.state.getChallengePath();
  }

  async getState(): Promise<ChallengeState> {
    return this.page.evaluate(() => (window as any)._sharedData.entry_data.Challenge[0]);
  }

  protected async next() {
    const httpResponse = await this.page.waitForResponse(`https://www.instagram.com${this.path}`);
    return <any>await httpResponse.json();
  }
}

export type ChallengeState =
  | SubmitPhoneNumberChallengeStateRootObject
  | VerifyCodeChallengePageRootObject
  | SelectVerificationMethodFormChallengeRootObject
  | VerifySmsCaptchaCodeRootObject;
