import { ChallengePage } from './challenge.page';

export class SelectVerificationMethodChallengePage extends ChallengePage {
  async submit() {
    await this.page.click('form button');
    return this.next();
  }
}
