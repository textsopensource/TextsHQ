import { ChallengePage } from './challenge.page';

export class VerifyCodeChallengePage extends ChallengePage {
  async submit(code: string) {
    await this.page.focus('#security_code');
    await this.page.keyboard.type(code);
    await this.page.click('form button');
    const body = await this.next();
    if (body.status === 'ok') {
      delete this.state.challengePath;
    }
    return body;
  }
}
