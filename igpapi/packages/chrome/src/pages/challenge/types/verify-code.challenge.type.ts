interface Value {
  label: string;
  placeholder?: any;
  value?: any;
}

interface Field {
  __typename: string;
  input_name: string;
  input_type: string;
  value: Value;
}

interface Content {
  __typename: string;
  description?: any;
  title: string;
  alignment: string;
  html: string;
  text: string;
  call_to_action: string;
  display: string;
  fields: Field[];
  href?: any;
}

interface ExtraData {
  __typename: string;
  content: Content[];
}

interface Fields {
  security_code: string;
  resend_delay: number;
  contact_point: string;
  form_type: string;
}

interface Navigation {
  forward: string;
  replay: string;
  back: string;
}

export interface VerifyCodeChallengePageRootObject {
  challengeType: 'VerifyEmailCodeForm';
  errors: any[];
  extraData: ExtraData;
  fields: Fields;
  navigation: Navigation;
  privacyPolicyUrl: string;
  type: string;
  status: string;
}
