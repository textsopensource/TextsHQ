interface SubmitPhoneNumberChallengeStateExperiments {}

interface SubmitPhoneNumberChallengeStateFields {
  phone_number: string;
}

interface SubmitPhoneNumberChallengeStateNavigation {
  forward: string;
  replay: string;
  dismiss: string;
}

export interface SubmitPhoneNumberChallengeStateRootObject {
  challengeType: 'SubmitPhoneNumberForm';
  errors: any[];
  experiments: SubmitPhoneNumberChallengeStateExperiments;
  extraData?: any;
  fields: SubmitPhoneNumberChallengeStateFields;
  navigation: SubmitPhoneNumberChallengeStateNavigation;
  privacyPolicyUrl: string;
  type: string;
}
