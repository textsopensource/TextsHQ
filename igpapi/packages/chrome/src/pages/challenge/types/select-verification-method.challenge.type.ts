interface Value {
  label: string;
  selected: boolean;
  value: number;
}

interface Field {
  __typename: string;
  input_name: string;
  input_type: string;
  values: Value[];
}

interface Content {
  __typename: string;
  banner_text: string;
  description?: any;
  title: string;
  alignment: string;
  html?: any;
  text: string;
  call_to_action: string;
  display: string;
  fields: Field[];
  href?: any;
}

interface ExtraData {
  __typename: string;
  content: Content[];
}

interface Fields {
  choice: string;
  fb_access_token: string;
  big_blue_token: string;
  google_oauth_token: string;
  vetted_device: string;
  email: string;
}

interface Navigation {
  forward: string;
  replay: string;
  dismiss: string;
}

export interface SelectVerificationMethodFormChallengeRootObject {
  challengeType: 'SelectVerificationMethodForm';
  errors: string[];
  extraData: ExtraData;
  fields: Fields;
  navigation: Navigation;
  privacyPolicyUrl: string;
  type: string;
}
