interface Fields {
  security_code: string;
  sms_resend_delay: number;
  phone_number_preview: string;
  resend_delay: number;
  contact_point: string;
  form_type: string;
  phone_number_formatted: string;
  phone_number: string;
}

interface Navigation {
  forward: string;
  replay: string;
  back: string;
}

export interface VerifySmsCaptchaCodeRootObject {
  challengeType: 'VerifySMSCodeFormForSMSCaptcha';
  errors: any[];
  extraData?: any;
  fields: Fields;
  navigation: Navigation;
  privacyPolicyUrl: string;
  type: string;
  status: string;
}
