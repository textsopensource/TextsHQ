import { Service } from 'typedi';
import { ChallengePage } from './challenge.page';

@Service({ transient: true, global: true })
export class VerifySmsCaptchaCodeChallengePage extends ChallengePage {
  async submit(code: string) {
    await this.page.focus('[type="tel"]');
    await this.page.keyboard.type(code);
    await this.page.click('[type="button"]:nth-child(1)');
    return this.next();
  }
}
