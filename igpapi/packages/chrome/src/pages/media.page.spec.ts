import { ChromeIgpapiFactory, MediaPage } from '../';
import { ChromeIgpapi } from '../chrome.igpapi';
import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';

describe('login page', () => {
  let igpapi: ChromeIgpapi;
  let android = new AndroidIgpapi();
  beforeAll(async () => {
    await android.execute(AccountLoginCommand, {});
    igpapi = await ChromeIgpapiFactory.create({ state: android.state, launch: { headless: false } });
  });
  it('should inverse like', async () => {
    const page = await igpapi.page.open(MediaPage, { shortcode: 'CEW-nakMRFq' });
    await page.inverseLike();
  });
});
