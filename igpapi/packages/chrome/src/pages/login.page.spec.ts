import { ChromeIgpapiFactory, LoginPage } from '../';
import { ChromeIgpapi } from '../chrome.igpapi';
import { AndroidIgpapi, AndroidState } from '@textshq/android';

describe('login page', () => {
  let igpapi: ChromeIgpapi;
  let loginPage: LoginPage;
  let state = new AndroidState();
  let android = new AndroidIgpapi({ state });
  beforeAll(async () => {
    igpapi = await ChromeIgpapiFactory.create({ state, launch: { headless: false } });
    loginPage = await igpapi.page.open(LoginPage, {});
  });
  it('should log in', async () => {
    expect(() => igpapi.state.cookies.value('mid')).toThrowError();
    await loginPage.logIn({
      username: process.env.IG_USERNAME as string,
      password: process.env.IG_PASSWORD as string,
    });
    expect(igpapi.state.cookies.value('mid')).toBeDefined();
    await android.account.currentUser();
  });
});
