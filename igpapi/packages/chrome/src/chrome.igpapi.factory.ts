import { Browser, LaunchOptions, SetCookie } from 'puppeteer';
import { IgpapiState } from '@textshq/core';
import { ChromeIgpapi } from './chrome.igpapi';
import { URL } from 'url';
import puppeteer = require('puppeteer');

interface ChromeIgpapiCreateOptions {
  state?: IgpapiState;
  /**
   * Puppeeteer options
   */
  launch?: LaunchOptions;
}

export class ChromeIgpapiFactory {
  // noinspection JSUnusedLocalSymbols
  private constructor() {}

  public static async create({ state = new IgpapiState(), launch }: ChromeIgpapiCreateOptions) {
    const args: string[] = [...(launch?.args ?? [])];
    const proxyUrl = state.getProxy();
    if (proxyUrl) {
      const proxy = new URL(proxyUrl);
      args.push(`--proxy-server=${proxy.origin}`);
    }
    const browser = await puppeteer.launch({
      ...launch,
      args,
    });
    const page = await browser.newPage();
    if (proxyUrl) {
      const proxy = new URL(proxyUrl);
      await page.authenticate({ username: proxy.username, password: proxy.password });
      await page.goto('https://www.instagram.com/favicon.ico');
    }
    const cookies = state.cookies.toJSON().cookies.map(
      (cookie): SetCookie => ({
        name: cookie.key,
        value: cookie.value,
        domain: '.instagram.com',
        path: cookie.path,
        httpOnly: cookie.httpOnly,
        secure: cookie.secure,
      }),
    );
    await page.setCookie(...cookies);
    await page.close();
    await this.dismissNotificationsAlert(browser);
    return new ChromeIgpapi(browser, state);
  }

  private static async dismissNotificationsAlert(browser: Browser) {
    const page = await browser.newPage();
    await page.setRequestInterception(true);
    page.on('request', r => {
      r.respond({
        status: 200,
        contentType: 'text/plain',
        body: 'tweak me.',
      });
    });
    await page.goto('https://www.instagram.com');
    await page.evaluate(() => {
      // @ts-ignore
      localStorage.setItem('ig_notifications_dismiss', Date.now() + 24 * 60 * 60 * 1000);
    });
    await page.close();
  }
}
