import { IgpapiFactory, IgpapiFactoryInput, IgpapiState } from '@textshq/core';
import { Page, Response } from 'puppeteer';
import { ContainerInstance, Service } from 'typedi';
import { ChallengePage, MediaPage, ProfilePage } from './pages';
import { IgpapiPage } from './igpapi.page';
import { ChromeBrowser } from './chrome.browser';

export type PageProperties<T extends object> = Omit<IgpapiFactoryInput<T>, 'path' | 'page'>;

@Service()
export class PagesFactory {
  constructor(
    protected container: ContainerInstance,
    private browser: ChromeBrowser,
    private state: IgpapiState,
    private factory: IgpapiFactory,
  ) {}

  media(input: PageProperties<MediaPage>) {
    return this.open(MediaPage, input);
  }

  profile(input: PageProperties<ProfilePage>) {
    return this.open(ProfilePage, input);
  }

  challenge(input: PageProperties<ChallengePage>) {
    return this.open(ChallengePage, input);
  }

  /**
   * Creates new browser tab
   * @param cls
   * @param plain
   */
  async open<T extends IgpapiPage>(cls: { new (...args: any[]): T }, plain: PageProperties<T>): Promise<T> {
    const page = await this.browser.newPage();
    const instance = this.transfer(cls, plain, page);
    await page.goto(`https://www.instagram.com${instance.path}`);
    page.on('response', r => this.syncCookies(r, page));
    return instance;
  }

  /**
   * Creates instance of IgpapiPage and assigns existing page
   */
  transfer<T extends IgpapiPage>(cls: { new (...args: any[]): T }, plain: PageProperties<T>, page: Page) {
    const instance = this.factory.instantiate(cls, plain as IgpapiFactoryInput<T>);
    instance.page = page;
    return instance;
  }

  close(page: IgpapiPage) {
    return page.page.close();
  }

  private async syncCookies(response: Response, page: Page) {
    const url = new URL(response.url());
    // We only need to sync cookies when hostname is instagram and content type is application/json
    // In other cases response not contains something useful
    if (
      url.hostname.includes('instagram.com') &&
      response.headers()['content-type'] === 'application/json; charset=utf-8'
    ) {
      const cookies = await page.cookies();
      cookies.forEach(cookie =>
        this.state.cookies.jar.setCookieSync(
          `${cookie.name}=${cookie.value}; Domain=.instagram.com; Path=/; Secure`,
          'https://instagram.com/',
          {
            secure: true,
            http: true,
          },
        ),
      );
    }
  }
}
