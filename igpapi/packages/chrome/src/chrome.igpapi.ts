import { ContainerInstance } from 'typedi';
import { Browser } from 'puppeteer';
import { ChromeBrowser } from './chrome.browser';
import { Igpapi, IgpapiState } from '@textshq/core';
import { PagesFactory } from './pages.factory';

export class ChromeIgpapi extends Igpapi {
  public container = new ContainerInstance('igpapi');
  constructor(private browser: Browser, readonly state: IgpapiState) {
    super();
    this.container.set(ContainerInstance, this.container);
    this.container.set(ChromeBrowser, browser);
    this.container.set(IgpapiState, state);
  }
  public get page() {
    return this.resolve(PagesFactory);
  }

  async destroy() {
    return this.browser.close();
  }
}
