import { Page } from 'puppeteer';

export interface IgpapiPage {
  page: Page;

  readonly path: string;
}
