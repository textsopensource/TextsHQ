import { IrisSubData } from '@textshq/realtime-core';

export interface WebMqttClientConnectOptions {
  connectOptions?: Partial<WebMqttConnectUsername>;
  keepAlive?: number;
  irisSubData?: IrisSubData;
  noAppPresence?: boolean;
}

export interface WebMqttSessionData {
  sessionId?: number;
  deviceId?: string;
}

export interface WebMqttConnectUsername {
  userId: number;
  sessionId: number;
  // unsure
  capabilities: number;
  endpointCapabilities: number;

  chatOn: boolean;
  foreground: boolean;
  deviceId: string;
  clientType: 'cookie_auth';
  mqttSessionId: string;
  appId: string;
  subscribeTopics: string[];
  pendingMessages: any[];

  dc: string;
  noAutomaticForeground: boolean;
  userAgent: string;
  appSpecificInfo?: any;
}

export type Mapping<T> = { [x in keyof T]: string };
