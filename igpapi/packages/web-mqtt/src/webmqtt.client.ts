import { WebIgpapi } from '@textshq/web';
import { MqttClient, WebsocketTransport } from 'mqtts';
import { Chance } from 'chance';
import {
  GraphQLSubscriptions,
  RealtimeCoreClient,
  RealtimeTopics,
  SkywalkerSubscriptions,
  IrisSubData,
} from '@textshq/realtime-core';
import { Mapping, WebMqttClientConnectOptions, WebMqttConnectUsername, WebMqttSessionData } from './types';

export class WebMqttClient extends RealtimeCoreClient<WebMqttClientConnectOptions> {
  protected chance = new Chance();

  protected session: WebMqttSessionData = {
    sessionId: Math.floor(Math.random() * Number.MAX_SAFE_INTEGER),
    deviceId: this.chance.guid({ version: 4 }),
  };

  constructor(private ig: WebIgpapi) {
    super({ compressData: false });
  }

  protected async getIrisSubData(): Promise<IrisSubData> {
    const { raw } = await this.ig.feed.inbox().next();
    return {
      seq_id: raw.seq_id,
      snapshot_at_ms: Date.now(),
      snapshot_app_version: 'message',
    };
  }

  protected async createMqttClient(): Promise<MqttClient> {
    return new MqttClient({
      transport: new WebsocketTransport({
        url: 'wss://edge-chat.instagram.com/chat',
        additionalOptions: {
          host: 'edge-chat.instagram.com',
          origin: 'https://www.instagram.com',
          headers: {
            'User-Agent': this.ig.state.device.userAgent,
            // @ts-ignore strange error
            Cookie: this.ig.state.cookies.jar.getCookieStringSync('https://www.instagram.com'),
          },
        },
      }),
      autoReconnect: this.connectOptions.autoReconnect ?? true,
    });
  }
  protected connectToBroker(): Promise<any> {
    return this.mqttClient.connect({
      protocolLevel: 3,
      protocolName: 'MQIsdp',
      username: this.createUsername(this.connectOptions.connectOptions),
      clean: true,
      keepAlive: this.connectOptions.keepAlive ?? 10,
      connectDelay: 60 * 1000,
    });
  }

  protected async postConnect(): Promise<any> {
    await this.mqttSubscribe(
      RealtimeTopics.MessageSync,
      RealtimeTopics.SendMessageResponse,
      RealtimeTopics.SubIrisResponse,
      RealtimeTopics.Pubsub,
    );
    const irisData = this.connectOptions.irisSubData ?? (await this.getIrisSubData());
    if (irisData.snapshot_app_version === 'message') {
      await this.skywalkerSub(SkywalkerSubscriptions.directSub(this.ig.state.userId));
    }
    if (!this.connectOptions.noAppPresence) {
      await this.mqttClient.subscribe({ topic: RealtimeTopics.RealtimeSub });
      await this.graphQlSub(GraphQLSubscriptions.getAppPresenceSubscription());
    }
    return this.irisSubscribe(irisData);
  }

  protected createUsername(additional: Partial<WebMqttConnectUsername> = {}): string {
    return JSON.stringify(
      remap(
        {
          userId: this.ig.state.userId,
          capabilities: 1,
          endpointCapabilities: 0,

          chatOn: true,
          foreground: true,
          noAutomaticForeground: true,

          deviceId: this.session.deviceId,
          clientType: 'cookie_auth',
          mqttSessionId: '',
          appId: this.ig.state.appId,
          subscribeTopics: [],
          pendingMessages: [],
          dc: '',
          userAgent: this.ig.state.device.userAgent,
          ...additional,
          sessionId: this.session.sessionId,
        },
        usernameMapping,
      ),
    );
  }
}

export const usernameMapping: Mapping<WebMqttConnectUsername> = {
  userId: 'u',
  sessionId: 's',
  capabilities: 'cp',
  endpointCapabilities: 'ecp',
  chatOn: 'chat_on',
  foreground: 'fg',
  deviceId: 'd',
  clientType: 'ct',
  mqttSessionId: 'mqtt_sid',
  appId: 'aid',
  subscribeTopics: 'st',
  pendingMessages: 'pm',
  dc: 'dc',
  noAutomaticForeground: 'no_auto_fg',
  userAgent: 'a',
  appSpecificInfo: 'asi',
};

export function remap<T>(target: T, mapping: Mapping<T>): any {
  const result: any = {};
  for (const [key, value] of Object.entries(target)) {
    // @ts-ignore -- this is indexable
    result[mapping[key]] = value;
  }
  return result;
}
