import { promises } from 'fs';
import { join } from 'path';
import { difference, uniq } from 'lodash';

describe('Exports', () => {
  const directories = {
    android: ['entities', 'errors', 'feeds', 'responses', 'types'],
    chrome: ['pages'],
    core: ['decorators', 'errors', 'types'],
    mqttot: ['mqttot', 'thrift'],
    'realtime-core': ['commands', 'payloads', 'subscriptions'],
    sticker: ['stickers'],
    web: ['errors', 'feeds', 'responses', 'types'],
  };

  for (const [name, targets] of Object.entries(directories)) {
    const root = join('packages', name, 'src');
    for (const target of targets) {
      it(`${name}/${target} properly exports all required files`, async () => {
        const files = await promises.readdir(join(root, target));
        const index = await promises.readFile(join(root, target, 'index.ts'), { encoding: 'utf8' });
        const required = files
          .filter(x => x !== 'index.ts' && x.endsWith('.ts'))
          .map(x => x.substring(0, x.length - 3));
        const exported = index
          .split(/[\n\r]/g)
          .map(x => x.match(/^export \* from ['"]\.\/(.+)['"];$/)?.[1])
          .filter(Boolean);
        expect(uniq(exported)).toStrictEqual(exported);
        // simplify the output
        expect([]).toStrictEqual(difference(required, exported) ?? []);
      });
    }
  }
});
