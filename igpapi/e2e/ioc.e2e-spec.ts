import { AndroidHttp, AndroidIgpapi } from '../packages/android/src';

describe('Igpapi IoC', () => {
  const igpapi = new AndroidIgpapi();

  it('should be defined', () => {
    expect(igpapi).toBeDefined();
  });

  it('should return new feed instance for the same client every time', () => {
    const feed1 = igpapi.feed.discover();
    const feed2 = igpapi.feed.discover();
    expect(feed1).not.toBe(feed2);
  });

  it('should return new entity instance for the same client every time', () => {
    const feed1 = igpapi.entity.profile('1');
    const feed2 = igpapi.entity.profile('1');
    expect(feed1).not.toBe(feed2);
  });

  it('should return the same repository instance every time', () => {
    expect(igpapi.music).toBe(igpapi.music);
    expect(igpapi.state).toBe(igpapi.state);
    expect(igpapi.container.get(AndroidHttp)).toBe(igpapi.container.get(AndroidHttp));
  });

  it('should return different repository for different clients', () => {
    const igpapi1 = new AndroidIgpapi();
    expect(igpapi.music).not.toBe(igpapi1.music);
  });
});
