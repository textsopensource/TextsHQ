import { join } from 'path';
import { promises } from 'fs';

describe('Dependencies', function() {
  it('should contain only one version of a dependency', async function() {
    const packagesPath = join(__dirname, '../packages');
    const files = await promises.readdir(packagesPath);
    const dependencyRegister = {};
    for (const filePath of files) {
      const { dependencies, devDependencies } = JSON.parse(
        await promises.readFile(join(packagesPath, filePath, 'package.json'), { encoding: 'utf8' }),
      );
      console.debug('Checking:', filePath);
      checkDependencies(dependencies);
      checkDependencies(devDependencies);
    }

    function checkDependencies(dependencies: object) {
      if (!dependencies) return;
      for (const [dependency, version] of Object.entries(dependencies)) {
        if (dependencyRegister[dependency]) {
          if (dependencyRegister[dependency] !== version) {
            console.debug(`Invalid: ${dependency}; (${dependencyRegister[dependency]} vs ${version})`);
          }
          expect(dependencyRegister[dependency]).toBe(version);
        } else {
          dependencyRegister[dependency] = version;
        }
      }
    }
  });
});
