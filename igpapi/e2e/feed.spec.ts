import { Feed } from '@textshq/core';

class TestFeed extends Feed<any, any> {
  id = 1;
  cursor? = 'important';

  items() {
    return [undefined];
  }

  handle(): boolean {
    return false;
  }

  protected async request(): Promise<any> {
    return undefined;
  }
}

describe('Abstract feed', () => {
  const feed = new TestFeed();
  it('should be serializable', () => {
    const json = feed.toJSON();
    expect(json.id).toBe(1);
    expect(json.cursor).toBe('important');
  });
  it('should be partially deserializable', () => {
    feed.deserialize({ id: 5 });
    expect(feed.id).toBe(5);
    expect(feed.cursor).toBe('important');
  });
});
