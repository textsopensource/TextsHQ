/* tslint:disable:no-console */
import 'dotenv/config';
import inquirer from 'inquirer';
import { ChromeIgpapiFactory, VerifyCodeChallengePage } from '@textshq/chrome';
import { AndroidIgpapi, IgCheckpointError } from '@textshq/android';

jest.setTimeout(999999);

test('challenge', async () => {
  const android = new AndroidIgpapi();
  android.state.device.generate(process.env.IG_USERNAME);
  try {
    await android.qe.syncLoginExperiments();
    await android.account.login(process.env.IG_USERNAME, process.env.IG_PASSWORD);
  } catch (e) {
    if (e instanceof IgCheckpointError) {
      await android.challenge.selectVerifyMethod('1');
      const chrome = await ChromeIgpapiFactory.create({
        // Pass your existing state here
        state: android.state,
        // in production you should negate this options
        launch: { headless: false, devtools: true },
      });
      const verifyCodeChallengePage = await chrome.page.open(VerifyCodeChallengePage, {});
      const { code } = await inquirer.prompt([
        {
          type: 'input',
          name: 'code',
          message: 'Enter code',
        },
      ]);
      const checkpointResponse = await verifyCodeChallengePage.submit(code);
      // Dont forget to close the page
      await chrome.page.close(verifyCodeChallengePage);
      console.log(checkpointResponse);
      // Now you should be able to get info about your current user
      console.log(await android.account.currentUser());
    }
  }
});
