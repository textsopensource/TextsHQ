/* tslint:disable:no-console */
import 'dotenv/config';
// @ts-ignore - it's part of @textshq/android
import { get } from 'request-promise';
import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';

/*

         This example isn't really necessary (upload-photo.example is the same with more functionality)

 */

(async () => {
  const ig = new AndroidIgpapi();
  ig.state.proxyUrl = process.env.IG_PROXY;
  await ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });

  // getting random square image from internet as a Buffer
  const imageBuffer = await get({
    url: 'https://picsum.photos/800/800', // random picture with 800x800 size
    encoding: null, // this is required, only this way a Buffer is returned
  });

  const publishResult = await ig.publish.photo({
    file: imageBuffer, // image buffer, you also can specify image from your disk using fs
    caption: 'Really nice photo from the internet! 💖', // nice caption (optional)
  });

  console.log(publishResult); // publishResult.status should be "ok"
})();
