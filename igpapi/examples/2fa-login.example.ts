/* tslint:disable:no-console */
import inquirer = require('inquirer');
import { AndroidIgpapi } from '@textshq/android';

// Return logged in user object
(async () => {
  // Initiate Instagram API client
  const ig = new AndroidIgpapi();
  ig.state.proxyUrl = process.env.IG_PROXY;
  await ig.simulate
    .bootstrap({
      credentials: {
        username: process.env.IG_USERNAME,
        password: process.env.IG_PASSWORD,
      },
      onTwoFactor: async ({ twoFactorInfo }) => {
        const verificationMethod = twoFactorInfo.totp_two_factor_on ? 'TOTP' : 'SMS';
        const { code } = await inquirer.prompt([
          {
            type: 'input',
            name: 'code',
            message: `Enter code received via ${verificationMethod}`,
          },
        ]);
        return { code, method: verificationMethod };
      },
    })
    .catch(e => console.error('An error occurred while processing two factor auth', e, e.stack));
})();
