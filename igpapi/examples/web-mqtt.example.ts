import { WebIgpapi } from '@textshq/web';
import { WebMqttClient } from '@textshq/web-mqtt';

const ig = new WebIgpapi();

async function login() {
  return ig.simulate.bootstrap({
    // login is required
    login: {
      username: process.env.IG_USERNAME,
      password: process.env.IG_PASSWORD,
    },
    // optional;
    // use the mobile version
    isMobile: false,
  });
}

(async () => {
  await login();
  const mqtt = new WebMqttClient(ig);
  // subscribe to direct messages
  mqtt.message$.subscribe(({ message }) => console.log(message));
  // see realtime.example for more Subjects
  await mqtt.connect({});
})();
