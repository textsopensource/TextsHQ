/* tslint:disable:no-console */
import 'dotenv/config';
import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';

(async () => {
  const ig = new AndroidIgpapi();
  await ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });
  const followersFeed = ig.feed.accountFollowers({ id: String(ig.state.cookies.userId) });
  // Get pages manually?
  const { items, raw } = await followersFeed.next();
  console.log(items, raw);
  // Iterate until the end of feed
  for await (const { items } of followersFeed) {
    console.log(items);
  }
})();
