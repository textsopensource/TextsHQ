/* tslint:disable:no-console */
import 'dotenv/config';
import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';

(async () => {
  const ig = new AndroidIgpapi();
  ig.state.device.generate(process.env.IG_USERNAME);
  ig.state.proxyUrl = process.env.IG_PROXY;
  await ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });

  const targetUser = await ig.user.searchExact('username'); // getting exact user by login
  const reelsFeed = ig.feed.userStory({ userId: String(targetUser.pk) });
  const { items } = await reelsFeed.next(); // getting reels, see "account-followers.feed.example.ts" if you want to know how to work with feeds
  if (items.length === 0) {
    // we can check items length and find out if the user does have any story to watch
    console.log(`${targetUser.username}'s story is empty`);
    return;
  }
  const seenResult = await ig.story.seen([items[0]]);
  // now we can mark story as seen using story-service, you can specify multiple stories, in this case we are only watching the first story

  console.log(seenResult.status); // seenResult.status should be "ok"
})();
