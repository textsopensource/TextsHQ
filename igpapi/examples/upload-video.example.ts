/* tslint:disable:no-console */
import { readFile } from 'fs';
import { promisify } from 'util';
import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';

const readFileAsync = promisify(readFile);

const ig = new AndroidIgpapi();

async function login() {
  // basic login-procedure
  ig.state.proxyUrl = process.env.IG_PROXY;
  await ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });
}

(async () => {
  await login();

  const videoPath = './myVideo.mp4';
  const coverPath = './myVideoCover.jpg';

  const publishResult = await ig.publish.video({
    // read the file into a Buffer
    video: await readFileAsync(videoPath),
    coverImage: await readFileAsync(coverPath),
    /*
      this does also support:
      caption (string),  ----+
      usertags,          ----+----> See upload-photo.example.ts
      location,          ----+
     */
  });

  console.log(publishResult);
})();
