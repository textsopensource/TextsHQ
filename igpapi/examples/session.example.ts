/* tslint:disable:no-console */
import 'dotenv/config';
import { AndroidIgpapi } from '@textshq/android';

async function fakeSave(data: object): Promise<void> {
  // here you would save it to a file/database etc.
  // you could save it to a file: writeFile(path, JSON.stringify(data))
  console.log(data);
}

async function fakeLoad(): Promise<string> {
  // here you would load the data
  return '';
}

(async () => {
  const ig = new AndroidIgpapi();
  ig.state.proxyUrl = process.env.IG_PROXY;
  await ig.simulate.bootstrap({
    credentials: {
      username: process.env.IG_USERNAME,
      password: process.env.IG_PASSWORD,
    },
    state: fakeLoad,
    saveState: state => fakeSave(state),
  });
})();
