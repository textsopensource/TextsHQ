import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';
import { FbnsClient } from '@textshq/fbns';

const ig = new AndroidIgpapi();

async function login() {
  return ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });
}

(async () => {
  await login();
  const fbns = new FbnsClient(ig);
  // log all push notifications
  fbns.push$.subscribe(({ title, message }) => console.log(title, message));
  await fbns.connect();
})();
