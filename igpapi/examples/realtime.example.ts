import { AccountLoginCommand, AndroidIgpapi } from '@textshq/android';
import 'dotenv/config';
import { AndroidRealtimeClient } from '@textshq/android-realtime';
import { GraphQLSubscriptions, SkywalkerSubscriptions, ThreadItemType } from '@textshq/realtime-core';
import { filter } from 'rxjs/operators';

const ig = new AndroidIgpapi();

async function login() {
  return ig.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });
}

(async () => {
  await login();
  const realtime = new AndroidRealtimeClient(ig);
  realtime.message$.subscribe(({ message }) => console.log(message));
  realtime.appPresence$.subscribe(({ presence_event }) => console.log(presence_event));
  realtime.activityIndicator$.subscribe(activity => console.log(activity));
  await realtime.connect({
    graphQlSubs: [
      GraphQLSubscriptions.getAppPresenceSubscription(),
      GraphQLSubscriptions.getDirectTypingSubscription(ig.state.extractUserId()),
      GraphQLSubscriptions.getDirectStatusSubscription(),
    ],
    skywalkerSubs: [
      SkywalkerSubscriptions.directSub(ig.state.extractUserId()),
      SkywalkerSubscriptions.liveSub(ig.state.extractUserId()),
    ],
    irisData: await ig.feed.directInbox().request(),
  });

  realtime.message$
    // listen for 'ping'
    .pipe(filter(({ message }) => message.item_type === ThreadItemType.TEXT && message.text === 'ping'))
    // reply with 'pong!'
    .subscribe(({ message }) => realtime.directCommands.sendText({ text: 'pong!', thread_id: message.thread_id }));

  // simulate the device being off
  // Note: you won't receive any messages after that, you'll have to turn it back on
  await realtime.directCommands.sendForegroundState({
    inForegroundApp: false,
    inForegroundDevice: false,
    keepAliveTimeout: 900,
  });

  // simulate inapp
  await realtime.directCommands.sendForegroundState({
    inForegroundApp: true,
    inForegroundDevice: true,
    keepAliveTimeout: 60,
  });
})();
