import { WebIgpapi } from '@textshq/web';
import 'dotenv/config';
import { promises } from 'fs';

const ig = new WebIgpapi();

async function login() {
  return ig.simulate.bootstrap({
    // optional
    login: {
      username: process.env.IG_USERNAME,
      password: process.env.IG_PASSWORD,
    },
    // optional;
    // callback, whenever the state is saved
    saveState: input => promises.writeFile('state.json', JSON.stringify(input)),
    // optional;
    // read the state
    state: promises.readFile('state.json', { encoding: 'utf8' }),
    // optional;
    // use the mobile version
    isMobile: false,
  });
}

(async () => {
  await login();
  // get the user info
  const user = await ig.users.infoByName('instagram');
  console.log(user);
  // print the feed
  for await (const item of ig.feed.user({ id: user.graphql.user.id })) {
    console.log(item);
  }
})();
