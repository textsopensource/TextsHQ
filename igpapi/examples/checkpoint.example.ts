/* tslint:disable:no-console */
import 'dotenv/config';
import inquirer from 'inquirer';
import { ChromeIgpapiFactory, VerifyCodeChallengePage } from '@textshq/chrome';
import { AccountLoginCommand, AndroidIgpapi, IgCheckpointError } from '@textshq/android';
import Bluebird from 'bluebird';

/**
 * This method won't catch all checkpoint errors
 * There's currently a new checkpoint used by instagram which requires 'web-support'
 */

const android = new AndroidIgpapi();
// android.state.device.generate(process.env.IG_USERNAME);

Bluebird.try(async () => {
  return android.execute(AccountLoginCommand, {
    username: process.env.IG_USERNAME,
    password: process.env.IG_PASSWORD,
  });
}).catch(IgCheckpointError, async () => {
  await android.challenge.selectVerifyMethod('1');
  const chrome = await ChromeIgpapiFactory.create({
    // Pass your existing state here
    state: android.state,
    // in production you should negate this options
    launch: { headless: false, devtools: true },
  });
  const verifyCodeChallengePage = await chrome.page.open(VerifyCodeChallengePage, {});
  const { code } = await inquirer.prompt([
    {
      type: 'input',
      name: 'code',
      message: 'Enter code',
    },
  ]);
  const checkpointResponse = await verifyCodeChallengePage.submit(code);
  // Dont forget to close the page
  await chrome.page.close(verifyCodeChallengePage);
  console.log(checkpointResponse);
  // Now you should be able to get info about your current user
  console.log(await android.account.currentUser());
  // This line upgrades web session to android.
  // Call it once if you got `login_required` on some endpoints
  // Basically if you use chrome challenge it's better to call it right after
  await android.simulate.upgrade();
});
