import { promises as fs } from 'fs'
import type { AccountInfo, ActivityType, CurrentUser, LoginCreds, LoginResult, Message, MessageContent, MessageSendOptions, OnServerEventCallback, Paginated, PaginationArg, PlatformAPI, Thread, ThreadFolderName, User } from '@textshq/platform-sdk'
import IRC from './irc'
import { handleCommand } from './commands'
import { AuthCredentials } from './types'

export default class IRCAPI implements PlatformAPI {
  private irc?: IRC

  private accountInfo?: AccountInfo

  init = async (session: AuthCredentials, accountInfo?: AccountInfo) => {
    this.accountInfo = accountInfo

    if (session) {
      this.irc = new IRC(session)
    }
  }

  login = async (creds?: LoginCreds): Promise<LoginResult> => {
    this.irc = new IRC(creds!.custom)

    return {
      type: 'success',
    }
  }

  logout = async () => {
    await this.dispose()

    if (this.accountInfo) {
      await fs.rm(this.accountInfo!.dataDirPath, { recursive: true })
    }
  }

  dispose = async () => {
    await this.irc?.disconnect()
  }

  getCurrentUser = async (): Promise<CurrentUser> => this.irc!.currentUser()

  subscribeToEvents = async (onEvent: OnServerEventCallback): Promise<any> => {
    this.irc!.startListening(onEvent)
  }

  searchUsers = async (typed: string): Promise<User[]> => {
    console.log('searched', typed)

    return []
  }

  getThreads = async (folderName: ThreadFolderName, pagination?: PaginationArg): Promise<Paginated<Thread>> => ({
    items: this.irc!.allThreads(),
    hasMore: false,
  })

  sendMessage = async (threadID: string, content: MessageContent, options?: MessageSendOptions): Promise<Message[]> => {
    const text = content!.text!

    // Delegate to command handlers
    if (text.startsWith('/') && text.length > 1) return handleCommand(this.irc!, threadID, text)

    await this.irc!.say(threadID, text)

    return [{
      id: Date.now().toString(),
      timestamp: new Date(),
      senderID: this.irc!.nick(),
      text: content.text!,
      isSender: true,
    }]
  }

  getMessages = async (threadID: string, pagination?: PaginationArg): Promise<Paginated<Message>> => ({
    items: [],
    hasMore: false,
  })

  createThread = async (channels: string[], title?: string): Promise<boolean | Thread> => true

  deleteThread = async (channel: string) => {
    if (IRC.isChannel(channel)) await this.irc!.part(channel)
  }

  serializeSession = async () => this.irc!.sessionCreds()

  sendActivityIndicator = async (type: ActivityType, threadID?: string): Promise<void> => {}

  sendReadReceipt = async (threadID: string, messageID: string): Promise<void | boolean> => {}
}
