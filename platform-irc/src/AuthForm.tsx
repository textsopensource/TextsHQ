import { AuthProps, texts } from '@textshq/platform-sdk'
import type { ChangeEvent } from 'react'
import type { AuthCredentials } from './types'

const { React } = texts
const { useState } = React!

function useInput(
  defaultValue = '',
): [
    string,
    (e: ChangeEvent<HTMLInputElement>)=> void,
    React.Dispatch<React.SetStateAction<string>>,
  ] {
  const [value, setValue] = useState(defaultValue)
  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value)
  }
  return [value, onChange, setValue]
}

const AuthForm: React.FC<AuthProps> = ({ login }) => {
  const [server, onServerChange] = useInput('irc.libera.chat')
  const [nick, onNickChange] = useInput('texts-user')
  const [realName, onRealNameChange] = useInput('Texts User')
  const [leaveMessage, onLeaveMessageChange] = useInput('Texts App!')
  const [channels, onChannelsChange] = useInput('#libera,#spam,#spam1')
  const [password, onPasswordChange] = useInput('')

  const [hasPassword, setHasPassword] = useState(false)

  const onFormSubmit = () => {
    login!({
      custom: {
        server,
        nick,
        realName,
        leaveMessage,
        channels: channels.split(',').map(i => i.trim()),
        password,
      } as AuthCredentials,
    })
  }

  return (
        <form onSubmit={onFormSubmit}>
            <label>
                <span>Server</span>
                <input onChange={onServerChange} value={server}></input>
            </label>

            <label>
                <span>Nick</span>
                <input onChange={onNickChange} value={nick}></input>
            </label>

            <label>
                <span>Real name</span>
                <input onChange={onRealNameChange} value={realName}></input>
            </label>

            <label>
                <span>Leave message</span>
                <input onChange={onLeaveMessageChange} value={leaveMessage}></input>
            </label>

            <label>
                <span>Channel(s)</span>
                <input onChange={onChannelsChange} value={channels}></input>
            </label>

            <label>
                <input type="checkbox" checked={hasPassword} onChange={e => setHasPassword(e.target.checked)}></input>
                <span>Have a password</span>
            </label>

            {
                hasPassword
                && <label>
                    <span>Password</span>
                    <input onChange={onPasswordChange} value={password}></input>
                </label>
            }

            <label>
                <button type="submit">Connect</button>
            </label>
        </form>
  )
}

export default AuthForm
