export interface AuthCredentials {
  server: string

  nick: string

  realName: string

  leaveMessage: string

  channels: string[]

  password: string
}
