import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['ctcp'],

  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 1) {
      await irc.raw(['PRIVMSG', args[0], args.slice(1).join(' ')])
    }

    return []
  },
}

export default plugin
