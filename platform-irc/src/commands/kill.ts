import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['kill'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.6.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 1) {
      await irc.raw(['KILL', args[0], args.slice(1).join(' ')])
    }

    return []
  },
}

export default plugin
