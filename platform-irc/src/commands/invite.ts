import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['invite'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.2.7
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length === 2) {
      irc.raw(['INVITE', args[0], args[1]])
    } else if (args.length === 1) {
      irc.raw(['INVITE', args[0], channel])
    }

    return []
  },
}

export default plugin
