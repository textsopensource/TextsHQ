import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  // TODO: Maybe add convenient mode mappings? Such as op/deop.
  commands: ['mode'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.2.3
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    await irc.raw(['MODE', ...args])

    return []
  },
}

export default plugin
