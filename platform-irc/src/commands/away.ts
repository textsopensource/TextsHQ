import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['away', 'back'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-5.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (command === 'away') {
      const reason = args.join(' ') || ' '

      await irc.raw(['AWAY', reason])
    } else {
      await irc.raw(['AWAY'])
    }

    return []
  },
}

export default plugin
