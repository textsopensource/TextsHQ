import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['msg'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-5.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 2) {
      const text = args.slice(1).join(' ')

      await irc.say(args[0], text)

      return [{
        id: Date.now().toString(),
        timestamp: new Date(),
        senderID: irc.nick(),
        text,
        isSender: true,
      }]
    }

    return []
  },
}

export default plugin
