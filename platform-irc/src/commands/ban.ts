import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['ban', 'unban'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.2.3.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    switch (command) {
      case 'ban':
        await irc.raw(['MODE', channel, '+b', args[0]])
        break
      case 'unban':
        await irc.raw(['MODE', channel, '-b', args[0]])
        break
    }

    return []
  },
}

export default plugin
