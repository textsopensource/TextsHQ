import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['nick'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.1.2
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 1) {
      await irc.raw(['NICK', args[0]])
    }

    return []
  },
}

export default plugin
