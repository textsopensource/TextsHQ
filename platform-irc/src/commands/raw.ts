import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['raw'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-5.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 1) {
      await irc.raw(args)
    }

    return []
  },
}

export default plugin
