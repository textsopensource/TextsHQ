import type { Message } from '@textshq/platform-sdk'
import type IRC from '../irc'
import type Plugin from './plugin'

/**
 * IRC passthrough commands.
 *
 * Instead of using private message command to alias to CS or NS.
 * Sending it directly to the IRC server will avoid much nick camping and resolve some
 * security concerns.
 */
export const passThroughCommands = [
  'as',
  'bs',
  'cs',
  'ho',
  'hs',
  'join',
  'ms',
  'ns',
  'os',
  'rs',
].reduce((commands, c) => {
  commands.set(c, '')

  return commands
}, new Map())

export const localCommands = new Map();
[
  require('./action').default,
  require('./away').default,
  require('./ban').default,
  require('./ctcp').default,
  require('./invite').default,
  require('./kick').default,
  require('./kill').default,
  require('./mode').default,
  require('./msg').default,
  require('./nick').default,
  require('./notice').default,
  require('./part').default,
  require('./raw').default,
  require('./topic').default,
  require('./whois').default,
].forEach(plugin => {
  plugin.commands.forEach((c: string) => localCommands.set(c, plugin))
})

export async function handleCommand(irc: IRC, channel: string, text: string): Promise<Message[]> {
  text = text.substr(1)

  const parts = text.split(' ')
  const command = parts[0]
  const args = parts.slice(1)

  if (passThroughCommands.has(command)) {
    await irc.raw(parts)

    return []
  }

  if (localCommands.has(command)) {
    const c: Plugin = localCommands.get(command)

    return c.run(irc, channel, command, args)
  }

  return [{
    id: Date.now().toString(),
    timestamp: new Date(),
    senderID: 'none',
    isAction: true,
    isSender: false,
    text: `Unknown command: ${command}`,
  }]
}

export const allCommands = () =>
  Array.from([
    ...localCommands.keys(),
    ...passThroughCommands.keys(),
  ])
