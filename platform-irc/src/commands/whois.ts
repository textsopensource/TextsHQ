import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['whois'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.5.2
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    await irc.raw(['WHOIS', ...args])

    // irc's whois fn is not used due to not able to accept multiple args beyond nick.
    // Reply is received in IRC event listener.

    return []
  },
}

export default plugin
