import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['notice'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-5.1
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 2) {
      await irc.notice(args[0], args.slice(0).join(' '))
    }

    return []
  },
}

export default plugin
