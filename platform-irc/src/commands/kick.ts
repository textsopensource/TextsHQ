import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['kick'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.2.8
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    if (args.length >= 1) {
      await irc.raw(['KICK', channel, args[0], args.slice(1).join(' ')])
    }

    return []
  },
}

export default plugin
