import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['me', 'action'],

  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    await irc.action(channel, args.join(' '))

    return []
  },
}

export default plugin
