import { Message } from '@textshq/platform-sdk'
import IRC from '../irc'
import type Plugin from './plugin'

export const plugin: Plugin = {
  commands: ['topic'],

  // https://datatracker.ietf.org/doc/html/rfc1459#section-4.2.4
  run: async (irc: IRC, channel: string, command: string, args: string[]): Promise<Message[]> => {
    await irc.raw(['TOPIC', args.join(' ')])

    return []
  },
}

export default plugin
