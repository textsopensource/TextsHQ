import type { Message } from '@textshq/platform-sdk'
import type IRC from '../irc'

export default interface Plugin {
  /**
     * Command constants.
     *
     * Used for lookups.
     */
  readonly commands: string[]

  /**
     * Runs an IRC command
     *
     * @param irc - IRC instance.
     * @param channel - Channel this command was sent in.
     * @param command - Command name
     * @param text - Space delimited args without prefixing slash and the command name.
     */
  readonly run: (irc: IRC, channel: string, command: string, args: string[])=> Promise<Message[]>
}
