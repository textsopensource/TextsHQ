import { EventEmitter } from 'events'
import { Client } from 'matrix-org-irc'
import { CurrentUser, MessageActionType, OnServerEventCallback, Participant, ServerEvent, ServerEventType, StateSyncEvent, Thread, ToastEvent } from '@textshq/platform-sdk'
import type { AuthCredentials } from './types'

export default class IRC extends EventEmitter {
  private client: Client

  constructor(
    private creds: AuthCredentials,
  ) {
    super()

    this.client = new Client(creds.server, creds.nick, {
      userName: 'texts',
      realName: creds.realName,
      channels: creds.channels,
      password: creds.password,
    })

    this.client.on('registered', () => {
      this.emit('registered')
    })

    // Primary listener for error for cases listening doesn't start.
    // TODO: Relay connection errors (banned,etc) back to user instead of console.
    this.client.on('error', msg => {
      console.error('IRC error:', msg)
    })
  }

  public startListening(onEvent: OnServerEventCallback) {
    // Secondary listener for toast only
    this.client.on('error', msg => {
      onEvent([{
        type: ServerEventType.TOAST,
        toast: {
          text: `IRC error: ${msg.toString()}`,
        },
      } as ToastEvent])
    })

    this.client.on('names', (channel, nicks) => {
      let events: ServerEvent[] = []

      events = events.concat(Object.keys(nicks)
        .map(n => ({
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: n,
          },
          mutationType: 'upsert',
          objectName: 'thread',
          entries: [this.userThread(n)],
        })))

      // TODO: Not sure if this is necessary,
      // thread participants is not clickable in texts?

      // events = events.concat(Object.keys(nicks)
      //     .map(n => {
      //         return {
      //             type: ServerEventType.STATE_SYNC,
      //             objectIDs: {
      //                 threadID: channel,
      //             },
      //             mutationType: 'upsert',
      //             objectName: 'participant',
      //             entries: [this.userThread(n)],
      //         }
      //     }));

      onEvent(events)
    })

    this.client.on('message', (
      nick: string,
      to: string,
      text: string,
      message,
    ) => {
      onEvent([{
        type: ServerEventType.STATE_SYNC,
        objectIDs: {
          // When to equals to self nick, it indicates a PM.
          threadID: (to === this.nick()) ? nick : to,
        },
        mutationType: 'upsert',
        objectName: 'message',
        entries: [{
          id: Date.now().toString(),
          timestamp: new Date(),
          senderID: nick,
          text,
          isSender: false,
        }],
      }])
    })

    this.client.on('topic', (channel, topic, nick, message) => {
      onEvent([
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'update',
          objectName: 'thread',
          entries: [this.channelThread(channel)],
        },
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'upsert',
          objectName: 'message',
          entries: [{
            id: Date.now().toString(),
            timestamp: new Date(),
            senderID: 'none',
            isAction: true,
            isSender: false,
            action: {
              type: MessageActionType.THREAD_TITLE_UPDATED,
              // Maintain title for searchability
              title: channel,
              actorParticipantID: nick,
            },
            text: `${nick} changed the topic to "${topic}"`,
          }],
        },
      ])
    })

    this.client.on('join', (channel, nick, message) => {
      // Push to creds to automatically join this channel next session.
      if (!this.creds.channels.includes(channel)) this.creds.channels.push(channel)

      // We initially joined a channel
      if (nick === this.nick()) {
        onEvent([{
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'upsert',
          objectName: 'thread',
          entries: [this.channelThread(channel)],
        }])
      }

      // Needs to be after the onEvent above, otherwise self join won't register.
      onEvent([{
        type: ServerEventType.STATE_SYNC,
        objectIDs: {
          threadID: channel,
        },
        mutationType: 'upsert',
        objectName: 'message',
        entries: [{
          id: Date.now().toString(),
          timestamp: new Date(),
          senderID: 'none',
          isAction: true,
          isSender: false,
          action: {
            type: MessageActionType.THREAD_PARTICIPANTS_ADDED,
            participantIDs: [nick],
            actorParticipantID: nick,
          },
          text: `${nick} has joined`,
        }],
      }])
    })

    this.client.on('part', (channel, nick, reason, message) => {
      onEvent([
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'update',
          objectName: 'thread',
          entries: [this.channelThread(channel)],
        },
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'upsert',
          objectName: 'message',
          entries: [{
            id: Date.now().toString(),
            timestamp: new Date(),
            senderID: 'none',
            isAction: true,
            isSender: false,
            action: {
              type: MessageActionType.THREAD_PARTICIPANTS_REMOVED,
              participantIDs: [nick],
              actorParticipantID: nick,
            },
            text: `${nick} has left (${reason})`,
          }],
        },
      ])
    })

    this.client.on('quit', (nick, reason, channels, message) => {
      for (const channel of channels) {
        onEvent([
          {
            type: ServerEventType.STATE_SYNC,
            objectIDs: {
              threadID: channel,
            },
            mutationType: 'update',
            objectName: 'thread',
            entries: [this.channelThread(channel)],
          },
          {
            type: ServerEventType.STATE_SYNC,
            objectIDs: {
              threadID: channel,
            },
            mutationType: 'upsert',
            objectName: 'message',
            entries: [{
              id: Date.now().toString(),
              timestamp: new Date(),
              senderID: 'none',
              isAction: true,
              isSender: false,
              action: {
                type: MessageActionType.THREAD_PARTICIPANTS_REMOVED,
                participantIDs: [nick],
                actorParticipantID: nick,
              },
              text: `${nick} has quit (${reason})`,
            }],
          },
        ])
      }
    })

    this.client.on('kick', (
      channel: string,
      nick: string,
      by: string,
      reason: string,
    ) => {
      onEvent([
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'update',
          objectName: 'thread',
          entries: [this.channelThread(channel)],
        },
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: channel,
          },
          mutationType: 'upsert',
          objectName: 'message',
          entries: [{
            id: Date.now().toString(),
            timestamp: new Date(),
            senderID: 'none',
            isAction: true,
            isSender: false,
            action: {
              type: MessageActionType.THREAD_PARTICIPANTS_REMOVED,
              participantIDs: [nick],
              actorParticipantID: by,
            },
            text: `${nick} kicked from channel (${reason})`,
          }],
        },
      ])
    })

    this.client.on('nick', (
      oldnick: string,
      newnick: string,
      channels: string[],
      message,
    ) => {
      for (const channel of channels) {
        onEvent([this.channelAction(channel, `${oldnick} is now known as ${newnick}`)])
      }
    })

    this.client.on('whois', info => {
      if (!info || !info.channels) return

      // Fire event as a PM from the requested nick.
      onEvent([
        {
          type: ServerEventType.STATE_SYNC,
          objectIDs: {
            threadID: info.nick,
          },
          mutationType: 'upsert',
          objectName: 'message',
          entries: [{
            id: Date.now().toString(),
            timestamp: new Date(),
            senderID: 'none',
            isAction: true,
            isSender: false,
            textHeading: `Whois result for ${info.nick}`,
            text: `
                                Nickname: ${info.nick}
                                Username: ${info.user}
                                Host: ${info.host}
                                Realname: ${info.realname}
                                Channels: ${info.channels.join(', ')}
                                Server: ${info.server}
                                Server info: ${info.serverinfo}
                                Operator: ${info.operator}
                            `,
          }],
        },
      ])
    })

    this.client.on('+mode', (channel, by, mode, argument, message) => {
      onEvent([this.channelAction(channel, `${by} sets +${mode} on ${argument}`)])
    })

    this.client.on('-mode', (channel, by, mode, argument, message) => {
      onEvent([this.channelAction(channel, `${by} sets -${mode} on ${argument}`)])
    })

    this.client.on('motd', (motd: string) => {
      onEvent([{
        type: ServerEventType.TOAST,
        toast: {
          text: motd,
        },
      } as ToastEvent])
    })
  }

  private channelAction(channel: string, text: string): StateSyncEvent {
    return {
      type: ServerEventType.STATE_SYNC,
      objectIDs: {
        threadID: channel,
      },
      mutationType: 'upsert',
      objectName: 'message',
      entries: [{
        id: Date.now().toString(),
        timestamp: new Date(),
        senderID: 'none',
        isAction: true,
        isSender: false,
        text,
      }],
    }
  }

  public disconnect() {
    return new Promise<void>(resolve => {
      this.client.disconnect(this.creds.leaveMessage, () => {
        resolve()
      })
    })
  }

  public part(channel: string) {
    return new Promise<void>(resolve => {
      this.client.part(channel, this.creds.leaveMessage, () => {
        resolve()
      })
    })
  }

  public sessionCreds(): AuthCredentials {
    return this.creds
  }

  public currentUser(): CurrentUser {
    return {
      id: this.nick(),
      displayText: `${this.nick()} (${this.creds.server})`,
    }
  }

  public channelThread(channel: string): Thread {
    return {
      id: channel,
      isReadOnly: false,
      isUnread: false,
      type: 'channel',
      title: channel,
      description: this.client.chanData(channel).topic,
      messages: {
        items: [],
        hasMore: false,
      },
      participants: {
        items: this.participants(channel),
        hasMore: false,
      },
    }
  }

  public userThread(nick: string): Thread {
    return {
      id: nick,
      isReadOnly: false,
      isUnread: false,
      type: 'single',
      title: nick,
      messages: {
        items: [],
        hasMore: false,
      },
      participants: {
        items: [],
        hasMore: false,
      },
    }
  }

  public participants(channel: string): Participant[] {
    return Object.keys(this.client.chanData(channel).users).map(u => ({
      id: u,
      isSelf: u === this.nick(),
    }))
  }

  public allThreads(): Thread[] {
    return [
      ...Object.keys(this.client.chans)
        .map(c => this.channelThread(c)),

      ...this.allParticipants()
        .map(p => this.userThread(p.id)),
    ]
  }

  // Participants from all joined channels.
  // Does not include duplicates if user exists across multiple channels.
  public allParticipants(): Participant[] {
    const keys: Record<string, string> = {}

    return Object.keys(this.client.chans)
      .map(channel => this.participants(channel))
      .flat()
      .filter(p => {
        if (!(p.id in keys)) {
          keys[p.id] = ''

          return true
        }

        return false
      })
  }

  public nick(): string {
    return this.client.nick
  }

  public say(target: string, text: string): Promise<void> {
    return this.client.say(target, text)
  }

  public notice(target: string, text: string): Promise<void> {
    return this.client.notice(target, text)
  }

  public join(channel: string): Promise<void> {
    return this.client.join(channel)
  }

  public raw(args: string[]): Promise<void> {
    return this.client.send(...args)
  }

  public action(channel: string, text: string): Promise<void> {
    return this.client.action(channel, text)
  }

  public list(): Promise<any[]> {
    return new Promise(async resolve => {
      await this.client.list()

      this.client.once('channellist', (channels: any[]) => {
        resolve(channels)
      })
    })
  }

  public static isChannel(threadID: string): boolean {
    return ['&', '#', '+', '!'].includes(threadID.charAt(0))
  }
}
