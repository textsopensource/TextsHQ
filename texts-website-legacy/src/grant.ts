// @ts-expect-error
import Grant from 'grant/lib/grant'
import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'

import { IS_DEV } from './constants'

export default function registerGrantRoutes(app: FastifyInstance, config: any) {
  const grant = Grant({ config })

  const handler = async (request: FastifyRequest, reply: FastifyReply) => {
    if (request.session.uid) {
      reply.redirect(config.defaults.callback)
      return
    }
    const { provider, override } = request.params as any
    const { location, session, state } = await grant({
      method: request.raw.method,
      params: { provider, override },
      query: request.query,
      body: request.body,
      state: {},
      session: request.session.grant,
    })
    if (IS_DEV) console.log(location, session, state)
    request.session.grant = session
    reply.redirect(location)
  }

  app.get(`/${config.defaults.prefix}/:provider`, handler)
  app.get(`/${config.defaults.prefix}/:provider/:override`, handler)
  app.post(`/${config.defaults.prefix}/:provider/:override`, handler)
}
