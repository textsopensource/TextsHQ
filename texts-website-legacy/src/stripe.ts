import Stripe from 'stripe'

const config = require('./config')

export default new Stripe(config.STRIPE.SECRET_KEY, { apiVersion: null })
