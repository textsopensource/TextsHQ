import winston from 'winston'
import os from 'os'
import 'winston-syslog'

const { PAPERTRAIL } = require('./config')

// @ts-expect-error
const papertrail = new winston.transports.Syslog({
  host: PAPERTRAIL.HOST,
  port: PAPERTRAIL.PORT,
  protocol: 'tls4',
  localhost: os.hostname(),
  eol: '\n',
})

const logger = winston.createLogger({
  format: winston.format.simple(),
  levels: winston.config.syslog.levels,
  transports: [papertrail],
})

const ogConsoleLog = console.log
const ogConsoleError = console.error
console.log = (...args: any[]) => {
  logger.info(JSON.stringify(args))
  ogConsoleLog(...args)
}
console.error = (...args: any[]) => {
  logger.error(JSON.stringify(args))
  ogConsoleError(...args)
}

export default logger
