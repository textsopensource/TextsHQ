// from texts-app-desktop/src/common/default-remote-config

export enum RemoteConfKey {
  MAGIC_HEADER = 'magic_header',
  BUILD_EXPIRY = 'build_expiry',
  SOFT_BUILD_EXPIRY = 'soft_build_expiry',
  APP_UPDATE_CHECK_INTERVAL = 'app_update_check_interval',
  CONFIG_UPDATE_CHECK_INTERVAL = 'config_update_check_interval',
  ALLOWED_PLATFORM_NAMES = 'allowed_platform_names',
  DISALLOWED_PLATFORM_NAMES = 'disallowed_platform_names',
}

export const REMOTE_CONFIG_MAGIC_HEADER = 'TextsConf'

export const defaultRemoteConfig = {
  [RemoteConfKey.MAGIC_HEADER]: REMOTE_CONFIG_MAGIC_HEADER,
  [RemoteConfKey.BUILD_EXPIRY]: undefined as number,
  [RemoteConfKey.SOFT_BUILD_EXPIRY]: false,
  [RemoteConfKey.APP_UPDATE_CHECK_INTERVAL]: 1 * 60 * 60 * 1000, // 1 hour
  [RemoteConfKey.CONFIG_UPDATE_CHECK_INTERVAL]: 6 * 60 * 60 * 1000, // 6 hours
  [RemoteConfKey.DISALLOWED_PLATFORM_NAMES]: [] as string[],
}
