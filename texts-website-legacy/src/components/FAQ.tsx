import Template from './Template'

const children = (
  <div>
    <dl>
      <dt>What do you do for security? Are my messages secure?</dt>
      <dd>Messages never touch our servers. They're sent directly to the platforms preserving end-to-end encryption. Texts app works like the official app does.</dd>
      <dt>Where can I run the app?</dt>
      <dd>Texts app runs on macOS, Windows and Linux. Texts for iOS is in alpha testing.</dd>
      <dt>What messaging platforms do you support?</dt>
      <dd>iMessage, SMS (with iMessage), WhatsApp, Telegram, Signal, FB Messenger, Twitter, Instagram, LinkedIn, IRC, Slack and Discord DMs.</dd>
      <dt>How do you make money?</dt>
      <dd>Texts charges users a monthly subscription to use the app.</dd>
      <dt>How does it work technically?</dt>
      <dd>All integrations were implemented in-house using the Texts Platform SDK. The SDK will be open sourced at a later date.</dd>
    </dl>
  </div>
)

const FAQ = (
  <Template
    title="FAQ"
    header={<h1>FAQ</h1>}
    links={[
      { href: 'mailto:support@texts.com', title: 'Support' },
      { href: '/privacy', title: 'Privacy' },
      { href: '/jobs', title: "We're hiring!" },
    ]}
  >{children}
  </Template>
)

export default FAQ
