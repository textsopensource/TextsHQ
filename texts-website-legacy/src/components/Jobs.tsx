import Template from './Template'

const roles = [
  {
    title: 'Full-stack engineer',
    desc: `Stack: React.js, TypeScript, Node.js
Experience: mid-level, senior or more`,
  },
  {
    title: 'iOS engineer',
    desc: `Stack: Swift, SwiftUI
Experience: mid-level, senior or more`,
  },
  {
    title: 'Backend engineer',
    desc: `Stack: TypeScript, Node.js, Postgres, Rust
Experience: mid-level, senior or more`,
  },
  {
    title: 'Reverse engineer',
    desc: `Tools: Ghidra, IDA, or Frida
Experience: mid-level, senior or more`,
  },
]

const Jobs = (
  <Template
    title="Jobs"
    className="jobs"
    header={<h2>Join the founding team of Texts</h2>}
    links={[
      { href: 'mailto:support@texts.com', title: 'Support' },
      { href: '/privacy', title: 'Privacy' },
      { href: '/jobs', title: "We're hiring!" },
    ]}
  >
    <p>Texts is hiring engineers for our founding team. We're backed by investors affiliated to Stripe, Coinbase, Superhuman, Vercel, Twitter, Facebook, Snap and others.</p>
    <p>We believe messaging is extremely fragmented and are building Texts app, a messaging client for power users.</p>
    <p>You'll work with a distributed team asynchronously, be self-directed and make engineering and product decisions. You'll have interesting user-facing features to build and interesting technical challenges to solve.</p>
    <hr />
    <h3>Open roles – Remote (all timezones)</h3>
    {roles.map((role, idx) => (
      <div key={idx}>
        <h4>{idx + 1}. {role.title}</h4>
        <p style={{ whiteSpace: 'pre-wrap' }}>{role.desc}</p>
      </div>
    ))}
    <hr />
    <h3>Apply</h3>
    <p>
      Drop your link here and we'll get in touch:
      <form method="POST" action="/jobs-submission">
        <input name="url" className="styled" placeholder="Your Twitter/GitHub/LinkedIn/AngelList/website link" required />
        <button type="submit" className="button">Submit</button>
      </form>
    </p>
    <p>Alternatively, send a short email to <a href="mailto:jobs@texts.com">jobs@texts.com</a></p>
    <hr />
    <h3>Internal tools</h3>
    <p>We use GitHub, Sentry and <a href="https://linear.app/">Linear</a> to get stuff done.</p>
    <hr />
    <h3>Hiring process</h3>
    <p>We don't do whiteboard/algorithm interviews. We'll talk about things you've previously worked on and then do a work trial for a week – you'll be paid as a contractor for this.</p>
  </Template>
)

export default Jobs
