import { standardFooterLinks } from './footer-links'
import Template from './Template'

const script = `document.querySelector('textarea').onclick = function() {
  this.select()
  document.execCommand('copy')
}`

const InstallMacOS: React.FC<{
  installScriptURL: string
  query: string
}> = ({ installScriptURL, query }) => (
  <Template
    title="Install"
    header={<h1>Install on macOS</h1>}
    footerScript={script}
    links={[
      { href: '/install/windows', title: 'Install on Windows' },
      { href: '/install/linux', title: 'Install on Linux' },
      ...standardFooterLinks,
    ]}
  >
    <h3>Traditional install</h3>
    <h4><a href={'/install/macos/latest/x64.zip' + query}>Download for Intel-based Macs &rarr;</a></h4>
    <h4><a href={'/install/macos/latest/arm64.zip' + query}>Download for Apple silicon-based Macs &rarr;</a></h4>
    <a href="https://support.apple.com/HT211814"><small>How to find which Mac you're using &rarr;</small></a>
    <hr />
    <h3>Quick install</h3>
    <div>
      <ol>
        <li>Open Terminal.app (hit ⌘ Space and type "Terminal")</li>
        <li>Paste in:</li>
        <textarea className="code" cols={80} readOnly value={`curl ${installScriptURL} | sh`} />
        <li>Wait for Texts.app to open up! You'll automatically be logged into your Texts account.</li>
      </ol>
    </div>
  </Template>
)

export default InstallMacOS
