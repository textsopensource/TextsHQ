import Template from './Template'

const children = (
  <div>
    <li>Messages, contacts, auth credentials, account information never touch Texts servers.</li>
    <li>Your messages are sent directly to the messaging platforms.</li>
    <li>All end-to-end encryption is preserved when the platform supports it.</li>
    <li>Texts is a client and works like the official app. It's similar to a web browser like Chrome.</li>
    <li>Account credentials are stored locally with the help of system keychain (Apple Keychain/Credential Vault), similar to how a browser stores your passwords.</li>
    <li>Texts makes money by charging you a monthly subscription, not by monetizing data.</li>
    <br />
    <h4>Data we collect</h4>
    <li>Name and profile picture provided by Google</li>
    <li>Performance metrics</li>
    <li>Engagement metrics like last used</li>
    <li>Errors and crashes</li>
    <br />
    <h4>3rd party services we use</h4>
    <li><a href="https://stripe.com/privacy">Stripe</a></li>
    <li><a href="https://policies.google.com/privacy">Google Analytics</a></li>
    <li><a href="https://sentry.io/privacy">Sentry</a></li>
    <li><a href="https://www.cloudflare.com/privacypolicy/">Cloudflare</a></li>
    <li><a href="https://www.digitalocean.com/legal/privacy-policy/">Digital Ocean</a></li>
    <li><a href="https://aws.amazon.com/privacy/">AWS</a></li>
    <li><a href="https://superhuman.com/privacy">Superhuman</a></li>
    <h4><a href="/a/texts-privacy-policy.pdf">View full legal privacy policy &rarr;</a></h4>
  </div>
)

const Privacy = (
  <Template
    title="Privacy"
    header={<h1>Privacy</h1>}
    links={[
      { href: 'mailto:support@texts.com', title: 'Support' },
      { href: '/privacy', title: 'Privacy' },
      { href: '/jobs', title: "We're hiring!" },
    ]}
  >{children}
  </Template>
)

export default Privacy
