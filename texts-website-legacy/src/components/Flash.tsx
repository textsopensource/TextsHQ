import Template from './Template'

const Flash = ({ text }: any) => (
  <Template
    header={(
      <>
        <h1>Texts</h1>
        <p>{text}</p>
      </>
    )}
    links={[
      { href: 'mailto:support@texts.com', title: 'Support' },
      { href: '/privacy', title: 'Privacy' },
    ]}
  />
)

export default Flash
