import { standardFooterLinks } from './footer-links'
import Template from './Template'

const AppLoginHeader = ({ username, loginURL }: any) => (
  <>
    <h1>Texts</h1>
    <h2>Hi, {username}!</h2>
    <a className="button big" href={loginURL}>Continue to Texts.app</a>
    <p>If you don't have the app, <a href="/install">click here to download</a>.</p>
    {/* eslint-disable-next-line jsx-a11y/iframe-has-title */}
    <iframe style={{ visibility: 'hidden' }} src={loginURL} />
  </>
)

const AppLogin = (props: any) => (
  <Template
    title="Login"
    header={AppLoginHeader(props)}
    links={[
      { href: '/install', title: 'Download app' },
      ...standardFooterLinks,
    ]}
    footerScript={`if(/iPad|iPhone|iPod/.test(navigator.userAgent))window.location=${JSON.stringify(props.loginURL)}`}
  />
)

export default AppLogin
