import { standardFooterLinks } from './footer-links'
import Template from './Template'

const tweetPrompts = [
  "I'm on the @TextsHQ waitlist, can anyone send me an invite?",
  "I'm on the @TextsHQ waitlist, can anyone skip me ahead?",
  "I'm on the @TextsHQ waitlist, can someone send me an invite?",
  "I'm on the @TextsHQ waitlist, can someone skip me ahead?",
  "I'm on the Texts.com waitlist, can anyone send me an invite?",
  "I'm on the Texts.com waitlist, can anyone skip me ahead?",
  "I'm on the Texts.com waitlist, can someone send me an invite?",
  "I'm on the Texts.com waitlist, can someone skip me ahead?",
  'Anyone here using Texts.com who can spare an invite?',
  'Can anyone invite me to @TextsHQ?',
  'Can anyone invite me to Texts.com / @TextsHQ?',
  'Can anyone invite me to Texts.com?\n\ncc @TextsHQ',
  'Can someone invite me to @TextsHQ?',
  'Can someone invite me to Texts.com / @TextsHQ?',
  'Can someone invite me to Texts.com?\n\ncc @TextsHQ',
  'Does anyone have a @TextsHQ invite?',
  'Does anyone have a Texts.com / @TextsHQ invite?',
  'Does anyone have a Texts.com invite?',
  'Does anyone have a Texts.com invite?\n\ncc @TextsHQ',
  'Texts.com, can anyone invite me to it?',
  'Texts.com, can someone invite me to it?',
]

const header = (message: string) => (
  <>
    <h1>Texts</h1>
    <p>{message}</p>
    <h4><a href="https://twitter.com/intent/user?screen_name=TextsHQ">Follow @TextsHQ on Twitter for updates &rarr;</a></h4>
    <h4><a href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(tweetPrompts[~~(tweetPrompts.length * Math.random())])}`}>Get an invite from a Twitter friend &rarr;</a></h4>
  </>
)

const OnWaitlist: React.FC<{
  message: string
}> = ({ message }) => (
  <Template
    title="Waitlist"
    header={header(message)}
    links={standardFooterLinks}
  />
)

export default OnWaitlist
