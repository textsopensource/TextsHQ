import Template from './Template'

const BigIcon = (
  <svg className="big-icon" width="391" height="373" viewBox="0 0 391 373" fill="none">
    <path opacity="0.3" d="M112 233.304C112 156.368 174.369 94 251.304 94C328.24 94 390.608 156.369 390.608 233.304C390.608 310.24 328.24 372.608 251.304 372.608H128.555C119.412 372.608 112 365.196 112 356.053L112 233.304Z" fill="#334AC0" />
    <path opacity="0.1" d="M139.304 278.608C62.3685 278.608 -2.72622e-06 216.24 -6.08917e-06 139.304C-9.45213e-06 62.3685 62.3685 -2.72622e-06 139.304 -6.08918e-06C216.24 -9.45213e-06 278.608 62.3686 278.608 139.304L278.608 262.053C278.608 271.196 271.196 278.608 262.053 278.608L139.304 278.608Z" fill="#9BB1DE" />
  </svg>
)

const renderTweet = (link: string) =>
  <blockquote className="twitter-tweet"><a href={link} /></blockquote>

const footerScript = `
document.getElementById('manual-waitlist').onclick = function(ev) {
  ev.preventDefault()
  document.querySelector('.modal').style.display = null
  document.querySelector('input').focus()
}`

const HomePage = (
  <Template
    header={(
      <>
        <h1>Texts</h1>
        <h2>One inbox.<br />All your messages.</h2>
        <a className="button big" href="/auth/google" style={{ marginTop: 32 }}>Sign up with Google</a>
        <small style={{ marginTop: 4, display: 'block' }}><a href="#" id="manual-waitlist">Prefer entering email instead?</a></small>
      </>
    )}
    headChildren={(
      <>
        <meta property="og:title" content="Texts" />
        <meta property="og:description" content="The ultimate messaging app" />
        <meta property="og:site_name" content="Texts" />
        <meta property="og:url" content="https://texts.com/" />
        <meta property="og:type" content="product" />
        <meta property="og:image" content="/ogcover.png" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:image" content="https://texts.com/twitter-cover-3.png" />
        <script async src="https://platform.twitter.com/widgets.js" />
      </>
    )}
    links={[
      { href: 'https://twitter.com/TextsHQ', title: 'Twitter' },
      { href: 'mailto:support@texts.com', title: 'Support' },
      { href: '/faq', title: 'FAQ' },
      { href: '/privacy', title: 'Privacy' },
      { href: '/jobs', title: 'Jobs' },
    ]}
    mainChildren={BigIcon}
    footerScript={footerScript}
    className="home-page"
  >
    <figure className="hero">
      <img className="dark-mode-only" src="/a/hero-dark-v2.png" alt="Texts app" height="662" width="1012" style={{ display: 'none' }} />
      <img className="light-mode-only" src="/a/hero-light-v2.png" alt="Texts app" height="662" width="1012" style={{ display: 'none' }} />
    </figure>
    <section>
      <h2>All in one.</h2>
      <p>Texts has integrations for all major messaging platforms including iMessage, WhatsApp, Telegram, Signal, FB Messenger, Twitter, Instagram, LinkedIn, Slack, Discord and more.</p>
    </section>
    <section>
      <h2>Privacy first.</h2>
      <p>Messages never touch our servers. They're sent directly to the platforms preserving end-to-end encryption. We make money by charging you a monthly subscription.</p>
    </section>
    <section>
      <h2>Archive. Snooze. Mark as unread.</h2>
      <p>Never miss a message again. Archive chats to hit inbox zero. Keep chats unread by default until you respond to them. Snooze people that you don't want to get to just yet.</p>
    </section>
    <section>
      <h2>Send later.</h2>
      <p>Schedule messages so they get sent at appropriate times when people are active.</p>
    </section>
    <section>
      <h2>Search all messages.</h2>
      <p>Find that link, document, picture or video from forever ago easily.</p>
    </section>
    <section>
      <h2>Customizable.</h2>
      <p>Use the custom CSS feature to customize Texts to the last pixel.</p>
    </section>
    <div className="tweets">
      {renderTweet('https://twitter.com/nic__carter/status/1356665370740989953')}
      {renderTweet('https://twitter.com/brian_lovin/status/1350134524278001665')}
      {renderTweet('https://twitter.com/rauchg/status/1381703346998845440')}
      {renderTweet('https://twitter.com/karrisaarinen/status/1361928046257664001')}
      {renderTweet('https://twitter.com/martyrdison/status/1376710800170086411')}
      {renderTweet('https://twitter.com/KishanBagaria/status/1315751935333429248')}
    </div>
    <div className="modal" style={{ display: 'none' }}>
      <div className="waitlist-signup">
        <form method="POST" action="/waitlist-signup">
          <input type="email" name="email" className="styled" placeholder="Your email" required />
          <button type="submit" className="button big">Submit</button>
        </form>
      </div>
    </div>
    <a href="/jobs" className="we-re-hiring">We're hiring! ✨</a>
  </Template>
)

export default HomePage
