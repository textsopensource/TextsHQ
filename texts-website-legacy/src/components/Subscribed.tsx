import { standardFooterLinks } from './footer-links'
import Template from './Template'

const header = (
  <>
    <h1>Texts</h1>
    <h2>You're subscribed.</h2>
    <h2>Thanks for being an early supporter!</h2>
    <a href="/customer-portal">View billing history | Edit payment methods</a>
  </>
)

const Subscribed = (
  <Template
    title="Subscription"
    header={header}
    links={standardFooterLinks}
  />
)

export default Subscribed
