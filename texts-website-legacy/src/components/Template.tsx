const gaScript = {
  __html: `window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());
gtag('config', 'UA-15889022-31');`,
}

const Template: React.FC<{
  className?: string
  title?: string
  headChildren?: React.ReactNode
  mainChildren?: React.ReactNode
  header?: React.ReactNode
  links?: { href: string, title: string, className?: string }[]
  footerScript?: string
}> = ({ className, title: pageTitle, headChildren, mainChildren, header, links, footerScript, children }) => (
  <html lang="en">
    <head>
      <meta charSet="utf-8" />
      <title>{pageTitle || 'Texts'}</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.1/normalize.min.css" />
      <link rel="stylesheet" type="text/css" href="/style.css?30" />
      <link rel="apple-touch-icon" href="/favicon.png" />
      <link rel="icon" type="image/png" href="/favicon.png" />
      {headChildren}
      <meta name="description" content="The ultimate messaging app" />
      <script src="https://js.stripe.com/v3/" />
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-15889022-31" />
      <script dangerouslySetInnerHTML={gaScript} />
    </head>
    <body className={className}>
      <div className="wrapper">
        <main>
          <div>
            <img className="icon" src="/icon.png" alt="Texts" width="64" />
            <header>{header}</header>
          </div>
          {mainChildren}
        </main>
        <aside>{children}</aside>
      </div>
      <footer>
        {links && links.map(({ href, title, ...rest }) =>
          <a key={href} href={href} {...rest}>{title}</a>)}
      </footer>
      {footerScript && <script dangerouslySetInnerHTML={{ __html: footerScript }} />}
    </body>
  </html>
)

export default Template
