import Template from './Template'

const SwiftRuntimeSupport = (
  <Template
    title="Swift Runtime Support for CLI"
    header={(
      <>
        <h1>Swift Runtime Support</h1>
        <h2><a href="/_binaries/Swift_5_Runtime_Support_for_Command_Line_Tools.dmg">Download &rarr;</a></h2>
      </>
    )}
  >
    Install this to make sure contacts are loaded correctly on macOS. Having issues? Contact <a href="mailto:support@texts.com">support@texts.com</a>
  </Template>
)

export default SwiftRuntimeSupport
