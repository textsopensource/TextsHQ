import { standardFooterLinks } from './footer-links'
import Template from './Template'

const InstallLinux: React.FC<{
  query: string
}> = ({ query }) => (
  <Template
    title="Install"
    header={(
      <>
        <h1>Install on Linux</h1>
        <h2><a href={'/install/linux/latest.AppImage' + query}>Download &rarr;</a></h2>
      </>
    )}
    links={[
      { href: '/install/macos', title: 'Install on macOS' },
      { href: '/install/windows', title: 'Install on Windows' },
      ...standardFooterLinks,
    ]}
  />
)

export default InstallLinux
