import { standardFooterLinks } from './footer-links'
import Template from './Template'

const InstallWindows: React.FC<{
  query: string
}> = ({ query }) => (
  <Template
    title="Install"
    header={(
      <>
        <h1>Install on Windows</h1>
        <h2><a href={'/install/windows/latest.exe' + query}>Download &rarr;</a></h2>
      </>
    )}
    links={[
      { href: '/install/macos', title: 'Install on macOS' },
      { href: '/install/linux', title: 'Install on Linux' },
      ...standardFooterLinks,
    ]}
  />
)

export default InstallWindows
