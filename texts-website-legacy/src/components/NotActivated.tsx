import Template from './Template'

const Aside = (
  <small>Believe something's wrong? Contact <a href="mailto:support@texts.com">support@texts.com</a></small>
)

const Header = (
  <>
    <h1>Texts</h1>
    <h2>Your account isn't activated yet.</h2>
  </>
)

const NotActivated = (
  <Template
    title="Account Not Activated"
    header={Header}
    links={[
      { href: '/privacy', title: 'Privacy' },
      { href: '/logout', title: 'Logout' },
    ]}
  >{Aside}
  </Template>
)

export default NotActivated
