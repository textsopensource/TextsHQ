export const standardFooterLinks = [
  { href: '/subscription', title: 'Billing' },
  { href: '/privacy', title: 'Privacy' },
  { href: 'mailto:support@texts.com', title: 'Support' },
  { href: '/jobs', title: 'Jobs' },
  { href: '/logout', title: 'Logout' },
]
