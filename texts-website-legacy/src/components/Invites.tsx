import cn from 'classnames'

import { standardFooterLinks } from './footer-links'
import Template from './Template'

const { ORIGIN } = require('../config')

const script = `document.querySelectorAll('textarea').forEach(function(textarea) {
  textarea.onclick = function() {
    this.select()
    document.execCommand('copy')
  }
})`

type Invite = {
  inviteID: string
  used: boolean
  usedBy: string
}

const Invites: React.FC<{ invites: Invite[] }> = ({ invites }) => {
  const freeInviteCount = invites.filter(i => !i.used).length
  return (
    <div>
      <p>
        You have {freeInviteCount === 0 ? 'no' : freeInviteCount} {freeInviteCount === 1 ? 'invite' : 'invites'} available.{' '}
        {freeInviteCount === 0 ? 'Check back later!' : ''}
        {freeInviteCount > 0 && 'Copy a link and send it to your friend to invite them.'}
      </p>
      <div>
        {invites.map(({ inviteID, used, usedBy }) => {
          const link = `${ORIGIN}invite/${inviteID}`
          return (
            <div key={inviteID} className="invite">
              <textarea className={cn('code', { used })} readOnly value={link} cols={link.length + 1} />
              <div>{usedBy}</div>
            </div>
          )
        })}
      </div>
    </div>
  )
}

const InvitesPage = (props: any) => (
  <Template
    title="Invites"
    header={<h1>Invites</h1>}
    footerScript={script}
    links={standardFooterLinks}
    className="send-invite-page"
  >{Invites(props)}
  </Template>
)

export default InvitesPage
