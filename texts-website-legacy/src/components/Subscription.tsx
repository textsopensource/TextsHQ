import { standardFooterLinks } from './footer-links'
import Template from './Template'

const typeformScript = '(function() { var qs,js,q,s,d=document, gi=d.getElementById, ce=d.createElement, gt=d.getElementsByTagName, id="typef_orm_share", b="https://embed.typeform.com/"; if(!gi.call(d,id)){ js=ce.call(d,"script"); js.id=id; js.src=b+"embed.js"; q=gt.call(d,"script")[0]; q.parentNode.insertBefore(js,q) } })()'

type Price = {
  price: string
  desc: string
}

type Props = {
  stripePublishableKey: string
  checkoutSessionIDs: string[]
  yearly: boolean
  pricesMap: Record<string, Price>
}

const aside = (props: Props) => (
  <>
    <div className="pricing">
      {Object.entries(props.pricesMap).map(([priceID, price], index) => (
        <a key={index} id={`checkout-button-${index}`} href={`#cb${index}`}>
          <h3><span>${price.price}</span>/{props.yearly ? 'year' : 'month'}</h3>
          <h4>{price.desc}</h4>
          <p>&rarr;</p>
        </a>
      ))}
    </div>
    {props.yearly
      ? <p><a href="/subscription">Monthly pricing &rarr;</a></p>
      : <p><a href="/subscription?yearly">Yearly pricing &rarr;</a></p>}
    <p><a className="typeform-share" href="https://form.typeform.com/to/ZVPUSIzf?typeform-medium=embed-snippet" data-mode="popup" data-size="100" target="_blank" rel="noreferrer">Take our pricing survey to share your thoughts anonymously</a> &rarr;</p>
    <small>Student or unemployed? Contact <a href="mailto:support@texts.com">support@texts.com</a> for a discount</small>
  </>
)

const header = (
  <>
    <h1>Texts</h1>
    <h2>Pricing</h2>
    <h3 id="error-message" />
  </>
)

const getScript = ({ stripePublishableKey, checkoutSessionIDs }: Props) => `(function() {
var stripe = Stripe('${stripePublishableKey}');
var thenHandler = function (result) {
  if (result.error) {
    var displayError = document.getElementById('error-message');
    displayError.textContent = result.error.message;
  }
}

${checkoutSessionIDs.map((id, index) => `{
  var subscribe${index} = document.getElementById('checkout-button-${index}');
  subscribe${index}.addEventListener('click', function(e) {
    stripe.redirectToCheckout({ sessionId: '${id}' }).then(thenHandler);
    e.preventDefault();
  });
}`).join('\n')}

${typeformScript}
})();`

const Subscription: React.FC<Props> = props => (
  <Template
    title="Subscription"
    header={header}
    footerScript={getScript(props)}
    links={standardFooterLinks}
  >{aside(props)}
  </Template>
)

export default Subscription
