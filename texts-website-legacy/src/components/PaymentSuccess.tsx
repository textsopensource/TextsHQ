import { standardFooterLinks } from './footer-links'
import Template from './Template'

const PaymentSuccessHeader = (
  <>
    <h1>Texts</h1>
    <h2>Payment successful.</h2>
    <h2>Thanks for supporting Texts!</h2>
  </>
)

const PaymentSuccess = (
  <Template
    title="Payment Successful"
    header={PaymentSuccessHeader}
    links={standardFooterLinks}
  />
)

export default PaymentSuccess
