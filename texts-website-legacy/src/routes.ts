import path from 'path'
import osModule from 'os'
import semver from 'semver'
import { promises as fs } from 'fs'
import { addWeeks, differenceInDays } from 'date-fns'
import { cloneDeep, memoize } from 'lodash'
import type { Connection } from 'typeorm'
import type { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify'

import lang from './lang'
import { IS_DEV, SITE_ROOT, STATIC_DIR_PATH } from './constants'
import { getRepos } from './orm'
import { getIP, getIPCountry } from './util'
import User from './entity/User'
import Event, { EventType } from './entity/Event'
import AppSession from './entity/AppSession'
import Sentry from './init-sentry'
import { deleteAppLoginToken, getUIDFromAppLoginToken } from './tokens'
import registerHTMLRoutes from './html-routes'
import langEn from './lang-en'
import { replyWithReact, serveFlashMessage } from './react'
import { getUpdateFeed, getUpdateFeedJSON, getUpdateFeedYML } from './app-binaries'
import sendEmail from './emails'
import allEmails from './all-emails'
import { getChangelogEntries } from './changelog'
import { defaultRemoteConfig } from './remote-config'
import { sendFeedback, saveAttachment } from './send-feedback'
import logEvent from './analytics'
import OnWaitlist from './components/OnWaitlist'
import * as tgBot from './telegram-bot'
import { isUserOnSuperhuman } from './superhuman'
import type Invite from './entity/Invite'

const { TWITTER_HANDLE, ADMIN_USER_IDS, DISABLE_NO_INVITE_REGISTRATION, SKIP_UPDATE_FOR_VERSIONS, SECRETS, EXPIRE_BUILDS, DEBUG_SECRET } = require('./config')

const isValidOS = (os: string) => ['macos', 'windows', 'linux'].includes(os)
const isValidChannel = (channel: string) => ['stable', 'beta', 'nightly'].includes(channel)

// shared with texts-app-desktop
type LoggedInUser = {
  uid: string
  fullName: string
  emails: string[]
  paymentStatus: 'free' | 'trial' | 'premium' | 'expired'
  availableInvites: number
  flags?: string[]
}

const addCORSHeaders = (request: FastifyRequest, reply: FastifyReply) => {
  reply.header('Access-Control-Allow-Headers', 'Content-Type, x-device-id, x-app-version, x-os')
  if (request.headers.origin) {
    reply
      .header('Access-Control-Allow-Origin', request.headers.origin)
      .header('Access-Control-Allow-Credentials', 'true')
  } else {
    reply.header('Access-Control-Allow-Origin', '*')
  }
  return reply
}

function tryJSONParse(json: string): any {
  try {
    return JSON.parse(json)
  } catch (err) {
    Sentry.captureException(err, { extra: { str: json } })
  }
}

const logout = (request: FastifyRequest) => {
  request.destroySession(console.error)
  delete request.session
}

const getLoggedInUser = ({ id: uid, fullName, emails, paymentStatus, availableInvites }: User): LoggedInUser =>
  ({ uid, fullName, emails, paymentStatus, availableInvites })

function getInstallPageLink(ua = '') {
  if (ua.includes('Mac OS X')) return '/install/macos'
  if (ua.includes('Linux')) return '/install/linux'
  return '/install/windows'
  // return '/install'
}

export default function registerRoutes(app: FastifyInstance, conn: Connection) {
  const { userRepo, eventRepo, sessionRepo, inviteRepo, waitlistRepo } = getRepos(conn)
  const getUserFromDeviceID = memoize((deviceID: string) =>
    userRepo
      .createQueryBuilder('user')
      .where('user.id in (select uid from app_session where "deviceID" = :deviceID)', { deviceID })
      .getOne())

  async function onBuildExpiry(request: FastifyRequest, userPromise: User | Promise<User>, appVersion: string, osHeader: string, deviceID: string) {
    const user = await userPromise
    sendEmail(allEmails.userBuildExpired(user, `${appVersion} – ${osHeader} – ${deviceID}` + (user ? ' (user found through device ID)' : ''))).catch(console.error)
    const ip = getIP(request)
    const event = Event.create(EventType.BUILD_EXPIRED, ip, user?.id, { appVersion, os: osHeader, deviceID })
    await eventRepo.save(event)
  }
  function upsertSession(sid: string, uid: string, ip: string, appVersion: string, deviceID: string, os: string) {
    const sess = AppSession.create(sid, uid, ip, appVersion, deviceID, os)
    return sessionRepo.save(sess)
  }
  async function loginUser(request: FastifyRequest, user: User, eventBody = {}) {
    if (!user) throw Error("Couldn't find the user")
    // await redis.set('user-login:' + user.id, request.session.sessionId)
    const ip = getIP(request)
    const ua = request.headers['user-agent']
    request.session.uid = user.id
    request.session.originIP = ip
    request.session.ua = ua
    const event = Event.create(EventType.USER_LOGIN, ip, user.id, eventBody)
    await eventRepo.save(event)
  }
  async function sendWelcomeEmail(user: User) {
    const usesSuperhuman = await isUserOnSuperhuman(user.emails[0], user.fullName)
    return sendEmail(allEmails.welcome(user, usesSuperhuman)).catch(console.error)
  }
  async function createUserFromProfile(request: FastifyRequest, userEmails: string[], invite?: Invite) {
    const { grant } = request.session
    const { sub, given_name, family_name } = grant.response.profile
    const invitedByUser = invite?.createdByUser
    const user = new User()
    user.assignRandomID()
    user.emails = userEmails
    user.googleInfo = grant.response
    user.googleAuthID = sub
    const [firstName, middleName] = given_name.split(' ')
    user.firstName = firstName
    user.middleName = middleName
    user.lastName = family_name
    user.invitedByUser = invitedByUser
    const createdUser = await userRepo.save(user)
    if (invite) {
      invite.usedByUser = user.id
      await inviteRepo.save(invite)
    }
    const ip = getIP(request)
    const ev = Event.create(EventType.USER_CREATE, ip, user.id, invite ? { invitedByUser: user.invitedByUser } : {})
    await eventRepo.save(ev)
    if (ADMIN_USER_IDS.includes(invitedByUser)) sendEmail(allEmails.welcomeAdminOnly(user)).catch(console.error)
    else sendWelcomeEmail(user)
    delete request.session.inviteID
    return createdUser
  }

  registerHTMLRoutes(app, conn)

  app.get('/twitter', (request, reply) => {
    reply.redirect(`https://twitter.com/${TWITTER_HANDLE}`)
  })

  app.options('/metrics', (request, reply) => {
    addCORSHeaders(request, reply).send()
  })

  app.post('/metrics', async (request, reply) => {
    addCORSHeaders(request, reply)
    const res = await logEvent(request.body as any, request.ip, getUserFromDeviceID)
    reply.send(res)
  })

  app.post(`/stripe-webhook/${SECRETS.STRIPE_WEBHOOK}`, async (request, reply) => {
    const { type, data } = request.body as any
    const { customer } = data?.object || {}
    if (!type.startsWith('customer.subscription')) return reply.code(204).send()
    if (!customer) return reply.code(204).send()
    const user = await userRepo.findOneOrFail({ stripeCustomerID: customer })
    const { paymentStatus } = user
    await user.syncStripeSubStatus()
    if (user.paymentStatus !== paymentStatus) {
      tgBot.userPaymentStatusChanged(user, paymentStatus, user.paymentStatus)
    }
    await userRepo.save(user)
    reply.send('"OK"')
  })

  app.get('/conf.json', (request, reply) => {
    reply.code(204).send()
  })

  app.get('/update-feed.json', async (request, reply) => {
    const { version: installedVersion, channel, platform = 'darwin', arch, mid } = request.query as any
    // @ts-expect-error
    const os = {
      darwin: 'macos',
      linux: 'linux',
      win32: 'windows',
    }[platform] || platform
    if (!isValidOS(os)) {
      reply.code(400).send()
      return
    }
    const deviceID = request.headers['x-device-id'] || mid
    const updateFeed = await getUpdateFeed({
      os,
      channel,
      arch,
      deviceID,
      installedVersion: installedVersion as string,
    })
    if (updateFeed) {
      reply.send(updateFeed)
    } else {
      reply.code(204).send()
    }
  })

  app.get('/update-feed/:name', async (request, reply) => {
    // @ts-expect-error
    const os = {
      'latest-mac.yml': 'macos',
      'latest-linux.yml': 'linux',
      'latest.yml': 'windows',
    }[(request.params as any).name]
    if (!os) return reply.code(400).send()
    const { version: installedVersion, channel, arch, mid } = request.query as any
    const deviceID = request.headers['x-device-id'] || mid
    const yml = await getUpdateFeedYML({ os, channel, arch, deviceID, installedVersion })
    return reply.send(yml)
  })

  app.get('/i/:name.sh', async (request, reply) => {
    const sh = await fs.readFile(path.join(SITE_ROOT, 'macos-install-texts-script-invalid.sh'), 'utf-8')
    reply.send(sh)
  })

  app.get('/i/:authToken/:name.sh', async (request, reply) => {
    const { authToken } = request.params as any
    const { channel = 'stable' } = request.query as any
    if (!isValidChannel(channel)) {
      return reply.send('echo bad request')
    }
    const sh = await fs.readFile(path.join(SITE_ROOT, 'macos-install-texts-script.sh'), 'utf-8')
    const [updateFeedX64, updateFeedArm64] = await Promise.all([
      getUpdateFeedJSON('macos', channel, 'x64'),
      getUpdateFeedJSON('macos', channel, 'arm64'),
    ])
    const uid = await getUIDFromAppLoginToken(authToken)
    if (uid) {
      const ip = getIP(request)
      const ua = request.headers['user-agent']
      const event = Event.create(EventType.INSTALL_SCRIPT_FETCH, ip, uid, { ua })
      await eventRepo.save(event)
      reply.send(sh
        .replace('{{DOWNLOAD_URL_X64}}', updateFeedX64.url)
        .replace('{{DOWNLOAD_URL_ARM64}}', updateFeedArm64.url)
        .replaceAll('{{AUTH_TOKEN}}', authToken))
    } else {
      const invalidSh = await fs.readFile(path.join(SITE_ROOT, 'macos-install-texts-script-invalid.sh'), 'utf-8')
      reply.send(invalidSh)
    }
  })

  const redirectMacDownload = (arch: 'x64' | 'arm64') => async (request: FastifyRequest, reply: FastifyReply) => {
    const { uid } = request.session
    const { channel = 'stable' } = request.query as any
    if (!isValidChannel(channel)) return reply.code(400).send()
    const os = 'macos'
    const updateFeed = await getUpdateFeedJSON(os, channel, arch)
    if (uid) {
      const ip = getIP(request)
      const ua = request.headers['user-agent']
      const event = Event.create(EventType.DOWNLOAD_BINARY, ip, uid, { ua, appVersion: updateFeed.version, os, channel, arch })
      await eventRepo.save(event)
    }
    reply.redirect(updateFeed.url)
  }

  app.get('/install', (request, reply) => {
    reply.redirect(getInstallPageLink(request.headers['user-agent']))
  })

  app.get('/install/macos/latest/x64.zip', redirectMacDownload('x64'))
  app.get('/install/macos/latest/arm64.zip', redirectMacDownload('arm64'))

  app.get('/install/windows/latest.exe', async (request, reply) => {
    const { uid } = request.session
    const { channel = 'stable' } = request.query as any
    if (!isValidChannel(channel)) return reply.code(400).send()
    const os = 'windows'
    const updateFeed = await getUpdateFeedJSON(os, channel)
    if (uid) {
      const ip = getIP(request)
      const ua = request.headers['user-agent']
      const event = Event.create(EventType.DOWNLOAD_BINARY, ip, uid, { ua, appVersion: updateFeed.version, os, channel })
      await eventRepo.save(event)
    }
    reply.redirect(updateFeed.url)
  })

  app.get('/install/linux/latest.AppImage', async (request, reply) => {
    const { uid } = request.session
    const { channel = 'stable' } = request.query as any
    if (!isValidChannel(channel)) return reply.code(400).send()
    const os = 'linux'
    const updateFeed = await getUpdateFeedJSON(os, channel)
    if (uid) {
      const ip = getIP(request)
      const ua = request.headers['user-agent']
      const event = Event.create(EventType.DOWNLOAD_BINARY, ip, uid, { ua, appVersion: updateFeed.version, os, channel })
      await eventRepo.save(event)
    }
    reply.redirect(updateFeed.url)
  })

  app.get('/auth/done', async (request, reply) => {
    const { uid, grant } = request.session
    let user: User
    if (uid) user = await userRepo.findOneOrFail(uid)
    else if (grant?.response?.profile) {
      if (IS_DEV) console.log(grant)
      if (grant.provider !== 'google') {
        throw Error('provider !== google')
      }
      const { email, email_verified } = grant.response.profile
      if (!email_verified) {
        delete request.session.grant
        serveFlashMessage(langEn.GOOGLE_EMAIL_NOT_VERIFIED, reply)
        return
      }
      const emails = [email].filter(Boolean)
      if (emails.length === 0) {
        delete request.session.grant
        serveFlashMessage(langEn.GOOGLE_NO_EMAIL, reply)
        return
      }
      user = await userRepo.findOne({ emails })
      if (!user) {
        const { inviteID } = request.session
        if (DISABLE_NO_INVITE_REGISTRATION && !inviteID) {
          delete request.session.grant
          const waitlistEntry = await waitlistRepo.findOne({ email })
          if (waitlistEntry) {
            waitlistEntry.checkCount = (waitlistEntry.checkCount || 0) + 1
            await waitlistRepo.save(waitlistEntry)
            replyWithReact(reply, OnWaitlist, { message: langEn.ALREADY_ON_WAITLIST })
          } else {
            const firstName = grant.response.profile.given_name
            const lastName = grant.response.profile.family_name
            const newWaitlistRecord = waitlistRepo.create({
              email,
              firstName,
              lastName,
              os: request.headers['user-agent'],
              ip: getIP(request),
              country: getIPCountry(request),
            })
            await waitlistRepo.save(newWaitlistRecord)
            replyWithReact(reply, OnWaitlist, { message: langEn.ADDED_TO_WAITLIST })
            sendEmail(allEmails.onWaitlist(email, firstName)).catch(console.error)
          }
          return
        }
        if (inviteID) {
          const invite = await inviteRepo.findOne(inviteID)
          if (!invite) {
            serveFlashMessage(langEn.INVITE_NOT_FOUND, reply)
            return
          }
          if (invite.usedByUser) {
            Sentry.captureMessage(`INVITE_ALREADY_USED: ${inviteID}`, {
              extra: {
                sid: request.session.sessionId,
                esid: request.session.encryptedSessionId,
                invite,
              },
            })
            serveFlashMessage(langEn.INVITE_ALREADY_USED, reply)
            return
          }
          user = await createUserFromProfile(request, emails, invite)
        } else {
          user = await createUserFromProfile(request, emails)
        }
      }
      await loginUser(request, user)
      delete request.session.grant
    } else {
      reply.redirect('/')
      return
    }
    if (!user) {
      reply.send('No user found')
      logout(request)
    }
    const { redirectTo } = request.session
    request.session.redirectTo = null
    reply.redirect(redirectTo || '/app-login')
  })

  app.get('/logout', async (request, reply) => {
    const { uid } = request.session
    logout(request)
    reply.redirect('/')
    if (!uid) return
    const ip = getIP(request)
    const event = Event.create(EventType.USER_LOGOUT, ip, uid, {})
    await eventRepo.save(event)
  })

  app.options('/api/*', (request, reply) => {
    addCORSHeaders(request, reply).send()
  })

  app.get('/api/conf', async (request, reply) => {
    addCORSHeaders(request, reply)
    const deviceID = request.headers['x-device-id'] as string
    const appVersion = request.headers['x-app-version'] as string
    const osHeader = request.headers['x-os'] as string
    const conf = cloneDeep(defaultRemoteConfig)
    const { uid } = request.session
    const user = uid ? await userRepo.findOne(uid) : undefined
    if (semver.valid(appVersion) && !SKIP_UPDATE_FOR_VERSIONS.includes(appVersion) && (
      semver.lte(appVersion, EXPIRE_BUILDS.LTE_VERSION)
      || ((osHeader ? [osHeader.split(' ')[0], '*'] : []).includes(EXPIRE_BUILDS.SPECIFIC_VERSIONS[appVersion]))
    )) {
      conf.build_expiry = 1 // 1970-01-01T00:00:00.001Z
      onBuildExpiry(request, user || getUserFromDeviceID(deviceID), appVersion, osHeader, deviceID)
    } else {
      const buildExpiry = addWeeks(new Date(), 2)
      conf.build_expiry = new Date(buildExpiry).getTime()
    }
    if (!uid) return reply.send(conf)
    if (!user) return reply.send(conf)
    const anyConf = conf as any
    anyConf.signal_enabled = true
    anyConf.slack_enabled = true
    // if (user.flags.includes('matrix')) anyConf.matrix_enabled = true
    // conf.disallowed_platform_names = conf.disallowed_platform_names.filter(x => x !== 'fb-messenger')
    console.log(uid, { conf })
    reply.send(conf)
  })

  app.post('/api/app-login', async (request, reply) => {
    addCORSHeaders(request, reply)
    const deviceID = request.headers['x-device-id'] as string
    const appVersion = request.headers['x-app-version'] as string
    const osHeader = request.headers['x-os'] as string
    const { token } = request.body as any
    const { uid: existingUID } = request.session
    if (existingUID) {
      console.warn('existing uid', existingUID)
    }
    const uid = await getUIDFromAppLoginToken(token)
    if (!uid) {
      reply.send({ error: lang.LOGIN_FAIL_NO_UID })
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    if (!user.isActivated) {
      reply.send({ error: lang.LOGIN_FAIL_ACCOUNT_UNACTIVATED })
      return
    }
    await loginUser(request, user, { deviceID })
    await upsertSession(request.session.sessionId, user.id, getIP(request), appVersion, deviceID, osHeader)
    await deleteAppLoginToken(token)
    const { emails } = user
    reply.send({
      login_hint: {
        platform: 'google',
        display_name: emails[0],
      },
      user: getLoggedInUser(user),
    })
  })

  app.post('/api/app-logout', async (request, reply) => {
    addCORSHeaders(request, reply)
    const deviceID = request.headers['x-device-id']
    const { uid } = request.session
    logout(request)
    reply.code(204).send()
    if (uid) {
      const ip = getIP(request)
      const event = Event.create(EventType.USER_LOGOUT, ip, uid, { deviceID })
      await eventRepo.save(event)
    }
  })

  app.get('/api/validate-session', async (request, reply) => {
    addCORSHeaders(request, reply)
    const { uid } = request.session
    const appVersion = request.headers['x-app-version'] as string
    const deviceID = request.headers['x-device-id'] as string
    const osHeader = request.headers['x-os'] as string
    if (!uid) {
      console.log('logging out', request.session.id, appVersion, osHeader)
      reply.send({ logged_in: false })
      // reply.code(500).send()
      return
    }
    const user = await userRepo.findOne(uid)
    if (!user) {
      reply.send({ error: lang.ACCOUNT_NOT_FOUND })
      return
    }
    if (!user.isActivated) {
      reply.send({ error: lang.LOGIN_FAIL_ACCOUNT_UNACTIVATED })
      return
    }
    const diffDays = differenceInDays(new Date(), user.lastActive || user.createdAt)
    if (diffDays > 7) {
      tgBot.userUsedAppAfterDays(user, diffDays, appVersion, osHeader)
    }
    user.lastActive = new Date()
    user.sessionValidateCount += 1
    await userRepo.save(user)
    await upsertSession(request.session.sessionId, uid, getIP(request), appVersion, deviceID, osHeader)
    reply.send({
      logged_in: !!uid,
      user: getLoggedInUser(user),
    })
  })

  app.get('/api/changelog', async (request, reply) => {
    addCORSHeaders(request, reply)
    const appVersion = request.headers['x-app-version'] as string
    const entries = await getChangelogEntries(appVersion)
    reply.send({ entries })
  })

  app.get('/api/platform-status', async (request, reply) => {
    addCORSHeaders(request, reply)
    const json = await fs.readFile(path.join(STATIC_DIR_PATH, 'platform-status.json'), 'utf-8')
    reply.send(JSON.parse(json))
  })
  app.get('/api/platform-integration-store', async (request, reply) => {
    addCORSHeaders(request, reply)
    const json = await fs.readFile(path.join(SITE_ROOT, 'platform-integration-store.json'), 'utf-8')
    reply.send(JSON.parse(json))
  })
  app.get('/api/app-icons', async (request, reply) => {
    addCORSHeaders(request, reply)
    const json = await fs.readFile(path.join(SITE_ROOT, 'app-icons.json'), 'utf-8')
    reply.send(JSON.parse(json))
  })

  app.post('/api/feedback', async (request, reply) => {
    addCORSHeaders(request, reply)

    const { uid } = request.session
    if (!uid) {
      reply.code(403).send({ error: lang.ACCOUNT_NOT_FOUND })
      return
    }
    const user = await userRepo.findOne(uid)
    if (!user) {
      reply.code(403).send({ error: lang.ACCOUNT_NOT_FOUND })
      return
    }

    const json = await sendFeedback(request, user)
    reply.send(json)
  })

  app.post('/api/feedback/upload', async (request, reply) => {
    addCORSHeaders(request, reply)

    const json = await saveAttachment(request)
    reply.send(json)
  })

  app.get(`/debug/${DEBUG_SECRET}`, (request, reply) => {
    reply.send({
      uptime: process.uptime(),
      hostname: osModule.hostname(),
      loadavg: osModule.loadavg(),
      env: process.env,
    })
  })

  app.get('/deeplink/*', (request, reply) => {
    reply.redirect('texts://' + request.url.slice('/deeplink/'.length))
  })

  app.get('/inbox-zero-wallpaper', (request, reply) => {
    reply.redirect('https://source.unsplash.com/2000x1200/?nature')
  })
}
