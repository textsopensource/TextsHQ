import type { FastifyRequest } from 'fastify'
import crypto from 'crypto'
import { ensureDir, readFile, createWriteStream, unlink } from 'fs-extra'
import { LinearClient } from '@linear/sdk'
import path from 'path'

import allEmails from './all-emails'
import sendEmail from './emails'
import bot from './telegram-bot'
import { SITE_ROOT } from './constants'
import type User from './entity/User'

const { TELEGRAM_BOT, LINEAR } = require('./config')

const LINEAR_TEAM_ID = '4be12dc3-8799-4c19-ae96-fe0f492d0b6f'
const { FEEDBACK_CHAT_ID } = TELEGRAM_BOT

type FeedbackRequest = {
  text: string
  emotion: string
  meta: {
    activePlatform: string
    connectedPlatforms: string[]
    isWKWV?: boolean
  }
  attachment?: {
    type: string
    id?: string
    data?: string
  }
}

const UNRESPONDED_HASHTAG = '\n🟡 #unresponded'
const RESPONDED_HASHTAG = '\n✅ #responded'

const FEEDBACK_FILES_DIR_PATH = path.join(SITE_ROOT, 'feedback-files')

bot.on('callback_query', async ({ message, data: data_, id }) => {
  const data = JSON.parse(data_)

  // We might want to add something later
  if (data.type === 'mark_as_responded') {
    await bot.editMessageReplyMarkup(null, { message_id: message.message_id, chat_id: message.chat.id })
    bot.editMessageText(message.text.replace(UNRESPONDED_HASHTAG, RESPONDED_HASHTAG), { message_id: message.message_id, chat_id: message.chat.id, parse_mode: 'Markdown' })
    bot.answerCallbackQuery(id, { text: 'Marked as responded!' })
  }
})

function getIssueText(appVersion: string, osHeader: string, feedback: FeedbackRequest) {
  const { text, emotion, meta } = feedback
  return `
${text}

-
${emotion || ''}
Active: ${meta.activePlatform}
Connected: ${meta.connectedPlatforms.join(', ')}
Texts v${appVersion} on ${osHeader}
WKWV: ${meta.isWKWV}
`
}

async function sendFeedbackToChannel(user: User, appVersion: string, osHeader: string, feedback: FeedbackRequest, linearUrl?: string) {
  const { attachment } = feedback
  const fromUser = user.firstName ? `${user.firstName} ${user.lastName || ''}` : (user.emails[0] || user.id)

  const msgTxt = `${fromUser} ${user.emails?.[0] ? `(${user.emails[0]}) ` : ''}wrote in:

${getIssueText(appVersion, osHeader, feedback).trim()}

🔍 Linear: ${linearUrl}
` + UNRESPONDED_HASHTAG

  const msg = await bot.sendMessage(FEEDBACK_CHAT_ID, msgTxt, {
    parse_mode: 'Markdown',
    disable_web_page_preview: true,
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: '✅ Mark as responded',
            callback_data: JSON.stringify({
              type: 'mark_as_responded',
            }),
          },
        ],
      ],
    },
  })

  console.log(msg)

  async function sendMessages(data: any) {
    if (attachment.type.startsWith('image')) await bot.sendPhoto(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
    else await bot.sendDocument(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
  }

  if (attachment) {
    // legacy format
    if (attachment.data) {
      const data = Buffer.from(attachment.data.split('base64,')[1], 'base64')
      await sendMessages(data)
    }

    // new format
    if (attachment.id) {
      const attachmentPath = path.join(FEEDBACK_FILES_DIR_PATH, attachment.id)
      const data = await readFile(attachmentPath)
      await sendMessages(data)

      await unlink(attachmentPath)
    }
  }
}

async function sendFeedbackToLinear(user: User, appVersion: string, osHeader: string, feedback: FeedbackRequest) {
  const linear = new LinearClient({ apiKey: LINEAR.API_KEY })
  const fromUser = user.firstName ? `${user.firstName} ${user.lastName || ''}` : (user.emails[0] || user.id)

  const issueTxt = getIssueText(appVersion, osHeader, feedback)

  const res = await linear.issueCreate({
    teamId: LINEAR_TEAM_ID,
    title: `Feedback from ${fromUser} ${user.emails?.[0] ? `(${user.emails[0]}) ` : ''}`,
    description: issueTxt,
  })

  return res.issue
}

export async function sendFeedback(req: FastifyRequest, user: User) {
  const appVersion = req.headers['x-app-version'] as string
  const osHeader = req.headers['x-os'] as string

  const fr = req.body as FeedbackRequest
  fr.meta.activePlatform = fr.meta.activePlatform?.replaceAll('whatsapp-baileys', 'whatsapp')
  fr.meta.connectedPlatforms = fr.meta.connectedPlatforms.map(p => (p === 'whatsapp-baileys' ? 'whatsapp' : p))
  const issue = await sendFeedbackToLinear(user, appVersion, osHeader, fr)
  await sendFeedbackToChannel(user, appVersion, osHeader, fr, issue.url)
  if (fr.text) {
    sendEmail(allEmails.userSentFeedback(user, getIssueText(appVersion, osHeader, fr))).catch(console.error)
  }

  return { ok: true }
}

export async function saveAttachment(req: FastifyRequest) {
  const { file } = await req.file()
  const fileName = crypto.randomUUID()
  await ensureDir(FEEDBACK_FILES_DIR_PATH)
  const filePath = path.join(FEEDBACK_FILES_DIR_PATH, fileName)
  const writeStream = createWriteStream(filePath)
  return new Promise(resolve => {
    file.pipe(writeStream).on('finish', () => {
      resolve({ id: fileName })
    })
  })
}
