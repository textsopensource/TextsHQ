import { uploadEvents } from './uploadEvents'
import { uploadUsers } from './uploadUsers'

const { JUNE_API_KEY } = require('../../config')

async function main() {
  if (!JUNE_API_KEY) throw new Error('MISSING API KEY FOR JUNE!!!')

  console.log('Users')
  await uploadUsers()
  console.log('Events')
  await uploadEvents()
}

main().then(() => process.exit())
