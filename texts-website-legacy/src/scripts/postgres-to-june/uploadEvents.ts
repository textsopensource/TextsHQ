import got from 'got'
import { chunk } from 'lodash'
import { Presets, SingleBar } from 'cli-progress'
import { promisify } from 'util'
import { createORMConn } from '../../orm'
import Event from '../../entity/Event'

const { JUNE_API_KEY } = require('../../config')

function createPromise(event: Event) {
  return got.post('https://api.june.so/api/track', {
    json: {
      type: 'track',
      event: event.type,
      userId: event.uid,
      timestamp: event.createdAt.toISOString(),
      properties: { body: event.body, ipAddress: event.ip },
    },
    headers: {
      Authorization: `Basic ${JUNE_API_KEY}`,
    },
  })
}

export async function uploadEvents() {
  const conn = await createORMConn()

  const results = await conn
    .getRepository(Event)
    .createQueryBuilder()
    .getMany()

  const pb = new SingleBar({}, Presets.shades_classic)
  pb.start(results.length, 0)
  for (const eventChunk of chunk(results, 96)) {
    await Promise.all(eventChunk.map(createPromise))
    pb.increment(eventChunk.length)
    await promisify(setTimeout)(200)
  }
  pb.stop()

  await conn.close()
}
