import got from 'got'
import { chunk } from 'lodash'
import { SingleBar, Presets } from 'cli-progress'
import { promisify } from 'util'
import User from '../../entity/User'
import { createORMConn } from '../../orm'

const { JUNE_API_KEY } = require('../../config')

function createPromise(user: User) {
  return got.post('https://api.june.so/api/identify', {
    json: {
      type: 'identify',
      userId: user.id,
      traits: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.emails[0],
        emails: user.emails,
        createdAt: user.createdAt,
        lastActive: user.lastActive,
        avatar: user.googleInfo?.profile.picture,
      },
    },
    headers: {
      Authorization: `Basic ${JUNE_API_KEY}`,
    },
  })
}

export async function uploadUsers() {
  const conn = await createORMConn()

  const results = await conn
    .getRepository(User)
    .createQueryBuilder()
    .getMany()

  const pb = new SingleBar({}, Presets.shades_classic)
  pb.start(results.length, 0)
  for (const userChunk of chunk(results, 96)) {
    await Promise.all(userChunk.map(createPromise))
    pb.increment(userChunk.length)
    await promisify(setTimeout)(200)
  }
  pb.stop()

  await conn.close()
}
