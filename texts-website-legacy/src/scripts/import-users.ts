import bluebird from 'bluebird'

import { createORMConn, getRepos } from '../orm'

const TIMEZONE = ' +0530'

const users = 'cus_I,k@gmail.com,K,2021-01-23 17:46,usr_01EW'.split('\n').map(l => l.split(','))

async function main() {
  const conn = await createORMConn()
  const { userRepo } = getRepos(conn)
  await Promise.all(users)
  await bluebird.map(users, u => {
    const [stripeCustomerID, email, fullName, created, id] = u
    const [firstName = '', lastName = ''] = fullName.split(' ') || []
    const user = userRepo.create({
      id,
      emails: [email],
      stripeCustomerID,
      createdAt: new Date(created + TIMEZONE),
      firstName,
      lastName,
    })
    return userRepo.save(user)
  })
  console.log('done')
}
main()
