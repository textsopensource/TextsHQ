import fs from 'fs'
import path from 'path'

import { STATIC_DIR_PATH } from '../constants'
import { renderReactToStaticMarkup } from '../react'
import HomePage from '../components/HomePage'

export default function compileHomePage() {
  fs.writeFileSync(
    path.join(STATIC_DIR_PATH, 'index.html'),
    renderReactToStaticMarkup(HomePage, null, true),
  )
}

if (require.main === module) {
  compileHomePage()
}
