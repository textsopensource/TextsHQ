import { promises as fs } from 'fs'
import type { InsertResult, Repository } from 'typeorm'

import Waitlist from '../entity/Waitlist'
import { createORMConn, getRepos } from '../orm'

const filename = process.argv[2] || '_waitlist.csv'

const TIMEZONE = '+0530'

// https://github.com/typeorm/typeorm/issues/1090#issuecomment-582669430
function upsert<T>(repo: Repository<T>, data: any[], primaryKey: string): Promise<InsertResult> {
  const row = Array.isArray(data) ? data[0] : data
  const keys = Object.keys(row)

  if (keys.length < 1) {
    throw new Error('Cannot upsert without any values specified')
  }

  const updateStr = keys.map(key => `"${key}" = EXCLUDED."${key}"`).join(',')

  return repo
    .createQueryBuilder()
    .insert()
    .values(data)
    .onConflict(`("${primaryKey}") DO UPDATE SET ${updateStr}`)
    .execute()
}

async function main() {
  const conn = await createORMConn()
  const { waitlistRepo } = getRepos(conn)
  const txt = await fs.readFile(filename, 'utf-8')
  const emails = new Set<string>()
  const entries = txt.split('\n').map(line => {
    if (!line) return
    const [created, _email, os] = line.split(',')
    const email = _email.replace('+texts@gmail.com', '@gmail.com').toLowerCase().trim()
    if (emails.has(email)) return
    emails.add(email)
    const w = new Waitlist()
    w.createdAt = new Date(created + TIMEZONE)
    w.os = os.trim()
    w.email = email
    return w
  }).filter(Boolean)
  await upsert(waitlistRepo, entries, 'email')
}
main()
