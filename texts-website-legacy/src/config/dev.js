module.exports = {
  ORIGIN: 'https://dev.texts.com/',
  DEFAULT_PORT: 443,
  TYPEORM: {
    type: 'postgres',
    host: '127.0.0.1',
    port: 5432,
    username: 'postgres',
    password: 'password',
    database: 'jack',
  },
  SECRETS: {
    SESSION: 'cT%uFp5rpZJ97&Y!cPDKy!F%qTa32imBAimg$*z9ma#&LgfqgMkAW@9PvebitYva',
    STRIPE_WEBHOOK: 'secret',
  },
  STRIPE_PRODUCT_PRICE_IDS: {
    monthly: {
      'price_1HAaE5AFF27SH7gUNjODrtmA': { price: '15', desc: 'For personal usage' },
      'price_1IFoWtAFF27SH7gUXGX1cUEN': { price: '30', desc: 'For work' },
    },
    yearly: {
      'price_1IXZ4jAFF27SH7gU98uP8uoI': { price: '150', desc: 'For personal usage' },
      'price_1IXZ4uAFF27SH7gUonIn6eP2': { price: '300', desc: 'For work' },
    },
  },
  STRIPE_PRODUCT_PRICE_IDS_STUDENT: {
    monthly: {
      'price_1IeNEFAFF27SH7gUjltjrC84': { price: '5', desc: 'For students' },
      'price_1HAaE5AFF27SH7gUNjODrtmA': { price: '15', desc: 'For personal usage' },
      'price_1IFoWtAFF27SH7gUXGX1cUEN': { price: '30', desc: 'For work' },
    },
    yearly: {
      'price_1IXZ4jAFF27SH7gU98uP8uoI': { price: '150', desc: 'For personal usage' },
      'price_1IXZ4uAFF27SH7gUonIn6eP2': { price: '300', desc: 'For work' },
    },
  },
  TELEGRAM_BOT: {
    TOKEN: '1813586981:AAFMdcKD9ghR08Tk8MAa1KnbLwHR2dAEGn8',
    FEEDBACK_CHAT_ID: -1001369904384,
    USER_UPDATES_CHAT_ID: -1001243865773,
  },
  LINEAR: {
    API_KEY: 'lin_api_3n7AX3vqQ0QtyqsmCi2pcjPc8PpH5lTL9f4AeRmq'
  },
  ...require('./_dev.secrets.js')
}
