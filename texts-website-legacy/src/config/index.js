import { IS_DEV } from '../constants'

const config = {
  ...require('./default'),
  ...require(IS_DEV ? './dev' : './prod'),
}

module.exports = config
