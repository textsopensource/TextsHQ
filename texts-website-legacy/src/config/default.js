const REDIS_HOST = '127.0.0.1'
const REDIS_PORT = 6379

const APP_RELEASE_CONFIG = {
  NIGHTLY_MACHINE_IDS: [],
  SKIP_UPDATE_FOR_VERSIONS: [],
  SPECIFIC_RELEASE: {
    // 'deviceID': {
    //   version: '1.33.7',
    //   url: '',
    //   name: '1.33.7',
    //   notes: [],
    //   download_size: '',
    //   pub_date: '',
    // },
  },
  EXPIRE_BUILDS: {
    SPECIFIC_VERSIONS: { // value should be process.platform or *
      '0.35.10': 'darwin', // onboarding -> contacts crashes UI
    },
    LTE_VERSION: '0.35.0',
  },
  RELEASE_BREAKPOINTS: [
    // this is for the code sign cert / designated requirement change
    // https://linear.app/texts/issue/TXT-358/change-designated-requirement
    // versions 0.20.7 and lower don't have the updated DR
    {
      version: '0.20.7',
      feed: {
        version: '0.32.9',
        url: 'https://texts-static.s3-us-west-1.amazonaws.com/builds/Texts-macOS-x64-v0.32.9.zip',
        name: '0.32.9',
        notes: [],
        download_size: '109655512',
        pub_date: '2020-12-27T21:52:53.993Z',
      },
      os: 'macos',
      arch: '*',
    },
  ],
}

module.exports = {
  ORIGIN: 'https://texts.com/',
  DEFAULT_PORT: 11234,
  REDIS_CONNECTION_STRING: `redis://${REDIS_HOST}:${REDIS_PORT}`,
  DEBUG_SECRET: 'REPLACE_WITH_REALLY_REALLY_LONG_AND_RANDOM_SECRET',
  SESSION_COOKIE_MAX_AGE: 180 * 86400 * 1000, // 180 days
  OAUTH: {
    GOOGLE: {
      CLIENT_ID: '631329320742-stvuhuf71ugur7b3hheqbg128hfgh7ie.apps.googleusercontent.com',
      CLIENT_SECRET: 'ICldK5vpSM2OPIO4mcyWUQ1s',
    },
    TWITTER: {
      API_KEY: 'IUYbpEvu8gGNYiygJegUK1bwK',
      API_SECRET_KEY: 'f3dXUH1xHCdksptZdNqSwLMMbC6DoxcAt0FAijubodxScbQoHg',
    },
  },
  SECRETS: {
    SESSION: 'cT%uFp5rpZJ97&Y!cPDKy!F%qTa32imBAimg$*z9ma#&LgfqgMkAW@9PvebitYva',
    STRIPE_WEBHOOK: 'HWwRDzhPyix7JRbixYVYgVEijT9Hk3XH588E7EvV7x5Lo5ducZucqBzVLU2Tkc57duxNiuFdE8WcpGsE78Cy2jJafik3CBnD',
  },
  PAPERTRAIL: {
    HOST: 'logs6.papertrailapp.com',
    PORT: 26328,
  },
  SENTRY: 'https://2a530f028ca24a608a66f66113048998@sentry.texts.com/3',
  TYPEORM: {
    type: 'postgres',
    host: '127.0.0.1',
    port: 5432,
    username: 'postgres',
    password: 'password',
    database: 'jack',
  },
  STRIPE_PRODUCT_PRICE_IDS: {
    monthly: {
      'price_1HEMq2AFF27SH7gUUaxLG4U2': { price: '15', desc: 'For personal usage' },
      'price_1HG7iVAFF27SH7gUVmIDZIQn': { price: '30', desc: 'For work' },
    },
    yearly: {
      'price_1IXYnHAFF27SH7gU3odSYBvV': { price: '150', desc: 'For personal usage' },
      'price_1IXYnxAFF27SH7gU9Y8a7ubO': { price: '300', desc: 'For work' },
    },
  },
  STRIPE_PRODUCT_PRICE_IDS_STUDENT: {
    monthly: {
      'price_1IOWEDAFF27SH7gUv0ZIuDif': { price: '5', desc: 'For students' },
      'price_1HEMq2AFF27SH7gUUaxLG4U2': { price: '15', desc: 'For personal usage' },
      'price_1HG7iVAFF27SH7gUVmIDZIQn': { price: '30', desc: 'For work' },
    },
    yearly: {
      'price_1IXYnHAFF27SH7gU3odSYBvV': { price: '150', desc: 'For personal usage' },
      'price_1IXYnxAFF27SH7gU9Y8a7ubO': { price: '300', desc: 'For work' },
    },
  },
  DISABLE_NO_INVITE_REGISTRATION: true,
  SMTP: {
    host: 'email-smtp.us-west-2.amazonaws.com',
    port: 465,
    secure: true,
    auth: {
      user: 'AKIASHI4K25TBIQB7DN6',
      pass: 'BM40ee0ErI0aLParhtL9NgyfB9Z2ncHITyB0TCdCf6kS',
    },
  },
  EMAIL_FROM: 'Kishan Bagaria <kishanbagaria@texts.com>',
  EMAIL_SIGNATURE: `Kishan Bagaria
Founder & CEO, Texts.com`,
  EMAIL_BCC: 'kishan@texts.com',
  ADMIN_USER_IDS: ['usr_01EAQFBDBZNBDNSJ2QWVHWTTDY'],
  ...APP_RELEASE_CONFIG,

  STRIPE: {
    // PUBLISHABLE_KEY: 'OVERRIDE_THIS',
    // SECRET_KEY: 'OVERRIDE_THIS',
  },
  AWS: {
    // ACCESS_KEY_ID: 'OVERRIDE_THIS',
    // SECRET_ACCESS_KEY: 'OVERRIDE_THIS',
    // REGION: 'OVERRIDE_THIS',
    // BUCKET: 'OVERRIDE_THIS',
  },
  MONGO_CONNECTION_URL: '',
  TWITTER_HANDLE: 'TextsHQ',
  TELEGRAM_BOT: {
    TOKEN: '1703729064:AAG1pyFx4JSv4pJaGeEyISvAFi7ft5Mwhrg',
    FEEDBACK_CHAT_ID: -1001369904384,
    USER_UPDATES_CHAT_ID: -1001243865773,
  },
  LINEAR: {
    API_KEY: 'lin_api_3n7AX3vqQ0QtyqsmCi2pcjPc8PpH5lTL9f4AeRmq'
  },
  SUPERHUMAN_AUTH_TOKEN: '',
  // JUNE_API_KEY: 'CacZiaqShYEb092k', // texts
  JUNE_API_KEY: '6MKsUlVaZMK7Mxkr', // texts dev
}
