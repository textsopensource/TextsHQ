import qs from 'qs'
import { promises as fs } from 'fs'
import path from 'path'
import slugify from 'slugify'
import bluebird from 'bluebird'
import { groupBy, range } from 'lodash'
import type { Connection } from 'typeorm'
import type { FastifyInstance, FastifyReply } from 'fastify'

import { getRepos } from './orm'
import { createAppLoginToken } from './tokens'
import { IS_DEV, STATIC_DIR_PATH, CHANGELOG_LINK, JOBS_SUBMISSION_FILE_PATH } from './constants'
import stripe from './stripe'
import Invite from './entity/Invite'
import { replyWithReact, serveFlashMessage } from './react'
import langEn from './lang-en'

import OnWaitlist from './components/OnWaitlist'
import Invites from './components/Invites'
import AppLogin from './components/AppLogin'
import Jobs from './components/Jobs'
import InstallMacOS from './components/InstallMacOS'
import InstallWindows from './components/InstallWindows'
import InstallLinux from './components/InstallLinux'
import Privacy from './components/Privacy'
import FAQ from './components/FAQ'
import Subscribed from './components/Subscribed'
import Subscription from './components/Subscription'
import PaymentSuccess from './components/PaymentSuccess'
import NotActivated from './components/NotActivated'
import SwiftRuntimeSupport from './components/SwiftRuntimeSupport'
import { getIP, getIPCountry } from './util'
import sendEmail from './emails'
import allEmails from './all-emails'

const { ORIGIN, STRIPE, STRIPE_PRODUCT_PRICE_IDS, STRIPE_PRODUCT_PRICE_IDS_STUDENT } = require('./config')

const DEFAULT_UNAUTH_REDIRECT = '/auth/google'

const createCheckoutSession = (customer: string, price: string, cancelURLPathname: string) =>
  stripe.checkout.sessions.create({
    line_items: [{
      price,
      quantity: 1,
    }],
    customer,
    payment_method_types: ['card'],
    mode: 'subscription',
    success_url: `${ORIGIN}payment-success`,
    cancel_url: `${ORIGIN}${cancelURLPathname}`,
  })

async function serveStatic(fileName: string, reply: FastifyReply) {
  const txt = await fs.readFile(path.join(STATIC_DIR_PATH, `html/${fileName}.html`), 'utf-8')
  reply.type('text/html').send(txt)
}

export default function registerHTMLRoutes(app: FastifyInstance, conn: Connection) {
  const { userRepo, inviteRepo, waitlistRepo } = getRepos(conn)

  app.get('/privacy.html', (request, reply) => {
    reply.redirect('/privacy')
  })

  app.get('/privacy-policy', (request, reply) => {
    reply.redirect('/privacy')
  })

  app.get('/privacypolicy', (request, reply) => {
    reply.redirect('/privacy')
  })

  app.get('/terms', (request, reply) => {
    reply.redirect('/privacy')
  })

  app.get('/tos', (request, reply) => {
    reply.redirect('/privacy')
  })

  app.get('/privacy-form', (request, reply) => {
    // TODO
    reply.redirect('/privacy')
  })

  app.get('/install-macos/:anything', (request, reply) => {
    reply.redirect('/install/macos')
  })

  app.get('/install-mac', (request, reply) => {
    reply.redirect('/install/macos')
  })

  app.get('/swift-cli-runtime', (request, reply) => {
    replyWithReact(reply, SwiftRuntimeSupport)
  })

  app.get('/privacy', (request, reply) => {
    replyWithReact(reply, Privacy)
  })

  app.get('/faq', (request, reply) => {
    replyWithReact(reply, FAQ)
  })

  app.get('/payment-success', (request, reply) => {
    replyWithReact(reply, PaymentSuccess)
  })

  app.post('/waitlist-signup', async (request, reply) => {
    const { email: _email } = request.body as any
    const email = _email.trim().toLowerCase()
    const waitlistEntry = await waitlistRepo.findOne({ email })
    if (waitlistEntry) {
      waitlistEntry.checkCount = (waitlistEntry.checkCount || 0) + 1
      await waitlistRepo.save(waitlistEntry)
      replyWithReact(reply, OnWaitlist, { message: langEn.ALREADY_ON_WAITLIST })
    } else {
      const newWaitlistRecord = waitlistRepo.create({
        email,
        os: request.headers['user-agent'],
        ip: getIP(request),
        country: getIPCountry(request),
      })
      await waitlistRepo.save(newWaitlistRecord)
      replyWithReact(reply, OnWaitlist, { message: langEn.ADDED_TO_WAITLIST })
      sendEmail(allEmails.onWaitlist(email, null)).catch(console.error)
    }
  })

  app.post('/jobs-submission', async (request, reply) => {
    const { url } = request.body as any
    if (url?.length > 2048) return reply.status(400).send()
    console.log('jobs submission', url)
    sendEmail(allEmails.jobsSubmission({ link: url, headers: JSON.stringify(request.headers, null, '\t') }))
    fs.appendFile(JOBS_SUBMISSION_FILE_PATH, JSON.stringify([new Date(), url]) + '\n')
    serveFlashMessage('Thanks for sending in your link! We receive a high number of inbound so we may not be able to respond to each submission manually.', reply)
  })

  app.get('/changelog', (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    reply.redirect(CHANGELOG_LINK)
  })

  app.get('/customer-portal', async (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    const session = await stripe.billingPortal.sessions.create(
      {
        customer: user.stripeCustomerID,
        return_url: 'https://texts.com/',
      },
    )
    reply.redirect(session.url)
  })

  app.get('/app-login', async (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    const token = await createAppLoginToken(user.id)
    const loginURL = 'texts://auth?' + qs.stringify({ token })
    replyWithReact(reply, AppLogin, { username: user.firstName, loginURL })
  })

  app.get('/pricing', (request, reply) => {
    reply.redirect('/subscription')
  })

  app.get('/subscription', async (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    if (!user.stripeCustomerID) {
      await user.createStripeCustomer()
      await userRepo.save(user)
    }
    if (user.subscription) {
      replyWithReact(reply, Subscribed)
    }
    const priceIDs = user.studentUntil > new Date() ? STRIPE_PRODUCT_PRICE_IDS_STUDENT : STRIPE_PRODUCT_PRICE_IDS
    const yearly = (request.query as any).yearly === ''
    const pricesMap = yearly ? priceIDs.yearly : priceIDs.monthly
    const cancelPathname = request.url.slice(1) // slice(1) removes the leading slash
    const checkoutSessions = await Promise.all(
      Object.keys(pricesMap).map(priceID =>
        createCheckoutSession(user.stripeCustomerID, priceID, cancelPathname)),
    )
    replyWithReact(reply, Subscription, {
      stripePublishableKey: STRIPE.PUBLISHABLE_KEY,
      checkoutSessionIDs: checkoutSessions.map(s => s.id),
      yearly,
      pricesMap,
    })
  })

  app.get('/send-invite', async (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)

    const createdInvites = await inviteRepo.find({ createdByUser: uid })
    const invitedUserIDs = createdInvites.map(i => i.usedByUser).filter(Boolean)
    const allInvitedUsers = await userRepo.findByIds(invitedUserIDs)
    const groupedInvitedUsers = groupBy(allInvitedUsers, 'id')

    const invites = createdInvites.map(i => ({
      inviteID: i.id,
      used: !!i.usedByUser,
      usedBy: groupedInvitedUsers[i.usedByUser]?.[0]?.fullName,
    })).reverse()

    if (user.availableInvites > 0) {
      await bluebird.map(range(user.availableInvites), _ => {
        const invite = new Invite()
        invite.assignRandomID()
        invite.createdByUser = uid
        invites.push({
          inviteID: invite.id,
          used: false,
          usedBy: null,
        })
        return inviteRepo.save(invite)
      })
      user.availableInvites = 0
      userRepo.save(user)
    }
    replyWithReact(reply, Invites, { invites })
  })

  app.get('/invite/:inviteID', async (request, reply) => {
    const { inviteID } = request.params as any
    const { uid } = request.session
    const redirect = '/install'
    if (uid) {
      reply.redirect(redirect)
      return
    }
    request.session.inviteID = inviteID
    request.session.redirectTo = redirect
    reply.redirect(DEFAULT_UNAUTH_REDIRECT)
  })

  app.get('/invite', (request, reply) => {
    serveStatic('invite', reply)
  })

  app.get('/jobs', (request, reply) => {
    replyWithReact(reply, Jobs)
  })

  app.get('/careers', (request, reply) => {
    reply.redirect('/jobs')
  })

  app.get('/hiring', (request, reply) => {
    reply.redirect('/jobs')
  })

  app.get('/install/macos', async (request, reply) => {
    const { uid } = request.session
    const { channel } = request.query as any
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    if (!user.isActivated) {
      replyWithReact(reply, NotActivated)
      return
    }
    const token = await createAppLoginToken(user.id)
    const slug = slugify(user.firstName, { lower: true, strict: true })
    const query = channel ? `?channel=${channel}` : ''
    const installScriptURL = `${ORIGIN}i/${token}/${slug}.sh` + (query ? '\\' + query : '')
    replyWithReact(reply, InstallMacOS, { installScriptURL, query })
  })

  app.get('/install/windows', async (request, reply) => {
    const { uid } = request.session
    const { channel } = request.query as any
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    if (!user.isActivated) {
      replyWithReact(reply, NotActivated)
      return
    }
    const query = channel ? `?channel=${channel}` : ''
    replyWithReact(reply, InstallWindows, { query })
  })

  app.get('/install/linux', async (request, reply) => {
    const { uid } = request.session
    const { channel } = request.query as any
    if (!uid) {
      request.session.redirectTo = request.raw.url
      reply.redirect(DEFAULT_UNAUTH_REDIRECT)
      return
    }
    const user = await userRepo.findOneOrFail(uid)
    if (!user.isActivated) {
      replyWithReact(reply, NotActivated)
      return
    }
    const query = channel ? `?channel=${channel}` : ''
    replyWithReact(reply, InstallLinux, { query })
  })

  if (IS_DEV) {
    app.get('/not-activated.html', (request, reply) => {
      replyWithReact(reply, NotActivated)
    })
  }
}
