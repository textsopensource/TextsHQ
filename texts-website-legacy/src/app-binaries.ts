import fse from 'fs-extra'
import path from 'path'
import semver from 'semver'

import { STATIC_DIR_PATH } from './constants'

const { NIGHTLY_MACHINE_IDS, SKIP_UPDATE_FOR_VERSIONS, SPECIFIC_RELEASE, RELEASE_BREAKPOINTS } = require('./config')

export type Channel = 'stable' | 'beta' | 'nightly'

export type OS = 'macos' | 'windows' | 'linux'

export type Arch = 'x64' | 'arm64'

export const getUpdateFeedJSON = (os: OS, channel: Channel = 'stable', arch: Arch = 'x64') =>
  fse.readJSON(path.join(STATIC_DIR_PATH, `update-feed/${os}-${arch}-${channel}.json`))

type UpdateFeedOptions = {
  os: OS
  channel: Channel
  arch: Arch
  deviceID: string
  installedVersion?: string
}

export async function getUpdateFeed({ os, channel, arch, deviceID, installedVersion }: UpdateFeedOptions) {
  let selectedChannel: Channel = 'stable'
  if (['nightly', 'beta'].includes(channel)) selectedChannel = channel
  if (NIGHTLY_MACHINE_IDS.includes(deviceID)) selectedChannel = 'nightly'

  if (SPECIFIC_RELEASE) {
    const specificReleaseForDevice = SPECIFIC_RELEASE[deviceID]
    if (specificReleaseForDevice) {
      if (specificReleaseForDevice.version === installedVersion) return
      return specificReleaseForDevice
    }
  }
  if (SKIP_UPDATE_FOR_VERSIONS?.includes(installedVersion)) return

  if (RELEASE_BREAKPOINTS && installedVersion) {
    for (const breakpoint of RELEASE_BREAKPOINTS) {
      if (
        os === breakpoint.os
        && ['*', arch].includes(breakpoint.arch)
        && semver.lt(installedVersion, breakpoint.version)
      ) {
        return breakpoint.feed
      }
    }
  }

  const updateFeed = await getUpdateFeedJSON(os, selectedChannel, arch === 'arm64' ? 'arm64' : 'x64')
  if (installedVersion && updateFeed.version === installedVersion) return
  return updateFeed
}

export async function getUpdateFeedYML(updateFeedOptions: UpdateFeedOptions) {
  const updateFeed = await getUpdateFeed(updateFeedOptions)
  if (!updateFeed) return `version: ${updateFeedOptions.installedVersion}`
  return `version: ${updateFeed.version}
files:
  - url: ${updateFeed.url}
    size: ${updateFeed.download_size}
    sha512: ${updateFeed.sha512}
path: ${updateFeed.url}
sha512: ${updateFeed.sha512}
releaseDate: '${updateFeed.pub_date}'`
}
