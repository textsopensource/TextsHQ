import path from 'path'
import { promises as fs } from 'fs'

import { STATIC_DIR_PATH } from './constants'

const getChangelogText = () => fs.readFile(path.join(STATIC_DIR_PATH, 'changelog.md'), 'utf-8')

const parseChangelog = (changelogMd: string, currentAppVersion: string) =>
  changelogMd.split('\n# ').filter(Boolean).slice(0, 50).map(entry => {
    if (!entry.includes('—')) {
      const lines = entry.split('\n')
      return {
        id: lines[0],
        attachments: [] as any[],
        reactions: [] as any[],
        timestamp: new Date(),
        senderID: '$thread',
        isSender: false,
        text: lines.slice(2),
      }
    }
    const [header, ...rest] = entry.split('\n\n')
    const [date, version] = header.split(' — ') // em dash
    const heading = version + (version === `v${currentAppVersion}` ? " — You're on this version!" : '')
    const m = {
      id: version,
      attachments: [] as any[],
      reactions: [] as any[],
      timestamp: new Date(date),
      senderID: '$thread',
      isSender: false,
      text: heading + '\n\n',
      textAttributes: {
        entities: [
          {
            from: 0,
            to: version.length,
            bold: true,
          },
        ],
      },
    }
    const items = rest.map(lines =>
      lines.split('\n').map(line => {
        const image = line.startsWith('!')
        if (image) {
          const link = /^!\[\]\((.+?)\)$/.exec(line)?.[1]
          m.attachments.push({
            id: link,
            type: 'img',
            srcURL: link,
          })
        }
        return image ? '' : line
      }).join('\n'))
    m.text += items.join('\n\n').trim()
    return m
  }).reverse()

export async function getChangelogEntries(currentAppVersion: string) {
  const changelogMd = await getChangelogText()
  const entries = parseChangelog(changelogMd, currentAppVersion)
  return entries
}
