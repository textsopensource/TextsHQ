import nodemailer from 'nodemailer'
import type Mail from 'nodemailer/lib/mailer'

import { IS_DEV } from './constants'

const { EMAIL_FROM, SMTP } = require('./config')

const transporter = nodemailer.createTransport(SMTP)

const MAIL_DEFAULTS: Mail.Options = {
  from: EMAIL_FROM,
}

export default async function sendEmail(_mail: Mail.Options) {
  const mail = { ...MAIL_DEFAULTS, ..._mail }
  if (IS_DEV) {
    console.log('sending mail', mail)
    return
  }
  await transporter.sendMail(mail)
}
