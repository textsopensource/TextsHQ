import type { FastifyInstance } from 'fastify'
import type { Connection } from 'typeorm'

import { STATIC_DIR_PATH } from './constants'
import allEmails from './all-emails'
import sendEmail from './emails'
import { getRepos } from './orm'
import HomePage from './components/HomePage'
import { replyWithReact } from './react'
import compileHomePage from './scripts/compile-home'

export default function registerDevRoutes(app: FastifyInstance, conn: Connection) {
  const { userRepo } = getRepos(conn)

  // eslint-disable-next-line global-require, import/no-extraneous-dependencies
  const fastifyStatic = require('fastify-static')
  app.register(fastifyStatic, {
    root: STATIC_DIR_PATH,
    prefix: '/',
  })

  app.get('/dev/debug', (request, reply) => {
    reply.send(request.session)
  })

  app.get('/dev/mail/welcome', async (request, reply) => {
    const { uid } = request.session
    if (!uid) {
      reply.code(401).send()
      return
    }
    const user = await userRepo.findOne(uid)
    await sendEmail(allEmails.welcome(user))
    reply.send()
  })

  app.get('/', (request, reply) => {
    compileHomePage()
    replyWithReact(reply, HomePage)
  })
}
