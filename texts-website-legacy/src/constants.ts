import path from 'path'

export const IS_DEV = process.env.NODE_ENV === 'development'

export const SITE_ROOT = path.join(__dirname, '../..')

export const STATIC_DIR_PATH = IS_DEV
  ? path.join(SITE_ROOT, 'backend-static')
  : path.join(SITE_ROOT, '../static')

export const LOG_FILE_PATH = path.join(SITE_ROOT, 'analytics.log')

export const JOBS_SUBMISSION_FILE_PATH = path.join(SITE_ROOT, 'jobs.log')

export const CHANGELOG_LINK = 'https://www.notion.so/Texts-Changelog-1c9190fe057a41a6bfa7443014b3a3ed'
