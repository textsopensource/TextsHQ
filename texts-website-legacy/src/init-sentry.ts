import * as Sentry from '@sentry/node'

import { IS_DEV } from './constants'

const { SENTRY } = require('./config')

Sentry.init({
  enabled: !IS_DEV,
  dsn: SENTRY,
  beforeSend(event, eventHint) {
    console.log(event.exception, eventHint.originalException)
    return event
  },
})

export default Sentry
