import type Mail from 'nodemailer/lib/mailer'
import type User from './entity/User'

const { EMAIL_SIGNATURE, EMAIL_BCC } = require('./config')

const adminEmails = {
  welcomeAdminOnly: (user: User): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts sign up',
    text: `${user.fullName} ${user.emails[0]} has signed up`,
  }),
  userUsedAfterDays: (user: User, days: number): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts user used app after days',
    text: `${user.fullName} ${user.emails[0]} (since ${user.createdAt.toDateString()}) has used app after ${days} days`,
  }),
  userHasNotUsedInDays: (user: User, days: number): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts user has not used app in days',
    text: `${user.fullName} ${user.emails[0]} (since ${user.createdAt.toDateString()}) has not used app for ${days} days`,
  }),
  userBuildExpired: (user: User, details: string): Mail.Options => ({
    to: EMAIL_BCC,
    subject: "Texts user's build was expired",
    text: user ? `${user.fullName} ${user.emails[0]} (since ${user.createdAt.toDateString()}) – ${details}` : `unknown user – ${details}`,
  }),
  userSentFeedback: (user: User, text: string): Mail.Options => ({
    to: EMAIL_BCC,
    replyTo: `${user.fullName} <${user.emails[0]}>`,
    subject: `Texts feedback from ${user.fullName}`,
    text,
  }),
  jobsSubmission: ({ link, headers }: { link: string, headers: string }): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts link submission',
    text: `${link}\n\n${headers}`,
  }),
}

const allEmails = {
  ...adminEmails,
  welcome: (user: User, usesSuperhuman = false): Mail.Options => ({
    to: `${user.fullName} <${user.emails[0]}>`,
    bcc: EMAIL_BCC,
    subject: 'Texts',
    text: `Hey ${user.firstName}, excited for you to use Texts and become more productive and focused.
${usesSuperhuman ? "\nI see you're on Superhuman, so you'll be familiar with ⌘ K and getting to inbox zero. Hotkeys in Texts are prefixed with ⌘ and Ctrl.\n" : ''}
PS: We love hearing from users, hit me up with feedback or questions anytime.

${EMAIL_SIGNATURE}`,
  }),
  onWaitlist: (email: string, firstName: string): Mail.Options => ({
    to: `${firstName || ''} <${email}>`,
    from: 'Kishan Bagaria <kishanbagaria+waitlist@texts.com>',
    subject: 'Texts',
    text: `Hey${firstName ? ` ${firstName}` : ''}, thanks for signing up on the Texts.com waitlist! We're addressing feedback from beta users atm to ensure a smooth experience for you.

If you'd like to skip ahead and use the beta desktop app, you can get an invite from a friend who's already using it.

${EMAIL_SIGNATURE}`,
  }),
}

export default allEmails
