import { ulid } from 'ulid'
import {
  Entity,
  PrimaryColumn,
  Index,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeUpdate,
  BeforeInsert,
  AfterInsert,
} from 'typeorm'
import type Stripe from 'stripe'

import { pick } from 'lodash'
import stripe from '../stripe'
import { juneUploader } from '../analytics/june'

@Entity()
export default class User {
  @PrimaryColumn()
  id: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Index() @Column('text', { array: true })
  emails: string[]

  @Column({ nullable: true })
  firstName: string

  @Column({ nullable: true })
  middleName: string

  @Column({ nullable: true })
  lastName: string

  @Column({ nullable: true })
  lastActive: Date

  /** used for student pricing */
  @Column({ nullable: true })
  studentUntil: Date

  @Column({ default: 0 })
  sessionValidateCount: number

  @Column({ default: 0 })
  availableInvites: number

  @Column({ nullable: true })
  twitterAuthID: string

  @Column('jsonb', { nullable: true })
  twitterInfo: any

  @Column({ nullable: true })
  googleAuthID: string

  @Column('jsonb', { nullable: true })
  googleInfo: any

  @Column({ nullable: true })
  appleAuthID: string

  @Column('jsonb', { nullable: true })
  appleInfo: any

  @Index() @Column({ nullable: true })
  stripeCustomerID: string

  @Column('jsonb', { nullable: true })
  metadata: any

  @Column('text', { default: '{}', array: true })
  flags: string[]

  @Column({ default: true })
  isActivated: boolean

  @Column({ nullable: true })
  invitedByUser: string

  @Column('jsonb', { nullable: true })
  subscription: Stripe.Subscription

  get fullName() {
    return [this.firstName, this.middleName, this.lastName].filter(Boolean).join(' ')
  }

  assignRandomID() {
    this.id = 'usr_' + ulid()
  }

  @BeforeUpdate() @BeforeInsert()
  async createStripeCustomer() {
    if (this.stripeCustomerID) return
    const customer = await stripe.customers.create({
      email: this.emails[0],
      name: this.fullName,
      metadata: {
        uid: this.id,
      },
    }, { idempotencyKey: this.id })
    this.stripeCustomerID = customer.id
  }

  async syncStripeSubStatus() {
    const subs = await stripe.subscriptions.list({ customer: this.stripeCustomerID })
    const [firstSub] = subs.data
    this.subscription = firstSub || null
    return subs
  }

  get paymentStatus() {
    if (!this.subscription) return 'free'
    if (this.subscription.status === 'trialing') return 'trial'
    return 'premium'
  }

  @AfterInsert()
  async uploadToJune() {
    juneUploader.uploadUser({
      ...pick(this, ['id', 'emails', 'firstName', 'lastName']),
      createdAt: this.createdAt?.toISOString(),
      updatedAt: this.updatedAt?.toISOString(),
      lastActive: this.lastActive?.toISOString(),
    })
  }
}
