import { ulid } from 'ulid'
import { Entity, PrimaryColumn, Column, CreateDateColumn, UpdateDateColumn, Index } from 'typeorm'

@Entity()
export default class Invite {
  @PrimaryColumn()
  id: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Index() @Column()
  createdByUser: string

  @Column({ nullable: true })
  usedByUser: string

  assignRandomID() {
    this.id = 'ivt_' + ulid()
  }
}
