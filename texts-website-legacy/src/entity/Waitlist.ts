import { Entity, PrimaryColumn, CreateDateColumn, UpdateDateColumn, Column } from 'typeorm'

@Entity()
export default class Waitlist {
  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Column({ nullable: true })
  firstName: string

  @Column({ nullable: true })
  middleName: string

  @Column({ nullable: true })
  lastName: string

  @PrimaryColumn()
  email: string

  @Column({ nullable: true })
  os: string

  @Column({ nullable: true })
  ip: string

  @Column({ nullable: true })
  country: string

  @Column({ default: 0, nullable: true })
  checkCount: number
}
