import { Entity, PrimaryColumn, Column, CreateDateColumn, UpdateDateColumn } from 'typeorm'

@Entity()
export default class AppSession {
  @PrimaryColumn()
  id: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Column()
  uid: string

  @Column()
  deviceID: string

  @Column()
  ip: string

  @Column()
  appVersion: string

  @Column({ nullable: true })
  os: string

  static create(sid: string, uid: string, ip: string, appVersion: string, deviceID: string, os: string) {
    const sess = new AppSession()
    sess.id = sid
    sess.uid = uid
    sess.ip = ip || '127.0.0.1'
    sess.appVersion = appVersion
    sess.deviceID = deviceID
    sess.os = os
    return sess
  }
}
