import { promises as fs, readFileSync } from 'fs'
import path from 'path'
import fastify from 'fastify'
import fastifySession from 'fastify-session'
import fastifyCookie from 'fastify-cookie'
import fastifyFormBody from 'fastify-formbody'
import ConnectRedis from 'connect-redis'
import mongoose from 'mongoose'
import multipart from 'fastify-multipart'

import './winston'
import Sentry from './init-sentry'
import registerGrantRoutes from './grant'
import redis from './redis'
import registerRoutes from './routes'
import { createORMConn } from './orm'
import { SITE_ROOT, IS_DEV, STATIC_DIR_PATH } from './constants'
import { getIP } from './util'
import registerDevRoutes from './dev'

const RedisStore = ConnectRedis(fastifySession as any)
const { ORIGIN, DEFAULT_PORT, SECRETS, OAUTH, SESSION_COOKIE_MAX_AGE, MONGO_URL } = require('./config')

const app = fastify({
  logger: true,
  trustProxy: true,
  bodyLimit: 50 * 1024 * 1024, // 50mb should be plenty for feedback attachment
  ...(IS_DEV ? {
    https: {
      key: readFileSync(path.join(SITE_ROOT, '_/ssl/key.pem')),
      cert: readFileSync(path.join(SITE_ROOT, '_/ssl/cert.pem')),
    },
    // http2: true,
  } : {}),
})

app.register(multipart)
app.register(fastifyFormBody)
app.register(fastifyCookie)
app.register(fastifySession, {
  secret: SECRETS.SESSION,
  cookieName: 'sid',
  saveUninitialized: false,
  cookie: {
    maxAge: SESSION_COOKIE_MAX_AGE,
  },
  store: new RedisStore({ client: redis }),
})
app.decorateReply('locals', { grant: null })

app.setErrorHandler((err, request, reply) => {
  Sentry.withScope(scope => {
    scope.setUser({
      ip_address: getIP(request),
    })
    scope.setTag('path', request.raw.url)
    Sentry.captureException(err)
    console.error(err)
    reply.send({
      error: "Something went wrong! We've been notified.",
    })
  })
})

registerGrantRoutes(app, {
  defaults: {
    prefix: 'auth',
    origin: ORIGIN,
    transport: 'session',
    state: true,
    response: ['tokens', 'profile'],
    callback: '/auth/done',
  },
  google: {
    key: OAUTH.GOOGLE.CLIENT_ID,
    secret: OAUTH.GOOGLE.CLIENT_SECRET,
    scope: ['openid', 'profile', 'email'],
  },
  twitter: {
    key: OAUTH.TWITTER.API_KEY,
    secret: OAUTH.TWITTER.API_SECRET_KEY,
    scope: ['openid', 'email'],
  },
})

app.setNotFoundHandler(async (req, reply) => {
  reply.type('text/html').send(await fs.readFile(path.join(STATIC_DIR_PATH, 'index.html'), 'utf-8'))
})

async function start() {
  const conn = await createORMConn()
  mongoose.connect(MONGO_URL, { useNewUrlParser: true }).catch(console.error)
  registerRoutes(app, conn)
  if (IS_DEV) registerDevRoutes(app, conn)
  const { PORT = DEFAULT_PORT } = process.env
  app.listen(+PORT)
}
start().catch(console.error)
