import React from 'react'
import ReactDOMServer from 'react-dom/server'
import type { FastifyReply } from 'fastify'

import Flash from './components/Flash'

export function renderReactToStaticMarkup(element: JSX.Element | ((props: any) => JSX.Element), props?: any, includeDocType = false) {
  const node = typeof element === 'function' ? React.createElement(element, props) : element
  return (includeDocType ? '<!DOCTYPE html>' : '') + ReactDOMServer.renderToStaticMarkup(node)
}

export function replyWithReact(reply: FastifyReply, element: JSX.Element | ((props: any) => JSX.Element), props?: any) {
  const rendered = renderReactToStaticMarkup(element, props, true)
  reply.type('text/html').send(rendered)
}

export function serveFlashMessage(message: string, reply: FastifyReply) {
  replyWithReact(reply, Flash, { text: message })
}
