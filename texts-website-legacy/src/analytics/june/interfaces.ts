export interface JuneUser {
  id: string,
  createdAt: string,
  updatedAt: string,
  emails?: string[],
  firstName?: string,
  lastName?: string,
  lastActive?: string,
  googleInfo?: Record<string, any>
}

export interface JuneEvent {
  id: string,
  uid: string,
  createdAt: string,
  type: string,
  body: {
    deviceId?: string
  },
  ip: string,
}
