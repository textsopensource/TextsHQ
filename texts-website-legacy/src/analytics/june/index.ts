import got, { Got } from 'got'
import type { JuneEvent, JuneUser } from './interfaces'

const { JUNE_API_KEY } = require('../../config')

const juneEndpoints = {
  identify: 'https://api.june.so/api/identify',
  track: 'https://api.june.so/api/track',
}

export class JuneUploader {
  private readonly http: Got

  constructor(apiKey: string) {
    this.http = got.extend({
      responseType: 'json',
      resolveBodyOnly: true,
      headers: {
        Authorization: `Basic ${apiKey}`,
      },
    })
  }

  uploadEvent(event: JuneEvent) {
    return this.http.post(juneEndpoints.track, { json: {
      type: 'track',
      userId: event.uid,
      event: event.type,
      timestamp: event.createdAt,
      properties: { body: event.body, ipAddress: event.ip },
    } })
  }

  uploadUser(user: JuneUser) {
    return this.http.post(juneEndpoints.identify, { json: {
      type: 'identify',
      userId: user.id,
      traits: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.emails[0],
        emails: user.emails,
        createdAt: user.createdAt,
        lastActive: user.lastActive,
        avatar: user.googleInfo?.profile.picture,
      },
    } })
  }
}

export const juneUploader = new JuneUploader(JUNE_API_KEY)
