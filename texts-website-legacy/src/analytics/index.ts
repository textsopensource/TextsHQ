import semver from 'semver'
import { memoize } from 'lodash'

import { Event, Device } from './db'
import { juneUploader } from './june'

const deviceExists = memoize(deviceId => Device.findOne({ deviceId }))

export default async function logEvent(body: { type: string, deviceId?: string, metadata?: any, eventData?: any }, ip: string, getUserFromDeviceID: Function) {
  if (!body) return

  const version = body.metadata?.appVersion || '0.0.0'

  // Drop events below version when new format was introduced
  if (!semver.satisfies(version, '>0.34.1')) {
    console.log('Dropping event', `"${body.type}"`, 'due to app version', version)
    return { rejected: true }
  }

  const event = new Event(body)

  const { deviceId } = body
  if (deviceId) {
    // If this is a first event we ever see for a given device, create one in mongo
    const existingDevice = await deviceExists(deviceId)

    if (!existingDevice) {
      const device = new Device({ deviceId })
      await device.save()
    }
  }

  await event.save()

  if (body.type === 'timing' && ['First thread from page boot', 'First render from page boot'].includes(body.eventData.value)) {
    const user = await getUserFromDeviceID(deviceId)

    await juneUploader.uploadEvent({
      body,
      createdAt: new Date().toISOString(),
      ip,
      type: body.type,
      uid: user?.id,
      id: event.id,
    })
  }

  return { ok: true }
}
