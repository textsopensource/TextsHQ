import mongoose from 'mongoose'

const DeviceSchema = new mongoose.Schema({
  id: String,
}, { strict: false, timestamps: true })

const EventSchema = new mongoose.Schema({
  user: String,
  metadata: {},
}, { strict: false, timestamps: true })

export const Device = mongoose.model('Device', DeviceSchema)
export const Event = mongoose.model('Event', EventSchema)
