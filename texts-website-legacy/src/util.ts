import type { FastifyRequest } from 'fastify'

export const getIP = (request: FastifyRequest) =>
  request.headers['cf-connecting-ip'] as string

export const getIPCountry = (request: FastifyRequest) =>
  request.headers['cf-ipcountry'] as string
