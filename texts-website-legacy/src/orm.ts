import 'reflect-metadata'
import { createConnection, Connection } from 'typeorm'

import User from './entity/User'
import Event from './entity/Event'
import AppSession from './entity/AppSession'
import Invite from './entity/Invite'
import Waitlist from './entity/Waitlist'

import { IS_DEV } from './constants'

const { TYPEORM } = require('./config')

export const createORMConn = () =>
  createConnection({
    ...TYPEORM,
    entities: [
      User,
      Event,
      Invite,
      AppSession,
      Waitlist,
    ],
    synchronize: true,
    logging: IS_DEV,
  })

export const getRepos = (conn: Connection) => ({
  userRepo: conn.getRepository(User),
  eventRepo: conn.getRepository(Event),
  inviteRepo: conn.getRepository(Invite),
  sessionRepo: conn.getRepository(AppSession),
  waitlistRepo: conn.getRepository(Waitlist),
})
