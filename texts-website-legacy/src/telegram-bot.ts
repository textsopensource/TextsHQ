import TelegramBot from 'node-telegram-bot-api'
import type User from './entity/User'

const { TELEGRAM_BOT } = require('./config')

const bot = new TelegramBot(TELEGRAM_BOT.TOKEN, { polling: true })

const send = (text: string) => {
  console.log({ text })
  return bot.sendMessage(TELEGRAM_BOT.USER_UPDATES_CHAT_ID, text)
}

export const userUsedAppAfterDays = (user: User, days: number, appVersion: string, os: string) => {
  const text = `${user.fullName} ${user.emails[0]} (since ${user.createdAt.toDateString()}) has used app v${appVersion} on ${os} after ${days} days`
  return send(text)
}
export const userPaymentStatusChanged = (user: User, previous: string, now: string) => {
  const text = `${user.fullName} ${user.emails[0]} (since ${user.createdAt.toDateString()}) payment status changed from ${previous} -> ${now}`
  return send(text)
}

export default bot
