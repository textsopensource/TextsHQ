import Redis from 'ioredis'

const { REDIS_CONNECTION_STRING } = require('./config')

const redis = new Redis(REDIS_CONNECTION_STRING)

export default redis
