#!/usr/bin/env sh
git config --global url."https://api:$GH_TOKEN@github.com/".insteadOf "https://github.com/"
git config --global url."https://ssh:$GH_TOKEN@github.com/".insteadOf "ssh://git@github.com/"
git config --global url."https://git:$GH_TOKEN@github.com/".insteadOf "git@github.com:"
echo "//npm.pkg.github.com/:_authToken=$GH_TOKEN" > ~/.npmrc
