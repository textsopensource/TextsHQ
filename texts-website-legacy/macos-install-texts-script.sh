#!/usr/bin/env zsh

echo
echo '[*] Texts will launch when it finishes installing'
echo '[*] Downloading Texts...'
echo

rm /tmp/Texts.zip > /dev/null 2>&1

if uname -m | fgrep -q arm64 || sysctl sysctl.proc_translated 2>/dev/null | fgrep -q 1; then
  downloadURL={{DOWNLOAD_URL_ARM64}}
else
  downloadURL={{DOWNLOAD_URL_X64}}
fi

n=0
until [ "$n" -ge 5 ]; do
  curl -C - --retry 5 -Lo /tmp/Texts.zip $downloadURL && break
  n=$((n+1))
  sleep 15
done
if [[ ! -a /tmp/Texts.zip ]]; then
  echo '[*] Failed to download Texts'
  exit
fi
echo '[*] Installing Texts...'
killall Texts > /dev/null 2>&1
rm -rf /Applications/Texts.app
ditto -xk /tmp/Texts.zip /tmp/
mv /tmp/Texts.app /Applications/
open /Applications/Texts.app --args --auth-token={{AUTH_TOKEN}} || open /Applications/Texts.app --args --auth-token={{AUTH_TOKEN}}
echo
echo '[*] Texts has been installed'
echo
