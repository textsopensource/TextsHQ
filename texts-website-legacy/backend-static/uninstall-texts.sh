#!/usr/bin/env sh

echo
echo '[*] Uninstalling Texts...'
killall Texts > /dev/null 2>&1
rm -rf ~/"Library/Application Support/jack"
rm -rf /Applications/Texts.app
echo '[*] Uninstalled Texts'
echo
