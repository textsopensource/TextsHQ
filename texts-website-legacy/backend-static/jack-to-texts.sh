#!/usr/bin/env sh

echo
echo '[*] Texts will launch automatically after installation'
echo

curl -Lo /tmp/Texts.zip https://texts-static.s3.fr-par.scw.cloud/builds/Texts-mac.zip

if [[ ! -a /tmp/Texts.zip ]]; then
  echo '[*] Failed to download Texts'
  exit
fi

ditto -xk /tmp/Texts.zip /tmp/

killall Jack > /dev/null 2>&1
killall Texts > /dev/null 2>&1

rm -rf /Applications/Jack.app
rm -rf /Applications/Texts.app
rm -rf /tmp/Jack.app
rm /tmp/Jack.zip > /dev/null 2>&1

mv /tmp/Texts.app /Applications/
xattr -rd com.apple.quarantine /Applications/Texts.app
open /Applications/Texts.app

echo
echo '[*] Texts has been installed'
echo
