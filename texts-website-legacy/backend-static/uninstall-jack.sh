#!/usr/bin/env sh

echo
echo '[*] Uninstalling Jack...'
killall Jack > /dev/null 2>&1
rm -rf ~/"Library/Application Support/jack"
rm -rf /Applications/Jack.app
rm -rf /tmp/Jack.app
rm /tmp/Jack.zip
echo '[*] Uninstalled Jack'
echo
