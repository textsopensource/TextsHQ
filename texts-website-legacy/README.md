# texts-website

### Stack

* TypeScript
* Node.js
* React.js
* Postgres, TypeORM
* Redis

### Developing

#### Initial setup

1. Make sure Redis is installed (`brew install redis`) and running (`brew services start redis`)

2. Make sure Postgres is installed (`brew install postgres`) and running (`brew services start postgres`)

3. Create a Postgres DB named `jack` by running `createdb jack` (the tables will automatically be created by TypeORM)

4. `cd` into `backend` directory

5. Install [mkcert](https://github.com/FiloSottile/mkcert) (`brew install mkcert`) and run:
```
mkcert -cert-file _/ssl/cert.pem -key-file _/ssl/key.pem dev.texts.com
```

6. Open `/etc/hosts` and append `127.0.0.1 dev.texts.com` (necessary for OAuth login)

7. Run `echo "module.exports = {}" > src/config/_dev.secrets.js`

8. Refer to `src/config/default.js` and add your Redis/Postgres creds in `src/config/_dev.secrets.js` as necessary

#### Running the server

In the `backend` directory, open two terminal panes and run:
1. `yarn tsc` – this will watch the TypeScript code and transpile to JS on changes
2. `yarn nodemon` – this will restart the Node.js server on changes (will likely require `sudo` because of listening port 443)
