# texts-analytics-ingest

This is a tiny endpoint for receiving analytics events and writing them to Mongo

Events below app version `0.34.2` will be dropped

## Running locally

1. Make sure Vercel CLI is installed:

```bash
npm install -g vercel
```

2. Run:

```bash
vc dev
```

Function is recompiled on request if changed. Save the file and fire the request - it will be handled with the changes made.

## Mongo Credentials

Vercel CLI supports `.env` files. Make sure to create `.env` with your Mongo conneciton URL as `MONGO_URL`
