import mongoose from 'mongoose'
import { json, send } from 'micro'
import semver from 'semver'

const DeviceSchema = new mongoose.Schema({
  deviceId: String,
}, { strict: false, timestamps: true })

const EventSchema = new mongoose.Schema({
  device: String,
  type: String,
  eventData: {},
  metadata: {}
}, { strict: false, timestamps: true })

const Device = mongoose.model('Device', DeviceSchema)
const Event = mongoose.model('Event', EventSchema)

export default async function(req, res) {
  if (req.method !== 'POST') {

    return send(res, 200, { alive: true })
  }

  const body = await json(req)
  const version = body.metadata.appVersion || '0.0.0'

  // Drop events below version when new format was introduced
  if (!semver.satisfies(version, '>0.34.1')) {
    console.log('Dropping event', `"${body.type}"`, 'due to app version', version)
    return send(res, 200, { rejected: true }) // Return 200 so we don't trigger HTTPError
  }

  console.log('Received event', `"${body.type}"`)
  await mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true })
  const event = new Event(body)

  if (body.deviceId) {
    // If this is a first event we ever see for a given device, create one in mongo
    const existingDevice = await Device.findOne({ deviceId: body.deviceId })

    if (!existingDevice) {
      const device = new Device({ deviceId: body.deviceId })
      await device.save()
    }
  }

  await event.save()

  return send(res, 200, { ok: true })
}
