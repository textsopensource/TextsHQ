# platform-discord

## Risky actions
*Based on [Discord-S.C.U.M](https://github.com/Merubokkusu/Discord-S.C.U.M/issues/66#issue-876713938)*
- creating new DM channels ([1](https://github.com/Merubokkusu/Discord-S.C.U.M/issues/41))
- sending friend requests
- joining guilds

## Useful links
- [Official Discord Documentation](https://discord.com/developers/docs/)
- [discord-unofficial-docs](https://luna.gitlab.io/discord-unofficial-docs/)
- [Discord-S.C.U.M](https://github.com/Merubokkusu/Discord-S.C.U.M)
