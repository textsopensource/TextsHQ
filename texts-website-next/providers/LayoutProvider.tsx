import { ReactNode, useState } from "react"

import { LayoutContext } from "@/hooks/useLayout"

export function LayoutProvider(props: { children: ReactNode }) {
  const [isFlash, setIsFlash] = useState<boolean>(false)

  return (
    <LayoutContext.Provider value={{ isFlash, setIsFlash }}>
      {props.children}
    </LayoutContext.Provider>
  )
}
