# texts-website-next

### Stack

* TypeScript
* Node.js
* React.js
* Next.js
* Postgres, TypeORM
* Redis

### Developing

#### Initial setup

1. Make sure Redis is installed (`brew install redis`) and running (`brew services start redis`)

2. Make sure Postgres is installed (`brew install postgres`) and running (`brew services start postgres`)

3. Create a Postgres DB named `jack` by running `createdb jack` (the tables will automatically be created by TypeORM)

4. Install [mkcert](https://github.com/FiloSottile/mkcert) (`brew install mkcert`) and run:
```
mkdir dev-certs
mkcert -cert-file dev-certs/cert.pem -key-file dev-certs/key.pem dev.texts.com
```

5. Open `/etc/hosts` and append `127.0.0.1 dev.texts.com` (necessary for OAuth login)

6. Run `echo "module.exports = {}" > lib/config/_dev.secrets.js`

7. Refer to `lib/config/default.js` and add your Redis/Postgres creds in `lib/config/_dev.secrets.js` as necessary.

#### Running the server

Run `bun run dev` (or `yarn dev`)

#####  Stripe Webhooks

You can run `bun run dev:stripe` (or `yarn dev:stripe`) to recieve Stripe webhooks.

#####  Queues & workers

- workers in development mode: `bun run dev:workers` (or `yarn dev:workers`)
- test/trigger job for pausing subscriptions for inactive users flow: `bun run dev:trigger-inactive-check` (or `yarn dev:trigger-inactive-check`)
- monitor/see all queues, workers and jobs: `bun run dev:queue-monitor` (or `yarn dev:queue-monitor`) and go to [localhost:3000](http://localhost:3000)
