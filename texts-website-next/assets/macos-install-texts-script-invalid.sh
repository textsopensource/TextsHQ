#!/usr/bin/env zsh

echo
echo '[*] Script expired. Please go to https://texts.com/install to get a new script or contact support@texts.com'
echo
