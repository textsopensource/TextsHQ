import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import cn from 'clsx'

import Header from '@/components/Header'
import Footer from '@/components/Footer'
import { LayoutProvider } from '@/providers/LayoutProvider'
import styles from '@/layouts/BlogLayout.module.scss'
import { md5 } from '@/lib/client-util'

type FrontmatterProps = {
  title?: string
  date?: Date
  author?: { name: string, link: string, email: string }
  category?: string
}

type BlogLayoutProps = {
  children: React.ReactNode
} & FrontmatterProps

export function BlogLayout(props: BlogLayoutProps) {
  const router = useRouter()
  const { title, date, author, category, children } = props
  return (
    <LayoutProvider>
      <Head>
        <title>{title}</title>
        <link href="https://fonts.googleapis.com/css2?family=Source+Code+Pro:ital,wght@0,400;0,600;1,400&amp;display=swap" rel="stylesheet" />
        <meta property="og:title" content={title} />
        <meta property="og:image" content={`${router.pathname}.png`} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta property="twitter:image" content={`${process.env.NEXT_PUBLIC_ORIGIN}${router.pathname}.png`} />
      </Head>
      <Header />
      <main className={cn(styles.main, "blog-post")}>
        <h1 className="text-5xl font-bold">
          <Link href={router.pathname}>
            <a>{title}</a>
          </Link>
        </h1>
        <div className={styles.meta}>
          <div className="inline-flex">
            <Link href={author.link}>
              <a><img src={`https://www.gravatar.com/avatar/${md5(author.email)}?s=128`} className="rounded-full" width={48} height={48} /></a>
            </Link>
            <div className="flex flex-col ml-2 justify-around">
              <Link href={author.link}><a><span className="ml-1">{author.name}</span></a></Link>
              <span className={styles.category}>{category}</span>
            </div>
          </div>
          <span className={styles.date}>{date.toLocaleDateString(undefined, { dateStyle: 'long' })}</span>
        </div>
        <section className={styles.blogPostContent}>
          {children}
        </section>
      </main>
      <Footer />
    </LayoutProvider>
  )
}
