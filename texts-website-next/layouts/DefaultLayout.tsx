import React, { ReactNode } from 'react'

import { LayoutProvider } from '@/providers/LayoutProvider'
import Footer from '@/components/Footer'
import styles from '@/layouts/DefaultLayout.module.css'

type DefaultLayoutProps = {
  children: ReactNode
  header?: ReactNode
}

export function DefaultLayout(props: DefaultLayoutProps) {
  const { children } = props
  return (
    <LayoutProvider>
      <main className={styles.main}>{children}</main>
      <Footer />
    </LayoutProvider>
  )
}
