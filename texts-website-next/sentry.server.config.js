// This file configures the initialization of Sentry on the server.
// The config you add here will be used whenever the server handles a request.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

import * as Sentry from '@sentry/nextjs'

import { IS_DEV } from '@/lib/constants'
import config from '@/lib/config'

const { SENTRY } = config

Sentry.init({
  enabled: !IS_DEV,
  dsn: SENTRY.SERVER_DSN,
  beforeSend(event, eventHint) {
    console.error(event.exception, eventHint.originalException)
    return event
  },
})

export default Sentry
