import React from 'react'
import Head from 'next/head'

import { Hero } from '@/components/Hero'

const TWITTER_HANDLE = 'TextsHQ'

export default function SupportPage() {
  return (
    <>
      <Head>
        <title>Support</title>
      </Head>

      <div className="p-12 m-auto">
        <Hero title="Support" />
        <h4 className="text-lg m-2">
          <a href="mailto:support@texts.com" className="text-blue-500">
            Email support@texts.com &rarr;
          </a>
        </h4>
        <h4 className="text-lg m-2">
          <a href={`https://twitter.com/${TWITTER_HANDLE}`} className="text-blue-500">
            DM @{TWITTER_HANDLE} on Twitter &rarr;
          </a>
        </h4>
      </div>
    </>
  )
}
