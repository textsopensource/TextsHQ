import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import { Hero } from '@/components/Hero'

export default function MSVCRTPage() {
  return (
    <>
      <Head>
        <title>Microsoft Visual C++ Redistributable</title>
      </Head>
      <Hero
        title="Microsoft Visual C++ Redistributable"
        cta={(
          <>
            <h2><a href="https://aka.ms/vs/17/release/vc_redist.x64.exe">Download &rarr;</a></h2>
            <p>Install this to make sure certain features and integrations work properly on Windows. Having issues? Contact <Link href="/support"><a className="text-blue-500">support</a></Link></p>
            <p><a href="https://docs.microsoft.com/en-us/cpp/windows/latest-supported-vc-redist?view=msvc-170" rel="noopener noreferrer" target="_blank">Alternative Download Link &rarr;</a></p>
          </>
        )}
      />
    </>
  )
}
