import React from 'react'
import Head from 'next/head'

import { Hero } from '@/components/Hero'

export default function PaymentSuccessPage() {
  return (
    <>
      <Head>
        <title>Payment Successful</title>
      </Head>
      <div className="m-auto">
        <Hero
          cta={(
            <>
              <h2 className="mb-4 text-xl font-semibold">Payment successful.</h2>
              <a className="block py-4 px-8 text-white" href="texts://refresh-user">Click here to refresh status in the app &rarr;</a>
            </>
          )}
        />
      </div>
    </>
  )
}
