import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import { Hero } from '@/components/Hero'

const questions = [
  {
    title: "Simplifying IPC in Electron",
    link: '/blog/simplifying-ipc-in-electron',
  },
]

export default function BlogPage() {
  return (
    <>
      <Head>
        <title>Blog</title>
      </Head>
      <div className="p-12">
        <Hero title="Blog" />
        <div className="mt-8">
          <dl className="grid grid-cols-1 max-w-xl mx-auto">
            {questions.map(link => (
              <div key={link.link} className="leading-relaxed">
                <dt className="text-lg font-bold mb-4"><Link href={link.link}><a>{link.title}</a></Link></dt>
              </div>
            ))}
          </dl>
        </div>
      </div>
    </>
  )
}
