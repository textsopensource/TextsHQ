import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'

import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import { fetchJSON } from '@/lib/fetch-json'
import useUser from '@/hooks/useUser'
import { Hero } from '@/components/Hero'

export default function AppLoginPage() {
  const router = useRouter()
  const user = useUser({ redirectTo: `${DEFAULT_UNAUTH_REDIRECT}?redirectTo=${router.asPath}` })
  const [token, setToken] = useState<string>()
  const { firstName } = user ?? {}

  useEffect(() => {
    async function fetchToken() {
      const { token: loginToken } = await fetchJSON('/api/app-login-token')
      setToken(loginToken)
    }

    if (user) fetchToken()
  }, [user])

  const deepLinkURL = token && 'texts://auth?' + new URLSearchParams({ token }).toString()

  useEffect(() => {
    // auto redirect on iOS for https://developer.apple.com/documentation/authenticationservices/aswebauthenticationsession
    if (deepLinkURL && /iPad|iPhone|iPod/.test(navigator.userAgent)) window.location.href = deepLinkURL
  }, [deepLinkURL])

  return (
    <>
      <Head>
        <title>Login</title>
      </Head>
      <div className="m-auto">
        <Hero
          cta={
            deepLinkURL && (
              <>
                <h2 className="mb-8 text-xl font-semibold">Hi, {firstName}!</h2>
                <a className="brand-button py-4 px-8 text-white" href={deepLinkURL}>
                  Continue to Texts.app
                </a>
                <p className="mt-8 text-sm">If you don&apos;t have the app, <Link href="/install"><a className="text-blue-500">click here to download</a></Link>.</p>
                <iframe title="texts app auth link" style={{ visibility: 'hidden', height: 10 }} src={deepLinkURL} />
              </>
            )
          }
        />
      </div>
    </>
  )
}
