import React, {
  MutableRefObject,
  SyntheticEvent,
  useEffect,
  useRef,
  useState,
} from 'react'
import Head from 'next/head'

import { fetchJSON } from '@/lib/fetch-json'

import { Hero } from '@/components/Hero'
import jobs from '@/lib/jobs'
import Tweets from '@/components/Tweets'

const RECAPTCHA_SITE_KEY = '6LcLj68gAAAAABoO9nciPMFY9JXma_InzFsQiWfR'

function useRecaptcha() {
  useEffect(() => {
    const s = document.createElement('script')
    s.src = `https://www.google.com/recaptcha/api.js?render=${RECAPTCHA_SITE_KEY}`
    document.head.appendChild(s)
  }, [])
}

declare var grecaptcha: any

export default function JobsPage() {
  const [isSubmitSuccessful, setIsSubmitSuccessful] = useState(false)
  const urlInputRef = useRef() as MutableRefObject<HTMLInputElement>
  useRecaptcha()

  async function handleSubmit(event: SyntheticEvent) {
    event.preventDefault()

    grecaptcha.ready(async () => {
      const token = await grecaptcha.execute(RECAPTCHA_SITE_KEY, { action: 'submit' })
      await fetchJSON('/api/jobs-submission', {
        body: JSON.stringify({ url: urlInputRef.current?.value, token }),
        headers: { 'Content-Type': 'application/json' },
        method: 'POST',
      })
      setIsSubmitSuccessful(true)
    })
  }

  return (
    <>
      <Head>
        <title>Jobs</title>
      </Head>
      {isSubmitSuccessful ? (
        <Hero cta={<p>Thanks for sending in your link! We receive a high number of inbound so we may not be able to respond to each submission manually.</p>} />
      ) : (
        <div className="p-4 sm:p-10">
          <Hero cta={<h2 className="text-3xl font-bold mb-6">Build the future of messaging software</h2>} title={null} />
          <div className="w-4/5 mx-auto">
            <p className="leading-relaxed mb-4">
              Texts is hiring engineers for our founding team. We're backed by investors affiliated to Stripe, Coinbase, Superhuman, Vercel, Twitter, Facebook, Snap and others.
            </p>
            <p className="leading-relaxed mb-4">
              We believe messaging is extremely fragmented and are building Texts app, a messaging client for power users.
            </p>
            <p className="leading-relaxed mb-4">
              You'll work with a distributed team asynchronously, be self-directed and make engineering and product decisions. You'll have interesting user-facing features to build and interesting technical challenges to solve.
            </p>
            <hr />
            <h3 className="text-xl font-bold my-8">Open roles – Remote (all timezones)</h3>
            {jobs.map((role, idx) => (
              <div key={idx} className="mb-8">
                <h4 className="text-lg font-semibold">
                  {idx + 1}. {role.title}
                </h4>
                <p className="whitespace-pre-wrap leading-relaxed">{role.desc}</p>
              </div>
            ))}
            <hr />
            <h3 className="text-xl font-bold mt-8 mb-4">Apply</h3>

            <form onSubmit={handleSubmit}>
              <div>
                {/* it's associated correctly, eslint just sucks */}
                {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
                <label htmlFor="url" className="block text-sm font-medium mb-2">
                  Drop your link here and we&apos;ll get in touch:
                </label>
                <div className="mt-1 flex items-start sm:items-center flex-col sm:flex-row w-full sm:w-3/4 mb-1">
                  <input
                    type="text"
                    name="url"
                    id="url"
                    ref={urlInputRef}
                    className="p-2 shadow-sm focus:ring-blue-500 focus:border-blue-500 block w-full sm:text-sm border-gray-300 rounded-md"
                    placeholder="Your Twitter/GitHub/LinkedIn/AngelList/website link"
                    required
                  />
                  <button
                    type="submit"
                    className="mt-4 sm:mt-0 sm:ml-4 inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500"
                  >
                    Submit
                  </button>
                </div>
              </div>
            </form>

            <span className="text-sm text-gray-500">
              Alternatively, send a short email to{' '}
              <a href="mailto:jobs@texts.com" className="text-blue-500">jobs@texts.com</a>
            </span>
            <hr />
            <h3 className="text-xl font-bold mt-8 mb-4">Internal Tools</h3>
            <p className="leading-relaxed">
              We use GitHub, Sentry and{' '}
              <a href="https://linear.app/" className="text-blue-500">Linear</a> to get stuff done.
            </p>
            <hr />
            <h3 className="text-xl font-bold mt-8 mb-4">Hiring process</h3>
            <p className="leading-relaxed">
              We don't do whiteboard interviews and you're always allowed to google. We'll talk about things you've previously worked on and do a work trial – you'll be paid as a contractor for this.
            </p>
          </div>
        </div>
      )}
      <Tweets />
    </>
  )
}
