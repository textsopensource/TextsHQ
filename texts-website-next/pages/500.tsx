export default function Custom500() {
  return (
    <>
      <h1>Server-side error occurred</h1>
      <p>Please contact <a href="mailto:support@texts.com">support@texts.com</a> about this error and what you were doing on the site.</p>
    </>
  )
}
