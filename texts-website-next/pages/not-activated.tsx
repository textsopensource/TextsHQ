import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import { Hero } from '@/components/Hero'

export default function NotActivatedPage() {
  return (
    <>
      <Head>
        <title>Account Not Activated</title>
      </Head>
      <div className="m-auto">
        <Hero
          cta={(
            <>
              <h2 className="mb-8 text-xl font-semibold">Your account is not activated.</h2>
              <small>Believe something&apos;s wrong? Contact <Link href="/support"><a className="text-blue-500">support</a></Link></small>
            </>
          )}
        />
      </div>
    </>
  )
}
