import React from 'react'
import Head from 'next/head'

import { Hero } from '@/components/Hero'

const questions = [
  {
    question: 'What do you do for security? Are my messages secure?',
    answer: "Messages never touch our servers. They're sent directly to the platforms preserving end-to-end encryption. Texts app works like the official app does.",
  },
  {
    question: 'Where can I run the app?',
    answer: 'Texts app runs on macOS, Windows and Linux. Texts for iOS is under development.',
  },
  {
    question: 'What messaging platforms do you support?',
    answer: 'iMessage, SMS (with iMessage), WhatsApp, Telegram, Signal, Messenger, Twitter, Instagram, LinkedIn, IRC, Reddit, Google Chat, Slack and Discord DMs.',
  },
  {
    question: 'How do you make money?',
    answer: 'Texts charges users a monthly subscription starting at $15/month to use the app.',
  },
  {
    question: 'How does it work technically?',
    answer: 'All integrations were implemented in-house using the Texts Platform SDK.',
  },
]

export default function FAQPage() {
  return (
    <>
      <Head>
        <title>FAQ</title>
      </Head>
      <div className="p-12">
        <Hero title="FAQ" />
        <div className="mt-8">
          <dl className="grid grid-cols-1 max-w-2xl mx-auto">
            {questions.map(question => (
              <div key={question.question} className="leading-relaxed">
                <dt className="text-lg font-bold mb-4">{question.question}</dt>
                <dd className="mb-10">{question.answer}</dd>
              </div>
            ))}
          </dl>
        </div>
      </div>
    </>
  )
}
