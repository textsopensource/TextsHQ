import React from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { Hero } from '@/components/Hero'
import useActivation from '@/hooks/useActivation'

export default function InstallLinuxPage() {
  const router = useRouter()
  useActivation()

  const query = router.query.channel ? `?channel=${router.query.channel}` : ''

  return (
    <div className="m-auto">
      <Head>
        <title>Install on Linux</title>
      </Head>

      <Hero title="Install on Linux">
        <h2><a href={'/api/install/linux/x64/latest.AppImage' + query} className="brand-button text-white px-5 py-3">Download &rarr;</a></h2>
      </Hero>

      <aside className="w-full text-center mt-12 text-sm text-blue-500">
        <Link href="/install/macos"><a className="mr-4">Install on macOS</a></Link>
        <Link href="/install/windows"><a>Install on Windows</a></Link>
      </aside>
    </div>
  )
}
