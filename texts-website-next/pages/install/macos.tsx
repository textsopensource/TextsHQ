import React, { useEffect, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import slugify from 'slugify'

import { Hero } from '@/components/Hero'
import { fetchJSON } from '@/lib/fetch-json'
import useActivation from '@/hooks/useActivation'

import useUser from '@/hooks/useUser'
import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import Toast, { useToast } from '@/components/Toast'

export default function InstallMacOSPage() {
  const router = useRouter()
  useActivation()
  const user = useUser({ redirectTo: `${DEFAULT_UNAUTH_REDIRECT}?redirectTo=${router.asPath}` })
  const [token, setToken] = useState('')
  const { toastShown, showToast } = useToast()
  const query = router.query.channel ? `?channel=${router.query.channel}` : ''
  const slug = slugify(user?.firstName || 'install', { lower: true, strict: true })

  useEffect(() => {
    async function getToken() {
      const { token: loginToken } = await fetchJSON('/api/app-login-token')
      setToken(loginToken)
    }

    getToken()
  }, [user])

  if (!token) return 'Loading...' // todo improve

  const scriptURL = `${process.env.NEXT_PUBLIC_ORIGIN}/i/${token}/${slug}.sh`
  const script = `curl ${scriptURL} | sh`
  const onTextareaClick = (ev: React.MouseEvent<HTMLTextAreaElement>) => {
    (ev.target as HTMLTextAreaElement).select()
    navigator.clipboard.writeText(script)
    showToast()
  }
  const tradInstall = (
    <div className="flex flex-col">
      <h3 className="text-xl font-bold my-4">Traditional install</h3>
      <h4 className="my-2"><a href={'/api/install/macos/x64/latest.zip' + query}>Download for Intel-based Macs &rarr;</a></h4>
      <h4 className="my-2"><a href={'/api/install/macos/arm64/latest.zip' + query}>Download for Apple silicon-based Macs &rarr;</a></h4>
      <a href="https://support.apple.com/HT211814"><small>How to find which Mac you&apos;re using &rarr;</small></a>
    </div>
  )
  const quickInstall = (
    <div>
      <h3 className="text-xl font-bold my-4">Quick install</h3>
      <ol className="list-decimal pl-4">
        <li>Copy:</li>
        <textarea onClick={onTextareaClick} className="font-mono text-sm dark:bg-gray-900 border-0 resize-none w-full h-8 p-1 rounded-md" cols={80} readOnly value={script} />
        <li>Open Terminal.app (hit ⌘ Space and type &quot;Terminal&quot;)</li>
        <li>Paste and hit Enter.</li>
        <li>Wait for Texts.app to open up! You&apos;ll automatically be logged into your Texts account.</li>
      </ol>
    </div>
  )

  return (
    <div className="p-4 sm:p-10 m-auto w-4/5">
      <Head>
        <title>Install on MacOS</title>
      </Head>
      <Hero title="Install on MacOS" />
      <section>
        {tradInstall}
        <hr />
        {quickInstall}
      </section>
      <aside className="w-full mt-8 text-right text-sm text-blue-500">
        <Link href="/install/windows"><a className="mr-4">Install on Windows</a></Link>
        <Link href="/install/linux"><a>Install on Linux</a></Link>
      </aside>
      <Toast show={toastShown} setShow={showToast} />
    </div>
  )
}
