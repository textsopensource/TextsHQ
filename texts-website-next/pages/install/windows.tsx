import React from 'react'
import Link from 'next/link'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { Hero } from '@/components/Hero'
import useActivation from '@/hooks/useActivation'

export default function InstallWindowsPage() {
  const router = useRouter()
  useActivation()

  const query = router.query.channel ? `?channel=${router.query.channel}` : ''

  return (
    <div className="m-auto">
      <Head>
        <title>Install on Windows</title>
      </Head>

      <Hero title="Install on Windows">
        <h2><a href={'/api/install/windows/x64/latest.exe' + query} className="brand-button text-white px-5 py-3">Download &rarr;</a></h2>
      </Hero>
      <aside className="w-full text-center mt-12 text-sm text-blue-500">
        <Link href="/install/macos"><a className="mr-4">Install on macOS</a></Link>
        <Link href="/install/linux"><a>Install on Linux</a></Link>
      </aside>
    </div>
  )
}
