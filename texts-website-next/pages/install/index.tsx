import { useEffect } from 'react'
import { useRouter } from 'next/router'
import useActivation from '@/hooks/useActivation'

function getInstallPageLink(ua = '') {
  if (ua.includes('Mac OS X')) return '/install/macos'
  if (ua.includes('Linux')) return '/install/linux'
  return '/install/windows'
}

export default function InstallPage(): null {
  const router = useRouter()
  const activated = useActivation()
  useEffect(() => {
    if (activated) {
      router.push(getInstallPageLink(navigator.userAgent))
    }
  }, [activated])

  return null
}
