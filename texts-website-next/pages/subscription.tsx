import React, { useEffect, useReducer, useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { useRouter } from 'next/router'
import cn from 'clsx'

import { Switch } from '@headlessui/react'
import useSWR, { useSWRConfig } from 'swr'

import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import { fetchJSON } from '@/lib/fetch-json'
import useUser from '@/hooks/useUser'

import { Hero } from '@/components/Hero'

function Toggle({ enabled, setEnabled }: { enabled: boolean, setEnabled: (enabled: boolean) => void }) {
  return (
    <Switch
      checked={enabled}
      onChange={setEnabled}
      className={cn(
        enabled ? 'bg-blue-600' : 'bg-gray-200 dark:bg-gray-800',
        'relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500',
      )}
    >
      <span className="sr-only">Billing Period</span>
      <span
        aria-hidden="true"
        className={cn(
          enabled ? 'translate-x-5' : 'translate-x-0',
          'pointer-events-none inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200',
        )}
      />
    </Switch>
  )
}

enum SubscriptionPeriod {
  MONTHLY = 'MONTHLY',
  YEARLY = 'YEARLY',
}

const customerPortalLink = (
  <Link href="/customer-portal">
    <a className="text-sm font-medium text-indigo-600 hover:text-indigo-500">
      View billing history or edit payment methods
    </a>
  </Link>
)

function ActiveSubscription() {
  return (
    <>
      <h2>You're subscribed.</h2>
      {customerPortalLink}
    </>
  )
}

function PausedSubscription() {
  const [inProgress, setInProgress] = useState(false)
  const [error, setError] = useState(null)
  const { mutate } = useSWRConfig()

  return (
    <>
      <h2>Your subscription was paused due to inactivity.</h2>
      <div className="flex flex-col space-y-2 mt-5">
        {error ? (<>
          <p>Something went wrong resuming the subscription.</p>
          <Link href="/support">
            <a className="text-sm font-medium text-indigo-600 hover:text-indigo-500">Please try again and contact support if the issue persists.</a>
          </Link>
        </>
        ) : (
          <>
            <button
              type="button"
              onClick={async () => {
                setInProgress(true)
                try {
                  const data = await fetchJSON('/api/resume-subscription', { method: 'post' })
                  if (!data) throw new Error('Failed to resume subscription')
                  mutate('/api/user')
                } catch (error) {
                  setError(error)
                  setInProgress(false)
                }
              }}
              className={`justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm disabled ${inProgress && 'disabled:opacity-75 cursor-not-allowed'}`}
              disabled={inProgress}
            >
              {inProgress ? 'Resuming subscription...' : 'Resume subscription'}
            </button>
            <p className="text-xs">Your card may be charged after resuming the subscription.</p>
          </>
        )}
        <div className="mt-3">{customerPortalLink}</div>
      </div>
    </>
  )
}

export default function SubscriptionPage() {
  const router = useRouter()
  const user = useUser({ redirectTo: `${DEFAULT_UNAUTH_REDIRECT}?redirectTo=${router.asPath}` })
  const [subscriptionPeriod, setSubscriptionPeriod] = useState(SubscriptionPeriod.MONTHLY)
  const { data: stripeConfig } = useSWR('/api/stripe-config', fetchJSON)
  const [pppActivated, setPPPActivated] = useState<boolean>()

  if (!stripeConfig) return 'Loading...'

  const { stripeSubscriptionId, paymentStatus } = user ?? {}
  const isYearly = subscriptionPeriod === SubscriptionPeriod.YEARLY
  const { priceIds, pppDiscount } = stripeConfig
  const pricesMap = isYearly ? priceIds.yearly : priceIds.monthly

  const toggleSubscriptionPeriod = () => {
    setSubscriptionPeriod(isYearly ? SubscriptionPeriod.MONTHLY : SubscriptionPeriod.YEARLY)
  }

  const activatePPP = () => {
    Object.assign(stripeConfig.priceIds.monthly, pppDiscount.monthly)
    setPPPActivated(true)
  }

  return (
    <div className="p-4 sm:p-10 m-auto">
      <Head>
        <title>Subscription</title>
      </Head>
      <Hero
        cta={
          stripeSubscriptionId ? (paymentStatus === 'paused' ? <PausedSubscription /> : <ActiveSubscription />) : (
            <>
              <h2>Pricing</h2>
              <h3 id="error-message" />
            </>
          )
        }
      />
      {!stripeSubscriptionId && (
        <div>
          <div className="flex items-center justify-center mt-8 mb-12">
            <span className="text-sm mr-2">Monthly</span>
            <Toggle enabled={isYearly} setEnabled={toggleSubscriptionPeriod} />
            <span className="text-sm ml-2">Yearly</span>
          </div>
          <section className="flex flex-col sm:flex-row items-center justify-center mb-12 flex-wrap">
            {Object.entries(pricesMap).map(
              ([priceId, { desc, price }]: any, index) => (
                <Link
                  key={index}
                  href={`/api/stripe/checkout_sessions?priceId=${priceId}`}
                >
                  <a className="bg-blue-300 dark:bg-blue-900 mb-8 sm:mb-3 sm:mx-8 rounded-lg py-4 px-8 flex items-center w-64 justify-between">
                    <div>
                      <h3 className="text-xl">
                        <span className="text-2xl font-bold">${price}</span>/{isYearly ? 'year' : 'month'}
                      </h3>
                      <h4 className="text-sm mt-1 mb-2">{desc}</h4>
                    </div>
                    <p className="ml-4 text-2xl">&rarr;</p>
                  </a>
                </Link>
              ),
            )}
          </section>
          <p className="text-center leading-relaxed text-sm flex flex-col items-center">
            {/* <a
              className="text-blue-500"
              href="https://form.typeform.com/to/ZVPUSIzf?typeform-medium=embed-snippet"
              data-mode="popup"
              data-size="100"
              target="_blank"
              rel="noreferrer"
            >
              Take our pricing survey to share your thoughts anonymously  &rarr;
            </a> */}
            <span className="mt-2">Student? Contact <Link href="/support"><a className="text-blue-500">support</a></Link> for a discount.</span>
            {pppDiscount && (
              <span className="mt-2">
                Texts offers purchasing power parity pricing.{' '}
                {pppActivated ? 'Activated!' : <span className="text-blue-500 cursor-pointer" onClick={activatePPP}>Activate &rarr;</span>}
              </span>
            )}
          </p>
        </div>
      )}
    </div>
  )
}
