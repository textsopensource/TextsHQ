import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import { Hero } from '@/components/Hero'

export default function SwiftRuntimeSupportPage() {
  return (
    <>
      <Head>
        <title>Swift Runtime Support</title>
      </Head>
      <Hero
        title="Swift Runtime Support"
        cta={(
          <>
            <h2><a href="https://static.texts.com/Swift_5_Runtime_Support_for_Command_Line_Tools.dmg">Download &rarr;</a></h2>
            <p>Install this to make sure certain features work properly on macOS Catalina and lower. Having issues? Contact <Link href="/support"><a className="text-blue-500">support</a></Link></p>
          </>
        )}
      />
    </>
  )
}
