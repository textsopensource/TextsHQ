import React, { useEffect, useState } from 'react'
import Head from 'next/head'

import Header from '@/components/Header'
import Footer from '@/components/Footer'

import SocialIcons from '@/components/landing/sections/social-icons'
import LandingHeading from '@/components/landing/sections/heading'
import PrivacySection from '@/components/landing/sections/privacy'
import FeaturesSection from '@/components/landing/sections/features'
import Signup from '@/components/landing/signup'
import Tweets from '@/components/Tweets'

const socialMetaTags = (
  <>
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="description" content="The ultimate messaging app" />
    <meta property="og:title" content="Texts" />
    <meta property="og:description" content="The ultimate messaging app" />
    <meta property="og:site_name" content="Texts" />
    <meta property="og:url" content={`${process.env.NEXT_PUBLIC_ORIGIN}/`} />
    <meta property="og:type" content="product" />
    <meta property="og:image" content="/ogcover.png" />
    <meta property="twitter:image" content={`${process.env.NEXT_PUBLIC_ORIGIN}/twitter-cover-3.png`} />
  </>
)

export default function HomePage() {
  return (
    <>
      <Head>
        <title>Texts</title>
        {socialMetaTags}
        <script async src="https://platform.twitter.com/widgets.js" />
      </Head>
      <div className="min-h-screen h-full relative">
        <Header />
        <LandingHeading />
        <SocialIcons />
        <PrivacySection />
        <FeaturesSection />

        <section className="flex flex-col items-center justify-center pb-16 mt-10 sm:mt-0" id="cta">
          <h2 className="text-4xl sm:text-5xl font-bold text-center">Supercharge your messaging.</h2>
          <Signup />
        </section>
        <Tweets />
        <Footer />
      </div>
    </>
  )
}

// to avoid footer being duplicated by default layout
HomePage.getLayout = (page: any) => page
