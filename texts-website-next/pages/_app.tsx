import '@/styles/tailwind.css'
import '@/styles/globals.scss'
import '@/styles/highlightjs.css'
import '@/styles/tweet.scss'

import React, { ReactElement, ReactNode } from 'react'
import { NextPage } from 'next'
import Head from 'next/head'
import type { AppProps } from 'next/app'

import { DefaultLayout } from '@/layouts/DefaultLayout'

type Page<P = {}> = NextPage<P> & {
  getLayout?: (page: ReactElement, pageProps?: any) => ReactNode
}

type MyAppProps = AppProps & {
  Component: Page
}

function TextsApp({ Component, pageProps, router }: MyAppProps) {
  const getLayout = router.pathname.startsWith('/blog/')
    ? (page: any) => page
    : (Component.getLayout || (page => <DefaultLayout>{page}</DefaultLayout>))

  return (
    <>
      <Head>
        <title>Texts</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="apple-touch-icon" href="/favicon.png" />
        <link rel="icon" type="image/png" href="/favicon.png" />
      </Head>
      {getLayout(<Component {...pageProps} />, pageProps)}
    </>
  )
}
export default TextsApp
