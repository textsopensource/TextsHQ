import React from 'react'
import Head from 'next/head'
import Link from 'next/link'

import { Hero } from '@/components/Hero'

export default function PrivacyPage() {
  return (
    <>
      <Head>
        <title>Privacy</title>
      </Head>
      <div className="p-12">
        <Hero title="Privacy" />
        <div className="p-4 sm:p-10 leading-relaxed w-full sm:w-4/5 m-auto">
          <li>
            Messages, contacts, auth credentials, account information never touch
            Texts servers.
          </li>
          <li>Your messages are sent directly to the messaging platforms.</li>
          <li>
            All end-to-end encryption is preserved when the platform supports it.
          </li>
          <li>
            {`Texts is a client and works like the official app. It's similar to a web browser like Chrome.`}
          </li>
          <li>
            Account credentials are stored locally with the help of system
            keychain (Apple Keychain/Credential Vault), similar to how a browser
            stores your passwords.
          </li>
          <li>
            Texts makes money by charging you a monthly subscription, not by
            monetizing data.
          </li>
          <br />
          <h4>Data we collect</h4>
          <li>Name and profile picture provided by Google</li>
          <li>Performance metrics</li>
          <li>Engagement metrics like last used</li>
          <li>Errors and crashes</li>
          <br />
          <h4>3rd party services we use</h4>
          <li>
            <Link href="https://stripe.com/privacy"><a className="text-blue-500">Stripe</a></Link>
          </li>
          <li>
            <Link href="https://policies.google.com/privacy">
              <a className="text-blue-500">Google Analytics</a>
            </Link>
          </li>
          <li>
            <Link href="https://sentry.io/privacy"><a className="text-blue-500">Sentry</a></Link>
          </li>
          <li>
            <Link href="https://www.cloudflare.com/privacypolicy/">
              <a className="text-blue-500">Cloudflare</a>
            </Link>
          </li>
          <li>
            <Link href="https://www.digitalocean.com/legal/privacy-policy/">
              <a className="text-blue-500">Digital Ocean</a>
            </Link>
          </li>
          <li>
            <Link href="https://aws.amazon.com/privacy/"><a className="text-blue-500">AWS</a></Link>
          </li>
          <li>
            <Link href="https://superhuman.com/privacy"><a className="text-blue-500">Superhuman</a></Link>
          </li>
          <h4>
            <Link href="/a/texts-privacy-policy.pdf">
              <a className="text-blue-500">View full legal privacy policy &rarr;</a>
            </Link>
          </h4>
        </div>
      </div>
    </>
  )
}
