import React from 'react'
import Head from 'next/head'

import { useRouter } from 'next/router'
import { DefaultLayout } from '@/layouts/DefaultLayout'

import { Hero } from '@/components/Hero'
import { OnWaitlist } from '@/components/OnWaitlist'
import Footer from '@/components/Footer'

export default function AuthDonePage(): React.ReactNode {
  const router = useRouter()
  const { isWaitlist = false, message } = router.query

  return (
    <div className="p-4 sm:p-10 m-auto">
      <Head>
        <title>{isWaitlist ? 'Waitlist' : 'Texts'}</title>
      </Head>
      <Hero
        {...(isWaitlist && { title: 'Waitlist' })}
        cta={isWaitlist ? <OnWaitlist {...{ message: String(message) }} /> : <p>{message}</p>}
      />
    </div>
  )
}
