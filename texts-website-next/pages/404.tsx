import Link from 'next/link'
import { Hero } from '@/components/Hero'

export default function Custom404() {
  return (
    <div className="m-auto">
      <Hero
        cta={(
          <>
            <h2 className="mb-4 text-xl font-semibold">404</h2>
            <div className="text-lg mb-4">Page Not Found</div>
          </>
        )}
      >
        <Link href="/"><a>&larr; Go back home</a></Link>
      </Hero>
    </div>
  )
}
