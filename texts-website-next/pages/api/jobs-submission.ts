import type { NextApiRequest, NextApiResponse } from 'next'

import config from '@/lib/config'
import bot, { jobsReplyMarkup } from '@/lib/telegram-bot'
import { getIP, getIPCountry } from '@/lib/util'
import validateRecaptcha from './recaptcha'

const { TELEGRAM_BOT } = config

const { JOBS_CHAT_ID } = TELEGRAM_BOT

export default async function jobsSubmissionHandler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method !== 'POST') return res.status(405).json({ error: 'Method not supported' })

  const { body } = req
  const { url, token } = body

  if (url?.length > 2048) return res.status(400).end()

  const ip = getIP(req)
  if (!await validateRecaptcha(token, ip)) return res.status(403).json({})

  const country = getIPCountry(req) || '??'
  const ua = req.headers['user-agent']

  const messageText = `💼 New job submission!
${url}

${ip} (${country})
${ua}

Status: ⏳ #Unresponded`

  await bot.sendMessage(JOBS_CHAT_ID, messageText, { reply_markup: jobsReplyMarkup })

  return res.json({ success: true })
}
