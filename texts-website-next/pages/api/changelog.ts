import type { NextApiRequest, NextApiResponse } from 'next'
import { getChangelogEntries } from '@/lib/changelog'
import runMiddleware from '@/lib/run-middleware'
import cors from '@/lib/cors'

export default async function changelogHandler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  await runMiddleware(req, res, cors())
  const appVersion = String(req.headers['x-app-version'])
  const entries = await getChangelogEntries(appVersion)
  res.json({ entries })
}
