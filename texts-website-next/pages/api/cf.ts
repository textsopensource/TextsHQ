import type { NextApiResponse } from 'next'

import { NextApiRequestWithSession } from '@/lib/session'
import { getIP, getIPCountry } from '@/lib/util'

export default async function confHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  const ip = getIP(req)
  const country = getIPCountry(req)

  res.json({ ip, country })
}
