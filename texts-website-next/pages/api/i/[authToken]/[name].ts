import fse from 'fs-extra'
import { UAParser } from 'ua-parser-js'
import { Point } from '@influxdata/influxdb-client'
import type { NextApiRequest, NextApiResponse } from 'next'

import { getIP, getIPCountry, isValidChannel } from '@/lib/util'
import { getUpdateFeedJSON } from '@/lib/update-feed'
import { getUIDFromAppLoginToken } from '@/lib/tokens'
import Event, { EventType } from '@/lib/entity/Event'
import { getConnection } from '@/lib/orm'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import { SITE_ROOT } from '@/lib/server-constants'

export default async function macInstall(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { authToken, channel = 'stable' } = req.query as any

  if (!isValidChannel(channel)) {
    // this url is likely to be piped to sh so we want to use echo here
    return res.status(400).send('echo bad request')
  }

  const sh = await fse.readFile(`${SITE_ROOT}/assets/macos-install-texts-script.sh`, 'utf-8')

  const [updateFeedX64, updateFeedArm64] = await Promise.all([
    getUpdateFeedJSON('macos', channel, 'x64'),
    getUpdateFeedJSON('macos', channel, 'arm64'),
  ])

  const uid = await getUIDFromAppLoginToken(authToken)

  if (uid) {
    const ip = getIP(req)
    const country = getIPCountry(req) ?? '??'
    const ua = req.headers['user-agent']

    const UA = UAParser(ua)

    analytic.writeAPI.writePoint(
      new Point(AnalyticMeasurement.InstallScriptFetch)
        .tag('browser', UA.browser.name)
        .tag('osName', UA.os.name)
        .tag('arch', UA.cpu.architecture || 'unknown')
        .tag('country', country)
        .tag('channel', channel)
        .booleanField('v', true),
    )

    await (await getConnection())
      .getRepository<Event>('Event')
      .save(Event.create(EventType.INSTALL_SCRIPT_FETCH, ip, uid, { ua }))

    res.send(sh
      .replace('{{DOWNLOAD_URL_X64}}', updateFeedX64.url)
      .replace('{{DOWNLOAD_URL_ARM64}}', updateFeedArm64.url)
      .replaceAll('{{AUTH_TOKEN}}', authToken))
  } else {
    const invalidSh = await fse.readFile(`${SITE_ROOT}/assets/macos-install-texts-script-invalid.sh`, 'utf-8')
    res.send(invalidSh)
  }
}
