import fse from 'fs-extra'
import type { NextApiRequest, NextApiResponse } from 'next'

import { SITE_ROOT } from '@/lib/server-constants'

export default async function macInstall(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (!req.query.authToken?.toString().endsWith('.sh')) return res.status(400).send('echo bad request')
  const invalidSh = await fse.readFile(`${SITE_ROOT}/assets/macos-install-texts-script-invalid.sh`, 'utf-8')
  res.send(invalidSh)
}
