import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession } from '@/lib/session'
import { createAppLoginToken } from '@/lib/tokens'
import runMiddleware from '@/lib/run-middleware'
import { getConnection } from '@/lib/orm'
import User from '@/lib/entity/User'
import ensureUserActivated from '@/lib/ensure-user-activated'

export default async function tokenHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)

  const { uid } = req.session

  const conn = await getConnection()
  const userRepo = conn.getRepository<User>('User')
  const user = await userRepo.findOne({ id: uid })

  if (!ensureUserActivated(user, res)) return // response sent

  res.json({ token: await createAppLoginToken(uid) })
}
