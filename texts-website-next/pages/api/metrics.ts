import { NextApiRequest, NextApiResponse } from 'next'

import cors from '@/lib/cors'
import logEvent from '@/lib/metrics'
import { getIP } from '@/lib/util'
import runMiddleware from '@/lib/run-middleware'

export default async function metricsHandler(req: NextApiRequest, res: NextApiResponse) {
  await runMiddleware(req, res, cors())

  const response = await logEvent(req.body as any, getIP(req))
  res.status(200).json(response)
}
