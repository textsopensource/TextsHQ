import os from 'os'
import { NextApiRequest, NextApiResponse } from 'next'
import config from '@/lib/config'

export default async function debugHandler(req: NextApiRequest, res: NextApiResponse) {
  if (req.query.secret !== config.DEBUG_SECRET) {
    res.status(404).end()
    return
  }
  res.json({
    uptime: process.uptime(),
    hostname: os.hostname(),
    loadavg: os.loadavg(),
    env: process.env,
  })
}
