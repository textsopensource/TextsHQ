import { Point } from '@influxdata/influxdb-client'
import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession, upsertSession } from '@/lib/session'
import en from '@/lib/lang-en'
import { getIP, getLoggedInUser } from '@/lib/util'
import loginUser from '@/lib/login-user'
import { getConnection } from '@/lib/orm'
import { deleteAppLoginToken, getUIDFromAppLoginToken } from '@/lib/tokens'
import User from '@/lib/entity/User'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'
import ensureUserActivated from '@/lib/ensure-user-activated'

export default async function appLoginHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())

  if (req.method !== 'POST') return res.status(405).json({ error: 'Method not allowed' })

  const deviceID = String(req.headers['x-device-id'])
  const appVersion = String(req.headers['x-app-version'])
  const osHeader = String(req.headers['x-os'])

  const { token } = req.body
  if (!token) return res.status(400).json({ error: 'Token not found' })

  const { uid: existingUID } = req.session
  if (existingUID) {
    console.warn('existing uid', existingUID)
  }

  const uid = await getUIDFromAppLoginToken(token)
  if (!uid) return res.status(401).json({ error: en.LOGIN_FAIL_NO_UID })

  const conn = await getConnection()
  const userRepo = conn.getRepository<User>('User')
  const user = await userRepo.findOne({ id: uid })

  if (!ensureUserActivated(user, res)) return // response sent

  await loginUser(conn, req, user, { deviceID })
  await upsertSession(conn, req.sessionID, uid, getIP(req), appVersion, deviceID, osHeader)
  await deleteAppLoginToken(token)

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.AppLogin)
      .tag('appVersion', appVersion)
      .tag('os', osHeader)
      .booleanField('v', true),
  )

  const { emails } = user

  res.json({
    login_hint: {
      platform: 'google',
      display_name: emails[0],
    },
    user: getLoggedInUser(user),
  })
}
