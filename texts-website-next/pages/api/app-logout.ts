import type { NextApiResponse } from 'next'
import { Point } from '@influxdata/influxdb-client'
import session, { destroySession, NextApiRequestWithSession } from '@/lib/session'
import { getIP } from '@/lib/util'
import { getConnection } from '@/lib/orm'
import Event, { EventType } from '@/lib/entity/Event'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'

export default async function appLogoutHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  if (req.method !== 'POST') {
    return res.status(404).end()
  }

  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())

  const deviceID = String(req.headers['x-device-id'])
  const appVersion = String(req.headers['x-app-version'])
  const osHeader = String(req.headers['x-os'])
  const { uid } = req.session

  await destroySession(req, res)

  if (uid) {
    const ip = getIP(req)

    analytic.writeAPI.writePoint(
      new Point(AnalyticMeasurement.AppLogout)
        .tag('appVersion', appVersion)
        .tag('os', osHeader)
        .booleanField('v', true),
    )

    await (await getConnection())
      .getRepository<Event>('Event')
      .save(Event.create(EventType.USER_LOGOUT, ip, uid, { deviceID }))
  }

  return res.status(204).end()
}
