import type { NextApiResponse } from 'next'

import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
import session, { NextApiRequestWithSession } from '@/lib/session'
import runMiddleware from '@/lib/run-middleware'
import en from '@/lib/lang-en'
import cors from '@/lib/cors'

export default async function resumeSubscriptionHandler(req: NextApiRequestWithSession, res: NextApiResponse<{ user?: Partial<User>, error?: string }>) {
  if (!['POST', 'OPTIONS'].includes(req.method)) {
    return res.status(405).end()
  }

  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())

  const { uid } = req.session

  if (!uid) {
    return res.status(403).json({ error: en.ACCOUNT_NOT_FOUND })
  }

  const userRepo = (await getConnection()).getRepository<User>('User')
  const user = await userRepo.findOne({ id: uid })

  try {
    await user.resumeStripeSubscription()
    await userRepo.save(user)

    return res.json({
      user: {
        paymentStatus: user.paymentStatus
      }
    })
  } catch (error: any) {
    // TODO: report to sentry
    return res.status(500).json({ error: error.message })
  }
}
