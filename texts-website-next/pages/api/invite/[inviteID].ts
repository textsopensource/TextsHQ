import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession } from '@/lib/session'
import { AFTER_SIGNUP_REDIRECT, DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import runMiddleware from '@/lib/run-middleware'

export default async function inviteHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)

  const { uid } = req.session
  const { inviteID } = req.query as any

  if (uid) {
    res.redirect(AFTER_SIGNUP_REDIRECT)
    return
  }

  req.session.inviteID = inviteID
  req.session.redirectTo = AFTER_SIGNUP_REDIRECT
  res.redirect(DEFAULT_UNAUTH_REDIRECT)
}
