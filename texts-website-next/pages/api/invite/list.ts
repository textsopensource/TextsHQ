import type { NextApiRequest, NextApiResponse } from 'next'
import { groupBy, range } from 'lodash'
import sessionMiddleware, { NextApiRequestWithSession } from '@/lib/session'
import { getConnection, getRepos } from '@/lib/orm'
import Invite from '@/lib/entity/Invite'
import runMiddleware from '@/lib/run-middleware'
import ensureUserActivated from '@/lib/ensure-user-activated'

export default async function listUserInvites(req: NextApiRequest, res: NextApiResponse) {
  await runMiddleware(req, res, sessionMiddleware)

  const { session } = req as NextApiRequestWithSession

  const { uid } = session

  const { inviteRepo, userRepo } = getRepos(await getConnection())
  const user = await userRepo.findOne({ id: uid })

  if (!ensureUserActivated(user, res)) return // response sent

  const createdInvites = await inviteRepo.find({ createdByUser: uid })
  const invitedUserIDs = createdInvites.map(i => i.usedByUser).filter(Boolean)
  const allInvitedUsers = await userRepo.findByIds(invitedUserIDs)
  const groupedInvitedUsers = groupBy(allInvitedUsers, 'id')

  const invites = createdInvites.map(i => ({
    inviteID: i.id,
    used: !!i.usedByUser,
    usedBy: groupedInvitedUsers[i.usedByUser]?.[0]?.fullName || null,
  })).reverse()

  if (user.availableInvites > 0) {
    await Promise.all(range(user.availableInvites).map(() => {
      const invite = new Invite()
      invite.assignRandomID()
      invite.createdByUser = uid
      invites.push({
        inviteID: invite.id,
        used: false,
        usedBy: null,
      })
      return inviteRepo.save(invite)
    }))
    user.availableInvites = 0
    await userRepo.save(user)
  }

  return res.json({ invites })
}
