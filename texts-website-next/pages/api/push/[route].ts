import got from 'got'
import { pipeline } from 'stream/promises'
import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession } from '@/lib/session'
import runMiddleware from '@/lib/run-middleware'
import config from '@/lib/config'

export default async function pushHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)
  const { uid } = req.session
  if (!uid) return res
    .status(403)
    .json({ error: 'User ID not found' })
  const { route: _route } = req.query
  const route = String(_route)
  if (!['register', 'unregister'].includes(route)) return res.status(400).end()
  await pipeline(
    got.stream(config.PUSH_RELAY.ENDPOINT + route, {
      method: 'POST',
      headers: {
        'Content-Type': req.headers['content-type'],
        Authorization: config.PUSH_RELAY.AUTH_TOKEN,
      },
      body: JSON.stringify({ ...req.body, userId: uid }),
    }),
    res,
  )
}
