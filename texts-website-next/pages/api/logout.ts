import { UAParser } from 'ua-parser-js'
import { Point } from '@influxdata/influxdb-client'
import type { NextApiResponse } from 'next'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'

import session, { destroySession, NextApiRequestWithSession } from '@/lib/session'
import runMiddleware from '@/lib/run-middleware'
import { getIPCountry } from '@/lib/util'

export default async function logoutHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)

  await destroySession(req, res)

  const UA = UAParser(req.headers['user-agent'])

  const country = getIPCountry(req) ?? '??'

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.SiteLogout)
      .tag('browser', UA.browser.name)
      .tag('osName', UA.os.name)
      .tag('arch', UA.cpu.architecture || 'unknown')
      .tag('country', country)
      .booleanField('v', true),
  )

  return res.redirect('/')
}
