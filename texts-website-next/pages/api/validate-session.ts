import { differenceInDays } from 'date-fns'
import { Point } from '@influxdata/influxdb-client'
import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession, upsertSession } from '@/lib/session'
import en from '@/lib/lang-en'
import { getIP, getIPCountry, getLoggedInUser } from '@/lib/util'
import { getConnection } from '@/lib/orm'
import * as tgBot from '@/lib/telegram-bot'
import User from '@/lib/entity/User'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'
import ensureUserActivated from '@/lib/ensure-user-activated'

export default async function validateSessionHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())

  const { openAtLogin, wasOpenedAtLogin } = req.body || {}

  const { uid } = req.session
  const appVersion = String(req.headers['x-app-version'])
  const deviceID = String(req.headers['x-device-id'])
  const os = String(req.headers['x-os'])

  if (!uid) {
    console.log('logging out', req.session.id, uid, appVersion, deviceID, os)
    return res.json({ logged_in: false })
  }
  if (!deviceID || !appVersion || !os) {
    return res.status(400).end()
  }

  const conn = await getConnection()
  const userRepo = conn.getRepository<User>('User')

  const user = await userRepo.findOne({ id: uid })
  if (!user) return res.json({ error: en.ACCOUNT_NOT_FOUND })

  if (!ensureUserActivated(user, res)) return // response sent

  const diffDays = differenceInDays(new Date(), user.lastActive || user.createdAt)
  if (diffDays > 5) {
    tgBot.userUsedAppAfterDays(user, diffDays, appVersion, os, openAtLogin, wasOpenedAtLogin, getIPCountry(req))
  }
  user.lastActive = new Date()
  user.sessionValidateCount += 1
  await userRepo.save(user)
  await upsertSession(conn, req.sessionID, uid, getIP(req), appVersion, deviceID, os)

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.SessionValidation)
      .tag('appVersion', appVersion)
      .tag('os', os)
      .intField('sessionValidateCount', user.sessionValidateCount)
      .intField('usedAppAfterDays', diffDays),
  )

  res.json({
    logged_in: !!uid,
    user: getLoggedInUser(user),
  })
}
