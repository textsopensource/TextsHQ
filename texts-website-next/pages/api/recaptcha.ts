import got from 'got'
import config from '@/lib/config'

export default async function validateRecaptcha(token: string, ip: string) {
  const res = await got<{
    success: boolean
    challenge_ts: string
    hostname: string
    score: number
    action: string
    'error-codes'?: string[]
  }>('https://www.google.com/recaptcha/api/siteverify', {
    method: 'POST',
    form: { secret: config.RECAPTCHA_SECRET_KEY, response: token, remoteip: ip || undefined },
    resolveBodyOnly: true,
    responseType: 'json',
  })
  return res.success && res.score && res.score > 0.5
}
