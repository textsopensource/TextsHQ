import type { NextApiRequest, NextApiResponse } from 'next'
import { clearStaticSecretCache } from '@/lib/static-secret-asset'

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  await clearStaticSecretCache()
  return res.send('OK')
}

