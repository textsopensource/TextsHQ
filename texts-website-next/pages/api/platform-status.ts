import fse from 'fs-extra'
import type { NextApiRequest, NextApiResponse } from 'next'

import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'
import { SITE_ROOT } from '@/lib/server-constants'

export default async function platformStatusJson(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  await runMiddleware(req, res, cors())
  const txt = await fse.readFile(`${SITE_ROOT}/assets/platform-status.json`, 'utf-8')
  res.json(txt)
}
