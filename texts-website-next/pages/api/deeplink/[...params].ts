import { NextApiRequest, NextApiResponse } from 'next'

export default function deepLinkHandler(req: NextApiRequest, res: NextApiResponse) {
  res.redirect('texts://' + req.url.slice('/deeplink/'.length))
}
