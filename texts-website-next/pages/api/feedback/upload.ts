import { NextApiRequest, NextApiResponse } from 'next'
import formidable, { File } from 'formidable'

import runMiddleware from '@/lib/run-middleware'
import cors from '@/lib/cors'
import { UPLOAD_DIR } from '@/lib/send-feedback'

const form = formidable({
  uploadDir: UPLOAD_DIR,
})

const parseMultipartForm = (req: NextApiRequest) =>
  new Promise<File | File[]>((resolve, reject) => {
    if (!req.headers['content-type']?.includes('multipart/form-data')) {
      return reject(new Error('No multipart data found'))
    }
    form.parse(req, (err, fields, files) => {
      if (!err) {
        resolve(files.file)
      } else {
        reject(err)
      }
    })
  })

export default async function uploadFeedbackAttachment(req: NextApiRequest, res: NextApiResponse) {
  await runMiddleware(req, res, cors())
  const files = await parseMultipartForm(req)
  const file = Array.isArray(files) ? files[0] : files
  if (!file) return res.status(400).end()

  // @ts-expect-error bad typedef
  const fileName = file.newFilename

  res.json({
    id: Buffer.from(fileName).toString('base64'),
  })
}

export const config = {
  api: {
    bodyParser: false,
  },
}
