import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession } from '@/lib/session'
import en from '@/lib/lang-en'
import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
import { sendFeedbackToLinearAndTelegram } from '@/lib/send-feedback'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'
import ensureUserActivated from '@/lib/ensure-user-activated'
import sendFeedbackToSlack from '@/lib/send-slack-feedback'

export default async function feedbackHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  if (!['POST', 'OPTIONS'].includes(req.method)) {
    return res.status(404).end()
  }

  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())
  const { uid } = req.session

  if (!uid) {
    return res.status(403).json({ error: en.ACCOUNT_NOT_FOUND })
  }

  const user = await (await getConnection())
    .getRepository<User>('User')
    .findOne({ id: uid })

  if (!ensureUserActivated(user, res)) return // response sent

  if (!user) {
    return res.status(403).json({ error: en.ACCOUNT_NOT_FOUND })
  }

  await sendFeedbackToLinearAndTelegram(req, user)
  await sendFeedbackToSlack(req, user)
  res.json({ ok: true })
}
