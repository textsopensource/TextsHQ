import { NextApiRequest, NextApiResponse } from 'next'
import fetch from 'isomorphic-unfetch'

export default async function slackHandler(req: NextApiRequest, res: NextApiResponse) {
  if (!req.body.payload) return

  const payload = JSON.parse(req.body.payload)

  const [action] = payload.actions

  const isMarkingResponded = action.value === 'mark_responded'

  // Handle responded action
  if (action.value?.includes('responded')) {
    const { blocks } = payload.message
    const lastBlock = blocks.pop() // Pull the last block out. This is the buttons section

    if (!isMarkingResponded) {
      // If we're marking it as unresponded, we also need to nuke the second-to-last block, which is the responded checkmark
      blocks.pop()
    }

    lastBlock.elements = lastBlock.elements.map((element: any) => {
      // Go over the buttons and see what we need to do for the current action
      // If we're marking as responded, and we're looking at a "mark responded" button, we need to change it to a "mark unresponded"
      if (
        action.value === 'mark_responded'
        && element.value === 'mark_responded'
      ) {
        const el = { ...element }
        el.text.text = 'Mark as Unresponded'
        el.value = 'mark_unresponded'
        delete el.style

        return el
      }

      // And vice versa
      if (
        action.value === 'mark_unresponded'
        && element.value === 'mark_unresponded'
      ) {
        const el = { ...element }
        el.text.text = 'Mark as Responded'
        el.value = 'mark_responded'
        el.style = 'primary'

        return el
      }

      return element
    })

    if (isMarkingResponded) {
      blocks.push({
        type: 'context',
        elements: [
          {
            type: 'mrkdwn',
            text: '✅ Responded',
          },
        ],
      })
    }

    // Re-append the buttons section to blocks
    blocks.push(lastBlock)

    // Modify the message
    try {
      await fetch(payload.response_url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          replace_original: 'true',
          blocks,
        }),
      }).then(r => r.json())
    } catch (error: any) {
      console.error(error)
      return res.status(500).json({ error: error?.message })
    }
  }

  return res.send('')
}
