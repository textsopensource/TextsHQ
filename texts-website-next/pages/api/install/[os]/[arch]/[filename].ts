import { Point } from '@influxdata/influxdb-client'
import { UAParser } from 'ua-parser-js'
import type { NextApiResponse } from 'next'

import session, { NextApiRequestWithSession } from '@/lib/session'
import { getIP, getIPCountry, isValidChannel, isValidOS } from '@/lib/util'
import { Arch, Channel, getUpdateFeedJSON, OS } from '@/lib/update-feed'
import { getConnection } from '@/lib/orm'
import Event, { EventType } from '@/lib/entity/Event'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import runMiddleware from '@/lib/run-middleware'
import User from '@/lib/entity/User'
import ensureUserActivated from '@/lib/ensure-user-activated'

export default async function installHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)

  const { uid } = req.session
  const { os: _os, channel: _channel = 'stable', arch: _arch = 'x64' } = req.query
  const os = String(_os) as OS
  const channel = String(_channel) as Channel
  const arch = String(_arch) as Arch

  if (!isValidChannel(channel) || !isValidOS(os)) return res.status(400).end()

  const updateFeed = await getUpdateFeedJSON(os, channel, arch)
  const ip = getIP(req)
  const country = getIPCountry(req) ?? '??'
  const ua = req.headers['user-agent']

  if (uid) { // we deliberately allow download on unauthenticated routes
    const conn = await getConnection()
    const user = await conn
      .getRepository<User>('User')
      .findOne({ id: uid })
    if (!ensureUserActivated(user, res)) return // response sent
    await conn
      .getRepository<Event>('Event')
      .save(Event.create(EventType.DOWNLOAD_BINARY, ip, uid, { ua, appVersion: updateFeed.version, os, channel, arch }))
  }

  const UA = UAParser(req.headers['user-agent'])

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.BinaryDownload)
      .tag('browser', UA.browser.name)
      .tag('browserOSName', UA.os.name)
      .tag('browserArch', UA.cpu.architecture || 'unknown')
      .tag('country', country)
      .tag('appVersion', updateFeed.version)
      .tag('os', os)
      .tag('channel', channel)
      .tag('arch', arch)
      .booleanField('v', true),
  )

  res.redirect(updateFeed.url)
}
