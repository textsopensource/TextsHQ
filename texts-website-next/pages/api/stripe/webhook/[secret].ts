import { NextApiRequest, NextApiResponse } from 'next'
// import { buffer } from 'micro'
// import type { Stripe } from 'stripe'

import config from '@/lib/config'
import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
// import stripe from '@/lib/stripe'

const { SECRETS, STRIPE } = config

const webhookHandler = async (req: NextApiRequest, res: NextApiResponse) => {
  const { secret } = req.query

  if (req.method !== 'POST' || secret !== SECRETS.STRIPE_WEBHOOK_URL) {
    return res.status(404).end()
  }

  // const buf = await buffer(req)
  // const sig = req.headers['stripe-signature']!

  try {
    // const event: Stripe.Event = stripe.webhooks.constructEvent(
    //   buf.toString(),
    //   sig,
    //   STRIPE.WEBHOOK_SIGNING_SECRET,
    // )

    const { type, data } = req.body // event
    const { customer } = data?.object as any || {}
    if (!type.startsWith('customer.subscription')) return res.status(204).end()
    if (!customer) return res.status(204).end()
    const userRepo = (await getConnection()).getRepository<User>('User')
    const user = await userRepo.findOne({ stripeCustomerID: customer })
    await user.syncStripeSubStatus()
    await userRepo.save(user)

    res.json({ ok: true })
  } catch (err: any) {
    console.log(err)
    res.status(400).send(`Webhook Error: ${err.message}`)
  }
}

export default webhookHandler
