import type { NextApiResponse } from 'next'

import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
import sessionMiddleware, { NextApiRequestWithSession } from '@/lib/session'
import stripe from '@/lib/stripe'
import runMiddleware from '@/lib/run-middleware'

export default async function customerPortal(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, sessionMiddleware)
  const { uid } = req.session

  if (!uid) {
    return res.redirect(`${DEFAULT_UNAUTH_REDIRECT}?redirectTo=${req.url}`)
  }

  const user = await (await getConnection())
    .getRepository<User>('User')
    .findOne({ id: uid })

  const portalSession = await stripe.billingPortal.sessions.create({
    customer: user.stripeCustomerID,
    return_url: process.env.NEXT_PUBLIC_ORIGIN,
  })

  res.redirect(portalSession.url)
}
