import type { NextApiResponse } from 'next'

import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
import session, { NextApiRequestWithSession } from '@/lib/session'
import stripe from '@/lib/stripe'
import runMiddleware from '@/lib/run-middleware'

export default async function checkoutSessionHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)

  try {
    const {
      query: { priceId },
      session: { uid },
    } = req

    if (!uid) {
      return res.redirect(`${DEFAULT_UNAUTH_REDIRECT}?redirectTo=${req.url}`)
    }

    const userRepo = (await getConnection()).getRepository<User>('User')
    const user = await userRepo.findOne({ id: uid })

    if (!user.stripeCustomerID) {
      await user.createStripeCustomer()
      await userRepo.save(user)
    }

    const localSession = await stripe.checkout.sessions.create({
      customer: user.stripeCustomerID,
      mode: 'subscription',
      line_items: [
        {
          price: priceId as string,
          quantity: 1,
        },
      ],
      // {CHECKOUT_SESSION_ID} will be replaced by Stripe
      // https://stripe.com/docs/payments/checkout/custom-success-page
      success_url: `${process.env.NEXT_PUBLIC_ORIGIN}/payment-success?session_id={CHECKOUT_SESSION_ID}`,
      cancel_url: `${process.env.NEXT_PUBLIC_ORIGIN}/subscription`,
    })

    res.redirect(303, localSession.url)
  } catch (err: any) {
    res.status(400).json({ error: { message: err.message } })
  }
}
