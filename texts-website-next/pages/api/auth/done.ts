import { Point } from '@influxdata/influxdb-client'
// import Sentry from '@sentry/nextjs'
import { UAParser } from 'ua-parser-js'
import type { NextApiResponse } from 'next'

import { Repository } from 'typeorm'
import allEmails from '@/lib/all-emails'
import config from '@/lib/config'
import { AFTER_SIGNUP_REDIRECT, IS_DEV } from '@/lib/constants'
import sendEmail from '@/lib/emails'
import Event, { EventType } from '@/lib/entity/Event'
import User from '@/lib/entity/User'
import langEn from '@/lib/lang-en'
import { getConnection, getRepos } from '@/lib/orm'
import sessionMiddleware, { NextApiRequestWithSession } from '@/lib/session'
import { getIP, getIPCountry } from '@/lib/util'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import runMiddleware from '@/lib/run-middleware'
import loginUser from '@/lib/login-user'
// import { isUserOnSuperhuman } from '@/lib/superhuman'
import Waitlist from '@/lib/entity/Waitlist'
import type Invite from '@/lib/entity/Invite'

const { ADMIN_USER_IDS, DISABLE_NO_INVITE_REGISTRATION } = config

async function sendWelcomeEmail(user: User) {
  const usesSuperhuman = false
  // await isUserOnSuperhuman(
  //   user.emails[0],
  //   user.fullName,
  // )
  return sendEmail(allEmails.welcome(user, usesSuperhuman)).catch(console.error)
}

async function createUserFromProfile(
  ua: string,
  country: string,
  ip: string,
  grant: any,
  userEmails: string[],
  invite?: Invite,
) {
  const { response } = grant
  const { eventRepo, inviteRepo, userRepo } = getRepos(await getConnection())
  const { sub, given_name, family_name } = response?.profile
  const invitedByUser = invite?.createdByUser

  const user = new User()
  user.assignRandomID()
  user.emails = userEmails
  user.googleInfo = response
  user.googleAuthID = sub
  const [firstName, middleName] = given_name.split(' ')
  user.firstName = firstName
  user.middleName = middleName
  user.lastName = family_name
  user.invitedByUser = invitedByUser

  const createdUser = await userRepo.save(user)

  if (invite) {
    // eslint-disable-next-line no-param-reassign
    invite.usedByUser = user.id
    await inviteRepo.save(invite)
  }

  const UA = UAParser(ua)

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.UserCreated)
      .tag('browser', UA.browser.name)
      .tag('osName', UA.os.name)
      .tag('arch', UA.cpu.architecture || 'unknown')
      .tag('country', country)
      .booleanField('v', true),
  )

  const ev = Event.create(
    EventType.USER_CREATE,
    ip,
    user.id,
    invite ? { invitedByUser: user.invitedByUser } : {},
  )
  await eventRepo.save(ev)

  if (ADMIN_USER_IDS.includes(invitedByUser)) sendEmail(allEmails.welcomeAdminOnly(user)).catch(console.error)
  else sendWelcomeEmail(user)

  return createdUser
}

const ALLOW_REGISTRATION_AFTER_ATTEMPTS = 2

async function isWaitlisted(email: string, profile: any, waitlistRepo: Repository<Waitlist>, req: NextApiRequestWithSession) {
  const waitlistEntry = await waitlistRepo.findOne({ email })
  if (waitlistEntry) {
    waitlistEntry.checkCount = (waitlistEntry.checkCount || 0) + 1
    await waitlistRepo.save(waitlistEntry)
    if (waitlistEntry.checkCount > ALLOW_REGISTRATION_AFTER_ATTEMPTS) return // allow registration after ALLOW_REGISTRATION_AFTER_ATTEMPTS times
    return `/auth/completed?${new URLSearchParams({ isWaitlist: 'true', message: langEn.ALREADY_ON_WAITLIST })}`
  }
  const firstName = profile.given_name
  const lastName = profile.family_name
  const newWaitlistRecord = waitlistRepo.create({
    email,
    firstName,
    lastName,
    os: req.headers['user-agent'],
    ip: getIP(req),
    country: getIPCountry(req),
  })
  await waitlistRepo.save(newWaitlistRecord)
  // sendEmail(allEmails.onWaitlist(email, firstName)).catch(console.error)
  return `/auth/completed?${new URLSearchParams({ isWaitlist: 'true', message: langEn.ADDED_TO_WAITLIST })}`
}

export default async function authDoneHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, sessionMiddleware)

  const { session } = req
  const { uid, grant, redirectTo } = session
  const { provider, response } = grant ?? {}
  delete req.session.grant

  const { profile } = response ?? {}
  const conn = await getConnection()
  const { inviteRepo, userRepo, waitlistRepo } = getRepos(conn)

  const ip = getIP(req)
  const country = getIPCountry(req) ?? '??'
  const ua = req.headers['user-agent']
  let user: User

  if (uid) {
    user = await userRepo.findOneOrFail({ id: uid })
  } else if (profile) {
    if (IS_DEV) console.log(grant)

    if (provider !== 'google') throw Error('provider !== google')

    const { email, email_verified } = profile
    if (!email_verified) {
      return res.redirect(`/auth/completed?${new URLSearchParams({ message: langEn.GOOGLE_EMAIL_NOT_VERIFIED })}`)
    }

    const emails = [email].filter(Boolean)
    if (emails.length === 0) {
      return res.redirect(`/auth/completed?${new URLSearchParams({ message: langEn.GOOGLE_NO_EMAIL })}`)
    }

    user = await userRepo.createQueryBuilder('user')
      .where(':email = ANY(user.emails)', { email })
      .getOne()

    if (!user) {
      const { inviteID } = session

      if (DISABLE_NO_INVITE_REGISTRATION && !inviteID) {
        const redirect = await isWaitlisted(email, profile, waitlistRepo, req)
        if (redirect) return res.redirect(redirect)
      }

      if (inviteID) {
        const invite = await inviteRepo.findOne(inviteID)
        if (!invite) {
          return res.redirect(`/auth/completed?${new URLSearchParams({ message: langEn.INVITE_NOT_FOUND })}`)
        }
        if (invite.usedByUser) {
          // Sentry?.captureMessage(`INVITE_ALREADY_USED: ${inviteID}`, {
          //   extra: {
          //     sid: req.sessionID,
          //     invite,
          //   },
          // })
          return res.redirect(`/auth/completed?${new URLSearchParams({ message: langEn.INVITE_ALREADY_USED })}`)
        }

        user = await createUserFromProfile(ua, country, ip, grant, emails, invite)
      } else {
        req.session.redirectTo = AFTER_SIGNUP_REDIRECT
        user = await createUserFromProfile(ua, country, ip, grant, emails)
      }
      delete req.session.inviteID
    }
    await loginUser(conn, req, user)
  } else {
    return res.redirect('/')
  }
  req.session.redirectTo = null
  return res.redirect(user ? redirectTo ?? '/app-login' : '/logout')
}
