import type { NextApiResponse } from 'next'

import grant from '@/lib/grant'
import sessionMiddleware, { NextApiRequestWithSession } from '@/lib/session'
import runMiddleware from '@/lib/run-middleware'

export default async function grantHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, sessionMiddleware)

  if (req.session.uid) {
    return res.redirect('/auth/done')
  }
  if (req.query.redirectTo) {
    req.session.redirectTo = String(req.query.redirectTo)
  }
  const { provider, override } = req.query as any
  const { location, session } = await grant({
    method: req.method,
    params: { provider, override },
    query: req.query,
    body: req.body,
    state: {},
    session: req.session.grant,
  })
  req.session.grant = session

  return res.redirect(location)
}
