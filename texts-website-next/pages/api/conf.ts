import semver from 'semver'
import { cloneDeep } from 'lodash'
import { addWeeks } from 'date-fns'
import { Point } from '@influxdata/influxdb-client'
import type { NextApiResponse } from 'next'

import { defaultRemoteConfig, RemoteConfKey } from '@/lib/remote-config'
import User from '@/lib/entity/User'
import config from '@/lib/config'
import session, { NextApiRequestWithSession } from '@/lib/session'
import { getConnection } from '@/lib/orm'
import sendEmail from '@/lib/emails'
import allEmails from '@/lib/all-emails'
import { getIP, getUserFromDeviceID } from '@/lib/util'
import Event, { EventType } from '@/lib/entity/Event'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'

async function onBuildExpiry(request: NextApiRequestWithSession, userPromise: User | Promise<User>, appVersion: string, osHeader: string, deviceID: string) {
  const user = await userPromise
  sendEmail(allEmails.userBuildExpired(user, `${appVersion} – ${osHeader} – ${deviceID}` + (user ? ' (user found through device ID)' : ''))).catch(console.error)
  const ip = getIP(request)

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.BuildExpired)
      .tag('appVersion', appVersion)
      .tag('os', osHeader)
      .booleanField('v', true),
  )

  await (await getConnection())
    .getRepository<Event>('Event')
    .save(Event.create(EventType.BUILD_EXPIRED, ip, user?.id, { appVersion, os: osHeader, deviceID }))
}

const isVersionExpiredForOS = (os: string, appVersion: string) =>
  (os ? [os, '*'] : []).includes((config.EXPIRE_BUILDS.SPECIFIC_VERSIONS as Record<string, string>)[appVersion])

export default async function confHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)
  await runMiddleware(req, res, cors())

  const deviceID = String(req.headers['x-device-id'])
  const appVersion = String(req.headers['x-app-version'])
  const osHeader = String(req.headers['x-os'])

  const conf = cloneDeep(defaultRemoteConfig)

  const { uid } = req.session

  const user = uid ? await (await getConnection())
    .getRepository<User>('User')
    .findOne({ id: uid }) : undefined

  const isValidAppVersion = semver.valid(appVersion)
  const skipUpdateForVersion = config.SKIP_UPDATE_FOR_VERSIONS.includes(appVersion)
  const isAdminUser = config.ADMIN_USER_IDS.includes(uid)
  if (!isAdminUser && isValidAppVersion && !skipUpdateForVersion && (
    semver.lte(appVersion, config.EXPIRE_BUILDS.LTE_VERSION)
    || isVersionExpiredForOS(osHeader?.split(' ', 1)[0], appVersion))
  ) {
    conf[RemoteConfKey.BUILD_EXPIRY] = 1 // 1970-01-01T00:00:00.001Z
    onBuildExpiry(req, user || getUserFromDeviceID(deviceID), appVersion, osHeader, deviceID)
  } else {
    const buildExpiry = addWeeks(new Date(), 2)
    conf[RemoteConfKey.BUILD_EXPIRY] = new Date(buildExpiry).getTime()
  }
  if (isValidAppVersion && !skipUpdateForVersion && semver.lte(appVersion, config.EXPIRE_BUILDS.SOFT_EXPIRY.LTE_VERSION)) {
    conf[RemoteConfKey.SOFT_BUILD_EXPIRY] = true
  }

  if (!uid || !user) {
    return res.json(conf)
  }

  res.json(conf)
}
