import { cloneDeep } from 'lodash'
import type { NextApiRequest, NextApiResponse } from 'next'

import sessionMiddleware, { NextApiRequestWithSession } from '@/lib/session'
import { getConnection, getRepos } from '@/lib/orm'
import config from '@/lib/config'
import runMiddleware from '@/lib/run-middleware'
import ensureUserActivated from '@/lib/ensure-user-activated'
import { getIPCountry } from '@/lib/util'
import { IS_DEV } from '@/lib/constants'

const { STRIPE_PRODUCT_PRICE_IDS_PPP, STRIPE_PRODUCT_PRICE_IDS_STUDENT, STRIPE_PRODUCT_PRICE_IDS } = config

const PPP_COUNTRY_CODES = new Set([
  'IN',
  'BR',
  'TR',
])

export default async function listUserInvites(req: NextApiRequest, res: NextApiResponse) {
  await runMiddleware(req, res, sessionMiddleware)

  const { session } = req as NextApiRequestWithSession

  const { uid } = session

  const { userRepo } = getRepos(await getConnection())
  const user = await userRepo.findOne({ id: uid })

  if (!ensureUserActivated(user, res)) return // response sent

  const priceIds = cloneDeep(STRIPE_PRODUCT_PRICE_IDS)

  if (user.studentUntil > new Date()) Object.assign(priceIds.monthly, STRIPE_PRODUCT_PRICE_IDS_STUDENT.monthly)

  const hasPPPDiscount = PPP_COUNTRY_CODES.has(getIPCountry(req)) || IS_DEV

  return res.json({ priceIds, pppDiscount: hasPPPDiscount ? STRIPE_PRODUCT_PRICE_IDS_PPP : undefined })
}
