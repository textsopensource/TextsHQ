import type { NextApiRequest, NextApiResponse } from 'next'
import { getUpdateFeed } from '@/lib/update-feed'
import { isValidOS } from '@/lib/util'

export default async function updateFeedJson(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const { version: installedVersion, channel, platform = 'darwin', arch, mid } = req.query as any

  const os = {
    darwin: 'macos',
    linux: 'linux',
    win32: 'windows',
  }[platform as string] || platform

  if (!isValidOS) {
    res.status(400).end()
    return
  }

  const deviceID = String(req.headers['x-device-id']) || mid

  const updateFeed = await getUpdateFeed({
    os,
    channel,
    arch,
    deviceID,
    installedVersion: installedVersion as string,
  })

  if (updateFeed) {
    res.json(updateFeed)
  } else {
    res.status(204).end()
  }
}
