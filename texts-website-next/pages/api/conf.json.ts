import { NextApiRequest, NextApiResponse } from 'next'

export default async function confHandler(req: NextApiRequest, res: NextApiResponse) {
  res.status(204).send(undefined)
}
