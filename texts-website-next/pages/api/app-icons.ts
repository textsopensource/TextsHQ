import { NextApiRequest, NextApiResponse } from 'next'
import cors from '@/lib/cors'
import runMiddleware from '@/lib/run-middleware'

const appIcons = {
  icons: [
    {
      link: 'https://texts.com/app-icons/jdsimcoe-1.png',
      creator: 'Jonathan Simcoe',
      creator_link: 'https://twitter.com/jdsimcoe',
    },
    {
      link: 'https://texts.com/app-icons/d7mtg-1.png',
      creator: 'D7mtg',
      creator_link: 'https://d7m.tg',
    },
  ],
}

export default async function appIconsHandler(req: NextApiRequest, res: NextApiResponse) {
  await runMiddleware(req, res, cors())

  return res.json(appIcons)
}
