import type { NextApiRequest, NextApiResponse } from 'next'
import { getUpdateFeedYML, OS } from '@/lib/update-feed'

export default async function updateFeed(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  const os: OS = {
    'latest-mac.yml': 'macos',
    'latest-linux.yml': 'linux',
    'latest.yml': 'windows',
  }[req.query.name as string] as OS

  if (!os) return res.status(400).end()

  const { version: installedVersion, channel, arch, mid } = req.query as any

  const deviceID = req.headers['x-device-id'] || mid

  const yml = await getUpdateFeedYML({ os, channel, arch, deviceID, installedVersion })

  return res.send(yml)
}
