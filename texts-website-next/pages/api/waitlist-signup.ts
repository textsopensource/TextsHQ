import type { NextApiRequest, NextApiResponse } from 'next'
import { UAParser } from 'ua-parser-js'
import { Point } from '@influxdata/influxdb-client'

import allEmails from '@/lib/all-emails'
import sendEmail from '@/lib/emails'
import { WaitlistStatus } from '@/lib/constants'
import Waitlist from '@/lib/entity/Waitlist'
import { getConnection } from '@/lib/orm'
import { getIP, getIPCountry } from '@/lib/util'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'

export default async function waitlistSignup(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method !== 'POST') return res.status(405).json({ message: 'Method not supported' })
  const conn = await getConnection()

  const email = req.body.email.trim().toLowerCase()
  const waitlistRepo = conn.getRepository<Waitlist>('Waitlist')
  const waitlistEntry = await waitlistRepo.findOne({ email })

  if (waitlistEntry) {
    waitlistEntry.checkCount = (waitlistEntry.checkCount || 0) + 1
    await waitlistRepo.save(waitlistEntry)

    return res.json({ status: WaitlistStatus.ALREADY_ON_WAITLIST })
  }

  const country = getIPCountry(req) ?? '??'

  await waitlistRepo.save(
    waitlistRepo.create({
      email,
      os: req.headers['user-agent'],
      ip: getIP(req),
      country,
    }),
  )

  // await sendEmail(allEmails.onWaitlist(email)).catch(console.error)

  const UA = UAParser(req.headers['user-agent'])

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.WaitlistSignup)
      .tag('browser', UA.browser.name)
      .tag('osName', UA.os.name)
      .tag('arch', UA.cpu.architecture || 'unknown')
      .tag('country', country)
      .booleanField('v', true),
  )

  return res.json({ status: WaitlistStatus.ADDED_TO_WAITLIST })
}
