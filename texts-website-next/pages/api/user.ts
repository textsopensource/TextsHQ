import type { NextApiResponse } from 'next'

import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'
import session, { NextApiRequestWithSession } from '@/lib/session'
import runMiddleware from '@/lib/run-middleware'

export default async function userHandler(req: NextApiRequestWithSession, res: NextApiResponse) {
  await runMiddleware(req, res, session)
  const { uid } = req.session

  if (!uid) {
    return res.json({ user: null })
  }

  const {
    firstName,
    fullName,
    lastName,
    emails,
    paymentStatus,
    availableInvites,
    flags,
    subscription,
    isActivated,
  } = await (await getConnection())
    .getRepository<User>('User')
    .findOne({ id: uid })

  res.json({
    user: {
      uid,
      firstName,
      lastName,
      fullName,
      emails,
      paymentStatus,
      availableInvites,
      flags,
      isActivated,
      stripeSubscriptionId: subscription?.id,
    },
  })
}
