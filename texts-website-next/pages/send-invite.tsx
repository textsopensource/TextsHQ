import React from 'react'
import copyToClipboard from 'copy-to-clipboard'
import useSWR from 'swr'
import cn from 'clsx'
import { partition } from 'lodash'

import { Hero } from '@/components/Hero'
import { fetchJSON } from '@/lib/fetch-json'
import Toast, { useToast } from '@/components/Toast'
import { ChevronDownIcon } from '@heroicons/react/solid'

const Invite = ({
  inviteID,
  usedBy,
  toastShown,
  showToast,
}: {
  inviteID: string
  usedBy?: string
  toastShown?: boolean
  showToast?: () => void
}) => {
  const link = `${process.env.NEXT_PUBLIC_ORIGIN}/invite/${inviteID}`

  return (
    <div key={inviteID} className="flex flex-col items-start my-4 bg-gray-300 dark:bg-gray-900 p-1 rounded-xl">
      <textarea
        onClick={e => {
          if (usedBy) return
          copyToClipboard(link);
          (e.target as HTMLTextAreaElement).select()
          if (!toastShown) {
            showToast()
          }
        }}
        className={cn('font-mono bg-gray-300 dark:bg-gray-900 border-0 resize-none w-full h-6 p-1 rounded-md', { 'opacity-50 cursor-not-allowed line-through': usedBy })}
        readOnly
        value={link}
        disabled={Boolean(usedBy)}
      />
      {usedBy && (
        <div className="ml-1 mt-4 text-right">Used by: <span className="font-semibold">{usedBy}</span></div>
      )}
    </div>
  )
}

export default function InvitePage() {
  const { toastShown, showToast } = useToast()
  const { data: invitesData } = useSWR('/api/invite/list', fetchJSON)

  const [usedInvites, unusedInvites] = partition(invitesData?.invites || [], i => i.usedBy)

  const freeInviteCount = unusedInvites.length

  const mapInvite = ({ inviteID, usedBy }: { inviteID: string, usedBy?: string }) => (
    <Invite
      key={inviteID}
      inviteID={inviteID}
      usedBy={usedBy}
      toastShown={toastShown}
      showToast={showToast}
    />
  )

  return (
    <div className="p-4 sm:p-10 m-auto w-full">
      <Hero title="Invites" className="w-full">
        {invitesData && (
          <div className="max-w-xl mx-auto">
            <p>
              You have {freeInviteCount === 0 ? 'no' : freeInviteCount} {freeInviteCount === 1 ? 'invite' : 'invites'} available.{' '}
              {freeInviteCount === 0 ? 'Check back later!' : ''}
              {freeInviteCount > 0 && 'Copy a link and send it to your friend to invite them.'}
            </p>
            <details open className="bg-gray-200 dark:bg-gray-700 mt-8 rounded-lg">
              <summary className="flex items-center p-2">
                <h4 className="flex items-center pl-2.5">
                  Unused invites
                  <ChevronDownIcon className="w-6 h-6 mt-1 ml-2" />
                </h4>
              </summary>
              <div className="dark:bg-gray-800 border border-solid border-gray-100 dark:border-gray-800 p-1 px-4 rounded-b-lg">
                {unusedInvites.length > 0 ? unusedInvites.map(mapInvite) : (
                  <div className="text-sm py-5 opacity-60">No unused invites</div>
                )}
              </div>
            </details>

            <details open className="bg-gray-200 dark:bg-gray-700 mt-8 rounded-lg">
              <summary className="flex items-center p-2">
                <h4 className="flex items-center pl-2.5">
                  Used invites
                  <ChevronDownIcon className="w-6 h-6 mt-1 ml-2" />
                </h4>
              </summary>
              <div className="dark:bg-gray-800 border border-solid border-gray-100 dark:border-gray-800 p-1 px-4 rounded-b-lg">
                {usedInvites.length > 0 ? usedInvites.map(mapInvite) : (
                  <div className="text-sm py-5 opacity-60">No used invites</div>
                )}
              </div>
            </details>
          </div>
        )}
      </Hero>
      <Toast show={toastShown} setShow={showToast} />
    </div>
  )
}
