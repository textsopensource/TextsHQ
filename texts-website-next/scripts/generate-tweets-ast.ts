import path from 'path'
import fs from 'fs/promises'
import { fetchTweetAst } from 'static-tweets'
import got from 'got'

const TWEETS = [
  'https://twitter.com/rauchg/status/1381703346998845440',
  'https://twitter.com/ianhunter/status/1504176856748154880',
  'https://twitter.com/martyrdison/status/1376710800170086411',
  'https://twitter.com/karrisaarinen/status/1361928046257664001',
  'https://twitter.com/KishanBagaria/status/1315751935333429248',
  'https://twitter.com/subtlyperfect/status/1514883464977616898',
  'https://twitter.com/fkpxls/status/1401918302587441152',
  'https://twitter.com/chanpory/status/1516528198615375875',
  'https://twitter.com/johncutlefish/status/1412459164450758660',
  'https://twitter.com/nic__carter/status/1356665370740989953',
  // 'https://twitter.com/rauchg/status/1500231107362504704',
  'https://twitter.com/gabagooldoteth/status/1509618302846312453',
  'https://twitter.com/brian_lovin/status/1350134524278001665',
]

function mapNode(node: any) {
  if (typeof node === 'string') return node.replace('http://', '')
  return node
}

function fetchAndEncodeImage(url: string) {
  return got(url).buffer().then((image) => `data:image/jpeg;base64,${image.toString('base64')}`)
}

async function mapAst(ast: any) {
  const arr = ast?.[0].nodes?.[0]?.nodes
  if (arr) ast[0].nodes[0].nodes = arr.map(mapNode)

  const avatarUrl = ast?.[0].data?.avatar?.normal
  if (avatarUrl) ast[0].data.avatar.normal = await fetchAndEncodeImage(avatarUrl)

  const nodes = ast?.[0].nodes
  if (nodes) {
    ast[0].nodes = await Promise.all(ast?.[0].nodes.map(async (node: any) => {
      if (node?.tag === 'blockquote' && node?.data?.ast?.[0].data?.avatar?.normal) {
        node.data.ast[0].data.avatar.normal = await fetchAndEncodeImage(node.data.ast[0].data.avatar.normal)
      }
      return node
    }))
  }

  return ast
}

async function main() {
  const tweetIDs = TWEETS.map(link => link.split('/').pop())
  const tweets = await Promise.all(tweetIDs.map(async tweetID => ({ id: tweetID, ast: await mapAst(await fetchTweetAst(tweetID)) })))
  const json = JSON.stringify(tweets, null, 2)
  await fs.writeFile(path.join(__dirname, '../tweets.json'), json)
}
main()
