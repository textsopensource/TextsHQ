import '@/lib/config/loadDotEnv'
import { Queues, processInactiveUsersQueue } from '@/lib/queues'

async function main() {
  await processInactiveUsersQueue.add(
    `adhoc-${new Date().toISOString()}`, // every trigger creates a new, unique job
    { inactiveSince: process.env.INACTIVE_SINCE_DAYS && parseInt(process.env.INACTIVE_SINCE_DAYS) },
  )
}

main()
  .then(() => {
    console.log(`
      Queued "PROCESS_INACTIVE_USERS" job.
      Make sure the workers are running, this script only adds a job to the queue and does not run it.
    `)
    process.exit(0)
  })
  .catch(() => {
    console.error(`
      !!! Failed to queue the "PROCESS_INACTIVE_USERS" job.
      Make sure the Redis is running and the credentials are correct.
    `)
    process.exit(1)
  })
