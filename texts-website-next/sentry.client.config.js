// This file configures the initialization of Sentry on the browser.
// The config you add here will be used whenever a page is visited.
// https://docs.sentry.io/platforms/javascript/guides/nextjs/

import * as Sentry from '@sentry/nextjs'

import { IS_DEV } from '@/lib/constants'

Sentry.init({
  enabled: !IS_DEV,
  dsn: process.env.NEXT_PUBLIC_SENTRY_CLIENT_DSN,
  beforeSend(event, eventHint) {
    console.error(event.exception, eventHint.originalException)
    return event
  },
})

export default Sentry
