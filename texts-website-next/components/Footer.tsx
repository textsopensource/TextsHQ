import Link from 'next/link'
import { useRouter } from 'next/router'

import useUser from '@/hooks/useUser'

const footerLinkClassName = 'text-gray-900 hover:text-gray-700 dark:text-white dark:hover:text-gray-200 font-semibold my-3 sm:my-0 sm:mx-3'
const FooterLink: React.FC<{ href: string }> = ({ href, children }) => (
  <Link href={href}>
    <a className={footerLinkClassName}>{children}</a>
  </Link>
)
const FooterExtLink: React.FC<{ href: string }> = ({ href, children }) => (
  <a href={href} className={footerLinkClassName} rel="noopener noreferrer">{children}</a>
)

// eslint-disable-next-line react/no-unused-prop-types
type IFooterLink = { href: string, text: string, isExternal?: boolean }

const defaultUserLinks: IFooterLink[] = [
  { href: '/subscription', text: 'Subscription' },
  { href: '/install', text: 'Install App' },
  { href: '/send-invite', text: 'Invites' },
]
const defaultLinks: IFooterLink[] = [
  { href: '/support', text: 'Support' },
  { href: '/privacy', text: 'Privacy' },
  { href: '/faq', text: 'FAQ' },
  { href: '/jobs', text: 'Jobs' },
  { href: 'https://twitter.com/TextsHQ', text: 'Twitter', isExternal: true },
]

export default function Footer() {
  const user = useUser()
  const router = useRouter()
  const mapLink = ({ href, text, isExternal }: IFooterLink) => !router.pathname.startsWith(href) && (
    isExternal
      ? <FooterExtLink href={href} key={href}>{text}</FooterExtLink>
      : <FooterLink href={href} key={href}>{text}</FooterLink>
  )
  return (
    <footer className="flex flex-col sm:flex-row items-center justify-between p-8 footer">
      <FooterLink href="/">
        <div className="flex items-center">
          <img src="/icon.png" alt="Texts" className="w-10 filter saturate-0 footer-icon" />
          <span className="text-3xl font-bold ml-4">Texts</span>
        </div>
      </FooterLink>
      <div className="flex items-center flex-col sm:flex-row">
        {user?.isActivated && defaultUserLinks.map(mapLink)}
        {defaultLinks.map(mapLink)}
        {user?.isActivated && <FooterLink href="/logout">Logout</FooterLink>}
      </div>
    </footer>
  )
}
