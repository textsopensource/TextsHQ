import { fetchTweetAst } from 'static-tweets'
import { Tweet as ReactStaticTweet } from 'react-static-tweets'
import tweets from '../tweets.json'

type Tweets = [{ id: string, ast: Awaited<ReturnType<typeof fetchTweetAst>> }]

export default function Tweets() {
  return (
    <section className="grid grid-cols-1 sm:grid-cols-3 gap-4 sm:gap-5 p-4 sm:p-20 mb-16">
      {tweets.map(({ id, ast }) => <ReactStaticTweet key={id} id={id} ast={ast} />)}
    </section>
  )
}
