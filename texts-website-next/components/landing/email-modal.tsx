import { Fragment, useCallback, useRef, useState } from 'react'
import cn from 'clsx'
import { Dialog, Transition } from '@headlessui/react'

type EmailModalProps = {
  open: boolean
  setOpen: (open: boolean) => void
  onSubmit: Function
  email: string
  setEmail: (email: string) => void
}

export default function EmailModal({ open, setOpen: _setOpen, onSubmit }: EmailModalProps) {
  const [loading, setLoading] = useState(false)
  const inputRef = useRef(null)

  const setOpen = loading ? () => {} : _setOpen

  const handleSubmit = useCallback(async (e: any) => {
    if (loading) {
      return
    }
    setLoading(true)
    await onSubmit(e)

    setOpen(false)
    setLoading(false)
  }, [loading])

  return (
    <Transition.Root show={open} as={Fragment}>
      <Dialog as="div" className="fixed z-10 inset-0 overflow-y-auto" initialFocus={inputRef} onClose={setOpen}>
        <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <div className="inline-block align-bottom bg-white dark:bg-cool-gray-900 dark:text-gray-100 rounded-2xl px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full sm:p-6">
              <div>
                <div className="mx-auto flex items-center justify-center h-12 w-12">
                  <img src="/icon.png" alt="" className="w-full h-full" />
                </div>
                <div className="mt-3 text-center sm:mt-5">
                  <Dialog.Title as="h3" className="text-lg leading-6 font-medium mb-6">Continue With Email</Dialog.Title>
                  <div className="mt-2">
                    <div className="mt-1">
                      <input
                        autoFocus
                        type="email"
                        name="email"
                        id="email"
                        className="shadow-sm focus:ring-blue-500 focus:border-blue-500 block w-full sm:text-sm border-gray-300 dark:border-gray-600 rounded-md dark:bg-gray-700"
                        placeholder="you@example.com"
                        onKeyDown={e => {
                          if (e.key === 'Enter') handleSubmit(e)
                        }}
                        ref={inputRef}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-5 sm:mt-6 sm:grid sm:grid-cols-2 sm:gap-3 sm:grid-flow-row-dense">
                <button
                  type="button"
                  className={cn('w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-blue-600 text-base font-medium text-white hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500 dark:focus:ring-offset-gray-800 sm:col-start-2 sm:text-sm', {
                    'opacity-50 cursor-wait': loading,
                  })}
                  onClick={handleSubmit}
                >
                  Submit
                </button>
                <button
                  type="button"
                  className={cn('mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 dark:border-gray-500 dark:text-gray-200 shadow-sm px-4 py-2 bg-white dark:bg-gray-600  text-base font-medium text-gray-700 hover:bg-gray-50 dark:hover:bg-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 dark:focus:ring-offset-gray-800 focus:ring-blue-500 sm:mt-0 sm:col-start-1 sm:text-sm', {
                    'opacity-50 cursor-wait': loading,
                  })}
                  onClick={() => setOpen(false)}
                >
                  Cancel
                </button>
              </div>
            </div>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  )
}
