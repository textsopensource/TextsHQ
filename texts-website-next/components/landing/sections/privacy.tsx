// @ts-expect-error
import Plx from 'react-plx'
import { useEffect, useRef, useState } from 'react'
import cn from 'clsx'
import { privacyParagraph } from '@/lib/parallax-data'

import styles from './privacy.module.css'

const chars = '&#*+%?£@§$'

function genRandomString(length: number) {
  let random_text = ''
  while (random_text.length < length) {
    random_text += chars.charAt(Math.floor(Math.random() * chars.length))
  }

  return random_text
}

const Encrypted = () => {
  const [word, setWord] = useState('encrypted')
  const el = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const handler = () => requestAnimationFrame(() => {
      const screenSpaceOffset = el.current?.getBoundingClientRect().top / window.innerHeight
      if (screenSpaceOffset < 0.7 && screenSpaceOffset > 0) {
        const offset = screenSpaceOffset / 0.7

        const charOffset = word.length - Math.floor(offset * 10)
        const leftWordSegment = genRandomString(charOffset)
        const rightWordSegment = 'encrypted'.slice(charOffset, word.length)
        // console.log(leftWordSegment, rightWordSegment)
        setWord(leftWordSegment + rightWordSegment)
      }
    })

    document.addEventListener('scroll', handler)

    return () => document.removeEventListener('scroll', handler)
  })

  return <strong ref={el} className="tabular-nums font-mono">{word}</strong>
}

export default function PrivacySection() {
  return (
    <section className="flex flex-col items-center h-screen">
      <div className="relative">
        <h2 className="text-5xl font-bold">Privacy First.</h2>
      </div>
      <div className={cn('mt-32 grid grid-cols-3 gap-2 gap-y-20 w-full sm:w-2/3 p-4 sm:p-0 mx-auto relative', styles.privacy)} style={{ gridTemplateColumns: '1fr auto 1fr' }}>
        <svg width="1px" height="431px" viewBox="0 0 1 431" version="1.1" className="absolute left-1/2 z-0 -mt-10">
          <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" opacity="0.550967262" strokeDasharray="30,20" strokeLinecap="round">
            <g transform="translate(-712.000000, -2851.000000)" stroke="#5985FA">
              <line x1="712.5" y1="2851.5" x2="712.5" y2="3287.5" id="Line" />
            </g>
          </g>
        </svg>

        <div className="text-right text-xl">
          <Plx parallaxData={privacyParagraph}>
            <p className="-mt-5 sm:mt-0">Your messages <strong>never</strong> touch the Texts servers.</p>
          </Plx>
        </div>
        <div />
        <div />

        <div />
        <div>
          <img src="/images/lock.png" alt="" className="w-12 h-12 z-10" style={{ transform: 'translateZ(1px)' }} />
        </div>
        <div className="text-xl">
          <Plx parallaxData={privacyParagraph}>
            <p className="flex flex-col sm:block -mt-5 sm:mt-0">They’re <Encrypted /> and sent directly to the platforms they originate from.</p>
          </Plx>
        </div>

        <div className="text-right text-xl">
          <Plx parallaxData={privacyParagraph}>
            <p className="-mt-5 sm:mt-0">Texts makes money through a monthly subscription, <strong>not through your data.</strong></p>
          </Plx>
        </div>
        <div />
        <div />
      </div>
    </section>
  )
}
