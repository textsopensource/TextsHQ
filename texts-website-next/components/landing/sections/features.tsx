// @ts-expect-error
import Plx from 'react-plx'
import cn from 'clsx'
import { featureParallax, featureScreenshotParallax } from '@/lib/parallax-data'
import { useState } from 'react'
import styles from './features.module.css'

const features = [
  { title: '🗂 Archive conversations', description: 'Never miss a message again, but archive chats to hit inbox zero.', screenshot: '/images/features/archive.png' },
  { title: '💬 Mark as unread.', description: 'Keep chats unread by default until you respond to them.', screenshot: '/images/features/unread.png' },
  { title: '⏳ Send later.', description: 'Schedule messages so they get sent at appropriate times when people are active.', screenshot: '/images/features/schedule.png' },
  { title: '🔎 Search all messages.', description: 'Find that link, document, picture or video from forever ago easily.', screenshot: '/images/features/search.png' },
  { title: '⏰ Snooze messages.', description: "Snooze people that you don't want to reply to just yet.", screenshot: '/images/features/snooze.png' },
]

export default function FeaturesSection() {
  // const [screenshotStates, setScreenshotStates] = useState(features.map((_, i) => ({ [`feature-${i}`]: false })))

  return (
    <section className="flex justify-between text-center sm:text-left p-4 sm:p-20 min-h-screen relative" id="features">
      <div className={cn('w-full h-full flex flex-col text-center', styles.features)} style={{ height: 'calc(100% + 100vh)' }}>
        <h2 className="text-5xl font-bold mt-28 sm:mt-0 mb-16">Features</h2>
        <div className="flex justify-center flex-wrap">
          {features.map((feature, i) => (
            <div key={i} className="flex flex-col items-center mb-16 sm:mb-24 mx-6 w-full sm:w-1/4">
              <h4 className="text-2xl font-semibold mb-5">{feature.title}</h4>
              <p className="text-lg">{feature.description}</p>
            </div>
          ))}
        </div>
        {/* {features.map((feature, index) => (
          // eslint-disable-next-line no-nested-ternary
          <Plx
            parallaxData={typeof window !== 'undefined' ? document.body.clientWidth < 420 ? [] : featureParallax : []}
            className="flex flex-col p-4 sm:p-0 pb-12 sm:pb-0 sm:pt-48 w-full sm:w-4/6"
            key={feature.title}
            onPlxStart={() => {
              setScreenshotStates(prev => ({ ...prev, [`feature-${index}`]: true }))
            }}
            onPlxEnd={() => {
              setScreenshotStates(prev => ({ ...prev, [`feature-${index}`]: false }))
            }}
          >
            <h3 className="text-2xl font-semibold mb-6">
              {feature.title}
            </h3>
            <span className="text-lg">
              {feature.description}
            </span>
          </Plx>
        ))} */}
      </div>
      {/* <aside className="hidden sm:flex w-1/2 h-full top-32 flex-col relative sm:sticky" id="features-aside">
        {features.map(({ screenshot }, i) => (
          <img
            src={screenshot}
            className="absolute top-0 left-0 transition-opacity"
            alt=""
            style={{ opacity: screenshotStates[`feature-${i}` as any] ? 1 : 0 }}
          />
        ))}
      </aside> */}
    </section>
  )
}
