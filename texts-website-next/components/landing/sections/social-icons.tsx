import React from 'react'
// @ts-expect-error
import Plx from 'react-plx'
import { heroImage, socialIconsText, socialIconsTextsLogo } from '@/lib/parallax-data'

const ICONS: any[] = []

const PLATFORM_ICONS = [
  [
    'iMessage',
    'Instagram',
  ],
  [
    'Discord',
    'LinkedIn',
    'Messenger',
  ],
  [
    'GoogleChat',
    'Telegram',
    'Signal',
  ],
  [
    'Twitter',
    'WhatsApp',
    'Reddit',
  ],
  [
    'Slack',
    'IRC',
  ],
]
const PLATFORM_LABELS = [
  'iMessage',
  'WhatsApp',
  'Telegram',
  'Signal',
  'Messenger',
  'Twitter',
  'Instagram',
  'LinkedIn',
  'Reddit',
  'Google Chat',
  'Slack',
  'Discord DMs',
]

for (let i = 0; i < PLATFORM_ICONS.length; i++) {
  ICONS.push([])
  const group = PLATFORM_ICONS[i]

  // This is reverse explosion code stolen from react-plx docs, slightly modified for our use case
  for (let j = 0; j < group.length; j++) {
    const top = i < PLATFORM_ICONS.length / 2
    const yFactor = top ? -0.5 : 0.5
    const left = j < group.length / 2
    const xFactor = left ? -3 : 3
    const inside = (i === 1 || i === 2) && (j === 1 || j === 2)
    const scaleFactor = inside ? 0.5 : 1
    const start = inside ? 100 : 250
    const offset = inside ? 100 : 250
    const rotationFactor = Math.random() > 0.5 ? 180 : -180

    const platform = PLATFORM_ICONS[i][j]

    ICONS[i].push({
      data: [
        {
          start: '#social-icons',
          startOffset: -50,
          duration: 800,
          name: 'first',
          easing: 'easeInOut',
          properties: [
            {
              startValue: Math.random() * rotationFactor,
              endValue: 0,
              property: 'rotate',
            },
            {
              startValue: 1 + Math.random() * scaleFactor,
              endValue: 1,
              property: 'scale',
            },
            {
              startValue: (start + Math.random() * offset) * xFactor,
              endValue: 0,
              property: 'translateX',
              unit: '%',
            },
            {
              startValue: ((start + Math.random() * offset) * yFactor),
              endValue: 400,
              property: 'translateY',
              unit: '%',
            },
          ],
        },
      ],
      src: `/images/social-icons/${platform}.svg`,
    })
  }
}

export default function SocialIcons() {
  function renderBoxes() {
    const boxes: any[] = []

    ICONS.forEach((group, index) => {
      group.forEach((box: any, boxIndex: number) => {
        boxes.push(
          <Plx
            key={`${index} ${boxIndex}`}
            className="float-left w-10 h-10 absolute inset-0 mx-auto"
            parallaxData={box.data}
          >
            <img src={box.src} alt="" className="w-10 h-10" />
          </Plx>,
        )
      })
    })

    return boxes
  }

  const copy = (
    <>
      Texts lets you send and receive messages from all major messaging platforms:

      <br />
      <br />

      {PLATFORM_LABELS.map((platform, index) => (
        <div key={index} className="inline-block my-2">
          <img src={`/images/social-icons/${platform.replace(' ', '').replace('DMs', '')}.svg`} alt="" width="24" className="inline-block mr-2" />
          <span className="inline-block mr-3">{platform}</span>
        </div>
      ))}
    </>
  )

  return (
    <section className="w-full px-4 sm:px-20 text-center flex flex-col items-center transform translate-y-8">
      {/* This shoudl be iOS screenshot on mobile version. Hidden for now */}
      <Plx parallaxData={heroImage} className="hidden w-full sm:flex flex-col items-center -mb-24">
        <img src="/a/hero-dark-v2.png" alt="" className="-mt-20 w-full dark-mode-only" style={{ maxWidth: 1012 }} />
        <img src="/a/hero-light-v2.png" alt="" className="-mt-20 w-full light-mode-only" style={{ maxWidth: 1012 }} />
      </Plx>
      <h2 className="text-5xl 2xl:text-6xl font-bold z-10">All In One.</h2>

      <div className="relative hidden sm:flex flex-col items-center h-screen" id="social-icons" style={{ minHeight: 600 }}>
        {renderBoxes()}
        <div className="mt-44 flex flex-col items-center">
          <Plx parallaxData={socialIconsTextsLogo}>
            <img src="/icon.png" alt="" className="w-40" />
          </Plx>
          <Plx parallaxData={socialIconsText}>
            <p className="text-center text-lg w-3/4 m-auto mt-8">{copy}</p>
          </Plx>
        </div>
      </div>

      <div className="flex flex-col items-center text-center sm:hidden p-4 my-10">
        <img src="/images/mobile-social-logos.png" alt="" className="w-full h-auto" />
        <p className="text-center text-lg w-3/4 m-auto my-10">{copy}</p>
      </div>
    </section>
  )
}
