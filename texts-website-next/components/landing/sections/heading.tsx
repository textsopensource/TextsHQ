import cn from 'clsx'
import Signup from '../signup'

import styles from './heading.module.css'

export default function LandingHeading() {
  return (
    <header className={cn('min-h-screen w-screen flex flex-col items-center justify-center text-center relative', styles.heading)}>
      <img src="/images/msg1.png" alt="" className={cn(styles['floaty-msg-1'], "hidden sm:block absolute w-40 2xl:w-60")} style={{ zIndex: -1 }} />
      <img src="/images/msg4.png" alt="" className={cn(styles['floaty-msg-2'], "hidden sm:block absolute w-40 2xl:w-60")} style={{ zIndex: -1 }} />
      <img src="/images/msg3.png" alt="" className={cn(styles['floaty-msg-3'], "hidden sm:block absolute w-48 2xl:w-60")} style={{ zIndex: -1 }} />
      <img src="/images/msg2.png" alt="" className={cn(styles['floaty-msg-4'], "hidden sm:block absolute w-44 2xl:w-60")} style={{ zIndex: -1 }} />
      <h1 className="text-6xl 2xl:text-8xl font-semibold z-10">
        All of your <span className="text-blue-600">messages</span>
        <br />
        <span className="text-6xl sm:text-7xl">In one inbox</span>
      </h1>
      <Signup />
    </header>
  )
}
