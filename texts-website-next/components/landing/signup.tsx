import Link from 'next/link'
import { SyntheticEvent, useCallback, useState } from 'react'
import langEn from '@/lib/lang-en'
import { WaitlistStatus } from '@/lib/constants'
import { fetchJSON } from '@/lib/fetch-json'
import useUser from '@/hooks/useUser'
import EmailModal from './email-modal'

export default function Signup() {
  const user = useUser()
  const [waitlistStatus, setWaitlistStatus] = useState<WaitlistStatus>()
  const [emailSignup, setEmailSignup] = useState(false)
  const [email, setEmail] = useState('')

  const handleSubmit = useCallback(async (event: SyntheticEvent) => {
    event.preventDefault()

    const { status } = await fetchJSON('/api/waitlist-signup', {
      body: JSON.stringify({ email }),
      headers: { 'Content-Type': 'application/json' },
      method: 'POST',
    })

    setWaitlistStatus(status)
  }, [email])

  return (
    <>
      {waitlistStatus ? (
        <span className="text-lg text-center w-1/2 mt-6 bg-gray-100 dark:bg-gray-900 p-3 rounded-xl bg-opacity-75 backdrop-filter backdrop-blur-sm">
          {langEn[waitlistStatus]}
        </span>
      ) : (
        <>
          <a href="/auth/google" className="brand-button text-white py-4 px-8 mt-10">
            {user ? 'Continue to Texts.app' : 'Continue with Google'} →
          </a>
          {!user && (
            <button type="button" onClick={() => setEmailSignup(true)} className="text-blue-900 dark:text-blue-300 opacity-50 mt-2 text-sm">
              Or use email instead →
            </button>
          )}
        </>
      )}
      <EmailModal
        open={emailSignup}
        setOpen={setEmailSignup}
        onSubmit={handleSubmit}
        email={email}
        setEmail={setEmail}
      />
    </>
  )
}
