import React from 'react'

const TWITTER_HANDLE = 'TextsHQ'

const tweetPrompts = [
  `I'm on the @${TWITTER_HANDLE} waitlist, can anyone send me an invite?`,
  `I'm on the @${TWITTER_HANDLE} waitlist, can anyone skip me ahead?`,
  `I'm on the @${TWITTER_HANDLE} waitlist, can someone send me an invite?`,
  `I'm on the @${TWITTER_HANDLE} waitlist, can someone skip me ahead?`,
  "I'm on the Texts.com waitlist, can anyone send me an invite?",
  "I'm on the Texts.com waitlist, can anyone skip me ahead?",
  "I'm on the Texts.com waitlist, can someone send me an invite?",
  "I'm on the Texts.com waitlist, can someone skip me ahead?",
  'Anyone here using Texts.com who can spare an invite?',
  `Can anyone invite me to @${TWITTER_HANDLE}?`,
  `Can anyone invite me to Texts.com / @${TWITTER_HANDLE}?`,
  `Can anyone invite me to Texts.com?\n\ncc @${TWITTER_HANDLE}`,
  `Can someone invite me to @${TWITTER_HANDLE}?`,
  `Can someone invite me to Texts.com / @${TWITTER_HANDLE}?`,
  `Can someone invite me to Texts.com?\n\ncc @${TWITTER_HANDLE}`,
  `Does anyone have a @${TWITTER_HANDLE} invite?`,
  `Does anyone have a Texts.com / @${TWITTER_HANDLE} invite?`,
  'Does anyone have a Texts.com invite?',
  `Does anyone have a Texts.com invite?\n\ncc @${TWITTER_HANDLE}`,
  'Texts.com, can anyone invite me to it?',
  'Texts.com, can someone invite me to it?',
]

 type Props = {
   message: string
 }

export function OnWaitlist({ message }: Props) {
  return (
    <>
      <p className="w-3/4 m-auto mb-6">{message}</p>
      <h4>
        <a href={`https://twitter.com/intent/user?screen_name=${TWITTER_HANDLE}`} className="text-blue-500">
          Follow @{TWITTER_HANDLE} on Twitter for updates &rarr;
        </a>
      </h4>
      <h4>
        <a
          href={`https://twitter.com/intent/tweet?text=${encodeURIComponent(tweetPrompts[~~(tweetPrompts.length * Math.random())])}`}
          className="text-blue-500"
        >
          Get an invite from a Twitter friend &rarr;
        </a>
      </h4>
    </>
  )
}
