import Link from 'next/link'
import type { ReactNode } from 'react'

export function Hero(props: {
  children?: ReactNode
  cta?: ReactNode
  title?: string
  className?: string
}) {
  const { children, cta = null, title = 'Texts', className = '' } = props

  return (
    <section className="flex flex-col items-center justify-center text-center w-full">
      <Link href="/">
        <a>
          <img className="w-32 h-32 mb-8 ml-2" src="/icon.png" alt="Texts" width="64" />
        </a>
      </Link>
      <div className="flex flex-col">
        {title && <h1 className="text-5xl font-bold mb-4">{title}</h1>}
        <span className="text-xl mb-4">{cta}</span>
      </div>
      <div className={className}>
        {children}
      </div>
    </section>
  )
}
