import Link from 'next/link'

import jobs from '@/lib/jobs'

const defaultLinks = [
  { href: '/faq', text: 'FAQ' },
  { href: '/support', text: 'Support' },
]

export default function Header() {
  return (
    <nav className="flex absolute items-center justify-between p-5 w-full top-0 left-0 z-10">
      <Link href="/">
        <a className="flex items-center">
          <img src="/icon.png" alt="Texts" className="w-14" />
          <span className="text-5xl font-bold ml-4">Texts</span>
        </a>
      </Link>
      <ul className="hidden sm:flex items-center">
        {defaultLinks.map(link => (
          <li key={link.href} className="ml-5 font-semibold">
            <Link href={link.href}>
              <a>{link.text}</a>
            </Link>
          </li>
        ))}
        <li className="ml-5 font-semibold relative mr-6">
          <Link href="/jobs">
            <a>We’re Hiring</a>
          </Link>
          <span className="bottom-2 absolute inline-flex items-center px-2.5 py-0.5 rounded-full text-xs font-medium bg-pink-200 text-pink-800 dark:bg-pink-800 dark:text-pink-200">
            {jobs.length}
          </span>
        </li>
      </ul>
    </nav>
  )
}
