import mongoose from 'mongoose'
import config from '@/lib/config'

const { MONGO_CONNECTION_URL } = config
mongoose.connect(MONGO_CONNECTION_URL)

const DeviceSchema = new mongoose.Schema({
  id: String,
}, { strict: false, timestamps: true })

const EventSchema = new mongoose.Schema({
  user: String,
  metadata: {},
}, { strict: false, timestamps: true })

export const Device = mongoose.model('Device', DeviceSchema)
export const Event = mongoose.model('Event', EventSchema)
