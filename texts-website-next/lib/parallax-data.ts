export const heroImage = [
  {
    start: 0,
    end: typeof window !== 'undefined' ? window?.innerHeight * 2 : 700,
    easing: 'easeOut',
    properties: [
      {
        startValue: 0,
        endValue: -400,
        property: 'translateY',
      },
    ],
  },
]

export const socialIconsTextsLogo = [
  {
    start: 'self',
    end: 'self',
    easing: 'easeOut',
    endOffset: 350,
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
    ],
  },
  {
    start: 'self',
    end: 'self',
    easing: 'easeOut',
    endOffset: 500,
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: 'scale',
      },
      {
        startValue: 250,
        endValue: -80,
        property: 'translateY',
      },
    ],
  },
]

export const socialIconsText = [
  {
    start: 'self',
    end: 'self',
    endOffset: 300,
    easing: 'easeOut',
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
      {
        startValue: 300,
        endValue: 0,
        property: 'translateY',
      },
    ],
  },
]

export const privacyParagraph = [
  {
    start: 'self',
    end: 'self',
    endOffset: 200,
    easing: 'easeOut',
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
      {
        startValue: 100,
        endValue: 0,
        property: 'translateY',
      },
    ],
  },
]

export const featureParallax = [
  {
    start: 'self',
    duration: '30vh',
    easing: 'easeOut',
    properties: [
      {
        startValue: -30,
        endValue: -50,
        unit: 'vh',
        property: 'translateY',
      },
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
    ],
  },
  {
    start: 'self',
    startOffset: '50vh',
    duration: 50,
    properties: [
      {
        startValue: 1,
        endValue: 0,
        property: 'opacity',
      },
    ],
  },
]

export const featureScreenshotParallax = [
  {
    start: 'self',
    duration: '30vh',
    easing: 'easeOut',
    properties: [
      {
        startValue: 0,
        endValue: 1,
        property: 'opacity',
      },
    ],
  },
  {
    start: 'self',
    startOffset: '50vh',
    duration: 50,
    properties: [
      {
        startValue: 1,
        endValue: 0,
        property: 'opacity',
      },
    ],
  },
]
