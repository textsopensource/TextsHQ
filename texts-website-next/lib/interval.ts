import 'dotenv/config'
import Interval, { IO } from '@interval/sdk'
import User from '@/lib/entity/User'
import config from '@/lib/config'
import { getConnection } from '@/lib/orm'

const { INTERVAL_API_KEY } = config

const findUserByName = (io: IO, title: string) =>
  io.search(title, {
    renderResult: (u) => ({
      imageUrl: u.avatarURL,
      label: u.fullName,
      value: u.id,
    }),
    async onSearch(query) {
      const users = await (await getConnection())
        .getRepository<User>('User')
        .createQueryBuilder('user')
        .where('"firstName" ilike :query OR "lastName" ilike :query', { query: `%${query}%` })
        .getMany()
      return users
    },
  })

const interval = new Interval({
  apiKey: INTERVAL_API_KEY,
  actions: {
    apply_student_discount: async io => {
      const [user, studentUntil] = await io.group([
        findUserByName(io, 'Find student'),
        io.experimental.date('Student till?'),
      ])
      const returned = await (await getConnection())
        .getRepository<User>('User')
        .update(user.id as string, { studentUntil: studentUntil.jsDate })
      return {
        affectedRows: returned.affected,
        email: user.email,
        newStudentTill: studentUntil.jsDate,
      }
    },
    merge_accounts: async io => {
      const [oldUser, newUser] = await io.group([
        findUserByName(io, 'Old user'),
        findUserByName(io, 'New user'),
      ])

      const returned = await (await getConnection())
        .getRepository<User>('User')
        .update(newUser.id, {
          emails: [...oldUser.emails, ...newUser.emails],
        })

      // RETHINK: Should the old user be marked as deleted?
      const oldUserDeleted = await (await getConnection())
        .getRepository<User>('User')
        .delete(oldUser.id)

      return {
        affectedRows: returned.affected + oldUserDeleted.affected,
        email: newUser.email,
      }
    },
  },
})

interval.listen()
