import { Point } from '@influxdata/influxdb-client'
import { UAParser } from 'ua-parser-js'
import type { Connection } from 'typeorm'

import Event, { EventType } from '@/lib/entity/Event'
import { NextApiRequestWithSession } from '@/lib/session'
import analytic, { AnalyticMeasurement } from '@/lib/analytic'
import { getIP, getIPCountry } from '@/lib/util'
import type User from '@/lib/entity/User'

export default async function loginUser(
  conn: Connection,
  req: NextApiRequestWithSession,
  user: User,
  eventBody = {},
) {
  if (!user) throw new Error("Couldn't find the user")

  const ip = getIP(req)
  const country = getIPCountry(req) ?? '??'
  const ua = req.headers['user-agent']
  const uid = user.id
  req.session.uid = uid
  req.session.originIP = ip
  req.session.ua = ua
  delete req.session.grant

  const UA = UAParser(req.headers['user-agent'])

  analytic.writeAPI.writePoint(
    new Point(AnalyticMeasurement.SiteLogin)
      .tag('browser', UA.browser.name)
      .tag('osName', UA.os.name)
      .tag('arch', UA.cpu.architecture || 'unknown')
      .tag('country', country)
      .booleanField('v', true),
  )

  const event = Event.create(EventType.USER_LOGIN, ip, uid, eventBody)
  await conn
    .getRepository<Event>('Event')
    .save(event)
}
