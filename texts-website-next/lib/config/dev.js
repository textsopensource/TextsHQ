import devSecrets from './_dev.secrets'

const devConfig = {
  SECRETS: {
    SESSION: 'cT%uFp5rpZJ97&Y!cPDKy!F%qTa32imBAimg$*z9ma#&LgfqgMkAW@9PvebitYva',
    STRIPE_WEBHOOK_URL: 'secret',
  },
  STRIPE_PRODUCT_PRICE_IDS: {
    monthly: {
      price_1HAaE5AFF27SH7gUNjODrtmA: { price: '15', desc: 'For personal usage' },
      price_1IFoWtAFF27SH7gUXGX1cUEN: { price: '30', desc: 'For work' },
    },
    yearly: {
      price_1IXZ4jAFF27SH7gU98uP8uoI: { price: '150', desc: 'For personal usage' },
      price_1IXZ4uAFF27SH7gUonIn6eP2: { price: '300', desc: 'For work' },
    },
  },
  STRIPE_PRODUCT_PRICE_IDS_STUDENT: {
    monthly: {
      price_1IeNEFAFF27SH7gUjltjrC84: { price: '5', desc: 'For students' },
    },
  },
  STRIPE_PRODUCT_PRICE_IDS_PPP: {
    monthly: {
      price_1L09l8AFF27SH7gURThMIKr3: { price: '5', desc: 'For personal usage (discounted)' },
    },
  },
  TELEGRAM_BOT: {
    TOKEN: '1813586981:AAFMdcKD9ghR08Tk8MAa1KnbLwHR2dAEGn8',
    FEEDBACK_CHAT_ID: -1001369904384,
    JOBS_CHAT_ID: -793809796,
    USER_UPDATES_CHAT_ID: -1001243865773,
  },
  ...devSecrets,
}

export default devConfig
