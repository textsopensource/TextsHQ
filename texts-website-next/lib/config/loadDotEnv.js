// Loads the relevant .env.{ENV} to process.env depending on IS_DEV
// next.js handles this automatically, import this only in scripts, workers etc.
import dotenv from 'dotenv'
import path from 'path'

import { IS_DEV } from '@/lib/constants'

const ENV_FILE = IS_DEV ? '.env.development' : '.env.production'

const ROOT_PATH = path.resolve(__dirname, '../../')
const ENV_FILE_PATH = path.join(ROOT_PATH, ENV_FILE)

dotenv.config({ path: ENV_FILE_PATH })

console.log(`loading ${ENV_FILE_PATH} from the project root`)
