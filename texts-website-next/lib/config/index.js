import { IS_DEV } from '@/lib/constants'
import defaultConfig from '@/lib/config/default'
import devConfig from '@/lib/config/dev'
import prodConfig from '@/lib/config/prod'

const config = {
  ...defaultConfig,
  ...(IS_DEV ? devConfig : prodConfig),
}

export default config
