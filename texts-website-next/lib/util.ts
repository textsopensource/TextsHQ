import { memoize } from 'lodash'
import type { IncomingMessage } from 'http'

import User from '@/lib/entity/User'
import { getConnection } from '@/lib/orm'

export const getIP = (request: IncomingMessage) =>
  String(request.headers['cf-connecting-ip'] ?? '')

export const getIPCountry = (request: IncomingMessage) =>
  String(request.headers['cf-ipcountry'] ?? '')

// TODO: Check behavior of memoize against async fn.
export const getUserFromDeviceID = memoize(async (deviceID: string) => {
  const user = await (await getConnection())
    .getRepository<User>('User')
    .createQueryBuilder('user')
    .where('user.id in (select uid from app_session where "deviceID" = :deviceID)', { deviceID })
    .getOne()

  return user
})

enum UserFlag {
  SHOW_REONBOARDING_PROMPT = 'show_reonboarding_prompt',
  HIDE_PAYMENT_BADGE = 'hide_payment_badge',
}
// sync with texts-app-desktop/src/common/types.ts
type LoggedInUser = {
  uid: string
  fullName: string
  emails: string[]
  paymentStatus: 'free' | 'paused' | 'trial' | 'premium' | 'expired'
  availableInvites: number
  flags?: UserFlag[]
}

export const getLoggedInUser = ({ id: uid, fullName, emails, paymentStatus, availableInvites, ageInDays }: User): LoggedInUser => ({
  uid,
  fullName,
  emails,
  paymentStatus,
  availableInvites,
})

export const isValidOS = (os: string) => ['macos', 'windows', 'linux'].includes(os)

export const isValidChannel = (channel: string) => ['stable', 'beta', 'nightly'].includes(channel)
