import type Mail from 'nodemailer/lib/mailer'

import config from '@/lib/config'
import type User from '@/lib/entity/User'

const { EMAIL_SIGNATURE, EMAIL_BCC, SUPPORT_EMAIL } = config

const defaultFrom = 'Kishan Bagaria <kishan@texts.com>'

export const getUserText = (user: User) => (user
  ? `${user.fullName} ${user.emails[0]} – ${user.paymentStatus} – since ${user.createdAt.toDateString()} –`
  : 'unknown user')

const adminEmails = {
  welcomeAdminOnly: (user: User): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts sign up',
    text: `${user.fullName} ${user.emails[0]} has signed up`,
  }),
  userUsedAfterDays: (user: User, days: number): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts user used app after days',
    text: `${getUserText(user)} has used app after ${days} days`,
  }),
  userHasNotUsedInDays: (user: User, days: number): Mail.Options => ({
    to: EMAIL_BCC,
    subject: 'Texts user has not used app in days',
    text: `${getUserText(user)} has not used app for ${days} days`,
  }),
  userBuildExpired: (user: User, details: string): Mail.Options => ({
    to: EMAIL_BCC,
    subject: "Texts user's build was expired",
    text: `${getUserText(user)} ${details}`,
  }),
  userSentFeedback: (user: User, text: string): Mail.Options => ({
    to: SUPPORT_EMAIL,
    replyTo: `${user.fullName} <${user.emails[0]}>`,
    subject: `Texts feedback from ${user.fullName}`,
    text,
  }),
  userSentProblemReport: (user: User, text: string): Mail.Options => ({
    to: SUPPORT_EMAIL,
    replyTo: `${user.fullName} <${user.emails[0]}>`,
    subject: `Texts problem report from ${user.fullName}`,
    text,
  }),
  jobsSubmission: ({
    link,
    headers,
  }: {
    link: string
    headers: string
  }): Mail.Options => ({
    to: SUPPORT_EMAIL,
    subject: 'Texts link submission',
    text: `${link}\n\n${headers}`,
  }),
}

const allEmails = {
  ...adminEmails,
  welcome: (user: User, usesSuperhuman = false): Mail.Options => ({
    to: `${user.fullName} <${user.emails[0]}>`,
    from: defaultFrom,
    bcc: EMAIL_BCC,
    subject: 'Texts',
    text: `hey ${user.firstName.toLowerCase()}, excited for you to use texts and become more productive and focused.
${usesSuperhuman
      ? "\ni see you're on superhuman, so you'll be familiar with ⌘ k and getting to inbox zero. hotkeys in texts are prefixed with ⌘ and ctrl.\n"
      : ''}
ps: we love hearing from users, hit me up with feedback or questions anytime.

${EMAIL_SIGNATURE}`,
  }),
  onWaitlist: (email: string, firstName?: string): Mail.Options => ({
    to: `${firstName || ''} <${email}>`,
    from: 'Kishan Bagaria <kishanbagaria+waitlist@texts.com>',
    subject: 'Texts',
    text: `Hey${
      firstName ? ` ${firstName}` : ''
    }, thanks for signing up on the waitlist! We're addressing feedback from beta users atm to ensure a smooth experience for you.

If you'd like to skip ahead and use the beta desktop app, you can get an invite from a friend who's already using it.

${EMAIL_SIGNATURE}`,
  }),
  pausedSubscriptionAfterInactivity: (user: User, inactivityDays?: number): Mail.Options => ({
    to: `${user.fullName} <${user.emails[0]}>`,
    from: defaultFrom,
    bcc: EMAIL_BCC,
    subject: 'Texts subscription paused',
    text: `hey ${user.firstName.toLowerCase()}, noticed you didn't use texts.app in the last few weeks so we paused your subscription.

how can we make texts better for you and have you as a more active user?

${EMAIL_SIGNATURE}`,
  }),
}

export default allEmails
