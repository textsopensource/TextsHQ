import getConfig from 'next/config'

const { serverRuntimeConfig } = getConfig()

export const SITE_ROOT = serverRuntimeConfig.PROJECT_ROOT

export const STATIC_WEB_PATH = 'https://static.texts.com/'

export const STATIC_SECRET_WEB_PATH = `${STATIC_WEB_PATH}secret-c2ijv2cjpfpd2yyx/`
