import { Queue, QueueScheduler } from 'bullmq'
import redis from '@/lib/redis'

export enum Queues {
  PROCESS_INACTIVE_USERS = "PROCESS_INACTIVE_USERS", // Finds the inactive users
  PAUSE_SUBSCRIPTION_FOR_USER = "PAUSE_SUBSCRIPTION_FOR_USER", // Processes a single inactive user
}

export const queueConfig = {
  connection: redis
}

new QueueScheduler(Queues.PROCESS_INACTIVE_USERS, queueConfig) // this is needed for delayed jobs

export type ProcessInactiveUsersJobData = {
  inactiveSince?: number // expects days
}

export const processInactiveUsersQueue = new Queue<ProcessInactiveUsersJobData>(Queues.PROCESS_INACTIVE_USERS, queueConfig)

processInactiveUsersQueue.add(
  'cron',
  null,
  {
    repeat: {
      cron: '0 0 * * *', // every day at 00:00
    },
  },
)

export type PauseSubscriptionJobData = { userId: string, inactiveSince?: number}
export const pauseSubscriptionForUserQueue = new Queue<PauseSubscriptionJobData>(Queues.PAUSE_SUBSCRIPTION_FOR_USER, queueConfig)
