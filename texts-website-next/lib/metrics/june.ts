import { promisify } from 'util'
// @ts-expect-error
import Analytics from 'analytics-node'
import config from '@/lib/config'
import { IS_DEV } from '@/lib/constants'

export interface Identity {
  userId?: string | number
  anonymousId?: string | number
}

export enum JuneEvent {
  UserSessionCreate = 'user.session.create',
  UserSessionValidate = 'user.session.validate',
  UserInviteCreate = 'user.invite.create',
  UserPlanUpgrade = 'user.plan.upgrade',
  UserPlanDowngrade = 'user.plan.downgrade',
  UserTrialStart = 'user.trial.start',
  UserTrialEnd = 'user.trial.end',
  UserSubscriptionPause = 'user.subscription.pause',
  UserSubscriptionResume = 'user.subscription.resume',
  WaitListSignup = 'waitlist.signup',
  UserAccountAdd = 'user.account.add',
  UserAccountDisable = 'user.account.disable',
  UserAccountEnable = 'user.account.enable',
  UserAccountReauthenticate = 'user.account.reauthenticate',
  UserAccountRemove = 'user.account.remove',
}

interface Integrations {
  [integration_name: string]: IntegrationValue
}

type IntegrationValue = boolean | { [integration_key: string]: any }

class JuneAnalytic {
  protected client: Analytics

  constructor(writeKey: string, isDev: boolean) {
    this.client = new Analytics(writeKey, {
      flushAt: isDev ? 1 : 20,
    })

    this.client.identifyAsync = promisify(this.client.identify).bind(this.client)
    this.client.trackAsync = promisify(this.client.track).bind(this.client)
  }

  public identify(payload: Identity & {
    traits?: any
    timestamp?: Date | undefined
    context?: any
    integrations?: Integrations | undefined
  }): Promise<void> {
    return this.client.identifyAsync(payload)
  }

  public track(payload: Identity & {
    event: JuneEvent
    properties?: any
    timestamp?: Date | undefined
    context?: any
    integrations?: Integrations | undefined
  }): Promise<void> {
    return this.client.trackAsync(payload)
  }
}

export default new JuneAnalytic(config.SEGMENT_WRITE_KEY, IS_DEV)
