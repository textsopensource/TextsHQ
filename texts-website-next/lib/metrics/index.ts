import semver from 'semver'
import { memoize } from 'lodash'
import { Point } from '@influxdata/influxdb-client'

import analytic from '@/lib/analytic'
import { Device, Event } from '@/lib/mongoose'
import { AppEvent } from '@/lib/types'
import sendEventToSlack from '@/lib/send-slack-event'
import juneAnalytic, { JuneEvent } from '@/lib/metrics/june'
import { getUserFromDeviceID } from '@/lib/util'

function logToInflux(body: AppEvent) {
  const point = new Point(body.type)
    .tag('appVersion', body.metadata.appVersion)
    .tag('screenSize', body.metadata.screenSize)
    .tag('osName', body.metadata.os.name)
    .tag('osVersion', body.metadata.os.version)

  let a: Point

  switch (body.type) {
    case 'memory':
      a = point
        .tag('processType', body.eventData.processType)
        .intField('uptimeMinutes', body.eventData.uptimeMinutes)
      if (body.eventData.privateMb) {
        a = a.intField('privateMb', body.eventData.privateMb)
      }
      if (body.eventData.sharedMb) {
        a = a.intField('sharedMb', body.eventData.sharedMb)
      }
      if (body.eventData.rssMb) {
        a = a.intField('rssMb', body.eventData.rssMb ?? null)
      }
      break

    case 'timing':
      a = point
        .tag('category', body.eventData.category)
        .tag('value', body.eventData.value)
        .intField('timing', isNaN(body.eventData.time) ? 0 : body.eventData.time)
      break

    case 'account-add':
    case 'account-disable':
    case 'account-enable':
    case 'account-reauthenticate':
    case 'account-remove':
      a = point
        .tag('platformName', body.eventData ? body.eventData.platformName : 'unknown')
        .booleanField('v', true)
      break

    case 'app-relaunched':
      a = point
        .tag('reason', body.eventData.reason)
        .booleanField('v', true)
      break

    case 'accounts-refreshed':
      a = point
        .tag('trigger', body.eventData.trigger)
        .booleanField('v', true)
      break

    case 'app-reloaded':
      a = point
        .intField('times', body.eventData.times | 0) // Force to int, electron app is type juggling 0 & 1 to false & true in payload
      break

    case 'connection-state':
      a = point
        .tag('status', body.eventData.status)
        .booleanField('v', true)
      break

    case 'error':
      a = point
        .tag('context', body.eventData.context)
        .tag('message', body.eventData.message)
        .booleanField('v', true)
      break

    case 'macos-contacts-loaded':
      a = point
        .floatField('sizeMB', body.eventData.sizeMB ?? 0.0)
      break

    case 'message-not-sent':
      a = point
        .tag('platformName', body.eventData.platformName ?? 'unknown')
        .stringField('reason', body.eventData.reason)
        .booleanField('v', true)
      break

    case 'platform-event':
      a = point
        .tag('csrutilStatus', body.eventData.csrutilStatus)
        .booleanField('enabled', body.eventData.enabled)
      break

    case 'relayer-transport':
      a = point
        .tag('message', body.eventData.message)
        .tag('methodName', body.eventData.methodName)
        .booleanField('v', true)
      break

    case 'system-stats':
      a = point
        .tag('arch', body.eventData.arch)
        .intField('memoryRSS', body.eventData.memoryUsage.rss)
        .intField('memoryHeapTotal', body.eventData.memoryUsage.heapTotal)
        .intField('memoryHeapUsed', body.eventData.memoryUsage.heapUsed)
        .intField('memoryExternal', body.eventData.memoryUsage.external)
        .intField('memoryArrayBuffers', body.eventData.memoryUsage.arrayBuffers)
        .intField('cpuUser', body.eventData.cpuUsage.user)
        .intField('cpuSystem', body.eventData.cpuUsage.system)
      break

    case 'state-sync':
      a = point
        .tag('type', body.eventData.type)
        .stringField('reason', body.eventData.reason)

      // (body.eventData.eventDetails as string[]).forEach((d, index) => {
      //   a = a.stringField(`eventDetails[${index}]`, d)
      // })
      break

    case 'update-check':
      // Version is already in metadata, they share the same version
      break

    default:
      console.log('Unhandled event: ', body.type)
  }

  analytic.writeAPI.writePoint(a)
}

const deviceExists = memoize(async deviceId => !!await Device.findOne({ deviceId }))

async function logToMongo(body: AppEvent) {
  const event = new Event(body)
  const { deviceId } = body
  if (deviceId) {
    // If this is a first event we ever see for a given device, create one in mongo
    const existingDevice = await deviceExists(deviceId)
    if (!existingDevice) {
      const device = new Device({ deviceId })
      await device.save()
    }
  }
  await event.save()
}

function logToSlack(body: AppEvent) {
  switch (body.type) {
    case 'message-not-sent':
    // case 'state-sync':
      sendEventToSlack(body)
  }
}

async function logToJune(body: AppEvent) {
  const { type, eventData } = body

  let event: JuneEvent

  // Perhaps one-day we could consolidate all event types (mongodb, june, postgres) event into one type
  switch (type) {
    case 'account-add':
      event = JuneEvent.UserAccountAdd
      break
    case 'account-remove':
      event = JuneEvent.UserAccountRemove
      break
    case 'account-enable':
      event = JuneEvent.UserAccountEnable
      break
    case 'account-disable':
      event = JuneEvent.UserAccountDisable
      break
    case 'account-reauthenticate':
      event = JuneEvent.UserAccountReauthenticate
      break
    default:
      return
  }

  const platformName = eventData ? eventData.platformName : 'unknown'

  const user = await getUserFromDeviceID(body.deviceId)

  if (!user) return

  juneAnalytic.track({
    event,
    userId: user.id,
    properties: {
      platformName,
      deviceID: body.deviceId,
      viewportSize: body.metadata?.viewportSize,
      screenSize: body.metadata?.screenSize,
      osName: body.metadata?.os.name,
      osVersion: body.metadata?.os.version,
      appVersion: body.metadata?.appVersion,
    }
  })
}

export default async function logEvent(body: AppEvent, ip: string) {
  if (!body) return

  // TODO: In the future, should this endpoint be authenticated? To at least valid sessions?

  const version = body.metadata?.appVersion || '0.0.0'

  // Drop events below version when new format was introduced
  if (!semver.satisfies(version, '>0.34.1')) {
    console.log('Dropping event', `"${body.type}"`, 'due to app version', version)
    return { rejected: true }
  }

  logToInflux(body)
  logToMongo(body)
  logToSlack(body)
  logToJune(body)

  return { ok: true }
}
