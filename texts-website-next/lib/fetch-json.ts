import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'

type FetchError = Error & {
  response?: any
  data?: any
}

export async function fetchJSON(input: RequestInfo, init?: RequestInit) {
  const response = await fetch(input, init)
  const data = await response.json()

  if (response.ok) return data
  if (response.status === 401) {
    window.location.href = DEFAULT_UNAUTH_REDIRECT + `?redirectTo=${encodeURIComponent(window.location.href)}`
    return
  }

  const error: FetchError = new Error(response.statusText)
  error.response = response
  error.data = data || { message: error.message }
  console.error({ error })
  throw error
}
