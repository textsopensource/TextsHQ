import { LessThanOrEqual, Raw } from 'typeorm'
import { subDays } from 'date-fns'
import sendEmail from '@/lib/emails'
import { getConnection, getRepos } from '@/lib/orm'
import allEmails from '@/lib/all-emails'
import User from '@/lib/entity/User'

export const DEFAULT_INACTIVE_SINCE = 45

export async function getInactiveUsersWithActiveSubscriptions(inactiveSince = DEFAULT_INACTIVE_SINCE): Promise<Pick<User, 'id'>[]> {
  const log = (...args: any[]) => console.log('[getInactiveUsersWithActiveSubscriptions]', ...args)

  const {userRepo} = getRepos(await getConnection())

  const lastActive = subDays(new Date(), inactiveSince)

  log(`finding users who were last active before ${lastActive}`)

  const users = await userRepo.find({
    select: ['id'],
    where: {
      lastActive: LessThanOrEqual(lastActive),
      // this would miss users whose subscription state is not synced with stripe (missed webhooks) but that's an edge case
      subscription: Raw((alias) => `
        ${alias} is not null
        and ${alias} ->> 'id' is not null
        and ${alias} ->> 'status' = 'active'
        and ${alias} ->> 'pause_collection' is null
      `)
    }
  })

  log(`found ${users.length} users`)

  return users
}

export async function pauseSubscription(userId: string, inactiveSince = DEFAULT_INACTIVE_SINCE) {
  if (!userId) {
    throw new Error("User ID is required")
  }

  const log = (...args: any[]) => console.log(`[pauseSubscription][user:${userId}]`, ...args)

  const {userRepo} = getRepos(await getConnection())
  const user = await userRepo.findOne({ id: userId })

  log(`pausing subscription for ${user.fullName} [${inactiveSince} days]`)
  await user.pauseStripeSubscription()

  await userRepo.save(user)

  const email = allEmails.pausedSubscriptionAfterInactivity(user, inactiveSince)
  log("sending paused due to inactivity email to", email.to)
  await sendEmail(email) // we can do this in the webhook as well but it's handy to use `inactiveSince` from here

  log('done')

  return true
}
