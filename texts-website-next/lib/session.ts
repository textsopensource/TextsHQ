import connectRedis from 'connect-redis'
import cookie from 'cookie'
import session from 'express-session'
import { Connection } from 'typeorm'

import type { NextApiRequest, NextApiResponse } from 'next'

import config from '@/lib/config'
import redis from '@/lib/redis'
import AppSession from '@/lib/entity/AppSession'

const { SECRETS, SESSION_COOKIE_MAX_AGE } = config

const RedisStore = connectRedis(session)

export default session({
  cookie: { maxAge: SESSION_COOKIE_MAX_AGE },
  name: 'sid',
  resave: false,
  saveUninitialized: true,
  secret: SECRETS.SESSION,
  store: new RedisStore({ client: redis }),
})

export type Session = session.Session & {
  uid: string
  grant?: any
  inviteID?: string
  originIP?: string
  redirectTo?: string
  ua?: string
}

export type NextApiRequestWithSession = NextApiRequest & {
  session?: Session
  sessionID?: string
  sessionStore?: session.Store
}

export function destroySession(
  req: NextApiRequestWithSession,
  res: NextApiResponse,
) {
  const { headers, session: localSession } = req

  return new Promise((resolve, reject) => {
    try {
      localSession.destroy(err => {
        if (err) {
          return reject(err)
        }

        res.setHeader('Set-Cookie', [
          cookie.serialize('sid', '', {
            domain: headers.host.split(':')[0],
            expires: new Date(0),
            httpOnly: true,
            path: '/',
            secure: true,
          }),
        ])

        resolve(true)
      })
    } catch (err) {
      reject(err)
    }
  })
}

export async function upsertSession(conn: Connection, sid: string, uid: string, ip: string, appVersion: string, deviceID: string, os: string) {
  const appSession = AppSession.create(sid, uid, ip, appVersion, deviceID, os)
  await conn
    .getRepository<AppSession>('AppSession')
    .save(appSession)
}
