// Helper method to wait for a middleware to execute before continuing

import { IncomingMessage, OutgoingMessage } from 'http'

// And to throw an error when an error happens in a middleware
const runMiddleware = (
  req: IncomingMessage,
  res: OutgoingMessage,
  fn: Function,
) =>
  new Promise((resolve, reject) => {
    fn(req, res, (result: any) => {
      if (result instanceof Error) {
        return reject(result)
      }

      return resolve(result)
    })
  })

export default runMiddleware
