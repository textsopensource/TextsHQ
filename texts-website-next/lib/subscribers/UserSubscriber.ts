import { EntitySubscriberInterface, EventSubscriber, UpdateEvent } from 'typeorm'
import User from '@/lib/entity/User'
import * as tgBot from '@/lib/telegram-bot'
import juneAnalytic, { JuneEvent } from '@/lib/metrics/june'

async function onPaymentStatusChange(user: User, oldPaymentStatus: string, newPaymentStatus: string) {
  const promises: Promise<any>[] = [
    tgBot.userPaymentStatusChanged(user, newPaymentStatus)
  ]

  let juneEvent: JuneEvent
  if (oldPaymentStatus === 'free' && newPaymentStatus === 'trial') {
    juneEvent = JuneEvent.UserTrialStart
  } else if (oldPaymentStatus === 'trial' && newPaymentStatus === 'free') {
    juneEvent = JuneEvent.UserTrialEnd
  } else if ((oldPaymentStatus === 'free' || oldPaymentStatus === 'trial') && newPaymentStatus === 'premium') {
    juneEvent = JuneEvent.UserPlanUpgrade
  } else if (oldPaymentStatus === 'premium' && newPaymentStatus === 'free') {
    juneEvent = JuneEvent.UserPlanDowngrade
  } else if (oldPaymentStatus === 'premium' && newPaymentStatus === 'paused') {
    juneEvent = JuneEvent.UserSubscriptionPause
  } else if (oldPaymentStatus === 'paused' && newPaymentStatus === 'premium') {
    juneEvent = JuneEvent.UserSubscriptionResume
  }

  if (juneEvent) {
    promises.push(juneAnalytic.track({
      event: juneEvent,
      userId: user.id,
    }))
  }

  await Promise.all(promises)
}

@EventSubscriber()
export default class UserSubscriber implements EntitySubscriberInterface<User> {
    listenTo() {
        return User
    }

    async afterUpdate(event: UpdateEvent<User>): Promise<any> {
      const before = event.databaseEntity
      const after = event.entity
      const promises: Promise<any>[] = []

      if (before.paymentStatus !== after.paymentStatus) {
        promises.push(onPaymentStatusChange(before, before.paymentStatus, after.paymentStatus))
      }

      try {
        await Promise.all(promises)
      } catch (error) {
        console.error('[ERROR]', 'User/afterUpdate failed', error)
      }
    }
}
