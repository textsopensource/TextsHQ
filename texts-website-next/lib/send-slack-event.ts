import { Block, KnownBlock, WebClient } from '@slack/web-api'
import config from '@/lib/config'

import { getUserFromDeviceID } from '@/lib/util'
import { AppEvent } from '@/lib/types'

const { BOT_TOKEN, EVENTS_CHANNEL_ID } = config.SLACK

const getFields = (event: AppEvent) => {
  const { eventData = {}, type } = event

  switch (type) {
    case 'message-not-sent':
      return [
        'platformName',
        'reason',
        'attachmentCount',
        'textLength',
        'quoted',
        'couldHaveLink',
        'couldHaveMention',
        'participantCount',
      ].map(key => ({
        type: 'mrkdwn',
        text: `*${key}:* ${eventData[key]}`,
      }))

    case 'state-sync':
      return [
        {
          type: 'mrkdwn',
          text: `*Type:*\n${eventData.type}`,
        },
        {
          type: 'mrkdwn',
          text: `*Platform:*\n${eventData.platformName}`,
        },
        {
          type: 'mrkdwn',
          text: `*Reason:*\n${eventData.reason}`,
        },
        {
          type: 'mrkdwn',
          text: `*Details:*\n${eventData.eventDetails?.join(', ')}`,
        },
      ]

    default:
      return []
  }
}

export default async function sendEventToSlack(event: AppEvent) {
  const slackClient = new WebClient(BOT_TOKEN)

  const { type } = event
  const user = await getUserFromDeviceID(event.deviceId)
  const name = user?.fullName || event.deviceId

  const blocks: (Block | KnownBlock)[] = [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `\`${type}\` event | ${name} | ${user.emails.join(' ')} | ${user?.paymentStatus}`,
      },
    },
    {
      type: 'section',
      fields: getFields(event),
    },
    {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `Texts v${event.metadata.appVersion} on ${event.metadata.os.name} ${event.metadata.os.version} ${event.metadata.arch || '(unknown arch)'} (${event.metadata.isWKWV ? ':appleinc: WKWV' : ':electron: Electron'})`,
        },
      ],
    },
  ].filter(Boolean)

  await slackClient.chat.postMessage({
    channel: EVENTS_CHANNEL_ID,
    text: `New \`${type}\` event`,
    blocks,
    username: name || 'Texts Event',
    icon_url: user?.avatarURL || null,
  })
}
