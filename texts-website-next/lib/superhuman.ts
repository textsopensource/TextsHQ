import got from 'got'
import { memoize } from 'lodash'

import config from '@/lib/config'

const { SUPERHUMAN_AUTH_TOKEN } = config

type SHProfileResponse = { code: number, detail: string } & {
  // ...
  memberSince?: string
  canBeReferred: boolean
  canBeAddedToTeam: boolean
  canRefer: boolean
}

// we could persist this to redis or some other cache but atm this is only used once when the user signs up
export const isUserOnSuperhuman = memoize(
  async (email: string, name: string) => {
    const res = await got<SHProfileResponse>(
      'https://mail.superhuman.com/~backend/v2/profile',
      {
        searchParams: { email, name },
        responseType: 'json',
        headers: {
          Authorization: SUPERHUMAN_AUTH_TOKEN,
        },
        throwHttpErrors: false,
      },
    )
    if (res.body.code) {
      console.error(
        'superhuman profile error:',
        res.body.code,
        res.body.detail,
      )
      return
    }
    const isSHUser = !!res.body.memberSince
    console.log(email, name, 'using superhuman:', isSHUser, res.body)
    return isSHUser
  },
  (email: any) => email,
)
