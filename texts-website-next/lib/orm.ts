import { memoize } from 'lodash'
import { Connection, ConnectionOptions, getConnectionManager } from 'typeorm'
import type { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

import User from '@/lib/entity/User'
import Event from '@/lib/entity/Event'
import AppSession from '@/lib/entity/AppSession'
import Invite from '@/lib/entity/Invite'
import Waitlist from '@/lib/entity/Waitlist'

import UserSubscriber from '@/lib/subscribers/UserSubscriber'

import config from '@/lib/config'
import { IS_DEV } from '@/lib/constants'

const { TYPEORM } = config
const ALL_ENTITIES = [User, Event, Invite, AppSession, Waitlist]
const ALL_SUBSCRIBERS = [UserSubscriber]

class Database {
  private connectionManager = getConnectionManager()

  private static entitiesChanged(prevEntities: any[], newEntities: any[]): boolean {
    if (prevEntities.length !== newEntities.length) return true

    for (let i = 0; i < prevEntities.length; i++) {
      if (prevEntities[i] !== newEntities[i]) return true
    }

    return false
  }

  private static async updateConnectionEntities(
    connection: Connection,
    entities: any[],
  ) {
    const conn = connection

    if (!Database.entitiesChanged(conn.options.entities, entities)) return

    // @ts-expect-error
    conn.options.entities = entities

    // @ts-expect-error
    conn.buildMetadatas()

    if (conn.options.synchronize) {
      await conn.synchronize()
    }
  }

  public async getConnection(name = 'default'): Promise<Connection> {
    const options = {
      ...TYPEORM,
      entities: ALL_ENTITIES,
      subscribers: ALL_SUBSCRIBERS,
      synchronize: true,
      logging: IS_DEV,
    }

    if (this.connectionManager.has(name)) {
      const connection = this.connectionManager.get(name)

      if (!connection.isConnected) {
        await connection.connect()
      }

      if (!IS_DEV) {
        await Database.updateConnectionEntities(connection, options.entities)
      }

      return connection
    }

    return this.connectionManager.create({
      name,
      ...options,
    } as PostgresConnectionOptions).connect()
  }
}

const database = new Database()

export const getConnection = memoize(() => database.getConnection())

export function getRepos(conn: Connection) {
  return {
    userRepo: conn.getRepository<User>('User'),
    eventRepo: conn.getRepository<Event>('Event'),
    inviteRepo: conn.getRepository<Invite>('Invite'),
    sessionRepo: conn.getRepository<AppSession>('AppSession'),
    waitlistRepo: conn.getRepository<Waitlist>('Waitlist'),
  }
}
