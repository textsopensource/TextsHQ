import got from 'got'
import { STATIC_SECRET_WEB_PATH } from '@/lib/server-constants'
import redis from '@/lib/redis'

const redisKey = 'STATIC_SECRET_CACHE'

export async function getStaticSecretAsset(filePath: string) {
  const cached = await redis.hget(redisKey, filePath)
  if (cached) return cached
  const txt = await got(`${STATIC_SECRET_WEB_PATH}${filePath}`, { resolveBodyOnly: true, retry: 5 })
  redis.hset(redisKey, filePath, txt)
  return txt
}

export async function clearStaticSecretCache() {
  await redis.del(redisKey)
}
