export type EventType =
  'account-add' |
  'account-disable' |
  'account-enable' |
  'account-reauthenticate' |
  'account-remove' |
  'accounts-refreshed' |
  'app-relaunched' |
  'app-reloaded' |
  'connection-state' |
  'error' |
  'macos-contacts-loaded' |
  'memory' |
  'message-errored' |
  'message-not-sent' |
  'platform-event' |
  'relayer-transport' |
  'state-sync' |
  'system-stats' |
  'timing' |
  'update-check'

export type AppEvent = {
  type: EventType
  deviceId?: string
  metadata?: {
    os: {
      name: NodeJS.Platform
      version: string
    }
    appVersion: string
    arch: string
    viewportSize: string
    screenSize: string
    isWKWV: boolean
  }
  eventData?: any
}
