const en = {
  NOT_AUTHENTICATED: "You're not logged in.",
  ACCOUNT_UNACTIVATED:
    "Your account isn't activated.",
  LOGIN_FAIL_NO_UID:
    'Authentication failed. The link must be expired. Please close the existing browser tab and try again.',
  ACCOUNT_NOT_FOUND:
    'Account not found. Contact support@texts.com for more help.',
  GOOGLE_EMAIL_NOT_VERIFIED:
    "Your email hasn't been verified. Please try login with another platform, verify your email with Google, or contact support@texts.com for more help.",
  GOOGLE_NO_EMAIL:
    "Google isn't returning any email addressess at the moment. Please try login with another platform or contact support@texts.com",
  INVITE_ALREADY_USED:
    'This invite has already been used. Please ask for a new one!',
  INVITE_NOT_FOUND:
    "This invite wasn't found. Please check the link or ask for a new one!",
  NO_INVITELESS_REGISTRATION:
    "You don't have a registered user account with email {{email}}. Try logging in with another account or contact support@texts.com",
  ADDED_TO_WAITLIST:
    "You've been added to the waitlist! We'll invite you soon as we can. You can also skip the waitlist by getting an invite from a friend.",
  ALREADY_ON_WAITLIST:
    "You are on the waitlist. We'll invite you soon as we can. You can also skip the waitlist by getting an invite from a friend.",
}

export default en
