const jobs = [
  {
    title: 'Full-stack engineer',
    desc: `Stack: React.js, TypeScript, Node.js
Experience: mid-level, senior or more`,
  },
  {
    title: 'iOS engineer',
    desc: `Stack: Swift, SwiftUI
Experience: mid-level, senior or more`,
  },
  {
    title: 'Backend engineer',
    desc: `Stack: TypeScript, Node.js, Postgres, Rust
Experience: mid-level, senior or more`,
  },
  {
    title: 'Reverse engineer',
    desc: `Tools: Ghidra, IDA, or Frida
Experience: mid-level, senior or more`,
  },
]

export default jobs
