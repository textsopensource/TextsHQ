import { Entity, PrimaryColumn, Column, CreateDateColumn } from 'typeorm'
import { ulid } from 'ulid'

export enum EventType {
  USER_CREATE = 'user.create',
  USER_LOGIN = 'user.login',
  USER_LOGOUT = 'user.logout',
  INSTALL_SCRIPT_FETCH = 'install_script.fetch',
  DOWNLOAD_BINARY = 'download.binary',
  BUILD_EXPIRED = 'build.expired',
}

@Entity()
export default class Event {
  @PrimaryColumn() id: string

  @Column() uid: string

  @CreateDateColumn() createdAt: Date

  @Column('varchar') type: EventType

  @Column('jsonb') body: any

  @Column() ip: string

  assignRandomID() {
    this.id = 'evt_' + ulid()
  }

  static create(type: EventType, ip: string, uid: string, body: any) {
    const event = new Event()
    event.assignRandomID()
    event.type = type
    event.ip = ip || '0.0.0.0'
    event.uid = uid
    event.body = body
    return event
  }
}
