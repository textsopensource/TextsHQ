import crypto from 'crypto'
import { ulid } from 'ulid'
import { sub } from 'date-fns'
import {
  Entity,
  PrimaryColumn,
  Index,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeUpdate,
  BeforeInsert,
  AfterInsert,
  AfterUpdate,
} from 'typeorm'
import Stripe from 'stripe'

import stripe from '@/lib/stripe'
import juneAnalytic from '@/lib/metrics/june'

@Entity()
export default class User {
  @PrimaryColumn()
  id: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Index()
  @Column('text', { array: true })
  emails: string[]

  @Column({ nullable: true })
  firstName: string

  @Column({ nullable: true })
  middleName: string

  @Column({ nullable: true })
  lastName: string

  @Column({ nullable: true })
  lastActive: Date

  /** used for student pricing */
  @Column({ nullable: true })
  studentUntil: Date

  @Column({ default: 0 })
  sessionValidateCount: number

  @Column({ default: 0 })
  availableInvites: number

  @Column({ nullable: true })
  twitterAuthID: string

  @Column('jsonb', { nullable: true })
  twitterInfo: any

  @Column({ nullable: true })
  googleAuthID: string

  @Column('jsonb', { nullable: true })
  googleInfo: any

  @Column({ nullable: true })
  appleAuthID: string

  @Column('jsonb', { nullable: true })
  appleInfo: any

  @Index()
  @Column({ nullable: true })
  stripeCustomerID: string

  @Column('jsonb', { nullable: true })
  metadata: any

  @Column('text', { default: '{}', array: true })
  flags: string[]

  @Column({ default: true })
  isActivated: boolean

  @Column({ nullable: true })
  invitedByUser: string

  @Column('jsonb', { nullable: true })
  subscription: Stripe.Subscription

  get fullName() {
    return [this.firstName, this.middleName, this.lastName]
      .filter(Boolean)
      .join(' ')
  }

  assignRandomID() {
    this.id = 'usr_' + ulid()
  }

  @BeforeUpdate()
  @BeforeInsert()
  async createStripeCustomer() {
    if (this.stripeCustomerID) return
    const customer = await stripe.customers.create(
      {
        email: this.emails[0],
        name: this.fullName,
        metadata: {
          uid: this.id,
        },
      },
      { idempotencyKey: this.id },
    )
    this.stripeCustomerID = customer.id
  }

  @AfterInsert()
  @AfterUpdate()
  async identifyJune() {
    juneAnalytic.identify({
      userId: this.id,
      traits: {
        email: this.emails[0],
        firstName: this.firstName,
        middleName: this.middleName,
        lastName: this.lastName,
        stripeCustomerID: this.stripeCustomerID,
        flags: this.flags,
        paymentStatus: this.paymentStatus,
      },
    })
  }

  async syncStripeSubStatus() {
    const subs = await stripe.subscriptions.list({ customer: this.stripeCustomerID })
    const [firstSub] = subs.data
    this.subscription = firstSub || null
    return subs
  }

  async pauseStripeSubscription(): Promise<Stripe.Subscription> {
    await this.syncStripeSubStatus()

    if (this.paymentStatus === 'paused') {
      console.log('[user:pauseStripeSubscription]', 'Tried to pause an already paused subscription')
      return
    }

    this.subscription = await stripe.subscriptions.update(
      this.subscription.id,
      {
        pause_collection: {
          behavior: 'void',
        },
      },
    )
  }

  async resumeStripeSubscription(): Promise<Stripe.Subscription> {
    await this.syncStripeSubStatus()

    if (this.paymentStatus !== 'paused') {
      console.error('[user:resumeStripeSubscription]', 'Tried to resume subscription that is not paused')
      return
    }

    this.subscription = await stripe.subscriptions.update(
      this.subscription.id,
      {
        pause_collection: '',
      },
    )
  }

  get ageInDays() {
    return ((Date.now() - this.createdAt.getTime()) / 1000) / 86400
  }

  get paymentStatus() {
    if (!this.subscription) {
      if (this.createdAt < sub(new Date(), { days: 30 })) return 'expired'
      return 'free'
    }
    if (this.subscription.status === 'trialing') return 'trial'
    if (this.subscription.pause_collection) { // empty string ('') if not paused
      return 'paused'
    }
    return 'premium'
  }

  get avatarURL() {
    return this.googleInfo?.profile?.picture || `https://www.gravatar.com/avatar/${crypto.createHash('md5').update(this.emails[0]).digest('hex')}`
  }
}
