import { ulid } from 'ulid'
import {
  Entity,
  PrimaryColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  AfterInsert,
} from 'typeorm'

import juneAnalytic, { JuneEvent } from '@/lib/metrics/june'

@Entity()
export default class Invite {
  @PrimaryColumn()
  id: string

  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Index()
  @Column()
  createdByUser: string

  @Column({ nullable: true })
  usedByUser: string

  assignRandomID() {
    this.id = 'ivt_' + ulid()
  }

  @AfterInsert()
  async trackJune() {
    juneAnalytic.track({
      userId: this.createdByUser,
      event: JuneEvent.UserInviteCreate,
      properties: {
        inviteID: this.id,
      },
    })
  }
}
