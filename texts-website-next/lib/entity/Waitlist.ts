import { ulid } from 'ulid'
import {
  Entity,
  PrimaryColumn,
  CreateDateColumn,
  UpdateDateColumn,
  Column,
  AfterInsert,
} from 'typeorm'
import juneAnalytic, { JuneEvent } from '@/lib/metrics/june'

@Entity()
export default class Waitlist {
  @CreateDateColumn()
  createdAt: Date

  @UpdateDateColumn()
  updatedAt: Date

  @Column({ nullable: true })
  firstName: string

  @Column({ nullable: true })
  middleName: string

  @Column({ nullable: true })
  lastName: string

  @PrimaryColumn()
  email: string

  @Column({ nullable: true })
  os: string

  @Column({ nullable: true })
  ip: string

  @Column({ nullable: true })
  country: string

  @Column({ default: 0, nullable: true })
  checkCount: number

  @AfterInsert()
  async trackJune() {
    juneAnalytic.track({
      anonymousId: ulid(),
      event: JuneEvent.WaitListSignup,
      properties: {
        email: this.email,
        ip: this.ip,
        os: this.os,
        firstName: this.firstName,
        middleName: this.middleName,
        lastName: this.lastName,
        country: this.country,
      },
    })
  }
}
