import TelegramBot from 'node-telegram-bot-api'
import { getUserText } from '@/lib/all-emails'
import type User from '@/lib/entity/User'

import config from '@/lib/config'

const bot = new TelegramBot(config.TELEGRAM_BOT.TOKEN, { polling: true })

const send = (text: string) => {
  console.log({ text })
  return bot.sendMessage(config.TELEGRAM_BOT.USER_UPDATES_CHAT_ID, text)
}

export const userUsedAppAfterDays = (user: User, days: number, appVersion: string, os: string, openAtLogin: boolean, wasOpenedAtLogin: boolean, ipCountry: string) => {
  const text = `${getUserText(user)} used app v${appVersion} on ${os} after ${days} days in ${ipCountry}. ` + [openAtLogin && 'openAtLogin', wasOpenedAtLogin && 'wasOpenedAtLogin'].filter(Boolean).join(' ')
  return send(text)
}
export const userPaymentStatusChanged = (user: User, next: string) => {
  const text = `${getUserText(user)} payment status changed from ${user.paymentStatus} → ${next}`
  return send(text)
}

export const jobsReplyMarkup: TelegramBot.InlineKeyboardMarkup = {
  inline_keyboard: [
    [
      {
        text: '👀 Viewed',
        callback_data: JSON.stringify({ type: 'mark_as_viewed' }),
      },
      {
        text: '✅ Reached out',
        callback_data: JSON.stringify({ type: 'mark_as_reached_out' }),
      },
      {
        text: '❌ Passed',
        callback_data: JSON.stringify({ type: 'mark_as_passed' }),
      },
    ],
  ],
}

const UNRESPONDED_HASHTAG = '\n🟡 #unresponded'
const RESPONDED_HASHTAG = '\n✅ #responded'

const VIEWED_TEXT = 'Status: 👀 #Viewed'
const REACHED_OUT_TEXT = 'Status: 😌 #ReachedOut'
const PASSED_TEXT = 'Status: 👎🏻 #Passed'

bot.on('callback_query', async ({ message, data: data_, id }) => {
  const data = JSON.parse(data_)

  if (!data.type) return
  const opts: TelegramBot.EditMessageTextOptions = { message_id: message.message_id, chat_id: message.chat.id, parse_mode: 'Markdown' }
  await bot.editMessageReplyMarkup(null, { message_id: message.message_id, chat_id: message.chat.id })
  switch (data.type) {
    case 'mark_as_responded':
      bot.editMessageText(message.text.replace(UNRESPONDED_HASHTAG, RESPONDED_HASHTAG), opts)
      bot.answerCallbackQuery(id, { text: 'Marked as responded!' })
      break
    case 'mark_as_viewed':
      bot.editMessageText(message.text.replace(/Status:(.*)/g, VIEWED_TEXT), { ...opts, reply_markup: jobsReplyMarkup })
      bot.answerCallbackQuery(id, { text: 'Marked as viewed!' })
      break
    case 'mark_as_reached_out':
      bot.editMessageText(message.text.replace(/Status:(.*)/g, REACHED_OUT_TEXT), { ...opts, reply_markup: jobsReplyMarkup })
      bot.answerCallbackQuery(id, { text: 'Marked as reached out!' })
      break
    case 'mark_as_passed':
      bot.editMessageText(message.text.replace(/Status:(.*)/g, PASSED_TEXT), opts)
      bot.answerCallbackQuery(id, { text: 'Marked as passed!' })
      break
  }
})

export default bot
