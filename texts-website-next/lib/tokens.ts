import slugify from 'slugify'

import { randomBytes } from '@/lib/crypto'
import redis from '@/lib/redis'

const APP_LOGIN_TOKEN_EXPIRY_TIME = 2 * 60 * 60 // 2 hrs
const APP_LOGIN_TOKEN_PREFIX = 'app-sso-token:'

export const getUIDFromAppLoginToken = (token: string) =>
  redis.get(APP_LOGIN_TOKEN_PREFIX + token)

export const deleteAppLoginToken = (token: string) =>
  redis.del(APP_LOGIN_TOKEN_PREFIX + token)

export const isAppLoginTokenValid = (token: string) =>
  redis.exists(APP_LOGIN_TOKEN_PREFIX + token)

export async function createAppLoginToken(uid: string) {
  const tokenBuffer = await randomBytes(32)
  const token = slugify(tokenBuffer.toString('base64'), { strict: true })
  await redis.set(
    APP_LOGIN_TOKEN_PREFIX + token,
    uid,
    'EX',
    APP_LOGIN_TOKEN_EXPIRY_TIME,
  )
  return token
}
