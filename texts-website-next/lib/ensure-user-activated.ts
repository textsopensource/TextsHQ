import { NextApiResponse } from 'next'

import { DEFAULT_UNAUTH_REDIRECT } from '@/lib/constants'
import User from '@/lib/entity/User'
import en from '@/lib/lang-en'

export default function ensureUserActivated(user: User, res: NextApiResponse, redirect?: boolean) {
  if (user?.isActivated) return true

  if (redirect) {
    res.redirect(user ? '/not-activated' : DEFAULT_UNAUTH_REDIRECT)
  } else {
    res.status(401).json({ error: user ? en.ACCOUNT_UNACTIVATED : en.NOT_AUTHENTICATED })
  }
}
