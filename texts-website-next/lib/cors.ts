import Cors from 'cors'

const cors = () =>
  Cors({ methods: ['GET', 'POST', 'HEAD', 'OPTIONS'] })

export default cors
