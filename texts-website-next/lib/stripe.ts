import Stripe from 'stripe'

import config from '@/lib/config'

export default new Stripe(config.STRIPE.SECRET_KEY!, { apiVersion: null })
