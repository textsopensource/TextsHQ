import crypto from 'crypto'
import bluebird from 'bluebird'

export const randomBytes = bluebird.promisify(crypto.randomBytes)
