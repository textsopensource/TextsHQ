// Importing this in another file would result in workers immediately starting in that file
// This file should be directly run (unless you want to attach listeners to workers in another file)
import '@/lib/config/loadDotEnv'
import { Worker } from 'bullmq'
import { Queues, queueConfig, pauseSubscriptionForUserQueue, PauseSubscriptionJobData, ProcessInactiveUsersJobData } from '@/lib/queues';
import { getInactiveUsersWithActiveSubscriptions, DEFAULT_INACTIVE_SINCE, pauseSubscription } from '@/lib/subscriptions';

const workers: { [key in Queues]: Worker} = {
  [Queues.PROCESS_INACTIVE_USERS]: new Worker<ProcessInactiveUsersJobData>(
    Queues.PROCESS_INACTIVE_USERS,
    async (job) => {
      const inactiveSince = job.data?.inactiveSince
      const users = await getInactiveUsersWithActiveSubscriptions()
      await pauseSubscriptionForUserQueue.addBulk(
        users.map((user) => ({
          name: `user:${user.id}`,
          data: { userId: user.id, inactiveSince: inactiveSince || DEFAULT_INACTIVE_SINCE } }
        ))
      )
    },
    queueConfig
  ),
  [Queues.PAUSE_SUBSCRIPTION_FOR_USER]: new Worker<PauseSubscriptionJobData>(
    Queues.PAUSE_SUBSCRIPTION_FOR_USER,
    ({ data: { userId, inactiveSince } }) => pauseSubscription(userId, inactiveSince),
    queueConfig
  ),
}

export default workers
