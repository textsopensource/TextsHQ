// @ts-expect-error
import Grant from 'grant/lib/grant'

import config from '@/lib/config'

const { OAUTH } = config

const grant = Grant({
  config: {
    defaults: {
      prefix: '/auth',
      origin: process.env.NEXT_PUBLIC_ORIGIN,
      transport: 'session',
      state: true,
      response: ['tokens', 'profile'],
      callback: '/auth/done',
    },
    google: {
      key: OAUTH.GOOGLE.CLIENT_ID,
      secret: OAUTH.GOOGLE.CLIENT_SECRET,
      scope: ['openid', 'profile', 'email'],
    },
    twitter: {
      key: OAUTH.TWITTER.API_KEY,
      secret: OAUTH.TWITTER.API_SECRET_KEY,
      scope: ['openid', 'email'],
    },
  },
})

export default grant
