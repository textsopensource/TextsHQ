import { NextApiRequest } from 'next'
import { WebClient } from '@slack/web-api'
import config from '@/lib/config'

import { FeedbackRequest, Problem, withAttachment } from './send-feedback'
import type User from './entity/User'

const { BOT_TOKEN, CHANNEL_ID } = config.SLACK

export default async function sendFeedbackToSlack(req: NextApiRequest, user: User) {
  const slackClient = new WebClient(BOT_TOKEN)

  const appVersion = String(req.headers['x-app-version'])
  const osHeader = String(req.headers['x-os'])

  const fromUser = user.firstName ? `${user.firstName} ${user.lastName || ''}` : (user.emails[0] || user.id)
  const userEmail = user.emails?.[0] || 'N/A'

  // const elements: any = [
  //   {
  //     type: 'button',
  //     text: {
  //       type: 'plain_text',
  //       emoji: true,
  //       text: 'Mark as Responded',
  //     },
  //     style: 'primary',
  //     value: `mark_responded`,
  //   },
  // ]

  const isProblemReport = Boolean(req.body.problem)

  const activePlatform = (isProblemReport ? req.body.platform : req.body.meta.activePlatform)?.replaceAll('-baileys', '')
  const connectedPlatforms = (isProblemReport ? req.body.accounts : req.body.meta.connectedPlatforms).map((p: string) => p.replace('-baileys', ''))

  const request = req.body as FeedbackRequest

  const isWKWV = isProblemReport ? req.body.isWKWV : request.meta.isWKWV

  const blocks = [
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*${isProblemReport ? ':warning: Problem report' : 'New feedback'} from ${
          userEmail
            ? `<mailto:${userEmail}|${fromUser}>`
            : fromUser
        }*`,
      },
    },
    {
      type: 'section',
      fields: [
        (isProblemReport ? {
          type: 'mrkdwn',
          text: `*Problem:*\n${(request as unknown as Problem).problem || 'N/A'}`,
        } : {
          type: 'mrkdwn',
          text: `*Emotion:*\n${(request as FeedbackRequest).emotion || 'N/A'}`,
        }),
        {
          type: 'mrkdwn',
          text: `*Renderer:*\n${isWKWV ? ':appleinc: WKWV' : ':electron: Electron'}`,
        },
      ],
    },
    {
      type: 'section',
      fields: [
        {
          type: 'mrkdwn',
          text: `*Active Platform:*\n${activePlatform}`,
        },
        {
          type: 'mrkdwn',
          text: `*App Version:*\nTexts v${appVersion} on ${osHeader}`,
        },
      ],
    },
    {
      type: 'section',
      text: {
        type: 'mrkdwn',
        text: `*${isProblemReport ? 'Description' : 'Feedback'}:*\n${request.text || (request as unknown as Problem).description}`,
      },
    },
    {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `*Connected Platforms:* ${connectedPlatforms.join(', ')}`,
        },
      ],
    },
    request.attachment && {
      type: 'context',
      elements: [
        {
          type: 'mrkdwn',
          text: `*This ${isProblemReport ? 'report' : 'feedback'} has an attachment.* See thread for details.`,
        },
      ],
    },
    // {
    //   type: 'actions',
    //   elements,
    // },
  ].filter(Boolean)

  const msg = await slackClient.chat.postMessage({
    channel: CHANNEL_ID,
    text: req.body.text || req.body.description,
    blocks,
    username: fromUser || null,
    icon_url: user.avatarURL || null,
  })

  if (request.attachment?.id) {
    await withAttachment(request.attachment.id, async fileBuffer => {
      await slackClient.files.upload({
        file: fileBuffer,
        filetype: request.attachment.type,
        filename: request.attachment.id,
        thread_ts: msg.ts,
        channels: CHANNEL_ID,
      })
    }, true) // we delete here and not when sending to telegram/linear
  }
}
