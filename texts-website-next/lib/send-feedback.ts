import path from 'path'
import { NextApiRequest } from 'next'
import { LinearClient } from '@linear/sdk'
import fsp from 'fs/promises'

import config from '@/lib/config'
import allEmails from '@/lib/all-emails'
import sendEmail from '@/lib/emails'
import bot from '@/lib/telegram-bot'
import type User from '@/lib/entity/User'

const { TELEGRAM_BOT, LINEAR } = config

const LINEAR_TEAM_ID = '4be12dc3-8799-4c19-ae96-fe0f492d0b6f'
const { FEEDBACK_CHAT_ID } = TELEGRAM_BOT

export const UPLOAD_DIR = '/tmp/att-uploads'

fsp.mkdir(UPLOAD_DIR, { recursive: true })

const linear = new LinearClient({ apiKey: LINEAR.API_KEY })

export type FeedbackRequest = {
  text: string
  emotion: string
  meta: {
    activePlatform: string
    connectedPlatforms: string[]
    isWKWV?: boolean
  }
  attachment?: {
    type: string
    id?: string
    data?: string
  }
}
export type Problem = {
  platform: string
  problem: string
  description: string
  accounts: [string]
  attachment?: {
    type: string
    id?: string
    data?: string
  }
  isWKWV?: boolean
}

const UNRESPONDED_HASHTAG = '\n🟡 #unresponded'

function getFeedbackText(appVersion: string, osHeader: string, feedback: FeedbackRequest) {
  const { text, emotion, meta } = feedback
  return `
${text}

-
${emotion || ''}
Active: ${meta.activePlatform}
Connected: ${meta.connectedPlatforms.join(', ')}
Texts v${appVersion} on ${osHeader}
WK: ${meta.isWKWV}
`
}
function getProblemText(appVersion: string, osHeader: string, problem: Problem) {
  const { platform, problem: problem_, description, accounts, isWKWV } = problem

  return `*Platform:* ${platform}
*Problem:* ${problem_}
*Description:* ${description}
Texts v${appVersion} on ${osHeader}
*WK:* ${isWKWV}
*Platforms:*
\`\`\`
${accounts.map(a => a.replace('-baileys', '')).join('\n')}
\`\`\`
`
}

export async function withAttachment(attID: string, callback: (fileBuffer: Buffer) => Promise<any>, unlink: boolean) {
  // todo: attID is provided by user, ensure it isn't tampered with
  const attachmentName = Buffer.from(attID, 'base64').toString('utf-8')
  const attachmentPath = path.join(UPLOAD_DIR, attachmentName)
  if (!attachmentPath.startsWith(UPLOAD_DIR)) throw Error('invalid attachment id')

  try {
    const fileBuffer = await fsp.readFile(attachmentPath)
    await callback(fileBuffer)
  } catch (error) {
    console.error('Error reading attachment', error)
    // todo sentry
  } finally {
    if (unlink) await fsp.unlink(attachmentPath)
  }
}

async function sendFeedbackToChannel(userWithEmail: string, issueTxt: string, feedback: FeedbackRequest, linearUrl?: string) {
  const { attachment } = feedback

  const msgTxt = `${userWithEmail}wrote in:

${issueTxt.trim()}

🔍 Linear: ${linearUrl}
` + UNRESPONDED_HASHTAG

  const msg = await bot.sendMessage(FEEDBACK_CHAT_ID, msgTxt, {
    parse_mode: 'Markdown',
    disable_web_page_preview: true,
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: '✅ Mark as responded',
            callback_data: JSON.stringify({
              type: 'mark_as_responded',
            }),
          },
        ],
      ],
    },
  })

  console.log(msg)

  async function sendMessages(data: any) {
    if (attachment.type.startsWith('image')) await bot.sendPhoto(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
    else await bot.sendDocument(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
  }

  if (attachment) {
    // legacy format
    if (attachment.data) {
      const data = Buffer.from(attachment.data.split('base64,')[1], 'base64')
      await sendMessages(data)
    }

    // new format
    if (attachment.id) {
      await withAttachment(attachment.id, sendMessages, false)
    }
  }
}

async function sendFeedbackToLinear(title: string, description: string): Promise<string> {
  const res = await linear.issueCreate({
    teamId: LINEAR_TEAM_ID,
    title,
    description,
  })
  return (await res.issue).url
}

async function reportProblem(userWithEmail: string, problem: Problem, user: User, osHeader: string, appVersion: string) {
  const { attachment } = problem
  const issueText = getProblemText(appVersion, osHeader, problem)

  const issueURL = await sendFeedbackToLinear(`Problem report from ${userWithEmail}`, issueText).catch(() => '(none)')

  const msgTxt = `${userWithEmail} reported a problem:
${issueText}
🔍 Linear: ${issueURL}
`

  const msg = await bot.sendMessage(FEEDBACK_CHAT_ID, msgTxt, { parse_mode: 'Markdown' })

  if (attachment?.id) {
    await withAttachment(attachment.id, async data => {
      if (attachment.type.startsWith('image')) await bot.sendPhoto(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
      else await bot.sendDocument(FEEDBACK_CHAT_ID, data, { reply_to_message_id: msg.message_id })
    }, false)
  }

  sendEmail(allEmails.userSentProblemReport(user, issueText)).catch(console.error)
}

async function reportFeedback(userWithEmail: string, fr: FeedbackRequest, user: User, osHeader: string, appVersion: string) {
  fr.meta.activePlatform = fr.meta.activePlatform?.replaceAll('-baileys', '')
  fr.meta.connectedPlatforms = fr.meta.connectedPlatforms.map(p => p.replace('-baileys', ''))

  const issueTxt = getFeedbackText(appVersion, osHeader, fr)
  const issueURL = await sendFeedbackToLinear(`Feedback from ${userWithEmail}`, issueTxt).catch(() => '(none)')
  await sendFeedbackToChannel(userWithEmail, issueTxt, fr, issueURL)
  if (fr.text) {
    sendEmail(allEmails.userSentFeedback(user, issueTxt)).catch(console.error)
  }
}

export async function sendFeedbackToLinearAndTelegram(req: NextApiRequest, user: User) {
  const appVersion = String(req.headers['x-app-version'])
  const osHeader = String(req.headers['x-os'])

  const fromUser = [user.firstName, user.lastName].filter(Boolean).join(' ') || user.id
  const userWithEmail = `${fromUser} ${user.emails?.length ? `(${user.emails.join(' ')}) ` : ''}`

  if (req.body.type === 'problem') {
    await reportProblem(userWithEmail, req.body as Problem, user, osHeader, appVersion)
    return { ok: true }
  }

  const fr = req.body as FeedbackRequest
  await reportFeedback(userWithEmail, fr, user, osHeader, appVersion)
}
