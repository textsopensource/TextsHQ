export const IS_DEV = process.env.NODE_ENV !== 'production'

export enum WaitlistStatus {
  ADDED_TO_WAITLIST = 'ADDED_TO_WAITLIST',
  ALREADY_ON_WAITLIST = 'ALREADY_ON_WAITLIST',
}

export const DEFAULT_UNAUTH_REDIRECT = '/auth/google'
export const AFTER_SIGNUP_REDIRECT = '/install'
