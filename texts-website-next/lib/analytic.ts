import { InfluxDB, WriteApi, Point } from '@influxdata/influxdb-client'
import config from '@/lib/config'
import { getConnection, getRepos } from '@/lib/orm'

// These measurements are for the web server
// Electron app will sends it own variant of metrics depending on versions
export enum AnalyticMeasurement {
  AppLogin = 'appLogin',
  AppLogout = 'appLogout',
  SessionValidation = 'sessionValidation',
  SiteLogin = 'siteLogin',
  SiteLogout = 'siteLogout',
  FeedbackSubmitted = 'feedbackSubmitted',
  WaitlistSignup = 'waitlistSignup',
  UserCreated = 'userCreated',
  BinaryDownload = 'binaryDownload',
  InstallScriptFetch = 'installScriptFetch',
  BuildExpired = 'buildExpired',

  // SQL queries
  UserRows = 'userRows',
  WaitListRows = 'waitListRows',
  SessionRows = 'sessionRows',
  InviteRows = 'inviteRows',
  UsingAfterD1 = 'usingAfterD1',
  UsingAfterD7 = 'usingAfterD7',
  UsingAfterD14 = 'usingAfterD14',
  UsingAfterD30 = 'usingAfterD30',
  UsingAfterD90 = 'usingAfterD90',
  UsingAfterD180 = 'usingAfterD180',
}

class Analytic {
  protected client: InfluxDB

  protected _writeAPI: WriteApi

  constructor(url: string, token: string) {
    if (!url || !token) {
      console.warn('InfluxDB not connected')
      this._writeAPI = {
        writePoint: () => {},
      } as any
      return
    }

    this.client = new InfluxDB({ url, token })

    this._writeAPI = this.client.getWriteApi(config.INFLUXDB.ORG, config.INFLUXDB.BUCKET, 's')

    // Hopefully 5 minutes is granular enough for the queries in flush
    setInterval(this.flush.bind(this), 5 * 60 * 1000)
  }

  private async flush() {
    const { inviteRepo, userRepo, waitlistRepo, sessionRepo } = getRepos(await getConnection())

    const points = [
      new Point(AnalyticMeasurement.UserRows)
        .intField('count', await userRepo.count()),
      new Point(AnalyticMeasurement.WaitListRows)
        .intField('count', await waitlistRepo.count()),
      new Point(AnalyticMeasurement.SessionRows)
        .intField('count', await sessionRepo.count()),
      new Point(AnalyticMeasurement.InviteRows)
        .intField('count', await inviteRepo.count()),
    ]

    for (const i of [
      { e: AnalyticMeasurement.UsingAfterD1, d: 1 },
      { e: AnalyticMeasurement.UsingAfterD7, d: 7 },
      { e: AnalyticMeasurement.UsingAfterD14, d: 14 },
      { e: AnalyticMeasurement.UsingAfterD30, d: 30 },
      { e: AnalyticMeasurement.UsingAfterD90, d: 90 },
      { e: AnalyticMeasurement.UsingAfterD180, d: 180 },
    ]) {
      const { count } = (await userRepo.query(`select count(*) from "user" where COALESCE("lastActive", "createdAt") - "createdAt" >= interval '${i.d} day'`))[0]

      points.push(
        new Point(i.e)
          .intField('count', count),
      )
    }

    this._writeAPI.writePoints(points)

    this._writeAPI.flush(true)
  }

  public get writeAPI(): WriteApi {
    return this._writeAPI
  }
}

export default new Analytic(config.INFLUXDB.URL, config.INFLUXDB.TOKEN)
