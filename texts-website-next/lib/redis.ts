import Redis from 'ioredis'

import config from '@/lib/config'

const redis = new Redis(config.REDIS_CONNECTION_STRING)

export default redis
