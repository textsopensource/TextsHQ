// custom server in dev environment for listening on https
import https from 'https'
import http from 'http'
import url from 'url'
import fs from 'fs'
import next from 'next'

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

await app.prepare()

const { PORT: port = 443, NEXT_PUBLIC_ORIGIN } = process.env

https.createServer({
  key: fs.readFileSync('./dev-certs/key.pem'),
  cert: fs.readFileSync('./dev-certs/cert.pem')
}, (req, res) => {
  const parsedUrl = url.parse(req.url, true)
  handle(req, res, parsedUrl)
}).listen(port, () => {
  console.log(`> Ready on ${NEXT_PUBLIC_ORIGIN}`)
})

http.createServer((req, res) => {
  res.writeHead(301, { Location: NEXT_PUBLIC_ORIGIN }).end()
}).listen(80)
