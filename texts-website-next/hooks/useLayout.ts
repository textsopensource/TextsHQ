import { createContext, useContext } from 'react'

type LayoutContextProps = {
  isFlash: boolean;
  setIsFlash: (isFlash: boolean) => void;
}

export const LayoutContext = createContext<Partial<LayoutContextProps>>({
  isFlash: false,
})

export function useLayout() {
  const { isFlash, setIsFlash } = useContext(LayoutContext)

  return { isFlash, setIsFlash }
}
