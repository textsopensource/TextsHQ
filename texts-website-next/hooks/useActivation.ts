import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import useUser from './useUser'

export default function useActivation() {
  const [activated, setActivated] = useState<boolean>(undefined)
  const router = useRouter()
  const user = useUser({ redirectTo: '/' })

  useEffect(() => {
    if (typeof user !== 'undefined') {
      if (!user?.isActivated) {
        setActivated(false)
        router.push('/not-activated')
      } else {
        setActivated(true)
      }
    }
  }, [user])

  return activated
}
