import url from 'node:url'
import path from 'node:path'
import rehypeHighlight from 'rehype-highlight'
import remarkFrontmatter from 'remark-frontmatter'
import { remarkMdxFrontmatter } from 'remark-mdx-frontmatter'
import withMDX from '@next/mdx'
// const webpack = require('webpack')
// const { withSentryConfig } = require("@sentry/nextjs")

const nextOptions = {
  // webpack: (config, { dev, isServer }) => {
  //   if (isServer) {
  //     let definePlugin = config.plugins.find(p => p.constructor.name === 'DefinePlugin')
  //     if (!definePlugin) {
  //       definePlugin = new webpack.DefinePlugin()
  //       config.plugins.unshift(definePlugin)
  //     }
  //     definePlugin.definitions.__SITE_ROOT__ = JSON.stringify(__dirname)
  //     definePlugin.definitions.IS_DEV = dev
  //     definePlugin.definitions.__DEV__ = dev
  //   }
  //   return config
  // },
  images: {
    domains: ['pbs.twimg.com'],
  },
  pageExtensions: ['js', 'jsx', 'ts', 'tsx', 'mdx'],
  reactStrictMode: true,
  async redirects() {
    return [
      {
        source: "/twitter",
        destination: 'https://twitter.com/TextsHQ',
        permanent: false,
      },
      {
        source: "/legalese.html",
        destination: "/privacy",
        permanent: true,
      },
      {
        source: "/privacy(.+)",
        destination: "/privacy",
        permanent: true,
      },
      {
        source: "/terms",
        destination: "/privacy",
        permanent: true,
      },
      {
        source: "/tos",
        destination: "/privacy",
        permanent: true,
      },
      {
        source: "/download",
        destination: "/install",
        permanent: true,
      },
      {
        source: "/install-mac(.*)",
        destination: "/install/macos",
        permanent: true,
      },
      {
        source: "/pricing",
        destination: "/subscription",
        permanent: true,
      },
      {
        source: "/careers",
        destination: "/jobs",
        permanent: true,
      },
      {
        source: "/hiring",
        destination: "/jobs",
        permanent: true,
      },
      {
        source: "/update-feed/:slug*",
        destination: "/api/update-feed/:slug*",
        permanent: true,
      },
      {
        source: "/invite",
        destination: "/",
        permanent: false,
      },
      {
        source: "/changelog",
        destination: "https://www.notion.so/Texts-Changelog-1c9190fe057a41a6bfa7443014b3a3ed",
        permanent: false,
      },
      {
        source: "/swift-cli-runtime",
        destination: "/swift-runtime",
        permanent: true,
      },
      {
        source: "/inbox-zero-wallpaper",
        destination: "https://source.unsplash.com/2000x1200/?nature",
        permanent: true,
      },
    ]
  },
  async rewrites() {
    return [
      { source: "/conf.json", destination: "/api/conf.json" },
      { source: "/customer-portal", destination: "/api/stripe/customer-portal" },
      { source: "/logout", destination: "/api/logout" },
      { source: "/metrics", destination: "/api/metrics" },
      { source: "/update-feed.json", destination: "/api/update-feed.json" },

      { source: "/auth/:path*", destination: "/api/auth/:path*" },
      { source: "/deeplink/:path*", destination: "/api/deeplink/:path*" },
      { source: "/i/:path*", destination: "/api/i/:path*" },
      { source: "/invite/:inviteID", destination: "/api/invite/:inviteID" },
      { source: "/stripe-webhook/:secret*", destination: '/api/stripe/webhook/:secret*' },
    ]
  },
  serverRuntimeConfig: {
    PROJECT_ROOT: path.dirname(url.fileURLToPath(import.meta.url)),
  },
}

const sentryOptions = {
  // https://github.com/getsentry/sentry-webpack-plugin#options
}

export default withMDX({
  options: {
    remarkPlugins: [remarkFrontmatter, remarkMdxFrontmatter],
    rehypePlugins: [rehypeHighlight],
  },
})(nextOptions) // withSentryConfig(nextOptions, sentryOptions)
