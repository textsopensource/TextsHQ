module.exports = {
  content: ['./components/**/*.tsx', './pages/**/*.tsx', './lib/*/*.ts', './layouts/*.tsx'],
  darkMode: 'media',
  theme: {
    extend: {
      colors: {
        'gray-900': '#101116',
      }
    }
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
}
