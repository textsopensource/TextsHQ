// custom server in prod environment
const http = require('http')
const url = require('url')
const next = require('next')

const app = next({ dev: false })
const handle = app.getRequestHandler()

async function main() {
  await app.prepare()

  const { PORT: port = 30000, NEXT_PUBLIC_ORIGIN } = process.env

  http.createServer((req, res) => {
    const parsedUrl = url.parse(req.url, true)
    if (parsedUrl.pathname.endsWith('.map')) {
      res.statusCode = 404
      return res.end('404')
    }
    handle(req, res, parsedUrl)
  }).listen(port, () => {
    console.log(`> Ready on ${NEXT_PUBLIC_ORIGIN}`)
  })
}
main()
