import type { PlatformAPI, OnServerEventCallback, LoginResult, Paginated, Thread, Message, CurrentUser, InboxName, MessageContent, PaginationArg, LoginCreds } from '@textshq/platform-sdk'

export default class TestAPI implements PlatformAPI {

  private username: string

  private threads: Thread[]

  init = (session: any) => {
  }

  login = async ({ username, password }: LoginCreds): Promise<LoginResult> => {
    return { type: 'success' }
  }

  logout = () => { }

  getCurrentUser = (): CurrentUser => undefined
  subscribeToEvents = (onEvent: OnServerEventCallback) => {
  }

  dispose = () => {
  }

  serializeSession = () => undefined

  searchUsers = async (typed: string) => []

  createThread = (userIDs: string[]) => null as any

  getThreads = async (inboxName: InboxName, pagination?: PaginationArg): Promise<Paginated<Thread>> => undefined

  getMessages = async (threadID: string, pagination?: PaginationArg): Promise<Paginated<Message>> => undefined
  sendMessage = async (threadID: string, content: MessageContent) => undefined

  sendActivityIndicator = (threadID: string) => undefined

  addReaction = async (threadID: string, messageID: string, reactionKey: string) => {}

  removeReaction = async (threadID: string, messageID: string, reactionKey: string) => {}

  deleteMessage = async (threadID: string, messageID: string) => true

  deleteThread = async (threadID: string) => {}

  sendReadReceipt = async (threadID: string, messageID: string) => {}
}
