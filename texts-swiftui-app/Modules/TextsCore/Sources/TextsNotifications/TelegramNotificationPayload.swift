import Foundation
import UserNotifications
import TextsBase

/*
 `{"aps":{"alert":{"title":"1Conan .","body":"ctitgigixoco"},"mutable-content":1,"sound":"0.m4a","category":"r","thread-id":"5226804388"},"msg_id":"55834","from_id":"5226804388”}`
 `{"aps":{"alert":{"title":"1Conan .","body":"📷 Photo"},"mutable-content":1,"sound":"0.m4a","category":"r","thread-id":"5226804388"},"msg_id":"55835","from_id":"5226804388","attachb64":"ZXoZ-wAAAACSsDEbYMnxVfgJ8yWphElvFQBiPbObMhWWb4sfa0ewSH1dtnkq9gAAmrM9YhXEtRwEAAAAYI7HdQFzAABaAAAAQgAAAPACAABgjsd1AW0AAEABAADqAAAA3RwAAGCOx3UBeAAAIAMAAEgCAABLZQAAlfs--gF5AADaBAAAigMAABXEtRwFAAAASRQAAAknAABePgAAYVcAAKCKAAAFAAAA”}`

 Single Chat: msg_id,from_id,thread_id
 Group: msg_id,chat_from_id,chat_id,thread_id
 */
public struct TelegramNotificationPayload: NotificationPayloadProtocol {
    public let title: String?
    public let body: String

    public let accountID: Schema.AccountID
    public let messageID: Schema.MessageID
    public let threadID: Schema.ThreadID
    public let senderID: Schema.UserID
    public let isGroup: Bool
    public let attachment: Data?

    public var mappedPayload: Schema.MappedNotificationPayload
}

extension TelegramNotificationPayload {
    init(content: UNNotificationContent) throws {
        self.title = content.title
        self.body = content.body

        if let accountID = content.userInfo["user_id"] as? String {
            self.accountID = .init(rawValue: accountID)
        } else {
            throw NotificationDecodingError.badKey("user_id")
        }

        guard let messageID = content.userInfo["msg_id"] as? String else {
            throw NotificationDecodingError.badKey("msg_id")
        }
        self.messageID = .init(rawValue: messageID)
        self.threadID = .init(rawValue: content.threadIdentifier)

        if let fromID = content.userInfo["from_id"] as? String {
            self.senderID = .init(rawValue: fromID)
            self.isGroup = false
        } else if let fromID = content.userInfo["chat_from_id"] as? String {
            self.senderID = .init(rawValue: fromID)
            self.isGroup = true
        } else {
            throw NotificationDecodingError.badKey("from_id")
        }

        if let attachmentString = content.userInfo["attachb64"] as? String,
           let attachment = Data(base64Encoded: attachmentString) {
            // TODO: Decode TL-serialized payload
            self.attachment = attachment
        } else {
            self.attachment = nil
        }

        mappedPayload = .init(
            platform: "Telegram",
            thread: .init(
                title: title,
                message: body
            ),
            sender: .init()
        )
    }

    public func modifiedContent() async -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        if let title = title {
            content.title = title
        }
        content.subtitle = "Telegram"
        content.body = body
        content.threadIdentifier = threadID.rawValue
        content.categoryIdentifier = PushCategory.message.rawValue
        return content
    }
}
