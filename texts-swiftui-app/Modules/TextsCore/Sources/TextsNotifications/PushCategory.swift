import Foundation

public enum PushCategory: String {
    case message = "TXTPushCategoryMessage"
}
