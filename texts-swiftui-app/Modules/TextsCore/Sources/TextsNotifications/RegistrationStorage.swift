import Foundation
import Swallow
import TextsBase

public typealias RegistrationID = SchemaID<RelayClient, String>

public struct Registration: Codable, Equatable {
    public var id: RegistrationID
    public var type: Schema.NotificationKind
    public var accountID: Schema.AccountID
    public var token: String

    public init(
        id: RegistrationID,
        type: Schema.NotificationKind,
        accountID: Schema.AccountID,
        token: String
    ) {
        self.id = id
        self.type = type
        self.accountID = accountID
        self.token = token
    }

    public static func == (lhs: Registration, rhs: Registration) -> Bool {
        lhs.id == rhs.id
    }
}

public final class RegistrationStorage {
    public static let shared = RegistrationStorage()

    private static let key = "TXTRegistrations"
    private static let inverseKey = "TXTRegistrationsInverse"

    private let defaults: UserDefaults

    private init() {
        defaults = UserDefaults(suiteName: "group.com.texts.shared")!
    }

    public var inverseRegistrations: [Schema.AccountID: Registration] {
        get {
            guard let dict = defaults.dictionary(forKey: Self.key) else { return [:] }
            return .init(
                uniqueKeysWithValues: dict
                    .compactMapValues {
                        try? JSONDecoder().decode(
                            Registration.self,
                            from: ($0 as? Data).unwrap()
                        )
                    }
                    .map { ($1.accountID, $1) }
            )
        }
        set {
            defaults.setValue(
                Dictionary(uniqueKeysWithValues: newValue
                    .map { ($1.id.rawValue, try? JSONEncoder().encode($1)) }),
                forKey: Self.key
            )
        }
    }

    public var registrations: [RegistrationID: Registration] {
        get {
            guard let dict = defaults.dictionary(forKey: Self.key) else { return [:] }
            return .init(
                uniqueKeysWithValues: dict
                    .compactMapValues {
                        try? JSONDecoder().decode(
                            Registration.self,
                            from: ($0 as? Data).unwrap()
                        )
                    }
                    .map { ($1.id, $1) }
            )
        }
        set {
            defaults.setValue(
                newValue
                    .mapKeys(\.rawValue)
                    .mapValues({ try? JSONEncoder().encode($0) }),
                forKey: Self.key)
        }
    }

    public func updateRegistration<T>(for account: Schema.AccountID, update: (inout Registration?) async throws -> T) async rethrows -> T {
        let old = inverseRegistrations[account]
        var new = old
        let result = try await update(&new)
        guard new != old else {
            return result
        }

        if let oldID = old?.id {
            registrations[oldID] = nil
        }

        if let new = new {
            registrations[new.id] = new
        }

        return result
    }
}
