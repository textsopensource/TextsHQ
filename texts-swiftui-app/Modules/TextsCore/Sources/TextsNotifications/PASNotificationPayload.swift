import Foundation
import TextsBase
import UserNotifications
import Intents

public struct PASNotificationPayload: Decodable, NotificationPayloadProtocol {
    public let accountID: Schema.AccountID

    public struct Sender: Decodable {
        public let id: Schema.UserID
        public let username: String?
        public let fullName: String?
        public let email: String?
        public let phoneNumber: String?
        public let imgURL: String?
    }
    public let sender: Sender

    public struct Thread: Decodable {
        public let id: Schema.ThreadID
        public let title: String?
        public let imgURL: String?
        public let type: Schema.ThreadType
        public let numParticipants: Int
    }
    public let thread: Thread

    public struct Platform: Decodable {
        public let name: String
        public let displayName: String
        public let icon: String
    }
    public let platform: Platform

    public struct Message: Decodable {
        public let id: Schema.MessageID
        public let text: String?
    }
    public let message: Message

    public var senderID: Schema.UserID { sender.id }
    public var threadID: Schema.ThreadID { thread.id }
    public var messageID: Schema.MessageID { message.id }

    public var mappedPayload: Schema.MappedNotificationPayload {
        .init(
            platform: platform.displayName,
            thread: .init(
                title: thread.title,
                message: message.text,
                imgURL: thread.imgURL,
                numParticipants: thread.numParticipants,
                type: thread.type
            ),
            sender: .init(
                name: sender.displayName,
                phoneNumber: sender.phoneNumber,
                email: sender.email
            )
        )
    }
}

extension PASNotificationPayload.Sender {
    public var displayName: String {
        fullName ?? username ?? phoneNumber ?? email ?? "Unknown"
    }
}

extension PASNotificationPayload {
    public func modifiedContent() async -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()

        content.title = thread.title ?? sender.displayName
        content.subtitle = platform.displayName
        content.body = message.text ?? ""
        content.threadIdentifier = thread.id.rawValue
        content.categoryIdentifier = PushCategory.message.rawValue

        guard !Task.isCancelled else { return content }

        return content
    }
}
