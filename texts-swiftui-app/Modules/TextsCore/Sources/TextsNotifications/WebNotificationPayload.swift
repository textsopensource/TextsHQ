import Foundation
import TextsBase
import UserNotifications
import MessagePacker
import ECE
import Intents

public struct WebNotificationPayload: NotificationPayloadProtocol {
    private let child: NotificationPayloadProtocol?

    public let registrationID: RegistrationID

    public let accountID: Schema.AccountID
    public var senderID: Schema.UserID
    public var threadID: Schema.ThreadID
    public var messageID: Schema.MessageID

    public var mappedPayload: Schema.MappedNotificationPayload
}

extension WebNotificationPayload {
    init(content: UNNotificationContent) throws {
        guard let id = content.userInfo["TXTID"] as? String else {
            throw NotificationDecodingError.badKey("TXTID")
        }
        self.registrationID = RegistrationID(rawValue: String(id.dropFirst()))
        guard let encryptedStr = content.userInfo["d"] as? String,
              let encrypted = Data(base64Encoded: encryptedStr) else {
            throw NotificationDecodingError.badKey("encryptedData")
        }
        let secrets = try ECE.Secrets.textsCurrent()
        let decrypted: Data
        switch content.userInfo["c"] as? String {
        case "aesgcm":
            guard let encryptionHeader = content.userInfo["e"] as? String,
                  let keyHeader = content.userInfo["k"] as? String else {
                throw NotificationDecodingError.badKey("encryption/crypto-key")
            }
            let parameters = try ECE.AESGCM.Parameters(cryptoKey: keyHeader, encryption: encryptionHeader)
            decrypted = try ECE.AESGCM.decrypt(encrypted, using: secrets, parameters: parameters)
        case "aes128gcm":
            decrypted = try ECE.AES128GCM.decrypt(webPushPayload: encrypted, using: secrets)
        default:
            throw NotificationDecodingError.badKey("content-encoding")
        }

        if RegistrationStorage.shared.registrations[registrationID]?.accountID == "pas" {
            let pas = try MessagePackDecoder().decode(PASNotificationPayload.self, from: decrypted)
            child = pas
            accountID = pas.accountID
            threadID = pas.threadID
            senderID = pas.senderID
            messageID = pas.messageID
            mappedPayload = pas.mappedPayload
            return
        }

        guard let registration = RegistrationStorage.shared.registrations[registrationID]  else {
            throw NotificationDecodingError.badKey("TXTID")
        }

        accountID = registration.accountID
        child = nil

        print("decrypted web notif payload, \(accountID), \(String(decoding: decrypted, as: UTF8.self))")

        // TODO: Handle other platforms later, currently only Twitter supports web
        let decoded = try JSONDecoder().decode(Schema.TwitterNotificationPayload.self, from: decrypted)

        mappedPayload = .init(
            platform: "Twitter",
            thread: .init(
                title: decoded.title,
                message: decoded.data.type == "dm_message_one_to_one_push"
                    ? decoded.body.dropPrefixIfPresent("\(decoded.title): ")
                    : decoded.body,
                imgURL: decoded.icon
            ),
            sender: .init()
        )

        let tag = decoded.tag.split(separator: "-", maxSplits: 1, omittingEmptySubsequences: false)

        threadID = .init(rawValue: tag[1].stringValue)
        senderID = .init(rawValue: "unknown")
        messageID = .init(rawValue: "unknown")
    }

    public func modifiedContent() async -> UNMutableNotificationContent {
        if let child = self.child {
            return await child.modifiedContent()
        }

        let content = UNMutableNotificationContent()

        if let title = mappedPayload.thread.title {
            content.title = title
        }
        if let message = mappedPayload.thread.message {
            content.body = message
        }
        content.subtitle = mappedPayload.platform
        content.threadIdentifier = threadID.rawValue
        content.categoryIdentifier = PushCategory.message.rawValue
        return content
    }
}
