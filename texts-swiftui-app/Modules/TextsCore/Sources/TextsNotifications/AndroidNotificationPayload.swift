import Foundation
import TextsBase
import UserNotifications

public struct AndroidNotificationPayload: NotificationPayloadProtocol {
    public let rawData: Data
    public let registrationID: RegistrationID

    public var accountID: Schema.AccountID
    public var senderID: Schema.UserID
    public var threadID: Schema.ThreadID
    public var messageID: Schema.MessageID

    public var mappedPayload: Schema.MappedNotificationPayload
}

extension AndroidNotificationPayload {
    init(content: UNNotificationContent) throws {
        guard let id = content.userInfo["TXTID"] as? String else {
            throw NotificationDecodingError.badKey("TXTID")
        }
        self.registrationID = RegistrationID(rawValue: String(id.dropFirst()))
        guard let registration = RegistrationStorage.shared.registrations[registrationID] else {
            throw NotificationDecodingError.badKey("TXTID")
        }
        self.accountID = registration.accountID
        let dict = [String: String](uniqueKeysWithValues: content.userInfo.compactMap {
            guard let key = $0 as? String,
                  let value = $1 as? String,
                  key.hasPrefix("$") else { return nil }
            return (String(key.dropFirst()), value)
        })
        rawData = try dict.toJSONData()
        print("RECEIVED PUSH FOR \(registrationID): \(dict)")

        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase

        if let decoded = try? decoder.decode(Schema.InstagramNotificationPayload.self, from: rawData) {
            let (title, _) = decoded.data.m.splitInHalf(separator: ": ")

            self.mappedPayload = .init(
                platform: "Instagram",
                thread: .init(
                    title: title,
                    message: decoded.data.m,
                    imgURL: decoded.data.a
                ),
                sender: .init()
            )

            self.accountID = .init(rawValue: String(decoded.data.u))
            self.senderID = .init(rawValue: decoded.data.s)
            self.threadID = .init(rawValue: "unknown")
            self.messageID = .init(rawValue: decoded.data.n)
        } else if let decoded = try? decoder.decode(Schema.AndroidNotificationPayload.self, from: rawData) {
            self.mappedPayload = .init(
                platform: "Discord",
                thread: .init(
                    title: decoded.userUsername,
                    message: decoded.messageContent
                ),
                sender: .init(name: decoded.userUsername)
            )

            self.accountID = .init(rawValue: "unknown")
            self.senderID = .init(rawValue: "unknown")
            self.threadID = .init(rawValue: "unknown")
            self.messageID = .init(rawValue: "unknown")
        } else if let decoded = try? decoder.decode(Schema.SignalNotificationPayload.self, from: rawData),
                  decoded.notification.isEmpty {
            self.mappedPayload = .init(
                platform: "Signal",
                thread: .init(
                    message: "You have a message"
                ),
                sender: .init()
            )

            self.accountID = .init(rawValue: "unknown")
            self.senderID = .init(rawValue: "unknown")
            self.threadID = .init(rawValue: "unknown")
            self.messageID = .init(rawValue: "unknown")
        } else {
            throw NotificationDecodingError.unknownType
        }
    }

    public func modifiedContent() async -> UNMutableNotificationContent {
        let content = UNMutableNotificationContent()
        if let title = mappedPayload.thread.title {
            content.title = title
        }
        if let message = mappedPayload.thread.message {
            content.body = message
        }
        content.subtitle = mappedPayload.platform
        content.threadIdentifier = threadID.rawValue
        content.categoryIdentifier = PushCategory.message.rawValue
        return content
    }
}
