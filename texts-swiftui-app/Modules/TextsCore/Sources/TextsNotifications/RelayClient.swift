import Foundation
import ECE
import CryptoKit
import KeychainAccess
import TextsBase

private let encoder: JSONEncoder = {
    let encoder = JSONEncoder()
    encoder.dateEncodingStrategy = .javaScriptISO8601
    return encoder
}()

public final class RelayClient {
    public enum APIError: Error {
        case client, server, unknown
    }

    private init() {}
    public static let shared = RelayClient()

    private let textsServer = TextsServer.Repository()

    public enum Kind: String, Encodable {
        case web
        case android
    }

    public func register(
        deviceToken: String,
        for kind: Kind,
        authorizedEntity: String?,
        ttl: TimeInterval? = nil,
        regID: RegistrationID? = nil
    ) async throws -> TextsServer.Responses.PushRelayRegister {
        try await textsServer.pushRelayRegister(.init(
            deviceToken: deviceToken,
            type: kind.rawValue,
            regId: regID?.rawValue,
            authorizedEntity: authorizedEntity,
            ttl: ttl
        ))
    }

    public func unregister(_ regID: RegistrationID) async throws {
        try await textsServer.pushRelayUnregister(.init(regId: regID.rawValue))
//        switch httpResp.statusCode {
//        case 200..<300:
//            break
//        case 400..<500:
//            throw APIError.client
//        case 500..<600:
//            throw APIError.server
//        default:
//            throw APIError.unknown
//        }
    }
}

public struct PushSubscription: Codable {
    public struct Keys: Codable {
        public let auth: String
        public let p256dh: String

        public init(auth: String, p256dh: String) {
            self.auth = auth
            self.p256dh = p256dh
        }

        public init(receiverSecrets: ECE.Secrets) {
            (auth, p256dh) = receiverSecrets.webpushKeys
        }
    }

    public let endpoint: String
    public let keys: Keys
    public let expirationTime: Date?

    public init(endpoint: String, keys: Keys, expirationTime: Date? = nil) {
        self.endpoint = endpoint
        self.keys = keys
        self.expirationTime = expirationTime
    }

    public func jsonToken() throws -> String {
        String(decoding: try encoder.encode(self), as: UTF8.self)
    }
}

extension ECE.Secrets {
    private static let keychain = Keychain(service: "com.texts.push-ece", accessGroup: "W2SW389976.com.texts.keys")

    public static func textsCurrent() throws -> Self {
        if let auth = try keychain.getData("auth"),
           let priv = try keychain.getData("priv") {
            return try Self(privateKey: .init(x963Representation: priv), auth: auth)
        }
        let generated = Self()
        try keychain.set(generated.auth, key: "auth")
        try keychain.set(generated.privateKey.x963Representation, key: "priv")
        return generated
    }
}
