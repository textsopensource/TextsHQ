import Foundation
import UserNotifications
import TextsBase
import Intents

public enum NotificationDecodingError: Error {
    case unknownType
    case badKey(String)
}

public protocol NotificationPayloadProtocol {
    var accountID: Schema.AccountID { get }
    var senderID: Schema.UserID { get }
    var threadID: Schema.ThreadID { get }
    var messageID: Schema.MessageID { get }

    var mappedPayload: Schema.MappedNotificationPayload { get }

    // can return `UNNotificationContent()` to suppress notification
    func modifiedContent() async -> UNMutableNotificationContent
}

public enum NotificationPayload {
    case android(AndroidNotificationPayload)
    case web(WebNotificationPayload)
    case telegram(TelegramNotificationPayload)

    public init(content: UNNotificationContent) throws {
        if let id = content.userInfo["TXTID"] as? String {
            if id.hasPrefix("w") {
                self = .web(try WebNotificationPayload(content: content))
            } else if id.hasPrefix("a") {
                self = .android(try AndroidNotificationPayload(content: content))
            } else {
                throw NotificationDecodingError.unknownType
            }
        } else if content.userInfo["msg_id"] != nil {
            self = .telegram(try TelegramNotificationPayload(content: content))
        } else {
            throw NotificationDecodingError.unknownType
        }
    }

    public var anyPayload: NotificationPayloadProtocol {
        switch self {
        case .telegram(let value):
            return value
        case .web(let value):
            return value
        case .android(let value):
            return value
        }
    }

    @available(iOS 15.0, *)
    private func intent() async throws -> INSendMessageIntent {
        let image: INImage?
        let payload = anyPayload
        let mappedPayload = payload.mappedPayload
        if let urlString = mappedPayload.thread.imgURL, let url = URL(string: urlString) {
            let data = try? await URLSession.shared.dataTaskPublisher(for: url).value.data
            image = data.map(INImage.init(imageData:))
        } else {
            image = nil
        }

        let handle: INPersonHandle
        if let number = mappedPayload.sender.phoneNumber {
            handle = .init(value: number, type: .phoneNumber)
        } else if let email = mappedPayload.sender.email {
            handle = .init(value: email, type: .emailAddress)
        } else {
            handle = .init(value: payload.senderID.rawValue, type: .unknown)
        }
        let person = INPerson(
            personHandle: handle,
            nameComponents: nil,
            displayName: "\(mappedPayload.thread.title ?? "") · \(mappedPayload.platform)",
            image: image,
            contactIdentifier: nil,
            customIdentifier: payload.senderID.rawValue
        )
        // TODO: Add other recipients if we know them
        let intent = INSendMessageIntent(
            recipients: nil,
            outgoingMessageType: .outgoingMessageText,
            content: mappedPayload.thread.message,
            speakableGroupName: mappedPayload.thread.title.map(INSpeakableString.init(spokenPhrase:)),
            conversationIdentifier: payload.threadID.rawValue,
            serviceName: mappedPayload.platform,
            sender: person,
            attachments: nil
        )
        if mappedPayload.thread.type == .group {
            intent.setImage(image, forParameterNamed: \.speakableGroupName)
        }

        let metadata = INSendMessageIntentDonationMetadata()
        if let numParticipants = mappedPayload.thread.numParticipants {
            metadata.recipientCount = numParticipants - 2
        }
        // TODO: configure metadata.*CurrentUser

        // has to be after configuring the metadata since it's
        // @NSCopying
        intent.donationMetadata = metadata

        return intent
    }

    public func modifiedContent() async -> UNNotificationContent {
        let content = await anyPayload.modifiedContent()

        if #available(iOS 15, *) {
            do {
                let intent = try await intent()
                let interaction = INInteraction(intent: intent, response: nil)
                interaction.direction = .incoming
                try await interaction.donate()
                return try content.updating(from: intent)
            } catch {}
        }

        return content
    }
}
