//
// Copyright (c) Texts HQ
//

import FoundationX
import Merge
import UIKit.UIDevice
import TextsBase

public enum FeedbackManager {
    @MainActor
    public static func sendFeedback(
        text: String,
        emotion: String?,
        attachmentURL: URL?
    ) async -> Result<FeedbackRequestResponse, FeedbackError> {
        let accounts = AppState.shared.rootStore.accountsStore.accounts

        guard let activePlatformName = accounts.first?.platform.name else {
            return .failure(FeedbackError.noActivePlatform)
        }

        let connectedPlatforms = accounts.map { $0.platform.name }

        var attachment: SendFeedbackArguments.Attachment?

        if let url = attachmentURL {
            let localAttachment = MessageDraft.LocalAttachment.file(FileLocation(base: .absolute, relativePath: url.path))
            if let data = localAttachment.fileData, let mimeType = localAttachment.utType.preferredMIMEType {
                attachment = .init(type: mimeType, data: data)
            }
        }

        let meta = SendFeedbackArguments.Meta(activePlatform: activePlatformName, connectedPlatforms: connectedPlatforms)
        let args = SendFeedbackArguments(text: text, emotion: emotion, meta: meta, attachment: attachment)

        var request = URLRequest(url: URL(string: "https://texts.com/api/feedback")!)

        request.httpMethod = "POST"
        request.allHTTPHeaderFields = [
            "x-device-id": UIDevice.current.identifierForVendor?.uuidString ?? "unavailable",
            "x-app-version": Bundle.main.version?.description ?? "",
            "x-os": "iOS-\(UIDevice.current.systemVersion)-\(UIDevice.currentDeviceIdentifier)",
            "Content-Type": "application/json",
        ]

        guard let encoded = try? JSONEncoder().encode(args) else {
            return .failure(FeedbackError.errorEncoding)
        }

        request.httpBody = encoded

        do {
            let response = try await URLSession.shared
                .dataTaskPublisher(for: request)
                .tryMap({ try JSONDecoder().decode(FeedbackRequestResponse.self, from: $0.data) })
                .eraseToAnySingleOutputPublisher()
                .value

            return .success(response)
        } catch {
            return .failure(FeedbackError.requestError(error: error))
        }
    }
}

extension FeedbackManager {
    public enum FeedbackError: Error {
        case noActivePlatform
        case errorEncoding
        case requestError(error: Error)

        public var description: String {
            switch self {
            case .noActivePlatform: return "No active platform"
            case .errorEncoding: return "Error encoding"
            case let .requestError(error): return error.localizedDescription
            }
        }
    }

    public struct FeedbackRequestResponse: Decodable {
        public let ok: Bool?
        public let error: String?
    }

    public struct SendFeedbackArguments: Encodable {
        public struct Meta: Encodable {
            public let activePlatform: String
            public let connectedPlatforms: [String]

            public init(activePlatform: String, connectedPlatforms: [String]) {
                self.activePlatform = activePlatform
                self.connectedPlatforms = connectedPlatforms
            }
        }

        public struct Attachment: Encodable {
            public let type: String
            public let data: String

            public init(type: String, data: Data) {
                self.type = type
                self.data = data.base64EncodedString()
            }
        }

        public let text: String
        public let emotion: String?
        public let meta: Meta
        public let attachment: Attachment?

        public init(text: String, emotion: String?, meta: Meta, attachment: Attachment?) {
            self.text = text
            self.emotion = emotion
            self.meta = meta
            self.attachment = attachment
        }
    }
}
