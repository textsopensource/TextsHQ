//
// Copyright (c) Texts HQ
//

import FoundationX
import Merge
import os.log
import UIKit.UIDevice
import TextsBase

public final class AuthorizationManager: CancellablesHolder, ObservableObject {
    public static let shared = AuthorizationManager()

    enum AuthenticationError: String, Error {
        case failedCreatingURL
        case noAuthenticationData
    }

    private let logger = Logger(
        subsystem: Bundle.main.bundleIdentifier!,
        category: "AuthorizationManager"
    )

    private let textsServer = TextsServer.Repository()

    @UserDefault.Published(Preferences.Keys.user)
    public private(set) var user: TextsServer.Schema.AuthenticatedUser?
    @UserDefault.Published(Preferences.Keys.dateOfLastAuthorizationCheck)
    public private(set) var lastCheckedAuthorization: Date?

    public var isLoggedIn: Bool {
        user != nil
    }

    private init() {

    }

    @MainActor
    public func authenticateWithURL(_ url: URL) async throws {
        guard url.scheme == "texts", url.host == "auth", let token = URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.first(where: { $0.name == "token" })?.value else {
            throw AuthenticationError.noAuthenticationData
        }

        try await authenticateWithToken(token)
    }

    /// Authenticate a user in using an authentication token.
    ///
    /// This token is passed by Texts.com from the browser, through a URL scheme.
    @MainActor
    public func authenticateWithToken(_ token: String) async throws {
        do {
            let response = try await textsServer.login(TextsServer.Requests.AppLogin(token: token)).value

            guard let user = response.user else {
                self.logger.error("No authentication data received. This should not be happening.")
                throw AuthenticationError.noAuthenticationData
            }
            self.user = user
            self.lastCheckedAuthorization = Date()

            self.logger.info("Successfully authenticated user with UID: \(user.uid).")
        } catch {
            self.user = nil
        }
    }

    /// Validates the app session and logs out the current user if validation fails.
    @MainActor
    public func validateSession() async {
        let userUID = user?.uid ?? "<null>"

        logger.debug("Validating session...")

        do {
            let data = try await textsServer.validateSession()

            lastCheckedAuthorization = Date()

            if !data.loggedIn {
                logger.info("Failed to validate session, logging out user with UID: \(userUID).")

                await logout(removeAccounts: false)
            } else {
                logger.info("Successfully validated session for user with UID: \(userUID).")
            }
        } catch {
            logger.error("An error occured while attempting to validate the session.")
        }
    }

    /// Log the user out of the current active app session.
    @MainActor
    public func logout(removeAccounts: Bool = true) async {
        guard let user = user else {
            logger.warning("Attempted to log out with no user present.")

            return
        }

        do {
            logger.info("Logging out user with UID: \(user.uid)")

            try await textsServer.logout(TextsServer.Requests.AppLogout()).value

            logger.info("Logged out user with UID: \(user.uid).")

            self.user = nil

            try await AppState.shared.rootStore.accountsStore.removeAllAccounts()
        } catch {
            self.logger.error("Failed to log out user with UID: \(user.uid).")
        }
    }
}
