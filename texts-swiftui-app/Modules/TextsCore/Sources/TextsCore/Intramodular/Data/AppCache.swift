//
// Copyright (c) Texts HQ
//

import Cache
import Foundation
import Merge
import Swallow

public enum AppCache {
    public enum CachingError: Error {
        case couldNotResolveStorage
    }

    private static var cacheStorage: Cache.Storage<String, Data>? = {
        let diskConfig = DiskConfig(
            name: "appCache",
            maxSize: 5 * 1_000_000,
            directory: FileManager.default.temporaryDirectory
        )

        let memoryConfig = MemoryConfig(expiry: .never, countLimit: 50)
        let transformer = TransformerFactory.forData()
        let storage = try? Cache.Storage<String, Data>(diskConfig: diskConfig, memoryConfig: memoryConfig, transformer: transformer)

        return storage
    }()

    @MainActor
    public static func cache<T: Codable>(_ value: T, key: String) async throws {
        guard let asyncStorage = AppCache.cacheStorage?.async else {
            throw CachingError.couldNotResolveStorage
        }

        try await Future<Void, Error> { attemptToFulfill in
            DispatchQueue.global(qos: .userInitiated).async {
                guard let data = try? JSONEncoder().encode(value) else {
                    return
                }

                asyncStorage.setObject(data, forKey: key) { result in
                    switch result {
                    case .error(let error):
                        attemptToFulfill(.failure(error))
                    case .value:
                        attemptToFulfill(.success(()))
                    }
                }
            }
        }
        .value
    }

    public static func retrieve<T: Codable>(_: T.Type, key: String) -> T? {
        let val = try? AppCache.cacheStorage.unwrap().object(forKey: key)

        guard let data = val else {
            return nil
        }

        return try? JSONDecoder().decode(T.self, from: data)
    }
}
