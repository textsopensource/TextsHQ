//
// Copyright (c) Texts HQ
//

import AVFoundation
import Foundation
import SDWebImage
import UIKit
import Swallow

public final actor TextsAttachmentManager {
    public enum AssetError: Error {
        case invalidURL
    }

    private class AssetLoader: NSObject, AVAssetResourceLoaderDelegate {
        let queue = DispatchQueue(label: "asset-loader-queue", qos: .userInitiated)

        private var fullData: [URL: Data] = [:]

        private func load(url: URL) async throws -> Either<URLRequest, Data> {
            if let data = fullData[url] {
                return .right(data)
            }
            let res = try await PlatformServer.shared.fetchAsset(for: url)
            switch res {
            case .url(let urlString):
                guard let url = URL(string: urlString) else {
                    throw AssetError.invalidURL
                }
                return .left(URLRequest(url: url))
            case .data(let data):
                fullData[url] = data
                return .right(data)
            }
        }

        func resourceLoader(
            _ resourceLoader: AVAssetResourceLoader,
            shouldWaitForLoadingOfRequestedResource loadingRequest: AVAssetResourceLoadingRequest
        ) -> Bool {
            guard let url = loadingRequest.request.url, url.scheme == "asset" else { return false }

            Task {
                let res = await Result { try await load(url: url) }
                queue.async {
                    switch res {
                    case .success(.left(let req)):
                        loadingRequest.redirect = req
                        loadingRequest.finishLoading()
                    case .success(.right(let data)):
                        if let infoReq = loadingRequest.contentInformationRequest {
                            infoReq.contentType = UTType(filenameExtensionEx: url.pathExtension)?.identifier
                            infoReq.contentLength = Int64(data.count)
                            infoReq.isByteRangeAccessSupported = true
                        }
                        if let dataReq = loadingRequest.dataRequest {
                            let reqLength = dataReq.requestsAllDataToEndOfResource ?
                                data.count - Int(dataReq.requestedOffset) :
                                dataReq.requestedLength
                            let startIndex = data.startIndex + Int(dataReq.requestedOffset)
                            let endIndex = startIndex + reqLength
                            let respData = data[startIndex..<endIndex]
                            dataReq.respond(with: respData)
                        }
                        loadingRequest.finishLoading()
                    case .failure(let error):
                        loadingRequest.finishLoading(with: error)
                    }
                }
            }

            return true
        }
    }

    private class AssetCacheValue {
        let loader: AssetLoader
        let asset: AVAsset

        init(loader: AssetLoader, asset: AVAsset) {
            self.loader = loader
            self.asset = asset
        }
    }

    public static let shared = TextsAttachmentManager()

    public private(set) var tasksInProgress: [URL: Task<AVAsset, Never>] = [:]

    private let assetCache = NSCache<NSURL, AssetCacheValue>()

    private init() {
        self.assetCache.name = "Attachments Cache"
        self.assetCache.evictsObjectsWithDiscardedContent = true
        self.assetCache.countLimit = 128 // 128 objects
        self.assetCache.totalCostLimit = 128_000_000 // 128 MB
    }

    public func getCachedAsset(url: URL) -> AVAsset? {
        assetCache.object(forKey: url as NSURL)?.asset
    }

    public func videoPreview(url: URL) async throws -> UIImage {
        if let image = SDImageCache.shared.imageFromCache(forKey: url.absoluteString) {
            return image
        }

        let asset = await getAsset(url: url)
        let generator = AVAssetImageGenerator(asset: asset)
        generator.appliesPreferredTrackTransform = true
        let cgImage = try generator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
        let image = UIImage(cgImage: cgImage)
        Task { await SDImageCache.shared.store(image, forKey: url.absoluteString) }
        return image
    }

    public func getAsset(url: URL) async -> AVAsset {
        if let cachedAsset = assetCache.object(forKey: url as NSURL) {
            return cachedAsset.asset
        }

        let task: Task<AVAsset, Never>
        if let existingTask = tasksInProgress[url] {
            task = existingTask
        } else {
            task = Task<AVAsset, Never> {
                let loader = AssetLoader()
                let asset = AVURLAsset(url: url)
                asset.resourceLoader.setDelegate(loader, queue: loader.queue)
                await asset.loadValues(forKeys: ["tracks", "duration"])
                self.assetCache.setObject(AssetCacheValue(loader: loader, asset: asset), forKey: url as NSURL)
                self.tasksInProgress.removeValue(forKey: url)
                return asset
            }
            self.tasksInProgress[url] = task
        }

        return await task.value
    }

    public func clearCache() {
        self.assetCache.removeAllObjects()
    }
}
