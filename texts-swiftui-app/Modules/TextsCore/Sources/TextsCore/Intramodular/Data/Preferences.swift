//
// Copyright (c) Texts HQ
//

import Combine
import FoundationX
import Swallow

public final class Preferences: ObservableObject {
    public enum Keys: UserDefaultKey {
        case user
        case dateOfLastAuthorizationCheck
        case useHaptic
        case enableNotifications
        case themeID
        case stealthMode
        case useLocalPAS
        case showUnreadThreadsFirst
        case accountPreferences
        case privacyModeEnabled
        case autoPrivacyEnabled
        case nativeArchive
        case compactPinnedThreads
        case showTypingIndicator
    }

    public static let shared = Preferences()

    @UserDefault.Published(Keys.useHaptic) public var useHaptic: Bool = true
    @UserDefault.Published(Keys.enableNotifications) public var enableNotifications: Bool = false
    @UserDefault.Published(Keys.themeID) public var themeID: Int = 0
    @UserDefault.Published(Keys.stealthMode) public var stealthMode: Bool = false
    @UserDefault.Published(Keys.useLocalPAS) public var useLocalPAS: Bool = true
    @UserDefault.Published(Keys.showUnreadThreadsFirst) public var showUnreadThreadsFirst: Bool = false
    @UserDefault.Published(Keys.accountPreferences) public var accountPreferences: [Schema.AccountID: [String: Schema.PlatformPref.PrefValue]] = [:]
    @UserDefault.Published(Keys.privacyModeEnabled) public var isPrivacyForScreenCaptureEnabled = false
    @UserDefault.Published(Keys.autoPrivacyEnabled) public var enablePrivacyModeAutomaticallyWhenRecording = false
    @UserDefault.Published(Keys.nativeArchive) public var nativeArchive: Bool = false
    @UserDefault.Published(Keys.compactPinnedThreads) public var compactPinnedThreads: Bool = false
    @UserDefault.Published(Keys.showTypingIndicator) public var showTypingIndicator: Bool = true

    private init() {}
}

extension Preferences {
    enum ColorScheme: Int, CaseIterable {
        case system
        case light
        case dark

        var label: String {
            switch self {
            case .system:
                return "✨ System"
            case .light:
                return "🌞 Light"
            case .dark:
                return "🌚 Dark"
            }
        }
    }
}
