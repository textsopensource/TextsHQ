//
// Copyright (c) Texts HQ
//

import Swallow
import Merge
import os.log
import SDWebImage
import Sentry
import SwiftUIX
import UIKit

public final class AppState: CancellablesHolder, ObservableObject {
    public static let shared = AppState()

    @Published public private(set) var showPlatformIcon: Bool = false

    public let rootStore = RootStore()

    private var cancellables: Set<AnyCancellable> = []
    private let logger = Logger(category: "AppState")

    // MARK: - init

    private init() {
        let loader = SDImageLoadersManager()

        loader.addLoader(SDWebImageDownloader.shared)
        loader.addLoader(AssetImageLoader(server: .shared))

        SDWebImageManager.defaultImageLoader = loader

        setupObservers()
    }
}

extension AppState {

    // MARK: - Utilities

    public func log(_ msg: String, fileName: StaticString = #fileID) {
        logger.debug("\(msg) [\(fileName)]")
    }

    public func handleError(
        _ error: Error,
        alreadyLogged: Bool = false,
        fileName: StaticString = #fileID
    ) {
        SentrySDK.capture(error: error)

        if !alreadyLogged {
            logger.error("[\(fileName)] \(String(describing: error))")
        }
    }

    // MARK: - Cache

    public func clearCache() async {
        _ = await (
            TextsAttachmentManager.shared.clearCache(),
            SDImageCache.shared.clear(with: .all),
            AppDatabase.clearCache()
        )
        logger.info("Done clearing cache!")
    }

    // MARK: - Private functions

    private func setupObservers() {
        rootStore
            .objectWillChange
            .flatMap {
                Task { @MainActor in
                    !self.rootStore.accountsStore.accounts.isEmpty
                }
                .publisher()
            }
            .receiveOnMainQueue()
            .assign(to: \.showPlatformIcon, on: self)
            .store(in: &cancellables)

        NotificationCenter.default.addObserver(
            forName: UIScreen.capturedDidChangeNotification,
            object: nil,
            queue: .main
        ) { notification in
            guard let screen = notification.object as? UIScreen else { return }
            if Preferences.shared.enablePrivacyModeAutomaticallyWhenRecording {
                Preferences.shared.isPrivacyForScreenCaptureEnabled = screen.isCaptured
            }
        }
    }
}
