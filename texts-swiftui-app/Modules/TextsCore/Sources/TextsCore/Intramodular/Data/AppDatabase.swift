//
// Copyright (c) Texts HQ
//

import Foundation
import GRDB
import TextsBase

extension AppDatabase {
    // hack to remove the null values in the json
    // which causes json_patch to remove values
    private static func cleanJSON(_ data: Data) throws -> String {
        // swiftlint:disable:next force_cast
        let serialized = try JSONSerialization.jsonObject(with: data) as! [String: Any]
        let json = try JSONSerialization.data(withJSONObject: serialized.compactMapValues { $0 is NSNull ? nil : $0 })

        return String(decoding: json, as: UTF8.self)
    }
}

extension AppDatabase {
    public static func cachePlatformInfo(_ platforms: [Schema.PlatformInfo]) async {
        do {
            try await Self.shared.writer.write { db in
                for platform in platforms {
                    try db
                        .cachedStatement(literal: """
                        INSERT OR REPLACE INTO platforms (platformName, platformInfo)
                            VALUES(\(platform.name), \(platform.toJSONString().unwrap()))
                        """)
                        .execute()
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func setThreadProps(
        _ props: Schema.ThreadProps,
        threadID: Schema.ThreadID,
        accountID: Schema.AccountID
    ) {
        do {
            try Self.shared.writer.write { db in
                guard let propsJSON = props.toJSONString() else {
                    return
                }

                try db
                    .cachedStatement(literal: """
                        INSERT OR REPLACE INTO thread_props (accountID, threadID, props)
                            VALUES(\(accountID.rawValue), \(threadID.rawValue), \(propsJSON))
                    """)
                    .execute()
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func cacheThread(_ db: Database, thread: Schema.Thread, account: AccountStore) throws {
        guard let platformName = account.platform?.name,
              let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            INSERT OR REPLACE INTO threads (threadID, accountID, platformName, thread)
                VALUES(
                    \(thread.id.rawValue),
                    \(account.id.rawValue),
                    \(platformName),
                    \(thread.toJSONString().unwrap())
                );
            """)
            .execute()

        for message in thread.messages {
            try AppDatabase.cacheMessage(db, message: message, threadID: thread.id, account: account)
        }

        for participant in thread.participants {
            try AppDatabase.cacheUser(db, user: participant, account: account)
        }
    }

    public static func cacheThreads(threads: [Schema.Thread], account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for thread in threads {
                    try Self.cacheThread(db, thread: thread, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func updateCachedThread(_ db: Database, thread: Schema.Thread.Partial, account: AccountStore) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            UPDATE threads SET thread = json_patch(thread, \(Self.cleanJSON(thread.toJSONData())))
                WHERE accountID = \(account.id.rawValue) AND threadID = \(thread.id.rawValue)
            """)
            .execute()

        if let messages = thread.messages {
            for message in messages {
                try AppDatabase.cacheMessage(db, message: message, threadID: thread.id, account: account)
            }
        }

        if let participants = thread.participants {
            for participant in participants {
                try AppDatabase.cacheUser(db, user: participant, account: account)
            }
        }
    }

    public static func updateCachedThreads(threads: [Schema.Thread.Partial], account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for partial in threads {
                    try Self.updateCachedThread(db, thread: partial, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func updateCachedThreadTimestamp(
        _ db: Database,
        threadID: Schema.ThreadID,
        account: AccountStore,
        timestamp: Date
    ) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            UPDATE threads SET thread = json_patch(thread, \("{\"timestamp\":\(timestamp.timeIntervalSinceReferenceDate.toInt())}"))
                WHERE accountID = \(account.id.rawValue) AND threadID = \(threadID.rawValue)
            """)
            .execute()
    }

    public static func deleteCachedThread(_ db: Database, threadID: Schema.ThreadID, account: AccountStore) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        let statements = try db.allStatements(literal: """
            DELETE FROM threads WHERE accountID = \(account.id.rawValue) AND threadID = \(threadID.rawValue);
            DELETE FROM messages WHERE accountID = \(account.id.rawValue) AND threadID = \(threadID.rawValue);
        """)

        while let statement = try statements.next() {
            try statement.execute()
        }
    }

    public static func deleteCachedThreads(threadIDs: Set<Schema.ThreadID>, account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for id in threadIDs {
                    try Self.deleteCachedThread(db, threadID: id, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func cacheMessage(_ db: Database, message: Schema.Message, threadID: Schema.ThreadID, account: AccountStore) throws {
        guard let platformName = account.platform?.name,
              let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            INSERT OR REPLACE INTO messages (messageID, accountID, threadID, platformName, timestamp, message)
                VALUES (
                    \(message.id.rawValue),
                    \(account.id.rawValue),
                    \(threadID.rawValue),
                    \(platformName),
                    \(message.timestamp.timeIntervalSinceReferenceDate.toInt()),
                    \(message.toJSONString().unwrap())
                );
            """)
            .execute()
    }

    public static func cacheMessages(
        messages: [Schema.Message],
        threadID: Schema.ThreadID,
        account: AccountStore
    ) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for message in messages {
                    try Self.cacheMessage(db, message: message, threadID: threadID, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func updateCachedMessage(_ db: Database, message: Schema.Message.Partial, account: AccountStore) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            UPDATE messages SET message = json_patch(message, \(Self.cleanJSON(message.toJSONData()))
                WHERE accountID = \(account.id.rawValue) AND messageID = \(message.id.rawValue)
            """)
            .execute()
    }

    public static func updateCachedMessages(messages: [Schema.Message.Partial], account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for partial in messages {
                    try Self.updateCachedMessage(db, message: partial, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func deleteCachedMessage(_ db: Database, messageID: Schema.MessageID, account: AccountStore) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            DELETE FROM messages WHERE accountID = \(account.id.rawValue) AND messageID = \(messageID.rawValue)
            """)
            .execute()
    }

    public static func deleteCachedMessages(messageIDs: Set<Schema.MessageID>, account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for id in messageIDs {
                    try Self.deleteCachedMessage(db, messageID: id, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func cacheUser(_ db: Database, user: Schema.User, account: AccountStore) throws {
        guard let platformName = account.platform?.name,
              let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            INSERT OR REPLACE INTO users (userID, accountID, platformName, user)
                VALUES (
                    \(user.id.rawValue),
                    \(account.id.rawValue),
                    \(platformName),
                    \(user.toJSONString().unwrap())
                );
            """)
            .execute()
    }

    public static func cacheUsers(users: [Schema.User], account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for user in users {
                    try Self.cacheUser(db, user: user, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func updateCachedUser(_ db: Database, user: Schema.User.Partial, account: AccountStore) throws {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        try db
            .cachedStatement(literal: """
            UPDATE users SET user = json_patch(user, \(user.toJSONString().unwrap())
                WHERE accountID = \(account.id.rawValue) AND userID = \(user.id.rawValue)
            """)
            .execute()
    }

    public static func updateCachedUsers(users: [Schema.User.Partial], account: AccountStore) {
        guard let noCache = account.platform?.attributes.contains(.noCache),
              !noCache else {
            return
        }

        do {
            try Self.shared.writer.write { db in
                for partial in users {
                    try Self.cacheUser(db, user: partial, account: account)
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func changeAccountId(
        oldAccountID: Schema.AccountID,
        newAccountID: Schema.AccountID
    ) async throws {
        let tables = [
            "threads",
            "messages",
            "users",
            "thread_props",
            "message_props",
            "snoozed_messages",
            "snoozed_threads",
        ]

        try await AppDatabase.shared.writer.write { db in
            for table in tables {
                try db
                    .cachedStatement(sql: "UPDATE \(table) SET accountID = ? WHERE accountID = ?")
                    .execute(arguments: [newAccountID.rawValue, oldAccountID.rawValue])
            }
        }
    }

    public static func deleteAccountData(accountID: Schema.AccountID) {
        let tables = [
            "threads",
            "messages",
            "users",
            "thread_props",
            "message_props",
            "snoozed_messages",
            "snoozed_threads",
        ]

        do {
            try AppDatabase.shared.writer.write { db in
                for table in tables {
                    try db
                        .cachedStatement(sql: "DELETE FROM \(table) WHERE accountID = ?")
                        .execute(arguments: [accountID.rawValue])
                }
            }
        } catch {
            AppState.shared.handleError(error)
        }
    }

    public static func clearCache() async {
        let tables = [
            "threads",
            "messages",
            "users",
            "platforms",
        ]
        do {
            try await AppDatabase.shared.writer.write { db in
                for table in tables {
                    try db
                        .cachedStatement(sql: "DELETE FROM \(table)")
                        .execute()
                }
            }

            try await AppDatabase.shared.writer.vacuum()
        } catch {
            AppState.shared.handleError(error)
        }
    }
}
