//
// Copyright (c) Texts HQ
//

import Foundation
import Network
import Swallow

public final class NetworkMonitor: ObservableObject {
    public static let shared = NetworkMonitor()

    public let monitor = NWPathMonitor()
    private let queue = DispatchQueue.global(qos: .background)

    @Published var connected: Bool = false

    init() {
        monitor.start(queue: queue)

        monitor.pathUpdateHandler = { path in
            OperationQueue.main.addOperation {
                self.connected = path.status == .satisfied
            }
        }
    }

    public struct Error {
        public static let NoConnection = CustomStringError(stringLiteral: "No Connection")
    }
}
