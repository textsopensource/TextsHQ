//
// Copyright (c) Texts HQ
//

import Foundation
import UIKit
import Swallow
import TextsNotifications
import TextsBase
import os.log

private let allowedPlatforms = [
    "twitter",
    "signal",
    "telegram"
]

private enum ActionIdentifier: String {
    case reply
}

public protocol PushDelegate: AnyObject {
    var visibleThreadIds: Set<Schema.ThreadID> { get }
    func showThread(with threadUniqueId: String, in scene: UIScene?)
}

public final class PushNotificationManager: NSObject, UNUserNotificationCenterDelegate {
    public struct PermissionDeniedError: LocalizedError, CustomStringConvertible {
        public var description: String {
            "You have denied notification permissions for Texts; you can re-enable them in Settings and try again."
        }

        public var errorDescription: String? { description }
    }

    public enum NotificationsError: Error {
        case registrationFailed
    }

    public static let shared = PushNotificationManager()

    private let logger = Logger(category: "PushNotificationManager")

    public weak var delegate: PushDelegate?

    // keyed by notification request ID
    // TODO: Use an in-memory cache that auto-purges
    @MainActor private var payloads: [String: Result<NotificationPayload, Error>] = [:]

    private var continuation: CheckedContinuation<Data, Error>?
    private var tokenTask: Task<String, Error>?

    public var token: String? {
        get async { try? await tokenTask?.value }
    }

    private override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().setNotificationCategories([
            UNNotificationCategory(
                identifier: PushCategory.message.rawValue,
                actions: [
                    UNTextInputNotificationAction(
                        identifier: ActionIdentifier.reply.rawValue,
                        title: "Reply",
                        options: .authenticationRequired
                    )
                ],
                intentIdentifiers: [],
                hiddenPreviewsBodyPlaceholder: NSString.localizedUserNotificationString(
                    forKey: "MESSAGES_COUNT", arguments: nil
                ),
                categorySummaryFormat: nil,
                options: [
                    .hiddenPreviewsShowTitle,
                    .hiddenPreviewsShowSubtitle
                ]
            )
        ])
        if Preferences.shared.enableNotifications {
            Task { try await enable() }
        }
    }

    /// Only call this from AppDelegate
    @MainActor public func finishRegistration(with result: Result<Data, Error>) {
        guard let continuation = self.continuation else { return }
        continuation.resume(with: result)
        self.continuation = nil
    }

    @MainActor public func setEnabled(_ enabled: Bool) async throws {
        if enabled {
            _ = try await enable()
        } else {
            try await disable()
        }
    }

    @MainActor public func payload(for request: UNNotificationRequest) throws -> NotificationPayload {
        if let decoded = payloads[request.identifier] {
            return try decoded.get()
        }

        let result = Result {
            try NotificationPayload(content: request.content)
        }
        payloads[request.identifier] = result

        return try result.get()
    }
}

extension PushNotificationManager {
    public func handleAccountAddition(_ account: AccountStore) async {
        // if we're using a remote PAS, it manages notifs itself anyway
        guard Preferences.shared.useLocalPAS &&
                Preferences.shared.enableNotifications else { return }
        do {
            let ai = try account.accountInstance()
            guard ai.platform.attributes.contains(.supportsPushNotifications),
                  let deviceToken = await self.token else { return }
            try await registerAccount(account, deviceToken: deviceToken)
        } catch {
            self.logger.error("failed to handle addition of account \(account.id): \("\(error)")")
        }
    }

    public func handleAccountRemoval(_ account: AccountInstance) async {
        guard Preferences.shared.useLocalPAS &&
                account.platform.attributes.contains(.supportsPushNotifications) else { return }
        do {
            try await unregisterAccount(account)
        } catch {
            self.logger.error("failed to handle removal of account \(account.id): \("\(error)")")
        }
    }

    private func registerAccount(_ account: AccountStore, deviceToken: String) async throws {
        let instance = try account.accountInstance()
        guard let notifications = instance.platform.notifications, allowedPlatforms.contains(instance.platform.id) else {
            throw NotificationsError.registrationFailed
        }
        let type: Schema.NotificationKind
        let token: String
        if let apple = notifications.apple {
            _ = apple
            type = .apple
            token = deviceToken
        } else if let web = notifications.web {
            (type, token) = try await RegistrationStorage.shared.updateRegistration(for: account.id) { registration in
                let result = try await RelayClient.shared.register(
                    deviceToken: deviceToken,
                    for: .web,
                    authorizedEntity: web.vapidKey,
                    regID: registration?.id
                )
                let sub = try PushSubscription(
                    endpoint: result.token,
                    keys: .init(receiverSecrets: .textsCurrent()),
                    expirationTime: result.expiry
                )

                let jsonToken = try sub.jsonToken()

                registration = Registration(
                    id: .init(rawValue: result.id),
                    type: .web,
                    accountID: account.id,
                    token: jsonToken
                )
                return (.web, jsonToken)
            }
        } else if let android = notifications.android {
            (type, token) = try await RegistrationStorage.shared.updateRegistration(for: account.id) { registration in
                let existing = RegistrationStorage.shared.inverseRegistrations[account.id]
                let result = try await RelayClient.shared.register(
                    deviceToken: deviceToken,
                    for: .android,
                    authorizedEntity: android.senderID,
                    regID: existing?.id
                )
                registration = Registration(
                    id: .init(rawValue: result.id),
                    type: .android,
                    accountID: account.id,
                    token: result.token
                )
                return (.android, result.token)
            }
        } else {
            throw NotificationsError.registrationFailed
        }

        print("SUBSCRIBING \(account.id): \(token)")
        try await account.registerForPushNotifications(type: type, token: token)
    }

    private func registerAllAccounts(deviceToken: String) async throws {
        _ = await AppState.shared.rootStore.accountsStore.$hasLoadedAccounts.values.first { $0 }
        if let pushRegistrar = PlatformServer.shared.globalPushRegistrar {
            try await RegistrationStorage.shared.updateRegistration(for: "pas") { registration in
                let result = try await RelayClient.shared.register(
                    deviceToken: deviceToken,
                    for: .web,
                    authorizedEntity: nil,
                    regID: registration?.id
                )
                let sub = try PushSubscription(
                    endpoint: result.token,
                    keys: .init(receiverSecrets: .textsCurrent()),
                    expirationTime: result.expiry
                )
                try await pushRegistrar.registerForPushNotifications(with: sub)
                registration = Registration(
                    id: .init(rawValue: result.id),
                    type: .web,
                    accountID: .init(rawValue: "pas"),
                    token: ""
                )
            }
        } else {
            await withTaskGroup(of: Void.self) { group in
                for account in AppState.shared.rootStore.accountsStore.accounts

                where account.platform.attributes.contains(.supportsPushNotifications)
                    && account.state.isActive {
                    group.addTask {
                        do {
                            if let store = try await account.accountStore.values.first(
                                where: { $0.instance?.state.isLoadingThreads == false }
                            ) {
                                try await self.registerAccount(store, deviceToken: deviceToken)
                            }
                        } catch {
                            AppState.shared.handleError(error)
                        }
                    }
                }
            }
        }
    }

    private func unregisterAccount(_ account: AccountInstance) async throws {
        let registration = RegistrationStorage.shared.inverseRegistrations[account.id]

        // swiftlint:disable:next redundant_discardable_let
        async let _ = try await RegistrationStorage.shared.updateRegistration(for: account.id) {
            guard let registration = $0 else { return }
            try await RelayClient.shared.unregister(registration.id)
            $0 = nil
        }

        var type: Schema.NotificationKind = .apple
        if let registrationType = registration?.type, type != registrationType {
            type = registrationType
        }

        let token: String?
        switch type {
        case .apple:
            token = await self.token
        case .web, .android:
            token = registration?.token
        }

        guard let token = token else { return }

        // TODO: If the account is disabled, maybe re-enable and unregister
        // since otherwise the platform's backend will keep delivering
        // notifications
        // However, it's not a *huge* problem since
        // - APNS can be filtered once we have the entitlement
        // - Web/Android is unsubscribed on the FCM side anyway, and a short
        //   ttl will help
        try await account.currentAccountStore?.unregisterForPushNotifications(type: type, token: token)
    }

    private func unregisterAllAccounts() async throws {
        if let pushRegistrar = PlatformServer.shared.globalPushRegistrar {
            try await RegistrationStorage.shared.updateRegistration(for: "pas") {
                guard let registration = $0 else { return }
                try await RelayClient.shared.unregister(registration.id)
                $0 = nil
            }
            _ = pushRegistrar
            // TODO: Unregister with PAS as well once we cache tokens
        } else {
            await withTaskGroup(of: Void.self) { group in
                for account in AppState.shared.rootStore.accountsStore.accounts

                where account.platform.attributes.contains(.supportsPushNotifications) {
                    group.addTask {
                        try? await self.unregisterAccount(account)
                    }
                }
            }
        }
    }

    @discardableResult
    public func enable() async throws -> String {
        // if there's an existing registration task, reuse it
        // but retry if it fails
        if let token = await self.token { return token }
        let registerTask = Task<String, Error> {
            switch await UNUserNotificationCenter.current().notificationSettings().authorizationStatus {
            case .notDetermined:
                let granted = try await UNUserNotificationCenter.current().requestAuthorization(
                    options: [.alert, .badge, .sound]
                )
                guard granted else { throw PermissionDeniedError() }
                fallthrough
            case .provisional, .authorized, .ephemeral:
                async let result = withCheckedThrowingContinuation { continuation = $0 }
                await UIApplication.shared.registerForRemoteNotifications()
                let deviceToken = try await result
                let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
                Task { try await registerAllAccounts(deviceToken: token) }
                return token
            case .denied:
                fallthrough
            @unknown default:
                throw PermissionDeniedError()
            }
        }
        self.tokenTask = registerTask
        return try await registerTask.value
    }

    @MainActor private func disable() async throws {
        UIApplication.shared.applicationIconBadgeNumber = 0
        defer { tokenTask = nil }
        try await unregisterAllAccounts()
    }
}

// Notification handlers
extension PushNotificationManager {

    @MainActor
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification
    ) async -> UNNotificationPresentationOptions {
        let threadID: Schema.ThreadID?
        do {
            threadID = try payload(for: notification.request).anyPayload.threadID
        } catch {
            AppState.shared.handleError(error)
            threadID = nil
        }

        if let threadID = threadID, delegate?.visibleThreadIds.contains(threadID) == true {
            return []
        } else {
            return [.banner, .sound]
        }
    }

    @MainActor
    private func sendReply(_ response: UNTextInputNotificationResponse, payload: NotificationPayload) async throws {
        let accountsStore = AppState.shared.rootStore.accountsStore
        let messageID = payload.anyPayload.messageID
        let accountID = payload.anyPayload.accountID
        guard let account = accountsStore.accounts[id: accountID] else {
            return
        }
        guard let thread = account.currentAccountStore?.getThreadFromAllInboxes(payload.anyPayload.threadID) else {
            return
        }
        var draft = MessageDraft()
        draft.messageText = response.userText
        if account.platform.attributes.contains(.supportsQuotedMessages) {
            draft.quotedMessage = .sameThread(messageID)
        }
        try await thread.sendMessage(messageDraft: draft)
    }

    @MainActor
    public func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse
    ) async {
        let request = response.notification.request
        let payload: NotificationPayload
        do {
            payload = try self.payload(for: request)
        } catch {
            AppState.shared.handleError(error)
            return
        }

        switch response.actionIdentifier {
        case UNNotificationDefaultActionIdentifier:
            delegate?.showThread(with: "\(payload.anyPayload.accountID):\(payload.anyPayload.threadID)", in: response.targetScene)
            return

        case UNNotificationDismissActionIdentifier:
            return
        default:
            break
        }

        switch ActionIdentifier(rawValue: response.actionIdentifier) {
        case .reply:
            guard let textResponse = response as? UNTextInputNotificationResponse else {
                // wat
                return
            }
            Task {
                try await sendReply(textResponse, payload: payload)
            }
        case nil:
            return
        }
    }

    // TODO: Implement
    //    public func userNotificationCenter(
    //        _ center: UNUserNotificationCenter,
    //        openSettingsFor notification: UNNotification?
    //    ) {
    //
    //    }

}
