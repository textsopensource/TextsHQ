//
// Copyright (c) Texts HQ
//

import Foundation
import KeychainAccess

extension Keychain {
    public static let texts = Keychain(service: Bundle.main.bundleIdentifier!)
        .accessibility(.afterFirstUnlock)
        .synchronizable(false)
}
