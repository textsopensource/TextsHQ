//
// Copyright (c) Texts HQ
//

import Foundation

public enum LayoutConstants {
    public static let GAPPED_1_DURATION: TimeInterval = 1.0 * 60 // 1 min
    public static let GAPPED_2_DURATION: TimeInterval = 5.0 * 60 // 5 min
    public static let GAPPED_3_DURATION: TimeInterval = 4.0 * 60 * 60 // 4 hours
}
