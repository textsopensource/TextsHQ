//
// Copyright (c) Texts HQ
//

import Foundation

extension NSMutableAttributedString {
    func trimmingCharacters(in set: CharacterSet) -> NSMutableAttributedString {
        // swiftlint:disable:next force_cast
        let copy = self.mutableCopy() as! NSMutableAttributedString

        while let range = copy.string.rangeOfCharacter(from: set), range.lowerBound == copy.string.startIndex {
            let length = copy.string.distance(from: range.lowerBound, to: range.upperBound)

            copy.deleteCharacters(in: NSRange(location: 0, length: length))
        }

        while let range = copy.string.rangeOfCharacter(from: set, options: .backwards), range.upperBound == copy.string.endIndex {
            let location = copy.string.distance(from: copy.string.startIndex, to: range.lowerBound)
            let length = copy.string.distance(from: range.lowerBound, to: range.upperBound)

            copy.deleteCharacters(in: NSRange(location: location, length: length))
        }

        return copy
    }
}
