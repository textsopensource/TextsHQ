//
// Copyright (c) Texts HQ
//

import UniformTypeIdentifiers
import SDWebImage
import UIKit

/// An image text attachment that gets loaded from a remote URL
public class AsyncTextAttachment: NSTextAttachment {
    /// Image tint color
    let tintColor: UIColor

    /// Remote URL for the image
    public var imageURL: URL?

    /// To specify an absolute display size.
    public var displaySize: CGSize?

    /// if determining the display size automatically this can be used to specify a maximum width. If it is not set then the text container's width will be used
    public var maximumDisplayWidth: CGFloat?

    /// Remember the text container from delegate message, the current one gets updated after the download
    weak var textContainer: NSTextContainer?

    /// The size of the downloaded image. Used if we need to determine display size
    private var originalImageSize: CGSize?

    /// Designated initializer
    public init(imageURL: URL? = nil, tintColor: UIColor? = nil) {
        self.imageURL = imageURL
        self.tintColor = tintColor ?? .label
        super.init(data: nil, ofType: nil)
    }

    @available(*, unavailable)
    public required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public var image: UIImage? {
        didSet {
            self.originalImageSize = image?.size
        }
    }

    override public func image(forBounds _: CGRect, textContainer: NSTextContainer?, characterIndex _: Int) -> UIImage? {
        if let image = self.image { return image }

        guard let contents = self.contents, let image = UIImage(data: contents) else {
            // Remember reference so that we can update it later
            self.textContainer = textContainer

            // Load image
            loadImage()

            // Return placeholder
            return UIImage(systemName: "eyes.inverse")?.withTintColor(tintColor)
        }

        return image
    }

    override public func attachmentBounds(for _: NSTextContainer?, proposedLineFragment lineFrag: CGRect, glyphPosition _: CGPoint, characterIndex _: Int) -> CGRect {
        if let displaySize = displaySize {
            return CGRect(origin: CGPoint.zero, size: displaySize)
        }

        if let imageSize = originalImageSize {
            let maxWidth = maximumDisplayWidth ?? lineFrag.size.width
            let factor = maxWidth / imageSize.width

            return CGRect(origin: CGPoint.zero, size: CGSize(width: Int(imageSize.width * factor), height: Int(imageSize.height * factor)))
        }

        return CGRect.zero
    }

    // MARK: - Helpers

    private func loadImage() {
        guard let imageURL = imageURL, contents == nil else {
            return
        }

        SDWebImageManager.shared.loadImage(with: self.imageURL, options: [.decodeFirstFrameOnly], progress: nil) { image, data, error, _, _, _ in
            guard let data = data ?? image?.pngData(), error == nil else {
                return
            }

            var displaySizeChanged = false

            self.contents = data

            self.fileType = UTType(filenameExtension: imageURL.pathExtension)?.identifier

            if let image = image ?? UIImage(data: data) {
                let imageSize = image.size

                if self.displaySize == nil {
                    displaySizeChanged = true
                }

                self.originalImageSize = imageSize
            }

            // Tell layout manager so that it should refresh
            DispatchQueue.main.async {
                if displaySizeChanged {
                    self.textContainer?.layoutManager?.setNeedsLayout(forAttachment: self)
                } else {
                    self.textContainer?.layoutManager?.setNeedsDisplay(forAttachment: self)
                }
            }
        }
    }
}

// MARK: - Auxiliary Implementation

extension NSLayoutManager {
    /// Determine the character ranges for an attachment
    private func rangesForAttachment(attachment: NSTextAttachment) -> [NSRange]? {
        guard let attributedString = self.textStorage else {
            return nil
        }

        // find character range for this attachment
        let range = NSRange(location: 0, length: attributedString.length)
        var refreshRanges: [NSRange] = []

        attributedString.enumerateAttribute(.attachment, in: range, options: []) { value, effectiveRange, _ in
            guard let foundAttachment = value as? NSTextAttachment, foundAttachment == attachment else {
                return
            }

            // Add this range to the refresh ranges
            refreshRanges.append(effectiveRange)
        }

        if refreshRanges.isEmpty {
            return nil
        }

        return refreshRanges
    }

    /// Trigger a relayout for an attachment
    fileprivate func setNeedsLayout(forAttachment attachment: NSTextAttachment) {
        guard let ranges = rangesForAttachment(attachment: attachment) else {
            return
        }

        // Invalidate the display for the corresponding ranges
        for range in ranges.reversed() {
            self.invalidateLayout(forCharacterRange: range, actualCharacterRange: nil)

            // Also need to trigger re-display or already visible images might not get updated
            self.invalidateDisplay(forCharacterRange: range)
        }
    }

    /// Trigger a re-display for an attachment
    fileprivate func setNeedsDisplay(forAttachment attachment: NSTextAttachment) {
        guard let ranges = rangesForAttachment(attachment: attachment) else {
            return
        }

        // Invalidate the display for the corresponding ranges
        for range in ranges.reversed() {
            self.invalidateDisplay(forCharacterRange: range)
        }
    }
}
