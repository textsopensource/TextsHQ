//
// Copyright (c) Texts HQ
//

import SwiftUI
import UniformTypeIdentifiers

public enum Globals {
    public enum Views {
        public static let navigationBarCircleSize: CGFloat = 34
        public static let cornerRadius: CGFloat = 14
        public static let smallCornerRadius: CGFloat = 8
        public static let candyOpacity: Double = 0.12
        public static let secondaryOpacity: Double = 0.3
        public static let animation = Animation.interpolatingSpring(stiffness: 250, damping: 30)

        // Messages
        public static let messageCornerRadius: CGFloat = 16
        public static let messageSmallCornerRadius: CGFloat = 8
    }

    public enum Buttons {
        public static let pressedOpacity: Double = 0.75
        public static let pressedSize: CGFloat = 0.975
    }

    public enum Strings {
        public static let defaultUserAgent = """
        Mozilla/5.0 (Macintosh; Intel Mac OS X 12_3_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.3 Safari/605.1.15
        """
        public static let imageAttachmentPrefix: String = "🖼"
        public static let videoAttachmentPrefix: String = "🎥"
        public static let audioAttachmentPrefix: String = "🎵"
        public static let fileAttachmentPrefix: String = "📄"
        public static let genericAttachmentPrefix: String = "📎"
        public static let gifAttachmentPrefix: String = "🎬"
        public static let contactAttachmentPrefix: String = "👤"
        public static let deletedMessagePrefix: String = "🗑"
        public static let tweetMessagePrefix: String = "🐦"
    }

    public enum Icons {
        public static let texts = """
        <svg width="40px" height="40px" viewBox="0 0 10 10">
            <path d="M0 5C0 2.23858 2.23858 0 5 0C7.76142 0 10 2.23858 10 5C10 7.76142 7.76142 10 5 10H1C0.447715 10 0 9.55229 0 9V5Z" fill="#000000" />
        </svg>
        """
    }

    public enum Other {
        public static let allowedContentTypes: [UTType] = [
            .audiovisualContent,
            .movie,
            .video,
            .audio,
            .quickTimeMovie,
            .mpeg,
            .mpeg2Video,
            .mpeg2TransportStream,
            .mp3,
            .mpeg4Movie,
            .mpeg4Audio,
            .appleProtectedMPEG4Video,
            .appleProtectedMPEG4Audio,
            .avi,
            .aiff,
            .wav,
            .midi,
            .playlist,
            .m3uPlaylist,
            .image,
            .jpeg,
            .png,
            .icns,
            .gif,
            .tiff,
            .bmp,
            .ico,
            .svg,
            .rawImage,
            .heic,
            .heif,
            .webP,
            .livePhoto,
            .pdf,
            .rtfd,
            .flatRTFD,
            .epub,
            .archive,
            .zip,
            .gzip,
            .bz2,
            .text,
            .plainText,
            .utf8PlainText,
            .utf16PlainText,
            .utf16ExternalPlainText,
            .delimitedText,
            .commaSeparatedText,
            .tabSeparatedText,
            .utf8TabSeparatedText,
            .rtf,
            .xml,
            .yaml,
            .json,
            .propertyList,
            .xmlPropertyList,
            .binaryPropertyList,
            .vCard,
            .pkcs12,
            .x509Certificate,
            .html,
            .webArchive,
            .internetLocation,
            .internetShortcut,
            .spreadsheet,
            .presentation,
            .database,
            .message,
            .contact,
            .calendarEvent,
            .toDoItem,
            .emailMessage,
            .font,
            .threeDContent,
            .usd,
            .usdz,
            .realityFile,
            .sceneKitScene,
            .arReferenceObject,
            .sourceCode,
            .assemblyLanguageSource,
            .cHeader,
            .cSource,
            .cPlusPlusHeader,
            .cPlusPlusSource,
            .objectiveCPlusPlusSource,
            .objectiveCSource,
            .swiftSource,
            .script,
            .appleScript,
            .javaScript,
            .osaScript,
            .osaScriptBundle,
            .shellScript,
            .pythonScript,
            .rubyScript,
            .perlScript,
            .phpScript,
            .item,
            .content,
            .compositeContent,
            .diskImage,
            .data,
            .directory,
            .resolvable,
            .symbolicLink,
            .executable,
            .mountPoint,
            .aliasFile,
            .urlBookmarkData,
            .url,
            .fileURL,
            .folder,
            .volume,
            .package,
            .bundle,
            .pluginBundle,
            .spotlightImporter,
            .quickLookGenerator,
            .xpcService,
            .framework,
            .application,
            .applicationBundle,
            .applicationExtension,
            .unixExecutable,
            .exe,
            .systemPreferencesPane,
            .bookmark,
            .log
        ]
    }
}
