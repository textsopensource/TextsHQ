//
// Copyright (c) Texts HQ
//

import Foundation

public struct DecodableVoid: Decodable {
    public init() {}
}
