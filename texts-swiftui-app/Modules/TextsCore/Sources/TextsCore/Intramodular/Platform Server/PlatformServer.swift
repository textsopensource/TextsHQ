//
// Copyright (c) Texts HQ
//

import Merge
import os.log
import Swallow
import UIKit.UIDevice
import TextsNotifications

@_exported import TextsBase

typealias PASPush = (Schema.PushEventBase, Data)

public struct PASOptions: Encodable {
    let machineID: String
    let userDataDirPath: String
}

protocol GlobalPushRegistrar {
    func registerForPushNotifications(with subscription: PushSubscription) async throws
    func unregisterForPushNotifications(with subscription: PushSubscription) async throws
}

protocol PASBridge: AnyObject {
    init(options: PASOptions) async throws

    var platforms: [Schema.PlatformInfo] { get }
    var pushes: AsyncThrowingStream<PASPush, Error> { get }
    var globalPushRegistrar: GlobalPushRegistrar? { get }

    func call(
        _ method: String,
        on account: AccountInstance,
        with args: Data
    ) async throws -> Data
}

extension PASBridge {
    var globalPushRegistrar: GlobalPushRegistrar? { nil }
}

public final class PlatformServer: CancellablesHolder, ObservableObject {
    public enum Status {
        case notLaunched
        case launching
        case launched
        case exited

        public var canLaunch: Bool {
            self == .notLaunched || self == .exited
        }
    }

    enum State {
        case notLaunched
        case launching
        case launched(PASBridge)
        case exited(Error?)

        var status: Status {
            switch self {
            case .notLaunched:
                return .notLaunched
            case .launching:
                return .launching
            case .launched:
                return .launched
            case .exited:
                return .exited
            }
        }

        var bridge: PASBridge? {
            switch self {
            case .launched(let bridge):
                return bridge
            default:
                return nil
            }
        }
    }

    public static let shared = PlatformServer()

    @Published var state: State = .notLaunched

    public var status: Status { state.status }

    public var options: PASOptions?

    var globalPushRegistrar: GlobalPushRegistrar? {
        state.bridge?.globalPushRegistrar
    }

    let logger = Logger(category: "PlatformServer")
    let signposter = Signposters.create(category: "PlatformServer")

    private var subscriptionTasks: [Schema.PushEventBase: AnyTask<Data, Error>] = [:]

    // MARK: - init

    private init() {
        logger.info("init()")

        $state
            .filter { $0.status == .exited }
            .sink(in: cancellables) { _ in
                self.logger.error("Launching failed!")

                AppState.shared.handleError(PlatformServerError.launchingFailed(nil))
            }
    }

    // MARK: - Public functions

    /// Starts the PlatformServer, waits until its ready and connects to WebSocket.
    public func start() {
        guard status.canLaunch else {
            logger.info("Node server already running!")
            return
        }

        let interval = signposter.beginInterval("PASStartup")

        state = .launching
        logger.info("Launching...")

        // swiftlint:disable:next force_try
        let userDataDirPath = try! FileManager.default
            .url(for: .documentDirectory, in: .userDomainMask)
            .appendingPathComponent("workerData")

        try? FileManager.default.createDirectory(at: userDataDirPath, withIntermediateDirectories: true)

        options = PASOptions(
            machineID: UIDevice.current.identifierForVendor!.uuidString,
            userDataDirPath: userDataDirPath.path
        )

        Task { @MainActor in
            let bridge: PASBridge
            do {
                defer { self.signposter.endInterval("PASStartup", interval) }
                if Preferences.shared.useLocalPAS {
                    bridge = try await PASBridgeNode(options: options.unwrap())
                } else {
                    bridge = try await PASBridgeRemote(options: options.unwrap())
                }
            } catch {
                self.logger.error("Node exited with error: \("\(error)")")
                self.state = .exited(error)
                return
            }

            self.logger.log("PAS launched!")
            self.state = .launched(bridge)
            self.objectWillChange.send()

            await AppDatabase.cachePlatformInfo(bridge.platforms)
            AppState.shared.rootStore.platformsStore.initialize(with: bridge.platforms)

            var bridgeError: Error?
            do {
                for try await _ in bridge.pushes {}
            } catch {
                bridgeError = error
            }
            state = .exited(bridgeError)
        }
    }
}

extension PlatformServer {
    // returns empty Data if PAS method returns undefined
    public func callAnyMethod(
        _ methodName: String,
        on account: AccountInstance,
        with args: Data
    ) async throws -> Data {
        guard NetworkMonitor.shared.connected else {
            throw NetworkMonitor.Error.NoConnection
        }

        let start = Date()
        logger.info("↑ Call \(account.platform.name).\(methodName)")
        defer {
            logger.info("↓ Return \(account.platform.name).\(methodName) in \(Int(start.timeIntervalSinceNow * -1000))ms")
        }

        guard let caller = state.bridge else {
            logger.error("Attempted to call method on uninitialized PAS")
            throw PlatformServerError.notLaunched
        }

        do {
            return try await caller.call(methodName, on: account, with: args)
        } catch {
            AppState.shared.handleError(error)
            throw error
        }
    }

    public func callAnyMethod(
        _ methodName: String,
        on account: AccountInstance,
        with args: Data
    ) -> AnyTask<Data, Error> {
        Task { try await callAnyMethod(methodName, on: account, with: args) }
            .publisher()
            .convertToTask()
    }

    @MainActor
    public func fetchAsset(for url: URL) async throws -> Schema.Asset {
        guard url.scheme == "asset", let accountID = url.host.map(Schema.AccountID.init(rawValue:)) else {
            throw CustomStringError(description: "The URL is not a valid asset:// URL")
        }

        guard let account = AppState.shared.rootStore.accountsStore.accounts[id: accountID] else {
            throw CustomStringError(description: "Unknown account: \(accountID)")
        }

        var components: [String?] = url.pathComponents
        if components.first == "/" {
            components.removeFirst()
        }
        components.insert(nil, at: 0) // fetchOptions
        let payload: Data
        do {
            payload = try Self.makeEncoder().encode(components)
        } catch {
            throw error
        }

        return try await callAnyMethod("getAsset", on: account, with: payload)
            .successPublisher
            .tryMap({ try Self.makeDecoder().decode(Schema.Asset.self, from: $0) })
            .value
    }

    public func anyPublisher(
        for methodName: String,
        on account: AccountInstance
    ) -> AnyPublisher<Data, Error> {
        guard let bridge = state.bridge else {
            return .failure(PlatformServerError.notLaunched)
        }

        let eventBase = Schema.PushEventBase(
            accountID: account.id,
            methodName: methodName
        )

        // this isn't just memoization for efficiency; it might actually be invalid
        // to call the given method more than once on the underlying platform, so
        // we reuse the publisher for subsequent calls to avoid invoking callMethod
        // again
        let targetTask: AnyTask<Data, Error>
        switch subscriptionTasks[eventBase] {
        // if the last call succeeded, skip callMethod entirely
        case let task? where task.status == .success:
            targetTask = .success(.init())
        // if the last call failed or this is the first call to the method on the account,
        // invoke callMethod and save the task for future use
        case let task? where task.status == .error || task.status == .canceled:
            // swiftlint:disable:next no_fallthrough_only
            fallthrough
        case nil:
            let payload: Data
            do {
                payload = try Self.makeEncoder().encode([] as [String])
            } catch {
                return .failure(error)
            }
            targetTask = callAnyMethod(methodName, on: account, with: payload)
            subscriptionTasks[eventBase] = targetTask
        // the task is paused/progressing/idle; reuse it
        case let task?:
            targetTask = task
        }

        return targetTask
            .successPublisher
            .flatMap { _ in
                bridge.pushes
                    .filter { $0.0 == eventBase }
                    .map(\.1)
                    .publisher()
            }
            .eraseToAnyPublisher()
    }

    public func publisher<Event: Decodable>(
        for methodName: Schema.CallbackMethod,
        on account: AccountInstance
    ) -> AnyPublisher<Event, Error> {
        anyPublisher(for: methodName.rawValue, on: account)
            // we avoid tryMap here because that would result in any decoding error
            // terminating the entire stream
            .compactMap {
                do {
                    return try Self.makeDecoder().decode(Schema.PushEvent<Event>.self, from: $0).cbData
                } catch {
                    self.logger.error("Failed to handle push for \(methodName, privacy: .private): \("\(error)")")
                    return nil
                }
            }
            .eraseToAnyPublisher()
    }

    // this is necessary because when an account is disposed and re-enabled,
    // we need to re-subscribe to callbacks but if the old subscriptionTasks
    // keys still existed then PAS would think we're already subscribed
    func removeSubscriptions(forAccountID accountID: Schema.AccountID) {
        subscriptionTasks.keys.filter { $0.accountID == accountID }.forEach {
            subscriptionTasks.removeValue(forKey: $0)
        }
    }
}
