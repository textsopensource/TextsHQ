import Foundation
import MessagePacker

extension PlatformServer {

    public static func makeEncoder() -> MessagePackEncoder {
        .init()
    }

    public static func makeDecoder() -> MessagePackDecoder {
        .init()
    }

    // we can't store a single encoder and decoder because
    // MessagePack[En|De]coder aren't thread-safe
//    public static let encoder = makeEncoder()
//    public static let decoder = makeDecoder()

//    private struct Buffer: Codable {
//        private enum Kind: String, Codable {
//            case buffer = "Buffer"
//        }
//        private let type: Kind
//        private let data: [UInt8]
//
//        init(value: Data) {
//            type = .buffer
//            data = Array(value)
//        }
//
//        var value: Data { Data(data) }
//    }
//
//    private static let formatter: ISO8601DateFormatter = {
//        let formatter = ISO8601DateFormatter()
//        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds, .withTimeZone]
//        return formatter
//    }()
//
//    public static let encoder = JSONEncoder(dateEncodingStrategy: .custom { date, encoder in
//        var container = encoder.singleValueContainer()
//        try container.encode(formatter.string(from: date))
//    }, dataEncodingStrategy: .custom { data, encoder in
//        var container = encoder.singleValueContainer()
//        try container.encode(Buffer(value: data))
//    })
//
//    public static let decoder = JSONDecoder(dateDecodingStrategy: .custom { decoder in
//        let container = try decoder.singleValueContainer()
//        let isoDate = try container.decode(String.self)
//        guard let date = formatter.date(from: isoDate) else {
//            throw DecodingError.dataCorruptedError(in: container, debugDescription: "Could not decode ISO8601 date from '\(isoDate)'")
//        }
//        return date
//    }, dataDecodingStrategy: .custom {
//        try $0.singleValueContainer().decode(Buffer.self).value
//    })

}
