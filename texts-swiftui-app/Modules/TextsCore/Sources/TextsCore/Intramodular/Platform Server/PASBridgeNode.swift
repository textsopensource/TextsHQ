import Foundation
import NodeAPI
import Merge
import Swallow
import PASMobile
import PASShared
import Atomics

private final class PASMethodCaller {
    // a Handle reference keeps the Node runtime alive
    let queueHandle: NodeAsyncQueue.Handle
    private let callMethod: NodeFunction

    @NodeActor fileprivate init(queueHandle: NodeAsyncQueue.Handle, callMethod: NodeFunction) {
        self.queueHandle = queueHandle
        self.callMethod = callMethod
    }

    @NodeActor func call(
        _ methodName: String,
        with args: Data,
        platformName: String,
        accountID: Schema.AccountID
    ) async throws -> Data {
        let res = try self.callMethod(NodeObject([
            "platformName": platformName,
            "accountID": accountID.rawValue,
            "methodName": methodName,
            "args": args
        ]))
        guard let promise = try res.as(NodePromise.self) else {
            throw PlatformServerError.methodFailed(methodName)
        }
        do {
            return try await promise.value.as(Data.self) ?? Data()
        } catch let error as NodeValue {
            let nodeError = try? error.as(NodeError.self)
            let message = try? nodeError?.message.as(String.self)
            let name = try? nodeError?.name.as(String.self)
            let stack = try? nodeError?.stack.as(String.self)
            throw Schema.MethodError(
                platformName: platformName,
                methodName: methodName,
                name: name,
                message: message ?? "\(error)",
                stack: stack
            )
        } catch {
            throw PlatformServerError.methodFailed(methodName)
        }
    }
}

private final class AtomicPromise<T>: @unchecked Sendable {
    private let hasCompleted = ManagedAtomic<Bool>(false)

    let promise: (Result<T, Error>) -> Void
    init(promise: @escaping (Result<T, Error>) -> Void) {
        self.promise = promise
    }

    func complete(with result: Result<T, Error>) {
        let wasCompleted = hasCompleted.compareExchange(
            expected: false,
            desired: true,
            ordering: .acquiringAndReleasing
        ).original
        guard !wasCompleted else { return }
        promise(result)
    }
}

final class PASBridgeNode: PASBridge {
    private typealias PASPromise = AtomicPromise<([Schema.PlatformInfo], PASMethodCaller)>

    private final class Adapter: NodeClass {
        static let properties: NodeClassPropertyList = [
            "didLaunch": NodeMethod(didLaunch),
            "didFail": NodeMethod(didFail),
            "push": NodeMethod(push),
        ]

        let promise: PASPromise
        let subject: PassthroughSubject<PASPush, Error>

        nonisolated init(promise: PASPromise, subject: PassthroughSubject<PASPush, Error>) {
            self.promise = promise
            self.subject = subject
        }

        func exports() throws -> NodeValueConvertible {
            ["bridge": self]
        }

        func didLaunch(_ args: NodeArguments) throws -> NodeValueConvertible {
            do {
                // TODO: Try to avoid extra copying data here
                guard args.count == 2,
                      let platformInfoMap = try args[0].as([Data].self),
                      let callMethod = try args[1].as(NodeFunction.self)
                else { return undefined }
                let infos = try platformInfoMap.map {
                    try PlatformServer.makeDecoder().decode(Schema.PlatformInfo.self, from: $0)
                }
                let q = try NodeAsyncQueue(label: "call-pas-method")
                let caller = try PASMethodCaller(queueHandle: q.handle(), callMethod: callMethod)
                promise.complete(with: .success((infos, caller)))
            } catch {
                subject.send(completion: .failure(error))
            }
            return undefined
        }

        private func didFail(_ error: AnyNodeValue) throws -> NodeValueConvertible {
            let failure = PlatformServerError.launchingFailed("\(error)")
            promise.complete(with: .failure(failure))
            subject.send(completion: .failure(failure))
            return undefined
        }

        func push(_ data: Data) throws -> NodeValueConvertible {
            let ev = try PlatformServer.makeDecoder().decode(Schema.PushEventBase.self, from: data)
            subject.send((ev, data))
            return undefined
        }
    }

    private let adapter: Adapter
    private let allPushes: PassthroughSubject<PASPush, Error>
    private let caller: PASMethodCaller
    let platforms: [Schema.PlatformInfo]

    init(options: PASOptions) async throws {
        #if DEBUG
        setenv("NODE_ENV", "development", 1)
        #endif

        var promise: PASPromise!
        let future = Future { promise = .init(promise: $0) }
        let subject = PassthroughSubject<PASPush, Error>()
        adapter = .init(promise: promise, subject: subject)
        self.allPushes = subject

        let optionsData = try JSONEncoder().encode(options)
        let options = String(decoding: optionsData, as: UTF8.self)

        let thread = Thread {
            var _error: Error?
            do {
                try PASRunner.run(withArguments: [options])
            } catch {
                _error = error
            }
            Task { @MainActor [_error, promise] in
                promise!.complete(with: .failure(_error ?? PlatformServerError.launchingFailed(nil)))
                subject.send(completion: _error.map { .failure($0) } ?? .finished)
            }
        }
        thread.name = "com.texts.NodeThread"
        thread.qualityOfService = .userInteractive
        thread.stackSize = 2 * 1024 * 1024

        // Note: this must be set before the thread starts, because once Node runs
        // it requires the PASNative module, which calls this method synchronously
        PASInit = adapter.exports as AnyObject

        thread.start()

        (platforms, caller) = try await future.value

        // bump down the priority once the initial load is complete
        thread.qualityOfService = .userInitiated
    }

    func call(
        _ method: String,
        on account: AccountInstance,
        with args: Data
    ) async throws -> Data {
        try await caller.queueHandle.queue.run {
            try await caller.call(method, with: args, platformName: account.platform.name, accountID: account.id)
        }
    }

    var pushes: AsyncThrowingStream<PASPush, Error> {
        allPushes.values
    }
}
