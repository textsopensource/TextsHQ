//
// Copyright (c) Texts HQ
//

import Foundation
import Swallow
import UIKit.UIImagePickerController
import UniformTypeIdentifiers

public struct MessageDraft: Equatable, Codable {
    public enum LocalAttachment: Identifiable, Equatable, Codable {
        case file(FileLocation)
        case gif(FileLocation, Schema.Size?)

        public var id: FileLocation {
            location
        }

        public var location: FileLocation {
            switch self {
            case .file(let location):
                return location
            case .gif(let location, _):
                return location
            }
        }

        public var fileData: Data? {
            guard let url = fileURL else { return nil }
            return try? Data(contentsOf: url)
        }

        public var fileName: String? {
            fileURL?.lastPathComponent
        }

        public var fileURL: URL? {
            switch self {
            case let .file(location):
                return location.url
            case let .gif(location, _):
                return location.url
            }
        }

        public var utType: UTType {
            fileURL.map(\.pathExtension).flatMap(UTType.init(filenameExtensionEx:)) ?? .item
        }

        public var isGif: Bool {
            switch self {
            case .gif:
                return true
            default:
                return false
            }
        }

        public var size: Schema.Size? {
            switch self {
            case let .gif(_, size):
                return size
            default:
                return nil
            }
        }

        // MARK: - Codable

        enum CodingKeys: CodingKey {
            case fileLocation
            case size
        }

        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let fileLocation = try values.decode(FileLocation.self, forKey: .fileLocation)

            guard FileManager.default.fileExists(atPath: fileLocation.url.path) else {
                throw CustomStringError(description: "Could not find file at path:  \(fileLocation.url.path).")
            }

            if values.contains(.size) {
                let size = try values.decode(Schema.Size.self, forKey: .size)
                self = .gif(fileLocation, size)
            } else {
                self = .file(fileLocation)
            }
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            switch self {
            case let .file(fileLocation):
                try container.encode(fileLocation, forKey: .fileLocation)
            case let .gif(fileLocation, size):
                try container.encode(fileLocation, forKey: .fileLocation)
                try container.encode(size, forKey: .size)
            }
        }
    }

    public enum QuotedMessage: Hashable, Codable {
        case sameThread(Schema.MessageID)
        case differentThread(Schema.ThreadID, Schema.MessageID)

        var messageID: Schema.MessageID {
            switch self {
            case .sameThread(let id):
                return id
            case .differentThread(_, let id):
                return id
            }
        }

        var threadID: Schema.ThreadID? {
            switch self {
            case .sameThread:
                return nil
            case .differentThread(let id, _):
                return id
            }
        }
    }

    public var attachments: [LocalAttachment] = []
    public var messageText = ""
    public var quotedMessage: QuotedMessage?

    public var mentionedUsers: [Schema.UserID] {
        return []
    }

    public var isEmpty: Bool {
        attachments.isEmpty && messageText.isEmpty
    }

    public init() {}
}

extension MessageDraft.LocalAttachment {
    var messageContent: Schema.MessageContent {
        .init(
            text: nil,
            filePath: nil,
            fileBuffer: fileData,
            fileName: fileName,
            mimeType: utType.preferredMIMEType,
            isGif: isGif,
            size: size,
            videoURL: nil
        )
    }
}
