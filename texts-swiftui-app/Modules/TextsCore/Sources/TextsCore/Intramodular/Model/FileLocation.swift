//
// Copyright (c) Texts HQ
//

import Foundation

// Because app's directory is not guaranteed to be stable
// and can change between app launches,
// we should avoid storing absolute URLs.
// Instead, we're storing a path relative to the base URL
// which is defined by the enum, and dynamically resolved to the appropriate URL.
public struct FileLocation: Hashable, Codable {
    public let base: BaseLocation
    public let relativePath: String

    public init(base: BaseLocation, relativePath: String) {
        self.base = base
        self.relativePath = relativePath
    }

    public var url: URL {
        if let url = URL(string: relativePath, relativeTo: base.url) {
            return url
        } else {
            return URL(fileURLWithPath: relativePath, relativeTo: base.url)
        }
    }
}

public enum BaseLocation: String, Codable {
    case temporaryDirectory = "tmp"
    case absolute

    var url: URL? {
        switch self {
        case .temporaryDirectory:
            return FileManager.default.temporaryDirectory
        case .absolute:
            return nil
        }
    }
}
