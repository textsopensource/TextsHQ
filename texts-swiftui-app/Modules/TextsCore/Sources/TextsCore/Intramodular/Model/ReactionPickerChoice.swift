//
// Copyright (c) Texts HQ
//

public enum ReactionPickerChoice: Hashable {
    case emoji(String)
    case supportedReaction(String)

    public var isEmoji: Bool {
        switch self {
        case .emoji:
            return true
        case .supportedReaction:
            return false
        }
    }

    public var key: String {
        switch self {
        case let .emoji(emoji):
            return emoji
        case let .supportedReaction(key):
            return key
        }
    }
}
