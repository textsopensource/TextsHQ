//
// Copyright (c) Texts HQ
//

import Foundation
import IdentifiedCollections

public enum MessageListItem: Identifiable, Hashable {
    public struct MessagePayload: Identifiable, Hashable {
        // this id can be *different* from message.id to maintain
        // stability when a pending message turns into a delivered
        // message (see ThreadStore.pendingToRealMap)
        public let id: Schema.MessageID
        public var message: Schema.Message
        public var participants: IdentifiedArrayOf<Schema.User>
        public var userID: Schema.UserID
        public var previousMessage: Schema.Message?
        public var nextMessage: Schema.Message?

        public init(
            id: Schema.MessageID,
            message: Schema.Message,
            participants: IdentifiedArrayOf<Schema.User>,
            userID: Schema.UserID,
            previousMessage: Schema.Message?,
            nextMessage: Schema.Message?
        ) {
            self.id = id
            self.message = message
            self.participants = participants
            self.userID = userID
            self.previousMessage = previousMessage
            self.nextMessage = nextMessage
        }

        public var avatarURL: String? {
            message.isSender == true ? nil : message.senderID.flatMap { participants[id: $0] }?.imgURL
        }

        public var showUserAvatar: Bool {
            let hasMultipleParticipants = participants.count > 2
            let canShowSender = hasMultipleParticipants && message.isSender != true && message.isAction != true

            guard canShowSender else {
                return false
            }

            let isNextMessageFromTheSameSender = nextMessage?.senderID == message.senderID
            let messagesSeparatedByLongTime = abs(nextMessage?.timestamp.timeIntervalSince(message.timestamp) ?? 0.0) > LayoutConstants.GAPPED_2_DURATION

            return !isNextMessageFromTheSameSender || messagesSeparatedByLongTime
        }
    }

    case activityIndicator
    case message(MessagePayload)

    public var id: AnyHashable {
        switch self {
        case .activityIndicator:
            return self
        case .message(let payload):
            return payload.id
        }
    }
}
