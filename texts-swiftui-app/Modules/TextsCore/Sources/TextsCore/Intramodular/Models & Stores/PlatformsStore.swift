//
// Copyright (c) Texts HQ
//

import Combine
import Foundation
import IdentifiedCollections

public final class PlatformsStore: ObservableObject {
    @Published public private(set) var platforms: IdentifiedArrayOf<Schema.PlatformInfo> = []
    @Published public private(set) var hasLoadedPlatforms = false

    public func initialize(with platforms: [Schema.PlatformInfo]) {
        self.platforms = .init(uniqueElements: platforms)
        self.hasLoadedPlatforms = true
    }

    public subscript(platformName: String) -> Schema.PlatformInfo? {
        platforms[id: platformName]
    }
}
