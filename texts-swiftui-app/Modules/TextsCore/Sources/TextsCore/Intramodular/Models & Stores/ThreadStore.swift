//
// Copyright (c) Texts HQ
//

import Diagnostics
import Foundation
import IdentifiedCollections
import Merge
import Swallow

public struct UserActivity: Hashable {
    public let id = UUID()
    public let type: Schema.ActivityType
    public let participantID: Schema.UserID
    public let customLabel: String?
}

extension Schema.Paginated where Element == Schema.Message {
    mutating func sortMessages() {
        items.sort { $0.timestamp < $1.timestamp }
    }
}

// basically a hydrated Thread
public class ThreadStore: Identifiable, ObservableObject {
    let logger = os.Logger(subsystem: Bundle.main.bundleIdentifier!, category: "ThreadStore")

    let accountID: Schema.AccountID

    public let id: Schema.ThreadID
    public var inboxName: Schema.InboxName {
        .init(rawValue: folderName ?? "normal") ?? Schema.InboxName.normal
    }

    @Published public internal(set) var type: Schema.ThreadType
    @Published public internal(set) var createdAt: Date?
    @Published public internal(set) var timestamp: Date?
    @Published public internal(set) var imgURL: String?
    @Published public internal(set) var threadDescription: String?
    @Published public internal(set) var title: String?
    @Published public internal(set) var isUnread: Bool
    @Published public internal(set) var isReadOnly: Bool
    @Published public internal(set) var isArchived: Bool
    @Published public internal(set) var isPinned: Bool
    @Published public internal(set) var mutedUntil: String?
    @Published public internal(set) var folderName: String?

    @Published public internal(set) var lastMessageSnippet: String?
    @Published public internal(set) var messages: Schema.Paginated<Schema.Message>
    @Published public internal(set) var pendingMessages: IdentifiedArrayOf<Schema.Message> = []
    @Published public internal(set) var linkedMessages: [Schema.MessageID: Schema.Message] = [:]

    @Published public internal(set) var participants: Schema.Paginated<Schema.User>
    @Published public internal(set) var activities: [Schema.UserID: UserActivity] = [:]

    @Published public var props: Schema.ThreadProps

    // when a pending message turns into a delivered message, the "real" (delivered)
    // message might have a different id than the one we gave the pending one. This
    // field maps the original id to the new one, so that we can continue telling
    // SwiftUI that the message has the old ID, to maintain view stability. This
    // should only be used for UI stuff; the platform will always want the real ID.
    public internal(set) var pendingToRealMap: [Schema.MessageID: Schema.MessageID] = [:]

    public var inbox: InboxStore? {
        account?.inboxes[inboxName]
    }

    public var allMessages: AnyPublisher<AnyCollection<Schema.Message>, Never> {
        $messages
            .map(\.items)
            .combineLatest($pendingMessages)
            .map { AnyCollection([$0, $1].joined()) }
            .eraseToAnyPublisher()
    }

    public var account: AccountStore? {
        AppState.shared.rootStore.accountsStore[accountID]?.currentAccountStore
    }

    public var isSending = false

    public lazy private(set) var uniqueID = "\(accountID):\(id)"

    public var currentUser: Schema.User? {
        account?.user
    }

    private var cancellables = Cancellables()

    public var stopTypingTask: DispatchWorkItem?

    private func updateTimestamp(withMessages messages: Schema.Paginated<Schema.Message>) {
        if let lastTimestamp = messages.items.last(where: { $0.isHidden != true })?.timestamp,
           lastTimestamp > (timestamp ?? .distantPast) {
            self.timestamp = lastTimestamp
        }
    }

    public init(base: Schema.Thread, accountID: Schema.AccountID) {
        self.accountID = accountID
        self.id = base.id
        self.type = base.type
        self.timestamp = base.timestamp
        self.createdAt = base.createdAt
        self.imgURL = base.imgURL
        self.threadDescription = base.description
        self.title = base.title
        self.isUnread = base.isUnread
        self.isReadOnly = base.isReadOnly == true
        self.isArchived = base.isArchived == true
        self.isPinned = base.isPinned == true
        self.mutedUntil = base.mutedUntil
        self.folderName = base.folderName
        self.lastMessageSnippet = base.lastMessageSnippet
        self.participants = base.participants

        do {
            self.props = try AppDatabase.getThreadProps(id, accountID: accountID) ?? Schema.ThreadProps()
        } catch {
            AppState.shared.handleError(error)
            self.props = Schema.ThreadProps()
        }

        var messages = base.messages
        messages.sortMessages()
        self.messages = messages

        updateTimestamp(withMessages: messages)

        $props.sink(in: cancellables) { [weak self] newValue in
            guard let self = self else { return }
            AppDatabase.setThreadProps(newValue, threadID: self.id, accountID: self.accountID)
        }

        $messages.sink(in: cancellables) { [weak self] newValue in
            guard let self = self else { return }
            self.updateTimestamp(withMessages: newValue)
        }
    }

    // MARK: - Conveniences

    public lazy var isTwitterNotifications: Bool = {
        id == "notifications" && (try? account?.accountInstance().platform.id) == "twitter"
    }()

    @MainActor
    public lazy var isFromPlatformWithOtherAccounts: Bool = {
        AppState.shared.rootStore
            .accountsStore.accounts
            .filter { $0.platform.id == account?.instance?.platform.id }
            .count > 1
    }()
}

extension ThreadStore {
    func apply(partial: Schema.Thread.Partial) {
        func coalesce<T>(_ dest: ReferenceWritableKeyPath<ThreadStore, T>, _ src: KeyPath<Schema.Thread.Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        func coalesce<T>(_ dest: ReferenceWritableKeyPath<ThreadStore, T?>, _ src: KeyPath<Schema.Thread.Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        coalesce(\.type, \.type)
        coalesce(\.timestamp, \.timestamp)
        coalesce(\.createdAt, \.createdAt)
        coalesce(\.imgURL, \.imgURL)
        coalesce(\.threadDescription, \.description)
        coalesce(\.title, \.title)
        coalesce(\.isUnread, \.isUnread)
        coalesce(\.isReadOnly, \.isReadOnly)
        coalesce(\.isArchived, \.isArchived)
        coalesce(\.isPinned, \.isPinned)
        coalesce(\.mutedUntil, \.mutedUntil)
        coalesce(\.lastMessageSnippet, \.lastMessageSnippet)
        // partial threads shouldn't overwrite our messages and participants
        // coalesce(\.messages, \.messages)
        // coalesce(\.participants, \.participants)
    }
}

extension ThreadStore {
    public var name: String {
        displayName() ?? id.rawValue
    }

    public var displayIsUnread: Bool {
        isUnread || props.isUnread ?? false
    }

    public var displayIsPinned: Bool {
        isPinned || props.isPinned ?? false
    }

    public var displayIsMuted: Bool {
        mutedUntil == "forever" || props.isMuted ?? false
    }

    public var displayIsArchived: Bool {
        if Preferences.shared.nativeArchive,
           let supportsArchive = account?.platform?.attributes.contains(.supportsArchive),
           supportsArchive {
            return isArchived
        }

        guard let isArchivedUpto = props.isArchivedUpto else {
            return false
        }

        if isArchivedUpto.hasPrefix("ts"),
           let archivedTimestampMS = Int(isArchivedUpto.dropFirst(2)) {
            let archivedTimestamp = Date(timeIntervalSince1970: TimeInterval(archivedTimestampMS) / 1000)

            return archivedTimestamp >= (timestamp ?? .distantPast)
        }

        return isArchivedUpto == messages.last?.id.rawValue
    }

    public var oldestMessage: Schema.Message? {
        messages.first
    }

    public var displayParticipant: Schema.User? {
        participants.first { !($0.isSelf ?? false) } ?? participants.first
    }

    public func getParticipant(id userId: Schema.User.ID) -> Schema.User? {
        if let participant = participants.items[id: userId] {
            return participant
        } else if let current = currentUser, current.id == userId {
            return current
        } else if userId == "$thread" {
            return participants.items[id: Schema.UserID(rawValue: id.rawValue)]
        } else if userId.rawValue.starts(with: "$thread_"), let thread = account?.getThreadFromAllInboxes(Schema.ThreadID(rawValue: String(userId.rawValue.dropFirst(8)))) {
            return Schema.User(
                id: Schema.UserID(rawValue: thread.id.rawValue),
                fullName: thread.title,
                imgURL: thread.imgURL
            )
        } else {
            return nil
        }
    }

    public func displayName() -> String? {
        if let title = self.props.customTitle ?? self.title, !title.isEmpty {
            return title
        } else {
            if self.type == .group {
                let displayNames = participants
                    .filter { $0.id != currentUser?.id }
                    .compactMap { $0.displayName }
                if displayNames.count > 1 {
                    return displayNames.joined(separator: ", ")
                }
            } else {
                return displayParticipant?.displayName
            }
        }

        return nil
    }

    public func getSubtitle() -> String? {
        var subtitle: String?
        if self.type != .group {
            if let parId = participants.first?.id, let presence = account?.presenceMap[parId] {
                if presence.status == "online" {
                    subtitle = "active now"
                } else if let lastActiveAt = presence.lastActive {
                    let formatter = RelativeDateTimeFormatter.presenceFormatter
                    let timeAgoStr = formatter.localizedString(for: lastActiveAt, relativeTo: Date())
                    subtitle = "active \(timeAgoStr)"
                }
            }
        }
        return subtitle
    }

    public func getAvatarURL() -> String? {
        if let imgURL: String = self.imgURL {
            return imgURL
        }

        if self.type != .group {
            return self.participants.first { !($0.isSelf ?? false) }?.imgURL
        }

        return nil
    }
}

extension ThreadStore {
    public func seenByOtherUsers(for message: Schema.Message?) -> [Schema.UserID] {
        allSeenBy(for: message)
            .map { id, _ in id }
            .filter {
                $0 != message?.senderID &&
                    $0 != currentUser?.id
            }
    }

    public func allSeenBy(for message: Schema.Message?) -> [Schema.UserID: Date] {
        switch message?.seen {
        case nil, .single(.notSeen):
            return [:]
        case .single(.seen(let date)):
            return .init(uniqueKeysWithValues: participants.map { ($0.id, date ?? Date()) })
        case .group(let people):
            return people.compactMapValues {
                switch $0 {
                case .seen(let date):
                    return date ?? Date()
                default:
                    return nil
                }
            }
        }
    }
}

// MARK: - Typing activity

extension ThreadStore {
    public var typingActivities: [UserActivity] {
        activities.values.filter { $0.type == .typing }
    }

    public var videoRecordingActivities: [UserActivity] {
        activities.values.filter { $0.type == .recordingVideo }
    }

    public var audioRecordingActivities: [UserActivity] {
        activities.values.filter { $0.type == .recordingVoice }
    }

    public var customActivities: [UserActivity] {
        activities.values.filter { $0.type == .custom }
    }

    public var shouldShowActivityIndicator: Bool {
        activities.values.contains { $0.type != .none }
    }

    public var activityIndicatorMessages: [String] {
        [
            typingActivities,
            videoRecordingActivities,
            audioRecordingActivities,
            customActivities,
        ]
        .compactMap { activities -> String? in
            let activities = activities.filter { $0.participantID != self.currentUser?.id }

            guard !activities.isEmpty else {
                return nil
            }

            let activityName = activities[0].customLabel ?? activities[0].type.localizedName

            if self.type != .group {
                return "\(activityName)..."
            }

            var label: String?

            if let participant = self.participants.items[id: activities[0].participantID] {

                label = participant.displayName

                if activities.count > 1 {
                    label?.append(" and \(activities.count - 1) others")
                }

                let isOrAre = activities.count > 1 ? "are" : "is"
                label?.append(" \(isOrAre) \(activityName)...")
            }

            return label
        }
    }
}

extension ThreadStore: Equatable, Hashable {
    public static func == (lhs: ThreadStore, rhs: ThreadStore) -> Bool {
        lhs.uniqueID == rhs.uniqueID
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(uniqueID)
    }
}

import UIKit

extension ThreadStore {

    public var lastMessageDescription: String? {
        guard let lastMessage = messages.last else {
            return lastMessageSnippet
        }

        if lastMessage.isDeleted == true {
            return "\(Globals.Strings.deletedMessagePrefix) [Deleted Message]"
        }

        let text: String
        let attachments = lastMessage.attachments ?? []
        let tweets = lastMessage.tweets ?? []
        let links = lastMessage.links ?? []

        if let lastMessageContent = lastMessage.displayString(in: self), !lastMessageContent.isEmpty {
            text = lastMessageContent
        } else {
            text = ""
        }

        if attachments.count == 1, let attachment = attachments.first {
            if attachment.isSticker == true {
                return "\(Globals.Strings.imageAttachmentPrefix) [Sticker] \(text)"
            }
            if attachment.isGif == true {
                return "\(Globals.Strings.gifAttachmentPrefix) [GIF] \(text)"
            }

            switch attachment.type {
            case .img:
                return "\(Globals.Strings.imageAttachmentPrefix) [Picture] \(text)"
            case .video:
                return "\(Globals.Strings.videoAttachmentPrefix) [Video] \(text)"
            case .audio:
                return "\(Globals.Strings.audioAttachmentPrefix) [Audio] \(text)"
            case .unknown:
                if attachment.fileName?.hasSuffix(".vcf") == true || attachment.mimeType == "text/x-vcard" {
                    if let contactName = attachment
                        .fileName?
                        .replacingOccurrences(of: "^\\d\\.\\w+\\.", with: "", options: [.regularExpression])
                        .replacingOccurrences(of: ".vcf", with: "") {
                        return "\(Globals.Strings.contactAttachmentPrefix) [Contact: \(contactName)] \(text)"
                    } else {
                        return "\(Globals.Strings.contactAttachmentPrefix) [Contact] \(text)"
                    }
                }

                return "\(Globals.Strings.fileAttachmentPrefix) [\(attachment.fileName ?? "Attachment")] \(text)"
            }
        } else if attachments.count > 1 {
            return "\(Globals.Strings.fileAttachmentPrefix) [\(attachments.count) attachments] \(text)"
        }

        let sender: String
        if lastMessage.isSender == true {
            sender = "You sent"
        } else if participants.count > 1, let id = lastMessage.senderID, let user = participants.items[id: id] {
            sender = "\(user.displayName) sent"
        } else {
            sender = "Sent"
        }

        if links.count == 1 {
            return "\(sender) a link"
        } else if links.count > 1 {
            return "\(sender) \(links.count) links"
        }

        if tweets.count == 1, let tweet = tweets.first {
            return "\(Globals.Strings.tweetMessagePrefix) [Tweet by @\(tweet.user.username)]"
        } else if tweets.count > 1 {
            return "\(Globals.Strings.tweetMessagePrefix) [\(links.count) tweets]"
        }

        return text
    }

    public var displayTicks: NSAttributedString {
        guard let lastMessage = messages.last, lastMessage.isSender == true else {
            return NSAttributedString(string: "")
        }

        if !seenByOtherUsers(for: lastMessage).isEmpty {
            let string = NSMutableAttributedString(string: "✓✓")
            string.addAttributes(
                [.kern: -11, .font: UIFont.preferredFont(forTextStyle: .headline)],
                range: NSRange(location: 0, length: 2)
            )
            string.addAttributes([.foregroundColor: UIColor(named: "AccentColor")!.withAlphaComponent(0.8)], range: NSRange(location: 0, length: 1))
            string.addAttributes([.foregroundColor: UIColor(named: "AccentColor")!], range: NSRange(location: 1, length: 1))
            return string
        } else if lastMessage.isDelivered == true || lastMessage.isErrored != true {
            return NSAttributedString(
                string: "✓",
                attributes: [
                    .foregroundColor: UIColor(named: "AccentColor")!
                ]
            )
        }

        return NSAttributedString(string: "")
    }

    public var isVerified: Bool {
        type == .single && displayParticipant?.isVerified ?? false
    }

    public var displayTimestamp: Date {
        messages.last?.timestamp ?? timestamp ?? Date()
    }

    public var relativeDisplayTimestamp: String? {
        Self.relativeFormatter.string(from: -displayTimestamp.timeIntervalSinceNow)
    }

    public var activityOrDraftOrLastMessage: NSAttributedString {
        if let activity = activityIndicatorMessages.first {
            return NSAttributedString(string: activity)
        } else if let draft = AppCache.retrieve(MessageDraft.self, key: "\(uniqueID)-message-draft"), !draft.isEmpty {
            let font = UIFont(descriptor: UIFont.preferredFont(forTextStyle: .subheadline).fontDescriptor.withSymbolicTraits(.traitItalic)!, size: 0)
            return NSAttributedString(string: draft.messageText, attributes: [.font: font, .foregroundColor: UIColor.secondaryLabel])
        } else {
            return NSAttributedString(string: lastMessageDescription?.trimmingWhitespace() ?? "")
        }
    }

    private static let relativeFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .abbreviated
        formatter.formattingContext = .listItem
        formatter.allowsFractionalUnits = false
        formatter.maximumUnitCount = 1
        return formatter
    }()
}
