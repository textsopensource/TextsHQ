//
// Copyright (c) Texts HQ
//

import Foundation
import Merge
import os
import GRDB
import TextsBase

public final class InboxStore: ObservableObject {
    private static let logger = Logger(category: "InboxStore")

    public let name: Schema.InboxName

    private var account: AccountStore

    @Published public var threads = Schema.Paginated<ThreadStore>()

    public var initialized = false

    init(name: Schema.InboxName, accountStore: AccountStore) {
        self.name = name
        account = accountStore
    }
}

extension InboxStore {
    public enum GetThreadsMode {
        case all // should only be used on activation
        case older
        case newer
    }

    private func cascadeLoad(_ iteration: Int = 0) async throws {
        try await getThreads(.older)

        guard threads.hasMore, iteration < 5, threads.items.count < 100 else {
            return
        }

        return try await cascadeLoad(iteration + 1)
    }

    public func getThreads(_ mode: GetThreadsMode) async throws {
        let instance = try account.accountInstance()

        #if INDEXDB_ENABLED
        try await getThreadsFromDatabase(mode)

        guard instance.state.type == .loaded,
              NetworkMonitor.shared.connected else {
            return
        }
        #endif

        let result = try await instance.api.getThreads((
            inboxName: name,
            args: mode == .older ? .init(.before, from: threads) : nil
        ))

        guard let result = result else {
            return
        }

        let stores = result.uniqueMapElements { ThreadStore(base: $0, accountID: account.id) }

        Task(priority: .utility) {
            AppDatabase.cacheThreads(threads: result.items.elements, account: account)
        }

        switch mode {
        case .all:
            pushThreads(stores)
            initialized = true
            try await cascadeLoad()
            await instance.setHasLoadedThreads()
        case .older, .newer:
            // work on a copy to avoid mutating `threads` N times since
            // it triggers the @Published each time
            var copy = threads
            if mode == .older {
                copy.newestCursor = stores.newestCursor
                copy.oldestCursor = stores.oldestCursor
                copy.hasMore = stores.hasMore
            }
            stores.forEach { pushThread($0, threads: &copy) }
            threads = copy
        }
    }

    public func getThreadsFromDatabase(_ mode: GetThreadsMode) async throws {
        let stores = try AppDatabase
            .getThreads(accountID: self.account.id, before: mode == .older ? self.threads.last?.timestamp : nil)
            .compactMap { ($0.folderName ?? "normal") == name.rawValue ? ThreadStore(base: $0, accountID: self.account.id) : nil }

        var copy = self.threads
        stores.forEach { pushThread($0, threads: &copy) }
        copy.hasMore = mode == .all || copy.count > self.threads.count

        self.threads = copy
    }

    private func pushThread(_ thread: ThreadStore, threads: inout Schema.Paginated<ThreadStore>) {
        if let existing = threads.items[id: thread.id] {
            let minTimestamp = existing.messages.last?.timestamp ?? .distantPast
            let messages: Array = thread.messages.items.filter { $0.timestamp > minTimestamp }
            messages.forEach { existing.messages.items.updateOrAppend($0) }
        } else {
            threads.items.append(thread)
        }
    }

    private func pushThread(_ thread: ThreadStore) {
        self.pushThread(thread, threads: &self.threads)
    }

    private func pushThreads(_ threadStores: Schema.Paginated<ThreadStore>, threads: inout Schema.Paginated<ThreadStore>) {
        var copy = threads
        threadStores.forEach { pushThread($0, threads: &copy) }
        threads = copy
    }

    private func pushThreads(_ threadStores: Schema.Paginated<ThreadStore>) {
        pushThreads(threadStores, threads: &self.threads)
    }
}
