//
// Copyright (c) Texts HQ
//

import Diagnostics
import Foundation
import Merge
import Swallow
import TextsNotifications
import TextsBase

public final class AccountStore: ObservableObject, CancellablesHolder, Identifiable {
    private let logger = Logger(category: "AccountStore")
    private let server = PlatformServer.shared

    public let id: Schema.AccountID

    private weak var _accountInstance: AccountInstance?

    private var throttledTypingIndicatorLastRun: TimeInterval = 0.0

    @Published public private(set) var inboxes: [Schema.InboxName: InboxStore] = [:]
    @Published public private(set) var presenceMap: Schema.PresenceMap = [:]
    public private(set) var customEmojis: Schema.CustomEmojiMap = [:]

    @Published public var selectedInbox: Schema.InboxName = .normal

    public var currentInbox: InboxStore? {
        inboxes[selectedInbox]
    }

    public var platformDisplayName: String? {
        _accountInstance?.platform.displayName
    }

    public func accountInstance() throws -> AccountInstance {
        guard let accountInstance = _accountInstance else {
            throw AccountError.invalidAccount
        }

        switch accountInstance.state {
        case .offline, .loaded:
            break
        default:
            throw AccountError.uninitialized
        }

        return accountInstance
    }

    // Workaround for platform info not accessible when AccountInstance is in loading state
    public var platform: Schema.PlatformInfo? {
        _accountInstance?.platform
    }

    /// `try? accountInstance` with Optional instead of throwing
    public var instance: AccountInstance? {
        try? accountInstance()
    }

    public var user: Schema.User? {
        _accountInstance?.user
    }

    init(accountInstance: AccountInstance) {
        _accountInstance = accountInstance

        self.id = accountInstance.id

        // Initialize inboxes
        inboxes[.normal] = InboxStore(name: Schema.InboxName.normal, accountStore: self)

        if accountInstance.platform.attributes.contains(.supportsRequestsInbox) {
            inboxes[.requests] = InboxStore(name: Schema.InboxName.requests, accountStore: self)
        }

        #if INDEXDB_ENABLED
        if PlatformServer.shared.state.status != .launched {
            self.getThreadsFromDatabase()
        } else {
            Task { @MainActor in
                self.getThreadsFromDatabase()
            }
        }
        #endif

        // connect each thread's objectWillChange to our own
        inboxes.forEach { _, inbox in
            inbox
                .$threads
                .flatMap {
                    $0.items
                        .map(\.objectWillChange)
                        .mergeManyPublisher
                }
                .sink(in: cancellables) { [weak self] in
                    self?.objectWillChange.send()
                }
        }

        $selectedInbox.sink(in: cancellables) { name in
            guard let inbox = self.inboxes[name] else {
                return
            }

            if inbox.initialized || !inbox.threads.items.isEmpty || accountInstance.state.isLoadingThreads {
                return
            }

            Task.detached(priority: .high) { @MainActor in
                accountInstance.setLoadingThreads()

                do {
                    try await inbox.getThreads(.all)

                    inbox.initialized = true
                } catch {
                    self.logger.error(error)

                    inbox.initialized = false
                }
            }
        }
    }

    @MainActor
    public func setUpEventAndConnectionStateObservers() throws {
        let accountInstance = try _accountInstance.unwrap()

        server
            .publisher(for: .subscribeToEvents, on: accountInstance)
            .receiveOnMainQueue()
            .discardError()
            .sink { [weak self] (events: [Schema.ServerEvent]) in
                guard let self = self else {
                    return
                }

                Task { @MainActor in
                    for event in events {
                        self.handleServerEvent(event)
                    }
                }
            }
            .store(in: cancellables)

        if accountInstance.platform.attributes.contains(.subscribeToConnStateChange) {
            server
                .publisher(for: .onConnectionStateChange, on: accountInstance)
                .receiveOnMainQueue()
                .discardError()
                .sink { [weak self] (state: Schema.ConnectionState) in
                    guard let self = self, let ai = try? self.accountInstance() else { return }
                    switch state.status {
                    case .unauthorized:
                        Task {
                            await ai.dispose(requireReauth: true)
                        }
                    case .conflict:
                        Task {
                            try await ai.api.takeoverConflict()
                        }
                    default:
                        break
                    }
                }
                .store(in: cancellables)
        }
    }
}

extension AccountStore {
    @MainActor
    public func login(
        with creds: Schema.LoginCreds?
    ) async throws -> Schema.LoginResult {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        return try await accountInstance.api.login(creds)
    }

    @MainActor
    func logout() async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        try await accountInstance.api.logout()
    }

    @MainActor
    public func searchUsers(query: String) async throws -> [Schema.User] {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        return try await accountInstance.api.searchUsers(query)
    }

    @MainActor
    public func loadPresence() async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.platform.attributes.contains(.supportsPresence) else {
            return
        }

        do {
            let presences = try await accountInstance.api.getPresence()

            for (key, value) in presences {
                presenceMap[.init(rawValue: key)] = value
            }
        } catch {
            self.logger.error("getPresence error: \("\(error)")")
        }
    }

    public func loadCustomEmojis() async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.platform.attributes.contains(.supportsCustomEmojis) else {
            return
        }

        do {
            let emojis = try await accountInstance.api.getCustomEmojis()

            for (key, value) in emojis {
                customEmojis[.init(rawValue: key)] = value
            }
        } catch {
            self.logger.error("getCustomEmojis error: \("\(error)")")
        }
    }

    @MainActor
    public func onActive() async throws {
        try await getThreads(.all)
        try await loadPresence()
        try await loadCustomEmojis()
    }

    public func throttledSendActivityIndiactor(_ activity: Schema.ActivityType, threadID: Schema.ThreadID) async throws {
        let accountInstance = try accountInstance()

        guard let typingDurationMs = accountInstance.platform.typingDurationMs else {
            return
        }

        let typingDuration: TimeInterval = typingDurationMs / 1000
        let currentInterval = Date().timeIntervalSinceReferenceDate

        guard (currentInterval - typingDuration) > throttledTypingIndicatorLastRun else {
            return
        }

        try await accountInstance.api.sendActivityIndicator((type: activity, threadID: threadID))
        throttledTypingIndicatorLastRun = Date().timeIntervalSinceReferenceDate
    }
}

extension AccountStore {
    public func registerForPushNotifications(type: Schema.NotificationKind, token: String) async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        try await accountInstance.api.registerForPushNotifications((type: type, token: token))
    }

    public func unregisterForPushNotifications(type: Schema.NotificationKind, token: String) async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        try await accountInstance.api.unregisterForPushNotifications((type, token))
    }
}

extension AccountStore {
    private func getThreadsFromDatabase() {
        do {
            try AppDatabase.getThreads(accountID: id).forEach { cachedThread in
                let thread = ThreadStore(base: cachedThread, accountID: id)

                inboxes[thread.inboxName]?.threads.items.updateOrAppend(thread)
            }
        } catch {
            self.logger.debug("Failed to get threads from database: \(String(describing: error))")
        }
    }

    /// Fetches more threads
    @MainActor
    public func getThreads(
        _ mode: InboxStore.GetThreadsMode
    ) async throws {
        try await currentInbox?.getThreads(mode)
    }

    @MainActor
    public func getThread(_ threadID: Schema.ThreadID) async throws {
        let thread = try await accountInstance().api.getThread(threadID)

        let threadStore = ThreadStore(base: thread, accountID: id)

        threadStore.inbox?.threads.items.updateOrAppend(threadStore)
    }

    public func getThreadFromAllInboxes(_ threadID: Schema.ThreadID) -> ThreadStore? {
        let inbox = inboxes.first { _, inbox in
            inbox.threads.items[id: threadID] != nil
        }

        return inbox?.value.threads.items[id: threadID]
    }

    private func loadMissingThread(_ id: Schema.ThreadID) async throws {
        guard let inbox = inboxes[.normal] else {
            return
        }

        do {
            try await getThread(id)
        } catch {
            try await inbox.getThreads(.newer)
        }
    }
}

extension AccountStore {
    /// Sends the thread selected event to the platform
    @MainActor
    public func onThreadSelected(threadID: Schema.ThreadID?) async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        if accountInstance.platform.attributes.contains(.subscribeToThreadSelection) {
            _ = try await accountInstance.api.onThreadSelected(threadID)
        }
    }

    /// Loads dynamic message
    /// - Parameter message: Message to be loaded
    @MainActor
    public func loadDynamicMessage(message: Schema.Message) async throws -> Schema.Message.Partial {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        return try await accountInstance.api.loadDynamicMessage(message)
    }

    public func loadMessage(
        threadID: Schema.ThreadID,
        messageID: Schema.MessageID
    ) async throws -> Schema.Message? {
        guard let thread = getThreadFromAllInboxes(threadID),
              thread.messages.items[id: messageID] != nil else {
            return nil
        }

        var message = try await AppDatabase.getMessage(
            accountID: id,
            threadID: threadID,
            messageID: messageID
        )

        if message == nil {
            message = try await thread.getMessage(messageID)
        } else {
            Task {
                _ = try await thread.getMessage(messageID)
            }
        }

       return message
    }

    public func loadLinkedMessage(
        threadID: Schema.ThreadID,
        messageID: Schema.MessageID
    ) async throws {
        guard let thread = getThreadFromAllInboxes(threadID),
              let message = try await loadMessage(threadID: threadID, messageID: messageID) else {
            return
        }

        thread.linkedMessages[messageID] = message
    }

    @MainActor
    public func existingThreadWith(users: [Schema.UserID]) -> ThreadStore? {
        guard let me = try? accountInstance().user, let inbox = currentInbox else {
            return nil
        }

        return inbox.threads
            .map({ Set($0.participants.map(\.id)) })
            .firstIndex(of: Set(users + [me.id]))
            .map({ inbox.threads[$0] })
    }

    @MainActor
    public func newOrExistingThreadWith(
        users: [Schema.UserID],
        title: String? = nil
    ) async throws -> ThreadStore {
        if let existingThread = existingThreadWith(users: users) {
            return existingThread
        } else {
            return try await createThread(users: users, title: title)
        }
    }

    @MainActor
    public func createThread(
        users: [Schema.UserID],
        title: String?
    ) async throws -> ThreadStore {
        let accountInstance = try accountInstance()

        guard accountInstance.state == .loaded else {
            throw AccountError.uninitialized
        }

        guard let user = accountInstance.user else {
            throw CustomStringError(description: "Please sign in first.")
        }

        guard ((users.count == 1 || users.count == 2) && !accountInstance.platform.attributes.contains(.noSupportSingleThreadCreation)) ||
                (users.count >= 3 && !accountInstance.platform.attributes.contains(.noSupportGroupThreadCreation)) else {
            throw CustomStringError(description: "You cannot create a new thread with this account.")
        }

        let newThreadAllParticipantIds = Set([user.id] + users)

        logger.info("Creating a thread with \(newThreadAllParticipantIds.count) participants.")

        let result: ThreadStore

        do {
            let thread = try await accountInstance.api.createThread((users: users, title: title))

            Task(priority: .utility) {
                do {
                    try await AppDatabase.shared.writer.write { db in
                        try AppDatabase.cacheThread(db, thread: thread, account: self)
                    }
                } catch {
                    logger.error(error)
                }
            }

            result = ThreadStore(base: thread, accountID: id)
        } catch {
            logger.info("Getting a list of all threads to find a new one.")

            try await getThreads(.older)

            let thread = inboxes[.normal]!.threads.first { thread in
                let participantIds = thread.participants.items.map(\.id)

                return Set(participantIds) == newThreadAllParticipantIds
            }

            guard let thread = thread else {
                throw CustomStringError(description: "Texts could not create a thread with these participants.")
            }

            result = thread
        }

        inboxes[.normal]!.threads.items.append(result)

        return result
    }

    @MainActor
    public func deleteThread(threadID: Schema.ThreadID) async throws {
        guard let supportsDeleteThread = platform?.attributes.contains(.supportsDeleteThread),
              supportsDeleteThread,
              let thread = getThreadFromAllInboxes(threadID) else {
            return
        }

        try await accountInstance().api.deleteThread(threadID)

        inboxes[thread.inboxName]?.threads.items.remove(thread)

        try await AppDatabase.shared.writer.write { db in
            try AppDatabase.deleteCachedThread(db, threadID: threadID, account: self)
        }
    }

    @MainActor
    public func reportThread(threadID: Schema.ThreadID) async throws {
        let accountInstance = try accountInstance()
        guard accountInstance.platform.attributes.contains(.supportsReportThread),
              let thread = getThreadFromAllInboxes(threadID) else {
            return
        }

        _ = try await accountInstance.api.reportThread((
            type: .spam,
            threadID: threadID,
            firstMessageID: thread.messages.first?.id
        ))
    }
}

extension AccountStore {
    @MainActor fileprivate func handleServerEvent(_ event: Schema.ServerEvent) {
        if let threadID = event.threadID, getThreadFromAllInboxes(threadID) == nil {
            logger.warning("warning: thread not found: \(threadID)")
            Task { @MainActor in
                try await self.loadMissingThread(threadID)
            }
            return
        }

        switch event {
        case .userActivity(let data):
            onUserActivity(data)
        case .threadMessagesRefresh(let data):
            onThreadMessagesRefresh(data)
        case .threadTrusted(let data):
            onThreadTrusted(data)
        case .stateSync(let data):
            onStateSync(data)
        case .toast(let data):
            onToast(data)
        case .userPresenceUpdated(let data):
            onUserPresenceUpdated(data)
        case .sessionUpdated:
            onSessionUpdated()
        case .openWindow:
            break
        }

        switch event {
        case .stateSync, .userActivity, .threadTrusted:
            NotificationCenter.default.post(.refreshInbox)
        default:
            break
        }
    }

    @MainActor
    fileprivate func onUserActivity(_ event: Schema.UserActivityEvent) {
        guard let thread = getThreadFromAllInboxes(event.threadID) else {
            return
        }

        switch event.activityType {
        case .typing, .recordingVoice, .recordingVideo, .custom:
            thread.onUserActivity(
                type: event.activityType,
                participantID: event.participantID,
                customLabel: event.customLabel,
                durationMs: event.durationMs
            )
        case .none:
            thread.onUserActivityStopped(participantID: event.participantID)
        default:
            break
        }
    }

    fileprivate func onThreadMessagesRefresh(_ event: Schema.ThreadMessagesRefreshEvent) {
        guard let thread = getThreadFromAllInboxes(event.threadID) else {
            return
        }

        Task { @MainActor in
            try await thread.getMessages()
        }
    }

    fileprivate func onThreadTrusted(_ event: Schema.ThreadTrustedEvent) {
        guard let thread = inboxes[.requests]?.threads.items[id: event.threadID] else {
            return
        }
        inboxes[.normal]?.threads.items.updateOrAppend(thread)
        inboxes[.requests]?.threads.items.remove(thread)

        thread.folderName = Schema.InboxName.normal.rawValue
    }

    fileprivate func onStateSync(_ event: Schema.StateSyncEvent) {
        switch event {
        case .customEmojis(let mutation):
            onStateSyncCustomEmojis(mutation: mutation)
        case .threads(let mutation):
            onStateSyncThreads(mutation: mutation)
        case .participants(let mutation, let threadID):
            onStateSyncParticipants(mutation: mutation, threadID: threadID)
        case .messages(let mutation, let threadID):
            onStateSyncMessages(mutation: mutation, threadID: threadID)
        case .messageReactions(let mutation, let threadID, let messageID):
            onStateSyncMessageReactions(mutation: mutation, threadID: threadID, messageID: messageID)
        // default:
        //     logger.error("Unimplemented event: \("\(event)")")
        }
    }

    fileprivate func onStateSyncCustomEmojis(mutation: Schema.StateSyncEvent.Mutation<Schema.CustomEmoji>) {
        switch mutation {
        case .upsert(let emojis):
            for emoji in emojis {
                customEmojis[emoji.id] = emoji
            }
        case .update(let emojis):
            for emoji in emojis {
                guard var copy = customEmojis[emoji.id] else {
                    continue
                }

                if let url = emoji.url {
                    copy.url = url
                }

                customEmojis[emoji.id] = copy
            }
        case .delete(let emojis):
            let emojiIDs = Set(emojis)
            for id in emojiIDs {
                customEmojis.removeValue(forKey: id)
            }
        case .deleteAll:
            customEmojis = [:]
        }
    }

    fileprivate func onStateSyncThreads(mutation: Schema.StateSyncEvent.Mutation<Schema.Thread>) {
        switch mutation {
        case .upsert(let threads):
            // TODO: Elide unnecessary updates here (so that we only publish changes once)
            Task(priority: .utility) {
                AppDatabase.cacheThreads(threads: threads, account: self)
            }

            for thread in threads {
                guard let inbox = inboxes[.init(rawValue: thread.folderName ?? "normal")] else {
                    self.logger.warning("thread upsert unknown inbox: \(thread.folderName ?? "normal")")
                    continue
                }
                let threadStore = ThreadStore(base: thread, accountID: id)
                inbox.threads.items.updateOrAppend(threadStore)
            }
        //            logger.debug("UPSERTED \(threads.count) threads")
        case .update(let threads):
            Task(priority: .utility) {
                AppDatabase.updateCachedThreads(threads: threads, account: self)
            }

            var missingThreads: [Schema.ThreadID] = []

            for partial in threads {
                if let thread = getThreadFromAllInboxes(partial.id) {
                    thread.apply(partial: partial)
                } else {
                    missingThreads.append(partial.id)
                }
            }

            if !missingThreads.isEmpty {
                Task { @MainActor in
                    try await getThreads(.newer)
                }
            }
        //            logger.debug("UPDATED \(threads.count) threads")
        case .delete(let threads):
            let threadIDs = Set(threads)

            AppDatabase.deleteCachedThreads(threadIDs: threadIDs, account: self)

            inboxes.forEach { _, inbox in
                inbox.threads.items.removeAll { threadIDs.contains($0.id) }
            }
        //            logger.debug("DELETED \(threads.count) threads")
        case .deleteAll:
            inboxes.forEach { _, inbox in
                inbox.threads = .init()
            }
        }
    }

    fileprivate func onStateSyncParticipants(mutation: Schema.StateSyncEvent.Mutation<Schema.User>, threadID: Schema.ThreadID?) {
        if threadID == nil {
            //            logger.debug("updating \(participants.count) participants for account")
            return
        }
        guard let thread = getThreadFromAllInboxes(threadID!) else {
            return
        }
        switch mutation {
        case .upsert(let participants):
            Task(priority: .utility) {
                AppDatabase.cacheUsers(users: participants, account: self)
            }

            var copy = thread.participants
            copy.merge(items: participants, direction: .after)
            thread.participants = copy
        //                logger.debug("upserting \(participants.count) participants for thread \(threadID)")
        case .update(let participants):
            Task(priority: .utility) {
                AppDatabase.updateCachedUsers(users: participants, account: self)
            }

            var copy = thread.participants
            for partial in participants {
                copy.items[id: partial.id]?.coalesceInPlace(with: partial)
            }
            thread.participants = copy
        //                logger.debug("updating \(participants.count) participants for thread \(threadID)")
        case .delete(let participants):
            let participantIDs = Set(participants)
            var copy = thread.participants
            copy.items.removeAll { participantIDs.contains($0.id) }
            thread.participants = copy
        //                logger.debug("deleted \(participants.count) participants for thread \(threadID)")
        case .deleteAll:
            thread.participants = .init()
        }
    }

    fileprivate func onStateSyncMessages(mutation: Schema.StateSyncEvent.Mutation<Schema.Message>, threadID: Schema.ThreadID) {
        guard let thread = getThreadFromAllInboxes(threadID) else {
            return
        }

        switch mutation {
        case .upsert(let messages):
            onStateSyncUpsertMessageEvent(messages: messages, thread: thread)
        case .update(let messages):
            var copy = thread.messages
            for partial in messages {
                copy.items[id: partial.id]?.coalesceInPlace(with: partial)
            }
            thread.messages = copy
        //                logger.debug("UPDATED \(messages.count) messages")
        case .delete(let messages):
            let messageIDs = Set(messages)

            AppDatabase.deleteCachedMessages(messageIDs: messageIDs, account: self)

            var copy = thread.messages
            copy.items.removeAll { messageIDs.contains($0.id) }
            thread.messages = copy
        //                logger.debug("DELETED \(messages.count) messages")
        case .deleteAll:
            thread.messages.items = .init()
        //                logger.debug("DELETED ALL messages in thread \(threadID)")
        }
    }

    fileprivate func onStateSyncUpsertMessageEvent(messages: [Schema.Message], thread: ThreadStore) {
        var copy = thread.messages
        let lastMessageTimestamp = copy.items.last?.timestamp

        for message in messages {
            copy.items.updateOrAppend(message)

            // only handle new messages
            if lastMessageTimestamp != nil && message.timestamp >= lastMessageTimestamp {
                guard let action = message.action else { continue }

                if action.type == .threadTitleUpdated {
                    thread.title = action.title
                } else if action.type == .threadParticipantsRemoved {
                    let participantIDs = Set(action.participantIDs!)
                    var participantsCopy = thread.participants
                    participantsCopy.items.removeAll(where: { participantIDs.contains($0.id.rawValue) })
                    thread.participants = participantsCopy
                    //                    logger.debug("deleted \(participants.count) participants for thread \(threadID)")
                }
            }
        }

        if let lastMessage = copy.last {
            try? AppDatabase.shared.writer.write { db in
                try AppDatabase
                    .updateCachedThreadTimestamp(
                        db,
                        threadID: thread.id,
                        account: self,
                        timestamp: lastMessage.timestamp
                    )
            }
        }

        Task(priority: .utility) {
            AppDatabase.cacheMessages(messages: messages, threadID: thread.id, account: self)
        }

        if let last = copy.last, last.isSender != true {
            if last.behavior != .silent || last.behavior != .keepRead {
                thread.isUnread = true
            }
        }

        messages.forEach { _ = thread.linkedMessages.removeValue(forKey: $0.id) }

        thread.messages = copy
        //        logger.debug("UPSERTED \(messages.count) messages")
    }

    fileprivate func onStateSyncMessageReactions(
        mutation: Schema.StateSyncEvent.Mutation<Schema.MessageReaction>,
        threadID: Schema.ThreadID,
        messageID: Schema.MessageID
    ) {
        guard let thread = getThreadFromAllInboxes(threadID) else {
            return
        }
        guard var message = thread.messages.items[id: messageID] else {
            logger.warning("Could not find message \(messageID) in thread \(threadID)")
            return
        }
        switch mutation {
        case .upsert(let reactions):
            if message.reactions == nil {
                message.reactions = []
            }

            for reaction in reactions {
                message.reactions!.updateOrAppend(reaction)
            }

            thread.messages.items[id: messageID] = message
        //                logger.debug("upserting \(reactions.count) reactions in message \(message.id)")
        case .delete(let reactions):
            let reactionIDs = Set(reactions)
            guard message.reactions != nil else { return }

            message.reactions!.removeAll { reactionIDs.contains($0.id) }
            thread.messages.items[id: messageID] = message
        //                logger.debug("deleting \(reactions.count) reactions from message \(message.id)")
        default:
            //                logger.error("Unimplemented event: \("\(event)")")
            return
        }
    }

    fileprivate func onToast(_ event: Schema.ToastEvent) {
        // let toast: Toasts.Toast = Toasts.Toast(id: String(event.hashValue), label: data.toast.text, expandedText: nil, dismissType: .after(5), onTap: nil, onUndo: nil)
        // Toasts.shared.add(toast)
    }

    fileprivate func onUserPresenceUpdated(_ event: Schema.UserPresenceEvent) {
        presenceMap[event.presence.userID] = event.presence
    }

    fileprivate func onSessionUpdated() {
        Task {
            try await accountInstance().refreshSession()
        }
    }
}

// MARK: - Auxiliary Implementation -

public enum AccountError: CustomStringConvertible, Error {
    case invalidAccount
    case notLoggedIn
    case uninitialized

    public var description: String {
        switch self {
        case .invalidAccount:
            return "Invalid Account"
        case .notLoggedIn:
            return "Not Logged In"
        case .uninitialized:
            return "Unintialized"
        }
    }
}
