//
// Copyright (c) Texts HQ
//

import IdentifiedCollections
import FoundationX
import KeychainAccess
import Merge
import os
import Swallow
import TextsBase

public class AccountsStore: ObservableObject {
    let logger = Logger(category: "AccountsStore")

    private var cancellables = Cancellables()

    private var accountObservations: [Schema.AccountID: Set<AnyCancellable>] = [:]

    @UserDefault.Published("accounts")
    private var persistedAccounts: IdentifiedArrayOf<AccountInstance.Persisted> = []
    @Published public private(set) var accounts: IdentifiedArrayOf<AccountInstance> = []
    @Published public private(set) var hasLoadedAccounts = false

    @PublishedObject public private(set) var platformsStore: PlatformsStore

    @Published public private(set) var displayThreads = IdentifiedArray(id: \ThreadStore.uniqueID)

    @Published public var selectedInbox: Schema.InboxName = .normal {
        didSet {
            accounts.forEach { account in
                account.currentAccountStore?.selectedInbox = selectedInbox
            }
            updateDisplayThreads()
        }
    }

    private var preferences: [Schema.AccountID: [String: Schema.PlatformPref.PrefValue]] = Preferences.shared.accountPreferences

    public var hasPlatformWithRequestsInbox: Bool {
        accounts.contains(where: { $0.platform.attributes.contains(.supportsRequestsInbox) })
    }

    public func updateDisplayThreads() {
        displayThreads = .init(uniqueElements: accounts.flatMap { $0.currentAccountStore.flatMap(\.currentInbox?.threads) ?? .init() }, id: \.uniqueID)
    }

    public var isLoadingThreads: Bool {
        accounts.contains(where: \.state.isLoadingThreads)
    }

    init(platformsStore: PlatformsStore) {
        self.platformsStore = platformsStore

        platformsStore.$hasLoadedPlatforms
            .receiveOnMainQueue()
            .sink(in: cancellables) { _ in
                self.loadAccounts()
                self.hasLoadedAccounts = true
            }

        PlatformServer.shared.$state
            .first(where: { $0.status == .launched })
            .sink(in: cancellables) { _ in
                guard NetworkMonitor.shared.connected else {
                    return
                }
                Task { @MainActor in
                    try await self.initializeAllOfflineAccounts()
                }
            }

        NetworkMonitor.shared.$connected
            .sink { connected in
                guard connected else { return }
                Task { @MainActor in
                    try await self.initializeAllOfflineAccounts()
                    try await self.reconnectAllAccounts()
                }
            }
            .store(in: cancellables)

        Preferences.shared
            .objectWillChange
            .filter { Preferences.shared.accountPreferences != self.preferences }
            .sink(in: cancellables) {
                Preferences.shared.accountPreferences.forEach { key, value in
                    guard self.preferences[key] != value else { return }

                    if let ai = self.accounts[id: key] {
                        self.preferences[key] = value

                        Task { @MainActor in
                            try await self.disableAccount(ai)
                            try await self.enableAccount(ai)
                        }
                    }
                }
            }

        updateDisplayThreads()
    }

    subscript(_ id: Schema.AccountID) -> AccountInstance? {
        accounts[id: id]
    }

    @MainActor
    private func initializeAllOfflineAccounts() async throws {
        guard PlatformServer.shared.status == .launched else {
            return
        }

        try await accounts
            .filter({ $0.state.isOffline })
            .concurrentForEach { account in
                try await self.initializeAccount(account)
            }
    }

    private func startObserving(account: AccountInstance) {
        guard accountObservations[account.id] == nil else { return }
        var cancellables: Set<AnyCancellable> = []
        account
            .persisted
            .receive(on: DispatchQueue.main)
            .sink { [weak self] session, persisted in
                guard let self = self else { return }
                Keychain.texts[account.id.rawValue] = session
                self.persistedAccounts.updateOrAppend(persisted)
            }
            .store(in: &cancellables)

        // updateDisplayThreads on change of account $state, $selectedInbox, $threads,
        // or any of the timestamps within $threads
        account
            .$state
            .mapTo(())
            .merge(
                with: account.accountStore
                    .discardError()
                    .flatMap {
                        $0.$selectedInbox.mapTo(())
                    }
                    .mapTo(())
            )
            .merge(
                with: account.accountStore
                    .discardError()
                    .flatMap {
                        $0.inboxes.map { _, inbox in
                            inbox.$threads.mapTo(()).merge(
                                with: inbox.$threads.flatMap {
                                    $0.items
                                        .map(\.$timestamp)
                                        .mergeManyPublisher
                                        .mapTo(())
                                }
                            )
                        }
                        .mergeManyPublisher
                        .mapTo(())
                    }
            )
            .debounce(for: .milliseconds(200), scheduler: DispatchQueue.main)
            .sink { [weak self] _ in
                self?.updateDisplayThreads()
            }
            .store(in: &cancellables)

        accountObservations[account.id] = cancellables
    }

    private func stopObserving(account: AccountInstance) {
        accountObservations.removeValue(forKey: account.id)
    }
}

extension AccountsStore {
    public func initializeBareAccount(
        _ account: AccountInstance
    ) -> AnyTask<Void, Error> {
        // whether this is reauth or the initial login, we want initialize() to skip
        // the stuff it does for logged-in accounts; we'll get to that stuff in
        // login().
        return Task { @MainActor in
            try await account.initialize(withoutSession: true)
        }
        .convertToObservableTask()
    }

    @MainActor
    public func initializeAccount(_ account: AccountInstance) async throws {
        try await account.initialize()
        try await self.changeAccountIDIfPossible(account: account)
        // re-enable account if changeAccountID is ran
        if !account.state.isActive {
            try await self.enableAccount(account)
        }
    }

    public func loadAccounts() {
        for persisted in persistedAccounts {
            guard accounts[id: persisted.id] == nil else {
                continue
            }

            accounts[id: persisted.id] = loadAccount(persisted)
        }

        #if INDEXDB_ENABLED
        updateDisplayThreads()
        #endif
    }

    public func loadAccount(_ persisted: AccountInstance.Persisted) -> AccountInstance? {
        guard let platform = platformsStore.platforms[id: persisted.platformName] else {
            return nil
        }

        let session = Keychain.texts[persisted.id.rawValue]

        let instance = self.accounts[id: persisted.id] ?? AccountInstance(
            accountsStore: self,
            persisted: persisted,
            session: session,
            platform: platform
        )

        self.startObserving(account: instance)

        if PlatformServer.shared.status == .launched,
           instance.state.isOffline,
           NetworkMonitor.shared.connected {
            Task { @MainActor in
                do {
                    try await self.initializeAccount(instance)
                } catch {
                    self.logger.error("Failed to initialize \(instance.id): \(String(describing: error))")
                }
            }
        }

        return instance
    }

    /// Authenticates the provided account and saves it if needed
    /// - Parameters:
    ///   - account: The account to add
    ///   in (for example via custom auth)
    @MainActor
    public func addAccount(_ account: AccountInstance) async throws {
        do {
            try await changeAccountIDIfPossible(account: account)

            self.accounts.append(account)

            try await account.initialize()

            guard let store = account.currentAccountStore else {
                throw AccountError.uninitialized
            }

            account.isSaved = true
            // if we're reauthenticating, we likely already have the account
            // observed but this method should be idempotent
            self.startObserving(account: account)

            await PushNotificationManager.shared.handleAccountAddition(store)
        } catch {
            Task.detached {
                try await self.disableAccount(account)
            }

            throw error
        }
    }

    @discardableResult
    public func enableAccount(_ account: AccountInstance) async throws -> AccountStore {
        logger.info("Attempting to enable account: \(account)")

        switch account.state {
        // we don't want to disallow initialize() on ReauthError, because that's
        // a valid use case that we see in the addAccount(_:credentials:) codepath.
        // However, that use case is *only* valid through addAccount (since here
        // we wouldn't end up logging in) so we bail early here
        case .disabled(let error as AccountInstance.ReauthError):
            throw error
        default:
            let store = try await account.initialize()

            await PushNotificationManager.shared.handleAccountAddition(store)

            return store
        }
    }

    @MainActor
    public func disableAccount(_ account: AccountInstance) async throws {
        logger.info("Disabling account: \(account)")
        await PushNotificationManager.shared.handleAccountRemoval(account)

        return try await account.isSaved ? account.dispose() : removeAccount(account)
    }

    public func reconnectAllAccounts() async throws {
        guard PlatformServer.shared.status == .launched,
              NetworkMonitor.shared.connected else {
            return
        }

        try await AppState.shared.rootStore.accountsStore.accounts.concurrentForEach { account in
            guard account.state == .loaded else {
                return
            }
            try await account.api.reconnectRealtime()
        }
    }
}

extension AccountsStore {
    /// Logs out of the account, disposes it and removes from keychain
    /// - Parameter account: Account to be removed
    @MainActor
    public func removeAccount(_ account: AccountInstance) async throws {
        await PushNotificationManager.shared.handleAccountRemoval(account)

        loop: for await state in account.$state.values {
            switch state {
            case .disabled, .disabling, .offline:
                break loop
            case .loading:
                continue
            case .loaded(let store, _):
                // TODO: Improve handling of logout failure
                do {
                    try await store.logout()

                    await account.dispose()

                    try account.cleanupAccount()
                } catch {
                    logger.error("Failed to log-out and dispose of account \(account.id)")
                }

                break loop
            }
        }

        AppDatabase.deleteAccountData(accountID: account.id)

        stopObserving(account: account)

        Keychain.texts[account.id.rawValue] = nil
        Preferences.shared.accountPreferences[account.id] = nil

        account.isSaved = false
        accounts.remove(account)
        persistedAccounts.remove(id: account.id)

        self.updateDisplayThreads()
    }

    @MainActor
    public func removeAllAccounts() async throws {
        try await accounts.concurrentForEach { @MainActor account in
            try await self.removeAccount(account)
        }
    }

    @MainActor
    public func removeAt(index: Int) async throws {
        let accountInstance = accounts[index]

        try await removeAccount(accountInstance)
    }

    @MainActor
    public func removeAllAccountsWithUserID(_ userID: Schema.UserID) async throws {
        try await self.accounts.concurrentForEach { account in
            guard account.user?.id == userID else {
                return
            }

            try await self.removeAccount(account)
        }
    }
}

extension AccountsStore {
    public func changeAccountIDIfPossible(account: AccountInstance) async throws {
        guard let userID = account.user?.id else {
            return
        }

        let userIDHash = try userID.rawValue.md5()
        let deterministicAccountID = Schema.AccountID(rawValue: "\(account.platform.id)_\(userIDHash)")

        if account.id == deterministicAccountID {
            return
        }

        if accounts.contains(where: { $0.id == deterministicAccountID }) {
            return
        }

        logger.info("Changing Account ID: \(account.id) -> \(deterministicAccountID)")
        try await changeAccountID(account, newAccountID: deterministicAccountID)
    }

    @MainActor
    public func changeAccountID(_ account: AccountInstance, newAccountID: Schema.AccountID) async throws {
        if account.state.isActive {
            await account.dispose()
        }
        let oldAccountID = account.id
        let emptyNewDirIfExists = accounts[id: newAccountID] != nil
        try moveDataDir(
            account: account,
            newAccountID: newAccountID,
            emptyNewDir: emptyNewDirIfExists
        )

        try await AppDatabase.changeAccountId(oldAccountID: oldAccountID, newAccountID: newAccountID)

        if let keychainData = Keychain.texts[oldAccountID.rawValue] {
            Keychain.texts[newAccountID.rawValue] = keychainData
            Keychain.texts[oldAccountID.rawValue] = nil
        }

        if let preferences = Preferences.shared.accountPreferences[oldAccountID] {
            Preferences.shared.accountPreferences[oldAccountID] = nil
            Preferences.shared.accountPreferences[newAccountID] = preferences
            self.preferences[oldAccountID] = nil
            self.preferences[newAccountID] = preferences
        }

        if let accountIndex = accounts.index(id: oldAccountID) {
            self.accounts.remove(at: accountIndex)
            account.id = newAccountID
            self.accounts.insert(account, at: accountIndex)
        } else {
            account.id = newAccountID
        }

        if let persistedIndex = persistedAccounts.index(id: oldAccountID),
           var persistedAccount = persistedAccounts[id: oldAccountID] {
            self.persistedAccounts.remove(at: persistedIndex)
            persistedAccount.id = newAccountID
            self.persistedAccounts.insert(persistedAccount, at: persistedIndex)
        }
    }

    public func moveDataDir(
        account: AccountInstance,
        newAccountID: Schema.AccountID,
        emptyNewDir: Bool
    ) throws {
        let oldDataDirPath = try account.dataDir()
        let newDataDirPath = try AccountInstance.accountsDir().appendingPathComponent(newAccountID.rawValue)

        let fileManager = FileManager.default

        guard fileManager.directoryExists(at: oldDataDirPath) else {
            return
        }

        if fileManager.directoryExists(at: newDataDirPath) {
            if !emptyNewDir {
                return
            }
            try? fileManager.removeItem(at: newDataDirPath)
        }

        try fileManager.moveItem(at: oldDataDirPath, to: newDataDirPath)
    }
}

extension AccountsStore {
    public func getThreadFromAllInboxes(_ threadID: Schema.ThreadID) -> ThreadStore? {
        for account in accounts {
            guard let thread = account.currentAccountStore?.getThreadFromAllInboxes(threadID) else { continue }
            return thread
        }

        return nil
    }
}
