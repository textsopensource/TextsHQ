import Foundation
import Swallow
import Merge

@_spi(APICaller) import TextsBase

@dynamicMemberLookup
public struct APICaller {
    fileprivate let account: AccountInstance

    private struct InputWrapper<Input, Output>: Encodable {
        let method: Methods.Method<Input, Output>
        let input: Input
        func encode(to encoder: Encoder) throws {
            try method.encode(input: input, to: encoder)
        }
    }

    private static let decoderKey = CodingUserInfoKey(rawValue: "PASOutputDecoder")

    private struct OutputWrapper<Output>: Decodable {
        let output: Output
        init(from decoder: Decoder) throws {
            guard let outputDecoder = decoder.userInfo[APICaller.decoderKey] as? ((Decoder) throws -> Output) else {
                throw DecodingError.dataCorrupted(.init(
                    codingPath: decoder.codingPath,
                    debugDescription: "OutputWrapper was not given a PASOutputDecoder"
                ))
            }
            output = try outputDecoder(decoder)
        }
    }

    private func method<Input, Output>(for keyPath: KeyPath<Methods, Methods.Method<Input, Output>>) -> (Input) -> AnySingleOutputPublisher<Output, Error> {
        let method = Methods()[keyPath: keyPath]
        return { input in
            let encoded: Data
            do {
                encoded = try PlatformServer.makeEncoder().encode(InputWrapper(method: method, input: input))
            } catch {
                return .failure(error)
            }
            return PlatformServer.shared.callAnyMethod(method.name, on: account, with: encoded)
                .successPublisher
                .tryMap {
                    let decoder = PlatformServer.makeDecoder()
                    decoder.userInfo[Self.decoderKey] = method.decode
                    return try decoder.decode(OutputWrapper<Output>.self, from: $0).output
                }
                .eraseToAnySingleOutputPublisher()
        }
    }

    @_disfavoredOverload
    public subscript<Input, Output>(dynamicMember keyPath: KeyPath<Methods, Methods.Method<Input, Output>>) -> (Input) -> AnySingleOutputPublisher<Output, Error> {
        method(for: keyPath)
    }

    public subscript<Input, Output>(dynamicMember keyPath: KeyPath<Methods, Methods.Method<Input, Output>>) -> (Input) async throws -> Output {
        { try await method(for: keyPath)($0).output() }
    }

    @_disfavoredOverload
    public subscript<Output>(dynamicMember keyPath: KeyPath<Methods, Methods.Method<Void, Output>>) -> () -> AnySingleOutputPublisher<Output, Error> {
        let m = method(for: keyPath)
        return { m(()) }
    }

    public subscript<Output>(
        dynamicMember keyPath: KeyPath<Methods, Methods.Method<Void, Output>>
    ) -> (() async throws -> Output) {
        let m = method(for: keyPath)
        return { try await m(()).output() }
    }
}

extension AccountInstance {
    public var api: APICaller {
        .init(account: self)
    }
}
