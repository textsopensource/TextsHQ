//
// Copyright (c) Texts HQ
//

import Foundation
import Merge
import Swallow
import TextsBase

public enum ThreadError: Error {
    case messageNotFound
}

extension ThreadStore {
    private var participantID: Schema.UserID? {
        currentUser?.id
    }

    private var allowsMultipleReactions: Bool {
        let result = try? account?.accountInstance().platform.reactions?.allowsMultipleReactionsToSingleMessage
        return result ?? false
    }

    private func accountInstance() throws -> AccountInstance {
        try account.unwrap().accountInstance()
    }

    @MainActor
    public func deleteMessage(withID messageID: Schema.MessageID) async throws {
        let result = try await accountInstance().api.deleteMessage((threadID: id, messageID: messageID))

        if result {
            _ = messages.items.remove(id: messageID)
        }
    }

    public func getMessage(_ messageID: Schema.MessageID) async throws -> Schema.Message? {
        guard let message = try await accountInstance().api.getMessage((
            threadID: id,
            messageID: messageID
        )) else {
            return nil
        }

        Task(priority: .utility) {
            try await AppDatabase.shared.writer.write { db in
                try AppDatabase.cacheMessage(
                    db,
                    message: message,
                    threadID: self.id,
                    account: self.account.unwrap()
                )
            }
        }

        return message
    }

    public func getMessages(
        before: Schema.Message? = nil
    ) async throws {
        let accountInstance = try accountInstance()
        let sortOnPush = accountInstance.platform.attributes.contains(.sortMessagesOnPush)

        #if INDEXDB_ENABLED
        try await getMessagesFromDatabase(before: before)

        guard NetworkMonitor.shared.connected else {
            return
        }
        #endif

        let task = Task {
            var messages = await MainActor.run {
                self.messages
            }

            var fetchedMessages: Schema.Paginated<Schema.Message>

            do {
                fetchedMessages = try await accountInstance.api.getMessages((
                    threadID: id,
                    args: before.map {
                        Schema.PaginationArguments(cursor: $0.cursor ?? $0.id.rawValue, direction: .before)
                    }
                ))
            } catch {
                logger.error(error)

                throw error
            }

            if !sortOnPush {
                // if we're not sorting globally, at least sort the incoming
                // "batch" of items
                fetchedMessages.sortMessages()
            }

            if before != nil {
                messages.merge(fetchedMessages, direction: .before)
            } else {
                messages = fetchedMessages
            }

            if sortOnPush {
                messages.sortMessages()
            }

            messages.items.removeAll(where: { $0.id.rawValue.hasPrefix("temp-") })
            messages.forEach { _ = linkedMessages.removeValue(forKey: $0.id) }

            let messagesCopy = messages

            await MainActor.run {
                self.messages = messagesCopy
            }

            Task(priority: .utility) {
                do {
                    try AppDatabase.cacheMessages(
                        messages: messagesCopy.items.elements,
                        threadID: self.id,
                        account: self.account.unwrap()
                    )

                    if let lastMessage = messagesCopy.last, self.timestamp != lastMessage.timestamp {
                        try await AppDatabase.shared.writer.write { db in
                            try AppDatabase.updateCachedThreadTimestamp(
                                db,
                                threadID: self.id,
                                account: try self.account.unwrap(),
                                timestamp: lastMessage.timestamp
                            )
                        }
                    }
                } catch {
                    logger.error(error)
                }
            }
        }

        #if !DEBUG
        _ = try await task.value
        #endif
    }

    private func getMessagesFromDatabase(
        before: Schema.Message?
    ) async throws {
        var fetchedMessages = try AppDatabase.getMessages(
            accountID: accountID,
            threadID: id,
            before: before
        )

        guard !fetchedMessages.isEmpty else {
            return
        }

        fetchedMessages.reverse()

        var messages = before == nil ? Schema.Paginated() : await MainActor.run {
            self.messages
        }

        messages.merge(items: messages, direction: .before)
        messages.hasMore = true

        let messagesCopy = messages

        await MainActor.run {
            self.messages = messagesCopy
        }
    }

    // Returns whether a refresh is required.
    @MainActor
    private func sendMessages(
        _ messages: [(Schema.MessageContent, Schema.SendMessageOptions)],
        ai: AccountInstance
    ) async throws -> Bool {
        struct FalsySendError: Error { }

        guard let message = messages.first else {
            return false
        }

        do {
            let sent = try await ai.api.sendMessage((
                threadID: id,
                content: message.0,
                options: message.1
            ))

            let needsRefresh: Bool

            switch sent {
            case .left(false):
                throw FalsySendError()
            case .left(true):
                needsRefresh = true
            case .right(let newMessages):
                newMessages.first.map {
                    pendingToRealMap[$0.id] = message.1.pendingMessageID
                }

                pendingMessages.remove(id: message.1.pendingMessageID)

                Task(priority: .utility) {
                    AppDatabase.cacheMessages(
                        messages: newMessages,
                        threadID: id,
                        account: try account.unwrap()
                    )
                }

                var copy = self.messages
                newMessages.forEach { copy.items.updateOrAppend($0) }
                self.messages = copy

                needsRefresh = false
            }

            return try await sendMessages(Array(messages[1...]), ai: ai) || needsRefresh
        } catch {
            pendingMessages[id: message.1.pendingMessageID]?.isErrored = true

            return false
        }
    }

    @MainActor
    public func sendMessage(messageDraft: MessageDraft) async throws {
        isSending = true
        defer {
            isSending = false
        }
        enum SendMessageError: LocalizedError {
            case messageDraftEmpty

            var errorDescription: String? {
                "The message draft cannot be empty."
            }
        }

        guard !messageDraft.isEmpty else {
            throw SendMessageError.messageDraftEmpty
        }

        let ai = try accountInstance()

        let newMessages: [Schema.MessageContent]
        if messageDraft.attachments.isEmpty {
            newMessages = [Schema.MessageContent(
                text: messageDraft.messageText,
                mentionedUserIDs: messageDraft.mentionedUsers
            )]
        } else if ai.platform.attachments?.supportsCaption == true && messageDraft.attachments.count == 1 {
            var content = messageDraft.attachments[0].messageContent
            content.text = messageDraft.messageText
            content.mentionedUserIDs = messageDraft.mentionedUsers
            newMessages = [content]
        } else {
            newMessages = [Schema.MessageContent(
                text: messageDraft.messageText,
                mentionedUserIDs: messageDraft.mentionedUsers
            )] + messageDraft.attachments.map(\.messageContent)
        }

        let newMessagesWithOptions: [(Schema.MessageContent, Schema.SendMessageOptions)] = newMessages.map { content in
            let options = Schema.SendMessageOptions(
                pendingMessageID: Schema.MessageID(rawValue: UUID().uuidString),
                quotedMessageThreadID: messageDraft.quotedMessage?.threadID,
                quotedMessageID: messageDraft.quotedMessage?.messageID
            )
            return (content, options)
        }

        let pendingIDs = Set(newMessagesWithOptions.map(\.1.pendingMessageID))

        var pendingCopy = pendingMessages

        newMessagesWithOptions.map { content, options in
            Schema.Message(
                id: options.pendingMessageID,
                timestamp: Date(),
                text: content.text,
                attachments: content.asPendingAttachment().map { [$0] } ?? [],
                isDelivered: false,
                isSender: true,
                linkedMessageID: options.quotedMessageID,
                accountID: accountID,
                threadID: id
            )
        }
        .forEach({ pendingCopy.append($0) })

        pendingMessages = pendingCopy

        let accountInstance = try accountInstance()
        let needsRefresh = try await sendMessages(newMessagesWithOptions, ai: accountInstance)

        pendingMessages.removeAll {
            $0.isErrored != true && pendingIDs.contains($0.id)
        }

        if needsRefresh {
            _ = try await getMessages()
        }
    }

    @MainActor
    public func resendPendingMessage(_ pendingMessageID: Schema.MessageID) async throws {
        guard let message = pendingMessages[id: pendingMessageID] else { return }

        let mentionedUserIDs = message.textAttributes?.entities?
            .compactMap { $0.mentionedUser?.id }

        let newMessage = [(
            Schema.MessageContent(
                text: message.text,
                mentionedUserIDs: mentionedUserIDs ?? []
            ),
            Schema.SendMessageOptions(
                pendingMessageID: pendingMessageID,
                quotedMessageThreadID: message.threadID,
                quotedMessageID: message.linkedMessageID
            )
        )]

        pendingMessages.remove(id: pendingMessageID)
        pendingMessages.append(message)

        let accountInstance = try accountInstance()
        let needsRefresh = try await sendMessages(newMessage, ai: accountInstance)

        pendingMessages.removeAll {
            $0.isErrored != true && $0.id == pendingMessageID
        }

        if needsRefresh {
            _ = try await getMessages()
        }
    }

    @MainActor
    public func removePendingMessage(_ pendingMessageID: Schema.MessageID) {
        pendingMessages.remove(id: pendingMessageID)
    }

    @MainActor
    private func reactionID(for reaction: ReactionPickerChoice, participant: Schema.UserID) -> Schema.MessageReactionID {
        .init(rawValue: "\(participant)\(allowsMultipleReactions ? reaction.key : "")")
    }

    @MainActor
    public func addReaction(messageID: Schema.MessageID, reaction: ReactionPickerChoice) async throws {
        let message = try messages.items[id: messageID].unwrapOrThrow(ThreadError.messageNotFound)
        let participantID = try participantID.unwrapOrThrow(AccountError.invalidAccount)
        let ai = try accountInstance()

        if Preferences.shared.stealthMode, isUnread {
            try await markAsRead()
        }

        let reactionID = reactionID(for: reaction, participant: participantID)
        let newReaction = Schema.MessageReaction(
            id: reactionID,
            reactionKey: reaction.key,
            participantID: participantID,
            emoji: reaction.isEmoji
        )

        var newMessage = message
        if newMessage.reactions == nil {
            newMessage.reactions = [newReaction]
        } else {
            newMessage.reactions!.updateOrAppend(newReaction)
        }
        messages.items[id: messageID] = newMessage

        do {
            try await ai.api.addReaction((threadID: id, messageID: messageID, reactionKey: reaction.key)).value
        } catch {
            // rollback
            messages.items[id: messageID] = message
            throw error
        }
    }

    @MainActor
    public func removeReaction(
        messageID: Schema.MessageID,
        reaction: ReactionPickerChoice
    ) async throws {
        let message = try messages.items[id: messageID].unwrapOrThrow(ThreadError.messageNotFound)
        let participantID = try participantID.unwrapOrThrow(AccountError.invalidAccount)
        let ai = try accountInstance()
        let reactionID = reactionID(for: reaction, participant: participantID)

        var newMessage = message
        newMessage.reactions?.remove(id: reactionID)
        messages.items[id: messageID] = newMessage

        do {
            try await ai.api.removeReaction((threadID: id, messageID: messageID, reactionKey: reaction.key)).value
        } catch {
            messages.items[id: messageID] = message

            throw error
        }
    }

    @MainActor
    public func sendActivityIndicator(type: Schema.ActivityType) async throws {
        try await accountInstance().api.sendActivityIndicator((type: type, threadID: id))
    }

    @MainActor
    public func markAsRead() async throws {
        guard isUnread else {
            return
        }

        isUnread = false

        if let isUnread = props.isUnread, isUnread {
            props.isUnread = nil
        }

        try await accountInstance().api.sendReadReceipt((threadID: id, messageID: messages.last?.id, messageCursor: messages.last?.cursor))
    }

    @MainActor
    public func markAsUnread() async throws {
        guard !isUnread else {
            return
        }

        isUnread = true

        if let supportsMarkAsUnread = account?.platform?.attributes.contains(.supportsMarkAsUnread),
           supportsMarkAsUnread {
            try await accountInstance().api.markAsUnread(id)
        } else {
            props.isUnread = true
        }
    }

    public func sendStopTypingIndicator() async throws {
        let accountInstance = try accountInstance()

        guard accountInstance.platform.attributes.contains(.supportsStopTypingIndicator) else {
            return
        }

        try await accountInstance.api.sendActivityIndicator((
            type: .none,
            threadID: self.id
        ))
    }

    public func onTyping() async throws {
        let accountInstance = try accountInstance()
        guard Preferences.shared.showTypingIndicator,
              !accountInstance.platform.attributes.contains(.noSupportTypingIndicator) else {
            return
        }

        if accountInstance.platform.attributes.contains(.supportsStopTypingIndicator) {
            if stopTypingTask == nil {
                try await sendActivityIndicator(type: .typing)
            }

            stopTypingTask?.cancel()
            stopTypingTask = DispatchWorkItem {
                Task {
                    try? await self.sendStopTypingIndicator()
                    self.stopTypingTask = nil
                }
            }

            DispatchQueue.main.asyncAfter(
                deadline: .now() + 3,
                execute: stopTypingTask!
            )
            return
        }

        try await account?.throttledSendActivityIndiactor(.typing, threadID: self.id)
    }

    @MainActor
    public func setIsPinned(_ newValue: Bool) async throws {
        if let supportsPinThread = account?.platform?.attributes.contains(.supportsPinThread),
           supportsPinThread {
            try await accountInstance().api.pinThread((threadID: id, isPinned: newValue))
        } else {
            props.isPinned = newValue
        }
    }

    @MainActor
    public func setIsArchived(_ newValue: Bool) async throws {
        if newValue {
            let bufferMs = 300

            if let timestamp = timestamp {
                let ms = Int((timestamp.timeIntervalSince1970 * 1000).rounded()) + bufferMs
                props.isArchivedUpto = "ts" + String(ms)
            }
        } else {
            props.isArchivedUpto = nil
        }

        if Preferences.shared.nativeArchive,
           let supportsArchive = account?.platform?.attributes.contains(.supportsArchive),
           supportsArchive {
            try await accountInstance().api.archiveThread((threadID: id, isArchiving: newValue))
        }
    }

    @MainActor
    public func setIsMuted(_ newValue: Bool, nativeMute: Bool = false) async throws {
        if nativeMute {
            try await accountInstance().api.updateThread((
                threadID: id,
                updates: Schema.Thread.Partial(
                    id: id,
                    mutedUntil: newValue == true ? "forever" : nil,
                    title: title
                )
            ))
        }

        props.isMuted = newValue
    }

    @MainActor
    public func setThreadTitle(_ newValue: String) async throws {
        try await accountInstance().api.updateThread((
            threadID: id,
            updates: Schema.Thread.Partial(
                id: id,
                mutedUntil: mutedUntil,
                title: newValue
            )
        ))
    }
}

// MARK: - Events callbacks

extension ThreadStore {
    @MainActor
    public func onUserActivity(
        type: Schema.ActivityType,
        participantID: Schema.UserID,
        customLabel: String? = nil,
        durationMs: Double? = nil
    ) {
        let activity = UserActivity(type: type, participantID: participantID, customLabel: customLabel)

        // Add activity
        activities[participantID] = activity

        // Schedule removing it
        let duration = durationMs ?? 4_000
        DispatchQueue.main.asyncAfter(deadline: .now() + (duration / 1_000)) { [weak self] in
            guard let self = self else { return }
            if self.activities[participantID]?.id == activity.id {
                self.activities.removeValue(forKey: participantID)
            }
        }
    }

    @MainActor
    public func onUserActivityStopped(participantID: Schema.UserID) {
        activities[participantID] = nil
    }
}

// MARK: - Auxiliary Implementation -

extension Schema.MessageContent {
    fileprivate func asPendingAttachment() -> Schema.MessageAttachment? {
        guard filePath != nil || fileBuffer != nil else {
            return nil
        }
        let kind: Schema.MessageAttachment.Kind
        if mimeType?.hasPrefix("image/") == true {
            kind = .img
        } else if mimeType?.hasPrefix("video/") == true {
            kind = .video
        } else if mimeType?.hasPrefix("audio/") == true {
            kind = .audio
        } else {
            kind = .unknown
        }
        return Schema.MessageAttachment(
            id: UUID().uuidString,
            type: kind,
            isGif: isGif,
            data: fileBuffer,
            size: size,
            srcURL: filePath,
            fileName: fileName,
            mimeType: mimeType
        )
    }
}
