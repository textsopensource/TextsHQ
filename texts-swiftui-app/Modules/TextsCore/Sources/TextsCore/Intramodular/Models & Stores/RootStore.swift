//
// Copyright (c) Texts HQ
//

import Foundation
import Merge

public final class RootStore: ObservableObject {
    @PublishedObject public private(set) var platformsStore: PlatformsStore
    @PublishedObject public private(set) var accountsStore: AccountsStore

    init() {
        let platformsStore = PlatformsStore()

        self.platformsStore = platformsStore
        self.accountsStore = AccountsStore(platformsStore: platformsStore)
    }
}
