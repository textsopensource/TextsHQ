//
// Copyright (c) Texts HQ
//

import Foundation
import Merge
import os.log
import Swallow

public final class AccountInstance: ObservableObject, CustomStringConvertible, Identifiable, CancellablesHolder {
    public enum State {
        public enum _ComparisonType {
            case disabled
            case loading
            case disabling
            case offline
            case loaded

            init(from state: AccountInstance.State) {
                switch state {
                case .disabled:
                    self = .disabled
                case .loading:
                    self = .loaded
                case .disabling:
                    self = .disabled
                case .offline:
                    self = .offline
                case .loaded:
                    self = .loaded
                }
            }

            public static func == (lhs: AccountInstance.State, rhs: _ComparisonType) -> Bool {
                _ComparisonType(from: lhs) == rhs
            }
        }
        case disabled(Error?)
        case loading(AccountStore?)
        case disabling(Task<Void, Never>)
        case offline(AccountStore, Error?)
        // showSpinner is true iff the first getThreads hasn't finished yet
        case loaded(AccountStore, showSpinner: Bool)

        public var type: _ComparisonType {
            .init(from: self)
        }

        public var isActive: Bool {
            switch self {
            case .disabled(nil), .disabling:
                return false
            default:
                return true
            }
        }

        public var isOffline: Bool {
            switch self {
            case .offline:
                return true
            default:
                return false
            }
        }

        public var isLoadingThreads: Bool {
            switch self {
            case .disabling, .disabled, .offline:
                return false
            case .loading:
                return true
            case .loaded(_, let showSpinner):
                return showSpinner
            }
        }
    }

    private static let logger = Logger(category: "AccountInstance")

    public let accountsStore: AccountsStore

    public var id: Schema.AccountID
    public let platform: Schema.PlatformInfo

    @Published public private(set) var session: String?
    @Published public private(set) var user: Schema.User?
    @Published public private(set) var state: State = .disabled(nil)
    @Published public private(set) var lastLoginResult: Schema.LoginResult?

    @MainActor func setHasLoadedThreads() {
        if case .loaded(let account, true) = state {
            state = .loaded(account, showSpinner: false)
        }
    }

    @MainActor func setLoadingThreads() {
        if case .loaded(let account, false) = state {
            state = .loaded(account, showSpinner: true)
        }
    }

    /// Whether this account is currently ephemeral, i.e. the user hasn't
    /// finished logging in, or they just deleted the account.
    ///
    /// If false, the receiver may recover associated filesystem resources
    /// when the instance is deinitialized.
    public var isSaved: Bool

    public var description: String {
        "Account (id: \(id))"
    }

    // impl detail
    public struct Persisted: Codable, Identifiable {
        public var id: Schema.AccountID
        public var platformName: String
        public var user: Schema.User?
        public var isActive: Bool
    }
    // stream of the latest save-able info for this account
    // (session, persisted)
    var persisted: AnyPublisher<(String?, Persisted), Never> {
        $session
            .combineLatest(
                $user.combineLatest($state).map {
                    Persisted(
                        id: self.id,
                        platformName: self.platform.name,
                        user: $0,
                        isActive: $1.isActive
                    )
                }
            )
            .eraseToAnyPublisher()
    }

    init(
        accountsStore: AccountsStore,
        persisted: Persisted,
        session: String?,
        platform: Schema.PlatformInfo
    ) {
        self.accountsStore = accountsStore
        self.id = persisted.id
        self.platform = platform
        self.session = session
        self.user = persisted.user
        self.isSaved = true

        if persisted.isActive {
            #if INDEXDB_ENABLED
            state = .offline(AccountStore(accountInstance: self), nil)
            #else
            Task { @MainActor in
                try await initialize()
            }
            #endif
        }
    }

    // creates a new, unauthenticated, uninitialized account. Activate
    // via AccountsStore.addAccount
    public init(platform: Schema.PlatformInfo, id: ID? = nil) {
        self.accountsStore = AppState.shared.rootStore.accountsStore
        self.id = id ?? .init(rawValue: "\(platform.name)-\(UUID().uuidString.lowercased())")
        self.platform = platform
        self.session = nil
        self.user = nil
        self.isSaved = false
    }

    public var currentAccountStore: AccountStore? {
        switch state {
        case .loaded(let store, _), .offline(let store, _), .loading(let store?):
            return store
        default:
            return nil
        }
    }

    public var accountStore: AnyPublisher<AccountStore, Error> {
        $state
            .tryCompactMap {
                switch $0 {
                case .loaded(let store, _), .offline(let store, _), .loading(let store?):
                    return store
                case .disabled(nil), .loading, .disabling:
                    return nil
                case .disabled(let error?):
                    throw error
                }
            }
            .eraseToAnyPublisher()
    }

    public static func accountsDir() throws -> URL {
        let library = try FileManager.default.url(for: .applicationSupportDirectory, in: .userDomainMask)
        let accounts = library.appendingPathComponent("Texts").appendingPathComponent("Accounts")

        if !FileManager.default.fileExists(at: accounts) {
            try FileManager.default.createDirectory(at: accounts, withIntermediateDirectories: true)
        }

        return accounts
    }

    func dataDir() throws -> URL {
        try Self.accountsDir().appendingPathComponent(id.rawValue)
    }
}

extension AccountInstance {
    @discardableResult
    @MainActor
    public func initialize(withoutSession: Bool = false) async throws -> AccountStore {
        switch state {
        case .loading:
            return try await accountStore.first().value
        case .loaded(let store, _):
            return store
        case .disabled, .offline:
            break
        case .disabling(let task):
            await task.value

            return try await self.initialize(withoutSession: withoutSession)
        }

        #if INDEXDB_ENABLED
        state = .loading(currentAccountStore ?? nil)
        #else
        state = .loading(nil)
        #endif

        let dataDir: URL = try self.dataDir()

        do {
            try await api.initialize((
                session: withoutSession ? nil : session,
                accountInfo: .init(accountID: id, dataDirPath: dataDir.path),
                prefs: Preferences.shared.accountPreferences[id] ?? [:]
            ))
        } catch {
            if let serverError = error as? Schema.MethodError, serverError.name == "ReAuthError" {
                state = .disabled(ReauthError())
            } else {
                #if INDEXDB_ENABLED
                state = .offline(currentAccountStore ?? AccountStore(accountInstance: self), error)
                #else
                state = .disabled(error)
                #endif
            }
        }

        let account = currentAccountStore ?? AccountStore(accountInstance: self)

        if !withoutSession {
            state = .loaded(account, showSpinner: true)

            try await refreshUserAndSession()
            try account.setUpEventAndConnectionStateObservers()
            try await account.onActive()
        } else {
            try account.setUpEventAndConnectionStateObservers()
        }

        state = .loaded(account, showSpinner: withoutSession)

        return account
    }

    @MainActor
    public func login(
        withCredentials credentials: Schema.LoginCreds?
    ) async throws -> Schema.LoginResult {
        let result = try await api.login(credentials)

        lastLoginResult = result

        return result
    }

    // TODO: There's a potential optimization to be made here:
    // instead of disposing when requiresReauth is true, we can simply
    // use a new state and keep the initialized AccountStore around. We
    // can then make all future PAS calls throw, except for initialize
    // which no-ops and login which re-authenticates. Doing this would
    // complicate the logic though so it's not worth it atm.
    @MainActor
    public func dispose(requireReauth: Bool = false) async {
        let taskBox = ReferenceBox<Task<Void, Never>?>(nil)

        taskBox.wrappedValue = Task { @MainActor in
            switch state {
            case .disabled, .disabling:
                return
            case .loading:
                // Wait for instance to be loaded.
                await $state.first(where: { $0 == .loaded }).first().mapTo(()).value
            case .offline:
                state = .disabled(nil)
                return
            case .loaded:
                break
            }

            state = .disabling(taskBox.value.unsafelyUnwrapped)

            let result = await Result(catching: { try await api.dispose() })

            PlatformServer.shared.removeSubscriptions(forAccountID: id)

            if requireReauth {
                // requireReauth means the caller is explicitly requesting that we set the state as reauth-required
                state = .disabled(ReauthError())
            } else {
                switch result {
                case .failure(let error):
                    state = .disabled(error)
                case .success:
                    state = .disabled(requireReauth ? ReauthError() : nil)
                }
            }
        }

        await taskBox.wrappedValue?.value
    }

    public func cleanupAccount() throws {
        Self.logger.debug("Cleaning up unsaved account \(self.id)")
        try FileManager.default.removeItem(at: dataDir())
    }

    public func logoutAndDispose() async {
        do {
            try await api.logout()
            try await api.dispose()
        } catch {
            Self.logger.error(error, metadata: nil)
        }
    }
}

extension AccountInstance {
    public func refreshUser() async throws {
        try await callRefreshMethod(api.getCurrentUser, assigning: \.user)
    }

    public func refreshSession() async throws {
        try await callRefreshMethod(api.serializeSession, assigning: \.session)
    }

    public func refreshUserAndSession() async throws {
        try await refreshUser()
        try await refreshSession()
    }

    @MainActor
    private func callRefreshMethod<T: Decodable>(
        _ method: () -> AnySingleOutputPublisher<T, Error>,
        assigning keyPath: ReferenceWritableKeyPath<AccountInstance, T?>
    ) async throws {
        switch state {
        case .disabled(nil), .disabling, .offline(_, .none):
            return
        case .disabled(let error?), .offline(_, let error?):
            throw error
        case .loading:
            throw AccountError.uninitialized
        case .loaded:
            self[keyPath: keyPath] = try await method().value
        }
    }
}

// MARK: - Protocol Conformances -

extension AccountInstance: Hashable {
    public static func == (lhs: AccountInstance, rhs: AccountInstance) -> Bool {
        lhs.id == rhs.id
    }

    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

// MARK: - Auxiliary Implementation -

extension AccountInstance {
    public struct ReauthError: Error, CustomStringConvertible, LocalizedError {
        public var description: String {
            "Re-authentication required"
        }
        public var errorDescription: String? {
            description
        }
    }
}
