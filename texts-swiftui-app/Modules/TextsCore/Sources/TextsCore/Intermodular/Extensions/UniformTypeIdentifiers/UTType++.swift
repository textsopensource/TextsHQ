import UniformTypeIdentifiers

extension UTType {
    public init?(filenameExtensionEx ext: String) {
        switch ext {
        case "mp4v":
            self = .mpeg4Movie
        default:
            self.init(filenameExtension: ext)
        }
    }
}
