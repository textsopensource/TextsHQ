//
// Copyright (c) Texts HQ
//

import UIKit.UIFont

extension UIFont {
    public static func preferredFont(for style: TextStyle, weight: Weight) -> UIFont {
        let metrics = UIFontMetrics(forTextStyle: style)
        let desc = UIFontDescriptor.preferredFontDescriptor(withTextStyle: style)
        let font = UIFont.systemFont(ofSize: desc.pointSize, weight: weight)
        return metrics.scaledFont(for: font)
    }

    public func withTraits(traits: UIFontDescriptor.SymbolicTraits) -> UIFont {
        let descriptor = fontDescriptor.withSymbolicTraits(traits)
        return UIFont(descriptor: descriptor!, size: 0)
    }

    public func bold() -> UIFont {
        withTraits(traits: .traitBold)
    }

    public func italic() -> UIFont {
        withTraits(traits: .traitItalic)
    }
}
