//
// Copyright (c) Texts HQ
//

import Foundation
import UIKit

extension NSMutableAttributedString {
    struct Configuration {
        let tintColor: UIColor
        var senderId: Schema.UserID? = nil
    }

    func apply(textAttributes: Schema.TextAttributes?, configuration: Configuration) {
        let attributedString = self

        let entities = textAttributes?.entities

        let textFont = UIFont.preferredFont(for: .subheadline, weight: .medium)

        attributedString.addAttribute(.font, value: textFont, range: string.nsRangeBounds)

        attributedString.addAttribute(.foregroundColor, value: configuration.tintColor, range: string.nsRangeBounds)

        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.allTypes.rawValue)
        if let dataDetector = dataDetector {
            let matches = dataDetector.matches(in: string, options: [], range: NSRange(location: 0, length: string.utf16.count))

            for match in matches {
                guard let range = Range(match.range, in: string), let link = URL(string: String(string[range])) else {
                    continue
                }

                let nsRange = NSRange(range, in: string)
                let attributes: [NSAttributedString.Key: Any] = [
                    .link: link,
                    .underlineStyle: NSUnderlineStyle.single.rawValue
                ]
                attributedString.addAttributes(attributes, range: nsRange)
            }
        }

        // Start from the biggest offset, so we don't have to account for the removed offset.
        let sorted = entities?.sorted(by: { ($0.from, $0.to) > ($1.from, $1.to) }) ?? []
        for entity in sorted {
            attributedString.apply(entity: entity, configuration: configuration)
        }
    }

    func apply(entity: Schema.TextEntity, message: Schema.Message) {
        let textColor = message.isSender == true ? UIColor.white : UIColor.label
        let senderId = message.senderID
        let config = Configuration(tintColor: textColor, senderId: senderId)
        apply(entity: entity, configuration: config)
    }

    // swiftlint:disable:next cyclomatic_complexity
    func apply(entity: Schema.TextEntity, configuration: Configuration) {
        guard entity.to <= string.unicodeScalars.count && entity.from <= entity.to else {
            print(#file, "Skipping entity parsing because text range is invalid.\nEntity: \(entity)\nConfiguration: \(configuration)")
            return
        }

        let fromIdx = string.unicodeScalars.index(string.unicodeScalars.startIndex, offsetBy: entity.from)
        let toIdx = string.unicodeScalars.index(string.unicodeScalars.startIndex, offsetBy: entity.to)

        let textFont = UIFont.preferredFont(for: .subheadline, weight: .medium)

        var attributes: [NSAttributedString.Key: Any] = [
            .font: textFont
        ]

        if entity.bold ?? false {
            attributes[.font] = textFont.bold
        }

        if entity.code ?? false {
            attributes[.font] = UIFont.monospacedSystemFont(ofSize: textFont.pointSize, weight: .medium)
        }

        if entity.italic ?? false {
            attributes[.font] = textFont.italic
        }

        if entity.pre ?? false {
            attributes[.font] = textFont.monospaced
        }

        if entity.strikethrough ?? false {
            attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
        }

        if entity.underline ?? false {
            attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        }

        if let linkString = entity.link {
            attributes[.link] = URL(string: linkString)
        }

        if let mentionedUser = entity.mentionedUser {
            if mentionedUser.id == configuration.senderId {
                attributes[.font] = textFont.bold
            }

            if let id = mentionedUser.id, let url = URL(string: "texts://thread/\(id.rawValue)") {
                attributes[.link] = url
            }
        }

        let nsRange = NSRange(fromIdx ..< toIdx, in: string)
        addAttributes(attributes, range: nsRange)

        if let replaceWith = entity.replaceWith {
            replaceCharacters(in: nsRange, with: replaceWith)
        }

        if let replaceWithMedia = entity.replaceWithMedia, let url = URL(string: replaceWithMedia.srcURL) {
            let imageAttachment = AsyncTextAttachment(imageURL: url, tintColor: configuration.tintColor)
            imageAttachment.adjustsImageSizeForAccessibilityContentSizeCategory = true

            let imageSize: CGFloat = (entity.from == 0 && entity.to == string.count) ? 32 : textFont.pointSize
            imageAttachment.displaySize = CGSize(width: imageSize, height: imageSize)
            imageAttachment.bounds = CGRect(x: 0, y: 0, width: imageSize, height: imageSize)

            let imageAttachmentString = NSAttributedString(attachment: imageAttachment)
            replaceCharacters(in: nsRange, with: imageAttachmentString)
        }
    }
}
