import Foundation

// wraps a value with a unique UUID
public struct Identified<T>: Identifiable {
    public let id = UUID()
    public let value: T
    public init(_ value: T) {
        self.value = value
    }
}
