//
// Copyright (c) Texts HQ
//

import Foundation

extension RelativeDateTimeFormatter {
    public static var presenceFormatter: RelativeDateTimeFormatter = {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .abbreviated
        return formatter
    }()
}
