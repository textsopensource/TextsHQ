//
// Copyright (c) Texts HQ
//

import FoundationX

private let entityDecodeMap: [String: String] = [
    "gt": ">", "lt": "<", "amp": "&", "quot": "\""
]

extension NSMutableAttributedString {
    func htmlUnescape() {
        do {
            let regex = try NSRegularExpression(
                pattern: "(&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});?)",
                options: [.caseInsensitive]
            )

            var start = 0

            while let match = regex.firstMatch(
                in: string,
                options: [],
                range: NSRange(location: start, length: string.length - start)
            ) {
                let entity = string
                    .substring(withRange: match.range(at: 2))
                    .stringValue
                    .lowercased()

                if let decoded = entityDecodeMap[entity] {
                    replaceCharacters(in: match.range, with: decoded)
                } else {
                    start = match.range.upperBound
                }
            }
        } catch {
            assertionFailure()
        }
    }
}
