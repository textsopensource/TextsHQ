//
// Copyright (c) Texts HQ
//

import Foundation

extension Bundle {
    public var buildNumber: String {
        Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "Unknown"
    }
}
