//
// Copyright (c) Texts HQ
//

import Foundation
import Crypto

extension String {
    public func md5() throws -> String {
        let data = try data()

        let hash = Insecure.MD5.hash(data: data)

        return hash.map { String(format: "%02hhx", $0) }.joined()
    }
}
