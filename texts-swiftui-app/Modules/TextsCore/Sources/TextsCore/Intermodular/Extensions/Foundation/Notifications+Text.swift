//
// Copyright (c) Texts HQ
//

import Foundation

extension Notification {
    public static let refreshInbox = Notification(name: .init(rawValue: "com.texts.refresh-inbox"))
}
