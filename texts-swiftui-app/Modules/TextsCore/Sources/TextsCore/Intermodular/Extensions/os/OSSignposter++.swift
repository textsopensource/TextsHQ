//
// Copyright (c) Texts HQ
//

import Foundation
import os.log

#if false
#warning("Signposting is enabled. Ensure that you disable it before committing.")

public protocol Signposter {
    // we can't use OSLogMessage/SignpostMetadata because they're hardcoded
    // into the compiler as os-only :/

    func withIntervalSignpost<T>(
        _ name: StaticString,
        id: OSSignpostID,
        _ message: String,
        around task: () throws -> T
    ) rethrows -> T

    func withIntervalSignpost<T>(
        _ name: StaticString,
        id: OSSignpostID,
        around task: () throws -> T
    ) rethrows -> T

    func emitEvent(_ name: StaticString, id: OSSignpostID)

    func emitEvent(_ name: StaticString, id: OSSignpostID, _ message: String)

    func beginInterval(_ name: StaticString, id: OSSignpostID) -> SignpostIntervalState

    func beginInterval(_ name: StaticString, id: OSSignpostID, _ message: String) -> SignpostIntervalState

    func endInterval(_ name: StaticString, _ state: SignpostIntervalState)
}

public protocol SignpostIntervalState: AnyObject {}

public class StubSignposterState: SignpostIntervalState {
    @usableFromInline
    init() {}
}

@available(iOS 15, *)
extension OSSignpostIntervalState: SignpostIntervalState {}

@available(iOS 15, *)
extension OSSignposter: Signposter {
    public func withIntervalSignpost<T>(
        _ name: StaticString,
        id: OSSignpostID,
        _ message: String,
        around task: () throws -> T
    ) rethrows -> T {
        try self.withIntervalSignpost(name, id: id, "\(message)" as SignpostMetadata, around: task)
    }

    public func emitEvent(_ name: StaticString, id: OSSignpostID, _ message: String) {
        emitEvent(name, id: id, "\(message)" as SignpostMetadata)
    }

    public func beginInterval(_ name: StaticString, id: OSSignpostID, _ message: String) -> SignpostIntervalState {
        beginInterval(name, id: id, "\(message)" as SignpostMetadata)
    }

    public func beginInterval(_ name: StaticString, id: OSSignpostID) -> SignpostIntervalState {
        beginInterval(name, id: id) as OSSignpostIntervalState
    }

    public func endInterval(_ name: StaticString, _ state: SignpostIntervalState) {
        // swiftlint:disable:next force_cast
        endInterval(name, state as! OSSignpostIntervalState)
    }
}

public struct StubSignposter: Signposter {}

@available(iOS 15, *)
public typealias DefaultSignposter = OSSignposter

extension StubSignposter {
    public func emitEvent(_ name: StaticString, id: OSSignpostID, _ message: String) {}

    public func beginInterval(_ name: StaticString, id: OSSignpostID, _ message: String) -> SignpostIntervalState {
        StubSignposterState()
    }
}
#else
public struct StubSignposter {
    fileprivate init() {}
    fileprivate init(subsystem: String, category: String) {}
    fileprivate init(logger: Logger) {}

    @inlinable
    public func emitEvent(
        _ name: StaticString,
        id: OSSignpostID,
        _ message: @autoclosure () -> String
    ) {}

    @inlinable
    public func beginInterval(
        _ name: StaticString,
        id: OSSignpostID,
        _ message: @autoclosure () -> String
    ) -> SignpostIntervalState {
        StubSignposterState()
    }
}
public typealias Signposter = StubSignposter
public typealias DefaultSignposter = StubSignposter
public struct StubSignposterState {
    @usableFromInline
    init() {}
}
public typealias SignpostIntervalState = StubSignposterState
#endif

extension Signposter {
    @inlinable
    public func withIntervalSignpost<T>(
        _ name: StaticString,
        _ message: String,
        around task: () throws -> T
    ) rethrows -> T {
        try withIntervalSignpost(name, id: .exclusive, message, around: task)
    }

    @inlinable
    public func withIntervalSignpost<T>(_ name: StaticString, around task: () throws -> T) rethrows -> T {
        try withIntervalSignpost(name, id: .exclusive, around: task)
    }

    @inlinable
    public func emitEvent(_ name: StaticString) {
        emitEvent(name, id: .exclusive)
    }

    @inlinable
    public func emitEvent(_ name: StaticString, _ message: String) {
        emitEvent(name, id: .exclusive, message)
    }

    @inlinable
    public func beginInterval(_ name: StaticString, _ message: String) -> SignpostIntervalState {
        beginInterval(name, id: .exclusive, message)
    }

    @inlinable
    public func beginInterval(_ name: StaticString) -> SignpostIntervalState {
        beginInterval(name, id: .exclusive)
    }
}

extension StubSignposter {
    @inlinable
    public func withIntervalSignpost<T>(
        _ name: StaticString,
        id: OSSignpostID,
        around task: () throws -> T
    ) rethrows -> T {
        try task()
    }

    @inlinable
    public func withIntervalSignpost<T>(
        _ name: StaticString,
        id: OSSignpostID,
        _ message: String,
        around task: () throws -> T
    ) rethrows -> T {
        try task()
    }

    @inlinable
    public func emitEvent(_ name: StaticString, id: OSSignpostID) {}

    @inlinable
    public func beginInterval(_ name: StaticString, id: OSSignpostID) -> SignpostIntervalState {
        StubSignposterState()
    }

    @inlinable
    public func endInterval(_ name: StaticString, _ state: SignpostIntervalState) {}
}

public enum Signposters {
    static func create(category: String) -> Signposter {
        if #available(iOS 15, *) {
            return DefaultSignposter(subsystem: Bundle.main.bundleIdentifier!, category: category)
        }
        return StubSignposter()
    }

    static func create(logger: Logger) -> Signposter {
        if #available(iOS 15, *) {
            return DefaultSignposter(logger: logger)
        }
        return StubSignposter()
    }
}
