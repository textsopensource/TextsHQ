//
// Copyright (c) Texts HQ
//

import Foundation
import Combine
import os.log

@usableFromInline
let _combineSignposter = Signposters.create(category: "CombinePublishers")

extension Publisher {

    @inlinable
    public func signpost(_ name: StaticString, id: OSSignpostID = .exclusive, _ message: @escaping @autoclosure () -> String) -> Publishers.HandleEvents<Self> {
        let signposter = _combineSignposter
        var state: SignpostIntervalState?
        lazy var lazyMessage = message()
        return handleEvents { subscription in
            guard state == nil else { return }
            state = signposter.beginInterval(name, id: id, lazyMessage)
        } receiveOutput: { output in
//            signposter.emitEvent(name, lazyMessage)
        } receiveCompletion: { completion in
            state.map { signposter.endInterval(name, $0) }
        } receiveCancel: {
            state.map { signposter.endInterval(name, $0) }
        } receiveRequest: { demand in

        }
    }

    @inlinable
    public func signpost(_ name: StaticString, id: OSSignpostID = .exclusive) -> Publishers.HandleEvents<Self> {
        let signposter = _combineSignposter
        let state = signposter.beginInterval(name, id: id)
        return handleCancelOrCompletion { _ in
            signposter.endInterval(name, state)
        }
    }

}
