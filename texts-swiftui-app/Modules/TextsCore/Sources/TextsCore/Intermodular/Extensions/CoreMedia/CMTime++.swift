//
// Copyright (c) Texts HQ
//

import AVKit

extension CMTime {
    public var roundedSeconds: TimeInterval {
        seconds.rounded()
    }

    public var hours: Int { Int(roundedSeconds / 3600) }
    public var minute: Int { Int(roundedSeconds.truncatingRemainder(dividingBy: 3600) / 60) }
    public var second: Int { Int(roundedSeconds.truncatingRemainder(dividingBy: 60)) }
    public var positionalTime: String {
        hours > 0 ?
            String(format: "%d:%02d:%02d", hours, minute, second) :
            String(format: "%02d:%02d", minute, second)
    }
}
