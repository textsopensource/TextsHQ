import Foundation
import Swallow
import TextsBase
import UIKit

extension Schema.Message {
    public var displayAttributedString: NSAttributedString? {
        memoize(uniquingWith: self.id) {
            guard let string = text else { return nil }

            let attributedString = NSMutableAttributedString(string: string)
            let textColor = isSender == true ? UIColor.white : UIColor.label
            let config = NSMutableAttributedString.Configuration(tintColor: textColor, senderId: senderID)

            attributedString.apply(textAttributes: self.textAttributes, configuration: config)

            if let htmlEntityDecode = textAttributes?.heDecode, htmlEntityDecode {
                attributedString.htmlUnescape()
            }

            return attributedString
                .trimmingCharacters(in: .whitespacesAndNewlines)
                .copy() as? NSAttributedString
        }
    }

    public func displayString(in thread: ThreadStore) -> String? {
        // remove object replacement characters added by attributed string
        var str = displayAttributedString?.string
            .replacingOccurrences(of: "\u{fffc}", with: "")
            .trimmingCharacters(in: .whitespacesAndNewlines)

        if isAction == true {
            for participant in thread.participants {
                str = str?.replacingOccurrences(of: "{{\(participant.id)}}", with: participant.displayName)
            }

            if let senderID = senderID, let sender = thread.getParticipant(id: senderID) {
                str = str?.replacingOccurrences(of: "{{sender}}", with: sender.displayName)
            }
        }

        return str
    }
}

private var displayText_objcAssociationKey: UInt = 0

extension Schema.Tweet {
    public var displayText: NSAttributedString {
        if let obj = objc_getAssociatedObject(self, &displayText_objcAssociationKey) as? NSAttributedString {
            return obj
        }

        let str = NSMutableAttributedString(string: text)

        str.apply(textAttributes: textAttributes, configuration: .init(tintColor: .label))

        // swiftlint:disable:next force_cast
        let immutable = str.copy() as! NSAttributedString

        objc_setAssociatedObject(self, &displayText_objcAssociationKey, immutable, .OBJC_ASSOCIATION_RETAIN)

        return immutable
    }
}
