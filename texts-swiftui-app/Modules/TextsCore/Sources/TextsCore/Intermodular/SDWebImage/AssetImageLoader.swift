import Foundation
import Merge
import SDWebImageSwiftUI

public enum AssetLoadingError: Error {
    case decodingFailed
    case invalidURL
    case unknown
}

private class CombineWebImageOperation: NSObject, SDWebImageOperation {
    public let cancellable: AnyCancellable
    public init(cancellable: AnyCancellable) {
        self.cancellable = cancellable
    }
    public func cancel() {
        cancellable.cancel()
    }
}

/// Loads PAS asset:// URLs
///
/// asset:// URLs are effectively a "token" returned by a platform that
/// can later be passed via getAsset to actually load the asset. This
/// defers the loading of assets until they're needed.
public class AssetImageLoader: NSObject, SDImageLoader {
    public let server: PlatformServer
    public init(server: PlatformServer) {
        self.server = server
    }

    public func canRequestImage(for url: URL?) -> Bool {
        url?.scheme == "asset"
    }

    public func requestImage(
        with url: URL?,
        options: SDWebImageOptions = [],
        context: [SDWebImageContextOption: Any]?,
        progress progressBlock: SDImageLoaderProgressBlock?,
        completed completedBlock: SDImageLoaderCompletedBlock? = nil
    ) -> SDWebImageOperation? {
        guard let url = url else { return nil }

        let cancellable = Task { @MainActor in
            try await server.fetchAsset(for: url)
        }
        .publisher()
        .receive(on: DispatchQueue.global(qos: .default))
        .flatMap { asset -> AnySingleOutputPublisher<(UIImage, Data?, Bool), Error> in
            switch asset {
            case .data(let data):
                guard let decoded = SDImageLoaderDecodeImageData(data, url, options, context) else {
                    return .failure(AssetLoadingError.decodingFailed)
                }
                return .just((decoded, data, true))
            case .url(let urlString):
                guard let url = URL(string: urlString) else {
                    return .failure(AssetLoadingError.invalidURL)
                }
                return Future { promise in
                    SDWebImageDownloader.shared.requestImage(
                        with: url, options: options, context: context, progress: progressBlock
                    ) { image, data, error, finished in
                        if let image = image {
                            promise(.success((image, data, finished)))
                        } else if let error = error {
                            promise(.failure(error))
                        } else {
                            promise(.failure(AssetLoadingError.unknown))
                        }
                    }
                }.eraseToAnySingleOutputPublisher()
            }
        }
        .sinkResult { result in
            switch result {
            case let .success((image, data, finished)):
                completedBlock?(image, data, nil, finished)
            case let .failure(error):
                completedBlock?(nil, nil, error, true)
            }
        }

        return CombineWebImageOperation(cancellable: cancellable)
    }

    public func shouldBlockFailedURL(with url: URL, error: Error) -> Bool {
        true
    }
}
