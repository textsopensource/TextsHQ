//
// Copyright (c) Texts HQ
//

import Foundation
import OrderedCollections
import Swallow

@propertyWrapper
public struct OrderedDictionaryDecoding<Value: Codable>: Codable {
    public let wrappedValue: OrderedDictionary<String, Value>

    @inlinable
    public init(from decoder: Decoder) throws {
        if let wrappedValue = try? OrderedDictionary<String, Value>(from: decoder) {
            self.wrappedValue = wrappedValue
        } else {
            let container = try decoder.container(keyedBy: AnyStringKey.self)

            var orderedDictionary = OrderedDictionary<String, Value>(minimumCapacity: container.allKeys.count)

            for key in container.allKeys {
                orderedDictionary[key.stringValue] = try container.decode(Value.self, forKey: key)
            }

            self.wrappedValue = orderedDictionary
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.unkeyedContainer()

        for (key, value) in wrappedValue {
            try container.encode(key)
            try container.encode(value)
        }
    }
}
