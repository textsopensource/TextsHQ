//
// Copyright (c) Texts HQ
//

import Swallow

public protocol PASNumberConvertible: Hashable {
    init(pasDecoder: Decoder) throws
    func pasEncode(to encoder: Encoder) throws
}

extension Double: PASNumberConvertible {
    public init(pasDecoder: Decoder) throws {
        let container = try pasDecoder.singleValueContainer()
        self = try (try? container.decode(Double.self)) ?? Double(container.decode(Int.self))
    }

    public func pasEncode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self)
    }
}

extension Optional: PASNumberConvertible where Wrapped == Double {
    public init(pasDecoder: Decoder) throws {
        let container = try pasDecoder.singleValueContainer()
        self = (try? container.decode(Double.self)) ?? (try? Double(container.decode(Int.self)))
    }

    public func pasEncode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(self)
    }
}

@propertyWrapper
public struct PASNumber<T: PASNumberConvertible>: Codable, Hashable {
    public var wrappedValue: T
    public init(wrappedValue: T) {
        self.wrappedValue = wrappedValue
    }

    public init(from decoder: Decoder) throws {
        wrappedValue = try .init(pasDecoder: decoder)
    }

    public func encode(to encoder: Encoder) throws {
        try wrappedValue.pasEncode(to: encoder)
    }
}

extension KeyedDecodingContainer {
    public func decode(_ type: PASNumber<Double?>.Type, forKey key: Key) throws -> PASNumber<Double?> {
        try decodeIfPresent(PASNumber<Double?>.self, forKey: key) ?? .init(wrappedValue: nil)
    }
}

// PASMaybe is necessary because Swift doesn't specialize the maybe-optional fields on
// our partial types: since the types are passed as generic params, the synthesized
// decodable impl goes through the generic container.decode(Decodable.self, ...).
// By annotating as @PASMaybe, the synthesized impl goes through decode(PASMaybe.self, ...)
// instead, allowing us to dynamically check if the type is optional in our decode impl.

@propertyWrapper
public struct PASMaybe<T: Codable & Hashable>: Codable, Hashable {
    public var wrappedValue: T

    public init(wrappedValue: T) {
        self.wrappedValue = wrappedValue
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        wrappedValue = try container.decode(T.self)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encode(wrappedValue)
    }
}

extension KeyedDecodingContainer {
    public func decode<T>(_ type: PASMaybe<T>.Type, forKey key: Key) throws -> PASMaybe<T> {
        if let type = T.self as? _opaque_Optional.Type {
            // swiftlint:disable:next force_cast
            return try .init(wrappedValue: decodeIfPresent(T.self, forKey: key) ?? (type.init(none: ()) as! T))
        } else {
            return try .init(wrappedValue: decode(T.self, forKey: key))
        }
    }
}
