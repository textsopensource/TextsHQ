//
// Copyright (c) Texts HQ
//

import Foundation

extension Schema {
    // TODO: Turn Action into an enum with associated values
    // to better represent the sum type structure used in ts

    public enum ActionType: String, Codable, Hashable {
        case groupThreadCreated = "group_thread_created"
        case messageRequestAccepted = "message_request_accepted"
        case threadParticipantsAdded = "thread_participants_added"
        case threadParticipantsRemoved = "thread_participants_removed"
        case threadTitleUpdated = "thread_title_updated"
        case threadImageChanged = "thread_img_changed"
        case messageReactionCreated = "message_reaction_created"
        case messageReactionDeleted = "message_reaction_deleted"
    }

    public struct Action: Codable, Hashable {
        public let type: ActionType
        public let title: String?
        public let actorParticipantID: String?
        public let participantIDs: [String]?
    }
}
