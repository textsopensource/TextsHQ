//
// Copyright (c) Texts HQ
//

import Foundation
import Swallow

extension Schema {
    public enum InboxName: String, Codable, Hashable {
        case normal
        case requests
    }

    public enum ThreadType: String, Codable, Hashable {
        case single
        case group
        case channel
        case broadcast
    }

    public struct ThreadProps: Codable {
        public var customTitle: String?
        public var draftText: String?
        public var isArchivedUpto: String?
        public var isHidden: Bool?
        public var isMuted: Bool?
        public var isPinned: Bool?
        public var isUnread: Bool?
        public var note: String?
        public var labels: [String]?

        public init() {}
    }

    public typealias ThreadID = SchemaID<Thread, String>

    // NOTE: If you update this schema, make sure that you also update
    // ThreadStore.init and ThreadStore.apply(partial:)
    public struct _Thread<
        MaybeType: Codable & Hashable,
        MaybeBool: Codable & Hashable,
        MaybeMessages: Codable & Hashable,
        MaybeParticipants: Codable & Hashable
    >: Codable, Hashable, Identifiable {
        public var folderName: String?

        public let id: ThreadID
        public var title: String?
        @PASMaybe public var isUnread: MaybeBool
        public var lastReadMessageID: String?
        public var isReadOnly: Bool?
        public var isArchived: Bool?
        public var isPinned: Bool?
        public var mutedUntil: String?

        @PASMaybe public var type: MaybeType
        public var timestamp: Date?

        public var imgURL: String?
        public var createdAt: Date?
        public var description: String?

        public var lastMessageSnippet: String?
        public var messageExpirySeconds: Int?

        @PASMaybe public var messages: MaybeMessages
        @PASMaybe public var participants: MaybeParticipants

        // needed for muteThread
        public init(
            id: Schema.ThreadID,
            mutedUntil: String?,
            title: String?
        ) where MaybeType == Schema.ThreadType?, MaybeBool == Bool?, MaybeMessages == Schema.Paginated<Schema.Message>?, MaybeParticipants == Schema.Paginated<Schema.User>? {
            self.id = id
            self.mutedUntil = mutedUntil
            self.title = title
            self.isUnread = nil
            self.type = nil
            self.messages = nil
            self.participants = nil
        }
    }

    public typealias Thread = _Thread<ThreadType, Bool, Paginated<Message>, Paginated<User>>
}

extension Schema.Thread: PartializableModel {
    public typealias Partial = Schema._Thread<
        Schema.ThreadType?,
        Bool?,
        Schema.Paginated<Schema.Message>?,
        Schema.Paginated<Schema.User>?
    >
}
