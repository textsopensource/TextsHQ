//
// Copyright (c) Texts HQ
//

import Swallow

public protocol PartializableModel: Identifiable {
    associatedtype Partial: Identifiable where Partial.ID == ID
}

extension Schema {
    public enum ActivityType: String, Codable, Hashable {
        case none
        case online
        case offline
        case typing
        case custom
        case recordingVoice = "recording_voice"
        case recordingVideo = "recording_video"

        public var localizedName: String {
            switch self {
            case .recordingVideo:
                return "recording a video message"
            case .recordingVoice:
                return "recording a voice message"
            case .typing:
                return "typing"
            case .online:
                return "online"
            case .offline:
                return "offline"
            case .none, .custom:
                return ""
            }
        }
    }

    public struct ToastEvent: Codable, Hashable {
        public struct Toast: Codable, Hashable {
            public var text: String
        }

        public let toast: Toast
    }

    public struct UserPresenceEvent: Codable, Hashable {
        public let presence: UserPresence
    }

    public typealias PresenceMap = [Schema.UserID: Schema.UserPresence]

    public struct UserActivityEvent: Codable, Hashable {
        public let activityType: ActivityType
        public let threadID: ThreadID
        public let participantID: UserID
        @PASNumber public private(set) var durationMs: Double?
        public let customLabel: String?
    }

    public struct ThreadTrustedEvent: Codable, Hashable {
        public let threadID: ThreadID
    }

    public struct ThreadMessagesRefreshEvent: Codable, Hashable {
        public let threadID: ThreadID
    }

    public struct OpenWindowEvent: Codable, Hashable {
        public let url: String?
        public let windowTitle: String?
        public let windowWidth: String?
        public let windowHeight: String?
        public let cookieJar: Schema.CookieJar
    }

    public enum StateSyncEvent: Decodable, Hashable {
        public enum Mutation<Model: PartializableModel>: Decodable, Hashable
        where Model: Decodable & Hashable, Model.ID: Decodable, Model.Partial: Decodable & Hashable {
            case upsert([Model])
            case update([Model.Partial])
            case delete([Model.ID])
            case deleteAll

            private enum CodingKeys: String, CodingKey {
                case entries
                case mutationType
            }

            public init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)
                let mutationType = try container.decode(String.self, forKey: .mutationType)
                switch mutationType {
                case "upsert":
                    self = try .upsert(container.decode([Model].self, forKey: .entries))
                case "update":
                    self = try .update(container.decode([Model.Partial].self, forKey: .entries))
                case "delete":
                    self = try .delete(container.decode([Model.ID].self, forKey: .entries))
                case "delete-all":
                    self = .deleteAll
                default:
                    throw DecodingError.dataCorruptedError(
                        forKey: .mutationType,
                        in: container,
                        debugDescription: "Received unknown mutation type: \(mutationType)"
                    )
                }
            }
        }

        private struct ObjectIDs: Codable {
            public let threadID: ThreadID?
            public let messageID: MessageID?
        }

        case threads(Mutation<Thread>)
        case customEmojis(Mutation<CustomEmoji>)
        case participants(Mutation<User>, threadID: ThreadID?)
        case messages(Mutation<Message>, threadID: ThreadID)
        case messageReactions(Mutation<MessageReaction>, threadID: ThreadID, messageID: MessageID)

        private enum CodingKeys: String, CodingKey {
            case objectName
            case objectIDs
        }

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let objectName = try container.decode(String.self, forKey: .objectName)
            let ids = try container.decode(ObjectIDs?.self, forKey: .objectIDs)
            let threadID = ids?.threadID
            let messageID = ids?.messageID
            let single = try decoder.singleValueContainer()
            switch objectName {
            case "thread":
                self = try .threads(single.decode(Mutation<Thread>.self))
            case "custom_emoji":
                self = try .customEmojis(single.decode(Mutation<CustomEmoji>.self))
            case "participant":
                self = try .participants(single.decode(Mutation<User>.self), threadID: threadID)
            case "message":
                guard let threadID = threadID else {
                    throw DecodingError.dataCorruptedError(
                        forKey: .objectIDs,
                        in: container,
                        debugDescription: "Expected threadID in objectIDs"
                    )
                }
                self = try .messages(single.decode(Mutation<Message>.self), threadID: threadID)
            case "message_reaction":
                guard let threadID = threadID, let messageID = messageID else {
                    let missing = [
                        threadID == nil ? "threadID" : nil,
                        messageID == nil ? "messageID" : nil
                    ].compactMap { $0 }.joined(separator: " and ")
                    throw DecodingError.dataCorruptedError(
                        forKey: .objectIDs,
                        in: container,
                        debugDescription: "Missing \(missing) from objectIDs"
                    )
                }
                self = try .messageReactions(single.decode(Mutation<MessageReaction>.self), threadID: threadID, messageID: messageID)
            default:
                throw DecodingError.dataCorruptedError(
                    forKey: .objectName,
                    in: container,
                    debugDescription: "Received unknown mutation object name: \(objectName)"
                )
            }
        }
    }

    public enum ServerEvent: Decodable, Hashable {
        case stateSync(StateSyncEvent)
        case toast(ToastEvent)
        case threadMessagesRefresh(ThreadMessagesRefreshEvent)
        case threadTrusted(ThreadTrustedEvent)
        case userActivity(UserActivityEvent)
        case userPresenceUpdated(UserPresenceEvent)
        case openWindow(OpenWindowEvent)
        case sessionUpdated

        private enum CodingKeys: CodingKey {
            case type
        }

        public var threadID: Schema.ThreadID? {
            switch self {
            case let .threadMessagesRefresh(data):
                return data.threadID
            case let .threadTrusted(data):
                return data.threadID
            case let .userActivity(data):
                return data.threadID
            case let .stateSync(event):
                switch event {
                case let .messageReactions(_, threadID, _):
                    return threadID
                case let .messages(_, threadID):
                    return threadID
                case let .participants(_, threadID):
                    return threadID
                default:
                    return nil
                }
            default:
                return nil
            }
        }

        public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let type = try values.decode(String.self, forKey: .type)
            switch type {
            case "state_sync":
                self = try .stateSync(decoder.decode(single: StateSyncEvent.self))
            case "toast":
                self = try .toast(decoder.decode(single: ToastEvent.self))
            case "thread_messages_refresh":
                self = try .threadMessagesRefresh(decoder.decode(single: ThreadMessagesRefreshEvent.self))
            case "thread_trusted":
                self = try .threadTrusted(decoder.decode(single: ThreadTrustedEvent.self))
            case "user_activity":
                self = try .userActivity(decoder.decode(single: UserActivityEvent.self))
            case "user_presence_updated":
                self = try .userPresenceUpdated(decoder.decode(single: UserPresenceEvent.self))
            case "open_window":
                self = try .openWindow(decoder.decode(single: OpenWindowEvent.self))
            case "session_updated":
                self = .sessionUpdated
            default:
                throw DecodingError.dataCorruptedError(
                    forKey: .type,
                    in: values,
                    debugDescription: "Received unknown server event: \(type)"
                )
            }
        }
    }
}
