//
// Copyright (c) Texts HQ
//

import IdentifiedCollections

extension Schema {
    public struct Paginated<Element: Identifiable> {
        public var hasMore: Bool
        public var items: IdentifiedArrayOf<Element>
        public var oldestCursor: String?
        public var newestCursor: String?
    }

    public struct PaginationArguments: Codable, Hashable {
        public enum Direction: String, Codable, Hashable {
            case after
            case before
        }

        public let cursor: String?
        public let direction: Direction

        public init(cursor: String?, direction: Direction) {
            self.cursor = cursor
            self.direction = direction
        }
    }
}

extension Schema.Paginated {
    public init() {
        hasMore = true
        items = .init()
    }
}

extension Schema.Paginated: Encodable where Element: Encodable {}

extension Schema.Paginated: Decodable where Element: Decodable {}

extension Schema.Paginated: Equatable where Element: Equatable {}

extension Schema.Paginated: Hashable where Element: Hashable {}

extension Schema.Paginated: RandomAccessCollection {
    public typealias Iterator = IdentifiedArrayOf<Element>.Iterator

    public var count: Int {
        items.count
    }

    public var startIndex: Int {
        items.startIndex
    }

    public var endIndex: Int {
        items.endIndex
    }

    public subscript(_ index: Int) -> Element {
        items[index]
    }

    public func makeIterator() -> IdentifiedArrayOf<Element>.Iterator {
        items.makeIterator()
    }
}

extension Schema.Paginated {
    public mutating func merge<C: Collection>(
        items: C,
        direction: Schema.PaginationArguments.Direction
    ) where C.Element == Element {
        switch direction {
        case .before:
            items.reversed().forEach { self.items.updateOrInsert($0, at: 0) }
        case .after:
            items.forEach { self.items.updateOrAppend($0) }
        }
    }

    public mutating func merge(_ newValue: Self, direction: Schema.PaginationArguments.Direction) {
        merge(items: newValue.items, direction: direction)
        hasMore = newValue.hasMore
        oldestCursor = newValue.oldestCursor
        newestCursor = newValue.newestCursor
    }

    // transform must return elements with unique IDs
    public func uniqueMapElements<T>(_ transform: (Element) throws -> T) rethrows -> Schema.Paginated<T> {
        try .init(
            hasMore: hasMore,
            items: .init(uniqueElements: items.map(transform)),
            oldestCursor: oldestCursor,
            newestCursor: newestCursor
        )
    }
}

extension Schema.PaginationArguments {
    public init<T>(_ direction: Direction, from paginated: Schema.Paginated<T>) {
        self.direction = direction
        switch direction {
        case .before:
            cursor = paginated.oldestCursor ?? paginated.newestCursor
        case .after:
            cursor = paginated.newestCursor ?? paginated.oldestCursor
        }
    }
}
