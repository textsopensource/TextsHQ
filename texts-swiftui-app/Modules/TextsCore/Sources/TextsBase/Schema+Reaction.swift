//
// Copyright (c) Texts HQ
//

import Swallow

extension Schema {
    public typealias MessageReactionID = SchemaID<MessageReaction, String>

    public struct _MessageReaction<MaybeString: Codable & Hashable, MaybeUserID: Codable & Hashable>: Codable, Hashable, Identifiable {
        public let id: MessageReactionID
        @PASMaybe public var reactionKey: MaybeString
        @PASMaybe public var participantID: MaybeUserID
        public var emoji: Bool?
    }

    public typealias MessageReaction = _MessageReaction<String, UserID>
}

extension Schema.MessageReaction: PartializableModel {
    public typealias Partial = Schema._MessageReaction<String?, Schema.UserID?>

    public mutating func coalesceInPlace(with partial: Partial) {
        func coalesce<T>(_ dest: WritableKeyPath<Self, T>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }
        func coalesce<T>(_ dest: WritableKeyPath<Self, T?>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }
        coalesce(\.reactionKey, \.reactionKey)
        coalesce(\.participantID, \.participantID)
        coalesce(\.emoji, \.emoji)
    }
}

extension Schema.MessageReaction {
    public init(id: ID, reactionKey: String, participantID: Schema.UserID, emoji: Bool?) {
        self.id = id
        self.reactionKey = reactionKey
        self.participantID = participantID
        self.emoji = emoji
    }
}
