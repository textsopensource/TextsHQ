//
// Copyright (c) Texts HQ
//

import Foundation
import os.log
import GRDB
import Merge
import Swallow

extension Database.ColumnType {
    public static let json = Database.ColumnType(rawValue: "JSON")

    public static let varchar = Database.ColumnType(rawValue: "VARCHAR")
}

public struct AppDatabase {
    public static let logger = Logger(subsystem: "TextsBase", category: "AppDatabase")

    public static let shared: AppDatabase = {
        do {
            let fileManager = FileManager.default

            guard let groupDir = fileManager.containerURL(forSecurityApplicationGroupIdentifier: "group.com.texts.shared") else {
                throw CustomStringError(description: "Failed to get app group directory")
            }

            let dbDir = groupDir.appendingPathComponent("db")
            try fileManager.createDirectory(at: dbDir, withIntermediateDirectories: true, attributes: nil)

            let databaseURL = dbDir.appendingPathComponent("texts.sqlite")
            let databasePool = try DatabasePool(path: databaseURL.path)

            return try AppDatabase(databasePool)
        } catch {
            fatalError(String(describing: error))
        }
    }()

    public let writer: DatabaseWriter

    private var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()

        #if DEBUG
        migrator.eraseDatabaseOnSchemaChange = true
        #endif

        migrator.registerMigration("createInitialTables") { db in
            try db.create(table: "threads") { t in
                t.column("threadID", .varchar).notNull()
                t.column("accountID", .varchar).notNull()
                t.column("platformName", .varchar).notNull()
                t.column("thread", .json).notNull()

                t.primaryKey(["accountID", "threadID"])
            }

            try db.create(table: "messages") { t in
                t.column("messageID", .varchar).notNull()
                t.column("accountID", .varchar).notNull()
                t.column("threadID", .varchar).notNull()
                t.column("platformName", .varchar).notNull()
                t.column("timestamp", .integer).notNull()

                t.column("message", .json).notNull()

                t.primaryKey(["accountID", "messageID"])
            }

            try db.create(table: "users") { t in
                t.column("userID", .varchar).notNull()
                t.column("accountID", .varchar).notNull()
                t.column("platformName", .varchar).notNull()

                t.column("user", .json).notNull()

                t.primaryKey(["accountID", "userID"])
            }

            try db.create(table: "thread_props") { t in
                t.column("accountID", .varchar).notNull()
                t.column("threadID", .varchar).notNull()

                t.column("props", .json).notNull()

                t.primaryKey(["accountID", "threadID"])
            }

            try db.create(table: "key_values") { t in
                t.column("key", .varchar).notNull()
                t.column("value", .varchar).notNull()

                t.primaryKey(["key"])
            }

            try db.create(table: "message_props") { t in
                t.column("accountID", .varchar).notNull()
                t.column("messageID", .varchar).notNull()
                t.column("threadID", .varchar).notNull()

                t.column("props", .json).notNull()

                t.primaryKey(["accountID", "messageID"])
            }

            try db.create(table: "snoozed_messages") { t in
                t.column("messageID", .varchar).notNull()
                t.column("accountID", .varchar).notNull()
                t.column("threadID", .varchar).notNull()
                t.column("messageTimestamp", .integer).notNull()
                t.column("remindOnTimestamp", .integer).notNull()
                t.column("addedOnTimestamp", .integer).notNull()

                t.column("options", .json).notNull()

                t.primaryKey(["accountID", "messageID"])
            }

            try db.create(table: "snoozed_threads") { t in
                t.column("accountID", .varchar).notNull()
                t.column("threadID", .varchar).notNull()
                t.column("messageTimestamp", .integer).notNull()
                t.column("remindOnTimestamp", .integer).notNull()
                t.column("addedOnTimestamp", .integer).notNull()

                t.column("options", .json).notNull()

                t.primaryKey(["accountID", "threadID"])
            }

            try db.create(table: "platforms") { t in
                t.column("platformName", .varchar).notNull()
                t.column("platformInfo", .json).notNull()

                t.primaryKey(["platformName"])
            }
        }

        return migrator
    }

    init(_ writer: DatabaseWriter) throws {
        self.writer = writer
        try migrator.migrate(writer)
    }
}

// MARK: - Database Access: Cache Reads

extension AppDatabase {
    var reader: DatabaseReader {
        writer
    }

    public static func getThreads(accountID: Schema.AccountID, before: Date? = nil) throws -> [Schema.Thread] {
        var threads: [Schema.Thread] = []

        try Self.shared.reader.read { db in
            let statement: Statement

            if let before = before {
                statement = try db.cachedStatement(literal: """
                    SELECT thread, cast(json_extract(thread, '$.timestamp') AS INT) AS timestamp FROM threads
                        WHERE threads.accountID = \(accountID.rawValue)
                            AND timestamp <= \(before.timeIntervalSinceReferenceDate.toInt())
                        GROUP BY threads.threadID
                        ORDER BY timestamp DESC
                        LIMIT 20;
                """)
            } else {
                statement = try db.cachedStatement(literal: """
                    SELECT thread, cast(json_extract(thread, '$.timestamp') AS INT) AS timestamp FROM threads
                        WHERE threads.accountID = \(accountID.rawValue)
                        GROUP BY threads.threadID
                        ORDER BY timestamp DESC
                        LIMIT 20;
                """)
            }

            threads = try Row
                .fetchAll(statement)
                .compactMap { row in
                    guard let threadJSON: String = row["thread"],
                          let threadData = threadJSON.data(using: .utf8) else {
                        return nil
                    }

                    do {
                        return try JSONDecoder().decode(Schema.Thread.self, from: threadData)
                    } catch {
                        Self.logger.error("Failed to decode thread from database: \(threadJSON)")
                        throw error
                    }
                }
        }

        return threads
    }

    public static func getMessages(
        accountID: Schema.AccountID,
        threadID: Schema.ThreadID,
        before: Schema.Message? = nil
    ) throws -> [Schema.Message] {
        var messages: [Schema.Message] = []

        try AppDatabase.shared.reader.read { db in
            let statement: Statement
            if let before = before {
                statement = try db.cachedStatement(literal: """
                    SELECT message FROM messages
                        WHERE messages.accountID = \(accountID.rawValue)
                            AND messages.threadID = \(threadID.rawValue)
                            AND messages.timestamp <= \(before.timestamp.timeIntervalSinceReferenceDate.toInt())
                        ORDER BY timestamp DESC
                        LIMIT 20
                """)
            } else {
                statement = try db.cachedStatement(literal: """
                    SELECT message FROM messages
                        WHERE messages.accountID = \(accountID.rawValue)
                            AND messages.threadID = \(threadID.rawValue)
                        ORDER BY timestamp DESC
                        LIMIT 20
                """)
            }

            messages = try Row
                .fetchAll(statement)
                .compactMap { row in
                    guard let messageJSON: String = row["message"],
                          let messageData = messageJSON.data(using: .utf8) else {
                        return nil
                    }

                    return try JSONDecoder().decode(Schema.Message.self, from: messageData)
                }
        }

        return messages
    }

    public static func getMessage(
        accountID: Schema.AccountID,
        threadID: Schema.ThreadID,
        messageID: Schema.MessageID
    ) async throws -> Schema.Message? {
        try await AppDatabase.shared.reader.read { db in
            let statement: Statement = try db.cachedStatement(literal: """
                    SELECT message FROM messages
                        WHERE messages.accountID = \(accountID.rawValue)
                            AND messages.threadID = \(threadID.rawValue)
                """)

            guard let row = try Row.fetchOne(statement),
                  let messageJSON: String = row["message"],
                  let messageData = messageJSON.data(using: .utf8) else {
                return nil
            }

           return try JSONDecoder().decode(Schema.Message.self, from: messageData)
        }
    }

    public static func getPlatformInfo() throws -> [Schema.PlatformInfo] {
        var platformInfo: [Schema.PlatformInfo] = []

        try Self.shared.reader.read { db in
            let statement = try db.cachedStatement(literal: "SELECT platformInfo FROM platforms")

            platformInfo = try Row
                .fetchAll(statement)
                .compactMap { row in
                    guard let platformInfoJSON: String = row["platformInfo"],
                          let platformInfoData = platformInfoJSON.data(using: .utf8) else {
                        return nil
                    }

                    return try JSONDecoder().decode(Schema.PlatformInfo.self, from: platformInfoData)
                }
        }

        return platformInfo
    }

    public static func getThreadProps(
        _ threadID: Schema.ThreadID,
        accountID: Schema.AccountID
    ) throws -> Schema.ThreadProps? {
        var threadProps: Schema.ThreadProps?

        try Self.shared.reader.read { db in
            let statement = try db.cachedStatement(literal: """
                SELECT props FROM thread_props
                    WHERE accountID = \(accountID.rawValue)
                        AND threadID = \(threadID.rawValue)
            """)

            guard let row = try Row.fetchOne(statement),
                  let propsJSON: String = row["props"],
                  let propsData = propsJSON.data(using: .utf8) else {
                return
            }

            threadProps = try JSONDecoder().decode(Schema.ThreadProps.self, from: propsData)
        }

        return threadProps
    }
}
