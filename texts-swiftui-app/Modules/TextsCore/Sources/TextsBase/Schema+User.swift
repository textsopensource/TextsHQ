//
// Copyright (c) Texts HQ
//

import Foundation

extension Schema {
    public typealias UserID = SchemaID<User, String>

    public struct _User: Codable, Hashable, Identifiable {
        public let id: UserID

        public var username: String?
        public var phoneNumber: String?
        public var email: String?

        public var fullName: String?
        public var nickname: String?
        public var imgURL: String?
        public var isVerified: Bool?

        public var cannotMessage: Bool?
        public var isSelf: Bool?

        public var social: UserSocialAttributes?

        // present iff this is a participant
        public var isAdmin: Bool?
        public var hasExited: Bool?

        // present iff this is a CurrentUser
        public var displayText: String?

        public init(
            id: Schema.UserID,
            username: String? = nil,
            phoneNumber: String? = nil,
            email: String? = nil,
            fullName: String? = nil,
            nickname: String? = nil,
            imgURL: String? = nil,
            isVerified: Bool? = nil,
            cannotMessage: Bool? = nil,
            isSelf: Bool? = nil,
            social: Schema.UserSocialAttributes? = nil,
            isAdmin: Bool? = nil,
            hasExited: Bool? = nil,
            displayText: String? = nil
        ) {
            self.id = id
            self.username = username
            self.phoneNumber = phoneNumber
            self.email = email
            self.fullName = fullName
            self.nickname = nickname
            self.imgURL = imgURL
            self.isVerified = isVerified
            self.cannotMessage = cannotMessage
            self.isSelf = isSelf
            self.social = social
            self.isAdmin = isAdmin
            self.hasExited = hasExited
            self.displayText = displayText
        }
    }

    public struct UserPresence: Codable, Hashable {
        public let userID: UserID
        public let status: String
        public let customStatus: String?
        public let lastActive: Date?
        public let durationMs: Int?
    }

    public typealias User = _User
}

extension Schema.User: PartializableModel {
    public typealias Partial = Schema._User

    public mutating func coalesceInPlace(with partial: Partial) {
        func coalesce<T>(_ dest: WritableKeyPath<Self, T>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        func coalesce<T>(_ dest: WritableKeyPath<Self, T?>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        coalesce(\.username, \.username)
        coalesce(\.phoneNumber, \.phoneNumber)
        coalesce(\.email, \.email)

        coalesce(\.fullName, \.fullName)
        coalesce(\.nickname, \.nickname)
        coalesce(\.imgURL, \.imgURL)
        coalesce(\.isVerified, \.isVerified)

        coalesce(\.cannotMessage, \.cannotMessage)
        coalesce(\.isSelf, \.isSelf)

        coalesce(\.social, \.social)

        coalesce(\.isAdmin, \.isAdmin)
        coalesce(\.hasExited, \.hasExited)

        coalesce(\.displayText, \.displayText)
    }
}

extension Schema.User {
    public var displayName: String {
        [
            displayText,
            nickname,
            fullName,
            username,
            phoneNumber,
            email,
            id.rawValue
        ].compactMap { $0 }.first { !$0.isEmpty } ?? ""
    }
}
