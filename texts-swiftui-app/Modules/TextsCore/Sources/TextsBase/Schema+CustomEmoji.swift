//
// Copyright (c) Texts HQ
//

extension Schema {
    public typealias CustomEmojiID = SchemaID<CustomEmoji, String>

    public struct _CustomEmoji<MaybeString: Codable & Hashable>: Codable, Hashable, Identifiable {
        public let id: CustomEmojiID
        @PASMaybe public var url: MaybeString
    }

    public typealias CustomEmoji = _CustomEmoji<String>

    public typealias CustomEmojiMap = [CustomEmojiID: CustomEmoji]
}

extension Schema.CustomEmoji: PartializableModel {
    public typealias Partial = Schema._CustomEmoji<String?>

    public mutating func coalesceInPlace(with partial: Partial) {
        func coalesce<T>(_ dest: WritableKeyPath<Self, T>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }
        func coalesce<T>(_ dest: WritableKeyPath<Self, T?>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }
        coalesce(\.url, \.url)
    }
}
