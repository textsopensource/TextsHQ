import Foundation

public struct AccountLoginError: Error, CustomStringConvertible {
    public let loginResult: Schema.LoginResult

    public var codeRequiredReason: Schema.CodeRequiredReason? {
        loginResult.type == .codeRequired ? loginResult.reason : nil
    }

    public var title: String? {
        loginResult.title
    }

    public var message: String? {
        loginResult.errorMessage
    }

    public init?(result: Schema.LoginResult) {
        guard result.type != .success else {
            return nil
        }

        self.loginResult = result
    }

    public var description: String {
        [codeRequiredReason.map { "\($0)" } ?? "Login error", title, message].compactMap { $0 }.joined(separator: ": ")
    }
}
