//
// Copyright (c) Texts HQ
//

import Foundation
import Swallow

extension Schema {
    // MARK: Enums

    public enum AuthObjectName: String, Codable {
        case loginHint = "login_hint"
        case displayName = "display_name"
    }

    public enum CodeRequiredReason: String, Codable {
        case checkpoint
        case twoFactor = "two_factor"
    }

    // MARK: Structs

    public struct LoginCreds: Codable {
        public let username: String?
        public let password: String?
        public let jsCodeResult: String?
        public let cookieJarJSON: CookieJar?
        public let code: String?
        public let lastLoginResult: LoginResult?

        public init(
            username: String?,
            password: String?,
            jsCodeResult: String?,
            cookieJarJSON: Schema.CookieJar?,
            code: String? = nil,
            lastLoginResult: Schema.LoginResult? = nil
        ) {
            self.username = username
            self.password = password
            self.jsCodeResult = jsCodeResult
            self.cookieJarJSON = cookieJarJSON
            self.code = code
            self.lastLoginResult = lastLoginResult
        }
    }

    public struct LoginResult: Codable {
        public enum LoginResultType: String, Codable {
            case codeRequired = "code_required"
            case error
            case success
            case wait
        }

        public let errorMessage: String?
        public let reason: CodeRequiredReason?
        public let metadata: AnyCodable?
        public let title: String?
        public let type: LoginResultType

        public init(error: Error?) {
            self.metadata = nil

            switch error {
            case let loginError as AccountLoginError:
                errorMessage = loginError.message
                reason = loginError.codeRequiredReason
                title = loginError.title
                type = loginError.codeRequiredReason == nil ? .error : .codeRequired
            case let error?:
                errorMessage = "\(error)"
                reason = nil
                title = "Error"
                type = .error
            case nil:
                errorMessage = nil
                reason = nil
                title = nil
                type = .success
            }
        }
    }

    public struct CookieJar: Codable, Hashable {
        public struct Cookie: Codable, Hashable {
            public let key: String
            public let value: String
            public let domain: String
            public let secure: Bool
            public let path: String

            public init(
                key: String,
                value: String,
                domain: String,
                secure: Bool,
                path: String
            ) {
                self.key = key
                self.value = value
                self.domain = domain
                self.secure = secure
                self.path = path
            }
        }

        public let cookies: [Cookie]

        public init(cookies: [Cookie]) {
            self.cookies = cookies
        }
    }
}
