//
// Copyright (c) Texts HQ
//

import Foundation
import OrderedCollections
import Swallow

extension Schema {
    public struct PlatformInfo: Codable, Identifiable, Hashable {
        public let name: String
        public let displayName: String
        public let icon: String?
        public let browserLogin: BrowserLogin?
        public let browserLogins: [BrowserLogin]?
        public let notifications: NotificationsInfo?
        public let deletionMode: MessageDeletionMode
        public let loginMode: LoginMode
        public let maxGroupTitleLength: Int?
        public let tags: [String]?
        @PASNumber public private(set) var typingDurationMs: Double?
        public let version: String?
        public let reactions: ReactionInfo?
        public let attachments: AttachmentInfo?
        public let attributes: Set<Attribute>
        public let prefs: [String: PlatformPref]?

        public var id: String {
            name
        }

        public static func == (lhs: Schema.PlatformInfo, rhs: Schema.PlatformInfo) -> Bool {
            lhs.id == rhs.id
        }

        public func hash(into hasher: inout Hasher) {
            hasher.combine(name)
        }

        public var gifsSupported: Bool {
            attachments?.gifMimeType != nil
        }
    }

    public enum MessageDeletionMode: String, Codable {
        case deleteForEveryone = "delete_for_everyone"
        case deleteForSelf = "delete_for_self"
        case none
        case unsend
        case unsupported
    }

    public enum LoginMode: String, Codable {
        case browser
        case custom
        case manual
    }

    public enum NotificationKind: String, Codable {
        case web
        case apple
        case android
    }

    public struct NotificationsInfo: Codable {
        public struct Web: Codable {
            public let vapidKey: String?
        }
        public let web: Web?

        public struct Apple: Codable {}
        public let apple: Apple?

        public struct Android: Codable {
            public let senderID: String
        }
        public let android: Android?
    }

    public struct Attribute: RawRepresentable, Codable, Hashable {
        public let rawValue: String
        public init(rawValue: String) {
            self.rawValue = rawValue
        }

        private init(_ rawValue: String) {
            self.rawValue = rawValue
        }

        /** Platform users can have an email address */
        public static let canMessageEmail = Attribute("can_message_email")
        /** Platform users can have a phone number */
        public static let canMessagePhoneNumber = Attribute("can_message_phone_number")
        /** Platform users can have a username */
        public static let canMessageUsername = Attribute("can_message_username")
        /** Don't cache messages threads users for the platform */
        public static let noCache = Attribute("no_cache")

        /** Platform defines Message.cursor. If it's missing for any message error checker plugin will complain. */
        public static let definesMessageCursor = Attribute("defines_message_cursor")

        /** When it's a group thread and user does an @-mention search all users in the autocomplete */
        public static let searchAllUsersForGroupMentions = Attribute("search_all_users_for_group_mentions")

        /** Platform doesn't allow creation of duplicate groups with the same set of users (Alice Bob) */
        public static let noSupportDuplicateGroupCreation = Attribute("no_support_duplicate_group_creation")
        /** Platform integration doesn't support adding participants to groups */
        public static let noSupportGroupAddParticipant = Attribute("no_support_group_add_participant")
        /** Platform integration doesn't support removing participants from groups */
        public static let noSupportGroupRemoveParticipant = Attribute("no_support_group_remove_participant")
        /** Platform integration doesn't support creating group threads */
        public static let noSupportGroupThreadCreation = Attribute("no_support_group_thread_creation")
        /** Platform integration doesn't support changing group titles */
        public static let noSupportGroupTitleChange = Attribute("no_support_group_title_change")
        /** Platform integration doesn't support creating single threads */
        public static let noSupportSingleThreadCreation = Attribute("no_support_single_thread_creation")
        /** Platform integration doesn't support sending typing indicator */
        public static let noSupportTypingIndicator = Attribute("no_support_typing_indicator")
        /** Creating a group thread requires the messageText arg to be present in PlatformAPI.createThread */
        public static let groupThreadCreationRequiresMessage = Attribute("group_thread_creation_requires_message")
        /** Creating a single thread requires the messageText arg to be present in PlatformAPI.createThread */
        public static let singleThreadCreationRequiresMessage = Attribute("single_thread_creation_requires_message")
        /** Creating a group thread requires the title arg to be present in PlatformAPI.createThread */
        public static let groupThreadCreationRequiresTitle = Attribute("group_thread_creation_requires_title")

        public static let sharesContacts = Attribute("shares_contacts")
        /** Sort messages by timestamp or custom key on receiving them */
        public static let sortMessagesOnPush = Attribute("sort_messages_on_push")
        public static let subscribeToConnStateChange = Attribute("subscribe_to_conn_state_change")
        public static let subscribeToThreadSelection = Attribute("subscribe_to_thread_selection")
        /** Send ONLINE/OFFLINE activity via sendActivityIndicator */
        public static let subscribeToOnlineOfflineActivity = Attribute("subscribe_to_online_offline_activity")

        public static let supportsArchive = Attribute("supports_archive")
        /** Platform supports reacting with custom emojis and sending custom emojis in Message.text
         *  and platform integration implements PlatformAPI.getCustomEmojis */
        public static let supportsCustomEmojis = Attribute("supports_custom_emojis")
        public static let supportsDeleteThread = Attribute("supports_delete_thread")
        public static let supportsReportThread = Attribute("supports_report_thread")
        /** Platform integration implements PlatformAPI.editMessage */
        public static let supportsEditMessage = Attribute("supports_edit_message")
        public static let supportsForward = Attribute("supports_forward")
        public static let supportsGroupImageChange = Attribute("supports_group_image_change")
        public static let supportsGroupParticipantRoleChange = Attribute("supports_group_participant_role_change")
        public static let supportsMarkAsUnread = Attribute("supports_mark_as_unread")
        public static let supportsPinThread = Attribute("supports_pin_thread")
        public static let supportsPresence = Attribute("supports_presence")
        public static let supportsQuotedMessages = Attribute("supports_quoted_messages")
        public static let supportsQuotedMessagesFromAnyThread = Attribute("supports_quoted_messages_from_any_thread")
        public static let supportsRequestsInbox = Attribute("supports_requests_inbox")
        public static let supportsSearch = Attribute("supports_search")
        public static let supportsStopTypingIndicator = Attribute("supports_stop_typing_indicator")
        public static let supportsPushNotifications = Attribute("supports_push_notifications")
        public static let supportsMessageExpiry = Attribute("supports_message_expiry")

        /** Platform integration implements editMessage() and has no rate limit for message edits */
        public static let supportsLiveTyping = Attribute("supports_live_typing")

        /** PlatformAPI.getMessages supports `direction=after` */
        public static let getMessagesSupportsAfterDirection = Attribute("get_messages_supports_after_direction")

        public init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            rawValue = try container.decode(String.self)
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            try container.encode(rawValue)
        }
    }

    public struct SupportedReaction: Codable, Equatable {
        public let title: String
        public let render: String?
        public let imgURL: String?
        public let disabled: Bool?
    }

    public struct ReactionInfo: Codable {
        @OrderedDictionaryDecoding public var supported: OrderedDictionary<String, SupportedReaction>

        public let supportsDynamicReactions: Bool?
        public let canReactWithAllEmojis: Bool?
        public let allowsMultipleReactionsToSingleMessage: Bool?

        public var enabledReactions: OrderedDictionary<String, SupportedReaction> {
            supported.filter({ !($0.value.disabled == true) })
        }

        public func renderValue(reaction: Schema.MessageReaction) -> String {
            guard reaction.emoji != true else {
                return reaction.reactionKey
            }

            let match = supported.first {
                $0.key.caseInsensitiveCompare(reaction.reactionKey) == .orderedSame
            }
            return match?.value.render ?? reaction.reactionKey
        }
    }

    public struct AttachmentInfo: Codable {
        public let noSupport: Bool?
        public let noSupportForImage: Bool?
        public let noSupportForVideo: Bool?
        public let noSupportForAudio: Bool?
        public let noSupportForFiles: Bool?
        public let supportsCaption: Bool?
        public let recordedAudioMimeType: String?
        public let gifMimeType: String?
        public let maxSize: MaxSize?

        public struct MaxSize: Codable {
            public let image: Int?
            public let video: Int?
            public let audio: Int?
            public let files: Int?
        }
    }

    public struct BrowserLogin: Codable {
        public let loginURL: String
        public let label: String?
        public let description: String?

        /** User-Agent to use for all requests in the window */
        public let userAgent: String?

        /** Closes the browser login window when a cookie with this name is found */
        public let authCookieName: String?

        /** Closes the browser login window when the page is redirected to a URL matching the regex */
        public let closeOnRedirectRegex: String?

        public let runJSOnLaunch: String?
        public let runJSOnNavigate: String?
        public let runJSOnClose: String?
    }

    public struct PlatformPref: Codable {
        public let label: String
        public let description: String?
        public let type: PrefType
        public let defaultValue: PrefValue

        enum CodingKeys: String, CodingKey {
            case label
            case description
            case type
            case defaultValue = "default"
        }

        public enum PrefType: String, Codable {
            case select
            case checkbox
        }

        // Either<T, U> is not encodable so we use this
        public enum PrefValue: Codable, Equatable {
            case bool(Bool), string(String)

            public init(from decoder: Decoder) throws {
                let container = try decoder.singleValueContainer()

                if let string = try? container.decode(String.self) {
                    self = .string(string)
                    return
                }

                if let bool = try? container.decode(Bool.self) {
                    self = .bool(bool)
                    return
                }

                throw DecodingError.dataCorruptedError(
                    in: container,
                    debugDescription: "Received unknown default value type"
                )
            }

            public func encode(to encoder: Encoder) throws {
                var container = encoder.singleValueContainer()

                switch self {
                case .bool(let value):
                    try container.encode(value)
                case .string(let value):
                    try container.encode(value)
                }
            }
        }
    }
}
