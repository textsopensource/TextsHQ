import Foundation

extension Schema {
    public struct TwitterNotificationPayload: Decodable {
        public let title: String
        public let body: String
        public let icon: String
        public let timestamp: String
        public let tag: String
        public let data: TwitterNotificationData

        public struct TwitterNotificationData: Decodable {
            public let lang: String
            public let bundle_text: String
            public let type: String
            public let uri: String
            public let impression_id: String
            public let title: String
            public let body: String
            public let tag: String
            public let scribe_target: String
            public let multi_body: String
        }
    }

    public struct InstagramNotificationPayload: Decodable {
        public struct InstagramNotificationData: Decodable {
            public let bc: String
            public let senderAppId: String
            public let gid: String
            public let sound: String
            public let bcTigd: String
            public let PushNotifID: String
            public let si: String
            public let pushPhase: String
            public let exp: String
            public let ig: String
            public let cc: String
            /** image URL */
            public let a: String
            public let ac: String
            public let c: String
            /** message */
            public let it: String
            /** title: message */
            public let m: String
            /** message ID */
            public let n: String
            public let collapseKey: String
            public let badge: String
            public let ndf: String
            /** sender ID */
            public let s: String
            /** user ID  */
            public let u: Double
            public let timeToLive: String
            public let messagingSourceTag: String
            public let pi: String
            public let bcTint: String
            public let networkClassification: String
            public let tp: String
            /** timestamp */
            public let ts: String
        }

        enum CodingKeys: String, CodingKey {
            case data
        }

        public let data: InstagramNotificationData

        public init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            let json = try container.decode(String.self, forKey: .data)
            let jsonDecoder = JSONDecoder()
            jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
            self.data = try jsonDecoder.decode(InstagramNotificationData.self, from: json.data())
        }
    }

    public struct SignalNotificationPayload: Decodable {
        public let notification: String
    }

    public struct AndroidNotificationPayload: Decodable {
        public let userUsername: String
        public let messageContent: String
    }

    public struct MappedNotificationPayload {
        public var platform: String
        public var sender: Sender
        public var thread: Thread

        public struct Thread {
            public var title: String?
            public var message: String?
            public var imgURL: String?
            public var numParticipants: Int?
            public var type: Schema.ThreadType

            public init(
                title: String? = nil,
                message: String? = nil,
                imgURL: String? = nil,
                numParticipants: Int? = nil,
                type: Schema.ThreadType = .single
            ) {
                self.title = title
                self.message = message
                self.imgURL = imgURL
                self.numParticipants = numParticipants
                self.type = type
            }
        }

        public struct Sender {
            public var name: String?
            public var phoneNumber: String?
            public var email: String?

            public init(
                name: String? = nil,
                phoneNumber: String? = nil,
                email: String? = nil
            ) {
                self.name = name
                self.phoneNumber = phoneNumber
                self.email = email
            }
        }

        public init(
            platform: String,
            thread: Thread,
            sender: Sender
        ) {
            self.platform = platform
            self.thread = thread
            self.sender = sender
        }
    }
}
