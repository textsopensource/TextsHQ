//
// Copyright (c) Texts HQ
//

import Foundation
import Swallow

extension Schema {
    public struct UserSocialAttributes: Codable, Hashable {
        public let coverImgURL: URL?
        public let bio: Schema.AttributedText?
        public let website: String?
        public let followers: UsersOrCount?
        public let followingUsers: UsersOrCount?
        public let friends: UsersOrCount?
        /// Is the logged in user following them.
        public let following: Bool?
        /// Is the logged in user followed by them.
        public let followedBy: Bool?
    }

    public struct UsersOrCount: Codable, Hashable {
        public let userIDs: [String]?
        public let count: Int?
    }
}
