//
// Copyright (c) Texts HQ
//

import Swallow

extension Schema {
    public struct AttributedText: Codable, Hashable {
        public let text: String?
        public let attributes: TextAttributes?
    }
}
