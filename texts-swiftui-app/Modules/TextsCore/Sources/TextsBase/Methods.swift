//
// Copyright (c) Texts HQ
//

import Foundation
import Swallow
import Merge

public enum PlatformServerError: Error {
    case launchingFailed(String?)
    case notLaunched
    case methodFailed(String)
}

public struct Methods {
    @_spi(APICaller)
    public init() {}
}

extension Methods {
    public struct Method<Input, Output> {
        @_spi(APICaller) public let name: String
        @_spi(APICaller) public let encode: ((Input, Encoder) throws -> Void)?
        @_spi(APICaller) public let decode: (Decoder) throws -> Output

        public init(
            _ name: String,
            encode: ((Input, Encoder) throws -> Void)? = nil,
            decode: @escaping (Decoder) throws -> Output
        ) {
            self.name = name
            self.encode = encode
            self.decode = decode
        }

        public init(
            _ name: String,
            encode: ((Input, Encoder) throws -> Void)? = nil
        ) where Output: Decodable {
            self.name = name
            self.encode = encode
            self.decode = {
                try $0.singleValueContainer().decode(Output.self)
            }
        }

        public init(
            _ name: String,
            encode: ((Input, Encoder) throws -> Void)? = nil
        ) where Output == Void {
            self.name = name
            self.encode = encode
            self.decode = { _ in }
        }

        @_spi(APICaller)
        public func encode(input: Input, to encoder: Encoder) throws {
            if let encode = encode {
                return try encode(input, encoder)
            }

            var container = encoder.unkeyedContainer()

            // if the input is a single encodable value, just encode it
            // into the container
            if let encodable = input as? Encodable {
                return try encodable.encode(to: &container)
            }

            let mirror = Mirror(reflecting: input)
            guard mirror.displayStyle == .tuple else {
                fatalError("Encountered non-tuple, non-encodable value in PAS method call: \(input)")
            }

            for (label, value) in mirror.children {
                // nil values will pass this test (Optional<T>.none is Encodable
                // when T: Encodable), and get encoded as null. We don't skip encoding
                // them because that would mess up the order of arguments.
                guard let encodable = value as? Encodable else {
                    fatalError("""
                    Encountered non-encodable type in PAS method call\(label.map { " (label: \($0))" } ?? ""): \(value)
                    """)
                }
                try encodable.encode(to: &container)
            }
        }
    }
}

extension Methods {
    public var initialize: Method<
        (session: String?, accountInfo: Schema.AccountInfo, prefs: [String: Schema.PlatformPref.PrefValue]),
        Void
    > { .init("init") }

    public var dispose: Method<(), Void> { .init("dispose") }

    public var getCurrentUser: Method<(), Schema.User> { .init("getCurrentUser") }

    public var login: Method<
        Schema.LoginCreds?,
        Schema.LoginResult
    > { .init("login") }

    public var logout: Method<(), Void> { .init("logout") }

    public var serializeSession: Method<(), String> {
        .init("serializeSession", decode: { decoder in
            (try? decoder.singleValueContainer().decode(String.self)) ?? "{}"
        })
    }

    public var takeoverConflict: Method<(), Void> { .init("takeoverConflict") }

    public var searchUsers: Method<
        String,
        [Schema.User]
    > { .init("searchUsers") }

    public var searchThreads: Method<
        String,
        [Schema.Thread]
    > { .init("searchThreads") }

    public var searchMessages: Method<
        (typed: String, pagination: Schema.PaginationArguments?, threadID: Schema.ThreadID?),
        Schema.Paginated<Schema.Message>
    > { .init("searchMessages") }

    public var getPresence: Method<(), [String: Schema.UserPresence]> { .init("getPresence") }

    public var getCustomEmojis: Method<(), [String: Schema.CustomEmoji]> { .init("getCustomEmojis") }

    public var getThreads: Method<
        (inboxName: Schema.InboxName, args: Schema.PaginationArguments?),
        Schema.Paginated<Schema.Thread>?
    > { .init("getThreads") }

    public var getMessages: Method<
        (threadID: Schema.ThreadID, args: Schema.PaginationArguments?),
        Schema.Paginated<Schema.Message>
    > { .init("getMessages") }

    public var getThreadParticipants: Method<
        (threadID: Schema.ThreadID, pagination: Schema.PaginationArguments?),
        Schema.Paginated<Schema.User>
    > { .init("getThreadParticipants") }

    public var getThread: Method<Schema.ThreadID, Schema.Thread> { .init("getThread") }

    public var getMessage: Method<
        (threadID: Schema.ThreadID, messageID: Schema.MessageID),
        Schema.Message?
    > { .init("getMessage") }

    public var getUser: Method<Schema.GetUserOptions, Schema.User?> { .init("getUser") }

    public var createThread: Method<
        (users: [Schema.UserID], title: String?),
        Schema.Thread
    > { .init("createThread") }

    public var updateThread: Method<
        (threadID: Schema.ThreadID, updates: Schema.Thread.Partial),
        Void
    > { .init("updateThread") }

    public var deleteThread: Method<
        Schema.ThreadID,
        Void
    > { .init("deleteThread") }

    public var reportThread: Method<
        (type: Schema.ReportType, threadID: Schema.ThreadID, firstMessageID: Schema.MessageID?),
        Bool
    > { .init("reportThread") }

    public var sendMessage: Method<
        (threadID: Schema.ThreadID, content: Schema.MessageContent, options: Schema.SendMessageOptions?),
        Either<Bool, [Schema.Message]>
    > { .init("sendMessage") }

    public var editMessage: Method<
        (
            threadID: Schema.ThreadID,
            messageID: Schema.ThreadID,
            content: Schema.MessageContent,
            options: Schema.SendMessageOptions?
        ),
        Either<Bool, [Schema.Message]>
    > { .init("editMessage") }

    public var forwardMessage: Method<
        (
            threadID: Schema.ThreadID,
            messageID: Schema.MessageID,
            threadIDs: [Schema.ThreadID]?,
            userIDs: [Schema.UserID]?
        ),
        Void
    > { .init("forwardMessage") }

    public var sendActivityIndicator: Method<
        (type: Schema.ActivityType, threadID: Schema.ThreadID),
        Void
    > { .init("sendActivityIndicator") }

    public var deleteMessage: Method<
        (threadID: Schema.ThreadID, messageID: Schema.MessageID),
        Bool
    > { .init("deleteMessage") }

    public var sendReadReceipt: Method<
        (threadID: Schema.ThreadID, messageID: Schema.MessageID?, messageCursor: String?),
        Void
    > { .init("sendReadReceipt") }

    public var addReaction: Method<
        (threadID: Schema.ThreadID, messageID: Schema.MessageID, reactionKey: String),
        Void
    > { .init("addReaction") }

    public var removeReaction: Method<
        (threadID: Schema.ThreadID, messageID: Schema.MessageID, reactionKey: String),
        Void
    > { .init("removeReaction") }

    public var getLinkPreview: Method<String, Schema.MessageLink> { .init("getLinkPreview") }

    public var addParticipant: Method<
        (threadID: Schema.ThreadID, participantID: Schema.UserID),
        Void
    > { .init("addParticipant") }

    public var removeParticipant: Method<
        (threadID: Schema.ThreadID, participantID: Schema.UserID),
        Void
    > { .init("removeParticipant") }

    public var changeParticipantRole: Method<
        (threadID: Schema.ThreadID, participantID: Schema.UserID, role: String),
        Void
    > { .init("changeParticipantRole") }

    public var changeThreadImage: Method<
        (threadID: Schema.ThreadID, image: Data, mimeType: String),
        Void
    > { .init("changeThreadImage") }

    public var markAsUnread: Method<
        Schema.ThreadID,
        Void
    > { .init("markAsUnread") }

    public var archiveThread: Method<
        (threadID: Schema.ThreadID, isArchiving: Bool),
        Void
    > { .init("archiveThread") }

    public var pinThread: Method<
        (threadID: Schema.ThreadID, isPinned: Bool),
        Void
    > { .init("pinThread") }

    public var notifyAnyway: Method<Schema.ThreadID, Void> { .init("notifyAnyway") }

    public var onThreadSelected: Method<
        Schema.ThreadID?,
        [Schema.User]
    > { .init("onThreadSelected") }

    public var loadDynamicMessage: Method<
        Schema.Message,
        Schema.Message.Partial
    > { .init("loadDynamicMessage") }

    public var registerForPushNotifications: Method<
        (type: Schema.NotificationKind, token: String),
        Void
    > { .init("registerForPushNotifications") }

    public var unregisterForPushNotifications: Method<
        (type: Schema.NotificationKind, token: String),
        Void
    > { .init("unregisterForPushNotifications") }

    public var handleDeepLink: Method<String, Void> { .init("handleDeepLink") }

    public var reconnectRealtime: Method<(), Void> { .init("reconnectRealtime") }
}

extension Schema {
    public enum ReportType: String, Codable, Hashable {
        case spam
    }
}

extension Schema {
    public struct GetUserOptions: Encodable {
        public let userID: Schema.UserID?
        public let username: String?
        public let phoneNumber: String?
        public let email: String?
    }
}

extension Schema {
    public struct SendMessageOptions: Encodable {
        public var pendingMessageID: Schema.MessageID
        public var quotedMessageThreadID: Schema.ThreadID?
        public var quotedMessageID: Schema.MessageID?

        public init(
            pendingMessageID: Schema.MessageID,
            quotedMessageThreadID: Schema.ThreadID? = nil,
            quotedMessageID: Schema.MessageID? = nil
        ) {
            self.pendingMessageID = pendingMessageID
            self.quotedMessageThreadID = quotedMessageThreadID
            self.quotedMessageID = quotedMessageID
        }
    }

    public struct MessageContent: Encodable, Hashable {
        public var text: String?
        public var filePath: String?
        public var fileBuffer: Data?
        public var fileName: String?

        public var mimeType: String?
        public var isGif: Bool?
        public var size: Size?
        public var videoURL: String?

        public var isRecordedAudio: Bool?
        @PASNumber public var audioDurationSeconds: Double?
        public var mentionedUserIDs: [UserID]

        //        isRecordedAudio?: boolean;
        //            audioDurationSeconds?: number;
        //            mentionedUserIDs?: string[];

        public init(
            text: String? = nil,
            filePath: String? = nil,
            fileBuffer: Data? = nil,
            fileName: String? = nil,
            mimeType: String? = nil,
            isGif: Bool? = nil,
            size: Size? = nil,
            videoURL: String? = nil,
            isRecordedAudio: Bool? = nil,
            audioDurationSeconds: Double? = nil,
            mentionedUserIDs: [UserID] = []
        ) {
            self.text = text
            self.filePath = filePath
            self.fileBuffer = fileBuffer
            self.fileName = fileName
            self.mimeType = mimeType
            self.isGif = isGif
            self.size = size
            self.videoURL = videoURL
            self.isRecordedAudio = isRecordedAudio
            self.audioDurationSeconds = audioDurationSeconds
            self.mentionedUserIDs = mentionedUserIDs
        }
    }
}

extension Schema {
    public enum CallbackMethod: String, CustomStringConvertible {
        case subscribeToEvents
        case onLoginEvent
        case onConnectionStateChange

        public var description: String {
            rawValue
        }
    }

    public struct MethodError: Error, Decodable, CustomStringConvertible {
        public let platformName: String
        public let methodName: String
        public let name: String?
        public let message: String
        public let stack: String?

        public var description: String {
            "MethodError [\(platformName).\(methodName)]: \(stack ?? "\(name ?? "Error"): \(message)")"
        }

        public init(
            platformName: String,
            methodName: String,
            name: String?,
            message: String,
            stack: String?
        ) {
            self.platformName = platformName
            self.methodName = methodName
            self.name = name
            self.message = message
            self.stack = stack
        }
    }

    public struct PushEventBase: Decodable, Hashable {
        public let accountID: AccountID
        public let methodName: String

        public init(accountID: AccountID, methodName: String) {
            self.accountID = accountID
            self.methodName = methodName
        }
    }

    public struct PushEvent<T: Decodable>: Decodable {
        public let accountID: AccountID
        public let methodName: String
        public let cbData: T
    }
}
