import Foundation
import Combine

extension Publisher {
    public func retry<S>(
        _ retries: Int,
        delay: S.SchedulerTimeType.Stride,
        scheduler: S
    ) -> AnyPublisher<Output, Failure> where S: Scheduler {
        self
            .catch { error in
                Future { completion in
                    scheduler.schedule(after: scheduler.now.advanced(by: delay)) {
                        completion(.failure(error))
                    }
                }
            }
            .retry(retries)
            .eraseToAnyPublisher()
    }
}
