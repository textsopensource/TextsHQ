import Foundation
import Merge

extension Publisher {
    @_disfavoredOverload
    public var values: AsyncThrowingStream<Output, Error> {
        AsyncThrowingStream { cont in
            let cancellable = sink { completion in
                switch completion {
                case .failure(let error):
                    cont.finish(throwing: error)
                case .finished:
                    cont.finish(throwing: nil)
                }
            } receiveValue: { value in
                cont.yield(value)
            }
            cont.onTermination = { @Sendable termination in
                if case .cancelled = termination {
                    cancellable.cancel()
                }
            }
        }
    }
}

extension Publisher where Failure == Never {
    public var values: AsyncStream<Output> {
        AsyncStream { cont in
            let cancellable = sink { _ in
                cont.finish()
            } receiveValue: { value in
                cont.yield(value)
            }
            cont.onTermination = { @Sendable termination in
                if case .cancelled = termination {
                    cancellable.cancel()
                }
            }
        }
    }
}

extension SingleOutputPublisher {
    public var value: Output {
        get async throws {
            guard let next = try await values.first(where: { _ in true }) else {
                throw CancellationError()
            }
            return next
        }
    }
}

extension SingleOutputPublisher where Failure == Never {
    // returns nil if the current task is cancelled
    public var value: Output? {
        get async {
            guard let next = await values.first(where: { _ in true }) else {
                return nil
            }
            return next
        }
    }
}

public protocol NonThrowingAsyncIterator: AsyncIteratorProtocol {
    mutating func noexceptNext() async -> Element?
}

public protocol SafeNonThrowingAsyncIterator: NonThrowingAsyncIterator {
    mutating func next() async -> Element?
}
extension SafeNonThrowingAsyncIterator {
    public mutating func noexceptNext() async -> Element? {
        await next()
    }
}

public protocol UnsafeNonThrowingAsyncIterator: NonThrowingAsyncIterator {}
extension UnsafeNonThrowingAsyncIterator {
    public mutating func noexceptNext() async -> Element? {
        // swiftlint:disable:next force_try
        try! await next()
    }
}

extension AsyncStream.Iterator: SafeNonThrowingAsyncIterator {}
extension TaskGroup.Iterator: SafeNonThrowingAsyncIterator {}

extension AsyncCompactMapSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncDropFirstSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncDropWhileSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncFilterSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncFlatMapSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncMapSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncPrefixSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}
extension AsyncPrefixWhileSequence.Iterator: UnsafeNonThrowingAsyncIterator, NonThrowingAsyncIterator where Base.AsyncIterator: NonThrowingAsyncIterator {}

extension AsyncSequence {
    public func publisher(priority: TaskPriority? = nil) -> AnyPublisher<Element, Error> {
        let subject = PassthroughSubject<Element, Error>()
        let task = Task.detached(priority: priority) {
            var it = makeAsyncIterator()
            do {
                while let elt = try await it.next(), !Task.isCancelled {
                    subject.send(elt)
                }
            } catch {
                subject.send(completion: .failure(error))
                return
            }
            subject.send(completion: .finished)
        }
        return subject
            .handleEvents(receiveCancel: task.cancel)
            .eraseToAnyPublisher()
    }
}

extension AsyncSequence where AsyncIterator: NonThrowingAsyncIterator {
    public func publisher(priority: TaskPriority? = nil) -> AnyPublisher<Element, Never> {
        let subject = PassthroughSubject<Element, Never>()
        let task = Task.detached(priority: priority) {
            var it = makeAsyncIterator()
            while let elt = await it.noexceptNext(), !Task.isCancelled {
                subject.send(elt)
            }
            subject.send(completion: .finished)
        }
        return subject
            .handleEvents(receiveCancel: task.cancel)
            .eraseToAnyPublisher()
    }
}
