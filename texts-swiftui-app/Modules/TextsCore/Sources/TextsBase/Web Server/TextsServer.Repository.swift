//
// Copyright (c) Texts HQ
//

import NetworkKit

extension TextsServer {
    public final class Repository: HTTPRepository {
        public let interface = TextsServer()
        public let session = HTTPSession()

        public init() {}
    }
}
