//
// Copyright (c) Texts HQ
//

import NetworkKit

private let decoder: JSONDecoder = {
    let decoder = JSONDecoder(keyDecodingStrategy: .convertFromSnakeCase)
    decoder.dateDecodingStrategy = .javaScriptISO8601
    return decoder
}()

public struct TextsServer: RESTfulHTTPInterface {
    public let id = UUID()

    public var host: URL {
        URL(string: "https://texts.com/api")!
    }

    @Path("app-login")
    @POST
    @Body(json: \.input)
    public var login = Endpoint<Requests.AppLogin, Responses.AppLogin, Void>()

    @Path("validate-session")
    @GET
    public var validateSession = Endpoint<Void, Responses.ValidateSession, Void>()

    @Path("app-logout")
    @POST
    @Body(json: \.input)
    public var logout = Endpoint<Requests.AppLogout, Void, Void>()

    @Path("push/register")
    @POST
    @Body(json: \.input)
    public var pushRelayRegister = Endpoint<Requests.PushRelayRegister, Responses.PushRelayRegister, Void>()

    @Path("push/unregister")
    @POST
    @Body(json: \.input)
    public var pushRelayUnregister = Endpoint<Requests.PushRelayUnregister, Void, Void>()
}

extension TextsServer {
    public class Endpoint<Input, Output, Options>: BaseHTTPEndpoint<TextsServer, Input, Output, Options> {
        public override func buildRequestBase(
            from input: Input,
            context: BuildRequestContext
        ) throws -> HTTPRequest {
            try super.buildRequestBase(from: input, context: context)
                .header(.custom(key: "x-device-id", value: UIDevice.current.identifierForVendor?.uuidString ?? "unavailable"))
                .header(.custom(key: "x-app-version", value: Bundle.main.version!.description))
                .header(.custom(key: "x-os", value: "iOS-\(UIDevice.current.systemVersion)-\(UIDevice.currentDeviceIdentifier)"))
                .header(.contentType(.json))
                .httpShouldHandleCookies(true)
        }

        public override func decodeOutputBase(
            from response: HTTPResponse,
            context _: DecodeOutputContext
        ) throws -> Output {
            try response.validate()

            return try response.decode(
                Output.self,
                using: decoder
            )
        }
    }
}

// MARK: - Schema

extension TextsServer {
    public enum Schema {
        public struct AuthenticatedUser: Codable, Hashable {
            public let uid: String
            public let fullName: String
            public let emails: [String]
            public let paymentStatus: PaymentStatus
        }

        public enum PaymentStatus: Codable, Hashable {
            case free
            case premium
            case unknown(String)

            public init(from decoder: Decoder) throws {
                let container = try decoder.singleValueContainer()
                let string = try container.decode(String.self)
                switch string {
                case "free":
                    self = .free
                case "premium":
                    self = .premium
                default:
                    self = .unknown(string)
                }
            }

            public func encode(to encoder: Encoder) throws {
                let string: String
                switch self {
                case .free:
                    string = "free"
                case .premium:
                    string = "premium"
                case .unknown(let value):
                    string = value
                }
                var container = encoder.singleValueContainer()
                try container.encode(string)
            }
        }
    }
}

// MARK: - Requests & Responses

extension TextsServer {
    public enum Requests {
        public struct AppLogin: Codable, Hashable {
            public let token: String

            public init(token: String) {
                self.token = token
            }
        }

        public struct AppLogout: Codable, Hashable {
            public init() {

            }
        }

        public struct PushRelayRegister: Codable, Hashable {
            public let deviceToken: String
            public let type: String
            public let regId: String?
            public let authorizedEntity: String?
            public let ttl: TimeInterval?

            public init(deviceToken: String, type: String, regId: String? = nil, authorizedEntity: String? = nil, ttl: TimeInterval? = nil) {
                self.deviceToken = deviceToken
                self.type = type
                self.regId = regId
                self.authorizedEntity = authorizedEntity
                self.ttl = ttl
            }
        }

        public struct PushRelayUnregister: Codable, Hashable {
            public let regId: String

            public init(regId: String) {
                self.regId = regId
            }
        }
    }
}

extension TextsServer {
    public enum Responses {
        public struct AppLogin: Codable {
            public struct LoginHint: Codable {
                let platform: String
                let displayName: String
            }

            public let loginHint: LoginHint?
            public let user: Schema.AuthenticatedUser?
        }

        public struct ValidateSession: Codable {
            public let loggedIn: Bool
            public let user: Schema.AuthenticatedUser?
        }

        public struct PushRelayRegister: Decodable {
            public let id: String
            public let expiry: Date?
            public let token: String
        }
    }
}
