import UIKit

extension UIDevice {
    public static var currentDeviceIdentifier: String {
        var systemInfo = utsname()

        uname(&systemInfo)

        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce(into: "") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else {
                return
            }

            identifier += String(UnicodeScalar(UInt8(value)))
        }

        return identifier
    }
}
