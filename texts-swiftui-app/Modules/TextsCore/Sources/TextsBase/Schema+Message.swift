//
// Copyright (c) Texts HQ
//

import Foundation
import UniformTypeIdentifiers
import Swallow
import UIKit
import IdentifiedCollections

extension Schema {
    public struct MessageLink: Codable, Hashable {
        public let url: String
        public let originalURL: String?
        public let favicon: String?
        public let img: String?
//        public let imgSize: Size?
        public let title: String?
        public let summary: String?
    }

    public struct MessageButton: Codable, Hashable {
        public let label: String
        public let linkURL: String
    }

    public struct MessagePreview: Codable, Hashable {
        public let id: String
        public let senderID: UserID
        public let text: String
    }

    public struct TextEntity: Codable, Hashable {
        public struct ReplacedMedia: Codable, Hashable {
            public let mediaType: String
            public let srcURL: String
            public let size: Size?
        }

        public struct MentionedUser: Codable, Hashable {
            public let id: UserID?
            public let username: String?
        }

        public let from: Int
        public let to: Int
        public var bold: Bool?
        public var italic: Bool?
        public var underline: Bool?
        public var strikethrough: Bool?
        public var code: Bool?
        public var pre: Bool?
        public var codeLanguage: String?
        public var replaceWith: String?
        public var replaceWithMedia: ReplacedMedia?
        public var link: String?
        public var mentionedUser: MentionedUser?
    }

    public struct TextAttributes: Codable, Hashable {
        public let entities: [TextEntity]?
        public let heDecode: Bool?
    }

    public struct MessageAttachment: Codable, Hashable, Identifiable {
        public enum Kind: String, Codable, Hashable {
            case audio
            case img
            case video
            case unknown
        }

        public let id: String
        public let type: Kind

        public let caption: String?

        public let isGif: Bool?
        public let isSticker: Bool?
        public let loading: Bool?

        public let data: Data?
        public let size: Size?
        public let srcURL: String?
        public let fileName: String?

        public let mimeType: String?
        public let posterImg: String?

        public var isDataImageMIMEType: Bool {
            mimeType.flatMap { UTType(mimeType: $0) }?.conforms(to: .image) == true
        }

        public init(
            id: String,
            type: Kind,
            caption: String? = nil,
            isGif: Bool? = nil,
            isSticker: Bool? = nil,
            loading: Bool? = nil,
            data: Data? = nil,
            size: Size? = nil,
            srcURL: String? = nil,
            fileName: String? = nil,
            mimeType: String? = nil,
            posterImg: String? = nil
        ) {
            self.id = id
            self.type = type
            self.caption = caption
            self.isGif = isGif
            self.isSticker = isSticker
            self.loading = loading
            self.data = data
            self.size = size
            self.srcURL = srcURL
            self.fileName = fileName
            self.mimeType = mimeType
            self.posterImg = posterImg
        }
    }

    public final class Tweet: Codable, Hashable, Identifiable {
        public struct User: Codable, Hashable {
            public let imgURL: String
            public let name: String
            public let username: String
        }

        public let id: String
        public let user: User
        public let timestamp: Date?
        public let url: String?
        public let text: String
        public let textAttributes: TextAttributes?
        public let attachments: [MessageAttachment]?
        public let quotedTweet: Tweet?

        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
            hasher.combine(user)
            hasher.combine(timestamp)
            hasher.combine(url)
            hasher.combine(text)
            hasher.combine(textAttributes)
            hasher.combine(attachments)
            hasher.combine(quotedTweet)
        }

        public static func == (lhs: Schema.Tweet, rhs: Schema.Tweet) -> Bool {
            lhs.id == rhs.id
                && lhs.user == rhs.user
                && lhs.timestamp == rhs.timestamp
                && lhs.url == rhs.url
                && lhs.text == rhs.text
                && lhs.textAttributes == rhs.textAttributes
                && lhs.attachments == rhs.attachments
                && lhs.quotedTweet == rhs.quotedTweet
        }
    }

    public typealias MessageID = SchemaID<Message, String>

    public enum MessageBehavior: String, Codable, Hashable {
        case silent
        case keepRead = "keep_read"
        case dontNotify = "dont_notify"
    }

    public enum Seen: Codable, Hashable {
        public enum Single: Codable, Hashable {
            case notSeen
            case seen(Date?)

            public init(from decoder: Decoder) throws {
                let container = try decoder.singleValueContainer()
                if let bool = try? container.decode(Bool.self) {
                    self = bool ? .seen(nil) : .notSeen
                } else {
                    let date = try container.decode(Date.self)
                    if abs(date.timeIntervalSince1970) <= 0.1 {
                        // UNKNOWN_DATE
                        self = .seen(nil)
                    } else if abs(date.timeIntervalSinceReferenceDate) <= 1 {
                        // MessagePacker decoding quirks seem to occasionally result in
                        // the reference date when we should be getting nil
                        self = .notSeen
                    } else {
                        self = .seen(date)
                    }
                }
            }

            public func encode(to encoder: Encoder) throws {
                var container = encoder.singleValueContainer()
                switch self {
                case .notSeen:
                    try container.encode(false)
                case .seen(nil):
                    try container.encode(true)
                case .seen(let date):
                    try container.encode(date)
                }
            }
        }

        case single(Single)
        case group([UserID: Single])

        public init(from decoder: Decoder) throws {
            let container = try decoder.singleValueContainer()
            if let single = try? container.decode(Single.self) {
                self = .single(single)
            } else {
                let dict = try container.decode([String: Single].self)
                self = .group(dict.mapKeys(UserID.init(rawValue:)))
            }
        }

        public func encode(to encoder: Encoder) throws {
            var container = encoder.singleValueContainer()
            switch self {
            case .single(let single):
                try container.encode(single)
            case .group(let dict):
                try container.encode(dict.mapKeys(\.rawValue))
            }
        }
    }

    public struct _Message<MaybeDate: Codable & Hashable>: Codable, Hashable, Identifiable {
        public let id: MessageID

        public var _original: String?
        @PASMaybe public var timestamp: MaybeDate
        public var editedTimestamp: Date?
        @PASNumber public var expiresInSeconds: Double?
        @PASNumber public var forwardedCount: Double?
        public var senderID: UserID?

        public var text: String?
        public var textAttributes: TextAttributes?
        public var textHeading: String?
        public var textFooter: String?

        public var attachments: [MessageAttachment]?
        public var tweets: [Tweet]?
        public var links: [MessageLink]?
        public var iframeURL: String?

        public var reactions: IdentifiedArrayOf<MessageReaction>?
        public var seen: Seen?
        public var isDelivered: Bool?
        public var isHidden: Bool?
        /** `isSender` should be true if the logged in user sent the message, default = false */
        public var isSender: Bool?
        public var isAction: Bool?
        public var isDeleted: Bool?
        public var isErrored: Bool?
        public var isDynamicMessage: Bool?
        public var parseTemplate: Bool?

        /** thread ID of the quoted message, should be null if same thread as this message */
        public var linkedMessageThreadID: Schema.ThreadID?
        /** message ID of the quoted message. also set `linkedMessageThreadID` if message belongs to a different thread */
        public var linkedMessageID: MessageID?
        public var linkedMessage: MessagePreview?
        public var action: Action?
        public var cursor: String?
        public var buttons: [MessageButton]?

//        public var extra: Any?

        public var behavior: MessageBehavior?

        public var accountID: AccountID?
        public var threadID: ThreadID?
    }

    public typealias Message = _Message<Date>
}

// MARK: - Extensions

extension Schema.Message: PartializableModel {
    public typealias Partial = Schema._Message<Date?>

    // TODO: Use KeyPath reflection once that's official
    // (https://github.com/apple/swift-evolution-staging/tree/reflection)
    public mutating func coalesceInPlace(with partial: Partial) {
        func coalesce<T>(_ dest: WritableKeyPath<Self, T>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        func coalesce<T>(_ dest: WritableKeyPath<Self, T?>, _ src: KeyPath<Partial, T?>) {
            partial[keyPath: src].map { self[keyPath: dest] = $0 }
        }

        coalesce(\._original, \._original)
        coalesce(\.timestamp, \.timestamp)
        coalesce(\.editedTimestamp, \.editedTimestamp)
        coalesce(\.expiresInSeconds, \.expiresInSeconds)
        coalesce(\.senderID, \.senderID)
        coalesce(\.text, \.text)
        coalesce(\.textAttributes, \.textAttributes)
        coalesce(\.textHeading, \.textHeading)
        coalesce(\.textFooter, \.textFooter)
        coalesce(\.attachments, \.attachments)
        coalesce(\.tweets, \.tweets)
        coalesce(\.links, \.links)
        coalesce(\.iframeURL, \.iframeURL)
        coalesce(\.reactions, \.reactions)
        coalesce(\.seen, \.seen)
        coalesce(\.isDelivered, \.isDelivered)
        coalesce(\.isHidden, \.isHidden)
        coalesce(\.isSender, \.isSender)
        coalesce(\.isAction, \.isAction)
        coalesce(\.isDeleted, \.isDeleted)
        coalesce(\.isErrored, \.isErrored)
        coalesce(\.isDynamicMessage, \.isDynamicMessage)
        coalesce(\.parseTemplate, \.parseTemplate)
        coalesce(\.linkedMessageID, \.linkedMessageID)
        coalesce(\.linkedMessage, \.linkedMessage)
        coalesce(\.action, \.action)
        coalesce(\.cursor, \.cursor)
        coalesce(\.accountID, \.accountID)
        coalesce(\.threadID, \.threadID)
    }
}

extension Schema.Message {
    public init(
        id: ID,
        _original: String? = nil,
        timestamp: Date,
        editedTimestamp: Date? = nil,
        expiresInSeconds: Double? = nil,
        senderID: Schema.UserID? = nil,
        text: String? = nil,
        textAttributes: Schema.TextAttributes? = nil,
        textHeading: String? = nil,
        textFooter: String? = nil,
        attachments: [Schema.MessageAttachment]? = nil,
        tweets: [Schema.Tweet]? = nil,
        links: [Schema.MessageLink]? = nil,
        iframeURL: String? = nil,
        reactions: IdentifiedArrayOf<Schema.MessageReaction>? = nil,
        isDelivered: Bool? = nil,
        isHidden: Bool? = nil,
        isSender: Bool,
        seen: Schema.Seen? = nil,
        isAction: Bool? = nil,
        isDeleted: Bool? = nil,
        isErrored: Bool? = nil,
        isDynamicMessage: Bool? = nil,
        parseTemplate: Bool? = nil,
        linkedMessageID: ID? = nil,
        linkedMessage: Schema.MessagePreview? = nil,
        action: Schema.Action? = nil,
        cursor: String? = nil,
        accountID: Schema.AccountID? = nil,
        threadID: Schema.ThreadID? = nil
    ) {
        self.id = id
        self._original = _original
        self.timestamp = timestamp
        self.editedTimestamp = editedTimestamp
        self.expiresInSeconds = expiresInSeconds
        self.senderID = senderID
        self.text = text
        self.textAttributes = textAttributes
        self.textHeading = textHeading
        self.textFooter = textFooter
        self.attachments = attachments
        self.tweets = tweets
        self.links = links
        self.iframeURL = iframeURL
        self.reactions = reactions
        self.isDelivered = isDelivered
        self.isHidden = isHidden
        self.isSender = isSender
        self.seen = seen
        self.isAction = isAction
        self.isDeleted = isDeleted
        self.isErrored = isErrored
        self.isDynamicMessage = isDynamicMessage
        self.parseTemplate = parseTemplate
        self.linkedMessageID = linkedMessageID
        self.linkedMessage = linkedMessage
        self.action = action
        self.cursor = cursor
        self.accountID = accountID
        self.threadID = threadID
    }
}

// MARK: - Conformances

extension Schema.Message: CustomDebugStringConvertible {
    public var debugDescription: String {
        if let text = text {
            return text
        } else {
            return "Message \(id)"
        }
    }
}
