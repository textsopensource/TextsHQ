//
// Copyright (c) Texts HQ
//

import Foundation
import CoreGraphics
import Swallow

public enum Schema {

}

extension Schema {
    public struct Size: Codable, Hashable {
        @PASNumber public var width: Double
        @PASNumber public var height: Double

        public init(width: Double, height: Double) {
            self.width = width
            self.height = height
        }

        public init(cgSize: CGSize) {
            self.width = cgSize.width
            self.height = cgSize.height
        }

        public var cgSize: CGSize {
            .init(width: width, height: height)
        }
    }

    public struct _AccountTag {}
    public typealias AccountID = SchemaID<_AccountTag, String>

    public struct AccountInfo: Encodable {
        public var accountID: AccountID
        public var dataDirPath: String
        public init(accountID: AccountID, dataDirPath: String) {
            self.accountID = accountID
            self.dataDirPath = dataDirPath
        }
    }

    public enum ConnectionStatus: String, Codable, Hashable {
        case conflict
        case connected
        case connecting
        case disconnected
        case unauthorized
        case unknown
    }

    public struct ConnectionState: Codable, Hashable {
        public let status: ConnectionStatus
    }

    public enum Asset: Decodable {
        case url(String)
        case data(Data)

        public init(from decoder: Decoder) throws {
            struct URLCase: Decodable {
                let url: String
            }

            struct DataCase: Decodable {
                let data: Data
            }

            let container = try decoder.singleValueContainer()
            let either = try container.decode(Either<URLCase, DataCase>.self)

            self = either.reduce({ .url($0.url) }, { .data($0.data) })
        }
    }
}

// loosely based on https://github.com/pointfreeco/swift-tagged
public struct SchemaID<Tag, RawValue: Hashable & Codable>: Hashable, Codable {
    public let rawValue: RawValue

    public init(rawValue: RawValue) {
        self.rawValue = rawValue
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()

        try container.encode(rawValue)
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let rawValue = try container.decode(RawValue.self)

        self.init(rawValue: rawValue)
    }
}

extension SchemaID: CustomStringConvertible where RawValue: CustomStringConvertible {
    public var description: String {
        rawValue.description
    }
}

extension SchemaID: ExpressibleByUnicodeScalarLiteral where RawValue: ExpressibleByUnicodeScalarLiteral {
    public init(unicodeScalarLiteral value: RawValue.UnicodeScalarLiteralType) {
        self.init(rawValue: .init(unicodeScalarLiteral: value))
    }
}

extension SchemaID: ExpressibleByExtendedGraphemeClusterLiteral where RawValue: ExpressibleByExtendedGraphemeClusterLiteral {
    public init(extendedGraphemeClusterLiteral value: RawValue.ExtendedGraphemeClusterLiteralType) {
        self.init(rawValue: .init(extendedGraphemeClusterLiteral: value))
    }
}

extension SchemaID: ExpressibleByStringLiteral where RawValue: ExpressibleByStringLiteral {
    public init(stringLiteral value: RawValue.StringLiteralType) {
        self.init(rawValue: .init(stringLiteral: value))
    }
}
