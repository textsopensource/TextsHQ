// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "TextsCore",
    platforms: [
        .iOS(.v14),
    ],
    products: [
        .library(
            name: "TextsBase",
            targets: ["TextsBase"]
        ),
        .library(
            name: "TextsNotifications",
            targets: ["TextsNotifications"]
        ),
        .library(
            name: "TextsCore",
            targets: ["TextsCore"]
        ),
    ],
    dependencies: [
        .package(name: "PASMobile", url: "https://github.com/TextsHQ/platform-api-server-ios.git", .upToNextMinor(from: "25.0.0")),
        .package(url: "https://github.com/apple/swift-collections", from: "1.0.2"),
        .package(url: "https://github.com/TextsHQ/ece-swift.git", .branch("main")),
        .package(url: "https://github.com/apple/swift-algorithms.git", .upToNextMajor(from: "1.0.0")),
        .package(url: "https://github.com/hyperoslo/Cache.git", .upToNextMajor(from: "6.0.0")),
        .package(url: "https://github.com/kishikawakatsumi/KeychainAccess.git", .upToNextMajor(from: "4.2.2")),
        .package(url: "https://github.com/TextsHQ/MessagePacker.git", .branch("master")),
        .package(url: "https://github.com/vmanot/Merge.git", .branch("master")),
        .package(url: "https://github.com/vmanot/Compute.git", .branch("master")),
        .package(url: "https://github.com/vmanot/NetworkKit.git", .branch("master")),
        .package(url: "https://github.com/SDWebImage/SDWebImageSwiftUI.git", .branch("master")),
        .package(name: "Sentry", url: "https://github.com/getsentry/sentry-cocoa.git", .upToNextMajor(from: "7.0.0")),
        .package(url: "https://github.com/vmanot/Swallow.git", .branch("master")),
        .package(url: "https://github.com/SwiftUIX/SwiftUIX.git", .branch("master")),
        .package(url: "https://github.com/pointfreeco/swift-identified-collections", from: "0.3.0"),
        .package(url: "https://github.com/apple/swift-atomics.git", .upToNextMajor(from: "1.0.0")),
    ],
    targets: [
        .target(
            name: "TextsBase",
            dependencies: [
                "MessagePacker",
                "Swallow",
                "Merge",
                "GRDB",
                "NetworkKit",
                .product(name: "IdentifiedCollections", package: "swift-identified-collections"),
            ]
        ),
        .target(
            name: "TextsNotifications",
            dependencies: [
                "TextsBase",
                "KeychainAccess",
                .product(name: "ECE", package: "ece-swift"),
            ]
        ),
        .target(
            name: "TextsCore",
            dependencies: [
                .product(name: "Collections", package: "swift-collections"),
                "TextsBase",
                "TextsNotifications",
                "PASMobile",
                .product(name: "NodeAPI", package: "PASMobile"),
                .product(name: "PASShared", package: "PASMobile"),
                .product(name: "Algorithms", package: "swift-algorithms"),
                .product(name: "IdentifiedCollections", package: "swift-identified-collections"),
                .product(name: "Atomics", package: "swift-atomics"),
                "Cache",
                "KeychainAccess",
                "MessagePacker",
                "Compute",
                "Merge",
                "SDWebImageSwiftUI",
                "Sentry",
                "Swallow",
                "SwiftUIX",
            ]
        ),
        .binaryTarget(
            name: "GRDB",
            url: "https://github.com/1Conan/GRDB.swift/releases/download/v5.25.0/GRDB.xcframework.zip",
            checksum: "8855538977ab9a7d17c2d38f96e817cda6302bff78e815efa590c9856a38e20f"
        )
    ]
)
