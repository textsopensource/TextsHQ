//
// Copyright (c) Texts HQ
//

import os.log

extension Logger {
    init(category: String) {
        self.init(subsystem: "TextsUI", category: category)
    }
}
