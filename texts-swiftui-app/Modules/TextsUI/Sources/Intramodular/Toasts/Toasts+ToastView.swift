import SwiftUI

extension Toasts {
    struct ToastView: View {
        let toast: Toast

        var body: some View {
            HStack {
                Text(LocalizedStringKey(toast.label))
                    .lineLimit(5)
                    .frame(maxWidth: .infinity, alignment: .leading)

                if let undoAction = toast.onUndo {
                    Button("Undo", action: undoAction)
                        .foregroundColor(.secondary)
                }

                CloseIndicator(dismissType: toast.dismissType, onDismiss: toast.dismiss)
            }
            .padding()
            .background(Color(UIColor.tertiarySystemBackground))
            .cornerRadius(12)
        }
    }
}

extension Toasts.ToastView {
    struct CloseIndicator: View {
        let dismissType: Toasts.Toast.DismissType
        let onDismiss: () -> Void

        // TODO: Circular progress with Timeout.after(_)
        var body: some View {
            Button(action: onDismiss) {
                Image(systemName: "xmark")
            }
            .accentColor(.primary)
        }
    }
}
