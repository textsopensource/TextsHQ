import SwiftUI

extension Toasts {
    struct ToastsOverlay: View {
        @ObservedObject var model: Toasts

        @State var dragOffset: CGSize = .zero

        let dragInWrongDirectionMultiplier: CGFloat = 0.05
        let dragThreshold: CGFloat = 30
        let transition: AnyTransition = .asymmetric(insertion: .move(edge: .bottom), removal: .opacity)

        var dragGesture: some Gesture {
            DragGesture()
                .onChanged { drag in
                    dragOffset.height = drag.translation.height > 0 ? drag.translation.height : drag.translation.height * dragInWrongDirectionMultiplier
                    dragOffset.width = drag.translation.width * dragInWrongDirectionMultiplier
                }
                .onEnded { drag in
                    dragOffset = .zero

                    if drag.translation.height > dragThreshold {
                        model.activeToasts.first?.dismiss()
                    }
                }
        }

        var body: some View {
            ZStack {
                ForEach(model.activeToasts.reversed()) { toast in
                    let index = model.activeToasts.firstIndex(of: toast) ?? 0
                    let (scale, offset) = transformForIndex(index)

                    ToastView(toast: toast)
                        .shadow(color: .black.opacity(0.25), radius: 16, x: 0, y: 6)
                        .opacity(Double(scale))
                        .zIndex(-Double(index))
                        .offset(index == 0 ? dragOffset : .zero) // Frontmost drag gesture offset
                        .gesture(index == 0 ? dragGesture : nil) // Frontmost drag gesture
                        .scaleEffect(scale) // Z-scale
                        .offset(x: 0, y: offset) // Z-offset
                        .transition(transition)
                }
            }
            .padding()
            .frame(maxHeight: .infinity, alignment: .bottom)
            .animation(.spring())
            .transition(transition)
        }

        private func transformForIndex(_ index: Int) -> (scale: CGFloat, offset: CGFloat) {
            let scaleMultiplier: CGFloat = 0.1
            let offsetMultiplier: CGFloat = 10

            // 0: 1, 1: 0.9, 2: 0.8...
            let scale: CGFloat = 1 - (CGFloat(index) * scaleMultiplier)

            // 0: 0, 1: -10, 2: -20...
            let offset: CGFloat = -CGFloat(index) * offsetMultiplier

            if index == 0 {
                return (scale, offset)
            } else {
                // Not dragged: 0, dragged >= threshold: >=1
                let dragStage = abs(dragOffset.height / dragThreshold)

                // Change values when dragging so it looks smoother
                let draggedScale = scale + (dragStage * scaleMultiplier)
                let draggedOffset = offset + (dragStage * offsetMultiplier)

                return (draggedScale, draggedOffset)
            }
        }
    }
}
