import Combine
import Foundation

extension Toasts {
    public class Toast: Identifiable, Hashable, ObservableObject {
        public enum DismissType {
            case manual
            case after(TimeInterval)
        }

        public let id: String
        public let label: String
        public let expandedText: String?
        public let dismissType: DismissType
        public let onTap: (() -> Void)?
        public let onUndo: (() -> Void)?

        @Published public var isExpanded: Bool = false {
            didSet { updateTimer() }
        }

        internal var dismiss: (() -> Void)!

        internal var timer: Timer? = nil

        public init(
            id: String,
            label: String,
            expandedText: String? = nil,
            dismissType: Toasts.Toast.DismissType,
            onTap: (() -> Void)? = nil,
            onUndo: (() -> Void)? = nil
        ) {
            self.id = id
            self.label = label
            self.expandedText = expandedText
            self.dismissType = dismissType
            self.onTap = onTap
            self.onUndo = onUndo
        }

        public static func == (lhs: Toasts.Toast, rhs: Toasts.Toast) -> Bool {
            lhs.id == rhs.id
        }

        public func hash(into hasher: inout Hasher) {
            hasher.combine(id)
        }

        internal func updateTimer() {
            guard case .after(let timeout) = self.dismissType, !isExpanded else {
                timer?.invalidate()
                return
            }

            timer?.invalidate()
            timer = Timer.scheduledTimer(withTimeInterval: timeout, repeats: false) { [weak self] _ in
                self?.dismiss()
            }
        }
    }
}
