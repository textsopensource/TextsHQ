import Combine
import Foundation

public class Toasts: ObservableObject {
    public static let shared: Toasts = Toasts()

    @Published internal private(set) var activeToasts: [Toast] = []

    private init() {}

    public func add(_ toast: Toast) {
        toast.dismiss = { [weak self] in
            self?.dismiss(toast)
        }
        toast.updateTimer()
        activeToasts.insert(toast, at: 0)
    }

    internal func dismiss(_ toast: Toast) {
        guard let index = activeToasts.firstIndex(of: toast) else { return }
        toast.timer?.invalidate()
        activeToasts.remove(at: index)
    }
}
