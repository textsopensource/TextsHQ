import SwiftUI

extension View {
    public func toastsOverlay(model: Toasts) -> some View {
        overlay(Toasts.ToastsOverlay(model: model))
    }
}
