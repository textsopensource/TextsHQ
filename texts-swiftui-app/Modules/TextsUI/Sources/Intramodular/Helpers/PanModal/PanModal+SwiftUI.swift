//
// Copyright (c) Stryds Inc.
//

import PanModal
import SwiftUIX
import UIKit

private struct PanPresenter<SheetContent: View>: ViewModifier {
    @Binding var isPresented: Bool

    let content: SheetContent

    @State private var viewController: UIViewController?

    init(
        isPresented: Binding<Bool>,
        @ViewBuilder content: () -> SheetContent
    ) {
        _isPresented = isPresented

        self.content = content()
    }

    func body(content: Content) -> some View {
        content
            .onChange(of: isPresented) { isPresented in
                if isPresented {
                    guard viewController == nil else {
                        return
                    }

                    guard let rootVC = UIApplication.shared.firstKeyWindow?.rootViewController else {
                        return
                    }

                    let viewController = HostingPanModalController(mainView: self.content)

                    viewController._fixSafeAreaInsetsIfNecessary()
                    viewController.onDismiss = { self.isPresented = false }

                    self.viewController = viewController

                    (rootVC.topmostPresentedViewController ?? rootVC).presentPanModal(viewController)
                } else {
                    viewController?.dismiss(animated: true, completion: nil)
                    viewController = nil
                }
            }
    }
}

// MARK: - API -

extension View {
    public func panModal<Sheet: View>(
        isPresented: Binding<Bool>,
        @ViewBuilder content: () -> Sheet
    ) -> some View {
        modifier(
            PanPresenter(
                isPresented: isPresented,
                content: content
            )
        )
    }
}

// MARK: - Auxiliary Implementation

internal class HostingPanModalController<Content: View>: CocoaHostingController<Content>, PanModalPresentable {
    var onDismiss: (() -> Void)?

    private var _cachedShortFormHeight: PanModalHeight?
    private var _cachedLongFormHeight: PanModalHeight?

    var panScrollable: UIScrollView? {
        nil
    }

    var topOffset: CGFloat {
        0
    }

    var shortFormHeight: PanModalHeight {
        if let height = _cachedShortFormHeight {
            return height
        } else {
            let height = view.systemLayoutSizeFitting(UIView.layoutFittingExpandedSize, withHorizontalFittingPriority: .defaultLow, verticalFittingPriority: .defaultLow).height

            if height == Screen.main.bounds.height {
                _cachedShortFormHeight = .contentHeight(height)
            } else if height == UIView.layoutFittingExpandedSize.height {
                _cachedShortFormHeight = .maxHeight
            }

            return .contentHeight(height)
        }
    }

    var longFormHeight: PanModalHeight {
        if let height = _cachedLongFormHeight {
            return height
        } else {
            let height = view.systemLayoutSizeFitting(UIView.layoutFittingExpandedSize, withHorizontalFittingPriority: .defaultLow, verticalFittingPriority: .defaultLow).height

            if height == Screen.main.bounds.height {
                _cachedLongFormHeight = .contentHeight(height)
            } else if height == UIView.layoutFittingExpandedSize.height {
                _cachedLongFormHeight = .maxHeight
            }

            return .contentHeight(height)
        }
    }

    var springDamping: CGFloat {
        1.0
    }

    var panModalBackgroundColor: UIColor {
        .clear
    }

    var showDragIndicator: Bool {
        false
    }

    func panModalDidDismiss() {
        onDismiss?()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        panModalSetNeedsLayoutUpdate()
        panModalTransition(to: .shortForm)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        panModalSetNeedsLayoutUpdate()
        panModalTransition(to: .shortForm)
    }
}
