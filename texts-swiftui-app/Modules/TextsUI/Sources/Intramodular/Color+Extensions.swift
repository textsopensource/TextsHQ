//
// Copyright (c) Texts HQ
//

import SwiftUI

extension Color {
    public static let tweetBackground = Color("Colors/TweetBackground")
}
