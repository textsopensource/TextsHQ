//
// Copyright (c) Texts HQ
//

import os.log
import SwiftUIX
import WebKit
import Merge

public struct DynamicWebView: UIViewRepresentable {
    public enum Page: Hashable {
        case remote(URLRequest)
        case file(URL, readAccess: URL?)
        case html(String, base: URL?)
        case data(Data, mimeType: String, characterEncoding: String, base: URL)

        fileprivate func apply(to webView: WKWebView) {
            switch self {
            case .remote(let request):
                webView.load(request)
            case let .file(url, readAccess):
                webView.loadFileURL(url, allowingReadAccessTo: readAccess ?? url)
            case let .html(string, base):
                webView.loadHTMLString(string, baseURL: base)
            case let .data(data, mimeType, characterEncoding, base):
                webView.load(data, mimeType: mimeType, characterEncodingName: characterEncoding, baseURL: base)
            }
        }
    }

    public struct Script: Hashable {
        var source: String
        var injectionTime: WKUserScriptInjectionTime
        var forMainFrameOnly: Bool

        public init(source: String, injectionTime: WKUserScriptInjectionTime, forMainFrameOnly: Bool) {
            self.source = source
            self.injectionTime = injectionTime
            self.forMainFrameOnly = forMainFrameOnly
        }

        fileprivate var wkScript: WKUserScript {
            .init(source: source, injectionTime: injectionTime, forMainFrameOnly: forMainFrameOnly)
        }
    }

    public typealias ReplyableMessageHandler = (WKScriptMessage, @escaping (Result<Any, Error>) -> Void) -> Void
    public typealias MessageHandler = (WKScriptMessage) -> Void
    public typealias NavigationHandler = (WKWebView, WKNavigation) -> Void

    private static let logger = Logger(category: "DynamicWebView")

    private let start: Page
    private var userAgent: String?
    private var onCommit: [NavigationHandler] = []
    private var onFinish: [NavigationHandler] = []
    private var scripts: [Script] = []
    private var handlers: [String: MessageHandler] = [:]
    private var replyHandlers: [String: ReplyableMessageHandler] = [:]
    public init(start: Page) {
        self.start = start
    }

    public func makeUIView(context: Context) -> WKWebView {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true

        let configuration = WKWebViewConfiguration()
        configuration.websiteDataStore = .nonPersistent()
        configuration.defaultWebpagePreferences = prefs

        let webView = WKWebView(frame: .zero, configuration: configuration)
        webView.navigationDelegate = context.coordinator
        webView.uiDelegate = context.coordinator

        webView.backgroundColor = .clear
        webView.isOpaque = false

        return webView
    }

    public func updateUIView(_ view: WKWebView, context: Context) {
        let coordinator = context.coordinator
        let contentController = view.configuration.userContentController

        view.customUserAgent = userAgent

        if coordinator.scripts != scripts {
            contentController.removeAllUserScripts()
            for script in scripts.map(\.wkScript) {
                contentController.addUserScript(script)
            }
            coordinator.scripts = scripts
        }

        if coordinator.start != start {
            start.apply(to: view)
            coordinator.start = start
        }

        if coordinator.handlers.keys != handlers.keys
            || coordinator.replyHandlers.keys != replyHandlers.keys {
            contentController.removeAllScriptMessageHandlers()
            handlers.keys.forEach { contentController.add(coordinator, name: $0) }
            replyHandlers.keys.forEach { contentController.addScriptMessageHandler(coordinator, contentWorld: .page, name: $0) }
        }
        // we should update the handlers regardless of whether
        // the keys differ, since even if the keys are the same
        // the (closure) values might differ
        coordinator.handlers = handlers
        coordinator.replyHandlers = replyHandlers

        coordinator.onCommit = onCommit
        coordinator.onFinish = onFinish
    }

    public func makeCoordinator() -> Coordinator {
        Coordinator()
    }

    public class Coordinator: NSObject, WKNavigationDelegate, WKScriptMessageHandler, WKScriptMessageHandlerWithReply, WKUIDelegate {
        fileprivate var start: Page?
        fileprivate var onCommit: [NavigationHandler] = []
        fileprivate var onFinish: [NavigationHandler] = []
        fileprivate var scripts: [Script] = []
        fileprivate var handlers: [String: MessageHandler] = [:]
        fileprivate var replyHandlers: [String: ReplyableMessageHandler] = [:]

        public func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
            onCommit.forEach { $0(webView, navigation) }
        }

        public func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
            onFinish.forEach { $0(webView, navigation) }
        }

        public func webView(_: WKWebView, createWebViewWith _: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures _: WKWindowFeatures) -> WKWebView? {
            if navigationAction.navigationType == .other, let url = navigationAction.request.url {
                UIApplication.shared.open(url)
            }
            return nil
        }

        public func userContentController(
            _ userContentController: WKUserContentController,
            didReceive message: WKScriptMessage
        ) {
            logger.debug("Handling message \(message.name)")
            handlers[message.name]?(message)
        }

        public func userContentController(
            _ userContentController: WKUserContentController,
            didReceive message: WKScriptMessage,
            replyHandler: @escaping (Any?, String?) -> Void
        ) {
            logger.debug("Handling replyable message \(message.name)")
            replyHandlers[message.name]?(message) { reply in
                switch reply {
                case .failure(let error):
                    replyHandler(nil, "\(error)")
                case .success(let value):
                    replyHandler(value, nil)
                }
            }
        }
    }
}

extension DynamicWebView {
    public func userAgent(_ value: String?) -> DynamicWebView {
        var copy = self
        copy.userAgent = value
        return copy
    }

    public func onCommit(_ handler: @escaping NavigationHandler) -> DynamicWebView {
        var copy = self
        copy.onCommit.append(handler)
        return copy
    }

    public func onFinish(_ handler: @escaping NavigationHandler) -> DynamicWebView {
        var copy = self
        copy.onFinish.append(handler)
        return copy
    }

    public func script(_ script: Script) -> DynamicWebView {
        var copy = self
        copy.scripts.append(script)
        return copy
    }

    public func handle(_ message: String, handler: @escaping MessageHandler) -> DynamicWebView {
        var copy = self
        copy.handlers[message] = handler
        return copy
    }

    public func handle(_ message: String, replyHandler: @escaping ReplyableMessageHandler) -> DynamicWebView {
        var copy = self
        copy.replyHandlers[message] = replyHandler
        return copy
    }
}
