//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore

public struct DecreasesOnPressButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled

    public init() {}

    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .compositingGroup()
            .opacity(configuration.isPressed ? Globals.Buttons.pressedOpacity : 1)
            .scaleEffect(configuration.isPressed ? Globals.Buttons.pressedSize : 1)
            .animation(.interpolatingSpring(stiffness: 250, damping: 15), value: configuration.isPressed)
            .opacity(isEnabled ? 1.0 : 0.7)
    }
}
