//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore

public struct SmallCandyRectangleButtonStyle: ButtonStyle {
    public let transparent: Bool
    public let color: Color

    public init(transparent: Bool = false, color: Color = .accentColor) {
        self.transparent = transparent
        self.color = color
    }

    @ViewBuilder
    public var background: some View {
        if transparent {
            VisualEffectBlurView(blurStyle: .regular)
        } else {
            color.opacity(Globals.Views.candyOpacity)
        }
    }

    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .font(.subheadline.weight(.semibold))
            .foregroundColor(color)
            .opacity(transparent ? 0.3 : 1)
            .padding(.vertical, 8)
            .padding(.horizontal, 12)
            .background(background)
            .cornerRadius(Globals.Views.smallCornerRadius)
            .opacity(configuration.isPressed ? Globals.Buttons.pressedOpacity : 1)
            .scaleEffect(configuration.isPressed ? Globals.Buttons.pressedSize : 1)
            .animation(.interpolatingSpring(stiffness: 250, damping: 15), value: configuration.isPressed)
    }
}
