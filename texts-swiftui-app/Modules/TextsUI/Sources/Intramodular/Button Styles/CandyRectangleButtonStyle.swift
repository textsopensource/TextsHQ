//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore

public struct CandyRectangleButtonStyle: ButtonStyle {
    public let fullWidth: Bool
    public let color: Color

    public init(fullWidth: Bool = false, color: Color = .accentColor) {
        self.fullWidth = fullWidth
        self.color = color
    }

    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .multilineTextAlignment(.center)
            .font(.headline)
            .foregroundColor(color)
            .padding(.vertical, 18)
            .padding(.horizontal, 20)
            .frame(maxWidth: fullWidth ? .infinity : nil, alignment: .center)
            .background(color.opacity(Globals.Views.candyOpacity))
            .cornerRadius(Globals.Views.cornerRadius)
            // .shadow(color: color, radius: 16, x: 0, y: 4)
            .opacity(configuration.isPressed ? Globals.Buttons.pressedOpacity : 1)
            .scaleEffect(configuration.isPressed ? Globals.Buttons.pressedSize : 1)
            .animation(.interpolatingSpring(stiffness: 250, damping: 15), value: configuration.isPressed)
    }
}
