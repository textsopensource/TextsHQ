//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore

public struct CandyCircleButtonStyle: ButtonStyle {
    public let size: CGFloat
    public let color: Color

    public init(size: CGFloat = Globals.Views.navigationBarCircleSize, color: Color = .accentColor) {
        self.size = size
        self.color = color
    }

    public func makeBody(configuration: Configuration) -> some View {
        configuration.label
            .lineLimit(1)
            .multilineTextAlignment(.center)
            .font(.system(size: size * 0.5, weight: .bold, design: .default))
            .foregroundColor(color)
            .frame(width: size, height: size, alignment: .center)
            .background(color.opacity(Globals.Views.candyOpacity))
            .clipShape(Circle())
            // .shadow(color: color.opacity(0.75), radius: 8, x: 0, y: 4)
            .opacity(configuration.isPressed ? Globals.Buttons.pressedOpacity : 1)
            .scaleEffect(configuration.isPressed ? Globals.Buttons.pressedSize : 1)
            .animation(.interpolatingSpring(stiffness: 250, damping: 15), value: configuration.isPressed)
    }
}
