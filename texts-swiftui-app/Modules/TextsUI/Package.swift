// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "TextsUI",
    platforms: [
        .iOS(.v14)
    ],
    products: [
        .library(
            name: "TextsUI",
            targets: ["TextsUI"]
        )
    ],
    dependencies: [
        .package(url: "https://github.com/slackhq/PanModal", .upToNextMajor(from: "1.2.7")),
        .package(url: "https://github.com/SwiftUIX/SwiftUIX", .branch("master")),
        .package(name: "TextsCore", path: "../TextsCore"),
    ],
    targets: [
        .target(
            name: "TextsUI",
            dependencies: [
                "PanModal",
                "SwiftUIX",
                "TextsCore"
            ],
            path: "Sources"
        )
    ]
)
