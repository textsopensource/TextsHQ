# Texts iOS

## Building
Simply open the Xcode project and build the Texts target.

## Linting and formatting
Xcode project already has a `Run Script` phase, linting all files. **To enable automatic formatting, run `cp pre-commit.sample .git/hooks/pre-commit`.**
- Lint with `swiftlint`
- Format with `swiftlint autocorrect [--format]`
