import TextsNotifications
import TextsBase
import UserNotifications
import Intents

class NotificationService: UNNotificationServiceExtension {

    private var task: Task<Void, Never>?

    // the task will be cancelled when serviceExtensionTimeWillExpire is called
    private func handle(request: UNNotificationRequest) async -> UNNotificationContent {
        let payload: NotificationPayload
        do {
            payload = try NotificationPayload(content: request.content)
        } catch {
            // we don't know what this notification is; filter it out
            return UNNotificationContent()
        }
        // swiftlint:disable:next force_cast
        let modified = await payload.modifiedContent().mutableCopy() as! UNMutableNotificationContent
        modified.userInfo = request.content.userInfo

        let props = try? AppDatabase.getThreadProps(payload.anyPayload.threadID, accountID: payload.anyPayload.accountID)
        if props?.isMuted == true {
            modified.sound = nil
        }

        // swiftlint:disable:next force_cast
        return modified.copy() as! UNNotificationContent
    }

    override func didReceive(
        _ request: UNNotificationRequest,
        withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void
    ) {
        task = Task {
            let content = await handle(request: request)
            contentHandler(content)
        }
    }

    override func serviceExtensionTimeWillExpire() {
        task?.cancel()
    }

}
