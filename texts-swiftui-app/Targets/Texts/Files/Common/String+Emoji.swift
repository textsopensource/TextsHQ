//
// Copyright (c) Texts HQ
//

import Foundation

extension String {
    var containsOnlyEmoji: Bool {
        !isEmpty && firstIndex(where: { !$0.isEmoji }) == nil
    }
}

extension Character {
    var isSimpleEmoji: Bool {
        guard let firstScalar = unicodeScalars.first else { return false }
        return firstScalar.properties.isEmoji && firstScalar.value > 0x238C
    }

    var isCombinedIntoEmoji: Bool { unicodeScalars.count > 1 && unicodeScalars.first?.properties.isEmoji ?? false }

    var isEmoji: Bool { isSimpleEmoji || isCombinedIntoEmoji }
}
