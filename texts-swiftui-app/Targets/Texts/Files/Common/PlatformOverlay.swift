//
// Copyright (c) Texts HQ
//

import SVGView
import SwiftUIX
import TextsCore

struct PlatformOverlay: View {
    let account: AccountInstance?

    var body: some View {
        if let path = account?.platform.icon {
            SVGView(string: path)
                .shadow(color: .black.opacity(0.1), radius: 4, x: 0, y: 1)
                .frame(
                    width: Avatar.Size.threadIcon.rawValue.width / 2.5,
                    height: Avatar.Size.threadIcon.rawValue.height / 2.5
                )
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottomTrailing)
                .padding(-2)
                .id(path)
        }
    }
}
