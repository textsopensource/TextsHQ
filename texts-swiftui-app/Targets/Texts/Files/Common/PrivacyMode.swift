//
// Copyright (c) Texts HQ
//

import TextsCore
import SwiftUI
import UIKit
import MessageKit

extension View {
    @ViewBuilder
    func redactInPrivacyMode() -> some View {
        if #available(iOS 15, *), Preferences.shared.isPrivacyForScreenCaptureEnabled {
            self.redacted(reason: .placeholder)
        } else {
            self
        }
    }
}

extension UIView {
    func redactInPrivacyMode() -> Self {
        // Requires view to be loaded after this setting is set to true
        // This saves a ton of overhead for normal app use
        guard Preferences.shared.isPrivacyForScreenCaptureEnabled else { return self }

        updateForPrivacyMode()
        AppState.shared.objectWillChange.makeConnectable().autoconnect().sink { [weak self] in
            DispatchQueue.main.asyncAfter(deadline: .now() + .leastNonzeroMagnitude) { [weak self] in
                self?.updateForPrivacyMode()
            }
        }.store(in: AppState.shared.cancellables)

        return self
    }

    var blurContentView: UIView {
        if let cell = self as? UICollectionViewCell {
            if let messageContainer = cell.contentView.subviews.first(where: { $0 is MessageContainerView }) {
                return messageContainer
            } else {
                return cell.contentView
            }
        } else {
            return self
        }
    }

    private var viewTag: Int { 05648 } // Leet 'OSCAR' with some imagination

    var blurView: UIView? {
        blurContentView.subviews.first { $0.tag == viewTag }
    }

    func updateForPrivacyMode() {
        let isPrivacyEnabled = Preferences.shared.isPrivacyForScreenCaptureEnabled

        if !isPrivacyEnabled, let existing = blurView {
            existing.removeFromSuperview()
        }
        if isPrivacyEnabled, blurView == nil {
            let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .systemThickMaterial))
            blurView.tag = viewTag
            blurView.frame = blurContentView.bounds
            blurView.clipsToBounds = true
            blurView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            blurContentView.addSubview(blurView)
        }
    }
}
