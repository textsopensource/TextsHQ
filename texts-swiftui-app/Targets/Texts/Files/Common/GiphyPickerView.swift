//
// Copyright (c) Texts HQ
//

import GiphyUISDK
import SwiftUI
import UIKit

struct GiphyPickerView: View {
    // TODO: Replace with production-ready key
    private static let apiKey = "8003D0ZPWSqYTlupHXpcCocdIraLw0xg"
    private static let configure: Void = {
        Giphy.configure(apiKey: Self.apiKey)
    }()

    @Binding var isPresented: Bool
    var onPick: (GPHMedia) -> Void

    // swiftlint:disable:next weak_delegate
    @State private var giphyDelegate = GiphyPickerDelegate()
    @State private var vc: GiphyViewController?

    var body: some View {
        ZStack {
            Color.clear
        }
        .frame(width: 1, height: 1)
        .onAppear {
            isPresented ? show() : hide()
        }
        .onChange(of: isPresented) {
            $0 ? show() : hide()
        }
    }

    private func hide() {
        vc?.dismiss(animated: true, completion: nil)
        vc = nil
    }

    private func show() {
        guard vc == nil else { return }

        _ = Self.configure

        // Because Giphy doesn't work well with SwiftUI
        // we need to do manually present it in a UIKit-manner.
        // As stated by one of Giphy developer (link below), they are
        // working on SwiftUI support so we should update this code when they release it.
        // https://github.com/Giphy/giphy-ios-sdk/issues/164#issuecomment-892779686
        let rootVC = UIApplication.shared.firstKeyWindow?.rootViewController
        let vc = GiphyViewController()
        vc.mediaTypeConfig = [.gifs, .recents]
        vc.shouldLocalizeSearch = true
        vc.swiftUIEnabled = true
        vc.delegate = giphyDelegate

        giphyDelegate.onDismiss = {
            isPresented = false
            self.vc = nil
        }
        giphyDelegate.onPick = onPick

        self.vc = vc
        rootVC?.present(vc, animated: true, completion: nil)
    }
}

private class GiphyPickerDelegate: GiphyDelegate {
    var onDismiss: (() -> Void)?
    var onPick: ((GPHMedia) -> Void)?

    func didSelectMedia(giphyViewController _: GiphyViewController, media: GPHMedia, contentType _: GPHContentType) {
        onPick?(media)
    }

    func didDismiss(controller _: GiphyViewController?) {
        onDismiss?()
    }
}

struct GiphyView_Previews: PreviewProvider {
    static var previews: some View {
        GiphyPickerView(isPresented: .constant(true), onPick: { _ in })
    }
}
