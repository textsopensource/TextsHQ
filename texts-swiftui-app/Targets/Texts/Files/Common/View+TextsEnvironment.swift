//
// Copyright (c) Texts HQ
//

import SwiftUI
import TextsCore

struct InjectTextsEnvironment: ViewModifier {
    func body(content: Content) -> some View {
        content
            .environmentObject(AppState.shared)
            .environmentObject(Preferences.shared)
            .environmentObject(AuthorizationManager.shared)
            .onOpenURL { url in
                Task { @MainActor in
                    await (UIApplication.shared.delegate as? AppDelegate)?.openURLWithInternalSchemes(url)
                }
            }
    }
}

extension View {
    func injectingTextsEnvironment() -> some View {
        modifier(InjectTextsEnvironment())
    }
}
