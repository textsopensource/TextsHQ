//
// Copyright (c) Texts HQ
//

import Foundation
import WebKit

extension WKWebView {

    func loadMessageAttachment(url: URL) {
        loadHTMLString(
            """
            <!DOCTYPE html>
            <html>
            <style>
                body {
                    background-color: transparent;
                    margin: 0;
                    padding: 0;
                }
                img {
                    display: block;
                    width: 100%;
                    height: 100%;
                    object-fit: fill;
                }
            </style>
            <img src='\(url.absoluteString)'/>
            </html>
            """,
            baseURL: nil
        )
    }

    func loadLocalAttachment(type: String, base64: String) {
        loadHTMLString(
            """
            <!DOCTYPE html>
            <html>
            <style>
                body {
                    background-color: transparent;
                    margin: 0;
                    padding: 0;
                }
                img {
                    display: block;
                    width: 100vw;
                    height: 100vh;
                    object-fit: fill;
                }
            </style>
            <img src='data:\(type);base64,\(base64)'/>
            </html>
            """,
            baseURL: nil
        )
    }
}
