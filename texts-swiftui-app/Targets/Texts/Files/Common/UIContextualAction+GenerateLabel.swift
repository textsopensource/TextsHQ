//
// Copyright (c) Texts HQ
//

import UIKit

extension UIContextualAction {

    fileprivate static var composedImageCache = [String: UIImage]()

    /// UIContextualAction doesn't support image + text, so we need to generate an image with the label included to have both.
    /// This image is memory cached uniquing on image name + text to improve performance.
    func setImage(_ systemName: String, with label: String) {
        let cacheKey = label + "-" + systemName
        if let composedImage = Self.composedImageCache[cacheKey] {
            image = composedImage
        } else {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            paragraphStyle.allowsDefaultTighteningForTruncation = true
            paragraphStyle.lineSpacing = 2
            let font = UIFont.preferredFont(forTextStyle: .caption1).fontDescriptor.withSymbolicTraits(.traitBold).flatMap { UIFont(descriptor: $0, size: 0) }
            let labelAttributes: [NSAttributedString.Key: Any] = [.font: font!, .paragraphStyle: paragraphStyle]
            let labelSize = NSString(label).boundingRect(
                with: CGSize(width: 60, height: 100),
                options: [.usesFontLeading, .usesLineFragmentOrigin],
                attributes: labelAttributes,
                context: nil
            )
            let icon = UIImage(systemName: systemName, withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .medium))!
            let size = CGSize(width: 60, height: 80)
            let renderedImage = UIGraphicsImageRenderer(size: size).image { context in
                icon.draw(in: CGRect(
                    x: size.width / 2 - icon.size.width / 2,
                    y: size.height / 2 - icon.size.height - 1,
                    width: icon.size.width,
                    height: icon.size.height
                ))
                NSString(label).draw(
                    in: CGRect(
                        x: size.width / 2 - labelSize.width / 2,
                        y: size.height / 2 + 1,
                        width: labelSize.width,
                        height: labelSize.height
                    ),
                    withAttributes: labelAttributes
                )
            }
            Self.composedImageCache[cacheKey] = renderedImage.withRenderingMode(.alwaysTemplate)
            image = Self.composedImageCache[cacheKey]
        }
    }
}
