//
// Copyright (c) Texts HQ
//

import UIKit

extension UIColor {
    static let accentColor = UIColor(named: "AccentColor")!
}
