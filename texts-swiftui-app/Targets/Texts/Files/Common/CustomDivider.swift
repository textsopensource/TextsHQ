//
// Copyright (c) Texts HQ
//

import SwiftUI

struct CustomDivider: View {
    var body: some View {
        RoundedRectangle(cornerRadius: 22, style: .continuous)
            .fill(Color.secondary.opacity(0.1))
            .frame(height: 3)
            .frame(maxWidth: .infinity, alignment: .center)
    }
}
