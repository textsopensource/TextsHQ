//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore

struct ButtonLabel: View {
    private let icon: String?
    private let label: String
    private let subtitle: String?
    private let color: Color
    private let showIndicator: Bool

    public init(icon: String? = nil, label: String, subtitle: String? = nil, color: Color = Color.accentColor, showIndicator: Bool = false) {
        self.icon = icon
        self.label = label
        self.subtitle = subtitle
        self.color = color
        self.showIndicator = showIndicator
    }

    var body: some View {
        HStack {
            if let icon = icon {
                Image(systemName: icon)
                    .font(.system(size: 15, weight: .heavy, design: .default))
                    .foregroundColor(.accentColor)
                    .padding(6)
                    .background(Color.accentColor.opacity(Globals.Views.candyOpacity))
                    .cornerRadius(10)
            }

            VStack(alignment: .leading, spacing: 0) {
                Text(LocalizedStringKey(label))
                    .font(.headline)
                    .foregroundColor(.primary)

                if let subtitle = subtitle {
                    Text(LocalizedStringKey(subtitle))
                        .font(.subheadline)
                        .foregroundColor(.primary)
                        .opacity(Globals.Views.secondaryOpacity)
                }
            }

            Spacer()

            if showIndicator {
                Image(systemName: "chevron.right")
                    .font(.system(size: 13, weight: .heavy, design: .rounded))
                    .foregroundColor(.secondary)
                    .opacity(Globals.Views.secondaryOpacity)
            }
        }
        .contentShape(Rectangle())
    }
}
