//
// Copyright (c) Texts HQ
//

import SwiftUI
import TextsCore

struct ToggleOption: View {
    @Binding var isOn: Bool
    let title: String
    let description: String?

    var body: some View {
        HStack {
            VStack(alignment: .leading, spacing: 4) {
                Text(LocalizedStringKey(title))
                    .font(.body)
                    .lineLimit(2)

                if let description = description {
                    Text(LocalizedStringKey(description))
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                        .lineLimit(4)
                }
            }

            Spacer()

            Toggle(LocalizedStringKey(title), isOn: $isOn)
                .labelsHidden()
                .toggleStyle(SwitchToggleStyle(tint: .accentColor))
                .frame(maxWidth: 80)
        }
        .frame(maxWidth: .infinity)
        .padding(.vertical, 2)
    }
}
