//
// Copyright (c) Texts HQ
//

import SDWebImageSwiftUI
import TextsCore
import SwiftUI

struct Avatar: View {
    public typealias Size = AvatarSize

    let imageURL: String?
    let size: Size
    let placeholderSymbolName: String

    public init(
        imageURL: String?,
        size: Size,
        placeholderSymbolName: String = "person.fill"
    ) {
        self.imageURL = imageURL
        self.size = size
        self.placeholderSymbolName = placeholderSymbolName
    }

    var body: some View {
        if let imageURL = imageURL {
            WebImage(url: URL(string: imageURL))
                .resizable()
                .placeholder {
                    if case .tweetAuthor = size {
                        Image("twitter-icon-blue")
                            .sizeToFitSquare(sideLength: Avatar.Size.tweetAuthor.rawValue.width)
                    } else {
                        Circle().fill(messagesAppAvatarGradient)
                    }
                }
                .aspectRatio(contentMode: .fit)
                .scaledToFit()
                .clipShape(Circle())
                .frame(size.rawValue)
        } else {
            Image(systemName: placeholderSymbolName)
                .foregroundColor(.white)
                .font(.system(
                    size: size.rawValue.height - (placeholderSymbolName.contains("bell") ? size.height * 0.5 : size.height * 0.33),
                    weight: .bold
                ))
                .frame(
                    width: size.rawValue.width, height: size.rawValue.height,
                    alignment: placeholderSymbolName.contains("bell") ? .center : .bottom
                )
                .background(Circle().fill(messagesAppAvatarGradient))
                .overlay(Circle().strokeBorder(messagesAppAvatarGradient, lineWidth: 3))
                .clipShape(Circle())
                .compositingGroup()
        }
    }

    private var messagesAppAvatarGradient: SwiftUI.LinearGradient {
        LinearGradient(
            gradient: Gradient(
                colors: [
                    Color(hexadecimal: "C1C5CF"),
                    Color(hexadecimal: "848994")
                ]
            ),
            startPoint: .top, endPoint: .bottom
        )
    }
}
