//
// Copyright (c) Vatsal Manot
//

import TextsCore
import TextsUI

struct ThreadPlaceholderIcon: View {
    let thread: ThreadStore

    var systemImageName: String {
        switch thread.type {
        case _ where thread.isTwitterNotifications:
            return thread.isUnread ? "bell.badge.fill" : "bell.fill"
        case .broadcast:
            return "megaphone.fill"
        case .channel, .group:
            switch thread.participants.count {
            case 0, 1:
                return "person.fill"
            case 2:
                return "person.2.fill"
            default:
                return "person.3.fill"
            }
        case .single:
            return "person.fill"
        }
    }

    var body: some View {
        Image(systemName: systemImageName)
            .sizeToFit()
    }
}
