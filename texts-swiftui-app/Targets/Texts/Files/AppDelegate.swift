//
// Copyright (c) Texts HQ
//

import UIKit

import TextsCore

import os.log
import Sentry

import Coordinator
import Merge
import TextsUI
import SwiftUIX

@main class AppDelegate: UIResponder, UIApplicationDelegate, PushDelegate {
    var appState = AppState.shared
    var authorizationManager = AuthorizationManager.shared
    var platformServer = PlatformServer.shared
    var preferences = Preferences.shared

    static var shared: AppDelegate {
        UIApplication.shared.delegate as? AppDelegate ?? AppDelegate()
    }

    private let logger = Logger(category: "AppDelegate")

    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        #if INDEXDB_ENABLED
        do {
            let platforms = try AppDatabase.getPlatformInfo()
            AppState.shared.rootStore.platformsStore.initialize(with: platforms)
        } catch {
            AppState.shared.handleError(error)
        }
        AppState.shared.rootStore.accountsStore.loadAccounts()
        #endif

        PlatformServer.shared.start()

        _ = PushNotificationManager.shared
        PushNotificationManager.shared.delegate = self

        if let dsn = Bundle.main.object(forInfoDictionaryKey: "SentryDSN") as? String {
            let options = Sentry.Options()
            options.dsn = dsn
            #if DEBUG
            // options.debug = true
            #endif
            SentrySDK.start(options: options)
        }

        // UserDefaults.standard.set(true, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")

        return true
    }

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        let configuration = UISceneConfiguration(name: nil, sessionRole: connectingSceneSession.role)
        if connectingSceneSession.role == .windowApplication {
            configuration.delegateClass = SceneDelegate.self
        }
        return configuration
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        PushNotificationManager.shared.finishRegistration(with: .success(deviceToken))
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        PushNotificationManager.shared.finishRegistration(with: .failure(error))
    }

    func openURLWithInternalSchemes(_ url: URL) async {
        guard url.scheme == "texts" else {
            return
        }

        switch url.host {
        case "auth":
            do {
                try await authorizationManager.authenticateWithURL(url)
            } catch {
                appState.handleError(error)
            }

//        case "thread":
//            guard url.pathComponents.count == 2, url.pathComponents[0] == "/" else {
//                return
//            }
//
//            self.showThread(with: .init(rawValue: url.pathComponents[1]), in: nil)

        default:
            break
        }
    }

    // MARK: PushDelegate

    var visibleThreadIds: Set<Schema.ThreadID> {
        guard let mainViewController = UIApplication.shared.connectedScenes
            .compactMap({ $0 as? UIWindowScene })
            .flatMap(\.windows)
            .first(where: \.isKeyWindow)?
            .rootViewController as? MainViewController
        else {
            return .init()
        }

        let threads = mainViewController.inboxViewController.collectionView.indexPathsForVisibleItems
            .map { indexPath in
                mainViewController.inboxViewController.dataSource.itemIdentifier(for: indexPath)
            }
            .compactMap { identifier -> ThreadStore? in
                switch identifier {
                case .thread(let inboxThread), .pin(let inboxThread):
                    return inboxThread.thread
                default:
                    return nil
                }
            }

        return Set(threads.map(\.id))
    }

    var deferredThreadId: String?

    func showThread(with threadUniqueId: String, in scene: UIScene?) {
        let mainViewController: MainViewController?
        if let scene = scene as? UIWindowScene {
            mainViewController = scene.windows
                .first { $0.isKeyWindow }?
                .rootViewController as? MainViewController
        } else {
            mainViewController = UIApplication.shared.connectedScenes
                .compactMap { $0 as? UIWindowScene }
                .flatMap(\.windows)
                .first(where: \.isKeyWindow)?
                .rootViewController as? MainViewController
        }

        guard let mainViewController = mainViewController else {
            deferredThreadId = threadUniqueId
            print("Could not show thread: No MainViewController - Deferring.")
            return
        }
        deferredThreadId = nil

        let (rawAccountID, rawThreadID) = threadUniqueId.splitInHalf(separator: ":")
        let accountID = Schema.AccountID(rawValue: rawAccountID)
        let threadID = Schema.ThreadID(rawValue: rawThreadID)

        if PlatformServer.shared.status == .launched,
           let account = AppState.shared.rootStore.accountsStore.accounts[id: accountID],
           let thread = account.currentAccountStore?.getThreadFromAllInboxes(threadID) {
            present(thread: thread, from: mainViewController)
        } else {
            waitForThread(with: threadUniqueId) { thread in
                self.present(thread: thread, from: mainViewController)
            }
        }
    }

    func showThread(thread: ThreadStore) {
        guard let mainViewController = UIApplication.shared.connectedScenes
            .compactMap({ $0 as? UIWindowScene })
            .flatMap(\.windows)
            .first(where: \.isKeyWindow)?
            .rootViewController as? MainViewController
        else {
            return
        }

        present(thread: thread, from: mainViewController)
    }

    func present(thread: ThreadStore, from mainViewController: MainViewController) {
        if let presentedViewController = mainViewController.inboxViewController.presentedViewController {
            print("Dismissing modal (settings?) and recursing")
            presentedViewController.dismiss(animated: true) {
                self.present(thread: thread, from: mainViewController)
            }
            return
        }

        let viewController = ThreadViewController(viewModel: ThreadViewModel(thread: thread))
        mainViewController.showDetailViewController(viewController, sender: mainViewController.inboxViewController)
    }

    private func waitForThread(with threadUniqueId: String, onCompletion: @escaping (ThreadStore) -> Void) {
        var cancellable: Cancellable?

        cancellable = AppState.shared
            .rootStore
            .accountsStore
            .$displayThreads
            .sink { threads in
                guard let thread = AppState.shared.rootStore.accountsStore.displayThreads[id: threadUniqueId] else {
                    return
                }

                DispatchQueue.main.async {
                    onCompletion(thread)
                }

                cancellable?.cancel()
            }
    }
}
