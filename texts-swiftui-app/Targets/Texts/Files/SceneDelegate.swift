//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore
import Combine

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    var isLoggedIn = AuthorizationManager.shared.isLoggedIn
    var wasSuspended = false

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let scene = scene as? UIWindowScene else { return }

        window = UIWindow(windowScene: scene)
        let viewController: UIViewController
        if isLoggedIn {
            viewController = MainViewController()
        } else {
            viewController = UIHostingController(
                rootView: LandingView().injectingTextsEnvironment()
            )
        }
        window!.rootViewController = viewController
        window!.makeKeyAndVisible()

        observeAuthorization()
    }

    private var authChangeObserver: AnyCancellable?

    func observeAuthorization() {
        authChangeObserver = AuthorizationManager.shared.objectWillChange.sink { [weak self] _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + .leastNonzeroMagnitude) { [weak self] in
                if AuthorizationManager.shared.isLoggedIn != self?.isLoggedIn {
                    self?.handleAuthorizationChanged()
                }
            }
        }
    }

    func handleAuthorizationChanged() {
        isLoggedIn = AuthorizationManager.shared.isLoggedIn

        let viewController: UIViewController
        if isLoggedIn {
            viewController = MainViewController()
            if let deferredThreadId = AppDelegate.shared.deferredThreadId {
                AppDelegate.shared.showThread(with: deferredThreadId, in: window?.windowScene)
            }
        } else {
            viewController = UIHostingController(
                rootView: LandingView().injectingTextsEnvironment()
            )
        }

        window!.rootViewController = viewController
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        authChangeObserver?.cancel() // TODO: Is this correct?
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        Task {
            let now = Date().timeIntervalSinceReferenceDate
            let lastCheckedAuthorization = (AuthorizationManager.shared.lastCheckedAuthorization ?? Date(timeIntervalSinceReferenceDate: 0)).timeIntervalSinceReferenceDate

            if (now - lastCheckedAuthorization) >= 60 * 30 {
                await AuthorizationManager.shared.validateSession()
            }
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {

    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        guard wasSuspended else { return }
        wasSuspended = false
        Task {
            try await AppState.shared.rootStore.accountsStore.reconnectAllAccounts()
        }
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        Timer.scheduledTimer(
            withTimeInterval: UIApplication.shared.backgroundTimeRemaining - 1,
            repeats: false
        ) { _ in
            self.wasSuspended = true
        }
    }
}
