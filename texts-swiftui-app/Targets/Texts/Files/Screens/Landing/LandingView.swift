//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsUI
import Merge

// TODO: Unify this with ManualLoginView
struct LandingView: View {
    @State private var status: AnyTask<Void, Error>.Status = .idle
    @ViewStorage private var cancellables = Cancellables()

    var error: Error? {
        switch status {
        case .error(AppAuthError.cancelled):
            return nil
        case .error(let error):
            return error
        default:
            return nil
        }
    }

    var showSpinner: Bool {
        switch status {
        case .idle, .error, .success:
            return true
        default:
            return false
        }
    }

    var body: some View {
        VStack {
            Spacer()

            Group {
                Image("Texts")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 140, height: 140)

                Text("Texts")
                    .font(.largeTitle.weight(.bold))

                Text("%ABOUT_DESCRIPTION%")
                    .font(.title2.weight(.semibold))
                    .multilineTextAlignment(.center)
                    .foregroundColor(.secondary)
            }

            Spacer()

            if let error = error {
                Text("Error: \("\(error)")")
                    .foregroundColor(.red)
            }

            DynamicActionButton(action: AppAuthAction(enableHaptics: true) { task in
                task.objectWillChange.map {
                    self.status = $0
                }.subscribe(in: cancellables)
                task.successPublisher.subscribe(in: cancellables)
            }) {
                HStack {
                    Text("Continue with Google")
                        .fixedSize()

                    if !showSpinner {
                        Spacer().fixedSize()
                        ProgressView().fixedSize()
                    }
                }
            }
            .buttonStyle(CandyRectangleButtonStyle(fullWidth: true))
            .disabled(!showSpinner || status == .success)
        }
        .animation(.default.speed(2), value: showSpinner)
        .padding()
    }
}

struct LandingView_Previews: PreviewProvider {
    static var previews: some View {
        LandingView()
    }
}
