//
// Copyright (c) Texts HQ
//

import AuthenticationServices
import os.log
import SwiftUIX
import TextsCore
import Merge

private class ContextProvider: NSObject, ASWebAuthenticationPresentationContextProviding, ObservableObject {
    func presentationAnchor(for _: ASWebAuthenticationSession) -> ASPresentationAnchor {
        UIApplication.shared.firstKeyWindow ?? ASPresentationAnchor()
    }
}

enum AppAuthError: Error {
    case unknownAuthError
    case cancelled
}

struct AppAuthAction: DynamicAction {
    let enableHaptics: Bool
    let onStart: (AnyTask<Void, Error>) -> Void

    @StateObject private var contextProvider = ContextProvider()
    @EnvironmentObject var authorizationManager: AuthorizationManager

    let logger = Logger(
        subsystem: Bundle.main.bundleIdentifier!,
        category: "AuthAction"
    )

    init(enableHaptics: Bool, onStart: @escaping (AnyTask<Void, Error>) -> Void = { _ in }) {
        self.enableHaptics = enableHaptics
        self.onStart = onStart
    }

    func perform() {
        if enableHaptics {
            UIDevice.current.generateHaptic(.light)
        }

        let publisher = Future<URL, Error> { promise in
            let session = ASWebAuthenticationSession(
                url: URL(string: "https://texts.com/auth/google")!,
                callbackURLScheme: "texts"
            ) { url, error in
                if let url = url {
                    promise(.success(url))
                } else if let error = error as? ASWebAuthenticationSessionError, error.code == .canceledLogin {
                    promise(.failure(AppAuthError.cancelled))
                } else {
                    promise(.failure(error ?? AppAuthError.unknownAuthError))
                }
            }
            session.presentationContextProvider = contextProvider
            session.start()
        }
        .flatMap { url in
            Task { @MainActor in
                try await authorizationManager.authenticateWithURL(url)
            }
            .publisher()
            .receiveOnMainQueue()
        }
        .share()

        publisher.subscribe(in: AppState.shared.cancellables)

        onStart(publisher.convertToTask())
    }
}
