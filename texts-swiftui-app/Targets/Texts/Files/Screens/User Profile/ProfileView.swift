//
// Copyright (c) Texts HQ
//

import SwiftUIX
import TextsCore
import PhoneNumberKit

struct ProfileView: View {
    let thread: ThreadStore
    // had to add metadataCallback so it wont crash with `unimplemented initializer 'init()'`
    let phoneNumberKit = PhoneNumberKit(metadataCallback: PhoneNumberKit.defaultMetadataCallback)

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 0) {
                header

                CustomDivider()

                detail
            }
        }
    }

    private var header: some View {
        HStack {
            Avatar(
                imageURL: thread.getAvatarURL(),
                size: .profileViewIcon,
                placeholderSymbolName: ThreadPlaceholderIcon(thread: thread).systemImageName
            )

            VStack(alignment: .leading) {
                HStack(spacing: 5) {
                    Text(thread.name)
                        .font(.title.weight(.bold))
                        .lineLimit(2)
                        .minimumScaleFactor(0.75)

                    if thread.type == .single && thread.participants.contains(where: { $0.isVerified == true }) {
                        Image(systemName: "checkmark.seal.fill")
                            .font(.title3)
                    }
                }

                if let username = thread.displayParticipant?.username, thread.type != .group {
                    Text("@\(username)")
                        .font(.body.weight(.medium))
                        .foregroundColor(.secondary)
                        .lineLimit(1)
                }
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding(.horizontal)
        .padding(.vertical, 15)
    }

    private var detail: some View {
        VStack(spacing: 20) {
            if thread.type == .group {
                VStack(alignment: .leading, spacing: 10) {
                    Text("Participants")
                        .font(.title2.weight(.bold))
                        .padding(.bottom, 2)

                    ForEach(thread.participants) { participant in
                        HStack {
                            Avatar(imageURL: participant.imgURL, size: .profileViewParticipant)

                            VStack(alignment: .leading, spacing: 0) {
                                if let fullName = participant.fullName ?? participant.nickname {
                                    Text(fullName)
                                        .font(.headline)
                                }

                                if let username = participant.username {
                                    Text("@\(username)")
                                        .font(.subheadline.weight(.medium))
                                        .foregroundColor(.secondary)
                                }

                                if let phoneNumber = participant.phoneNumber,
                                   let parsedPhoneNumber = try? phoneNumberKit.parse(phoneNumber) {
                                    Text(phoneNumberKit.format(parsedPhoneNumber, toType: .international))
                                        .font(.subheadline.weight(.medium))
                                        .foregroundColor(.secondary)
                                }

                                if let email = participant.email {
                                    Text(email)
                                        .font(.subheadline.weight(.medium))
                                        .foregroundColor(.secondary)
                                }
                            }
                            .frame(maxWidth: .infinity, alignment: .leading)
                        }
                    }
                }
            }

            Spacer()
        }
        .padding(.horizontal)
        .padding(.vertical, 15)
    }
}
