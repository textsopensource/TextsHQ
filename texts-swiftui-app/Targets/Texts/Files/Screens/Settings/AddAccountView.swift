//
// Copyright (c) Texts HQ
//

import SVGView
import TextsCore
import TextsUI

struct AddAccountView: View {
    @EnvironmentObject var appState: AppState

    @ObservedObject var platformServer = PlatformServer.shared

    @Binding var isShowingAddAccountView: Bool

    @State private var platformToAuthenticate: Schema.PlatformInfo? = nil

    var body: some View {
        List {
            ForEach(appState.rootStore.platformsStore.platforms) { platform in
                Button {
                    platformToAuthenticate = platform
                } label: {
                    HStack(spacing: 15) {
                        if let icon = platform.icon {
                            SVGView(string: icon)
                                .frame(width: 24, height: 24)
                        }

                        VStack(alignment: .leading, spacing: 2) {
                            Text(platform.displayName)
                                .foregroundColor(.primary)

                            HStack {
                                ForEach(platform.tags ?? [], id: \.self) { tag in
                                    Text(tag)
                                        .font(.footnote.weight(.semibold))
                                        .foregroundColor(Color.accentColor)
                                        .padding(.vertical, 4)
                                        .padding(.horizontal, 8)
                                        .background(Color.accentColor.opacity(Globals.Views.candyOpacity))
                                        .cornerRadius(Globals.Views.smallCornerRadius - 1)
                                }
                            }
                        }
                        .frame(maxWidth: .infinity, alignment: .leading)
                    }
                    .padding(.vertical, .small)
                }
            }
        }
        .navigationTitle("Add Account")
        .sheet(item: $platformToAuthenticate) { platform in
            AuthenticationView(
                isShowingAddAccountView: $isShowingAddAccountView,
                platform: platform
            )
        }
    }
}

// MARK: - Development Preview -

struct AddAccountView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            AddAccountView(isShowingAddAccountView: .constant(false))
                .environmentObject(AppState.shared)
        }
    }
}
