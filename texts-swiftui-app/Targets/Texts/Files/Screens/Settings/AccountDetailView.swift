//
// Copyright (c) Texts HQ
//

import Merge
import TextsCore
import TextsUI
import SVGView
import SwiftUI
import Swallow

struct AccountDetailView: View {
    @Environment(\.presentationManager) var presentationManager

    @EnvironmentObject var preferences: Preferences

    @ObservedObject private(set) var accountStore: AccountStore

    @State private var preferencesCopy: [String: Schema.PlatformPref.PrefValue] = [:]

    private var accountInstance: AccountInstance? {
        try? accountStore.accountInstance()
    }

    var body: some View {
        ZStack {
            Color.systemGroupedBackground
                .ignoresSafeArea()

            ScrollView {
                if let account = accountInstance {
                    if let user = account.user {
                        avatar(for: user)

                        if !account.platform.attributes.contains(.noSupportSingleThreadCreation) {
                            messageSelfButton(with: user.id)
                        }
                    }

                    if let preferences = account.platform.prefs {
                        accountSettings(for: preferences)
                    }

                    Spacer()
                } else {
                    Text("Could not retrieve account. Please try again and submit a bug report if this issue persists.")
                }
            }
            .toolbar {
                ToolbarItemGroup(placement: .navigationBarTrailing) {
                    SVGView(string: accountInstance?.platform.icon ?? "")
                        .frame(width: 30, height: 30)
                    Text(accountInstance?.platform.displayName ?? "")
                }
            }
            .onAppear {
                if let preferences = preferences.accountPreferences[accountStore.id] {
                    preferencesCopy = preferences
                }
            }
            .onDisappear {
                let preferences = preferences.accountPreferences[accountStore.id] ?? [:]

                if accountInstance?.platform.prefs != nil, !preferences.difference(from: preferencesCopy).isEmpty {
                    self.preferences.accountPreferences[accountStore.id] = preferencesCopy
                }
            }
        }
    }

    @ViewBuilder
    private func avatar(for user: Schema.User) -> some View {
        Avatar(imageURL: user.imgURL, size: .accountDetail)
            .padding()

        if let name = user.fullName {
            Text(name)
                .font(.title)
        }

        if let username = user.displayText {
            Text(username)
                .font(.subheadline)
        }
    }

    private func messageSelfButton(with userID: Schema.UserID) -> some View {
        TaskButton("Message yourself") {
            do {
                let thread = try await accountStore.newOrExistingThreadWith(users: [userID])

                presentationManager.dismiss()

                AppDelegate.shared.showThread(thread: thread)
            } catch {
                AppState.shared.handleError(error)
            }
        }
        .buttonStyle(CandyRectangleButtonStyle())
        .padding()
    }

    private func accountSettings(for platformPreferences: [String: Schema.PlatformPref]) -> some View {
        ForEach(Array(platformPreferences.keys).sorted(), id: \.self) { key in
            let pref = platformPreferences[key]!

            GroupBox {
                if case .checkbox = pref.type {
                    ToggleOption(
                        isOn: Binding(
                            get: {
                                if case .bool(let value) = preferencesCopy[key] {
                                    return value
                                }

                                if case .bool(let value) = pref.defaultValue {
                                    return value
                                }

                                return false
                            },
                            set: { preferencesCopy[key] = .bool($0) }
                        ),
                        title: pref.label,
                        description: pref.description
                    )
                } else {
                    SelectOption(
                        option: Binding(
                            get: {
                                if case .string(let value) = pref.defaultValue {
                                    return value
                                }
                                return ""
                            },
                            set: { _ = $0 }
                        ),
                        title: pref.label,
                        description: pref.description
                    )
                }
            }
            .padding(.horizontal)
            .groupBoxStyle(PrefGroupBox())
        }
    }

    @MainActor
    private func openSelfThread() async {
        guard let id = accountInstance?.user?.id else {
            return
        }

        do {
            let thread = try await accountStore.newOrExistingThreadWith(users: [id])
            AppDelegate.shared.showThread(thread: thread)
        } catch {
            AppState.shared.handleError(error)
        }
    }

    // TODO: proper select. temporary placeholder
    // we don't have a list of options yet in platform-sdk
    private struct SelectOption: View {
        @Binding var option: String
        let title: String
        let description: String?

        var body: some View {
            HStack {
                VStack(alignment: .leading, spacing: 4) {
                    Text(title)
                        .font(.body)
                        .lineLimit(2)

                    if let description = description {
                        Text(description)
                            .font(.subheadline)
                            .foregroundColor(.secondary)
                            .lineLimit(4)
                    }
                }

                Spacer()

                Text(option)
            }
            .frame(maxWidth: .infinity)
            .padding(.vertical, 2)
        }
    }

    private struct PrefGroupBox: GroupBoxStyle {
        func makeBody(configuration: Configuration) -> some View {
            configuration.content
                .frame(maxWidth: .infinity)
                .padding()
                .background(RoundedRectangle(cornerRadius: 8).fill(Color.secondarySystemGroupedBackground))
                .overlay(configuration.label.padding(.leading, 4), alignment: .topLeading)
        }
    }
}
