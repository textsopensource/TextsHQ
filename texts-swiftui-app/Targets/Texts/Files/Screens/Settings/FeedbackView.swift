//
// Copyright (c) Texts HQ
//

import Merge
import TextsCore
import TextsUI

struct FeedbackView: View {
    @Environment(\.presentationMode) var presentationMode

    @State private var cancellables: Set<AnyCancellable> = []

    @State private var feedback: String = ""
    @State private var selectedEmoji: String? = nil
    @State private var attachmentURL: URL?
    @State private var submittingStatus: SubmittingStatus? = nil
    @State private var isErrorAlertPresented: Bool = false
    @State private var isTextFieldFocused: Bool = true
    @State private var isDocumentPickerPresented: Bool = false

    let isTextEditorFocused: Bool = false // We currently can't detect if it's focused or not :(

    let emojis: [String] = ["😄", "😕", "🐛", "💡"]
    let borderWidth: CGFloat = 1.5

    let secondaryBorderColor: Color = Color.gray.opacity(Globals.Views.secondaryOpacity / 2)

    private var isSubmitButtonDisabled: Bool {
        if submittingStatus != nil { return true }
        return feedback.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty && selectedEmoji == nil && attachmentURL == nil
    }

    var body: some View {
        VStack {
            TextView(text: $feedback)
                .isFirstResponder(isTextFieldFocused) // TODO: Replace with @FocusState when deploying to iOS 15+
                .padding(.small)
                // .frame(maxHeight: 250)
                .border(isTextEditorFocused ? Color.accentColor : secondaryBorderColor, width: borderWidth, cornerRadius: Globals.Views.cornerRadius, style: .circular)
                .disabled(submittingStatus == .submitting)

            HStack {
                if submittingStatus != .success {
                    emojisView
                    Spacer()
                    attachmentButton
                }
            }
            .font(.subheadline.weight(.semibold))
            .animation(Globals.Views.animation, value: submittingStatus)

            Spacer()
        }
        .padding(.vertical, .regular)
        .padding(.horizontal, 10)
        // .background(backgroundColor)
        .cornerRadius(Globals.Views.cornerRadius, style: .circular)
        // .shadow(color: .black.opacity(Globals.Views.secondaryOpacity / 2), radius: 16)
        .disabled(submittingStatus == .submitting || submittingStatus == .success)
        .navigationBarTitleDisplayMode(.inline)
        .navigationTitle("Feedback")
        .navigationBarItems(leading: cancelButton, trailing: submitButton)
        .alert(isPresented: $isErrorAlertPresented) {
            if case .failure(let error) = submittingStatus, let error = error {
                return Alert(title: Text("Error!"), message: Text(error), dismissButton: .cancel(Text("Dismiss"), action: {
                    withAnimation {
                        isErrorAlertPresented = false
                        submittingStatus = nil
                    }
                }))
            } else {
                // shouldn't happen, but whatever
                return Alert(title: Text("Success!"), dismissButton: .cancel(Text("Dismiss"), action: {
                    withAnimation {
                        isErrorAlertPresented = false
                        submittingStatus = nil
                    }
                }))
            }
        }
    }

    private var emojisView: some View {
        ForEach(emojis, id: \.self) { emoji in
            Button {
                UIDevice.current.generateHaptic(.selectionChanged)
                selectedEmoji = selectedEmoji == emoji ? nil : emoji
            } label: {
                Text(emoji)
                    .padding(12)
            }
            .border(selectedEmoji == emoji ? Color.accentColor : secondaryBorderColor, width: borderWidth, cornerRadius: Globals.Views.smallCornerRadius, style: .circular)
            .animation(.easeInOut, value: selectedEmoji == emoji)
            .buttonStyle(DecreasesOnPressButtonStyle())
        }
    }

    private var attachmentButton: some View {
        Button {
            isDocumentPickerPresented = true
        } label: {
            Image(systemName: .paperclip)
                .padding(12)
        }
        .foregroundColor(attachmentURL != nil ? Color.white : Color.label)
        .background(attachmentURL != nil ? Color.accentColor : Color.systemGray6)
        .cornerRadius(Globals.Views.smallCornerRadius, style: .circular)
        .animation(Globals.Views.animation, value: attachmentURL != nil)
        .buttonStyle(DecreasesOnPressButtonStyle())
        .sheet(isPresented: $isDocumentPickerPresented) {
            DocumentPicker(mode: .import, allowedContentTypes: [.item]) { result in
                guard case let .success(urls) = result else { return }
                self.attachmentURL = urls.first
            }
            .edgesIgnoringSafeArea(.all)
        }
    }

    private var cancelButton: some View {
        DismissPresentationButton {
            Text("Cancel")
        }
        .keyboardShortcut(.cancelAction)
    }

    private var submitButton: some View {
        TaskButton {
            await submitFeedback()
        } label: {
            HStack {
                switch submittingStatus {
                case .submitting:
                    ProgressView()
                case .failure:
                    Text("Failed")
                case .success:
                    Text("Success!")
                case .none:
                    Text("Submit")
                }
            }
            .animation(.easeInOut, value: submittingStatus)
        }
        .disabled(isSubmitButtonDisabled)
    }

    func submitFeedback() async {
        submittingStatus = .submitting
        isErrorAlertPresented = false

        let result = await FeedbackManager.sendFeedback(
            text: feedback,
            emotion: selectedEmoji,
            attachmentURL: attachmentURL
        )

        switch result {
        case .success(let response):
            submitCompletion(success: response.ok, error: response.error)
        case .failure(let error):
            submitCompletion(success: false, error: error.description)
            AppState.shared.handleError(error)
        }
    }

    private func submitCompletion(success: Bool?, error: String? = nil) {
        let timeout: Double = 2

        if success ?? false {
            withAnimation {
                submittingStatus = .success
            }

            UIDevice.current.generateHaptic(.success)

            DispatchQueue.main.asyncAfter(deadline: .now() + timeout) {
                presentationMode.wrappedValue.dismiss()
            }
        } else {
            withAnimation {
                submittingStatus = .failure(error)
                isErrorAlertPresented = true
            }
            UIDevice.current.generateHaptic(.error)
            // DispatchQueue.main.asyncAfter(deadline: .now() + timeout) { submittingStatus = nil }
        }
    }
}

// MARK: - FeedbackView+SubmittingStatus

extension FeedbackView {
    enum SubmittingStatus: Equatable {
        case submitting
        case failure(String?)
        case success
    }
}

// MARK: - Development Preview -

struct FeedbackView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            FeedbackView().environment(\.colorScheme, .light)
            FeedbackView().environment(\.colorScheme, .dark)
        }
        .padding()
        .previewLayout(.sizeThatFits)
    }
}
