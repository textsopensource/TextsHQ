//
// Copyright (c) Texts HQ
//

import Merge
import SVGView
import TextsCore
import TextsUI

extension SettingsView {
    struct PlatformAccountRow: View {
        @EnvironmentObject var appState: AppState

        @ObservedObject var account: AccountInstance

        @State private var isDisabling = false
        @State private var isShowingAddAccountView = false
        @State private var presentedError: Identified<Error>?

        var body: some View {
            VStack {
                HStack {
                    avatar
                    accountName
                    Spacer()
                    toggleButton
                    Spacer()
                    moreDetailsContainer
                }
                .sheet(isPresented: $isShowingAddAccountView, onDismiss: nil) {
                    AuthenticationView(
                        isShowingAddAccountView: $isShowingAddAccountView,
                        platform: account.platform,
                        account: account
                    )
                }
            }
        }

        private var avatar: some View {
            Avatar(imageURL: account.user?.imgURL, size: .settingsAccount)
                .overlay(PlatformOverlay(account: account))
        }

        private var accountName: some View {
            Text(account.user?.displayName ?? account.id.rawValue)
                .lineLimit(1)
                .font(.headline)
                .frame(maxWidth: .infinity, alignment: .leading)
                .animation(.none)
        }

        private var toggleButton: some View {
            Toggle("Enable", isOn: toggleIsOn)
                .labelsHidden()
                .toggleStyle(SwitchToggleStyle(tint: .accentColor))
                .disabled(account.state == .disabling)
        }

        // HACK: enforce/ensure consistent horizontal alignment
        private var moreDetailsContainer: some View {
            ZStack {
                ProgressView()
                    .opacity(0)
                    .fixedSize()

                moreDetails
            }
            .animation(.default, value: account.state.type)
        }

        @ViewBuilder private var moreDetails: some View {
            switch account.state {
            case .disabled(nil):
                EmptyView()
            case  .offline:
                Image(systemName: .wifiSlash)
                    .renderingMode(.template)
                    .foregroundColor(.primary)
            case .disabled(let error?):
                Button {
                    presentedError = Identified(error)
                } label: {
                    Label("Failed to load account") {
                        if error is AccountInstance.ReauthError {
                            Image(systemName: .exclamationmarkTriangleFill)
                                .renderingMode(.original)
                        } else {
                            Image(systemName: .exclamationmarkCircleFill)
                                .renderingMode(.template)
                                .foregroundColor(.primary)
                        }
                    }
                    .labelStyle(.iconOnly)
                }
                .alert(item: $presentedError) { error in
                    Alert(
                        title: Text("Failed to load account"),
                        message: Text(verbatim: "\(error.value)"),
                        dismissButton: .default(Text("OK"))
                    )
                }
            case .loading, .loaded(_, true), .disabling:
                ProgressView()
            case .loaded(let store, false):
                NavigationLink(destination: AccountDetailView(accountStore: store)) {
                    EmptyView()
                }
                .fixedSize()
            }
        }

        private var toggleIsOn: Binding<Bool> {
            Binding<Bool> {
                if isShowingAddAccountView {
                    return true
                }
                if isDisabling {
                    return false
                }
                switch account.state {
                case .loading, .loaded, .offline:
                    return true
                case .disabled, .disabling:
                    return false
                }
            } set: {
                setEnabled($0)
            }
        }

        private func setEnabled(_ enabled: Bool) {
            Task { @MainActor in
                let accounts = appState.rootStore.accountsStore

                if enabled {
                    if case .disabled(_ as AccountInstance.ReauthError) = account.state {
                        isShowingAddAccountView = true
                    } else {
                        try await accounts.enableAccount(account)
                    }
                } else {
                    try await accounts.disableAccount(account)
                }
            }
        }
    }
}
