//
// Copyright (c) Texts HQ
//

import Merge
import TextsCore
import TextsUI

struct SettingsView: View {
    @EnvironmentObject var appState: AppState
    @EnvironmentObject var authorizationManager: AuthorizationManager
    @EnvironmentObject var preferences: Preferences

    @Environment(\.presentationMode) var presentationMode

    @State var isShowingAddAccountView = false
    @State private var presentedError: Identified<Error>?

    private var accountsStore: AccountsStore {
        AppState.shared.rootStore.accountsStore
    }

    var body: some View {
        NavigationView {
            List {
                accountsGroup
                generalSection
                notificationsSection
                appearanceSection
                otherSection
                developerSection
            }
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Settings")
            .animation(Globals.Views.animation, value: accountsStore.accounts.count)
            .animation(Globals.Views.animation, value: PlatformServer.shared.status)
            .animation(Globals.Views.animation, value: authorizationManager.isLoggedIn)
            .animation(Globals.Views.animation, value: preferences.isPrivacyForScreenCaptureEnabled)
            .navigationBarItems(
                trailing: Button("Done") {
                    UIApplication.shared.topmostViewController?.dismiss()
                }.font(.headline)
            )
            .alert(item: $presentedError) { error in
                Alert(
                    title: Text("Failed to enable notifications"),
                    message: Text(verbatim: "\(error.value)"),
                    dismissButton: .default(Text("OK"))
                )
            }
            .onChange(of: preferences.compactPinnedThreads) { isCompact in
                NotificationCenter.default.post(.refreshInbox)
            }
        }
        .navigationViewStyle(.stack)
    }

    private var accountsGroup: some View {
        Group {
            if authorizationManager.isLoggedIn {
                accountsSection
            } else {
                DynamicActionButton(action: AppAuthAction(enableHaptics: false)) {
                    Text("Log in")
                }
            }
        }
        .transition(.opacity)
        .animation(.easeInOut, value: authorizationManager.isLoggedIn)
    }

    private var accountsSection: some View {
        Section(header: Text("Accounts")) {
            ForEach(Array(accountsStore.accounts)) { account in
                PlatformAccountRow(account: account)
            }
            .onDelete(perform: deleteAccount)
            NavigationLink(isActive: $isShowingAddAccountView) {
                AddAccountView(isShowingAddAccountView: $isShowingAddAccountView)
            } label: {
                Text("Add Account")
            }
        }
    }

    private var generalSection: some View {
        let platformsWithArchive = appState.rootStore.platformsStore.platforms
            .filter { $0.attributes.contains(.supportsArchive) }
            .map(\.displayName)
            .joined(separator: ", ")
        return Section(header: Text("General")) {
            #if DEBUG
            ToggleOption(
                isOn: $preferences.useLocalPAS,
                title: "Local Mode",
                description: "Everything stays on-device. Limited notifications support."
            )
            #endif
            ToggleOption(
                isOn: $preferences.stealthMode,
                title: "%SETTINGS_STEALTH_MODE_HEADLINE%",
                description: "%SETTINGS_STEALTH_MODE_DESC%"
            )
            ToggleOption(
                isOn: $preferences.showTypingIndicator,
                title: "%SETTINGS_SHOW_TYPING_INDICATOR_HEADLINE%",
                description: nil
            )
            ToggleOption(
                isOn: $preferences.showUnreadThreadsFirst,
                title: "%SETTINGS_SHOW_UNREAD_THREADS_FIRST_DESCRIPTION%",
                description: nil
            )
            ToggleOption(
                isOn: $preferences.nativeArchive,
                title: "%SETTINGS_SYNC_NATIVE_ARCHIVE%",
                description: "This only has an effect on platforms that support archive: \(platformsWithArchive)"
            )
        }
    }

    private var appearanceSection: some View {
        Section(header: Text("Appearance")) {
            ToggleOption(
                isOn: $preferences.compactPinnedThreads,
                title: "%SETTINGS_COMPACT_PINNED_THREADS%",
                description: nil
            )
        }
    }

    private var notificationsSection: some View {
        Section(header: Text("Notifications")) {
            ToggleOption(
                isOn: $preferences.enableNotifications,
                title: "%SETTINGS_ENABLE_NOTIFICATIONS_HEADLINE%",
                description: nil
            )
            .onChange(of: preferences.enableNotifications) { newValue in
                Task { @MainActor in
                    do {
                        try await PushNotificationManager.shared.setEnabled(newValue)
                    } catch {
                        withAnimation {
                            // revert
                            preferences.enableNotifications = !newValue
                            presentedError = Identified(error)
                        }
                    }
                }
            }
        }
    }

    private var otherSection: some View {
        Section(header: Text("Other")) {
            NavigationLink(destination: AboutView()) {
                Text("About")
            }

            NavigationLink(destination: PrivacyView()) {
                Text("Privacy")
            }

            #if DEBUG
            NavigationLink(destination: DebugView(accountsStore: accountsStore)) {
                Text("Debug")
            }
            #endif

            Button {
                UIDevice.current.generateHaptic(.light)
                Task { await AppState.shared.clearCache() }
            } label: {
                Text("Clear Cache")
                    .foregroundColor(.accentColor)
            }

            PresentationLink {
                NavigationView {
                    FeedbackView()
                }
            } label: {
                Text("Feedback")
                    .foregroundColor(.accentColor)
            }

            TaskButton {
                UIDevice.current.generateHaptic(.light)

                presentationMode.dismiss()

                await authorizationManager.logout()
            } label: {
                Text("Log Out")
                    .foregroundColor(.systemRed)
            }
        }
    }

    private var developerSection: some View {
        Section(header: "Developer") {
            ToggleOption(
                isOn: $preferences.isPrivacyForScreenCaptureEnabled,
                title: "Privacy Mode",
                description: "Redacts all message texts and images."
            )

            ToggleOption(
                isOn: $preferences.enablePrivacyModeAutomaticallyWhenRecording,
                title: "Auto Enable Privacy Mode",
                description: "Enables privacy mode while recording screen."
            )

        }
    }

    // MARK: - Actions

    private func deleteAccount(at offsets: IndexSet) {
        let index = offsets[offsets.startIndex]

        Task { @MainActor in
            do {
                try await appState.rootStore.accountsStore.removeAt(index: index)
            } catch {
                AppState.shared.handleError(error)
            }
        }
    }
}
