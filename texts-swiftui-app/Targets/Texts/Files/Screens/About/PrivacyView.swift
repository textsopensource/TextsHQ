//
// Copyright (c) Texts HQ
//

import SwiftUIX

struct PrivacyView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 15) {
                Text("%PRIVACY_HEADLINE%")
                    .font(.title.weight(.bold))

                Text("%PRIVACY_DESCRIPTION%")
                    .font(.body.weight(.semibold))

                Link("texts.com/privacy", destination: URL(string: "https://texts.com/privacy")!)
                    .font(.body.weight(.bold))
            }
            .padding()
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
        }
        .navigationTitle("Privacy")
    }
}

struct PrivacyView_Previews: PreviewProvider {
    static var previews: some View {
        PrivacyView()
    }
}
