//
// Copyright (c) Texts HQ
//

#if DEBUG

import Merge
import SwiftUI
import TextsCore

struct DebugView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    @ObservedObject var platformServer = PlatformServer.shared
    @ObservedObject var accountsStore: AccountsStore

    var body: some View {
        List {
            Section(header: Text("PlatformServer")) {
                Text("Status: \(String(describing: PlatformServer.shared.status))")

                Button("Start PlatformServer") {
                    UIDevice.current.generateHaptic(.light)
                    PlatformServer.shared.start()
                }
                .disabled(!platformServer.status.canLaunch)
            }

            Section(header: Text("Platforms")) {
                ForEach(accountsStore.platformsStore.platforms) { platform in
                    Text(platform.name)
                }
            }

            Section(header: Text("Accounts")) {
                TaskButton("Remove all accounts") {
                    try await accountsStore.removeAllAccounts()
                }
            }
        }
        .listStyle(InsetGroupedListStyle())
        .navigationTitle("Debug")
    }
}

#endif
