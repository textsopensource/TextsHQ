//
// Copyright (c) Texts HQ
//

import SwiftUIX

struct AboutView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>

    private let libraries: [Library] = [
        .init(label: "SwiftUIX/SwiftUIX", url: URL(string: "https://github.com/SwiftUIX/SwiftUIX")!),
        .init(label: "SwiftUIX/Coordinator", url: URL(string: "https://github.com/SwiftUIX/Coordinator")!),
        .init(label: "SDWebImage/SDWebImageSwiftUI", url: URL(string: "https://github.com/SDWebImage/SDWebImageSwiftUI.git")!),
        .init(label: "exyte/Macaw", url: URL(string: "https://github.com/exyte/Macaw.git")!),
        .init(label: "Moriquendi/ISEmojiView", url: URL(string: "https://github.com/Moriquendi/ISEmojiView")!),
        .init(label: "slackhq/PanModal", url: URL(string: "https://github.com/slackhq/PanModal")!),
        .init(label: "kishikawakatsumi/KeychainAccess", url: URL(string: "https://github.com/kishikawakatsumi/KeychainAccess.git")!),
        .init(label: "hirotakan/MessagePacker", url: URL(string: "https://github.com/hirotakan/MessagePacker")!),
        .init(label: "nodejs-mobile/nodejs-mobile", url: URL(string: "https://github.com/nodejs-mobile/nodejs-mobile")!),
        .init(label: "getsentry/sentry-cocoa", url: URL(string: "https://github.com/getsentry/sentry-cocoa.git")!),
        .init(label: "pointfreeco/swift-identified-collections", url: URL(string: "https://github.com/pointfreeco/swift-identified-collections")!)
    ]

    var textsLogo: some View {
        VStack {
            Image("Texts")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 148, height: 148)
                .padding(.bottom)

            Text("Texts")
                .font(.largeTitle.weight(.bold))
                .foregroundColor(.primary)

            if let version = Bundle.main.shortVersion {
                Text("\(version.description) (#\(Bundle.main.buildNumber))")
                    .font(.body)
                    .foregroundColor(.secondary)
            }
        }
        .textCase(.none)
        .padding()
        .frame(maxWidth: .infinity, alignment: .center)
    }

    var body: some View {
        List {
            Section(header: textsLogo) {
                Link("Website", destination: URL(string: "https://texts.com")!)
                Link("Contact Us", destination: URL(string: "mailto:support@texts.com")!)
                Link("Twitter", destination: URL(string: "https://texts.com/twitter")!)
            }

            Section(header: Text("Licenses")) {
                ForEach(libraries) { library in
                    Link(library.label, destination: library.url)
                }
            }
        }
        .navigationTitle("About")
    }
}

extension AboutView {
    private struct Library: Identifiable {
        let id: String
        let label: String
        let url: URL

        init(label: String, url: URL) {
            self.label = label
            self.url = url
            self.id = url.absoluteString
        }
    }
}

struct AboutView_Previews: PreviewProvider {
    static var previews: some View {
        AboutView()
    }
}
