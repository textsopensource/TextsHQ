//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Combine

extension InboxViewController {
    class PinCell: UICollectionViewCell {
        override var isSelected: Bool {
            didSet {
                backgroundColor = isSelected ? .accentColor : .systemBackground
                label.textColor = isSelected ? .white : .secondaryLabel
            }
        }

        let avatarView = ThreadAvatarView(size: .pinnedThreadIcon, isSquare: true)
        let label = UILabel()
        let unreadIndicator = UIView()

        override init(frame: CGRect) {
            super.init(frame: frame)

            layer.cornerRadius = 8
            layer.cornerCurve = .continuous

            label.font = .preferredFont(forTextStyle: .caption2)
            label.textColor = .secondaryLabel
            label.textAlignment = .center
            label.allowsDefaultTighteningForTruncation = true

            unreadIndicator.backgroundColor = .accentColor
            unreadIndicator.layer.cornerRadius = 4

            [avatarView, label, unreadIndicator].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                avatarView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                avatarView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8),
                avatarView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8),

                label.topAnchor.constraint(equalTo: avatarView.bottomAnchor, constant: 4),
                label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 4),
                label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -4),

                unreadIndicator.heightAnchor.constraint(equalToConstant: 8),
                unreadIndicator.widthAnchor.constraint(equalToConstant: 8),
                unreadIndicator.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -2),
                unreadIndicator.centerYAnchor.constraint(equalTo: label.centerYAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with inboxThread: InboxThread) {
            avatarView.source = .init(thread: inboxThread.thread)
            avatarView.addPlatformOverlay(for: inboxThread.thread)
            unreadIndicator.isHidden = !inboxThread.isUnread
            label.text = inboxThread.title
            label.textColor = inboxThread.isUnread ? .label : (isSelected ? .white : .secondaryLabel)
        }
    }
}
