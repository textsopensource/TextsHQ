//
// Copyright (c) Texts HQ
//

import UIKit

extension InboxViewController {
    class SystemCell: UICollectionViewCell {
        lazy var loadingIndicator = UIActivityIndicatorView(style: .medium)
        lazy var symbolImageView = UIImageView()
        lazy var titleLabel = UILabel()

        override init(frame: CGRect) {
            super.init(frame: frame)

            symbolImageView.tintColor = .secondaryLabel

            titleLabel.textAlignment = .center
            titleLabel.font = .preferredFont(forTextStyle: .title2)
            titleLabel.numberOfLines = 0
            titleLabel.textColor = .secondaryLabel

            [loadingIndicator, symbolImageView, titleLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                symbolImageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                symbolImageView.bottomAnchor.constraint(equalTo: contentView.centerYAnchor, constant: -4),

                loadingIndicator.centerXAnchor.constraint(equalTo: symbolImageView.centerXAnchor),
                loadingIndicator.centerYAnchor.constraint(equalTo: symbolImageView.centerYAnchor, constant: -16),

                titleLabel.topAnchor.constraint(equalTo: contentView.centerYAnchor, constant: 4),
                titleLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                titleLabel.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 20),
                titleLabel.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -20)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        enum Message {
            case setup, noActive, empty, loading, noResults, emptyArchive, noRequests
        }

        func configure(with message: Message) {
            switch message {
            case .setup:
                titleLabel.text = "Add an account in Settings"
                symbolImageView.image = UIImage(
                    systemName: "plus",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )
            case .noActive:
                titleLabel.text = "Enable an account in Settings"
                symbolImageView.image = UIImage(
                    systemName: "plus",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )
            case .empty:
                titleLabel.text = "No threads"
                symbolImageView.image = UIImage(
                    systemName: "circle",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )
            case .loading:
                titleLabel.text = "Loading..."
                loadingIndicator.startAnimating()

            case .noResults:
                titleLabel.text = "No results"
                symbolImageView.image = UIImage(
                    systemName: "magnifyingglass",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )

            case .emptyArchive:
                titleLabel.text = "Archive is empty"
                symbolImageView.image = UIImage(
                    systemName: "tray.fill",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )

            case .noRequests:
                titleLabel.text = "Request inbox is empty"
                symbolImageView.image = UIImage(
                    systemName: "person.crop.circle.badge.questionmark",
                    withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
                )
            }

            symbolImageView.isHidden = message == .loading
            loadingIndicator.isHidden = message != .loading
        }
    }
}
