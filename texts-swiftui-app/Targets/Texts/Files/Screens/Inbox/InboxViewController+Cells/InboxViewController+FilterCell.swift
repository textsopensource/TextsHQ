//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension InboxViewController {
    class FilterCell: UICollectionViewListCell {
        let icon = UIImageView()
        let label = UILabel()

        override init(frame: CGRect) {
            super.init(frame: frame)

            preservesSuperviewLayoutMargins = true
            contentView.preservesSuperviewLayoutMargins = true

            icon.contentMode = .scaleAspectFit
            icon.tintColor = .label
            label.font = .preferredFont(forTextStyle: .body)

            [icon, label].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                icon.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
                icon.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -8),
                icon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                icon.heightAnchor.constraint(lessThanOrEqualTo: contentView.heightAnchor, constant: -16),
                icon.widthAnchor.constraint(equalToConstant: 40),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with filter: SearchFilter) {
            icon.image = filter.image
            label.text = filter.title
        }
    }
}
