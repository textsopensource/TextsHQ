//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Combine

extension InboxViewController {
    class ThreadCell: UICollectionViewListCell {
        let unreadIndicator = UIView()
        let avatarView = ThreadAvatarView(size: .threadIcon, isSquare: true).redactInPrivacyMode()
        let titleLabel = UILabel().redactInPrivacyMode()
        let messageLabel = UILabel().redactInPrivacyMode()
        let verifiedImageView = UIImageView()
        let pinnedImageView = UIImageView()
        let mutedImageView = UIImageView()
        let ticksLabel = UILabel().redactInPrivacyMode()
        let timestampLabel = UILabel().redactInPrivacyMode()
        let chevronImageView = UIImageView()

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            unreadIndicator.backgroundColor = .accentColor
            unreadIndicator.layer.cornerRadius = 5

            titleLabel.font = .preferredFont(forTextStyle: .headline)
            titleLabel.textColor = .label
            titleLabel.allowsDefaultTighteningForTruncation = true
            titleLabel.lineBreakMode = .byTruncatingTail
            titleLabel.setContentCompressionResistancePriority(.defaultLow, for: .horizontal)

            verifiedImageView.image = UIImage(
                systemName: "checkmark.seal.fill",
                withConfiguration: UIImage.SymbolConfiguration(font: titleLabel.font)
            )

            let spacer = UIView()

            ticksLabel.font = .preferredFont(forTextStyle: .headline)

            timestampLabel.font = .preferredFont(forTextStyle: .subheadline)
            timestampLabel.textColor = .secondaryLabel

            chevronImageView.image = UIImage(
                systemName: "chevron.right",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 15, weight: .semibold)
            )
            chevronImageView.tintColor = .systemFill

            let titleStack = UIStackView(arrangedSubviews: [
                titleLabel,
                verifiedImageView,
                spacer,
                mutedImageView,
                pinnedImageView,
                ticksLabel,
                timestampLabel,
                chevronImageView
            ])
            titleStack.axis = .horizontal
            titleStack.distribution = .fill
            titleStack.spacing = 4

            messageLabel.textColor = .secondaryLabel
            messageLabel.font = .preferredFont(forTextStyle: .subheadline)
            messageLabel.numberOfLines = 2

            pinnedImageView.image = UIImage(
                systemName: "pin.fill",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 12, weight: .semibold)
            )
            pinnedImageView.tintColor = .systemFill
            pinnedImageView.isHidden = true

            mutedImageView.image = UIImage(
                systemName: "bell.slash.fill",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 12, weight: .semibold)
            )
            mutedImageView.tintColor = .systemFill
            mutedImageView.isHidden = true

            [verifiedImageView, mutedImageView, pinnedImageView, chevronImageView].forEach {
                $0.setContentHuggingPriority(.required, for: .horizontal)
                $0.setContentCompressionResistancePriority(.required, for: .horizontal)
                $0.contentMode = .scaleAspectFit
            }

            [
                unreadIndicator,
                avatarView,
                titleStack,
                messageLabel
            ].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                unreadIndicator.heightAnchor.constraint(equalToConstant: 10),
                unreadIndicator.widthAnchor.constraint(equalToConstant: 10),
                unreadIndicator.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
                unreadIndicator.centerYAnchor.constraint(equalTo: avatarView.centerYAnchor),

                avatarView.topAnchor.constraint(equalTo: titleLabel.topAnchor, constant: 2),
                avatarView.leadingAnchor.constraint(equalTo: unreadIndicator.trailingAnchor, constant: 8),
                avatarView.widthAnchor.constraint(equalToConstant: AvatarSize.threadIcon.width),
                avatarView.heightAnchor.constraint(equalToConstant: AvatarSize.threadIcon.height),

                titleStack.topAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.topAnchor, constant: 8),
                titleStack.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 16),
                titleStack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16),
                titleStack.heightAnchor.constraint(equalToConstant: UIFont.preferredFont(forTextStyle: .headline).lineHeight),

                ticksLabel.widthAnchor.constraint(equalToConstant: 20),

                messageLabel.topAnchor.constraint(equalTo: titleStack.bottomAnchor),
                messageLabel.leadingAnchor.constraint(equalTo: titleStack.leadingAnchor),
                messageLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        override func updateConstraints() {
            super.updateConstraints()

            NSLayoutConstraint.activate([
                separatorLayoutGuide.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor)
            ])
        }

        override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
            CGSize(
                width: super.systemLayoutSizeFitting(
                    targetSize,
                    withHorizontalFittingPriority: horizontalFittingPriority,
                    verticalFittingPriority: verticalFittingPriority
                ).width,
                height: 80
            )
        }

        override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            super.traitCollectionDidChange(previousTraitCollection)

            chevronImageView.isHidden = window?.traitCollection.horizontalSizeClass == .regular
        }

        // MARK: - Configuration

        func configure(with inboxThread: InboxThread) {
            avatarView.source = .init(thread: inboxThread.thread)
            avatarView.addPlatformOverlay(for: inboxThread.thread)

            titleLabel.text = inboxThread.title
            messageLabel.attributedText = inboxThread.message

            pinnedImageView.isHidden = !inboxThread.isPinned
            mutedImageView.isHidden = !inboxThread.isMuted
            unreadIndicator.isHidden = !inboxThread.isUnread
            verifiedImageView.isHidden = !inboxThread.isVerified

            timestampLabel.text = inboxThread.timestamp
            ticksLabel.attributedText = inboxThread.ticks
            ticksLabel.isHidden = inboxThread.ticks.string.isEmpty

            chevronImageView.isHidden = window?.traitCollection.horizontalSizeClass == .regular
        }

        // MARK: - Message

        func updateMessageLabel(with thread: ThreadStore) {
            messageLabel.attributedText = thread.activityOrDraftOrLastMessage
        }

        func updateReadStatus(with thread: ThreadStore) {
            unreadIndicator.isHidden = !thread.displayIsUnread
        }
    }
}
