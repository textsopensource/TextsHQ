//
// Copyright (c) Texts HQ
//

import UIKit

extension InboxViewController {
    class LoadingCell: UICollectionViewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)

            let loadingIndicator = UIActivityIndicatorView(style: .medium)
            loadingIndicator.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(loadingIndicator)

            NSLayoutConstraint.activate([
                loadingIndicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                loadingIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            ])

            loadingIndicator.startAnimating()
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
