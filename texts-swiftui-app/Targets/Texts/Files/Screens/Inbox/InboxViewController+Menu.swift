//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension InboxViewController {
    struct CellAction {
        enum Attribute {
            case destructive
            case secondary
            case menuOnly
            case disabled
        }

        let title: String
        let systemImage: String?
        let color: UIColor
        let attributes: Set<Attribute>
        let handler: () -> Void

        var uiAction: UIAction {
            var attributes: UIMenuElement.Attributes = []
            if self.attributes.contains(.destructive) {
                attributes.insert(.destructive)
            }
            if self.attributes.contains(.disabled) {
                attributes.insert(.disabled)
            }
            return UIAction(title: title, image: systemImage.flatMap(UIImage.init(systemName:)), attributes: attributes) { _ in
                handler()
            }
        }

        var contextAction: UIContextualAction? {
            guard !attributes.contains(.menuOnly) else {
                return nil
            }
            let action = UIContextualAction(style: .normal, title: title) { _, _, completion in
                if !attributes.contains(.disabled) {
                    handler()
                }
                completion(true)
            }
            action.setImage(systemImage!, with: title)
            action.backgroundColor = color
            return action
        }
    }

    // TODO: These .refreshInbox calls should patch the snapshot
    // swiftlint:disable:next function_body_length
    func cellActions(for thread: ThreadStore) -> [CellAction] {
        var actions = [
            CellAction(
                title: thread.displayIsUnread ? "Mark Read" : "Mark Unread",
                systemImage: thread.displayIsUnread ? "eye" : "eye.slash",
                color: .systemBlue,
                attributes: []
            ) {
                Task {
                    try await (thread.displayIsUnread ? thread.markAsRead : thread.markAsUnread)()
                    NotificationCenter.default.post(.refreshInbox)
                }
            },
            CellAction(
                title: thread.displayIsArchived ? "Unarchive" : "Archive",
                systemImage: "archivebox",
                color: .systemOrange,
                attributes: []
            ) {
                Task {
                    try await thread.setIsArchived(!thread.displayIsArchived)
                    NotificationCenter.default.post(.refreshInbox)
                }
            },
            CellAction(
                title: thread.props.isHidden == true ? "Show" : "Hide",
                systemImage: thread.props.isHidden == true ? "arrow.up.bin" : "xmark.bin",
                color: .systemGray,
                attributes: [.secondary, .menuOnly]
            ) {
                thread.props.isHidden = thread.props.isHidden == true ? nil : true
                NotificationCenter.default.post(.refreshInbox)
            },
            CellAction(
                title: thread.displayIsMuted == true ? "Unmute" : "Mute",
                systemImage: thread.displayIsMuted == true ? "bell" : "bell.slash",
                color: .systemGray,
                attributes: [.secondary]
            ) {
                Task {
                    try await thread.setIsMuted(!thread.displayIsMuted)
                }
            },
            CellAction(
                title: thread.displayIsPinned ? "Unpin" : "Pin",
                systemImage: thread.displayIsPinned ? "pin.slash" : "pin",
                color: .systemPink,
                attributes: [.secondary]
            ) {
                Task {
                    try await thread.setIsPinned(!thread.displayIsPinned)
                    NotificationCenter.default.post(.refreshInbox)
                }
            },
            CellAction(
                title: "Rename",
                systemImage: "square.and.pencil",
                color: .systemPurple,
                attributes: {
                    var attributes = Set([CellAction.Attribute.secondary, .menuOnly])
                    if thread.account?.platform?.attributes.contains(.noSupportGroupTitleChange) == true {
                        attributes.insert(.disabled)
                    }
                    return attributes
                }()
            ) { [unowned self] in
                let actionSheet = UIAlertController(title: "New Name", message: nil, preferredStyle: .alert)
                actionSheet.addTextField { textField in
                    textField.text = thread.props.customTitle
                }
                actionSheet.addAction(
                    UIAlertAction(title: "Confirm", style: .default) { [unowned actionSheet] _ in
                        guard let newTitle = actionSheet.textFields?.first?.text else { return }
                        thread.props.customTitle = newTitle
                    }
                )
                actionSheet.addAction(
                    UIAlertAction(title: "Clear", style: .destructive) { _ in
                        thread.props.customTitle = nil
                    }
                )
                actionSheet.addAction(
                    UIAlertAction(title: "Dismiss", style: .cancel)
                )

                self.present(actionSheet, animated: true)
            },
            CellAction(
                title: "Delete",
                systemImage: "trash",
                color: .systemRed,
                attributes: [thread.account?.platform?.attributes.contains(.supportsDeleteThread) == true ? .destructive : .disabled]
            ) {
                let actionSheet = UIAlertController(
                    title: "Are you sure you want to delete this thread?",
                    message: thread.displayName(),
                    preferredStyle: .actionSheet
                )

                actionSheet.addAction(.init(title: "Delete", style: .destructive) { _ in
                    Task {
                        try await thread.account?.deleteThread(threadID: thread.id)
                    }
                })
                actionSheet.addAction(.init(title: "Cancel", style: .cancel))

                self.present(actionSheet, animated: true)
            }
        ]

        #if DEBUG
        actions.append(CellAction(
            title: "Report",
            systemImage: "exclamationmark.bubble",
            color: .systemRed,
            attributes: [
                thread.account?.platform?.attributes.contains(.supportsReportThread) == true ? .destructive : .disabled,
                .menuOnly,
                .secondary
            ]
        ) {
            let actionSheet = UIAlertController(
                title: "Are you sure you want to report this thread?",
                message: thread.displayName(),
                preferredStyle: .actionSheet
            )

            actionSheet.addAction(.init(title: "Report", style: .destructive) { _ in
                Task {
                    try await thread.account?.reportThread(threadID: thread.id)
                }
            })
            actionSheet.addAction(.init(title: "Cancel", style: .cancel))

            self.present(actionSheet, animated: true)
        })
        #endif

        return actions
    }

    func cellActions(at indexPath: IndexPath) -> [CellAction] {
        guard let identifier = dataSource.itemIdentifier(for: indexPath) else {
            return []
        }
        switch identifier {
        case .thread(let inboxThread), .pin(let inboxThread):
            return cellActions(for: inboxThread.thread)
        default:
            return []
        }
    }

    override func collectionView(_ collectionView: UICollectionView, contextMenuConfigurationForItemAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        .init(identifier: nil, previewProvider: nil) { [unowned self] _ in
            let actions = cellActions(at: indexPath)
            guard !actions.isEmpty else {
                return nil
            }
            return UIMenu(children: actions
                .filter { !$0.attributes.contains(.secondary) }
                .map(\.uiAction) + [
                    UIMenu(
                        title: "More",
                        image: UIImage(systemName: "ellipsis"),
                        children: cellActions(at: indexPath).filter { $0.attributes.contains(.secondary) }.map(\.uiAction)
                    )
                ]
            )
        }
    }
}
