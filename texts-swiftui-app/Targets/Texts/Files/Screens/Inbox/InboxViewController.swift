//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore
import Combine
import IdentifiedCollections

// swiftlint:disable:next type_body_length
class InboxViewController: UICollectionViewController, UISearchResultsUpdating {

    lazy var dataSource = createSource()
    lazy var layout = createLayout()
    lazy var searchController = UISearchController(searchResultsController: nil)

    // MARK: - Data

    var threads: IdentifiedArray<String, ThreadStore> {
        AppState.shared.rootStore.accountsStore.displayThreads
    }

    var inbox = Inbox.inbox {
        didSet {
            switch inbox {
            case .inbox:
                AppState.shared.rootStore.accountsStore.selectedInbox = .normal
            case .requests:
                AppState.shared.rootStore.accountsStore.selectedInbox = .requests
            case .archive:
                applySnapshot()
            }
            navigationItem.leftBarButtonItems = [settingsItem(), inboxItem()]
        }
    }

    private var changeObserver: AnyCancellable?
    private func configureObserver() {
        changeObserver = AppState.shared.rootStore.accountsStore.$displayThreads.sink { [weak self] displayThreads in
            self?.applySnapshot()
        }
    }

    // MARK: - Init

    init() {
        super.init(collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.setCollectionViewLayout(layout, animated: false)
    }

    required init?(coder: NSCoder) {
        nil
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavigationItem()
        configureObserver()

        searchController.searchResultsUpdater = self

        if #unavailable(iOS 15) {
            // iOS 14: Filter selection does not work
            searchController.obscuresBackgroundDuringPresentation = false
        }

        collectionView.keyboardDismissMode = .interactive
        collectionView.refreshControl = UIRefreshControl()
        collectionView.refreshControl!.addAction(UIAction { [unowned self] _ in
            refresh()
        }, for: .valueChanged)

        collectionView.register(FilterCell.self, forCellWithReuseIdentifier: "FilterCell")
        collectionView.register(ThreadCell.self, forCellWithReuseIdentifier: "ThreadCell")
        collectionView.register(PinCell.self, forCellWithReuseIdentifier: "PinCell")
        collectionView.register(SystemCell.self, forCellWithReuseIdentifier: "SystemCell")
        collectionView.register(LoadingCell.self, forCellWithReuseIdentifier: "LoadingCell")

        collectionView.dataSource = dataSource

        NotificationCenter.default.addObserver(
            forName: Notification.refreshInbox.name,
            object: nil, queue: .main
        ) { [weak self] _ in
            self?.applySnapshot()
        }
    }

    // MARK: - Actions

    func attemptPagination() {
        guard !AppState.shared.rootStore.accountsStore.isLoadingThreads else {
            return
        }

        Task {
            await withTaskGroup(of: Void.self) { group in
                for account in AppState.shared.rootStore.accountsStore.accounts {
                    if
                        let accountStore = account.currentAccountStore,
                        let inbox = accountStore.currentInbox,
                        inbox.threads.hasMore
                    {
                        group.addTask { _ = try? await inbox.getThreads(.older) }
                    }
                }
            }
        }
    }

    func refresh() {
        Task {
            await withTaskGroup(of: Void.self) { group in
                for account in AppState.shared.rootStore.accountsStore.accounts {
                    if
                        let accountStore = account.currentAccountStore,
                        let inbox = accountStore.currentInbox,
                        inbox.threads.hasMore
                    {
                        group.addTask { _ = try? await inbox.getThreads(.all) }
                    }
                }
            }
            DispatchQueue.main.async {
                self.collectionView.refreshControl?.endRefreshing()
                self.applySnapshot()
            }
        }
    }

    func updateSearchResults(for searchController: UISearchController) {
        applySnapshot()
    }

    // MARK: - Inbox

    private func applySnapshot() {
        let (allThreads, compactThreads, pinnedThreads) = threadsPreparedForSnapshot()

        let unreadCount = allThreads.filter { $0.isUnread }.count
        navigationItem.backButtonTitle = unreadCount == 0 ? "Texts" : String(unreadCount)

        var snapshot = NSDiffableDataSourceSnapshot<InboxSectionType, InboxCellType>()

        defer {
            dataSource.apply(snapshot, animatingDifferences: false)
        }

        guard !allThreads.isEmpty else {
            guard PlatformServer.shared.status != .launching else {
                return
            }

            snapshot.appendSections([.system])
            snapshot.appendItems(
                [.message({
                    if AppState.shared.rootStore.accountsStore.accounts.isEmpty {
                        return .setup
                    } else if !AppState.shared.rootStore.accountsStore.accounts.contains(where: \.state.isActive) {
                        return .setup
                    } else if searchController.isActive {
                        return .noResults
                    } else if inbox == .requests {
                        return .noRequests
                    } else if inbox == .archive {
                        return .emptyArchive
                    } else {
                        return .empty
                    }
                }())],
                toSection: .system
            )
            return
        }

        if searchController.isActive {
            snapshot.appendSections([.filters])
            snapshot.appendItems(
                filteredFilters().map { .filter($0) },
                toSection: .filters
            )
        }

        if !pinnedThreads.isEmpty {
            snapshot.appendSections([.pinned])
            snapshot.appendItems(
                pinnedThreads.map { .pin(.init(thread: $0)) },
                toSection: .pinned
            )
        }

        if !compactThreads.isEmpty {
            snapshot.appendSections([.compact])
            snapshot.appendItems(
                compactThreads.map { .thread(.init(thread: $0)) },
                toSection: .compact
            )
        }

        if AppState.shared.rootStore.accountsStore.isLoadingThreads {
            snapshot.appendSections([.loading])
            snapshot.appendItems(
                [.loading],
                toSection: .loading
            )
        }
    }

    func threadsPreparedForSnapshot() -> (all: [ThreadStore], compact: [ThreadStore], pinned: [ThreadStore]) {
        var compactThreads = threads.filter { thread in
            if thread.props.isHidden == true {
                return false
            }

            if thread.timestamp == nil {
                return false
            }

            if thread.displayIsArchived {
                return inbox == .archive || searchController.isActive
            } else if inbox == .archive {
                return false
            }

            return true
        }.sorted {
            if Preferences.shared.showUnreadThreadsFirst, $0.displayIsUnread != $1.displayIsUnread {
                return $0.displayIsUnread || !$1.displayIsUnread
            }
            return ($0.timestamp ?? .distantPast) > ($1.timestamp ?? .distantPast)
        }

        var pinnedThreads = compactThreads.filter { thread in
            if thread.displayIsPinned != true {
                return false
            }
            return true
        }

        if searchController.isActive {
            pinnedThreads = filterThreads(pinnedThreads)
            compactThreads = filterThreads(compactThreads)
        }

        if pinnedThreads.count < 16, !Preferences.shared.compactPinnedThreads {
            compactThreads = compactThreads.filter { $0.displayIsPinned != true }
        } else {
            pinnedThreads = []
            compactThreads = compactThreads.sorted {
                if $0.displayIsPinned != $1.displayIsPinned {
                    return $0.displayIsPinned || !$1.displayIsPinned
                }
                return false
            }
        }

        return (compactThreads + pinnedThreads, compactThreads, pinnedThreads)
    }

    // MARK: - Selection

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let identifier = dataSource.itemIdentifier(for: indexPath) else {
            return
        }

        switch identifier {
        case .pin(let inboxThread), .thread(let inboxThread):
            let viewController = ThreadViewController(viewModel: ThreadViewModel(thread: inboxThread.thread))

            viewController.inputController.onDraftChange = { [weak self] _ in
                self?.applySnapshot()
            }

            splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewController), sender: self)

        case .filter(let filter):
            searchController.searchBar.searchTextField.insertToken(filter.token(), at: searchFilters.count)

        case .message, .loading:
            break
        }
    }

    // MARK: - Cells and Layout

    private func createSource() -> UICollectionViewDiffableDataSource<InboxSectionType, InboxCellType> {
        .init(collectionView: collectionView) { collectionView, indexPath, itemIdentifier in
            switch itemIdentifier {
            case .message(let message):
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SystemCell", for: indexPath) as? SystemCell else {
                    fatalError("Configuration error.")
                }
                cell.configure(with: message)
                return cell

            case .pin(let thread):
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PinCell", for: indexPath) as? PinCell else {
                    fatalError("Configuration error.")
                }
                cell.configure(with: thread)
                return cell

            case .thread(let thread):
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThreadCell", for: indexPath) as? ThreadCell else {
                    fatalError("Configuration error.")
                }
                cell.configure(with: thread)
                return cell

            case .filter(let filter):
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCell", for: indexPath) as? FilterCell else {
                    fatalError("Configuration error.")
                }
                cell.configure(with: filter)
                return cell

            case .loading:
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadingCell", for: indexPath) as? LoadingCell else {
                    fatalError("Configuration error.")
                }
                return cell
            }
        }
    }

    private func createLayout() -> UICollectionViewLayout {
        UICollectionViewCompositionalLayout { [unowned self] section, layoutEnvironment in
            switch self.dataSource.snapshot().sectionIdentifiers[section] {
            case .system, .loading:
                let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)

                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(240))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])

                let section = NSCollectionLayoutSection(group: group)
                section.contentInsets = .init(top: 32, leading: 26, bottom: 32, trailing: 26)

                return section

            case .filters:
                let configuration = UICollectionLayoutListConfiguration(appearance: .plain)
                let section = NSCollectionLayoutSection.list(using: configuration, layoutEnvironment: layoutEnvironment)

                return section

            case .pinned:
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .absolute(8 + AvatarSize.pinnedThreadIcon.width + 8),
                    heightDimension: .absolute(8 + AvatarSize.pinnedThreadIcon.height + 4 + UIFont.preferredFont(forTextStyle: .caption1).lineHeight + 8)
                )
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(100))

                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
                group.interItemSpacing = .flexible(0)
                group.edgeSpacing = .init(leading: nil, top: .fixed(4), trailing: nil, bottom: .fixed(4))

                let section = NSCollectionLayoutSection(group: group)
                section.contentInsets = .init(top: 16, leading: 26, bottom: 8, trailing: 26)

                return section

            case .compact:
                var configuration = UICollectionLayoutListConfiguration(appearance: .plain)
                configuration.leadingSwipeActionsConfigurationProvider = { [unowned self] indexPath in
                    .init(actions: cellActions(at: indexPath).filter { !$0.attributes.contains(.secondary) }.compactMap(\.contextAction))
                }
                configuration.trailingSwipeActionsConfigurationProvider = { [unowned self] indexPath in
                    .init(actions: cellActions(at: indexPath).filter { $0.attributes.contains(.secondary) }.compactMap(\.contextAction))
                }
                let section = NSCollectionLayoutSection.list(using: configuration, layoutEnvironment: layoutEnvironment)

                return section
            }
        }
    }

    // MARK: - Pagination

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == threads.count - 5 {
            attemptPagination()
        }
    }
}
