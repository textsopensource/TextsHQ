//
// Copyright (c) Texts HQ
//

import Foundation
import TextsCore

extension InboxViewController {
    struct InboxThread: Hashable {
        let thread: ThreadStore

        let title: String
        let message: NSAttributedString

        let isPinned: Bool
        let isMuted: Bool
        let isUnread: Bool
        let isVerified: Bool

        let timestamp: String
        let ticks: NSAttributedString

        init(thread: ThreadStore) {
            self.thread = thread

            title = thread.name
            message = thread.activityOrDraftOrLastMessage

            isPinned = thread.displayIsPinned
            isMuted = thread.displayIsMuted
            isUnread = thread.displayIsUnread
            isVerified = thread.isVerified

            timestamp = thread.relativeDisplayTimestamp ?? ""
            ticks = thread.displayTicks
        }
    }

    enum InboxSectionType {
        case filters, pinned, compact, system, loading
    }

    enum InboxCellType: Hashable {
        case filter(SearchFilter), pin(InboxThread), thread(InboxThread), message(SystemCell.Message), loading
    }

    enum Inbox: String, CaseIterable {
        case inbox, requests, archive
    }
}
