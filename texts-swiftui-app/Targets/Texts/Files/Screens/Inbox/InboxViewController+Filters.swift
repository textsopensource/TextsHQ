//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Macaw
import SDWebImage

extension InboxViewController {

    // MARK: - Convenience

    var searchFilters: [SearchFilter] {
        searchController
            .searchBar.searchTextField
            .tokens.map(\.representedObject)
            .compactMap { $0 as? SearchFilter }
    }

    var searchQuery: String {
        searchController.searchBar.text ?? ""
    }

    // MARK: - Filtering

    func filteredFilters() -> [SearchFilter] {
        SearchFilter.allCases.filter { filter in
            if searchQuery.isEmpty && searchFilters.isEmpty {
                return true
            }

            if searchFilters.contains(filter) {
                return false
            }

            if !searchQuery.isEmpty, !filter.title.localizedStandardContains(searchQuery) {
                return false
            }

            return true
        }
    }

    func filterThreads(_ threads: [ThreadStore]) -> [ThreadStore] {
        threads.filter { thread in
            if thread.props.isHidden == true {
                return false
            }

            if searchQuery.isEmpty && searchFilters.isEmpty {
                return true
            }

            if thread.timestamp == nil {
                return false
            }

            if !searchQuery.isEmpty && !thread.name.localizedStandardContains(searchQuery) {
                return false
            }

            if searchFilters.contains(.unread), !thread.isUnread {
                return false
            }

            if searchFilters.contains(.unanswered), thread.messages.last?.isSender == false {
                return false
            }

            if searchFilters.contains(.unresponded), thread.messages.last?.isSender == true {
                return false
            }

            if searchFilters.contains(.group), thread.participants.count < 3 {
                return false
            }

            let allowedAccounts = searchFilters.compactMap({ filter -> AccountInstance? in
                if case .account(let account) = filter {
                    return account
                }
                return nil
            })

            if !allowedAccounts.isEmpty, let account = thread.account?.instance, !allowedAccounts.contains(account) {
                return false
            }

            return true
        }
    }

    // MARK: - Tokens

    enum SearchFilter: Hashable, Equatable {
        case account(AccountInstance)
        case unread, group
        case unanswered, unresponded

        static var allCases: [SearchFilter] {
            let tokens: [SearchFilter] = [.unread, .group, .unanswered, .unresponded]
            let accounts = AppState.shared.rootStore.accountsStore.accounts
                .filter {
                    $0.state.isActive
                }
                .sorted {
                    $0.user?.displayName ?? $0.platform.displayName
                    >
                    $1.user?.displayName ?? $1.platform.displayName
                }
                .map {
                    SearchFilter.account($0)
                }

            return tokens + (accounts.count > 1 ? accounts : [])
        }

        var title: String {
            switch self {
            case .account(let account):
                return account.user?.displayName ?? account.platform.displayName
            case .unread:
                return "Unread"
            case .group:
                return "Group"
            case .unanswered:
                return "Not Answered"
            case .unresponded:
                return "Not Responded"
            }
        }

        var image: UIImage? {
            switch self {
            case .account(let account):
                return UIImage(
                    cgImage: (try? account.platform.icon
                        .flatMap(SVGParser.parse(text:))?
                        .toNativeImage(size: Size(20 * UIScreen.main.scale, 20 * UIScreen.main.scale))
                        .cgImage
                    )!,
                    scale: UIScreen.main.scale,
                    orientation: .up
                )
            case .unread:
                return UIImage(systemName: "bell.badge")
            case .group:
                return UIImage(systemName: "person.3.fill")
            case .unanswered:
                return UIImage(systemName: "arrow.uturn.left")
            case .unresponded:
                return UIImage(systemName: "arrow.uturn.right")
            }
        }

        func token() -> UISearchToken {
            let token = UISearchToken(icon: image, text: title)
            token.representedObject = self
            return token
        }
    }
}
