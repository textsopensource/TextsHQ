//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore

extension InboxViewController {
    func setupNavigationItem() {
        navigationItem.titleView = navigationTitleView()
        navigationItem.searchController = searchController
        navigationItem.setLeftBarButtonItems([settingsItem(), inboxItem()], animated: false)
        navigationItem.setRightBarButtonItems([composeItem()], animated: false)
    }

    private func navigationTitleView() -> UIView {
        let image = UIImageView(image: UIImage(named: "Texts")!)
        image.contentMode = .scaleAspectFit
        NSLayoutConstraint.activate([
            image.widthAnchor.constraint(equalToConstant: 20),
            image.heightAnchor.constraint(equalToConstant: 20)
        ])

        let label = UILabel()
        label.font = .preferredFont(forTextStyle: .headline)
        label.text = "Texts"

        let stack = UIStackView(arrangedSubviews: [image, label])
        stack.axis = .horizontal
        stack.spacing = 6

        return stack
    }

    func settingsItem() -> UIBarButtonItem {
        UIBarButtonItem(
            image: UIImage(systemName: "gear"), style: .plain,
            target: self, action: #selector(settingsItemPressed)
        )
    }

    func inboxItem() -> UIBarButtonItem {
        UIBarButtonItem(title: "", image: UIImage(systemName: inbox == .inbox ? "tray" : "tray.fill"), primaryAction: nil, menu: UIMenu(children: Inbox.allCases.map { inbox in
            UIAction(
                title: inbox.rawValue.capitalized,
                image: UIImage(systemName: "tray"),
                attributes: (inbox == .requests && !AppState.shared.rootStore.accountsStore.hasPlatformWithRequestsInbox ? .disabled : [])
            ) { _ in
                self.inbox = inbox
            }
        }))
    }

    private func composeItem() -> UIBarButtonItem {
        UIBarButtonItem(
            systemItem: .compose,
            primaryAction: UIAction { [unowned self] _ in
                let composeViewController = ComposeViewController.NavigationController(
                    initialAccount: nil,
                    accounts: AppState.shared.rootStore.accountsStore,
                    presentingNavigationController: navigationController
                )
                present(composeViewController, animated: true)
            },
            menu: UIMenu(children: AppState.shared.rootStore.accountsStore.accounts
                .filter {
                    $0.state.isActive && !$0.platform.attributes.contains(.noSupportSingleThreadCreation)
                }.map { account in
                    UIAction(title: account.user?.displayName ?? "", image: nil) { [unowned self] _ in
                        let composeViewController = ComposeViewController.NavigationController(
                            initialAccount: account,
                            accounts: AppState.shared.rootStore.accountsStore,
                            presentingNavigationController: navigationController
                        )
                        present(composeViewController, animated: true)
                    }
                }
            )
        )
    }

    @objc private func settingsItemPressed() {
        let viewController = UIHostingController(
            rootView: SettingsView().injectingTextsEnvironment()
        )
        present(viewController, animated: true)
    }
}
