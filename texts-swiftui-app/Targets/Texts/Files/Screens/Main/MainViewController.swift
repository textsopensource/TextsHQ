//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

class MainViewController: UISplitViewController {

    // MARK: - Children

    lazy var inboxViewController = InboxViewController()

    // MARK: - Init

    init() {
        super.init(style: .doubleColumn)
        minimumPrimaryColumnWidth = 320
    }

    required init?(coder: NSCoder) {
        nil
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        presentsWithGesture = false
        preferredDisplayMode = .oneBesideSecondary

        setViewController(inboxViewController, for: .primary)
    }
}
