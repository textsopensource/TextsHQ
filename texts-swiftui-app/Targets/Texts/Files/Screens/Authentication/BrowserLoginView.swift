//
// Copyright (c) Texts HQ
//

import os.log
import SwiftUIX
import TextsCore
import TextsUI
import WebKit
import Merge
import SwiftUI
import DomainParser

struct BrowserLoginView: View {
    private static let logger = Logger(category: "BrowserLogin")

    private class ViewModel: ObservableObject, CancellablesHolder {
        let platform: Schema.PlatformInfo
        let account: AccountInstance?
        let confirmAddAccount: (AccountInstance) async throws -> Void
        var cookieCancelable: AnyCancellable?

        init(
            platform: Schema.PlatformInfo,
            account: AccountInstance?,
            confirmAddAccount: @escaping (AccountInstance) async throws -> Void
        ) {
            self.platform = platform
            self.account = account
            self.confirmAddAccount = confirmAddAccount
        }

        var webViewConfig: (start: DynamicWebView.Page, script: DynamicWebView.Script)? {
            guard let login = platform.browserLogin ?? platform.browserLogins?.first,
                  let url = URL(string: login.loginURL)
            else {
                // TODO: throw instead of return nil
                return nil
            }
            var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData)
            request.httpShouldHandleCookies = false
            return (
                start: .remote(request),
                script: .init(
                    source: """
                    const close = window.close
                    window.close = () => {
                        webkit.messageHandlers['close'].postMessage('')
                        close.call(window)
                    }
                    \(login.runJSOnLaunch ?? "")
                    """,
                    injectionTime: .atDocumentEnd,
                    forMainFrameOnly: true
                )
            )
        }

        /// Initializes new platform
        private func authCallback(cookies: [HTTPCookie]?, jsCodeResult: String?) {
            BrowserLoginView.logger.debug("cookiesCallback(_:)")

            // turns out that HTTPCookie.domain sometimes leaves a leading dot.
            // yeah, that dot makes it fail.
            let domain: String?
            if let loginURL: String = (platform.browserLogin ?? platform.browserLogins?.first)?.loginURL,
               let url = URL(string: loginURL) {
                let domainParser = try? DomainParser()

                if let host = url.host, let parsed = domainParser?.parse(host: host) {
                    domain = parsed.domain
                } else {
                    domain = url.host
                }
            } else {
                domain = nil
            }

            var cookieJar: Schema.CookieJar?
            if let cookies = cookies {
                cookieJar = .init(cookies: cookies.map { .init(key: $0.name, value: $0.value, domain: domain ?? $0.domain, secure: $0.isSecure, path: $0.path) })
            }

            let creds: Schema.LoginCreds = .init(
                username: nil,
                password: nil,
                jsCodeResult: jsCodeResult,
                cookieJarJSON: cookieJar
            )

            let instance = account ?? AccountInstance(platform: platform)

            Task { @MainActor in
                let store = try await instance.initialize(withoutSession: true)
                let result = try await store.login(with: creds)

                if let error = AccountLoginError(result: result) {
                    throw error
                }

                try await confirmAddAccount(instance)
            }
        }

        func webView(_ webView: WKWebView, onCommit navigation: WKNavigation) {
            guard let login = platform.browserLogin ?? platform.browserLogins?.first else { return }
            if let runJSOnNavigate = login.runJSOnNavigate {
                webView.evaluateJavaScript(runJSOnNavigate)
            }
        }

        func webView(_ webView: WKWebView, onFinish navigation: WKNavigation) {
            guard let login = platform.browserLogin ?? platform.browserLogins?.first else { return }

            cookieCancelable = Timer.publish(every: 1, on: .current, in: .common)
                .autoconnect()
                .sink { [weak self] _ in
                    guard let self = self else { return }
                    let store = webView.configuration.websiteDataStore
                    store.httpCookieStore.getAllCookies { cookies in
                        if let jsToRun = login.runJSOnClose {
                            webView.evaluateJavaScript(jsToRun) { content, error in
                                if let error = error {
                                    BrowserLoginView.logger.error("Error while evaluating runJSOnClose: \("\(error)")")
                                }
                                if let cookieName = login.authCookieName, cookies.contains(where: { $0.name == cookieName }) {
                                    self.authCallback(cookies: cookies, jsCodeResult: content as? String)
                                    self.cookieCancelable = nil
                                }
                            }
                        } else if let cookieName = login.authCookieName, cookies.contains(where: { $0.name == cookieName }) {
                            self.authCallback(cookies: cookies, jsCodeResult: nil)
                            self.cookieCancelable = nil
                        }
                    }
                }
        }

        func handleClose(message: WKScriptMessage) {
            logger.debug("Window closing!")
            cookieCancelable = nil

            if let webView = message.webView,
               let jsToRun = (platform.browserLogin ?? platform.browserLogins?.first)?.runJSOnClose {
                logger.debug("Evaluating runJSOnClose")
                webView.callAsyncJavaScript(
                    "return await \(jsToRun)",
                    arguments: [:],
                    in: nil,
                    in: message.world
                ) { result in
                    switch result {
                    case .success(let content):
                        webView.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in
                            self.authCallback(cookies: cookies, jsCodeResult: content as? String)
                        }
                    case .failure(let error):
                        BrowserLoginView.logger.error("Error while evaluating runJSOnClose: \("\(error)")")
                    }
                }
            }
        }
    }

    @StateObject private var viewModel: ViewModel

    init(
        platform: Schema.PlatformInfo,
        account: AccountInstance?,
        confirmAddAccount: @escaping (AccountInstance) async throws -> Void

    ) {
        _viewModel = .init(wrappedValue: .init(
            platform: platform,
            account: account,
            confirmAddAccount: confirmAddAccount
        ))
    }

    var body: some View {
        if let config = viewModel.webViewConfig {
            DynamicWebView(start: config.start)
                .userAgent(Globals.Strings.defaultUserAgent)
                .onCommit { [weak viewModel] webView, navigation in
                    viewModel?.webView(webView, onCommit: navigation)
                }
                .onFinish { [weak viewModel] webView, navigation in
                    viewModel?.webView(webView, onFinish: navigation)
                }
                .script(config.script)
                .handle("close") { [weak viewModel] message in
                    viewModel?.handleClose(message: message)
                }
                .onDisappear { [weak viewModel] in
                    viewModel?.cookieCancelable = nil
                }
        }
    }
}
