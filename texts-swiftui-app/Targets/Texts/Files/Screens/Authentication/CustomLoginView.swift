//
// Copyright (c) Texts HQ
//

import Diagnostics
import Merge
import PASMobile
import SVGView
import Swallow
import SwiftUIX
import TextsCore
import TextsUI

// swiftlint:disable:next type_body_length
struct CustomLoginView: View {
    private static let logger = Logger(category: "CustomLoginView")

    private class ViewModel: ObservableObject, CancellablesHolder {
        let initialAccount: AccountInstance?

        private let accountInstance: AccountInstance
        private let closeHandler: (Bool) -> Void
        private let confirmAddAccount: (AccountInstance) async throws -> Void

        @PublishedObject private(set) var loadedAccount: AnyTask<AccountInstance, Error>

        private(set) var hasAdded = false

        init(
            platform: Schema.PlatformInfo,
            account: AccountInstance?,
            closeHandler: @escaping (Bool) -> Void,
            confirmAddAccount: @escaping (AccountInstance) async throws -> Void
        ) {
            let accountInstance = account ?? AccountInstance(platform: platform)

            self.initialAccount = account
            self.accountInstance = accountInstance
            self.closeHandler = closeHandler
            self.confirmAddAccount = confirmAddAccount

            loadedAccount = Task { @MainActor in
                try await accountInstance.initialize(withoutSession: true)
            }
            .publisher()
            .mapTo(accountInstance)
            .convertToTask()

            loadedAccount
                .successPublisher
                .subscribe(in: cancellables)
        }

        func textsProps() -> String {
            struct TextsProps: Encodable {
                let platformName: String
                let accountID: Schema.AccountID
                let isReauthing: Bool
            }

            return TextsProps(
                platformName: accountInstance.platform.name,
                accountID: accountInstance.id,
                isReauthing: initialAccount != nil
            )
            .toJSONString() ?? "{}"
        }

        func initialState() -> String {
            struct InitialState: Encodable {
                struct UserInfo: Encodable {
                    let username: String
                }
                let os_userInfo: UserInfo
                let os_release: String
                let os_arch: String
                let os_homedir: String
                let os_platform: String
                let os_devicetype: String
                let __dirname: String
            }

            return InitialState(
                os_userInfo: .init(username: UIDevice.current.name),
                os_release: UIDevice.current.systemVersion,
                os_arch: "arm64",
                os_homedir: NSHomeDirectory(),
                os_platform: "ios",
                os_devicetype: UIDevice.current.userInterfaceIdiom == .phone ? "phone" : "pad",
                __dirname: PASRunner.resources.path
            ).toJSONString() ?? "{}"
        }

        private func handleLogin() {
            guard case .success(let account) = loadedAccount.status else {
                return
            }

            Task { @MainActor in
                defer {
                    hasAdded = true
                }

                do {
                    _ = try await account.initialize(withoutSession: true)

                    try await confirmAddAccount(account)
                } catch {
                    AppState.shared.handleError(error)
                }
            }
        }

        func handleLoginResult(_ result: Schema.LoginResult) {
            CustomLoginView.logger.log("Handling login result \("\(result)")")
            guard result.type != .wait else {
                return
            }

            if let error = AccountLoginError(result: result) {
                AppState.shared.handleError(error)
                closeHandler(false)
            } else {
                handleLogin()
            }
        }

        deinit {
            if !hasAdded, case .success(let account) = loadedAccount.status {
                Task { @MainActor in
                    do {
                        try await account.accountsStore.disableAccount(account)
                    } catch {
                        AppState.shared.handleError(error)
                    }
                }
            }

            cancellables.cancel()
        }
    }

    let platform: Schema.PlatformInfo
    let account: AccountInstance?
    let closeHandler: (Bool) -> Void

    @StateObject private var viewModel: ViewModel

    init(
        platform: Schema.PlatformInfo,
        account: AccountInstance?,
        closeHandler: @escaping (Bool) -> Void,
        confirmAddAccount: @escaping (AccountInstance) async throws -> Void
    ) {
        self.platform = platform
        self.account = account
        self.closeHandler = closeHandler
        self._viewModel = .init(wrappedValue: .init(
            platform: platform,
            account: account,
            closeHandler: closeHandler,
            confirmAddAccount: confirmAddAccount
        ))
    }

    var body: some View {
        switch viewModel.loadedAccount.status {
        case .success(let ai):
            DynamicWebView(start: .html(
                """
                <meta name="viewport" content="initial-scale=1, width=device-width, user-scalable=no"/>
                <style>
                    body {
                        font-family: system-ui, -apple-system, BlinkMacSystemFont, sans-serif;
                    }

                    .text-center {
                        text-align: center
                    }

                    #root {
                        display: flex;
                        height: 100%;
                        align-items: center;
                        justify-content: center;
                    }

                    #root > div > * {
                        margin: 8px 0;
                    }

                    .list {
                        display: flex;
                        flex-direction: column;
                        align-items: flex-start;
                    }

                    .list > div {
                        margin: 8px 0;
                        display: flex;
                        align-items: center;
                    }

                    .list > div > span {
                        display: flex;
                        height: 24px;
                        width: 24px;
                        background-color: #bbb;
                        border-radius: 50%;
                        margin-right: 12px;
                        font-weight: bold;
                        justify-content: center;
                        align-items: center;
                        cursor: default;
                        font-size: 14px;
                        padding: 12px;
                        flex-shrink: 0;
                    }

                    button {
                        width: 100%;
                        height: 50px;
                        font-size: 20px;
                        border-radius: 8px;
                        border-color: #000;
                    }

                    form {
                        font-size: 20px;
                        max-width: 450px
                    }

                    form > div {
                        margin: 8px 0;
                    }

                    form input {
                        width: 100%;
                        height: 50px;
                        margin: 16px 0;
                        font-size: 20px;
                    }

                    footer {
                        border-top: 1px solid #dbdbdb;
                        font-weight: 400;
                        color: #6f6f6f;
                        font-size: 12px;
                        margin-top: 32px;
                        padding-top: 32px;
                        text-align: center;
                    }

                    @media (prefers-color-scheme: dark) {
                        body {
                            background: #1c1c1c;
                            color: #ccc;
                        }

                        button {
                            background: #292a30;
                            border-color: #FFF;
                            color: #ccc;
                        }

                        .list > div > span {
                            background-color: #313131;
                        }
                    }
                </style>
                <div id="root"></div>
                <script src="renderer.js"></script>
                """,
                base: PASRunner.resources
            ))
            .script(.init(
                source: """
                    globalThis.textsProps = \(viewModel.textsProps());
                    globalThis.INITIAL_STATE = \(viewModel.initialState());
                    """,
                injectionTime: .atDocumentStart,
                forMainFrameOnly: true
            ))
            .handle("handleLoginResult") { [weak viewModel] message in
                guard let viewModel = viewModel else { return }
                if let resultB64 = message.body as? String,
                   let result = Data(base64Encoded: resultB64),
                   let login = try? PlatformServer.makeDecoder().decode(Schema.LoginResult.self, from: result) {
                    viewModel.handleLoginResult(login)
                } else {
                    viewModel.handleLoginResult(.init(error: nil))
                }
            }
            .handle("api") { [weak viewModel, weak ai] message, reply in
                guard let viewModel = viewModel,
                      let ai = ai,
                      let body = message.body as? [String: String],
                      let methodNameRaw = body["methodName"],
                      let rawArgs = body["args"] else {
                    return
                }
                if body["hasCallback"] == "1" {
                    let methodNameJS = methodNameRaw.toJSONString()!
                    PlatformServer.shared.anyPublisher(for: methodNameRaw, on: ai)
                        .receiveOnMainQueue()
                        .sink { _ in
                        } receiveValue: { (value: Data) in
                            CustomLoginView.logger.debug("Received push for method \(methodNameRaw): \(value)")
                            guard let webView = message.webView else { return }
                            // safe to simply singlequote the string because none of the b64
                            // characters can escape
                            webView.evaluateJavaScript("""
                                globalThis.textsCallbacks[\(methodNameJS)]('\(value.base64EncodedString())')
                                """)
                        }
                        .store(in: viewModel.cancellables)
                    reply(.success(nil as String? as Any))
                } else {
                    guard let data = Data(base64Encoded: rawArgs) else {
                        return
                    }
                    PlatformServer.shared.callAnyMethod(methodNameRaw, on: ai, with: data)
                        .successPublisher
                        .sinkResult { (result: Result<Data, Error>) in
                            reply(result.map { $0.base64EncodedString() })
                        }
                        .store(in: viewModel.cancellables)
                }
            }
            .overlay(shareButton)
        case .error(let error):
            Text("An error occurred: \("\(error)")")
        default:
            ProgressView()
                .progressViewStyle(CustomLoginProgressViewStyle())
        }
    }

    var shareButton: some View {
        VStack {
            if platform.name == "signal" || platform.name == "whatsapp-baileys" {
                Button(action: shareAction) {
                    Image(systemName: "square.and.arrow.up")
                        .foregroundColor(.blue)
                }
                .buttonStyle(SmallCandyRectangleButtonStyle(transparent: false))
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topLeading)
                .padding()
            }
        }
    }

    private func shareAction() {
        guard let image = screenshot() else { return }

        let activityController = UIActivityViewController(
            activityItems: [image],
            applicationActivities: nil
        )

        let windows = UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .compactMap { $0 as? UIWindowScene }
            .first?.windows

        windows?.last?.rootViewController?.present(activityController, animated: true, completion: nil)
    }

    private func screenshot() -> UIImage? {
        let window = UIApplication.shared.connectedScenes
            .filter { $0.activationState == .foregroundActive }
            .compactMap { $0 as? UIWindowScene }
            .first?.windows
            .first { $0.isKeyWindow }

        guard let layer: CALayer = window?.layer else { return nil }

        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(layer.frame.size, false, scale)
        defer { UIGraphicsEndImageContext() }

        layer.render(in: UIGraphicsGetCurrentContext()!)

        return UIGraphicsGetImageFromCurrentImageContext()
    }
}

extension CustomLoginView {
    private struct CustomLoginProgressViewStyle: ProgressViewStyle {
        @State private var opacity: Double = 1

        private let animation = Animation
            .easeIn(duration: 1)
            .repeatForever()

        func makeBody(configuration: Configuration) -> some View {
            ZStack {
                Color.systemGroupedBackground
                    .ignoresSafeArea()

                Color.systemGray5
                    .mask(SVGView(string: Globals.Icons.texts))
                    .squareFrame(sideLength: 100)
                    .foregroundColor(.systemGray5)
                    .opacity(opacity)
                    .onAppear {
                        withAnimation(animation) {
                            opacity = 0
                        }
                    }
            }
        }
    }
}
