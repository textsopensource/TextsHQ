//
// Copyright (c) Texts HQ
//

import SVGView
import SwiftUIX
import TextsCore
import TextsUI
import Merge

struct ManualLoginView: View {
    @EnvironmentObject private var appState: AppState

    let platform: Schema.PlatformInfo

    @State var account: AccountInstance?

    let confirmAddAccount: (AccountInstance) async throws -> Void

    @State private var lastLoginResult: Schema.LoginResult?
    @State private var status: Status = .notLoggedIn(nil)
    @State private var username: String = ""
    @State private var password: String = ""
    @State private var code: String = ""

    private var canSubmit: Bool {
        !username.isEmpty && !password.isEmpty
    }

    var body: some View {
        VStack {
            if case .twoFactor = status {
                Button("Back", action: { status = .notLoggedIn(nil) })
                    .buttonStyle(SmallCandyRectangleButtonStyle(transparent: true, color: .primary))
                    .frame(maxWidth: .infinity, alignment: .topLeading)
            }

            Spacer()

            if let icon = platform.icon {
                SVGView(string: icon)
                    .frame(width: 92, height: 92)
            }

            Text(platform.displayName)
                .font(.largeTitle.weight(.bold))

            Spacer()

            inputFields

            Spacer()

            switch status {
            case .twoFactor(let error?), .notLoggedIn(let error?):
                Text(verbatim: "\(error)")
                    .foregroundColor(.red)
            default:
                // prevents the layout from shifting when the error
                // message is shown/hidden
                Text(verbatim: " ")
                    .visible(false)
                    .accessibilityHidden(true)
            }

            switch status {
            case .twoFactor, .verifyingCode:
                Button(action: performCodeVerification) {
                    HStack {
                        Text(status.base == .verifyingCode ? "..." : "Continue login as \(username)").fixedSize()
                        if status.base == .verifyingCode {
                            Spacer().fixedSize()
                            ProgressView()
                        }
                    }
                }
                .buttonStyle(CandyRectangleButtonStyle(fullWidth: true))
                .disabled(code.isEmpty || status.base == .verifyingCode)

            default:
                TaskButton(action: { await performLogin() }) {
                    HStack {
                        Text(status.base == .loggingIn ? "..." : "Login").fixedSize()
                        if status.base == .loggingIn {
                            Spacer().fixedSize()
                            ProgressView()
                        }
                    }
                }
                .buttonStyle(CandyRectangleButtonStyle(fullWidth: true))
                .disabled(!canSubmit || status.base == .loggingIn || status.base == .loggedIn)
            }

        }
        .padding()
        .animation(.default.speed(2), value: status.base)
    }

    @ViewBuilder
    private var inputFields: some View {
        switch status {
        case .twoFactor, .verifyingCode:
            CocoaTextField("One-time code", text: $code, onCommit: performCodeVerification)
                .returnKeyType(.go)
                .enablesReturnKeyAutomatically(true)
                .textContentType(.oneTimeCode)
                .keyboardType(.numberPad)
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .multilineTextAlignment(.center)
                .font(.headline)
                .padding()
                .background(Color.systemGray5)
                .cornerRadius(Globals.Views.cornerRadius)

        default:
            CocoaTextField("Username", text: $username)
                .enablesReturnKeyAutomatically(true)
                .isInitialFirstResponder(true)
                .textContentType(.username)
                .disableAutocorrection(true)
                .autocapitalization(.none)
                .multilineTextAlignment(.center)
                .font(.headline)
                .padding()
                .background(Color.systemGray5)
                .cornerRadius(Globals.Views.cornerRadius)

            CocoaTextField("Password", text: $password, onCommit: {
                Task { @MainActor in
                    await performLogin()
                }
            })
            .returnKeyType(.go)
            .enablesReturnKeyAutomatically(true)
            .secureTextEntry(true)
            .textContentType(.password)
            .disableAutocorrection(true)
            .autocapitalization(.none)
            .multilineTextAlignment(.center)
            .font(.headline)
            .padding()
            .background(Color.systemGray5)
            .cornerRadius(Globals.Views.cornerRadius)
        }
    }

    @MainActor
    private func performLogin() async {
        guard canSubmit else {
            return
        }

        self.status = .loggingIn

        UIDevice.current.generateHaptic(.light)

        let credentials = Schema.LoginCreds(
            username: username,
            password: password,
            jsCodeResult: nil,
            cookieJarJSON: nil
        )

        let instance = account ?? AccountInstance(platform: platform)

        self.account = instance

        do {
            _ = try await instance.initialize(withoutSession: true)

            let result = try await instance.login(withCredentials: credentials)

            lastLoginResult = result

            if let error = AccountLoginError(result: result) {
                throw error
            }

            try await afterAuth()
        } catch {
            if let error = error as? AccountLoginError, error.codeRequiredReason == .twoFactor {
                status = .twoFactor(nil)
            } else {
                status = .notLoggedIn(error)
            }
        }
    }

    private func performCodeVerification() {
        guard !code.isEmpty else {
            return
        }

        self.status = .verifyingCode

        if let instance = account, let lastLoginResult = lastLoginResult {
            Task {
                do {
                    let credentials = Schema.LoginCreds(
                        username: nil,
                        password: nil,
                        jsCodeResult: nil,
                        cookieJarJSON: nil,
                        code: code,
                        lastLoginResult: lastLoginResult
                    )
                    let result = try await instance.login(withCredentials: credentials)

                    if let error = AccountLoginError(result: result) {
                        throw error
                    }

                    try await afterAuth()
                } catch {
                    status = .twoFactor(error)
                }
            }
        } else {
            assertionFailure()
        }
    }

    @MainActor
    private func afterAuth() async throws {
        do {
            try await confirmAddAccount(account!)

            status = .loggedIn
        } catch {
            AppState.shared.handleError(error)
        }
    }
}

extension ManualLoginView {
    private enum Status {
        enum Base: Equatable {
            case notLoggedIn
            case failed
            case loggingIn
            case twoFactor
            case verifyingCode
            case loggedIn
        }

        case notLoggedIn(Error?)
        case loggingIn
        case twoFactor(Error?)
        case verifyingCode
        case loggedIn

        var base: Base {
            switch self {
            case .notLoggedIn(nil):
                return .notLoggedIn
            case .notLoggedIn(_?):
                return .failed
            case .loggingIn:
                return .loggingIn
            case .twoFactor:
                return .twoFactor
            case .verifyingCode:
                return .verifyingCode
            case .loggedIn:
                return .loggedIn
            }
        }
    }
}
