//
// Copyright (c) Texts HQ
//

import SwiftUI
import UIKit
import TextsCore
import TextsUI

struct AuthenticationView: View {
    @Environment(\.presentationMode) var presentationMode

    @EnvironmentObject private var appState: AppState

    @Binding var isShowingAddAccountView: Bool

    let platform: Schema.PlatformInfo

    // pass a non-nil value if reauthenticating an existing account,
    // otherwise (if nil) we're creating a new account
    let account: AccountInstance?

    init(
        isShowingAddAccountView: Binding<Bool>,
        platform: Schema.PlatformInfo,
        account: AccountInstance? = nil
    ) {
        self._isShowingAddAccountView = isShowingAddAccountView
        self.platform = platform
        self.account = account
    }

    var body: some View {
        loginView
            .overlay(closeButton)
    }

    @ViewBuilder private var loginView: some View {
        switch platform.loginMode {
        case .browser:
            BrowserLoginView(platform: platform, account: account, confirmAddAccount: confirmAddAccount)
                .edgesIgnoringSafeArea(.all)
        // FIXME: This is a hack
        case .custom where platform.name != "imessage":
            CustomLoginView(platform: platform, account: account, closeHandler: closeHandler, confirmAddAccount: confirmAddAccount)
        default:
            ManualLoginView(platform: platform, account: account, confirmAddAccount: confirmAddAccount)
        }
    }

    var closeButton: some View {
        Button("Close", action: { closeHandler() })
            .buttonStyle(SmallCandyRectangleButtonStyle(transparent: false))
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
            .padding()
    }

    private func confirmAddAccount(_ account: AccountInstance) async throws {
        let accountsStore = appState.rootStore.accountsStore

        try await account.refreshUserAndSession()

        guard let userID = account.user?.id else {
            return
        }

        if accountsStore.accounts.contains(where: { $0.user?.id == userID }) {
            let alert = UIAlertController(
                title: "An account with this \(account.platform.displayName) user already exists",
                message: nil,
                preferredStyle: .alert
            )

            alert.addAction(.init(title: "Delete Existing and Add", style: .default) { _ in
                Task { @MainActor in
                    closeHandler(success: true)
                    try await accountsStore.removeAllAccountsWithUserID(userID)
                    try await accountsStore.addAccount(account)
                }
            })

            alert.addAction(.init(title: "Don't Add", style: .cancel) { _ in
                Task {
                    closeHandler(success: true)
                    await account.logoutAndDispose()
                }
            })

            UIApplication.shared.topmostViewController?.present(alert, animated: true)
        } else {
            closeHandler(success: true)
            try await accountsStore.addAccount(account)
        }
    }

    private func closeHandler(success: Bool? = nil) {
        presentationMode.wrappedValue.dismiss()
        switch success {
        case true?:
            isShowingAddAccountView = false
            UIDevice.current.generateHaptic(.success)
        case false?:
            isShowingAddAccountView = false
            UIDevice.current.generateHaptic(.error)
        case nil:
            UIDevice.current.generateHaptic(.soft)
        }
    }
}
