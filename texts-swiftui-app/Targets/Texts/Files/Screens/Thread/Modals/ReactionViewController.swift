//
// Copyright (c) Texts HQ
//

import Foundation
import UIKit
import TextsCore
import ISEmojiView
import SwiftUIX

struct ReactionsPicker: View {
    struct Item: Identifiable {
        let key: String
        let reaction: Schema.SupportedReaction

        var id: some Hashable {
            key
        }
    }

    @ObservedObject var thread: ThreadStore

    let message: Schema.Message
    let configuration: Schema.ReactionInfo

    @State var selection: [ReactionPickerChoice]

    private let emojiSize = CGSize(width: 44, height: 44)

    private var multipleReactionsAreSupported: Bool {
        configuration.allowsMultipleReactionsToSingleMessage == true
    }

    private var items: [Item] {
        configuration.enabledReactions.map({ Item(key: $0.key, reaction: $0.value) })
    }

    var body: some View {
        if configuration.canReactWithAllEmojis == true {
            EmojiPickerView(didSelect: { toggle(.emoji($0)) })
        } else {
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 4) {
                    ForEach(enumerating: items) { index, item in
                        LazyAppearView {
                            ItemView(
                                item: item,
                                selection: $selection,
                                isSelected: Binding(
                                    get: { selection.contains(where: { $0 == .supportedReaction(item.key) }) },
                                    set: { newValue in
                                        if newValue {
                                            addReaction(choice: .supportedReaction(item.key))
                                        } else {
                                            removeReaction(choice: .supportedReaction(item.key))
                                        }
                                    }
                                )
                            )
                            .transition(.scale(scale: 0.8).combined(with: .opacity).animation(.default.speed(2).delay(Double(min(index, 4)) * 0.025)))
                        }
                    }
                }
                .padding(.small)
            }
        }
    }

    func toggle(_ reaction: ReactionPickerChoice) {
        if selection.contains(reaction) {
            removeReaction(choice: reaction)
        } else {
            addReaction(choice: reaction)
        }
    }

    func addReaction(choice: ReactionPickerChoice) {
        if multipleReactionsAreSupported {
            selection.append(choice)
        } else {
            selection = [choice]
        }

        Task { @MainActor in
            do {
                try await thread.addReaction(messageID: message.id, reaction: choice)
            } catch {
                AppState.shared.handleError(error)
            }
        }
    }

    func removeReaction(choice: ReactionPickerChoice) {
        selection.removeAll(of: choice)

        Task {
            do {
                try await thread.removeReaction(messageID: message.id, reaction: choice)
            } catch {
                AppState.shared.handleError(error)
            }
        }
    }
}

extension ReactionsPicker {
    struct ItemView: View {
        @Environment(\.presentationManager) var presentationManager

        let item: Item

        @Binding var selection: [ReactionPickerChoice]
        @Binding var isSelected: Bool

        var body: some View {
            Text(item.reaction.render ?? "")
                .font(.system(size: 30))
                .frame(width: 44, height: 44)
                .background {
                    if isSelected {
                        Color.accentColor.opacity(0.5).transition(.opacity.combined(with: .opacity))
                    } else {
                        Color.systemGray5.transition(.opacity.combined(with: .opacity))
                    }
                }
                .clipShape(Circle())
                .contentShape(Rectangle())
                .onPress {
                    Task { @MainActor in
                        withAnimation {
                            isSelected.toggle()
                        }

                        try? await Task.sleep(.milliseconds(400))

                        presentationManager.dismiss()
                    }
                }
        }
    }
}

// MARK: - Auxiliary Implementation -

class ReactionsPickerViewController: CocoaHostingController<ReactionsPicker>, UIPopoverPresentationControllerDelegate {
    let configuration: Schema.ReactionInfo

    override var preferredContentSize: CGSize {
        get {
            if configuration.canReactWithAllEmojis ?? false {
                return CGSize(width: 320, height: 500)
            } else {
                let emojiSize = CGSize(width: 44, height: 44)

                return CGSize(
                    width: 12 + (emojiSize.width + 4) * CGFloat(configuration.enabledReactions.count),
                    height: emojiSize.height + 16
                )
            }
        }
        set {

        }
    }

    init(
        thread: ThreadStore,
        message: Schema.Message,
        configuration: Schema.ReactionInfo,
        selected: [ReactionPickerChoice],
        sourceView: UIView
    ) {
        self.configuration = configuration

        super.init(
            mainView: .init(
                thread: thread,
                message: message,
                configuration: configuration,
                selection: selected
            )
        )

        modalPresentationStyle = .popover

        popoverPresentationController?.delegate = self
        popoverPresentationController?.permittedArrowDirections = [.down, .up]
        popoverPresentationController?.sourceView = sourceView
        popoverPresentationController?.sourceRect = sourceView.bounds
    }

    required init?(coder: NSCoder) {
        nil
    }

    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        .none
    }
}

private struct EmojiPickerView: UIViewRepresentable {
    typealias UIViewType = EmojiView

    class Coordinator: EmojiViewDelegate {
        var didSelect: (String) -> Void

        init(didSelect: @escaping (String) -> Void) {
            self.didSelect = didSelect
        }

        func emojiViewDidSelectEmoji(_ emoji: String, emojiView: EmojiView) {
            didSelect(emoji)
        }
    }

    let didSelect: (String) -> Void

    func makeUIView(context: Context) -> UIViewType {
        let view = UIViewType(keyboardSettings: .init(bottomType: .categories))

        view.delegate = context.coordinator

        return view
    }

    func updateUIView(_ uiView: UIViewType, context: Context) {
        context.coordinator.didSelect = didSelect
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(didSelect: didSelect)
    }
}
