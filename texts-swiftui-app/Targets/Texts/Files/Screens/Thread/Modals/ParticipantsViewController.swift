//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore

class ParticipantsViewController: UIHostingController<ProfileView> {
    convenience init(thread: ThreadStore) {
        self.init(rootView: ProfileView(thread: thread))
    }
}
