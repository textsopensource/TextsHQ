//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import WebKit
import AVKit

extension ThreadViewController {

    class MediasViewController: UINavigationController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
        struct Media: Equatable {
            let message: Schema.Message?
            let aspect: CGSize?
            let source: Source

            enum Source: Equatable {
                case image(UIImage), url(URL)
                case gif(URL)
                case video(URL)
                case audio(URL)
            }

            init?(message: Schema.Message, attachment: Any) {
                self.message = message

                if let image = attachment as? Image, let data = image.attachment.data, let image = UIImage(data: data) {
                    source = .image(image)
                    aspect = nil
                } else if let image = attachment as? Image, let url = image.url {
                    source = .url(url)
                    aspect = message.attachments?.first(
                        where: { $0.srcURL == url.absoluteString }
                    )?.size?.cgSize
                } else if let video = attachment as? Video, let url = video.url {
                    source = video.isGif ? .gif(url) : .video(url)
                    aspect = message.attachments?.first(
                        where: { $0.srcURL == url.absoluteString }
                    )?.size?.cgSize
                }
                /*
                 // TODO: Audio is disabled because it needs a custom player view
                 else if let audio = attachment as? Audio {
                      source = .audio(audio.url)
                }*/
                else {
                    return nil
                 }
            }

            init?(attachment: MessageDraft.LocalAttachment) {
                // TODO: Switch this properly on type
                message = nil
                source = .url(attachment.location.url)
                aspect = nil
            }

            var title: String {
                switch source {
                case .image, .url:
                    return "Photo"
                case .video:
                    return "Video"
                case .gif:
                    return "Gif"
                case .audio:
                    return "Audio"
                }
            }

            func viewController() -> MediaViewController {
                switch source {
                case .image, .url:
                    return ImageViewController(media: self)
                case .gif:
                    return GifViewController(media: self)
                case .video:
                    return VideoViewController(media: self)
                case .audio:
                    return VideoViewController(media: self)
                }
            }

            var shareItem: Any? {
                switch source {
                case .image(let image):
                    return image
                case .url(let url), .video(let url), .gif(let url), .audio(let url):
                    return url
                }
            }
        }

        // MARK: - Init

        typealias ReactionControllerProvider = (Schema.Message, UIView) -> UIViewController
        typealias ReplyController = (Schema.Message) -> Void

        var currentMedia: Media
        let medias: [Media]
        let reactionController: ReactionControllerProvider?
        let replyController: ReplyController?

        init(
            currentMedia: Media,
            medias: [Media],
            reactionController: ReactionControllerProvider? = nil,
            replyController: ReplyController? = nil
        ) {
            self.currentMedia = currentMedia
            self.medias = medias
            self.reactionController = reactionController
            self.replyController = replyController

            super.init(nibName: nil, bundle: nil)

            modalTransitionStyle = .crossDissolve
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - View

        private lazy var pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)

        override func viewDidLoad() {
            super.viewDidLoad()

            let navigationItemAppearance = UINavigationBarAppearance()
            navigationItemAppearance.configureWithDefaultBackground()
            pageViewController.navigationItem.compactAppearance = navigationItemAppearance
            pageViewController.navigationItem.standardAppearance = navigationItemAppearance
            pageViewController.navigationItem.scrollEdgeAppearance = navigationItemAppearance

            pageViewController.navigationItem.title = titleString()
            pageViewController.navigationItem.leftBarButtonItem = UIBarButtonItem(
                systemItem: .done,
                primaryAction: UIAction { [unowned self] _ in
                    self.dismiss(animated: true, completion: viewCache.removeAllObjects)
                }
            )

            configureToolbar()

            pageViewController.view.backgroundColor = .secondarySystemBackground
            pageViewController.dataSource = self
            pageViewController.delegate = self

            guard let currentIndex = medias.map(\.source).firstIndex(of: currentMedia.source) else {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    UINotificationFeedbackGenerator().notificationOccurred(.warning)
                    self.dismiss(animated: true)
                }
                return
            }
            pageViewController.setViewControllers([mediaViewController(for: currentIndex)], direction: .forward, animated: false)

            setViewControllers([pageViewController], animated: false)
            setToolbarHidden(false, animated: false)
        }

        private func configureToolbar() {
            var toolbarItems = [
                UIBarButtonItem(systemItem: .action, primaryAction: UIAction { [unowned self] _ in
                    guard
                        let shareItem = (self.pageViewController.viewControllers?.first as? MediaViewController)?.media.shareItem
                    else {
                        return
                    }

                    let shareViewController = UIActivityViewController(activityItems: [shareItem], applicationActivities: nil)
                    self.present(shareViewController, animated: true)
                })
            ]
            if let replyController = replyController {
                toolbarItems.append(contentsOf: [
                    .flexibleSpace(),
                    UIBarButtonItem(systemItem: .reply, primaryAction: UIAction { [unowned self] _ in
                        guard
                            let message = self.currentMedia.message
                        else {
                            return
                        }

                        replyController(message)
                    })
                ])
            }
            if let reactionController = reactionController {
                toolbarItems.append(contentsOf: [
                    .flexibleSpace(),
                    UIBarButtonItem(systemItem: .add, primaryAction: UIAction { [unowned self] _ in
                        guard
                            let message = self.currentMedia.message,
                            let sourceView = self.pageViewController.toolbarItems![toolbarItems.count - 1].value(forKey: "view") as? UIView
                        else {
                            return
                        }

                        let viewController = reactionController(message, sourceView)
                        self.present(viewController, animated: true)
                    })
                ])
            }
            pageViewController.toolbarItems = toolbarItems
        }

        // MARK: - Cache

        private lazy var viewCache = NSCache<NSString, MediaViewController>()

        private func mediaViewController(for index: Int) -> MediaViewController {
            guard let cachedView = viewCache.object(forKey: String(index) as NSString) else {
                viewCache.setObject(medias[index].viewController(), forKey: String(index) as NSString)
                return mediaViewController(for: index)
            }

            return cachedView
        }

        // MARK: - Pager

        func titleString() -> String {
            guard let index = medias.firstIndex(of: currentMedia) else { return "" }
            if medias.count > 1 {
                return "\(index + 1) of \(medias.count)"
            } else {
                return medias.first!.title
            }
        }

        func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
            guard
                let viewController = viewController as? MediaViewController,
                let index = medias.firstIndex(of: viewController.media),
                medias.indices.contains(index - 1)
            else {
                return nil
            }

            return mediaViewController(for: index - 1)
        }

        func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard
                let viewController = viewController as? MediaViewController,
                let index = medias.firstIndex(of: viewController.media),
                medias.indices.contains(index + 1)
            else {
                return nil
            }

            return mediaViewController(for: index + 1)
        }

        func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            guard
                let viewController = pageViewController.viewControllers?.first as? MediaViewController,
                let index = medias.firstIndex(of: viewController.media)
            else {
                return
            }

            currentMedia = medias[index]
            pageViewController.navigationItem.title = titleString()
        }
    }
}

// MARK: - Gif View Controller
extension ThreadViewController.MediasViewController {
    class GifViewController: ZoomingMediaViewController {
        lazy var webView = WKWebView()
        override var zoomView: UIView {
            webView
        }

        override func viewDidLoad() {
            super.viewDidLoad()
            guard
                case .gif(let url) = media.source,
                let size = media.aspect
            else {
                fatalError("Invalid media passed to GifViewController")
            }

            webView.backgroundColor = .clear
            webView.isOpaque = false
            webView.isUserInteractionEnabled = false

            webView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(webView)

            NSLayoutConstraint.activate([
                webView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
                webView.heightAnchor.constraint(equalTo: scrollView.widthAnchor, multiplier: size.height / size.width, constant: -1),
                webView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
                webView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor),
            ])

            webView.loadMessageAttachment(url: url)
        }
    }
}

// MARK: - Video View Controller
extension ThreadViewController.MediasViewController {
    class VideoViewController: MediaViewController {

        lazy var playerViewController = AVPlayerViewController()

        var playerView: UIView {
            playerViewController.view
        }

        override func viewDidLoad() {
            super.viewDidLoad()

            guard
                case .video(let url) = media.source,
                let size = media.aspect
            else {
                fatalError("Invalid media passed to PlayerViewController")
            }

            addChild(playerViewController)

            playerView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(playerView)

            NSLayoutConstraint.activate([
                playerView.heightAnchor.constraint(equalTo: view.widthAnchor, multiplier: size.height / size.width),
                playerView.widthAnchor.constraint(equalTo: view.widthAnchor),
                playerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
                playerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            ])
            playerViewController.didMove(toParent: self)

            playerViewController.player = AVPlayer(url: url)
        }

        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)

            playerViewController.player?.play()
        }

        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)
            playerViewController.player?.pause()
        }
    }
}

// MARK: - Single Image View
extension ThreadViewController.MediasViewController {
    class ImageViewController: ZoomingMediaViewController {
        lazy var imageView = UIImageView(frame: view.bounds)
        override var zoomView: UIView {
            imageView
        }

        override func viewDidLoad() {
            super.viewDidLoad()

            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            scrollView.addSubview(imageView)

            NSLayoutConstraint.activate([
                imageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
                imageView.heightAnchor.constraint(equalTo: scrollView.heightAnchor),
                imageView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor),
                imageView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor),
            ])

            switch media.source {
            case .image(let image):
                imageView.image = image
            case .url(let url):
                imageView.sd_setImage(with: url)
            default:
                fatalError("Invalid media passed to ImageViewController.")
            }
        }
    }
}

// MARK: - Base View Controllers
extension ThreadViewController.MediasViewController {
    class MediaViewController: UIViewController {
        let media: Media

        init(media: Media) {
            self.media = media
            super.init(nibName: nil, bundle: nil)
        }

        required init?(coder: NSCoder) {
            nil
        }
    }

    class ZoomingMediaViewController: MediaViewController, UIScrollViewDelegate {
        private(set) lazy var scrollView = UIScrollView(frame: view.bounds)
        var zoomView: UIView {
            fatalError("Must be overridden")
        }

        override func viewDidLoad() {
            super.viewDidLoad()

            scrollView.contentInsetAdjustmentBehavior = .never
            scrollView.minimumZoomScale = 1.0
            scrollView.maximumZoomScale = 4.0
            scrollView.decelerationRate = .fast
            scrollView.showsHorizontalScrollIndicator = false
            scrollView.showsVerticalScrollIndicator = false
            scrollView.delegate = self
            scrollView.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview(scrollView)

            NSLayoutConstraint.activate([
                scrollView.topAnchor.constraint(equalTo: view.topAnchor),
                scrollView.leftAnchor.constraint(equalTo: view.leftAnchor),
                scrollView.rightAnchor.constraint(equalTo: view.rightAnchor),
                scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])

            let doubleTapZoom = UITapGestureRecognizer(target: self, action: #selector(doubleTapZoom))
            doubleTapZoom.numberOfTapsRequired = 2

            let toggleBars = UITapGestureRecognizer(target: self, action: #selector(toggleBars))
            toggleBars.numberOfTapsRequired = 1
            toggleBars.require(toFail: doubleTapZoom)

            [doubleTapZoom, toggleBars].forEach(scrollView.addGestureRecognizer)
        }

        func viewForZooming(in scrollView: UIScrollView) -> UIView? {
            zoomView
        }

        @objc private func doubleTapZoom(_ gesture: UITapGestureRecognizer) {
            if scrollView.zoomScale != 1 {
                scrollView.setZoomScale(1, animated: true)
                if navigationController!.isNavigationBarHidden {
                    toggleBars()
                }
            } else {
                let scale = CGFloat(2.5)
                let zoomPoint = gesture.location(in: scrollView)
                let zoomFactor = 1.0 / scrollView.zoomScale
                let zoomTargetPoint = CGPoint(
                    x: zoomFactor * (zoomPoint.x + scrollView.contentOffset.x),
                    y: zoomFactor * (zoomPoint.y + scrollView.contentOffset.y)
                )
                let zoomTargetSize = CGSize(
                    width: scrollView.frame.width / scale,
                    height: scrollView.frame.height / scale
                )
                scrollView.zoom(
                    to: CGRect(
                        x: zoomTargetPoint.x - zoomTargetSize.width * 0.5,
                        y: zoomTargetPoint.y - zoomTargetSize.height * 0.5,
                        width: zoomTargetSize.width,
                        height: zoomTargetSize.height
                    ),
                    animated: true
                )
                if !navigationController!.isNavigationBarHidden {
                    toggleBars()
                }
            }
        }

        @objc private func toggleBars() {
            navigationController?.setNavigationBarHidden(!navigationController!.isNavigationBarHidden, animated: true)
            navigationController?.setToolbarHidden(!navigationController!.isToolbarHidden, animated: true)
        }
    }
}
