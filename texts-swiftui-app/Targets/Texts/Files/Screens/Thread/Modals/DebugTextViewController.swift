//
// Copyright (c) Texts HQ
//

import UIKit

class DebugTextViewController: UINavigationController {

    convenience init(text: String) {
        let viewController = UIViewController()
        viewController.navigationItem.title = "JSON"

        let textView = UITextView(frame: UIScreen.main.bounds)
        textView.isEditable = false
        textView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        textView.font = .monospacedSystemFont(ofSize: 12, weight: .regular)
        textView.text = text
        viewController.view.addSubview(textView)

        self.init(rootViewController: viewController)

        viewController.navigationItem.leftBarButtonItem = UIBarButtonItem(
            systemItem: .done,
            primaryAction: UIAction { [unowned viewController] _ in
                viewController.dismiss(animated: true)
            }
        )
        viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(
            systemItem: .action,
            primaryAction: UIAction { [unowned self] _ in
                let activityController = UIActivityViewController(activityItems: [text], applicationActivities: [])
                self.present(activityController, animated: true)
            }
        )
    }
}
