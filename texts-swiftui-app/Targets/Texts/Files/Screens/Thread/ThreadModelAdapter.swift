//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

///
/// ThreadMessageItemsProvider publishes `[ListSection<Date, MessageListItem>]`.
/// That data type works great in SwiftUI where one view can contain multiple cells, but MessageKit can not do that.
/// MessageKit also has its own protcol-based object spesification, so we need to conform to those.
///
/// We fix by mapping every Message from the API into multiple `MessageType` items, with one bubble for each component.
///
///
extension ThreadViewController {
    struct Sender: SenderType, Equatable {
        let senderId: String
        let displayName: String

        static let system = Sender(senderId: "system", displayName: "")
    }

    struct Message: MessageType, Identifiable, Equatable, Hashable {

        let payload: MessageListItem.MessagePayload?

        let sender: SenderType
        let messageId: String
        let sentDate: Date
        let kind: MessageKind

        var id: String {
            messageId
        }

        static func == (lhs: ThreadViewController.Message, rhs: ThreadViewController.Message) -> Bool {
            lhs.id == rhs.id
        }

        func hash(into hasher: inout Hasher) {
            hasher.combine(payload)
            hasher.combine(messageId)
            hasher.combine(sentDate)
        }
    }

    // MARK: Custom message types

    struct Action {
        let title: String
    }

    struct Audio {
        var url: URL
    }

    struct Avatars {
        let profiles: [Schema.User]
    }

    /// Wraps headerText and footerText
    struct DetailText {
        let text: String
    }

    struct Image: MediaItem {
        let attachment: Schema.MessageAttachment

        let url: URL?
        let data: Data?
        let image: UIImage?
        let placeholderImage: UIImage
        let size: CGSize
        let isSticker: Bool
    }

    struct Link {
        let url: URL?
        let displayUrl: String?
        let title: String?
        let summary: String?
        let image: URL?
        let favIcon: URL?
    }

    struct Reactions {
        let countedEmojies: [(String, Int)]
        // TODO: Pass users along with their reaction for menu
        // let usersReactions: [(Schema.User, String)]
    }

    struct SendError { }

    struct SharedIGPost {
        let avatarUrl: URL?
        let username: String

        let isAlbum: Bool
        let image: URL
        let imageSize: CGSize
        let caption: String

        let url: URL
    }

    struct Timestamp {
        let date: Date
    }

    struct Tweet {
        let tweet: Schema.Tweet
    }

    struct Unknown {
        let name: String
        let sourceUrl: URL?
        let variant: Variant

        enum Variant {
            case file, message
        }
    }

    struct Video: MediaItem {
        let url: URL?
        let image: UIImage?
        let placeholderImage: UIImage
        let size: CGSize
        let isGif: Bool
    }

    struct Quote {
        let displayName: String?
        let displayText: String
        let linkedId: String
    }

    struct Loading {
        static var shared = Message(
            payload: nil,
            sender: Sender.system,
            messageId: "loading",
            sentDate: .distantPast,
            kind: .custom(
                Loading()
            )
        )
    }

    enum SystemButtonType {
        case markAsReadInStealth
    }

    // MARK: - Converter

    func threadMappedToMessages() -> [Message] {
        [Loading.shared] + viewModel.itemsProvider.messageListItems.flatMap { section -> [Message] in
            [
                Message(
                    payload: nil,
                    sender: Sender.system,
                    messageId: String(section.model.timeIntervalSinceReferenceDate),
                    sentDate: section.model,
                    kind: .custom(
                        Timestamp(
                            date: section.model
                        )
                    )
                )
            ] + section.items.flatMap { item -> [Message] in
                guard case .message(let payload) = item else {
                    return []
                }

                return messages(for: payload)
            }
        }
        + (
            !viewModel.thread.displayIsUnread || viewModel.thread.isSending ? [] : [
                Message(
                    payload: nil,
                    sender: Sender.system,
                    messageId: String(Date().timeIntervalSinceReferenceDate),
                    sentDate: Date(),
                    kind: .custom(
                        SystemButtonType.markAsReadInStealth
                    )
                )
            ]
        )
    }

    // swiftlint:disable:next cyclomatic_complexity function_body_length
    func messages(for payload: MessageListItem.MessagePayload) -> [Message] {
        var payloadMessages = [Message]()
        let sender = payload.message.isSender == true ? currentSender() : Sender(senderId: payload.userID.rawValue, displayName: "")
        let id = payload.message.id.rawValue
        let date = payload.message.timestamp

        // - Header
        if let header = payload.message.textHeading {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: id + "-header",
                    sentDate: date,
                    kind: .custom(DetailText(text: header))
                )
            )
        }

        // - Links
        for (offset, link) in (payload.message.links ?? []).enumerated() {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-link-\(offset)",
                    sentDate: date,
                    kind: .custom(
                        Link(
                            url: URL(string: link.url),
                            displayUrl: link.originalURL,
                            title: link.title ?? link.url,
                            summary: link.summary,
                            image: link.img.flatMap(URL.init(string:)),
                            favIcon: link.favicon.flatMap(URL.init(string:))
                        )
                    )
                )
            )
        }

        // - Quote
        if let linked = payload.message.linkedMessage {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-quoted",
                    sentDate: date,
                    kind: .custom(
                        Quote(
                            displayName: viewModel.thread.getParticipant(id: linked.senderID)?.displayName,
                            displayText: linked.text,
                            linkedId: linked.id
                        )
                    )
                )
            )
        } else if let linkedId = payload.message.linkedMessageID {
            if let linked = viewModel.itemsProvider.thread.messages.items[id: linkedId] ?? viewModel.thread.linkedMessages[linkedId] {
                payloadMessages.append(
                    Message(
                        payload: payload,
                        sender: sender,
                        messageId: "\(id)-quoted",
                        sentDate: date,
                        kind: .custom(
                            Quote(
                                displayName: linked.senderID.flatMap {
                                    viewModel.thread.getParticipant(id: $0)?.displayName
                                },
                                displayText: {
                                    if let string = linked.displayString(in: viewModel.thread), !string.isEmpty {
                                        return string
                                    } else {
                                        guard let attachments = linked.attachments, !attachments.isEmpty else {
                                            return ""
                                        }
                                        if attachments.allSatisfy({ $0.type == .audio }) {
                                            return "\(Globals.Strings.audioAttachmentPrefix)"
                                        } else if attachments.allSatisfy({ $0.type == .img }) {
                                            return "\(Globals.Strings.imageAttachmentPrefix)"
                                        } else if attachments.allSatisfy({ $0.type == .video }) {
                                            return "\(Globals.Strings.videoAttachmentPrefix)"
                                        } else {
                                            return attachments.allSatisfy({ $0.data != nil })
                                                ? "\(Globals.Strings.fileAttachmentPrefix) "
                                                : "\(Globals.Strings.genericAttachmentPrefix) "
                                        }
                                    }
                                }(),
                                linkedId: linkedId.rawValue
                            )
                        )
                    )
                )
            } else {
                Task {
                    try await viewModel.itemsProvider.thread.account?.loadLinkedMessage(
                        threadID: viewModel.itemsProvider.thread.id,
                        messageID: linkedId
                    )
                }
            }
        }

        // - Action
        if payload.message.isAction == true {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: Sender.system,
                    messageId: "\(id)-action",
                    sentDate: date,
                    kind: .custom(
                        Action(
                            title: payload.message.displayString(in: viewModel.thread) ?? payload.message.action?.title ?? ""
                        )
                    )
                )
            )
        }

        // - Tweets
        payload.message.tweets?.enumerated().forEach { offset, tweet in
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-tweet-\(offset)",
                    sentDate: date,
                    kind: .custom(
                        Tweet(
                            tweet: tweet
                        )
                    )
                )
            )
        }

        // - Attachments
        payload.message.attachments?.enumerated().forEach { offset, attachment in
            let kind: MessageKind

            switch attachment.type {
            case .img where attachment.srcURL.isNotNil || attachment.data.isNotNil:
                kind = .custom(
                    Image(
                        attachment: attachment,
                        url: { urlString in
                            guard let urlString = urlString, let url = URL(string: urlString) else {
                                return nil
                            }
                            return url
                        }(attachment.srcURL),
                        data: attachment.data,
                        image: nil,
                        placeholderImage: .init(),
                        size: attachment.size?.cgSize ?? CGSize(width: 200, height: 150),
                        isSticker: attachment.isSticker ?? false
                    )
                )

            case .audio where attachment.srcURL.isNotNil:
                kind = .custom(
                    Audio(url: URL(string: attachment.srcURL!)!)
                )

            case .video where attachment.srcURL.isNotNil:
                let video = Video(
                    url: URL(string: attachment.srcURL!),
                    image: nil,
                    placeholderImage: .init(),
                    size: attachment.size?.cgSize ?? CGSize(width: 200, height: 150),
                    isGif: attachment.isGif ?? false
                )
                kind = .custom(video)

            default:
                kind = .custom(
                    Unknown(
                        name: attachment.fileName ?? "Unknown",
                        sourceUrl: attachment.srcURL.flatMap(URL.init(string:)),
                        variant: .file
                    )
                )
            }

            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-attachment-\(offset)",
                    sentDate: date,
                    kind: kind
                )
            )
        }

        // - Text
        if let text = payload.message.displayString(in: viewModel.thread), !text.isEmpty, payload.message.isAction != true {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: id,
                    sentDate: date,
                    kind: text.containsOnlyEmoji
                        ? .emoji(text)
                        : .text(text)
                )
            )
        }

        // - Footer
        if let footer = payload.message.textFooter {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: id + "-footer",
                    sentDate: date,
                    kind: .custom(
                        DetailText(text: footer)
                    )
                )
            )
        }

        // - Reactions
        if
            let configuration = try? viewModel.thread.account?.accountInstance().platform.reactions,
            let reactions = payload.message.reactions,
            !reactions.isEmpty
        {
            let reactions = configuration.supported
                .filter {
                    reactions.elements
                        .map(\.reactionKey)
                        .contains($0.key)
                }
                .compactMap {
                    $0.value.render
                }
                .reduce(into: [String: Int]()) { existing, reaction in
                    existing[reaction, default: 0] += 1
                }
                .sorted(by: { $0.1 < $1.1 })
                .map(keyPath: \.self)

            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-reactions",
                    sentDate: date,
                    kind: .custom(
                        Reactions(countedEmojies: reactions)
                    )
                )
            )
        }

        // - Seen
        let seenBy = viewModel.seenByParticipants(for: payload.message.id)
        if !seenBy.isEmpty {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-seen",
                    sentDate: date,
                    kind: .custom(
                        Avatars(profiles: seenBy)
                    )
                )
            )
        }

        // - Error
        if payload.message.isErrored == true {
            payloadMessages.append(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-error",
                    sentDate: date,
                    kind: .custom(
                        SendError()
                    )
                )
            )
        }

        // - Unknown
        if !payloadMessages.contains(where: {
            switch $0.kind {
            case .custom(let model) where model is Avatars || model is Reactions:
                return false
            default:
                return true
            }
        }) {
            payloadMessages.insert(
                Message(
                    payload: payload,
                    sender: sender,
                    messageId: "\(id)-unknown",
                    sentDate: date,
                    kind: .custom(
                        Unknown(
                            name: "Failed to parse message",
                            sourceUrl: nil,
                            variant: .message
                        )
                    )
                ), at: 0)
        }

        return payloadMessages
    }
}
