//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit

extension ThreadViewController {
    class Layout: MessagesCollectionViewFlowLayout {

        private lazy var loadingSizeCalculator = LoadingIndicatorCell.SizeCalculator(layout: self)
        private lazy var actionSizeCalculator = ActionCell.SizeCalculator(layout: self)
        private lazy var audioSizeCalculator = AudioCell.SizeCalculator(layout: self)
        private lazy var avatarsSizeCalculator = AvatarsCell.SizeCalculator(layout: self)
        private lazy var detailSizeCalculator = DetailTextCell.SizeCalculator(layout: self)
        private lazy var videoSizeCalculator = VideoCell.SizeCalculator(layout: self)
        private lazy var imageSizeCalculator = ImageCell.SizeCalculator(layout: self)
        private lazy var gifSizeCalculator = GifCell.SizeCalculator(layout: self)
        private lazy var linkSizeCalculator = LinkCell.SizeCalculator(layout: self)
        private lazy var reactionsSizeCalculator = ReactionsCell.SizeCalculator(layout: self)
        private lazy var sendErrorSizeCalculator = SendErrorCell.SizeCalculator(layout: self)
        private lazy var sharedIgSizeCalculator = SharedIGPostCell.SizeCalculator(layout: self)
        private lazy var timestampSizeCalculator = TimestampCell.SizeCalculator(layout: self)
        private lazy var tweetSizeCalculator = TweetCell.SizeCalculator(layout: self)
        private lazy var unknownSizeCalculator = UnknownCell.SizeCalculator(layout: self)
        private lazy var quoteSizeCalculator = QuoteCell.SizeCalculator(layout: self)
        private lazy var systemButtonSizeCalculator = SystemButtonCell.SizeCalculator(layout: self)

        // swiftlint:disable:next cyclomatic_complexity
        override open func cellSizeCalculatorForItem(at indexPath: IndexPath) -> CellSizeCalculator {
            if isSectionReservedForTypingIndicator(indexPath.section) {
                return typingIndicatorSizeCalculator
            }

            guard case .custom(let model) = messagesDataSource.messageForItem(
                at: indexPath,
                in: messagesCollectionView
            ).kind else {
                return super.cellSizeCalculatorForItem(at: indexPath)
            }

            switch model {
            case is Loading:
                return loadingSizeCalculator
            case is Action:
                return actionSizeCalculator
            case is Audio:
                return audioSizeCalculator
            case is Avatars:
                return avatarsSizeCalculator
            case is DetailText:
                return detailSizeCalculator
            case is Image:
                return imageSizeCalculator
            case let video as Video where video.isGif:
                return gifSizeCalculator
            case let video as Video where !video.isGif:
                return videoSizeCalculator
            case is Link:
                return linkSizeCalculator
            case is Reactions:
                return reactionsSizeCalculator
            case is SendError:
                return sendErrorSizeCalculator
            case is SharedIGPost:
                return sharedIgSizeCalculator
            case is Timestamp:
                return timestampSizeCalculator
            case is Tweet:
                return tweetSizeCalculator
            case is Unknown:
                return unknownSizeCalculator
            case is Quote:
                return quoteSizeCalculator
            case is SystemButtonType:
                return systemButtonSizeCalculator
            default:
                return super.cellSizeCalculatorForItem(at: indexPath)
            }
        }

        override func messageSizeCalculators() -> [MessageSizeCalculator] {
            super.messageSizeCalculators() + [
                audioSizeCalculator,
                avatarsSizeCalculator,
                detailSizeCalculator,
                imageSizeCalculator,
                gifSizeCalculator,
                videoSizeCalculator,
                linkSizeCalculator,
                reactionsSizeCalculator,
                sendErrorSizeCalculator,
                sharedIgSizeCalculator,
                tweetSizeCalculator,
                unknownSizeCalculator,
                quoteSizeCalculator,
            ]
        }
    }
}
