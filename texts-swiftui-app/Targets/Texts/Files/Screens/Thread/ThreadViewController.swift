//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore
import MessageKit
import IdentifiedCollections
import AVKit
import Combine
import Macaw

// swiftlint:disable:next type_body_length
class ThreadViewController: MessageKit.MessagesViewController {

    let viewModel: ThreadViewModel

    private lazy var _messagesCollectionView = MessagesCollectionView(frame: view.bounds, collectionViewLayout: Layout())
    override var messagesCollectionView: MessagesCollectionView {
        get { _messagesCollectionView }
        set { _messagesCollectionView = newValue }
    }

    // MARK: - Init

    init(viewModel: ThreadViewModel) {
        self.viewModel = viewModel

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        nil
    }

    // MARK: - Data

    var messages = IdentifiedArrayOf(uniqueElements: [Message]())

    private lazy var me = Sender(
        senderId: (try? viewModel.thread.account?.accountInstance().user?.id.rawValue) ?? "",
        displayName: (try? viewModel.thread.account?.accountInstance().user?.displayName) ?? ""
    )

    // MARK: - State

    /// State toggle indicating if the view has ever been dragged
    var hasBeenDragged = false

    /// State toggle indicating if the view has ever been scrolled down
    /// Set to true on first scroll down triggered by `updateForThreadChanges`
    var hasScrolledToBottom = false

    // MARK: - Reloading

    private weak var loadingIndicator: LoadingIndicatorCell?

    private var threadChangeObserver: AnyCancellable?

    private func configureThreadObserver() {
        threadChangeObserver = viewModel.objectWillChange.sink { [weak self] _ in
            // Wait one run loop so the changed object is available
            DispatchQueue.main.asyncAfter(deadline: .now() + .leastNonzeroMagnitude) { [weak self] in
                self?.updateForThreadChanges()
            }
        }
        viewModel.onLoadingChanged = { [weak self] isLoading in
            self?.loadingIndicator?.spinner.isHidden = !isLoading
            self?.loadingIndicator?.imageView.isHidden = isLoading || (self?.viewModel.canLoadMessages ?? false)
        }
    }

    private func updateForThreadChanges() {
        updateThreadActivities()

        let updatedMessages = IdentifiedArrayOf(uniqueElements: threadMappedToMessages())
        guard !updatedMessages.difference(from: messages).isEmpty else {
            print("Rejecting updateForThreadChanges because difference.isEmpty")
            return
        }
        messages = updatedMessages

        if messages.count > 1, !hasScrolledToBottom || !hasBeenDragged {
            messagesCollectionView.reloadData()
            messagesCollectionView.scrollToLastItem(animated: false)
            hasScrolledToBottom = true
        } else {
            messagesCollectionView.reloadDataAndKeepOffset()
        }

        if hasScrolledToBottom, messagesCollectionView.alpha.isZero {
            UIView.animate(withDuration: 0.12) {
                self.messagesCollectionView.alpha = 1
            }
        }

        viewModel.markAsReadIfNeeded()
        print("Completed smartReloadData()")
    }

    private func updateThreadActivities() {
        var label: String = ""

        if !viewModel.thread.activityIndicatorMessages.isEmpty,
           let activityMessage = viewModel.thread.activityIndicatorMessages.first {
            label = activityMessage
        } else {
            if viewModel.thread.type == .group {
                label = "\(viewModel.thread.participants.count) Members"
            } else if let subtitle = viewModel.thread.getSubtitle() {
                label = subtitle
            }
        }

        UIView.animate(withDuration: 0.2, animations: {
            self.activityLabel.isHidden = label.isEmpty
            self.activityLabel.text = label
            self.navigationTitleStack.layoutIfNeeded()
        })
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureInputBar()
        configureMessageLayout()
        configureMessageCollectionView()
        configureGestures()
        configureCellMenuItems()
        configureThreadObserver()

        messagesCollectionView.alpha = .zero
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        Task { @MainActor in
            try await viewModel.thread.account?.onThreadSelected(threadID: viewModel.thread.id)
        }

        viewModel.markAsReadIfNeeded()
        markRelevantNotificationsAsSeen()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        Task { @MainActor in
            try await viewModel.thread.sendStopTypingIndicator()
            try await viewModel.thread.account?.onThreadSelected(threadID: nil)
        }

        AudioCell.audioCache.removeAllObjects()
        threadChangeObserver?.cancel()
        scrollObserver?.invalidate()
        if let observer = keyboardNotificationToken {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    // MARK: - Scroll to bottom

    override func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        super.scrollViewWillBeginDragging(scrollView)
        registerFirstInteraction()
    }

    override func scrollViewDidScrollToTop(_ scrollView: UIScrollView) {
        super.scrollViewDidScrollToTop(scrollView)
        registerFirstInteraction()
    }

    private func registerFirstInteraction() {
        guard !hasBeenDragged else {
            return
        }
        hasBeenDragged = true
        configureScrollToBottomButton()
    }

    // MARK: - Setup Collection View

    private func configureMessageCollectionView() {
        messagesCollectionView.keyboardDismissMode = .onDrag
        messagesCollectionView.messageCellDelegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self

        messagesCollectionView.register(LoadingIndicatorCell.self)

        messagesCollectionView.register(ActionCell.self)
        messagesCollectionView.register(AudioCell.self)
        messagesCollectionView.register(AvatarsCell.self)
        messagesCollectionView.register(DetailTextCell.self)
        messagesCollectionView.register(ImageCell.self)
        messagesCollectionView.register(GifCell.self)
        messagesCollectionView.register(VideoCell.self)

        LinkCell.Style.allCases.forEach {
            messagesCollectionView.register(
                LinkCell.self, // use reuseId to layout with or without image
                forCellWithReuseIdentifier: $0.reuseIdentifier
            )
        }

        messagesCollectionView.register(ReactionsCell.self)
        messagesCollectionView.register(SendErrorCell.self)
        messagesCollectionView.register(SharedIGPostCell.self)
        messagesCollectionView.register(TimestampCell.self)

        (0...4).forEach { mosaicTiles in
            messagesCollectionView.register(
                TweetCell.self, // use reuseId to pass image count so view can be setup correctly
                forCellWithReuseIdentifier: "\(TweetCell.self)-\(mosaicTiles)"
            )
        }
        messagesCollectionView.register(UnknownCell.self)
        messagesCollectionView.register(QuoteCell.self)
        messagesCollectionView.register(SystemButtonCell.self)
    }

    private var layout: Layout {
        messagesCollectionView.messagesCollectionViewFlowLayout as? Layout ?? Layout()
    }

    private func configureMessageLayout() {
        additionalSafeAreaInsets.top = 40

        scrollsToLastItemOnKeyboardBeginsEditing = true
        maintainPositionOnKeyboardFrameChanged = true
        showMessageTimestampOnSwipeLeft = true

        let horizontalInset = view.frame.inset(by: view.safeAreaInsets).width * 0.25
        layout.setMessageIncomingMessagePadding(.init(top: 0, left: 4, bottom: 0, right: horizontalInset))
        layout.setMessageOutgoingMessagePadding(.init(top: 0, left: horizontalInset, bottom: 0, right: 0))

        let messagePadding = UIEdgeInsets(top: 8, left: 12, bottom: 8, right: 12)
        layout.textMessageSizeCalculator.incomingMessageLabelInsets = messagePadding
        layout.textMessageSizeCalculator.outgoingMessageLabelInsets = messagePadding

        layout.emojiMessageSizeCalculator.incomingMessagePadding = .init(top: 0, left: 4, bottom: 0, right: horizontalInset)
        layout.emojiMessageSizeCalculator.outgoingMessagePadding = .init(top: 0, left: horizontalInset, bottom: 0, right: 0)

        layout.emojiMessageSizeCalculator.incomingMessageLabelInsets = .zero
        layout.emojiMessageSizeCalculator.outgoingMessageLabelInsets = .zero

        layout.setMessageOutgoingAvatarSize(.init(width: 5, height: 5))
        if viewModel.thread.participants.count < 3 {
            layout.setMessageIncomingAvatarSize(.zero)
        }
    }

    // MARK: - Setup Navigation Item

    private lazy var navigationTitleStack = UIStackView()
    private lazy var activityLabel = UILabel().redactInPrivacyMode()

    private func configureNavigationItem() {
        let titleContainer = UIView()

        navigationTitleStack.spacing = -1
        navigationTitleStack.axis = .vertical

        let usernameLabel = UILabel().redactInPrivacyMode()
        usernameLabel.font = .preferredFont(forTextStyle: .headline)
        usernameLabel.text = viewModel.thread.displayName()

        activityLabel.font = .preferredFont(forTextStyle: .caption1)
        activityLabel.textColor = .secondaryLabel

        navigationTitleStack.addArrangedSubview(usernameLabel)
        navigationTitleStack.addArrangedSubview(activityLabel)

        // Initialize Labels
        updateThreadActivities()

        let avatarView = ThreadAvatarView(
            size: .navigationBar,
            source: .init(thread: viewModel.thread)
        ).redactInPrivacyMode()

        [avatarView, navigationTitleStack].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            titleContainer.addSubview($0)
        }

        titleContainer.translatesAutoresizingMaskIntoConstraints = false
        titleContainer.setContentHuggingPriority(.required, for: .horizontal)

        NSLayoutConstraint.activate([
            avatarView.leftAnchor.constraint(equalTo: titleContainer.leftAnchor),
            avatarView.centerYAnchor.constraint(equalTo: titleContainer.centerYAnchor),

            navigationTitleStack.leftAnchor.constraint(equalTo: avatarView.rightAnchor, constant: 6),
            navigationTitleStack.centerYAnchor.constraint(equalTo: titleContainer.centerYAnchor),
            navigationTitleStack.rightAnchor.constraint(equalTo: titleContainer.rightAnchor)
        ])

        navigationItem.titleView = titleContainer
        navigationItem.rightBarButtonItems = [
            UIBarButtonItem(
                image: UIImage(systemName: "info.circle"),
                style: .plain, target: self,
                action: #selector(infoTapped)
            ),
            UIBarButtonItem(
                customView: PlatformIconView(thread: viewModel.thread)
            ),
        ]

        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.shadowColor = .clear

        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
    }

    // MARK: - Setup Gestures

    private lazy var reactionGesture = UITapGestureRecognizer(target: self, action: #selector(reactionGestureTriggered))

    private func configureGestures() {
        reactionGesture.numberOfTapsRequired = 2
        if case .some = reactionInfo {
            messagesCollectionView.addGestureRecognizer(reactionGesture)
        }

        let debugGesture = UITapGestureRecognizer(target: self, action: #selector(debugGestureTriggered))
        debugGesture.numberOfTouchesRequired = 2
        messagesCollectionView.addGestureRecognizer(debugGesture)
    }

    // MARK: - Setup Scroll to Bottom

    private var scrollObserver: NSKeyValueObservation?

    private func configureScrollToBottomButton() {
        let button = UIButton(
            frame: CGRect(x: 0, y: 0, width: 40, height: 40),
            primaryAction: UIAction { [unowned self] _ in
                UISelectionFeedbackGenerator().selectionChanged()
                self.messagesCollectionView.scrollToLastItem(animated: true)
            }
        )
        button.setImage(
            UIImage(
                systemName: "arrow.down.to.line",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .bold)),
            for: .normal
        )
        button.tintColor = UIColor(.accentColor)
        button.backgroundColor = UIColor(.accentColor).withAlphaComponent(0.25)
        button.layer.masksToBounds = false
        button.layer.cornerRadius = inputController.textView.minHeight() / 2
        button.alpha = 0

        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        NSLayoutConstraint.activate([
            button.widthAnchor.constraint(equalToConstant: button.layer.cornerRadius * 2),
            button.heightAnchor.constraint(equalTo: button.widthAnchor),
            button.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16),
            (viewModel.thread.isReadOnly
                ? button.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
                : button.bottomAnchor.constraint(equalTo: inputController.topAnchor, constant: -16)
            )
        ])

        scrollObserver = messagesCollectionView.observe(\.contentOffset) { [unowned button] collectionView, _ in
            let targetAlpha = collectionView.contentSize.height - collectionView.bounds.maxY > 500 ? 1.0 : CGFloat.zero
            if targetAlpha != button.alpha {
                UIView.animateKeyframes(withDuration: 0.2, delay: 0) {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1.0) {
                        button.transform = targetAlpha == 1 ? .identity : .init(scaleX: 0.1, y: 0.1)
                    }
                    UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                        button.alpha = targetAlpha
                    }
                }
            }
        }
    }

    // MARK: - Notifications

    private func markRelevantNotificationsAsSeen() {
        UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
            let relevantIdentifiers = notifications
                .filter { $0.request.content.threadIdentifier == self.viewModel.thread.id.rawValue }
                .map { $0.request.identifier }
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: relevantIdentifiers)
        }
    }

    // MARK: - Input Bar

    lazy var inputController = InputController()

    private func configureInputBar() {
        guard !viewModel.thread.isReadOnly else {
            return
        }

        inputController.thread = viewModel.thread
        inputController.autoInsetAdjustingThreadViewController = self
        inputController.modalPresentationController = self

        inputController.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(inputController)
        view.addLayoutGuide(keyboardGuide)

        NSLayoutConstraint.activate([
            keyboardGuide.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            keyboardGuide.leftAnchor.constraint(equalTo: view.leftAnchor),
            keyboardGuide.rightAnchor.constraint(equalTo: view.rightAnchor),
            keyboardGuideHeightAnchor,

            inputController.bottomAnchor.constraint(equalTo: keyboardGuide.topAnchor),
            inputController.contentView.bottomAnchor.constraint(lessThanOrEqualTo: keyboardGuide.topAnchor, constant: -8),
            inputController.contentView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor),
            inputController.leftAnchor.constraint(equalTo: view.leftAnchor),
            inputController.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])

        observeAndOffsetFromKeyboard()
    }

    override var inputAccessoryView: UIView? {
        nil
    }

    private var keyboardGuide = UILayoutGuide()
    private lazy var keyboardGuideHeightAnchor = keyboardGuide.heightAnchor.constraint(equalToConstant: 0)
    private var keyboardNotificationToken: Any?

    private func observeAndOffsetFromKeyboard() {
        keyboardNotificationToken = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillChangeFrameNotification,
            object: nil, queue: .main
        ) { notification in
            guard
                let targetFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
                let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
                let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
            else {
                return
            }

            self.keyboardGuideHeightAnchor.constant = UIScreen.main.bounds.intersection(targetFrame).height

            UIView.animate(
                withDuration: TimeInterval(truncating: animationDuration),
                delay: 0.0,
                options: .init(rawValue: UInt(truncating: animationCurve)),
                animations: {
                    self.view.layoutIfNeeded()
                }
            )
        }
    }

    // MARK: - Layout

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if isSectionReservedForTypingIndicator(section) {
            return .zero
        }

        let isDifferentSender = section == 0 ? true : messages[section - 1].sender.senderId != messages[section].sender.senderId
        let difference = section == 0 ? 0 : messages[section - 1].sentDate.timeIntervalSince(messages[section].sentDate).absoluteValue

        let top: CGFloat
        if isDifferentSender || difference > LayoutConstants.GAPPED_3_DURATION {
            top = 12
        } else if difference > LayoutConstants.GAPPED_2_DURATION {
            top = 6
        } else if difference > LayoutConstants.GAPPED_1_DURATION {
            top = 3
        } else {
            top = 1
        }

        switch messages[section].kind {
        case .custom(let model) where false
        //  🍄
            || model is Audio
            || model is Avatars
            || model is DetailText
            || model is Image
            || model is Video
            || model is Link
            || model is Reactions
            || model is SendError
            || model is SharedIGPost
            || model is Tweet
            || model is Unknown
            || model is Quote
        :
            let remainingWidth = collectionView.frame.width - layout.cellSizeCalculatorForItem(
                at: IndexPath(item: 0, section: section)
            ).sizeForItem(
                at: IndexPath(item: 0, section: section)
            ).width
            - layout.textMessageSizeCalculator.incomingAvatarSize.width
            - 12

            return UIEdgeInsets(
                top: (model is Tweet || model is Link || model is Quote) ? 8 : top,
                left: isFromCurrentSender(message: messages[section])
                    ? remainingWidth
                    : layout.textMessageSizeCalculator.incomingAvatarSize.width + 12,
                bottom: 0,
                right: isFromCurrentSender(message: messages[section])
                    ? 12
                    : remainingWidth
            )

        case .custom(let model) where model is Timestamp || model is Loading:
            return UIEdgeInsets(
                top: 32,
                left: 0,
                bottom: 0,
                right: 0
            )

        case .custom(let model) where model is Action || model is SystemButtonType:
            return UIEdgeInsets(
                top: 16,
                left: 0,
                bottom: 0,
                right: 0
            )

        case .custom(let model) where model is Loading:
            return .zero

        default:
            return UIEdgeInsets(
                top: top,
                left: 0,
                bottom: 0,
                right: 0
            )
        }
    }

    // MARK: - Overrides

    // swiftlint:disable:next cyclomatic_complexity function_body_length
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if isSectionReservedForTypingIndicator(indexPath.section) {
            return typingIndicator(at: indexPath, in: messagesCollectionView)
        }

        let message = messageForItem(at: indexPath, in: messagesCollectionView)
        let messageCell: UICollectionViewCell

        if case .custom(let model) = message.kind {
            switch model {
            case is Loading:
                let cell = messagesCollectionView.dequeueReusableCell(LoadingIndicatorCell.self, for: indexPath)
                cell.configure(isHidden: !viewModel.isLoadingMessages)
                cell.imageView.isHidden = viewModel.isLoadingMessages || viewModel.canLoadMessages
                loadingIndicator = cell
                messageCell = cell

            case let action as Action:
                let cell = messagesCollectionView.dequeueReusableCell(ActionCell.self, for: indexPath)
                cell.configure(with: action)
                messageCell = cell

            case let audio as Audio:
                let cell = messagesCollectionView.dequeueReusableCell(AudioCell.self, for: indexPath)
                cell.configure(with: audio, isSender: isFromCurrentSender(message: message))
                messageCell = cell

            case let avatars as Avatars:
                let cell = messagesCollectionView.dequeueReusableCell(AvatarsCell.self, for: indexPath)
                cell.configure(with: avatars, isSender: isFromCurrentSender(message: message))
                cell.menuProvider = { [unowned self] in
                    self.userListMenu(with: avatars.profiles)
                }
                messageCell = cell

            case let detail as DetailText:
                let cell = messagesCollectionView.dequeueReusableCell(DetailTextCell.self, for: indexPath)
                cell.configure(with: detail, isSender: isFromCurrentSender(message: message))
                messageCell = cell

            case let image as Image:
                let cell = messagesCollectionView.dequeueReusableCell(ImageCell.self, for: indexPath)
                cell.configure(with: image)
                cell.onTap = { [unowned self] cell in
                    self.didTapMediaCell(cell)
                }
                cell.tapGesture.require(toFail: reactionGesture)
                messageCell = cell

            case let video as Video where !video.isGif:
                let cell = messagesCollectionView.dequeueReusableCell(VideoCell.self, for: indexPath)
                cell.configure(with: video)
                cell.onTap = { [unowned self] cell in
                    self.didTapMediaCell(cell)
                }
                cell.contentView.gestureRecognizers?.forEach {
                    ($0 as? UITapGestureRecognizer)?.require(toFail: reactionGesture)
                }
                messageCell = cell

            case let video as Video where video.isGif:
                let cell = messagesCollectionView.dequeueReusableCell(GifCell.self, for: indexPath)
                cell.configure(with: video.url!)
                cell.onTap = { [unowned self] cell in
                    self.didTapMediaCell(cell)
                }
                cell.contentView.gestureRecognizers?.forEach {
                    ($0 as? UITapGestureRecognizer)?.require(toFail: reactionGesture)
                }
                messageCell = cell

            case let link as Link:
                let identifier = (link.image.isNil ? LinkCell.Style.plain : .image).reuseIdentifier
                guard let cell = messagesCollectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? LinkCell else {
                    fatalError("Could not dequeue style-indicated LinkCell")
                }
                cell.configure(with: link)
                messageCell = cell

            case let reactions as Reactions:
                let cell = messagesCollectionView.dequeueReusableCell(ReactionsCell.self, for: indexPath)
                cell.configure(with: reactions, isSender: isFromCurrentSender(message: message))
                cell.menuProvider = { [unowned self] in
                    print("TODO: Implement a list of all reactions")
                    return self.reactionListMenu()
                }
                messageCell = cell

            case is SendError:
                let cell = messagesCollectionView.dequeueReusableCell(SendErrorCell.self, for: indexPath)
                cell.menuProvider = { [unowned self] in
                    self.sendErrorMenu(at: indexPath)
                }
                messageCell = cell

            case let sharedPost as SharedIGPost:
                let cell = messagesCollectionView.dequeueReusableCell(SharedIGPostCell.self, for: indexPath)
                cell.configure(with: sharedPost, isSender: isFromCurrentSender(message: message))
                messageCell = cell

            case let timestamp as Timestamp:
                let cell = messagesCollectionView.dequeueReusableCell(TimestampCell.self, for: indexPath)
                cell.configure(with: timestamp)
                messageCell = cell

            case let tweet as Tweet:
                let count = tweet.tweet.attachments?.count ?? 0
                let identifier = String(describing: TweetCell.self) + "-" + String(count)
                guard let cell = messagesCollectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? TweetCell else {
                    fatalError("Could not dequeue image count-indicated TweetCell")
                }
                cell.configure(with: tweet)
                cell.contentView.gestureRecognizers?.forEach {
                    ($0 as? UITapGestureRecognizer)?.require(toFail: reactionGesture)
                }
                messageCell = cell

            case let unknown as Unknown:
                let cell = messagesCollectionView.dequeueReusableCell(UnknownCell.self, for: indexPath)
                cell.configure(with: unknown)
                cell.onTap = { [unowned self] in
                    guard let json = messages[indexPath.section].payload?.message.toJSONString(prettyPrint: true) else {
                        return
                    }
                    self.present(
                        DebugTextViewController(text: json),
                        animated: true
                    )
                }
                messageCell = cell

            case let quote as Quote:
                let cell = messagesCollectionView.dequeueReusableCell(QuoteCell.self, for: indexPath)
                cell.configure(with: quote)
                cell.onTap = { [unowned self] cell in
                    self.didTapQuoteCell(cell)
                }
                cell.contentView.gestureRecognizers?.forEach {
                    ($0 as? UITapGestureRecognizer)?.require(toFail: reactionGesture)
                }
                messageCell = cell

            case let systemButtonType as SystemButtonType:
                let cell = messagesCollectionView.dequeueReusableCell(SystemButtonCell.self, for: indexPath)
                cell.configure(with: systemButtonType)
                cell.onPress = { [unowned self] in
                    Task {
                        try await viewModel.thread.markAsRead()
                    }
                }
                messageCell = cell

            default:
                messageCell = super.collectionView(collectionView, cellForItemAt: indexPath)
            }
        } else {
            messageCell = super.collectionView(collectionView, cellForItemAt: indexPath)
        }

        return messageCell.redactInPrivacyMode()
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        super.collectionView(collectionView, willDisplay: cell, forItemAt: indexPath)

        if indexPath.section == 3 {
            loadMoreMessages()
        }
    }

    // MARK: - Cell Menu

    private var isQuotesEnabled: Bool {
        (try? viewModel.thread.account?.accountInstance().platform.attributes.contains(.supportsQuotedMessages)) ?? false
    }

    private var reactionInfo: Schema.ReactionInfo? {
        try? viewModel.thread.account?.accountInstance().platform.reactions
    }

    private func configureCellMenuItems() {
        UIMenuController.shared.menuItems = [
            UIMenuItem(
                title: isQuotesEnabled ? "Reply" : "Quote",
                action: #selector(MessageCollectionViewCell.quote(_:))
            )
        ]

        if case .some = reactionInfo {
            UIMenuController.shared.menuItems!.append(
                UIMenuItem(
                    title: "React",
                    action: #selector(MessageCollectionViewCell.react(_:))
                )
            )
        }
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        switch action {
        case NSSelectorFromString("delete:"):
            return false

        default:
            return super.collectionView(collectionView, canPerformAction: action, forItemAt: indexPath, withSender: sender)
        }
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        switch action {
        case NSSelectorFromString("delete:"):
            Task { @MainActor in
                try await viewModel.thread.deleteMessage(withID: messages[indexPath.section].payload!.message.id)
            }
        default:
            super.collectionView(collectionView, performAction: action, forItemAt: indexPath, withSender: sender)
        }
    }

    func quoteMessage(at indexPath: IndexPath) {
        guard let message = messages[indexPath.section].payload?.message else {
            return
        }

        quoteMessage(message)
    }

    func quoteMessage(_ message: Schema.Message) {
        inputController.replyTo(.init(
            type: .init(platformQuoteSupport: isQuotesEnabled),
            message: message,
            displayName: message.senderID.flatMap { viewModel.thread.getParticipant(id: $0)?.displayName },
            displayText: (message.displayString(in: viewModel.thread) ?? "").replacingOccurrences(of: "\n", with: " ")
        ))

        inputController.textView.becomeFirstResponder()
    }

    func reactMessage(at indexPath: IndexPath, sourceView: UIView) {
        guard let message = messages[indexPath.section].payload?.message else {
            return
        }

        let viewController = reactionViewController(for: message, sourceView: sourceView)
        present(viewController, animated: true)
    }

    func reactionViewController(for message: Schema.Message, sourceView: UIView) -> UIViewController {
        guard
            let configuration = try? viewModel.thread.account?.accountInstance().platform.reactions
        else {
            fatalError("No reactions available")
        }

        return ReactionsPickerViewController(
            thread: viewModel.thread,
            message: message,
            configuration: configuration,
            selected: message.reactions?.elements.map {
                ReactionPickerChoice.supportedReaction($0.reactionKey)
            } ?? [],
            sourceView: sourceView
        )
    }

    // MARK: - Actions

    private func loadMoreMessages() {
        guard hasScrolledToBottom else {
            print("Rejecting loadMoreMessages because hasScrolledToBottom is false")
            return
        }

        viewModel.loadMoreIfNeeded()
    }

    @objc private func infoTapped() {
        let viewController = ParticipantsViewController(thread: viewModel.thread)
        present(viewController, animated: true)
    }

    @objc private func reactionGestureTriggered(_ gesture: UIGestureRecognizer) {
        guard
            let indexPath = messagesCollectionView.indexPathForItem(at: gesture.location(in: messagesCollectionView)),
            let cell = messagesCollectionView.cellForItem(at: indexPath),
            let message = messages[indexPath.section].payload?.message
        else {
            return
        }

        /// Message cells fill the entire width, pick their visible subview.
        let sourceView: UIView
        switch messages[indexPath.section].kind {
        case .custom(let model) where model is Tweet:
            guard let cell = cell as? TweetCell else {
                fatalError("Configuration error.")
            }
            sourceView = cell.contentView

        case .custom:
            return

        default:
            sourceView = cell.contentView.subviews.first(where: { $0 is MessageContainerView }) ?? cell
        }

        let viewController = reactionViewController(for: message, sourceView: sourceView)
        present(viewController, animated: true)
        UIDevice.current.generateHaptic(.selectionChanged)
    }

    // MARK: - Menus

    private func userListMenu(with users: [Schema.User]) -> UIMenu {
        UIMenu(
            title: "Seen by",
            options: [.displayInline],
            children: users.map { user in
                UIAction(
                    title: user.displayName,
                    image: UIImage(systemName: "person.circle")
                ) { _ in
                    print("TODO: Navigate to user profile")
                }
            }
        )
    }

    private func reactionListMenu() -> UIMenu {
        UIMenu(
            title: "Reactions",
            options: [.displayInline],
            children: []
        )
    }

    private func sendErrorMenu(at indexPath: IndexPath) -> UIMenu {
        guard let messageId = messages[indexPath.section].payload?.message.id else {
            fatalError("Attempting to show resend menu for non-message item")
        }

        return UIMenu(
            title: "Message not delivered",
            options: [.displayInline],
            children: [
                UIAction(
                    title: "Retry",
                    image: UIImage(systemName: "arrow.counterclockwise")
                ) { [unowned self] _ in
                    Task { @MainActor in
                        do {
                            try await self.viewModel.thread.resendPendingMessage(messageId)

                            if self.viewModel.thread.isUnread {
                                try await self.viewModel.thread.markAsRead()
                            }
                        } catch {
                            AppState.shared.handleError(error)
                        }
                    }
                },
                UIAction(
                    title: "Delete",
                    image: UIImage(systemName: "trash"),
                    attributes: .destructive
                ) { [unowned self] _ in
                    Task { @MainActor in
                        self.viewModel.thread.removePendingMessage(messageId)
                    }
                }
            ]
        )
    }

    // MARK: - Debug UI

    @objc private func debugGestureTriggered(_ gesture: UITapGestureRecognizer) {
        guard
            let indexPath = messagesCollectionView.indexPathForItem(at: gesture.location(in: messagesCollectionView)),
            let payload = messages[indexPath.section].payload
        else {
            return
        }

        let alert = UIAlertController(
            title: "Debug Menu",
            message: payload.message.displayString(in: viewModel.thread),
            preferredStyle: .actionSheet
        )
        alert.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .cancel
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "This message JSON",
                style: .default,
                handler: { [unowned self] _ in
                    guard let json = payload.message.toJSONString(prettyPrint: true) else {
                        return
                    }
                    self.present(
                        DebugTextViewController(text: json),
                        animated: true
                    )
                }
            )
        )

        alert.addAction(
            UIAlertAction(
                title: "All messages JSON",
                style: .default,
                handler: { [unowned self] _ in
                    guard let json = self.viewModel.thread.messages.toJSONString(prettyPrint: true) else {
                        return
                    }
                    self.present(
                        DebugTextViewController(text: json),
                        animated: true
                    )
                }
            )
        )

        alert.addAction(
            UIAlertAction(
                title: "Participants JSON",
                style: .default,
                handler: { [unowned self] _ in
                    guard let json = self.viewModel.thread.participants.toJSONString(prettyPrint: true) else {
                        return
                    }
                    self.present(
                        DebugTextViewController(text: json),
                        animated: true
                    )
                }
            )
        )

        present(alert, animated: true)
    }

    // MARK: - Convenience

    private func isPreviousMessageSameSender(at indexPath: IndexPath) -> Bool {
        indexPath.section - 1 >= 0
            && sender(at: indexPath.section).senderId == sender(at: indexPath.section - 1).senderId
    }

    private func isNextMessageSameSender(at indexPath: IndexPath) -> Bool {
        indexPath.section + 1 < numberOfSections(in: messagesCollectionView)
            && sender(at: indexPath.section).senderId == sender(at: indexPath.section + 1).senderId
    }
}

extension ThreadViewController: MessagesDataSource {

    // MARK: - Message

    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        messages.count
    }

    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        messages[indexPath.section]
    }

    // MARK: - Sender

    func sender(at index: Int) -> SenderType {
        messages[index].sender
    }

    func currentSender() -> SenderType {
        me
    }

    // MARK: - Swipe Timestamp

    private static var timestampFormatter: DateFormatter = {
        $0.dateStyle = .none
        $0.timeStyle = .short
        return $0
    }(DateFormatter())

    func messageTimestampLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let timestamp = NSMutableAttributedString()

        if let msg = viewModel.thread.messages.items[id: Schema.MessageID(rawValue: message.messageId)], msg.isSender == true {
            if !viewModel.thread.seenByOtherUsers(for: msg).isEmpty {
                let doubleTick = NSMutableAttributedString(string: "✓✓ ")
                doubleTick.addAttribute(.kern, value: -8, range: NSRange(location: 0, length: 2))
                doubleTick.addAttribute(.kern, value: 8, range: NSRange(location: 2, length: 1))

                timestamp.append(doubleTick)
            } else if msg.isDelivered == true || msg.isErrored == false || msg.isErrored == nil {
                timestamp.append(NSAttributedString(string: "✓ "))
            }
        }

        timestamp.append(NSAttributedString(
            string: Self.timestampFormatter.string(from: message.sentDate),
            attributes: [.foregroundColor: UIColor.tertiaryLabel]
        ))

        timestamp.addAttribute(.font, value: UIFont.systemFont(ofSize: 12), range: NSRange(location: 0, length: timestamp.length))

        return timestamp.copy() as? NSAttributedString
    }
}

extension ThreadViewController: MessageCellDelegate {

    func didTapMessage(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else {
            return
        }

        _ = messageForItem(at: indexPath, in: messagesCollectionView)
    }

    func didSelectURL(_ url: URL) {
        UIApplication.shared.open(url)
    }

    func didTapImage(in cell: MessageCollectionViewCell) {
        didTapMediaCell(cell)
    }

    func didTapQuoteCell(_ cell: QuoteCell) {
        guard
            let indexPath = messagesCollectionView.indexPath(for: cell),
            case .custom(let model) = messages[indexPath.section].kind,
            let quote = model as? Quote,
            let linkedSection = messages.firstIndex(where: {
                $0.payload?.message.id.rawValue == quote.linkedId
            })
        else {
            return
        }

        messagesCollectionView.scrollToItem(
            at: IndexPath(item: 0, section: linkedSection),
            at: .centeredVertically,
            animated: true
        )
    }

    func didTapMediaCell(_ cell: UICollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell), let selectedMedia = media(for: messages[indexPath.section]) else {
            return
        }

        let viewController = MediasViewController(
            currentMedia: .init(
                message: messages[indexPath.section].payload!.message,
                attachment: selectedMedia
            )!,
            medias: messages.compactMap { message in
                guard let payload = message.payload, let media = media(for: message) else {
                    return nil
                }
                return .init(message: payload.message, attachment: media)
            },
            reactionController: { [unowned self] message, sourceView in
                self.reactionViewController(for: message, sourceView: sourceView)
            },
            replyController: { [unowned self] message in
                self.dismiss(animated: true) {
                    self.quoteMessage(message)
                }
            }
        )

        present(viewController, animated: true)
    }

    private func media(for message: Message) -> Any? {
        switch message.kind {
        case .custom(let model) where model is Image:
            return model
        case .custom(let model) where model is Video:
            return model
        case .custom(let model) where model is Audio:
            return model
        default:
            return nil
        }
    }
}

extension ThreadViewController: MessagesDisplayDelegate, MessagesLayoutDelegate {

    // MARK: - Avatar

    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        let message = messages[indexPath.section]

        avatarView.backgroundColor = .systemFill

        if isFromCurrentSender(message: message), let payload = message.payload?.message, viewModel.thread.pendingMessages.contains(payload) {
            avatarView.backgroundColor = UIColor(.accentColor)
            avatarView.image = nil
            avatarView.isHidden = false
        } else if case .text = message.kind, let payload = message.payload {
            avatarView.backgroundColor = .systemFill
            avatarView.isHidden = !payload.showUserAvatar

            if
                let url = viewModel.thread.getParticipant(id: payload.userID)?.imgURL,
                url.starts(with: "data:image/svg+xml,"),
                let svg = url.replacingOccurrences(of: "data:image/svg+xml,", with: "").removingPercentEncoding
            {
                let svgImage = try? SVGParser.parse(text: svg).toNativeImage(size: .init(100, 100))
                avatarView.image = svgImage ?? Self.placeholderAvatar
                avatarView.backgroundColor = .clear
            } else {
                avatarView.sd_setImage(
                    with: { urlString in
                        guard let urlString = urlString, let url = URL(string: urlString) else {
                            return nil
                        }
                        return url
                    }(viewModel.thread.getParticipant(id: payload.userID)?.imgURL),
                    placeholderImage: Self.placeholderAvatar
                )
            }
        } else {
            avatarView.isHidden = true
        }
    }

    private static var placeholderAvatar: UIImage = {
        UIGraphicsImageRenderer(size: Avatar.Size.messageAuthor.rawValue).image { context in
            let frame = CGRect(origin: .zero, size: Avatar.Size.messageAuthor.rawValue)

            let gradient = CAGradientLayer()
            gradient.frame = frame
            gradient.colors =  [
                UIColor(hex: 0xC1C5CF).cgColor,
                UIColor(hex: 0x848994).cgColor
            ]
            gradient.render(in: context.cgContext)
            let placeholder = UIImage(
                systemName: "person.fill",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: frame.height - 12)
            )!
            .withAlignmentRectInsets(.init(top: 16, left: 0, bottom: 0, right: 0))
            .withRenderingMode(.alwaysTemplate)
            .withTintColor(.white)

            placeholder.draw(in: frame)
        }
    }()

    // MARK: - Colors

    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        isFromCurrentSender(message: message)
            ? UIColor(.accentColor)
            : UIColor.systemGray6
    }

    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        isFromCurrentSender(message: message)
            ? UIColor.white
            : UIColor.label
    }

    func audioTintColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        isFromCurrentSender(message: message)
            ? UIColor.white
            : UIColor(.accentColor)
    }

    // MARK: - Content Detectors

    func detectorAttributes(for detector: DetectorType, and message: MessageType, at indexPath: IndexPath) -> [NSAttributedString.Key: Any] {
        var normal = MessageLabel.defaultAttributes
        normal[.foregroundColor] = isFromCurrentSender(message: message) ? UIColor.white : UIColor.label
        normal[.underlineColor] = normal[.foregroundColor]
        return normal
    }

    func enabledDetectors(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> [DetectorType] {
        guard case .text = message.kind else {
            return []
        }
        return [.url]
    }

    // MARK: - Corners

    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        // UIKit makes it hard to have different corner radii so we use layer.cornerRadius for the small corners and a shape mask for the large corners
        .custom { [weak self] container in
            guard let welf = self else { return }
            if case .emoji = message.kind {
                container.backgroundColor = .clear
            }
            if case .photo(let model) = message.kind, let image = model as? Image, image.isSticker {
                container.backgroundColor = .clear
            }

            let largeRadius = Globals.Views.messageCornerRadius
            let smallRadius = Globals.Views.messageSmallCornerRadius

            container.layer.cornerCurve = .continuous
            container.layer.cornerRadius = smallRadius

            let path = UIBezierPath(
                roundedRect: container.bounds,
                byRoundingCorners: welf.isFromCurrentSender(message: message)
                    ? UIRectCorner.allCorners.subtracting(.bottomRight)
                    : UIRectCorner.allCorners.subtracting(.bottomLeft),
                cornerRadii: CGSize(width: largeRadius, height: largeRadius)
            )
            let mask = CAShapeLayer()
            mask.path = path.cgPath

            container.layer.mask = mask
        }
    }
}
