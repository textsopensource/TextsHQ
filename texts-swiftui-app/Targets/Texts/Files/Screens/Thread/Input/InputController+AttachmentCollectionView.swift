//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import UniformTypeIdentifiers
import AVFoundation
import WebKit
import MessageKit

extension ThreadViewController.InputController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    // MARK: - Data Source

    static let attachmentSize = CGFloat(160)

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        messageDraft.attachments.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let attachment = messageDraft.attachments[indexPath.item]
        let fileType = attachment.fileURL.flatMap { UTType(filenameExtension: $0.pathExtension) }
        let previewCell: AttachmentPreviewCell

        switch fileType {
        case .some(let fileType) where attachment.isGif == true:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GifPreviewCell", for: indexPath) as? GifPreviewCell else {
                fatalError("Configuration error")
            }
            guard let mime = fileType.preferredMIMEType, let url = attachment.fileURL, let data = try? Data(contentsOf: url) else {
                fallthrough
            }

            cell.webView.loadLocalAttachment(type: mime, base64: data.base64EncodedString())

            previewCell = cell

        case .some(let fileType) where fileType.conforms(to: .image):
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImagePreviewCell", for: indexPath) as? ImagePreviewCell else {
                fatalError("Configuration error")
            }
            guard let url = attachment.fileURL else {
                fallthrough
            }

            let cellSize = self.collectionView(collectionView, layout: attachmentLayout, sizeForItemAt: indexPath)
            let pixelSize = UIScreen.main.scale * max(cellSize.width, cellSize.height)

            let sourceOptions = [
                kCGImageSourceShouldCache: false
            ] as CFDictionary

            let thumbnailOptions = [
                kCGImageSourceThumbnailMaxPixelSize: pixelSize,
                kCGImageSourceCreateThumbnailFromImageAlways: true,
                kCGImageSourceShouldCacheImmediately: true,
                kCGImageSourceCreateThumbnailWithTransform: true,
            ] as CFDictionary

            if
                let source = CGImageSourceCreateWithURL(url as CFURL, sourceOptions),
                let cgImage = CGImageSourceCreateThumbnailAtIndex(source, 0, thumbnailOptions)
            {
                cell.imageView.image = UIImage(cgImage: cgImage)
            } else {
                cell.imageView.image = UIImage(contentsOfFile: url.path)
            }

            previewCell = cell

        case .some(let fileType) where fileType.conforms(to: .movie):
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviePreviewCell", for: indexPath) as? MoviePreviewCell else {
                fatalError("Configuration error")
            }
            guard let url = attachment.fileURL else {
                fallthrough
            }

            let asset = AVURLAsset(url: url)
            let generator = AVAssetImageGenerator(asset: asset)
            let cellSize = self.collectionView(collectionView, layout: attachmentLayout, sizeForItemAt: indexPath)
            generator.maximumSize = cellSize.applying(.init(scaleX: UIScreen.main.scale, y: UIScreen.main.scale))
            generator.appliesPreferredTrackTransform = true

            guard let imageReference = try? generator.copyCGImage(at: .zero, actualTime: nil) else {
                fallthrough
            }
            cell.imageView.image = UIImage(cgImage: imageReference)

            previewCell = cell

        default:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilePreviewCell", for: indexPath) as? FilePreviewCell else {
                fatalError("Configuration error")
            }

            cell.imageView.image = attachment.fileURL.flatMap {
                UIDocumentInteractionController(url: $0).icons.last
            }
            cell.label.text = attachment.fileName

            previewCell = cell
        }

        previewCell.onSelection = { [unowned self] in
            presentAttachment(at: indexPath.item)
        }
        previewCell.button.menu = attachmentMenu(for: indexPath.item)
        previewCell.visibleButton.menu = attachmentMenu(for: indexPath.item)

        return previewCell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let defaultSize = CGSize(width: Self.attachmentSize, height: Self.attachmentSize)
        guard let fileUrl = messageDraft.attachments[indexPath.item].fileURL else {
            return defaultSize
        }
        if let size = messageDraft.attachments[indexPath.item].size {
            return CGSize(
                width: Self.attachmentSize * (size.width / size.height),
                height: Self.attachmentSize
            )
        }
        switch UTType(filenameExtension: fileUrl.pathExtension) {
        case .some(let fileType) where fileType.conforms(to: .image):
            guard
                let source = CGImageSourceCreateWithURL(fileUrl as CFURL, nil),
                let properties = CGImageSourceCopyPropertiesAtIndex(source, 0, nil) as Dictionary?,
                let width = properties[kCGImagePropertyPixelWidth] as? CGFloat,
                let heigth = properties[kCGImagePropertyPixelHeight] as? CGFloat,
                let orientation = properties[kCGImagePropertyOrientation] as? Int
            else {
                fallthrough
            }
            let sourceSize = CGSize(
                width: 5...8 ~= orientation ? heigth : width,
                height: 5...8 ~= orientation ? width : heigth
            )
            return CGSize(
                width: Self.attachmentSize * (sourceSize.width / sourceSize.height),
                height: Self.attachmentSize
            )

        case .some(let fileType) where fileType.conforms(to: .movie):
            guard let track = AVURLAsset(url: fileUrl).tracks(withMediaType: .video).first else {
                fallthrough
            }
            let sourceSize = track.naturalSize.applying(track.preferredTransform)
            return CGSize(
                width: Self.attachmentSize * (sourceSize.width.absoluteValue / sourceSize.height.absoluteValue),
                height: Self.attachmentSize
            )

        default:
            return defaultSize
        }
    }

    // MARK: - Cells

    class AttachmentPreviewCell: UICollectionViewCell {

        lazy var button = UIButton(frame: bounds)
        lazy var visibleButton = UIButton()

        var onSelection: () -> Void = { }

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.layer.masksToBounds = true
            contentView.layer.cornerRadius = Globals.Views.smallCornerRadius
            contentView.backgroundColor = .systemFill

            button.addTarget(self, action: #selector(tapped), for: .touchUpInside)

            visibleButton.showsMenuAsPrimaryAction = true
            visibleButton.setImage(UIImage(systemName: "ellipsis"), for: .normal)
            visibleButton.tintColor = .white
            visibleButton.backgroundColor = .systemGray.withAlphaComponent(0.7)
            visibleButton.layer.cornerRadius = 16
            visibleButton.layer.masksToBounds = true

            [button, visibleButton].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                button.topAnchor.constraint(equalTo: contentView.topAnchor),
                button.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                button.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                button.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                visibleButton.widthAnchor.constraint(equalToConstant: 32),
                visibleButton.heightAnchor.constraint(equalToConstant: 32),
                visibleButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                visibleButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        @objc private func tapped() {
            onSelection()
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            contentView.bringSubviewToFront(visibleButton)
        }
    }

    class ImagePreviewCell: AttachmentPreviewCell {
        lazy var imageView = UIImageView()

        override init(frame: CGRect) {
            super.init(frame: frame)

            imageView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(imageView)
            NSLayoutConstraint.activate([
                imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
                imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }

    class MoviePreviewCell: ImagePreviewCell {
        override init(frame: CGRect) {
            super.init(frame: frame)

            let playButton = MessageKit.PlayButtonView()
            playButton.isUserInteractionEnabled = false

            playButton.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(playButton)
            NSLayoutConstraint.activate([
                playButton.widthAnchor.constraint(equalToConstant: 35),
                playButton.heightAnchor.constraint(equalToConstant: 35),
                playButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                playButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }

    class GifPreviewCell: AttachmentPreviewCell {

        lazy var webView = WKWebView(frame: bounds)

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            webView.backgroundColor = .clear
            webView.isUserInteractionEnabled = false

            webView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(webView)
            NSLayoutConstraint.activate([
                webView.topAnchor.constraint(equalTo: contentView.topAnchor),
                webView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                webView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }

    class FilePreviewCell: AttachmentPreviewCell {

        lazy var imageView = UIImageView()
        lazy var label = UILabel()

        override init(frame: CGRect) {
            super.init(frame: frame)

            imageView.contentMode = .scaleAspectFit
            imageView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(imageView)

            label.textColor = .secondaryLabel
            label.font = .preferredFont(forTextStyle: .footnote)
            label.textAlignment = .center
            label.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(label)

            let guide = UILayoutGuide()
            contentView.addLayoutGuide(guide)

            NSLayoutConstraint.activate([
                imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 32),
                imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 1/3),

                guide.topAnchor.constraint(equalTo: imageView.bottomAnchor),
                guide.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),

                label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                label.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -16),
                label.centerYAnchor.constraint(equalTo: guide.centerYAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
