//
// Copyright (c) Texts HQ
//

import UIKit
import UniformTypeIdentifiers
import TextsCore
import PhotosUI
import GiphyUISDK

extension ThreadViewController.InputController: PHPickerViewControllerDelegate, UIImagePickerControllerDelegate, UIDocumentPickerDelegate, GiphyDelegate, UINavigationControllerDelegate {

    // MARK: - Menus

    func addAttachmentMenu() -> UIMenu {
        UIMenu(
            title: "Add Attachment",
            children: [
                // liveTextAction(),
                selectPhotoAction(),
                useCameraAction(),
                searchGifAction(),
                pickFileAction()
            ].compactMap { $0 }
        )
    }

    func attachmentMenu(for index: Int) -> UIMenu {
        UIMenu(
            children: [
                UIAction(
                    title: "View",
                    image: UIImage(systemName: "arrow.up.left.and.arrow.down.right"),
                    attributes: ThreadViewController.MediasViewController.Media(
                        attachment: messageDraft.attachments[index]
                    ) == nil ? .disabled : []
                ) { [unowned self] _ in
                    presentAttachment(at: index)
                },
                UIAction(
                    title: "Move",
                    image: UIImage(systemName: "arrow.backward.circle"),
                    attributes: index == 0 ? .disabled : []
                ) { [unowned self] _ in
                    messageDraft.attachments.swapAt(index, index - 1)
                    reloadAttachments()
                },
                UIAction(
                    title: "Remove",
                    image: UIImage(systemName: "trash"),
                    attributes: .destructive
                ) { [unowned self] _ in
                    messageDraft.attachments.remove(at: index)
                    reloadAttachments()
                },
            ]
        )
    }

    // MARK: - Helpers

    private func allowedFileTypes() -> [UTType] {
        guard let attachmentInfo = attachmentInfo else {
            return [.item]
        }

        return [
            (UTType.image, attachmentInfo.noSupportForImage),
            (UTType.audio, attachmentInfo.noSupportForAudio),
            (UTType.video, attachmentInfo.noSupportForVideo),
            (UTType.fileURL, attachmentInfo.noSupportForFiles)
        ].compactMap { fileType, notSupported in
            if let notSupported = notSupported, notSupported {
                return nil
            } else {
                return fileType
            }
        }
    }

    private var areImagesOrVideosAllowed: Bool {
        allowedFileTypes().contains(.item)
            || allowedFileTypes().contains(.image)
            || allowedFileTypes().contains(.video)
    }

    var attachmentInfo: Schema.AttachmentInfo? {
        guard
            let thread = thread,
            let platform = try? thread.account?.accountInstance().platform,
            let attachmentInfo = platform.attachments
        else {
            return nil
        }

        return attachmentInfo
    }

    // MARK: - Actions

    func selectPhotoAction() -> UIAction {
        UIAction(
            title: "Photos",
            image: UIImage(systemName: "photo"),
            attributes: areImagesOrVideosAllowed ? [] : .disabled
        ) { [unowned self] _ in
            var configuration = PHPickerConfiguration()
            configuration.selectionLimit = 0
            let picker = PHPickerViewController(configuration: configuration)
            picker.delegate = self
            modalPresentationController?.present(picker, animated: true)
        }
    }

    func useCameraAction() -> UIAction {
        UIAction(
            title: "Camera",
            image: UIImage(systemName: "camera"),
            attributes: areImagesOrVideosAllowed ? [] : .disabled
        ) { [unowned self] _ in
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
            modalPresentationController?.present(picker, animated: true)
        }
    }

    func searchGifAction() -> UIAction {
        UIAction(
            title: "GIF",
            image: UIImage(systemName: "play.square"),
            attributes: attachmentInfo?.gifMimeType == nil ? .disabled : []
        ) { [unowned self] _ in
            Giphy.configure(apiKey: "8003D0ZPWSqYTlupHXpcCocdIraLw0xg")
            let picker = GiphyViewController()
            picker.mediaTypeConfig = [.gifs, .recents]
            picker.shouldLocalizeSearch = true
            picker.delegate = self
            modalPresentationController?.present(picker, animated: true)
        }
    }

    func pickFileAction() -> UIAction {
        UIAction(
            title: "Files",
            image: UIImage(systemName: "paperclip")
        ) { [unowned self] _ in
            let picker = UIDocumentPickerViewController(forOpeningContentTypes: allowedFileTypes(), asCopy: true)
            picker.delegate = self
            modalPresentationController?.present(picker, animated: true)
        }
    }

    func liveTextAction() -> UIAction? {
        if #available(iOS 15, *) {
            return UIAction.captureTextFromCamera(responder: textView, identifier: .paste)
        } else {
            return nil
        }
    }

    // MARK: - Delegates

    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
        picker.dismiss(animated: true)

        results.forEach { item in
            item.itemProvider.loadFileRepresentation(
                forTypeIdentifier: [UTType.image.identifier, UTType.movie.identifier]
                    .first(
                        where: item.itemProvider.hasItemConformingToTypeIdentifier
                    )!
            ) { [weak self] fileUrl, error in
                guard let self = self, let fileUrl = fileUrl else {
                    print("Photos picker error", error as Any)
                    return
                }
                self.addFile(at: fileUrl)
            }
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        attachImage(info[.originalImage] as? UIImage)
    }

    func attachImage(_ image: UIImage?) {
        guard
            let image = image,
            let imageData = image.jpegData(compressionQuality: 0.7)
        else {
            print("Breaking from Camera picker because .originalImage is nil")
            return
        }

        let temporaryUrl = URL(
            fileURLWithPath: NSTemporaryDirectory(),
            isDirectory: true
        ).appendingPathComponent(
            ProcessInfo().globallyUniqueString + ".jpeg"
        )

        do {
            try imageData.write(to: temporaryUrl, options: .atomic)
            addFile(at: temporaryUrl)
        } catch {
            print(error)
            return
        }
    }

    func didSelectMedia(giphyViewController: GiphyViewController, media: GPHMedia) {
        giphyViewController.dismiss(animated: true)

        let fileType: GPHFileExtension
        switch attachmentInfo?.gifMimeType {
        case "image/gif": fileType = .gif
        case "video/mp4": fileType = .mp4
        default:          fileType = .mp4
        }
        guard
            let urlString = media.url(rendition: .original, fileType: fileType)
        else {
            return
        }

        DispatchQueue.global(qos: .userInitiated).async {
            GPHCache.shared.downloadAssetData(urlString) { [weak self] gifData, error in
                guard let self = self, let gifData = gifData else {
                    return
                }

                let targetLocation = FileLocation(
                    base: .temporaryDirectory,
                    relativePath: ProcessInfo().globallyUniqueString + (fileType == .gif ? ".gif" : ".mp4")
                )

                do {
                    try gifData.write(to: targetLocation.url)
                } catch {
                    return
                }

                var size: Schema.Size?
                if
                    let source = CGImageSourceCreateWithURL(targetLocation.url as CFURL, nil),
                    let properties = CGImageSourceCopyPropertiesAtIndex(
                        source, 0,
                        ([kCGImageSourceShouldCache: false] as CFDictionary)
                    ) as Dictionary?,
                    let width = properties[kCGImagePropertyPixelWidth] as? Int,
                    let heigth = properties[kCGImagePropertyPixelHeight] as? Int
                {
                    size = Schema.Size(width: Double(width), height: Double(heigth))
                }

                DispatchQueue.main.async {
                    self.messageDraft.attachments.append(.gif(targetLocation, size))
                    self.reloadAttachments()
                }
            }
        }
    }

    func didDismiss(controller: GiphyViewController?) { }

    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        urls.forEach(addFile)
    }
}
