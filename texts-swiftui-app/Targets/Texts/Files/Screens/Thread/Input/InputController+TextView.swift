//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension ThreadViewController.InputController {
    class TextView: UITextView, UITextViewDelegate {

        lazy var placeholderLabel = UILabel()

        let inputFont = UIFont.preferredFont(forTextStyle: .callout)

        var onHeightChange: (CGFloat) -> Void = { _ in }
        var onTextChange: (String) -> Void = { _ in }
        var onImagePaste: ([UIImage]) -> Void = { _ in }

        // MARK: - Setup

        override init(frame: CGRect, textContainer: NSTextContainer?) {
            super.init(frame: frame, textContainer: textContainer)

            delegate = self
            isScrollEnabled = false
            font = inputFont
            textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
            backgroundColor = .clear

            placeholderLabel.numberOfLines = 1
            placeholderLabel.textColor = .placeholderText
            placeholderLabel.font = .preferredFont(forTextStyle: .body).withSize(15)
            placeholderLabel.lineBreakMode = .byTruncatingTail
            placeholderLabel.allowsDefaultTighteningForTruncation = true

            placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
            addSubview(placeholderLabel)
            NSLayoutConstraint.activate([
                placeholderLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                placeholderLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 12),
                placeholderLabel.widthAnchor.constraint(equalTo: widthAnchor, constant: -24)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Background

        class Container: UIView {
            lazy var textView = TextView()
            lazy var background = BackgroundView()

            override init(frame: CGRect) {
                super.init(frame: frame)

                [background, textView].forEach {
                    $0.translatesAutoresizingMaskIntoConstraints = false
                    addSubview($0)
                }
                NSLayoutConstraint.activate([
                    background.topAnchor.constraint(equalTo: topAnchor),
                    background.leftAnchor.constraint(equalTo: leftAnchor),
                    background.rightAnchor.constraint(equalTo: rightAnchor),
                    background.bottomAnchor.constraint(equalTo: bottomAnchor),

                    textView.topAnchor.constraint(equalTo: topAnchor),
                    textView.leftAnchor.constraint(equalTo: leftAnchor),
                    textView.rightAnchor.constraint(equalTo: rightAnchor),
                    textView.bottomAnchor.constraint(equalTo: bottomAnchor),
                ])
            }

            required init?(coder: NSCoder) {
                nil
            }
        }

        class BackgroundView: UIView {
            override func layoutSubviews() {
                super.layoutSubviews()

                backgroundColor = .systemBackground

                let largeRadius = Globals.Views.messageCornerRadius
                let smallRadius = Globals.Views.messageSmallCornerRadius

                layer.cornerCurve = .continuous
                layer.cornerRadius = smallRadius

                let path = UIBezierPath(
                    roundedRect: bounds,
                    byRoundingCorners: UIRectCorner.allCorners.subtracting(.bottomRight),
                    cornerRadii: CGSize(width: largeRadius, height: largeRadius)
                )
                let mask = CAShapeLayer()
                mask.path = path.cgPath
                layer.mask = mask
            }
        }

        // MARK: - Frame Calculations

        func textViewDidChange(_ textView: UITextView) {
            placeholderLabel.isHidden = !textView.text.isEmpty
            recalculateHeight()
            onTextChange(textView.text)
        }

        lazy var oldFrame = CGRect.zero

        override func layoutSubviews() {
            super.layoutSubviews()

            guard frame != oldFrame else {
                return
            }
            oldFrame = frame
            recalculateHeight()
        }

        override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
            bounds.insetBy(dx: 0, dy: -32).contains(point)
        }

        func maxHeight() -> CGFloat {
            (UIScreen.main.bounds.height / 4).rounded(.down)
        }

        func minHeight() -> CGFloat {
            inputFont.lineHeight.rounded(.up) + 16
        }

        lazy var previousHeight = minHeight()

        func recalculateHeight() {
            let height = text.boundingRect(
                with: CGSize(width: textContainer.size.width - 16, height: .greatestFiniteMagnitude),
                options: [.usesLineFragmentOrigin, .usesFontLeading],
                attributes: [.font: inputFont], context: nil
            ).height.rounded(.up) + 16

            let clampedHeight = max(min(height, maxHeight()), minHeight())
            guard clampedHeight != previousHeight else {
                return
            }

            isScrollEnabled = height > maxHeight()
            previousHeight = clampedHeight
            onHeightChange(clampedHeight)
        }

        // MARK: - Pasting

        override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            if action == NSSelectorFromString("paste:"), UIPasteboard.general.hasImages {
                return true
            }
            return super.canPerformAction(action, withSender: sender)
        }

        override func paste(_ sender: Any?) {
            if let images = UIPasteboard.general.images {
                onImagePaste(images)
            }
            return super.paste(sender)
        }

    }
}
