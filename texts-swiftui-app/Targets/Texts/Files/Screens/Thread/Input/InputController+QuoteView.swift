//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension ThreadViewController.InputController {
    struct QuoteContainer {
        let type: QuoteType
        let message: Schema.Message
        let displayName: String?
        let displayText: String

        enum QuoteType {
            /// For platforms that support quotes. Embeds the message in a white bubble.
            case reply

            /// A replacement for platforms that don't support replies. Prepends the message to the current input.
            case quote

            init(platformQuoteSupport: Bool) {
                self = platformQuoteSupport ? .reply : .quote
            }
        }
    }

    class QuoteView: UIView {
        lazy var captionLabel = UILabel()
        lazy var quoteLabel = UILabel()
        lazy var xButton = InflatedTapAreaButton()

        override init(frame: CGRect) {
            super.init(frame: frame)

            backgroundColor = .secondarySystemBackground

            layer.masksToBounds = true
            layer.cornerRadius = Globals.Views.messageSmallCornerRadius

            let captionFont = UIFont(
                descriptor: UIFont.preferredFont(
                    forTextStyle: .caption1
                )
                .fontDescriptor
                    .withSymbolicTraits(.traitBold)!,
                size: .zero
            )

            captionLabel.textColor = UIColor(.accentColor)
            captionLabel.font = captionFont
            captionLabel.lineBreakMode = .byTruncatingTail

            xButton.setImage(
                UIImage(
                    systemName: "xmark",
                    withConfiguration: UIImage.SymbolConfiguration(font: captionFont)
                ), for: .normal
            )
            xButton.showsMenuAsPrimaryAction = true

            quoteLabel.numberOfLines = 5
            quoteLabel.font = .preferredFont(forTextStyle: .body)
            quoteLabel.lineBreakMode = .byTruncatingTail
            quoteLabel.setContentCompressionResistancePriority(.required, for: .vertical)

            [captionLabel, xButton, quoteLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                addSubview($0)
            }

            NSLayoutConstraint.activate([
                captionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
                captionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
                captionLabel.rightAnchor.constraint(equalTo: xButton.leftAnchor, constant: -8),

                xButton.topAnchor.constraint(equalTo: topAnchor, constant: 8),
                xButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
                xButton.heightAnchor.constraint(equalToConstant: captionFont.lineHeight),

                quoteLabel.topAnchor.constraint(equalTo: captionLabel.bottomAnchor, constant: 8),
                quoteLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
                quoteLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
                quoteLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        class InflatedTapAreaButton: UIButton {
            override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
                bounds.insetBy(dx: -20, dy: -20).contains(point)
            }
        }
    }
}
