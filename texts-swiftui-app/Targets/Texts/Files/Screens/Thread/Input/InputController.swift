//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import UniformTypeIdentifiers
import MessageKit

extension ThreadViewController {
    // swiftlint:disable:next type_body_length
    class InputController: UIView {

        // MARK: - Data

        var thread: ThreadStore? {
            didSet {
                if let cached = AppCache.retrieve(MessageDraft.self, key: InputController.draftKey(for: thread)) {
                    messageDraft = cached
                }
                textView.placeholderLabel.text = textInputLabel()
                attachmentButton.menu = addAttachmentMenu()
                reloadAvatar()
                removeInvalidAttachments()
            }
        }

        var messageDraft = MessageDraft() {
            didSet {
                sendButton.isEnabled = isSendEnabled && !messageDraft.isEmpty
                textView.text = messageDraft.messageText
                textView.placeholderLabel.isHidden = !textView.text.isEmpty

                Task { @MainActor in
                    try await AppCache.cache(messageDraft, key: InputController.draftKey(for: thread))
                    onDraftChange(messageDraft)
                }
            }
        }

        var quote: QuoteContainer?

        var isSendEnabled = true {
            didSet {
                sendButton.isEnabled = isSendEnabled && !messageDraft.isEmpty
            }
        }

        weak var autoInsetAdjustingThreadViewController: ThreadViewController?
        weak var modalPresentationController: UIViewController?

        var onSend: (InputController) -> Void = { _ in }
        var onHeightChange: (HeightChange) -> Void = { _ in }
        var onDraftChange: (MessageDraft) -> Void = { _ in }

        lazy var textViewHeightConstraint = textViewContainer.heightAnchor.constraint(equalToConstant: textView.previousHeight)

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)
            setupView()
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Height

        struct HeightChange {
            let oldHeight: CGFloat
            let newHeight: CGFloat
        }

        private lazy var oldHeight = CGFloat.zero

        override func layoutSubviews() {
            super.layoutSubviews()

            let newHeight = bounds.height
            let oldHeight = oldHeight
            guard newHeight != oldHeight else {
                return
            }
            onHeightChange(.init(oldHeight: oldHeight, newHeight: newHeight))
            self.oldHeight = newHeight

            guard let threadViewController = autoInsetAdjustingThreadViewController else {
                return
            }

            threadViewController.additionalBottomInset = newHeight + 16
            let difference = newHeight - oldHeight
            if threadViewController.hasScrolledToBottom, difference > 0 {
                var newOffset = threadViewController.messagesCollectionView.contentOffset
                newOffset.y += difference + 16
                threadViewController.messagesCollectionView.setContentOffset(newOffset, animated: true)
            }
        }

        // MARK: - View

        // - Container
        lazy var backgroundView = UIVisualEffectView(effect: UIBlurEffect(style: .systemThickMaterial))
        lazy var contentView = UIView()
        lazy var containerStack = UIStackView()

        // - Attachments
        lazy var attachmentCollectionView = UICollectionView(
            frame: CGRect(origin: .zero, size: CGSize(width: frame.width, height: InputController.attachmentSize)),
            collectionViewLayout: attachmentLayout
        )
        lazy var attachmentLayout = UICollectionViewFlowLayout()

        // - Quote
        lazy var quoteView = QuoteView()

        // - Input
        lazy var inputStack = UIStackView()
        lazy var avatarView = ThreadAvatarView(size: .size(textView.minHeight()))
        lazy var attachmentButton = UIButton()
        lazy var textViewContainer = TextView.Container()
        open var textView: TextView { textViewContainer.textView }
        lazy var sendButton = SendButton()

        // swiftlint:disable:next function_body_length
        private func setupView() {
            backgroundView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(backgroundView)

            contentView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(contentView)

            containerStack.axis = .vertical
            containerStack.spacing = 8
            containerStack.distribution = .fill
            containerStack.alignment = .fill

            containerStack.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(containerStack)

            NSLayoutConstraint.activate([
                backgroundView.topAnchor.constraint(equalTo: topAnchor),
                backgroundView.leftAnchor.constraint(equalTo: leftAnchor),
                backgroundView.rightAnchor.constraint(equalTo: rightAnchor),
                backgroundView.bottomAnchor.constraint(equalTo: bottomAnchor),

                contentView.topAnchor.constraint(equalTo: topAnchor),
                contentView.leftAnchor.constraint(equalTo: leftAnchor),
                contentView.rightAnchor.constraint(equalTo: rightAnchor),

                containerStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                containerStack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
                containerStack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
                containerStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            ])

            attachmentLayout.scrollDirection = .horizontal
            attachmentCollectionView.backgroundColor = .clear
            attachmentCollectionView.register(ImagePreviewCell.self, forCellWithReuseIdentifier: "ImagePreviewCell")
            attachmentCollectionView.register(MoviePreviewCell.self, forCellWithReuseIdentifier: "MoviePreviewCell")
            attachmentCollectionView.register(FilePreviewCell.self, forCellWithReuseIdentifier: "FilePreviewCell")
            attachmentCollectionView.register(GifPreviewCell.self, forCellWithReuseIdentifier: "GifPreviewCell")
            attachmentCollectionView.delegate = self
            attachmentCollectionView.dataSource = self
            attachmentCollectionView.alwaysBounceHorizontal = true
            attachmentCollectionView.showsHorizontalScrollIndicator = false
            attachmentCollectionView.clipsToBounds = false

            attachmentCollectionView.isHidden = true
            containerStack.addArrangedSubview(attachmentCollectionView)
            NSLayoutConstraint.activate([
                attachmentCollectionView.heightAnchor.constraint(equalToConstant: Self.attachmentSize)
            ])

            quoteView.alpha = .leastNonzeroMagnitude
            quoteView.isHidden = true
            quoteView.xButton.menu = UIMenu(children: [
                UIAction(
                    title: "Remove",
                    image: UIImage(systemName: "xmark"),
                    attributes: .destructive
                ) { [unowned self] _ in
                    replyTo(nil)
                }
            ])
            containerStack.addArrangedSubview(quoteView)

            inputStack.axis = .horizontal
            inputStack.spacing = 8
            inputStack.alignment = .bottom
            inputStack.distribution = .fill

            inputStack.translatesAutoresizingMaskIntoConstraints = false
            containerStack.addArrangedSubview(inputStack)

            avatarView.isHidden = shouldAvatarBeHidden()

            attachmentButton.setImage(UIImage(
                systemName: "paperclip",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 20, weight: .semibold)
            ), for: .normal)
            attachmentButton.tintColor = .tertiaryLabel
            attachmentButton.showsMenuAsPrimaryAction = true
            attachmentButton.menu = addAttachmentMenu()

            textView.placeholderLabel.text = textInputLabel()
            textView.onTextChange = { [unowned self] text in
                messageDraft.messageText = text
                sendButton.isEnabled = isSendEnabled && !messageDraft.isEmpty

                if !text.isEmpty {
                    Task {
                        try? await thread?.onTyping()
                    }
                }
            }
            textView.onImagePaste = { [unowned self] images in
                images.forEach(attachImage)
            }

            sendButton.onSend = { [unowned self] in
                sendMessage()

                Task {
                    try? await thread?.sendStopTypingIndicator()
                }
            }

            [avatarView, attachmentButton, textViewContainer, sendButton]
                .forEach(inputStack.addArrangedSubview)

            let inputSize = textView.minHeight()
            textViewHeightConstraint.priority = .required
            NSLayoutConstraint.activate([
                attachmentButton.widthAnchor.constraint(equalToConstant: inputSize),
                attachmentButton.heightAnchor.constraint(equalTo: attachmentButton.widthAnchor),

                textViewHeightConstraint,

                sendButton.heightAnchor.constraint(equalToConstant: inputSize),
                sendButton.widthAnchor.constraint(equalToConstant: inputSize)
            ])

            textView.onHeightChange = { [unowned self] height in
                textViewHeightConstraint.constant = height
                layoutIfNeeded()
            }

            let neatPopGesture = UISwipeGestureRecognizer(target: self, action: #selector(pop))
            neatPopGesture.direction = .right
            addGestureRecognizer(neatPopGesture)
        }

        // MARK: - Hurty fingies

        @objc private func pop() {
            autoInsetAdjustingThreadViewController?.splitViewController?.show(.primary)
        }

        // MARK: - Quotes

        func reloadQuote() {
            guard let quote = quote, quote.type == .reply else {
                if !quoteView.isHidden {
                    UIView.animateKeyframes(withDuration: 0.15, delay: .zero) {
                        UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
                            self.quoteView.alpha = .leastNonzeroMagnitude
                        }
                        UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                            self.quoteView.isHidden.toggle()
                            self.containerStack.layoutIfNeeded()
                        }
                    }
                }
                return
            }

            if quoteView.isHidden {
                UIView.animateKeyframes(withDuration: 0.15, delay: .zero) {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.5) {
                        self.quoteView.isHidden.toggle()
                        self.containerStack.layoutIfNeeded()
                    }
                    UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                        self.quoteView.alpha = 1
                    }
                }
            }

            quoteView.captionLabel.text = "Replying to " + (quote.displayName ?? "")
            quoteView.quoteLabel.text = quote.displayText
            sendButton.isEnabled = isSendEnabled && !messageDraft.isEmpty
        }

        // MARK: - Attachment View

        func reloadAttachments() {
            if messageDraft.attachments.isEmpty != attachmentCollectionView.isHidden {
                if messageDraft.attachments.isEmpty {
                    self.attachmentCollectionView.reloadData()
                }
                UIView.animate(withDuration: 0.2) {
                    self.attachmentCollectionView.isHidden.toggle()
                    self.containerStack.layoutIfNeeded()
                } completion: { _ in
                    if !self.messageDraft.attachments.isEmpty {
                        self.attachmentCollectionView.reloadData()
                    }
                }
            } else {
                print("Reloading attachments", messageDraft.attachments)
                attachmentCollectionView.performBatchUpdates {
                    self.attachmentCollectionView.reloadSections(IndexSet(integer: 0))
                } completion: { _ in
                    self.attachmentCollectionView.scrollToItem(
                        at: IndexPath(item: self.messageDraft.attachments.count - 1, section: 0),
                        at: .right,
                        animated: true
                    )
                }
            }
            sendButton.isEnabled = isSendEnabled && !messageDraft.isEmpty
        }

        func presentAttachment(at index: Int) {
            let mediasViewController = MediasViewController(
                currentMedia: .init(
                    attachment: messageDraft.attachments[index]
                )!,
                medias: messageDraft.attachments.compactMap(
                    MediasViewController.Media.init
                )
            )
            mediasViewController.modalPresentationStyle = .automatic
            modalPresentationController?.present(mediasViewController, animated: true)
        }

        // MARK: - Avatar

        private func shouldAvatarBeHidden() -> Bool {
            if let thread = thread, thread.isFromPlatformWithOtherAccounts {
                return false
            } else {
                return true
            }
        }

        func reloadAvatar() {
            if let user = thread?.account?.instance?.user {
                avatarView.source = .single(user)
            } else {
                avatarView.source = nil
            }

            if avatarView.isHidden != shouldAvatarBeHidden() {
                UIView.animate(withDuration: 0.2) {
                    self.avatarView.isHidden.toggle()
                }
            }
        }
    }
}
