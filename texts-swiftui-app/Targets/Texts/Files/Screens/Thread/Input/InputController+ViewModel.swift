//
// Copyright (c) Texts HQ
//

import TextsCore
import UIKit
import UniformTypeIdentifiers

extension ThreadViewController.InputController {

    // MARK: - Cache

    static func draftKey(for thread: ThreadStore?) -> String {
        if let thread = thread {
            return "\(thread.uniqueID)-message-draft"
        } else {
            return "compose-draft"
        }
    }

    private var draftCacheKey: String {
        Self.draftKey(for: thread)
    }

    // MARK: - Send

    func textInputLabel() -> String {
        if let threadName = thread?.name {
            if let platformName = thread?.account?.platformDisplayName {
                return "Message \(threadName) on \(platformName)"
            } else {
                return "Message \(threadName)"
            }
        } else {
            return "Message"
        }
    }

    func sendMessage() {
        guard let thread = thread else {
            return
        }

        let finishedMessage = messageDraft

        quote = nil
        messageDraft = MessageDraft()

        textView.recalculateHeight()
        reloadQuote()
        reloadAttachments()
        sendButton.isEnabled = false

        Task { @MainActor in
            do {
                try await thread.sendMessage(messageDraft: finishedMessage)
                onSend(self)
            } catch {
                let alert = UIAlertController(
                    title: "Message Not Sent",
                    message: error.localizedDescription,
                    preferredStyle: .alert
                )
                alert.addAction(.init(title: "Dismiss", style: .cancel))
                modalPresentationController?.present(alert, animated: true)
                AppState.shared.handleError(error)
            }
        }

        if thread.isUnread {
            Task { @MainActor in
                do {
                    try await thread.markAsRead()
                } catch {
                    AppState.shared.handleError(error)
                }
            }
        }

        UIDevice.current.generateHaptic(.light)
    }

    // MARK: - Quotes

    func replyTo(_ quote: QuoteContainer?) {
        self.quote = quote
        defer {
            reloadQuote()
        }
        guard let quote = quote else {
            messageDraft.quotedMessage = nil
            return
        }

        switch quote.type {
        case .reply:
            messageDraft.quotedMessage = MessageDraft.QuotedMessage.sameThread(quote.message.id)

        case .quote:
            if messageDraft.messageText.first == ">" {
                messageDraft.messageText = messageDraft.messageText.substrings(separatedBy: "\n").dropFirst().joined()
            }
            messageDraft.messageText.insert(
                contentsOf: "> \(quote.displayText)\(textView.text.isEmpty ? "" : "\n")\n\n",
                at: messageDraft.messageText.startIndex
            )
            textView.recalculateHeight()
        }
    }

    // MARK: - Attachments

    func addFile(at fileUrl: URL) {
        guard Thread.isMainThread else {
            DispatchQueue.main.sync {
                self.addFile(at: fileUrl)
            }
            return
        }

        let targetLocation = FileLocation(
            base: .temporaryDirectory,
            relativePath: fileUrl.lastPathComponent
        )
        do {
            try FileManager.default.moveItem(at: fileUrl, to: targetLocation.url)
        } catch {
            switch error {
            case let error as NSError where error.code == 516:
                // file already exists, its ok
                break
            default:
                print("Breaking from error", error)
                return
            }
        }

        switch validateAttachment(.file(targetLocation)) {
        case .valid(let attachment):
            messageDraft.attachments.append(attachment)
            reloadAttachments()

        case .unsupportedType:
            let alert = UIAlertController(
                title: "Attachment Not Added",
                message: "\(thread?.account?.platformDisplayName ?? "") does not support this attachment type. \(fileUrl.pathExtension)",
                preferredStyle: .alert
            )
            alert.addAction(.init(title: "Dismiss", style: .cancel))
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.modalPresentationController?.present(alert, animated: true)
            }
        case .aboveMaxSize:
            let alert = UIAlertController(
                title: "Attachment Not Added",
                message: "The attachment is too big for \(thread?.account?.platformDisplayName ?? "").",
                preferredStyle: .alert
            )
            alert.addAction(.init(title: "Dismiss", style: .cancel))
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.modalPresentationController?.present(alert, animated: true)
            }
        }
    }

    /// Attachments are assumed to be valid if there's no thread or attachment info.
    /// Revalidate attachments when the attachment info changes
    enum AttachmentValidationResult {
        case valid(MessageDraft.LocalAttachment)
        case unsupportedType
        case aboveMaxSize
    }

    func validateAttachment(_ attachment: MessageDraft.LocalAttachment) -> AttachmentValidationResult {
        guard let attachmentInfo = attachmentInfo else {
            return .valid(attachment)
        }

        switch attachment {
        case .gif:
            guard attachmentInfo.gifMimeType != nil else {
                return .unsupportedType
            }
            // TODO: Check MIME type match as well
            return .valid(attachment)

        case .file(let location):
            guard let fileType = UTType(filenameExtension: location.url.pathExtension) else {
                return .unsupportedType
            }

            let maxSize: Int?
            switch fileType {
            case let fileType where fileType.conforms(to: .image) && attachmentInfo.noSupportForImage != true:
                maxSize = attachmentInfo.maxSize?.image
            case let fileType where fileType.conforms(to: .audio) && attachmentInfo.noSupportForAudio != true:
                maxSize = attachmentInfo.maxSize?.audio
            case let fileType where fileType.conforms(to: .movie) && attachmentInfo.noSupportForVideo != true:
                maxSize = attachmentInfo.maxSize?.video
            case let fileType where fileType.conforms(to: .fileURL) && attachmentInfo.noSupportForFiles != true:
                maxSize = attachmentInfo.maxSize?.files
            default:
                return .unsupportedType
            }

            let fileSize = (try? FileManager.default.attributesOfItem(atPath: location.url.absoluteString)[FileAttributeKey.size] as? NSNumber)?.intValue ?? .zero
            let safeMaxSize = maxSize ?? .max
            return fileSize < safeMaxSize ? .valid(attachment) : .aboveMaxSize
        }
    }

    func removeInvalidAttachments() {
        messageDraft.attachments = messageDraft.attachments.filter {
            guard case .valid = validateAttachment($0) else {
                return false
            }
            return true
        }
    }
}
