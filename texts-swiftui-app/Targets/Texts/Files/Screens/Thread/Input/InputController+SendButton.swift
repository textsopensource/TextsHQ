//
// Copyright (c) Texts HQ
//

import UIKit

extension ThreadViewController.InputController {
    class SendButton: UIButton {
        var onSend: () -> Void = { }

        override var isEnabled: Bool {
            didSet {
                backgroundColor = isEnabled ? UIColor(.accentColor) : .secondarySystemFill
            }
        }

        override init(frame: CGRect) {
            super.init(frame: frame)
            setImage(UIImage(
                systemName: "arrow.up",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .semibold)
            ), for: .normal)
            tintColor = .white
            backgroundColor = UIColor(.accentColor)
            clipsToBounds = true
            addTarget(self, action: #selector(tapped), for: .touchUpInside)
        }

        required init?(coder: NSCoder) {
            nil
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            layer.cornerRadius = frame.height / 2
        }

        @objc func tapped() {
            UISelectionFeedbackGenerator().selectionChanged()
            onSend()
        }
    }
}
