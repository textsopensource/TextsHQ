//
// Copyright (c) Texts HQ
//

import UIKit

extension UIView {
    private class ShimmerView: UIView {

        lazy var gradient = CAGradientLayer()

        override init(frame: CGRect) {
            super.init(frame: frame)

            gradient.frame = bounds
            gradient.name = "shimmer"
            gradient.colors = gradientColors()
            gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
            gradient.locations = [0.0, 0.5, 1.0]

            layer.addSublayer(gradient)

            let animation = CABasicAnimation(keyPath: "locations")
            animation.fromValue = [-1.0, -0.5, 0.0]
            animation.toValue = [1.0, 1.5, 2.0]
            animation.duration = 1.0
            animation.repeatCount = .infinity

            gradient.add(animation, forKey: "shimmer")
        }

        required init?(coder: NSCoder) {
            nil
        }

        override func layoutSubviews() {
            super.layoutSubviews()

            layer.sublayers!.first!.frame = bounds
        }

        override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
            super.traitCollectionDidChange(previousTraitCollection)

            if previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle {
                gradient.colors = gradientColors()
            }
        }

        func gradientColors() -> [CGColor] {
            traitCollection.userInterfaceStyle == .dark
            ? [
                UIColor(white: 0.15, alpha: 1).cgColor,
                UIColor(white: 0.05, alpha: 1).cgColor,
                UIColor(white: 0.15, alpha: 1).cgColor
            ]
            : [
                UIColor(white: 0.85, alpha: 1).cgColor,
                UIColor(white: 0.95, alpha: 1).cgColor,
                UIColor(white: 0.85, alpha: 1).cgColor
            ]
        }
    }

    func addLoadingShimmer() {
        let shimmer = ShimmerView(frame: bounds)
        shimmer.tag = 1
        shimmer.translatesAutoresizingMaskIntoConstraints = false
        addSubview(shimmer)
        NSLayoutConstraint.activate([
            shimmer.topAnchor.constraint(equalTo: topAnchor),
            shimmer.leftAnchor.constraint(equalTo: leftAnchor),
            shimmer.rightAnchor.constraint(equalTo: rightAnchor),
            shimmer.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func removeLoadingShimmer() {
        if let shimmer = subviews.first(where: { $0.tag == 1 }) {
            shimmer.removeFromSuperview()
        }
    }
}
