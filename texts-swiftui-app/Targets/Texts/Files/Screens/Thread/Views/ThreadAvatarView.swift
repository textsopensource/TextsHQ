//
// Copyright (c) Texts HQ
//

import UIKit
import CryptoKit
import SDWebImage
import TextsCore
import TextsUI

enum AvatarSize {
    case settingsAccount
    case messageAuthor
    case threadIcon
    case pinnedThreadIcon
    case tweetAuthor
    case profileViewIcon
    case profileViewParticipant
    case navigationBar
    case messageSeenIndicator
    case accountDetail
    case searchResult
    case threadInput
    case size(CGFloat)

    var rawValue: CGSize {
        switch self {
        case .size(let size):
            return CGSize(width: size, height: size)
        case .messageSeenIndicator:
            return CGSize(width: 18, height: 18)
        case .messageAuthor:
            return CGSize(width: 22, height: 22)
        case .settingsAccount:
            return CGSize(width: 28, height: 28)
        case .threadInput:
            return CGSize(width: 28, height: 28)
        case .navigationBar:
            return CGSize(width: 29, height: 29)
        case .tweetAuthor:
            return CGSize(width: 30, height: 30)
        case .searchResult:
            return CGSize(width: 36, height: 36)
        case .profileViewParticipant:
            return CGSize(width: 42, height: 42)
        case .threadIcon:
            return CGSize(width: 45, height: 45)
        case .pinnedThreadIcon:
            return CGSize(width: 72, height: 72)
        case .profileViewIcon:
            return CGSize(width: 72, height: 72)
        case .accountDetail:
            return CGSize(width: 150, height: 150)
        }
    }

    var platformSize: CGFloat {
        switch self {
        case .threadIcon:
            return 18
        case .pinnedThreadIcon:
            return 24
        default:
            return width * (.pi / 8)
        }
    }

    var width: CGFloat {
        rawValue.width
    }

    var height: CGFloat {
        rawValue.height
    }
}

class ThreadAvatarView: UIView {
    enum Source {
        case empty
        case channel(symbol: String)
        case image(URL)
        case single(Schema.User)
        case multiple([Schema.User])

        init(thread: ThreadStore) {
            if let urlString = thread.imgURL, let url = URL(string: urlString) {
                self = .image(url)
            } else {
                let participants = thread.participants.items.elements
                switch thread.type {
                case .single where !participants.isEmpty:
                    self = .single(thread.displayParticipant!)
                case .group:
                    self = .multiple(participants.filter { $0.id != thread.currentUser?.id })
                case .channel:
                    self = .channel(
                        symbol: thread.isTwitterNotifications
                            ? (thread.isUnread ? "bell.badge.fill" : "bell.fill")
                            : "number"
                    )
                case .broadcast:
                    self = .empty
                default:
                    self = .empty
                }
            }
        }
    }

    let size: AvatarSize
    let isSquare: Bool

    var source: Source? {
        didSet {
            configureSubviews(for: source)
        }
    }

    init(
        size: AvatarSize,
        isSquare: Bool = false,
        source: Source? = nil
    ) {
        self.size = size
        self.isSquare = isSquare
        self.source = source

        super.init(frame: CGRect(origin: .zero, size: size.rawValue))

        configureSubviews(for: source)
    }

    required init?(coder: NSCoder) {
        nil
    }

    private func configureSubviews(for source: Source?) {
        subviews.forEach {
            $0.removeFromSuperview()
        }

        NSLayoutConstraint.deactivate(constraints)

        let subview: UIView
        switch source {
        case .none, .empty:
            let emptyView = UIView()
            NSLayoutConstraint.activate([
                emptyView.widthAnchor.constraint(equalToConstant: size.width),
                emptyView.heightAnchor.constraint(equalToConstant: size.height)
            ])
            subview = emptyView

        case .channel(let symbol):
            subview = AvatarView(size: size, symbol: symbol)
        case .image(let url):
            subview = AvatarView(size: size, url: url)
        case .single(let user):
            subview = AvatarView(size: size, user: user)
        case .multiple(let users) where users.isEmpty:
            subview = AvatarView(size: size)
        case .multiple(let users) where users.count == 1:
            subview = AvatarView(size: size, user: users.first!)
        case .multiple(let users) where !isSquare:
            let stackView = UIStackView(
                arrangedSubviews: users.prefix(4).map { user in
                    AvatarView(size: size, user: user)
                }
            )
            stackView.axis = .horizontal
            stackView.arrangedSubviews.forEach(stackView.sendSubviewToBack)
            stackView.spacing = -(size.width / 2)

            subview = stackView
        case .multiple(let users) where isSquare:
            subview = squareLayoutView(
                size: size,
                with: users
            )
        default:
            fatalError("Invalid configuration.")
        }

        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)

        NSLayoutConstraint.activate([
            subview.topAnchor.constraint(equalTo: topAnchor),
            subview.leftAnchor.constraint(equalTo: leftAnchor),
            subview.rightAnchor.constraint(equalTo: rightAnchor),
            subview.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }

    private lazy var platformView = ThreadPlatformView(thread: nil, size: CGSize(width: size.platformSize, height: size.platformSize))

    func addPlatformOverlay(for thread: ThreadStore?) {
        guard let thread = thread else {
            platformView.removeFromSuperview()
            return
        }
        if platformView.superview == nil {
            addSubview(platformView)
            NSLayoutConstraint.activate([
                platformView.heightAnchor.constraint(equalToConstant: (size.platformSize)),
                platformView.widthAnchor.constraint(equalToConstant: (size.platformSize)),
                platformView.centerXAnchor.constraint(equalTo: trailingAnchor, constant: -(size.width * 0.13)),
                platformView.centerYAnchor.constraint(equalTo: bottomAnchor, constant: -(size.height * 0.13)),
            ])
        }

        platformView.setIcon(from: thread)
    }

    private func squareLayoutView(size: AvatarSize, with users: [Schema.User]) -> UIView {
        let scale = users.count == 2 ? CGFloat(0.4) : CGFloat(0.35)
        let scaledSize = AvatarSize.size(size.width * scale)
        let diagonalViews = users.count < 4
        ? users.map { user in
            AvatarView(size: scaledSize, user: user)
        }
        : users.prefix(2).map { user in
            AvatarView(size: scaledSize, user: user)
        } + [
            AvatarView(size: scaledSize, initials: String(users.count - 2))
        ]

        let container = UIView()
        container.layer.cornerRadius = size.height / 2
        container.backgroundColor = .systemFill
        container.translatesAutoresizingMaskIntoConstraints = false

        diagonalViews.forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            container.addSubview($0)
            NSLayoutConstraint.activate([
                $0.widthAnchor.constraint(equalTo: container.widthAnchor, multiplier: scale),
                $0.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: scale),
            ])
        }
        if diagonalViews.count < 3 {
            NSLayoutConstraint.activate([
                diagonalViews[0].leftAnchor.constraint(equalTo: container.centerXAnchor, constant: -1),
                diagonalViews[0].bottomAnchor.constraint(equalTo: container.centerYAnchor, constant: 1),

                diagonalViews[1].rightAnchor.constraint(equalTo: container.centerXAnchor, constant: 1),
                diagonalViews[1].topAnchor.constraint(equalTo: container.centerYAnchor, constant: -1)
            ])
        } else {
            NSLayoutConstraint.activate([
                diagonalViews[0].rightAnchor.constraint(equalTo: container.centerXAnchor, constant: -1),
                diagonalViews[0].bottomAnchor.constraint(equalTo: container.centerYAnchor, constant: -1),

                diagonalViews[1].leftAnchor.constraint(equalTo: container.centerXAnchor, constant: 1),
                diagonalViews[1].bottomAnchor.constraint(equalTo: container.centerYAnchor, constant: 1),

                diagonalViews[2].centerXAnchor.constraint(equalTo: container.centerXAnchor, constant: 0),
                diagonalViews[2].topAnchor.constraint(equalTo: container.centerYAnchor, constant: 1)
            ])
        }

        return container
    }
}

extension ThreadAvatarView {
    private class AvatarView: UIView {
        private(set) var url: URL?
        private(set) var email: String?
        private(set) var initials: String?
        private(set) var symbol: String?

        init(size: Avatar.Size, url: URL? = nil, email: String? = nil, initials: String? = nil, symbol: String? = nil) {
            self.url = url
            self.email = email
            self.initials = initials
            self.symbol = symbol

            super.init(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))

            configureGradient()
            setupView()

            loadAvailableContent()
        }

        convenience init(size: Avatar.Size, user: Schema.User) {
            self.init(size: size)
            self.configure(with: user)
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with user: Schema.User) {
            self.url = user.imgURL.flatMap(URL.init)
            self.email = user.email
            self.initials = { name in
                let formatter = PersonNameComponentsFormatter()
                guard let name = name, let components = formatter.personNameComponents(from: name) else {
                    return nil
                }
                formatter.style = .abbreviated
                return formatter.string(from: components)
            }(user.fullName)
            self.symbol = "person.fill"
            loadAvailableContent()
        }

        // MARK: - Outlets

        private lazy var imageView = UIImageView(frame: bounds)
        private lazy var initialLabel = UILabel(frame: bounds)

        // MARK: - Setup

        private func setupView() {
            layer.masksToBounds = true
            layer.cornerRadius = frame.midY

            imageView.contentMode = .scaleAspectFill
            imageView.tintColor = .white

            initialLabel.textAlignment = .center
            initialLabel.textColor = .white
            initialLabel.font = .systemFont(ofSize: frame.height * 0.4, weight: .semibold)

            [imageView, initialLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                addSubview($0)
            }

            NSLayoutConstraint.activate([
                widthAnchor.constraint(equalToConstant: frame.width),
                heightAnchor.constraint(equalToConstant: frame.height),

                imageView.topAnchor.constraint(equalTo: topAnchor),
                imageView.leftAnchor.constraint(equalTo: leftAnchor),
                imageView.rightAnchor.constraint(equalTo: rightAnchor),
                imageView.bottomAnchor.constraint(equalTo: bottomAnchor),

                initialLabel.centerXAnchor.constraint(equalTo: centerXAnchor),
                initialLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
        }

        private func loadAvailableContent() {
            guard url != nil || email != nil || initials != nil || symbol != nil else {
                return
            }

            let fetchCompleted: SDExternalCompletionBlock = { [unowned self] _, error, _, _ in
                if error != nil {
                    self.loadPlaceholder()
                }
            }
            if let url = url {
                imageView.sd_setImage(with: url, completed: fetchCompleted)
            } else if let email = email, !email.isEmpty, let md5 = try? email.md5() {
                let url = URL(string: "https://gravatar.com/avatar/\(md5).jpg?d=404&size=256")
                imageView.sd_setImage(with: url, completed: fetchCompleted)
            } else {
                loadPlaceholder()
            }
        }

        private func loadPlaceholder() {
            if let initials = initials {
                initialLabel.text = initials.uppercased()
            } else if let symbol = symbol {
                imageView.contentMode = .center
                imageView.image = UIImage(
                    systemName: symbol,
                    withConfiguration: UIImage.SymbolConfiguration(pointSize: bounds.height / 2, weight: .bold)
                )

                let mask = UIView(frame: bounds.insetBy(dx: 2, dy: 2))
                mask.backgroundColor = .white
                mask.clipsToBounds = true
                mask.layer.cornerRadius = mask.bounds.midY

                imageView.mask = mask
            } else {
                initialLabel.text = nil
                imageView.image = nil
            }
        }

        // MARK: - Gradient

        override class var layerClass: AnyClass {
            CAGradientLayer.self
        }

        private func configureGradient() {
            guard let gradient = layer as? CAGradientLayer else {
                fatalError("Wrong layer class")
            }

            gradient.colors = [UIColor(hex: 0xC1C5CF).cgColor, UIColor(hex: 0x848994).cgColor]
            gradient.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradient.endPoint = CGPoint(x: 0.5, y: 1.0)
        }
    }
}
