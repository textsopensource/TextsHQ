//
// Copyright (c) Texts HQ
//

import UIKit

extension ThreadViewController {
    class AttachmentView: UIImageView {
        override init(frame: CGRect) {
            super.init(frame: frame)

            clipsToBounds = true
            contentMode = .scaleAspectFill
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
