//
// Copyright (c) Texts HQ
//

import Macaw
import TextsCore
import UIKit

typealias ThreadPlatformView = ThreadViewController.PlatformIconView

extension ThreadViewController {
    class PlatformIconView: UIImageView {
        let size: CGSize

        init(thread: ThreadStore?, size: CGSize = CGSize(width: 25, height: 25)) {
            self.size = size
            super.init(frame: CGRect(origin: .zero, size: size))

            if let thread = thread {
                setIcon(from: thread)
            }

            translatesAutoresizingMaskIntoConstraints = false
            NSLayoutConstraint.activate([
                widthAnchor.constraint(equalToConstant: size.width),
                heightAnchor.constraint(equalToConstant: size.height)
            ])
        }

        fileprivate static var iconCache = [String: UIImage]()

        func setIcon(from thread: ThreadStore) {
            guard let platform = thread.account?.platform else {
                return
            }

            let cacheKey = platform.displayName + ":\(size.width)x\(size.height)"
            if let cached = Self.iconCache[cacheKey] {
                image = cached
            } else if let iconString = platform.icon, let svg = try? SVGParser.parse(text: iconString) {
                let iconImage = svg.toNativeImage(
                    size: .init(
                        size.width * UIScreen.main.scale,
                        size.height * UIScreen.main.scale
                    )
                )
                image = iconImage
                Self.iconCache[cacheKey] = iconImage
            }
        }

        required init?(coder: NSCoder) {
            nil
        }
    }

}
