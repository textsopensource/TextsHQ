//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Macaw

extension ComposeViewController {

    enum SearchResult {
        case user(Schema.User), thread(ThreadStore)
    }

    class ResultCell: UITableViewCell {
        private(set) lazy var avatarView = ThreadAvatarView(size: .searchResult)
        private(set) lazy var primaryLabel = UILabel()
        private(set) lazy var secondaryLabel = UILabel()
        private(set) lazy var platformIcon = UIImageView()

        private var primaryLabelVerticalConstraint: NSLayoutConstraint?

        // MARK: - Init

        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)

            primaryLabel.font = .preferredFont(forTextStyle: .headline)
            primaryLabel.textColor = .label

            secondaryLabel.font = .preferredFont(forTextStyle: .subheadline)
            secondaryLabel.textColor = .secondaryLabel

            platformIcon.layer.shadowColor = UIColor.black.withAlphaComponent(0.1).cgColor
            platformIcon.layer.shadowOffset = CGSize(width: 0, height: 1)
            platformIcon.layer.shadowRadius = 5

            [
                avatarView,
                platformIcon,
                primaryLabel,
                secondaryLabel,
            ].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                avatarView.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
                avatarView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                primaryLabel.leftAnchor.constraint(equalTo: avatarView.rightAnchor, constant: 12),
                primaryLabel.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),

                secondaryLabel.leftAnchor.constraint(equalTo: avatarView.rightAnchor, constant: 12),
                secondaryLabel.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),
                secondaryLabel.topAnchor.constraint(equalTo: centerYAnchor),

                platformIcon.heightAnchor.constraint(equalToConstant: Avatar.Size.threadIcon.rawValue.width / 2.5),
                platformIcon.widthAnchor.constraint(equalTo: platformIcon.heightAnchor),
                platformIcon.rightAnchor.constraint(equalTo: avatarView.rightAnchor, constant: 4),
                platformIcon.bottomAnchor.constraint(equalTo: avatarView.bottomAnchor, constant: 4)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Setup

        func configure(with result: SearchResult) {
            switch result {
            case .user(let user):
                avatarView.source = .single(user)
                primaryLabel.text = user.displayName
                secondaryLabel.text = user.username ?? user.phoneNumber ?? user.email
                platformIcon.image = nil

            case .thread(let thread):
                avatarView.source = .init(thread: thread)
                primaryLabel.text = thread.name
                secondaryLabel.text = nil
                let size = Avatar.Size.threadIcon.rawValue.width * UIScreen.main.scale
                platformIcon.image = try? thread.account?.accountInstance().platform
                    .icon.flatMap(SVGParser.parse(text:))?
                    .toNativeImage(size: .init(size, size))
            }

            primaryLabelVerticalConstraint?.isActive = false
            primaryLabelVerticalConstraint = secondaryLabel.text.isNilOrEmpty
                ? primaryLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
                : primaryLabel.bottomAnchor.constraint(equalTo: contentView.centerYAnchor)
            primaryLabelVerticalConstraint?.isActive = true
            primaryLabel.layoutIfNeeded()
        }
    }

    class NoResultsCell: UITableViewCell {
        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)

            let label = UILabel()
            label.text = "No results"
            label.font = .preferredFont(forTextStyle: .subheadline)
            label.textColor = .tertiaryLabel

            label.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(label)

            NSLayoutConstraint.activate([
                label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
