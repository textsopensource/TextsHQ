//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Macaw

extension ComposeViewController {
    class FromCell: UITableViewCell {
        private(set) lazy var accountButton = UIButton()
        private(set) lazy var platformIcon = UIImageView()
        private(set) lazy var usernameLabel = UILabel()

        var menuProvider: () -> UIMenu = {
            fatalError("'From:' field UIMenu must be provided")
        }

        // MARK: - Configuration

        func configure(with account: AccountInstance?) {
            usernameLabel.text = account?.user?.displayName
            platformIcon.image = try? account?.platform.icon.flatMap(SVGParser.parse(text:))?.toNativeImage(size: .init(100, 100))
            accountButton.menu = menuProvider()
        }

        // MARK: - Init

        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)

            accountButton.showsMenuAsPrimaryAction = true
            accountButton.menu = UIMenu(
                children: [
                    UIDeferredMenuElement { [unowned self] completion in
                        completion([self.menuProvider()])
                    }
                ]
            )

            let fromLabel = UILabel()
            fromLabel.text = "From:"
            fromLabel.textColor = .secondaryLabel
            fromLabel.font = .preferredFont(forTextStyle: .subheadline)

            usernameLabel.textColor = .label
            usernameLabel.font = .preferredFont(forTextStyle: .subheadline)

            [
                fromLabel,
                platformIcon,
                usernameLabel,
                accountButton
            ].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                accountButton.topAnchor.constraint(equalTo: contentView.topAnchor),
                accountButton.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                accountButton.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                accountButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                fromLabel.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
                fromLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                usernameLabel.leftAnchor.constraint(equalTo: fromLabel.rightAnchor, constant: 9),
                usernameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                platformIcon.heightAnchor.constraint(equalToConstant: 16),
                platformIcon.widthAnchor.constraint(equalToConstant: 16),
                platformIcon.leftAnchor.constraint(equalTo: usernameLabel.rightAnchor, constant: 4),
                platformIcon.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
