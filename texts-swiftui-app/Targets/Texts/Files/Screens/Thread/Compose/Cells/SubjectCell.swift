//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension ComposeViewController {
    class SubjectCell: UITableViewCell {
        private(set) lazy var requiredIndicator = UIImageView()
        private(set) lazy var textField = UITextField()

        var isRequired = false {
            didSet {
                requiredIndicator.isHidden = !isRequired
            }
        }

        var subjectHandler: (String?) -> Void = { _ in
            fatalError("'Subject:' update handler must be provided")
        }

        // MARK: - Init

        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)

            let titleLabel = UILabel()
            titleLabel.text = "Title:"
            titleLabel.textColor = .secondaryLabel
            titleLabel.font = .preferredFont(forTextStyle: .subheadline)
            titleLabel.setContentHuggingPriority(.required, for: .horizontal)

            textField.font = .preferredFont(forTextStyle: .subheadline)
            textField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)

            requiredIndicator.contentMode = .scaleAspectFit
            requiredIndicator.image = UIImage(systemName: "exclamationmark.circle.fill")
            requiredIndicator.tintColor = .red

            [
                titleLabel,
                textField,
                requiredIndicator
            ].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                titleLabel.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor),
                titleLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                textField.leftAnchor.constraint(equalTo: titleLabel.rightAnchor, constant: 8),
                textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                textField.heightAnchor.constraint(equalTo: titleLabel.heightAnchor),
                textField.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor),

                requiredIndicator.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor),
                requiredIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                requiredIndicator.heightAnchor.constraint(equalToConstant: 16),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Actions

        @objc private func textFieldChanged() {
            if isRequired {
                requiredIndicator.isHidden = !textField.text.isNilOrEmpty
            }
            subjectHandler(textField.text)
        }
    }
}
