//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import Macaw

extension ComposeViewController {
    class ToCell: UITableViewCell {
        private(set) lazy var accountButton = UIButton()
        private(set) lazy var usernameLabel = UILabel()
        private(set) lazy var loadingIndicator = UIActivityIndicatorView(style: .medium)
        private(set) lazy var textField = UITextField()

        var menuProvider: () -> UIMenu = {
            fatalError("'To:' field UIMenu must be provided")
        }

        var searchHandler: (String?) -> Void = { _ in
            fatalError("'To:' search handler must be provided")
        }

        // MARK: - Configuration

        func configure(with user: Schema.User?) {
            textField.text = ""
            usernameLabel.text = user?.displayName
            textField.isEnabled = user.isNil
            accountButton.isHidden = user.isNil
        }

        // MARK: - Init

        override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)

            accountButton.showsMenuAsPrimaryAction = true
            accountButton.menu = UIMenu(
                children: [
                    UIDeferredMenuElement { [unowned self] completion in
                        completion([self.menuProvider()])
                    }
                ]
            )

            let fromLabel = UILabel()
            fromLabel.text = "To:"
            fromLabel.textColor = .secondaryLabel
            fromLabel.font = .preferredFont(forTextStyle: .subheadline)
            fromLabel.setContentHuggingPriority(.required, for: .horizontal)

            usernameLabel.textColor = .label
            usernameLabel.font = .preferredFont(forTextStyle: .subheadline)

            textField.font = .preferredFont(forTextStyle: .subheadline)
            textField.addTarget(self, action: #selector(textFieldChanged), for: .editingChanged)

            loadingIndicator.startAnimating()

            [
                fromLabel,
                usernameLabel,
                textField,
                loadingIndicator,
                accountButton
            ].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                accountButton.topAnchor.constraint(equalTo: contentView.topAnchor),
                accountButton.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                accountButton.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                accountButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                fromLabel.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
                fromLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                usernameLabel.leftAnchor.constraint(equalTo: fromLabel.rightAnchor, constant: 9),
                usernameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                usernameLabel.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),

                textField.leftAnchor.constraint(equalTo: fromLabel.rightAnchor, constant: 8),
                textField.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                textField.heightAnchor.constraint(equalTo: fromLabel.heightAnchor),
                textField.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),

                loadingIndicator.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),
                loadingIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                loadingIndicator.heightAnchor.constraint(equalToConstant: 16),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Actions

        @objc private func textFieldChanged() {
            searchHandler(textField.text)
        }
    }
}
