//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension ComposeViewController {
    func configureDetachedInputView() {
        detachedInputController.isSendEnabled = false
        detachedInputController.modalPresentationController = self
        detachedInputController.sendButton.onSend = { [unowned self] in
            createNewThread()
        }

        detachedInputController.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(detachedInputController)

        NSLayoutConstraint.activate([
            detachedInputController.bottomAnchor.constraint(equalTo: keyboardGuide.topAnchor),
            detachedInputController.contentView.bottomAnchor.constraint(lessThanOrEqualTo: keyboardGuide.topAnchor, constant: -8),
            detachedInputController.contentView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor),
            detachedInputController.leftAnchor.constraint(equalTo: view.leftAnchor),
            detachedInputController.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])

        if let draft = AppCache.retrieve(
            MessageDraft.self,
            key: ThreadViewController.InputController.draftKey(for: nil)
        ) {
            detachedInputController.messageDraft = draft
        }
    }

    func loadViewController(with existingThread: ThreadStore) {
        resetThreadViewController()
        detachedInputController.isHidden = true

        let threadViewController = ThreadViewController(viewModel: .init(thread: existingThread))
        addChild(threadViewController)

        let threadView = threadViewController.view!
        threadView.frame = CGRect(
            x: 0,
            y: fieldTableView.frame.maxY,
            width: view.frame.width,
            height: view.frame.height - fieldTableView.frame.maxY
        )
        threadView.clipsToBounds = true
        threadView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(threadView)
        threadViewController.didMove(toParent: self)

        NSLayoutConstraint.activate([
            threadView.topAnchor.constraint(equalTo: fieldTableView.bottomAnchor),
            threadView.leftAnchor.constraint(equalTo: view.leftAnchor),
            threadView.rightAnchor.constraint(equalTo: view.rightAnchor),
            threadView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])

        threadViewController.inputController.thread = existingThread
        threadViewController.inputController.messageDraft = detachedInputController.messageDraft
        threadViewController.inputController.onSend = { [unowned self] _ in
            navigate(to: existingThread)
        }
        threadViewController.inputController.textView.becomeFirstResponder()

        self.selectedThreadViewController = threadViewController
    }

    func resetThreadViewController() {
        detachedInputController.isHidden = false

        guard let existingThreadViewController = selectedThreadViewController else {
            return
        }

        existingThreadViewController.willMove(toParent: nil)
        existingThreadViewController.view.removeFromSuperview()
        existingThreadViewController.removeFromParent()

        selectedThreadViewController = nil
    }
}
