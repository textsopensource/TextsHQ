//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

// This entire file is dead code until group chats are to be implemented
/*
 extension ComposeViewController: RecipientCollectionViewDelegate {
 func searchTextDidChange(_ query: String) {
 searchUsers(with: query)
 }

 func didRemove(_ user: Schema.User) {
 guard let index = toUsers.firstIndex(of: user) else {
 return
 }

 toUsers.remove(at: index)
 configureContentView()
 }

 func pickedItems() -> [Schema.User] {
 toUsers
 }

 func selectedItem() -> Schema.User? {
 highlightedToUser
 }

 func didHighlight(_ user: Schema.User) {
 highlightedToUser = user
 }
 }

 protocol RecipientCollectionViewDelegate: AnyObject {

 func selectedItem() -> Schema.User?
 func pickedItems() -> [Schema.User]

 func didRemove(_ user: Schema.User)
 func didHighlight(_ user: Schema.User)

 func searchTextDidChange(_ query: String)
 }

 extension ComposeViewController {
 class RecipientCollectionView: UICollectionView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
 enum ItemType {
 case toLabel
 case user(Schema.User)
 case textField
 }

 unowned var viewDelegate: RecipientCollectionViewDelegate! {
 didSet {
 self.items = [.toLabel] + viewDelegate.pickedItems().map { .user($0) } + [.textField]
 }
 }

 private var items = [ItemType]()
 private lazy var queryString = emptyTextFieldString

 fileprivate let invisibleSign = "\u{200B}"
 fileprivate let emptyTextFieldString = "\u{200B} "

 init() {
 super.init(frame: .zero, collectionViewLayout: Layout())

 (collectionViewLayout as? Layout)?.itemSizeProvider = { [unowned self] indexPath in
 self.minimumSizeForItem(at: indexPath)
 }

 delegate = self
 dataSource = self
 backgroundColor = .white
 contentInset = .zero

 register(ToLabelCell.self, forCellWithReuseIdentifier: "ToLabelCell")
 register(PickedUserCell.self, forCellWithReuseIdentifier: "PickedUserCell")
 register(SearchCell.self, forCellWithReuseIdentifier: "SearchCell")
 }

 required init?(coder: NSCoder) {
 nil
 }

 func append(_ user: Schema.User) {
 items.insert(.user(user), at: items.count - 2)
 queryString = emptyTextFieldString

 performBatchUpdates {
 insertItems(
 at: [IndexPath(item: items.count - 1, section: 0)]
 )
 reloadItems(
 at: [IndexPath(item: items.count, section: 0)]
 )
 }
 }

 func focusCellAt(indexPath: IndexPath) {
 switch items[indexPath.item] {
 case .textField:
 focusTextCell()
 case .user(let user):
 becomeFirstResponder()
 selectItem(at: indexPath, animated: true, scrollPosition: .centeredVertically)
 collectionView(self, didSelectItemAt: indexPath)
 viewDelegate.didHighlight(user)
 default:
 break
 }
 }

 func focusTextCell() {
 searchCell()?.textField.becomeFirstResponder()
 }

 func removeItem(at indexPath: IndexPath) {
 switch items[indexPath.item] {
 case .user(let user):
 items.remove(at: indexPath.row)
 deselectItem(at: indexPath, animated: true)
 deleteItems(at: [indexPath])
 viewDelegate.didRemove(user)
 case .textField:
 queryString = emptyTextFieldString
 focusTextCell()
 default:
 break
 }
 }

 private func searchCell() -> SearchCell? {
 for cell in visibleCells {
 if let searchCell = cell as? SearchCell {
 return searchCell
 }
 }

 return nil
 }

 private func unselectSelectedCell(animated: Bool) {
 if let selectedIndexPath = indexPathsForSelectedItems?.first {
 deselectItem(at: selectedIndexPath, animated: true)
 collectionView(self, didDeselectItemAt: selectedIndexPath)
 }
 }

 private func update(query: String) {
 queryString = query

 setCollectionViewLayout(Layout(), animated: false)

 let sanitizedQuery = query.replacingOccurrences(of: invisibleSign, with: "").trimmingCharacters(in: .whitespaces)
 viewDelegate.searchTextDidChange(sanitizedQuery)
 }

 private func titleForItem(at indexPath: IndexPath) -> String {
 switch items[indexPath.item] {
 case .toLabel:
 return "To:"
 case .user(let user):
 return user.displayName
 case .textField:
 return queryString
 }
 }

 func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
 items.count
 }

 func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
 switch items[indexPath.item] {
 case .toLabel:
 return collectionView.dequeueReusableCell(withReuseIdentifier: "ToLabelCell", for: indexPath)
 case .user(let user):
 guard let userCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PickedUserCell", for: indexPath) as? PickedUserCell else {
 fatalError("Cell not registered")
 }

 userCell.usernameLabel.text = user.displayName

 if user == viewDelegate.selectedItem() {
 userCell.isSelected = true
 } else {
 userCell.isSelected = false
 }

 return userCell

 case .textField:
 guard let searchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCell", for: indexPath) as? SearchCell else {
 fatalError("Cell not registered")
 }

 searchCell.textField.text = queryString
 searchCell.textField.delegate = self
 searchCell.textField.inputDelegate = self
 searchCell.textField.addTarget(self, action: #selector(textFieldDidEdit(_:)), for: .editingChanged)

 return searchCell
 }
 }

 func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 if case .user = items[indexPath.item] {
 collectionView.becomeFirstResponder()
 collectionView.selectItem(at: indexPath, animated: true, scrollPosition: [])
 }
 }

 func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
 /*if let cell = collectionView.cellForItem(at: indexPath) as? NewThreadPickedUserCell {
 cell.setSelected(selected: false, animated: true)
 }*/
 }

 func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
 guard case .user = items[indexPath.item] else {
 return false
 }

 return true
 }

 func minimumSizeForItem(at indexPath: IndexPath) -> CGSize {
 CGSize(
 width: titleForItem(at: indexPath).boundingRect(
 with: CGSize(width: .greatestFiniteMagnitude, height: 100.0),
 options: [.usesLineFragmentOrigin, .usesFontLeading],
 attributes: [.font: UIFont.preferredFont(forTextStyle: .subheadline)],
 context: nil
 ).width,
 height: UIFont.preferredFont(forTextStyle: .subheadline).lineHeight
 )
 }

 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
 .zero
 }

 func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
 .zero
 }
 }
 }

 // MARK: - Key Input -
 extension ComposeViewController.RecipientCollectionView: UIKeyInput, UITextFieldDelegate, UITextInputDelegate {

 override var canBecomeFirstResponder: Bool {
 true
 }

 override func resignFirstResponder() -> Bool {
 unselectSelectedCell(animated: true)
 return super.resignFirstResponder()
 }

 var hasText: Bool {
 items.contains {
 switch $0 {
 case .user:
 return true
 default:
 return false
 }
 }
 }

 func deleteBackward() {
 guard let indexPath = indexPathsForSelectedItems?.first else {
 return
 }

 removeItem(at: indexPath)
 }

 func insertText(_ text: String) { }

 func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
 unselectSelectedCell(animated: true)
 return true
 }

 @objc func textFieldDidEdit(_ textField: UITextField) {
 if let text = textField.text {
 if text.count == 1, text != invisibleSign {
 textField.text = invisibleSign.appending(text)
 }

 update(query: text)
 }
 }

 func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
 let char = string.cString(using: .utf8)
 let isBackSpace = strcmp(char, "\\b") == -92
 if isBackSpace, var text = textField.text {
 text = text.replacingOccurrences(of: invisibleSign, with: "")
 if text.count == 1 {
 focusCellAt(indexPath: IndexPath(row: items.count - 1, section: 0))
 return false
 }
 }

 return true
 }

 // Un-needed required UITextInputDelegate methods
 func selectionWillChange(_ textInput: UITextInput?) { }

 func selectionDidChange(_ textInput: UITextInput?) { }

 func textWillChange(_ textInput: UITextInput?) { }

 func textDidChange(_ textInput: UITextInput?) { }
 }

 // MARK: - Cells -
 extension ComposeViewController.RecipientCollectionView {
 class ToLabelCell: UICollectionViewCell {

 lazy var label = UILabel()

 override init(frame: CGRect) {
 super.init(frame: frame)

 layer.masksToBounds = true
 layer.cornerRadius = 5.0

 label.text = "To:"
 label.font = .preferredFont(forTextStyle: .subheadline)
 label.textColor = .secondaryLabel

 label.translatesAutoresizingMaskIntoConstraints = false
 contentView.addSubview(label)

 NSLayoutConstraint.activate([
 label.leftAnchor.constraint(equalTo: contentView.leftAnchor),
 label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
 ])
 }

 required init?(coder: NSCoder) {
 nil
 }

 override var isSelected: Bool {
 didSet {
 contentView.backgroundColor = isSelected ? UIColor(.accentColor) : .clear
 label.textColor = isSelected ? .white : UIColor(.accentColor)
 }
 }
 }

 class PickedUserCell: UICollectionViewCell {

 lazy var usernameLabel = UILabel()

 override init(frame: CGRect) {
 super.init(frame: frame)

 layer.masksToBounds = true
 layer.cornerRadius = 5.0

 usernameLabel.font = .preferredFont(forTextStyle: .subheadline)
 usernameLabel.textColor = UIColor(.accentColor)

 usernameLabel.translatesAutoresizingMaskIntoConstraints = false
 contentView.addSubview(usernameLabel)

 NSLayoutConstraint.activate([
 usernameLabel.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
 usernameLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
 ])
 }

 required init?(coder: NSCoder) {
 nil
 }

 override var isSelected: Bool {
 didSet {
 contentView.backgroundColor = isSelected ? UIColor(.accentColor) : .clear
 usernameLabel.textColor = isSelected ? .white : UIColor(.accentColor)
 }
 }
 }

 class SearchCell: UICollectionViewCell {

 lazy var textField = UITextField()

 override init(frame: CGRect) {
 super.init(frame: frame)

 textField.font = .preferredFont(forTextStyle: .subheadline)
 textField.autocorrectionType = .no
 textField.textContentType = .nickname

 textField.translatesAutoresizingMaskIntoConstraints = false
 contentView.addSubview(textField)

 NSLayoutConstraint.activate([
 textField.topAnchor.constraint(equalTo: contentView.topAnchor),

 textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
 textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
 textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor)
 ])
 }

 required init?(coder: NSCoder) {
 nil
 }
 }
 }

 // MARK: - Layout -
 extension ComposeViewController.RecipientCollectionView {
 class Layout: UICollectionViewFlowLayout {
 typealias ItemSizeProvider = (IndexPath) -> CGSize
 var itemSizeProvider: ItemSizeProvider = { _ in .zero }

 override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
 (super.layoutAttributesForElements(in: rect) ?? [])
 .compactMap { layoutAttributesForItem(at: $0.indexPath) }
 }

 override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
 guard
 let collectionView = collectionView,
 0 ..< collectionView.numberOfItems(inSection: indexPath.section) ~= indexPath.row,
 let attributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes
 else {
 return nil
 }

 attributes.frame = frameForItem(at: indexPath)

 return attributes
 }

 private func frameForItem(at indexPath: IndexPath) -> CGRect {
 guard
 let collectionView = collectionView as? ComposeViewController.RecipientCollectionView,
 0 ..< collectionView.numberOfItems(inSection: indexPath.section) ~= indexPath.row
 else {
 return .zero
 }

 let maxWidth = collectionView.bounds.width
 let cellPadding = CGFloat(2.0)
 let cellSize = itemSizeProvider(indexPath)
 let cellWidth = cellSize.height + cellPadding * 4
 let isTextCell: Bool = {
 if case .textField = collectionView.items[indexPath.item] {
 return true
 }
 return false
 }()
 let interitemSpacing = CGFloat(0.0)
 let precedingIndexPath = IndexPath(row: indexPath.row - 1, section: indexPath.section)

 if let precedingCellAttributes = layoutAttributesForItem(at: precedingIndexPath) {
 let remainingWidth = maxWidth - (precedingCellAttributes.frame.maxY + cellPadding * 2)
 if (precedingCellAttributes.frame.maxX + cellWidth) < maxWidth {
 return CGRect(
 x: precedingCellAttributes.frame.maxX + interitemSpacing,
 y: precedingCellAttributes.frame.minY,
 width: isTextCell ? remainingWidth : cellWidth,
 height: cellSize.height
 )
 } else {
 return CGRect(
 x: collectionView.contentInset.left,
 y: precedingCellAttributes.frame.maxY + interitemSpacing,
 width: isTextCell ? maxWidth : cellWidth,
 height: cellSize.height
 )
 }
 } else {
 return CGRect(
 x: collectionView.contentInset.left,
 y: collectionView.contentInset.top,
 width: isTextCell ? maxWidth : cellWidth,
 height: cellSize.height
 )
 }
 }

 private var deleteIndexPaths: [IndexPath]?
 private var insertIndexPaths: [IndexPath]?

 override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
 super.prepare(forCollectionViewUpdates: updateItems)

 deleteIndexPaths = updateItems
 .filter { $0.updateAction == .delete }
 .compactMap { $0.indexPathBeforeUpdate }

 insertIndexPaths = updateItems
 .filter { $0.updateAction == .insert }
 .compactMap { $0.indexPathAfterUpdate }
 }

 override func finalizeCollectionViewUpdates() {
 super.finalizeCollectionViewUpdates()

 deleteIndexPaths = nil
 insertIndexPaths = nil
 }
 /*
 override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
 guard let attributes = (super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath) ?? layoutAttributesForItem(at: itemIndexPath))?.copy() as? UICollectionViewLayoutAttributes else {
 return nil
 }

 if let insertIndexPaths = insertIndexPaths, insertIndexPaths.contains(itemIndexPath) {
 attributes.frame = frameForItem(at: itemIndexPath)
 attributes.zIndex = -1
 }

 if let deleteIndexPaths = deleteIndexPaths, let indexPathToDelete = deleteIndexPaths.first, itemIndexPath.row > indexPathToDelete.row {
 // attributes.frame = frameForItem(at: IndexPath(row: itemIndexPath.row + 1, section: itemIndexPath.section))
 }

 return attributes
 }

 override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
 guard let attributes = (super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath) ?? layoutAttributesForItem(at: itemIndexPath))?.copy() as? UICollectionViewLayoutAttributes else {
 return nil
 }

 if let deleteIndexPaths = deleteIndexPaths, deleteIndexPaths.contains(itemIndexPath) {
 attributes.alpha = 0.0
 let endFrame = frameForItem(at: itemIndexPath)
 attributes.frame = CGRect(x: endFrame.minX, y: endFrame.minY, width: 0, height: endFrame.height)
 } else {
 attributes.frame = frameForItem(at: itemIndexPath)
 }

 return attributes
 }*/
 }
 }
 */
