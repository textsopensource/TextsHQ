//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore

extension ComposeViewController {
    func presentSendWithoutToUserAlert() {
        let alert = UIAlertController(
            title: "Could Not Send Message",
            message: "Recipient is not selected",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default))
        present(alert, animated: true)
    }

    func presentSendWithoutFromAccountStoreAlert() {
        let alert = UIAlertController(
            title: "Could Not Send Message",
            message: "The account you are sending from is not loaded.",
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "Dismiss", style: .default))
        present(alert, animated: true)
    }

    func presentDraftDecisionSheet() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(.init(title: "Delete Draft", style: .destructive) { [unowned self] _ in
             Task { @MainActor in
                 try await AppCache.cache(
                    MessageDraft(),
                    key: ThreadViewController.InputController.draftKey(
                        for: selectedThreadViewController?.viewModel.thread
                    )
                 )
             }

            dismiss(animated: true)
        })
        alert.addAction(.init(title: "Save Draft", style: .default) { [unowned self] _ in
            dismiss(animated: true)
        })
        alert.addAction(.init(title: "Cancel", style: .cancel))
        present(alert, animated: true)
    }
}
