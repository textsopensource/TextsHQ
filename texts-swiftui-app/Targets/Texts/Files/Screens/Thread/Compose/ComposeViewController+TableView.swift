//
// Copyright (c) Texts HQ
//

import UIKit
import Macaw
import TextsCore

extension ComposeViewController: UITableViewDataSource, UITableViewDelegate {

    /// Returns the first existing instance of the cell without dequeueing
    func existingCell<T: UITableViewCell>(ofKind kind: T.Type) -> T? {
        for cell in fieldTableView.visibleCells {
            if let cell = cell as? T {
                return cell
            }
        }
        return nil
    }

    func configureTableViews() {
        // - Fields
        fieldTableView.isScrollEnabled = false
        fieldTableView.showsVerticalScrollIndicator = false
        fieldTableView.allowsMultipleSelection = false
        fieldTableView.tableHeaderView = UIView()
        fieldTableView.dataSource = self
        fieldTableView.delegate = self

        fieldTableView.register(FromCell.self, forCellReuseIdentifier: "FromCell")
        fieldTableView.register(ToCell.self, forCellReuseIdentifier: "ToCell")
        fieldTableView.register(SubjectCell.self, forCellReuseIdentifier: "SubjectCell")

        // - Search
        resultTableView.showsVerticalScrollIndicator = false
        resultTableView.allowsMultipleSelection = false
        resultTableView.tableHeaderView = UIView()
        resultTableView.tableFooterView = UIView()
        resultTableView.dataSource = self
        resultTableView.delegate = self

        resultTableView.register(ResultCell.self, forCellReuseIdentifier: "ResultCell")
        resultTableView.register(NoResultsCell.self, forCellReuseIdentifier: "NoResultsCell")
    }

    // MARK: - Delegate

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        switch tableView {
        case fieldTableView:
            switch indexPath.row {
            case 1:
                toCell?.textField.becomeFirstResponder()
            case 2:
                subjectCell?.textField.becomeFirstResponder()

            default:
                break
            }
        case resultTableView:
            switch searchResults[indexPath.item] {
            case .user(let user):
                selectToUser(user)
            case .thread(let thread):
                selectToThread(thread)
            }

            Task { @MainActor in
                await search(with: nil)
            }

        default:
            break
        }
    }

    // MARK: - Data Source

    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch tableView {
        case fieldTableView:
            return 2 + Int(isSubjectAvailable)
        case resultTableView:
            if isSearching, searchResults.isEmpty {
                return 1
            } else if isSearching {
                return searchResults.count
            } else {
                return 0
            }

        default:
            fatalError("numberOfRows requested for unsupported tableView.")
        }
    }

    // swiftlint:disable:next cyclomatic_complexity
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableView {
        case fieldTableView:
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "FromCell", for: indexPath) as? FromCell else {
                    fatalError("FromCell not registered.")
                }

                cell.menuProvider = { [unowned self] in
                    UIMenu(
                        options: [.displayInline],
                        children: fromAccounts.map { account in
                            UIAction(
                                title: account.user?.displayName ?? "",
                                image: try? account.platform.icon.flatMap(SVGParser.parse(text:))?.toNativeImage(size: .init(100, 100))
                            ) { [unowned self] _ in
                                selectAccount(account)
                            }
                        } + (fromAccount == nil ? [] : [
                            UIAction(
                                title: "Remove",
                                image: UIImage(systemName: "minus.circle"),
                                attributes: .destructive
                            ) { [unowned self] _ in
                                selectAccount(nil)
                            }
                        ])
                    )
                }
                cell.configure(with: fromAccount)

                return cell

            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ToCell", for: indexPath) as? ToCell else {
                    fatalError("ToCell not registered.")
                }

                cell.configure(with: toUser)
                cell.loadingIndicator.isHidden = !searchLoading
                cell.searchHandler = { [unowned self] query in
                    Task { @MainActor in
                        await self.search(with: query)
                    }
                }

                cell.menuProvider = { [unowned self] in
                    UIMenu(
                        options: [.displayInline],
                        children: [
                            UIAction(
                                title: "Remove",
                                image: UIImage(systemName: "minus.circle"),
                                attributes: .destructive
                            ) { [unowned self] _ in
                                self.selectToUser(nil)
                                toCell?.textField.becomeFirstResponder()
                            }
                        ]
                    )
                }

                return cell

            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectCell", for: indexPath) as? SubjectCell else {
                    fatalError("SubjectCell not registered.")
                }

                cell.isRequired = isSubjectRequired
                cell.subjectHandler = { [unowned self] subject in
                    self.subject = subject ?? ""
                }

                return cell

            default:
                fatalError("Cell requested for unsupported indexPath \(indexPath).")
            }

        case resultTableView:
            if isSearching, !searchResults.isEmpty {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "ResultCell", for: indexPath) as? ResultCell else {
                    fatalError("ResultCell not registered.")
                }

                cell.configure(with: searchResults[indexPath.row])
                cell.platformIcon.isHidden = fromAccount != nil

                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "NoResultsCell", for: indexPath) as? NoResultsCell else {
                    fatalError("NoResultsCell not registered.")
                }

                return cell
            }
        default:
            fatalError("cell requested for unsupported tableView.")
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch tableView {
        case fieldTableView:
            return 44.0
        case resultTableView:
            return 60
        default:
            fatalError("heightForRowAt requested for unsupported tableView.")
        }
    }
}
