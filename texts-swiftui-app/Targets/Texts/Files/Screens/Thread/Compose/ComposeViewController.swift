//
// Copyright (c) Texts HQ
//

import UIKit
import SwiftUI
import TextsCore

// swiftlint:disable:next type_body_length
class ComposeViewController: UIViewController, UIAdaptivePresentationControllerDelegate {
    private let accounts: AccountsStore

    var fromAccount: AccountInstance?

    var toUser: Schema.User?

    var subject = ""
    let isSubjectAvailable = false
    let isSubjectRequired = false

    var searchResults = [SearchResult]()
    var isSearching = false
    var searchLoading = false

    // MARK: - Init

    init(initialAccount: AccountInstance? = nil, accounts: AccountsStore) {
        self.fromAccount = initialAccount
        self.accounts = accounts

        super.init(nibName: nil, bundle: nil)

        presentationController?.delegate = self
    }

    required init?(coder: NSCoder) {
        nil
    }

    // MARK: - Navigation Controller

    class NavigationController: UINavigationController {
        convenience init(
            initialAccount: AccountInstance? = nil, accounts: AccountsStore,
            presentingNavigationController: UINavigationController? = nil
        ) {
            self.init(nibName: nil, bundle: nil)
            navigationBar.prefersLargeTitles = true

            let composeViewController = ComposeViewController(initialAccount: initialAccount, accounts: accounts)
            presentationController?.delegate = composeViewController
            setViewControllers([composeViewController], animated: false)
        }
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationItem()
        configureTableViews()
        configureSubviews()
        configureDetachedInputView()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if toUser == nil {
            toCell?.textField.becomeFirstResponder()
        } else {
            detachedInputController.textView.becomeFirstResponder()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        headerHeightObservation?.invalidate()
        if let observer = keyboardNotificationToken {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        (selectedThreadViewController?.inputController ?? detachedInputController)?.messageDraft.isEmpty ?? true
    }

    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        presentDraftDecisionSheet()
    }

    // MARK: - Observations

    var headerHeightObservation: NSKeyValueObservation?
    var keyboardNotificationToken: NSObjectProtocol?

    // MARK: - Subviews

    lazy var fieldTableView = UITableView()

    var toCell: ToCell? { existingCell(ofKind: ToCell.self) }
    var fromCell: FromCell? { existingCell(ofKind: FromCell.self) }
    var subjectCell: SubjectCell? { existingCell(ofKind: SubjectCell.self) }

    lazy var separatorView = UIView()
    lazy var resultTableView = UITableView()

    lazy var detachedInputController = ThreadViewController.InputController()
    var selectedThreadViewController: ThreadViewController?

    // MARK: - Constraints

    lazy var fieldTableViewHeightConstraint = fieldTableView.heightAnchor.constraint(
        equalToConstant: fieldTableView.contentSize.height
    )

    // MARK: - Convenience

    lazy var fromAccounts = accounts.accounts.filter {
        $0.state.isActive && !$0.platform.attributes.contains(.noSupportSingleThreadCreation)
    }

    // MARK: - View Configuration

    private func configureNavigationItem() {
        navigationItem.title = "New Message"
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            systemItem: .cancel,
            primaryAction: UIAction { [weak self] _ in
                self?.navigationController?.dismiss(animated: true)
            }
        )
    }

    private func configureSubviews() {
        view.backgroundColor = .systemBackground

        resultTableView.isHidden = true

        separatorView.isHidden = true
        separatorView.backgroundColor = .separator

        [
            fieldTableView,
            separatorView,
            resultTableView,
        ].forEach {
            $0.translatesAutoresizingMaskIntoConstraints = false
            view.addSubview($0)
        }

        NSLayoutConstraint.activate([
            fieldTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            fieldTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            fieldTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            fieldTableViewHeightConstraint,

            separatorView.leftAnchor.constraint(equalTo: view.leftAnchor),
            separatorView.rightAnchor.constraint(equalTo: view.rightAnchor),
            separatorView.bottomAnchor.constraint(equalTo: fieldTableView.bottomAnchor),
            separatorView.heightAnchor.constraint(equalToConstant: 1),

            resultTableView.topAnchor.constraint(equalTo: fieldTableView.bottomAnchor),
            resultTableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            resultTableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            resultTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])

        var ignoreFirst = true
        headerHeightObservation = fieldTableView.observe(\.contentSize) { [unowned self] tableView, _ in
            if ignoreFirst {
                ignoreFirst.toggle()
                return
            }
            self.fieldTableViewHeightConstraint.constant = tableView.contentSize.height
            self.view.layoutIfNeeded()
        }
        observeAndOffsetFromKeyboard()
    }

    // MARK: - Actions

    func selectAccount(_ account: AccountInstance?) {
        guard account?.id != fromAccount?.id else {
            return
        }
        self.fromAccount = account

        selectToUser(nil)
        if account == nil || fromAccount == nil {
            fieldTableView.reloadRows(at: [IndexPath(item: 0, section: 0)], with: .automatic)
        } else {
            fromCell?.configure(with: fromAccount)
        }
        subjectCell?.isRequired = isSubjectRequired

        if fromAccount != nil {
            toCell?.textField.becomeFirstResponder()
        }
    }

    func selectToThread(_ thread: ThreadStore) {
        guard
            let account = try? thread.account?.accountInstance(),
            let toUser = thread.displayParticipant
        else {
            return
        }

        self.fromAccount = account
        fromCell?.configure(with: account)

        selectToUser(toUser)
    }

    func selectToUser(_ user: Schema.User?) {
        toUser = user
        toCell?.configure(with: toUser)

        if let user = toUser, let existingThread = searchThreads(with: user) {
            loadViewController(with: existingThread)
        } else {
            resetThreadViewController()
        }

        detachedInputController.isSendEnabled = fromAccount.isNotNil && toUser.isNotNil
    }

    func createNewThread() {
        guard let toUserId = toUser?.id else {
            presentSendWithoutToUserAlert()
            return
        }
        guard let fromAccountStore = fromAccount?.currentAccountStore else {
            presentSendWithoutFromAccountStoreAlert()
            return
        }

        Task { @MainActor in
            do {
                let thread = try await fromAccountStore.createThread(users: [toUserId], title: nil)
                self.detachedInputController.thread = thread
                self.detachedInputController.sendMessage()
                self.navigate(to: thread)
            } catch {
                AppState.shared.handleError(error)
            }
        }
    }

    func displayResults(threads: [ThreadStore], users: [Schema.User]) {
        searchResults = threads.map { .thread($0) } + users.map { .user($0) }
        resultTableView.reloadSections(IndexSet(integer: 0), with: .automatic)
    }

    func navigate(to thread: ThreadStore) {
        guard let mainViewController = navigationController?.presentingViewController?.splitViewController as? MainViewController else {
            print("FAILED TO FIND MAIN")
            return
        }
        print("FOUND MAIN")
        mainViewController.inboxViewController.showDetailViewController(
            ThreadViewController(viewModel: .init(thread: thread)),
            sender: mainViewController.inboxViewController
        )
        Task { @MainActor in
            try await AppCache.cache(MessageDraft(), key: ThreadViewController.InputController.draftKey(for: nil))
        }
    }

    // MARK: - Search

    private var searchTask: DispatchWorkItem?

    func search(with query: String?, isDebounced: Bool = false) async {
        isSearching = !query.isNilOrEmpty
        separatorView.isHidden = !isSearching
        resultTableView.isHidden = !isSearching

        guard let query = query, !query.isEmpty else {
            searchTask?.cancel()
            displayResults(threads: [], users: [])
            return
        }

        guard isDebounced else {
            searchTask?.cancel()
            searchTask = DispatchWorkItem { [weak self] in
                Task { @MainActor in
                    await self?.search(with: query, isDebounced: true)
                }
            }
            DispatchQueue.main.asyncAfter(
                deadline: .now() + 0.2,
                execute: searchTask!
            )
            return
        }

        let threads = searchThreads(with: query)
        guard let account = fromAccount, let store = account.currentAccountStore else {
            displayResults(threads: threads, users: [])
            return
        }

        searchLoading = true
        toCell?.loadingIndicator.isHidden.toggle()

        do {
            let users = try await store.searchUsers(query: query)

            let threadRecipients = threads.map(\.displayParticipant)
            let filteredUsers = users.filter { !threadRecipients.contains($0) }

            self.displayResults(threads: threads, users: filteredUsers)

            self.searchLoading = false
            self.toCell?.loadingIndicator.isHidden.toggle()
        } catch {
            AppState.shared.handleError(error)
        }
    }

    func searchThreads(with searchQuery: String) -> [ThreadStore] {
        accounts.displayThreads.filter { thread in
            if thread.type != .single {
                return false
            }

            if thread.timestamp == nil {
                return false
            }

            if !searchQuery.isEmpty && thread.displayName()?.localizedStandardContains(searchQuery) == false {
                return false
            }

            if let account = fromAccount {
                return account.id == (try? thread.account?.accountInstance().id)
            }

            return true
        }
    }

    func searchThreads(with user: Schema.User) -> ThreadStore? {
        guard
            let account = fromAccount,
            let store = account.currentAccountStore,
            let toUser = toUser,
            let thread = store.existingThreadWith(users: [toUser.id])
        else {
            return nil
        }

        return thread
    }

    // MARK: - Keyboard avoiding

    lazy var keyboardGuide = UILayoutGuide()

    lazy var keyboardGuideHeightAnchor = keyboardGuide.heightAnchor.constraint(
        equalToConstant: 0
    )

    private func observeAndOffsetFromKeyboard() {
        view.addLayoutGuide(keyboardGuide)

        NSLayoutConstraint.activate([
            keyboardGuide.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            keyboardGuide.leftAnchor.constraint(equalTo: view.leftAnchor),
            keyboardGuide.rightAnchor.constraint(equalTo: view.rightAnchor),
            keyboardGuideHeightAnchor
        ])

        keyboardNotificationToken = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillChangeFrameNotification,
            object: nil, queue: .main
        ) { notification in
            guard
                let targetFrame = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect,
                let animationCurve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as? NSNumber,
                let animationDuration = notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber
            else {
                return
            }

            self.keyboardGuideHeightAnchor.constant = UIScreen.main.bounds.intersection(targetFrame).height
            self.resultTableView.contentInset = .init(
                top: 0, left: 0,
                bottom: self.keyboardGuideHeightAnchor.constant + 50,
                right: 0
            )

            UIView.animate(
                withDuration: TimeInterval(truncating: animationDuration),
                delay: 0.0,
                options: .init(rawValue: UInt(truncating: animationCurve)),
                animations: {
                    self.view.layoutIfNeeded()
                }
            )
        }
    }
}
