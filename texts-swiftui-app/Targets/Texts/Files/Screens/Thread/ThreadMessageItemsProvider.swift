//
// Copyright (c) Texts HQ
//

import Combine
import Foundation
import SwiftUIX
import Merge
import Algorithms
import TextsCore

/// ThreadMessageItemsProvider provides an array of messages that is displayed in the Thread View.
/// It's responsible for chunking messages into Sections based on theirs timestamps.
/// TODO: As for now, we're iterating over all Messages from ThreadStore every time we recompute,
/// and it can be slow if the thread is very long. To optimize, we could avoid processing portions
/// of the list that aren't relevant.
public class ThreadMessageItemsProvider: ObservableObject {
    @Published public private(set) var messageListItems: [ListSection<Date, MessageListItem>] = []

    let thread: ThreadStore

    private var cancellables = Cancellables()

    init(thread: ThreadStore) {
        self.thread = thread

        self.recomputeMessageListItems()

        self.thread
            .objectWillChange
            .debounce(for: 0.1, scheduler: DispatchQueue.main)
            .sink(in: cancellables) { [weak self] _ in
                guard let self = self else {
                    return
                }

                self.recomputeMessageListItems()
            }
    }

    private func recomputeMessageListItems() {
        print("[ℹ️] Recompute messages list.")
        guard let ai = try? self.thread.account?.accountInstance(), let user = ai.user else {
            return
        }

        // this set is a hacky way to ensure that we don't have duplicate
        // message IDs. We should really not be receiving duplicates anyway
        // but removing them this way is better than the app crashing.
        var messageIDs: Set<Schema.MessageID> = []
        var items: [ListSection<Date, MessageListItem>] = thread.messages.items
            .lazy
            .join(thread.pendingMessages)
            .filter { $0.isHidden != true }
            .chunked(on: {
                // We use the start of the day instead of using a real timestamp. This
                // ensures that if older items are loaded for the same day, the identifier
                // remains stable.
                Calendar.current.startOfDay(for: $0.timestamp)
            })
            .compactMap { date, timeGroupChunk -> ListSection<Date, MessageListItem>? in
                // we're performing random access, so this should be more efficient
                let timeGroupChunk = Array(timeGroupChunk)
                let sectionItems = timeGroupChunk
                    .lazy
                    .indexed()
                    .compactMap { idx, message -> MessageListItem? in
                        guard messageIDs.insert(message.id).inserted else {
                            return nil
                        }

                        let prevIdx = idx.advanced(by: -1)
                        let nextIdx = idx.advanced(by: 1)
                        let previous = prevIdx >= timeGroupChunk.startIndex ? timeGroupChunk[prevIdx] : nil
                        let next = nextIdx < timeGroupChunk.endIndex ? timeGroupChunk[nextIdx] : nil
                        return .message(.init(
                            id: self.thread.pendingToRealMap[message.id] ?? message.id,
                            message: message,
                            participants: self.thread.participants.items,
                            userID: message.senderID ?? user.id,
                            previousMessage: previous,
                            nextMessage: next
                        ))
                    }
                return ListSection(date, items: Array(sectionItems))
            }

        if thread.shouldShowActivityIndicator {
            if items.isEmpty {
                items.append(ListSection(
                    Calendar.current.startOfDay(for: Date()),
                    items: [.activityIndicator]
                ))
            } else {
                let last = items[items.lastIndex]
                items[items.lastIndex] = ListSection(
                    last.model,
                    items: Array(last.items).appending(.activityIndicator)
                )
            }
        }

        messageListItems = items
    }
}
