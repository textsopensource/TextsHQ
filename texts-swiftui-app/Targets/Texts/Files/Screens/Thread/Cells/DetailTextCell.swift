//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class DetailTextCell: UICollectionViewCell {
        private lazy var label = UILabel()

        override init(frame: CGRect) {
            super.init(frame: frame)

            label.numberOfLines = 0
            label.font = .preferredFont(forTextStyle: .footnote)
            label.textColor = .secondaryLabel

            label.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(label)
            NSLayoutConstraint.activate([
                label.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -8),
                label.topAnchor.constraint(equalTo: contentView.topAnchor),
                label.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 8)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with detail: DetailText, isSender: Bool) {
            label.textAlignment = isSender ? .right : .left
            label.text = detail.text
        }

        class SizeCalculator: MessageSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                guard case .custom(let model) = message.kind, let detail = model as? DetailText else {
                    return .zero
                }

                let width = messageContainerMaxWidth(for: message)
                let height = (detail.text as NSString).boundingRect(
                    with: CGSize(width: width - 16, height: .greatestFiniteMagnitude),
                    options: [.usesFontLeading, .usesLineFragmentOrigin],
                    attributes: [.font: UIFont.preferredFont(forTextStyle: .footnote)],
                    context: nil
                ).height

                return CGSize(width: width, height: height)
            }
        }
    }
}
