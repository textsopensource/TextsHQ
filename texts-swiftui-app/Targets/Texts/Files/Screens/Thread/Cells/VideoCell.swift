//
//  VideoCell.swift
//  Texts
//
//  Created by Oscar Apeland on 12/06/2022.
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class VideoCell: ImageCell {

        override init(frame: CGRect) {
            super.init(frame: frame)

            let playButton = MessageKit.PlayButtonView()
            playButton.isUserInteractionEnabled = false

            playButton.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(playButton)
            NSLayoutConstraint.activate([
                playButton.widthAnchor.constraint(equalToConstant: 35),
                playButton.heightAnchor.constraint(equalToConstant: 35),
                playButton.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                playButton.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with video: Video) {
            guard let url = video.url else {
                return
            }

            imageView.image = nil
            imageView.addLoadingShimmer()

            Task {
                if let preview = try? await TextsAttachmentManager.shared.videoPreview(url: url) {
                    imageView.image = preview
                } else {
                    imageView.backgroundColor = .darkGray
                }
                imageView.removeLoadingShimmer()
            }
        }

        // MARK: - MessageKit

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                let size: CGSize
                if case .custom(let model) = message.kind, let video = model as? Video {
                    size = video.size
                } else {
                    size = CGSize(width: 4, height: 3)
                }

                return CGSize(width: width, height: (width * (size.height / size.width)).rounded(.down))
            }
        }

    }
}
