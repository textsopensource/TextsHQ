//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class SharedIGPostCell: UICollectionViewCell {
        private lazy var avatarView = UIImageView()
        private lazy var usernameLabel = UILabel()
        private lazy var imageView = UIImageView()
        private lazy var captionLabel = UILabel()
        private lazy var albumIndicator = UIImageView()

        lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(openPost))

        private var postUrl: URL?

        func configure(with message: SharedIGPost, isSender: Bool) {
            postUrl = message.url

            usernameLabel.text = message.username
            captionLabel.text = message.caption
            avatarView.sd_setImage(with: message.avatarUrl)
            imageView.sd_setImage(with: message.image)
            albumIndicator.isHidden = !message.isAlbum

            contentView.backgroundColor = isSender ? .secondarySystemBackground : .systemBackground
            contentView.layer.borderWidth = isSender ? 0 : 1.5
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.addGestureRecognizer(tapGesture)

            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true
            contentView.layer.borderColor = UIColor.systemFill.cgColor

            avatarView.backgroundColor = .systemGray6
            avatarView.layer.masksToBounds = true
            avatarView.layer.cornerRadius = Avatar.Size.tweetAuthor.rawValue.height / 2

            usernameLabel.font = .systemFont(ofSize: 15, weight: .semibold)
            usernameLabel.allowsDefaultTighteningForTruncation = true

            imageView.contentMode = .scaleAspectFill
            imageView.clipsToBounds = true
            imageView.image = UIImage(
                systemName: "square.on.square",
                withConfiguration: UIImage.SymbolConfiguration(pointSize: 18, weight: .bold)
            )

            albumIndicator.tintColor = .white
            albumIndicator.transform = .init(scaleX: -1, y: 1)

            captionLabel.font = .systemFont(ofSize: 17)
            captionLabel.numberOfLines = 2
            captionLabel.lineBreakMode = .byTruncatingTail

            [avatarView, usernameLabel, imageView, albumIndicator, captionLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                avatarView.heightAnchor.constraint(equalToConstant: Avatar.Size.tweetAuthor.rawValue.height),
                avatarView.widthAnchor.constraint(equalTo: avatarView.heightAnchor),
                avatarView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
                avatarView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),

                usernameLabel.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 8),
                usernameLabel.centerYAnchor.constraint(equalTo: avatarView.centerYAnchor),

                imageView.topAnchor.constraint(equalTo: avatarView.bottomAnchor, constant: 8),
                imageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                imageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                imageView.bottomAnchor.constraint(equalTo: captionLabel.topAnchor, constant: -8),

                albumIndicator.topAnchor.constraint(equalTo: imageView.topAnchor, constant: -8),
                albumIndicator.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: -8),

                captionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
                captionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
                captionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        @objc private func openPost() {
            guard let postUrl = postUrl else {
                return
            }

            UIApplication.shared.open(postUrl)
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let share = model as? SharedIGPost else {
                    return .zero
                }

                let height = CGFloat(8)
                    + Avatar.Size.tweetAuthor.rawValue.height
                    + 8
                    + width * (share.imageSize.height / share.imageSize.width)
                    + (share.caption.isEmpty ? 0 : 16 + (share.caption as NSString)
                        .boundingRect(
                            with: CGSize(width: width - 32, height: .greatestFiniteMagnitude),
                            options: [.usesFontLeading, .usesLineFragmentOrigin],
                            attributes: [.font: UIFont.systemFont(ofSize: 17)],
                            context: nil
                        ).height)

                return CGSize(width: width, height: height)
            }
        }
    }
}
