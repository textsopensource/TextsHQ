//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class TweetCell: UICollectionViewCell {
        private lazy var usernameLabel = UILabel()
        private lazy var tweetLabel = UILabel()
        private lazy var avatarView = UIImageView()
        private var imageViews: [UIImageView] {
            attachmentContainer?
                .subviews
                .compactMap { $0 as? UIImageView }
                ?? []
        }
        private weak var playButton: UIView?
        private var attachmentContainer: UIView?

        lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(openTweet))

        private var tweetUrl: URL?

        func configure(with message: Tweet) {
            if contentView.subviews.isEmpty {
                setupContentView()
            }

            if let urlString = message.tweet.url, let url = URL(string: urlString) {
                tweetUrl = url
            } else {
                tweetUrl = URL(string: "https://twitter.com/\(message.tweet.user.username)/status/\(message.tweet.id)")!
            }

            usernameLabel.text = message.tweet.user.name
            tweetLabel.text = message.tweet.displayText.string

            if let url = URL(string: message.tweet.user.imgURL) {
                avatarView.sd_setImage(with: url)
            } else {
                avatarView.image = UIImage(named: "twitter-icon-blue")
            }

            message.tweet.attachments?.enumerated().forEach { offset, attachment in
                guard let urlString = attachment.srcURL, let url = URL(string: urlString) else {
                    return
                }
                if url.absoluteString.contains(".mp4") {
                    Task {
                        if let preview = try? await TextsAttachmentManager.shared.videoPreview(url: url) {
                            imageViews[offset].image = preview
                        } else {
                            imageViews[offset].backgroundColor = .darkGray
                        }
                    }

                    if playButton.isNotNil { return }

                    let play = PlayButtonView()
                    play.translatesAutoresizingMaskIntoConstraints = false
                    contentView.addSubview(play)
                    NSLayoutConstraint.activate([
                        play.widthAnchor.constraint(equalToConstant: 35),
                        play.heightAnchor.constraint(equalToConstant: 35),
                        play.centerXAnchor.constraint(equalTo: imageViews[offset].centerXAnchor),
                        play.centerYAnchor.constraint(equalTo: imageViews[offset].centerYAnchor),
                    ])
                    playButton = play
                } else {
                    playButton?.removeFromSuperview()

                    imageViews[offset].sd_setImage(with: url)
                }
            }
        }

        private func setupContentView() {
            contentView.addGestureRecognizer(tapGesture)

            /// Use `reuseIdentifier` with a modifier for attachment count so we can reuse cells
            /// configured with the same image layout. Otherwise we'd have to reset everything.
            /// The default value should be unreachable.
            let numberOfImages = reuseIdentifier?
                .components(separatedBy: "-")
                .last
                .flatMap(Int.init)
                ?? 0

            contentView.backgroundColor = UIColor(named: "Colors/TweetBackground")
            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            tweetLabel.font = .systemFont(ofSize: 17)
            tweetLabel.numberOfLines = 0
            tweetLabel.setContentCompressionResistancePriority(.required, for: .vertical)

            avatarView.layer.masksToBounds = true
            avatarView.layer.cornerRadius = Avatar.Size.tweetAuthor.rawValue.height / 2

            usernameLabel.font = .systemFont(ofSize: 15, weight: .semibold)
            usernameLabel.allowsDefaultTighteningForTruncation = true

            let platformLabel = UILabel()
            platformLabel.text = "twitter.com"
            platformLabel.font = .systemFont(ofSize: 12)
            platformLabel.textColor = .secondaryLabel

            [usernameLabel, platformLabel, tweetLabel, avatarView].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                avatarView.heightAnchor.constraint(equalToConstant: Avatar.Size.tweetAuthor.rawValue.height),
                avatarView.widthAnchor.constraint(equalTo: avatarView.heightAnchor),
                avatarView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
                avatarView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12),

                usernameLabel.leadingAnchor.constraint(equalTo: avatarView.trailingAnchor, constant: 8),
                usernameLabel.bottomAnchor.constraint(equalTo: avatarView.centerYAnchor),
                usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),

                platformLabel.topAnchor.constraint(equalTo: avatarView.centerYAnchor),
                platformLabel.leadingAnchor.constraint(equalTo: usernameLabel.leadingAnchor),

                tweetLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
                tweetLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
                tweetLabel.bottomAnchor.constraint(equalTo: avatarView.topAnchor, constant: -12)
            ])

            if numberOfImages > 0 {
                attachmentContainer = MosaicView(
                    count: numberOfImages,
                    frame: CGRect(x: 0, y: 0, width: contentView.frame.width, height: contentView.frame.width)
                )

                attachmentContainer!.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview(attachmentContainer!)

                NSLayoutConstraint.activate([
                    attachmentContainer!.topAnchor.constraint(equalTo: contentView.topAnchor),
                    attachmentContainer!.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                    attachmentContainer!.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                    attachmentContainer!.bottomAnchor.constraint(equalTo: tweetLabel.topAnchor, constant: -12)
                ])
            }
        }

        @objc private func openTweet() {
            guard let tweetUrl = tweetUrl else {
                return
            }

            UIApplication.shared.open(tweetUrl)
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let tweet = model as? Tweet else {
                    return .zero
                }

                let attachmentHeight: CGFloat = {
                    guard let attachments = tweet.tweet.attachments, !attachments.isEmpty else {
                        return 0
                    }

                    return attachments.count == 1
                        ? width * (attachments[0].size!.height / attachments[0].size!.width)
                        : width
                }()

                let textHeight = tweet.tweet.displayText.string.isEmpty ? 0 : NSString(string: tweet.tweet.displayText.string).boundingRect(
                    with: CGSize(width: width - 24, height: .greatestFiniteMagnitude),
                    options: [.usesFontLeading, .usesLineFragmentOrigin, .usesDeviceMetrics],
                    attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.label],
                    context: nil
                ).height + 12

                let height = CGFloat.zero
                    + attachmentHeight
                    + textHeight
                    + 12 + Avatar.Size.tweetAuthor.rawValue.height + 12

                return CGSize(width: width, height: height)
            }
        }
    }

    class MosaicView: UIView {

        init(count: Int, frame: CGRect) {
            super.init(frame: frame)

            switch count {
            case 1:
                let main = AttachmentView()

                main.translatesAutoresizingMaskIntoConstraints = false
                self.addSubview(main)

                NSLayoutConstraint.activate([
                    main.topAnchor.constraint(equalTo: self.topAnchor),
                    main.leftAnchor.constraint(equalTo: self.leftAnchor),
                    main.rightAnchor.constraint(equalTo: self.rightAnchor),
                    main.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                ])

            case 2:
                let left = AttachmentView()
                let right = AttachmentView()

                [left, right].forEach {
                    $0.translatesAutoresizingMaskIntoConstraints = false
                    self.addSubview($0)
                }

                NSLayoutConstraint.activate([
                    left.topAnchor.constraint(equalTo: self.topAnchor),
                    left.leftAnchor.constraint(equalTo: self.leftAnchor),
                    left.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -1.5),
                    left.bottomAnchor.constraint(equalTo: self.bottomAnchor),

                    right.topAnchor.constraint(equalTo: self.topAnchor),
                    right.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 1.5),
                    right.rightAnchor.constraint(equalTo: self.rightAnchor),
                    right.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                ])

            case 3:
                let main = AttachmentView()
                let top = AttachmentView()
                let bottom = AttachmentView()

                [main, top, bottom].forEach {
                    $0.translatesAutoresizingMaskIntoConstraints = false
                    self.addSubview($0)
                }

                NSLayoutConstraint.activate([
                    main.topAnchor.constraint(equalTo: self.topAnchor),
                    main.leftAnchor.constraint(equalTo: self.leftAnchor),
                    main.rightAnchor.constraint(equalTo: self.centerXAnchor, constant: -1.5),
                    main.bottomAnchor.constraint(equalTo: self.bottomAnchor),

                    top.topAnchor.constraint(equalTo: self.topAnchor),
                    top.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 1.5),
                    top.rightAnchor.constraint(equalTo: self.rightAnchor),
                    top.bottomAnchor.constraint(equalTo: self.centerYAnchor, constant: -1.5),

                    bottom.topAnchor.constraint(equalTo: self.centerYAnchor, constant: 1.5),
                    bottom.leftAnchor.constraint(equalTo: self.centerXAnchor, constant: 1.5),
                    bottom.rightAnchor.constraint(equalTo: self.rightAnchor),
                    bottom.bottomAnchor.constraint(equalTo: self.bottomAnchor),
                ])

            case 4:
                let main = AttachmentView()
                let top = AttachmentView()
                let middle = AttachmentView()
                let bottom = AttachmentView()

                [main, top, middle, bottom].forEach {
                    $0.translatesAutoresizingMaskIntoConstraints = false
                    self.addSubview($0)
                }

                NSLayoutConstraint.activate([
                    main.topAnchor.constraint(equalTo: topAnchor),
                    main.leftAnchor.constraint(equalTo: leftAnchor),
                    main.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 2/3, constant: -1.5),
                    main.bottomAnchor.constraint(equalTo: bottomAnchor),

                    top.topAnchor.constraint(equalTo: topAnchor),
                    top.rightAnchor.constraint(equalTo: rightAnchor),
                    top.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3, constant: -1.5),
                    top.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/3, constant: -1.5),

                    middle.centerYAnchor.constraint(equalTo: centerYAnchor),
                    middle.rightAnchor.constraint(equalTo: rightAnchor),
                    middle.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3, constant: -1.5),
                    middle.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/3, constant: -1.5),

                    bottom.bottomAnchor.constraint(equalTo: bottomAnchor),
                    bottom.rightAnchor.constraint(equalTo: rightAnchor),
                    bottom.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1/3, constant: -1.5),
                    bottom.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1/3, constant: -1.5),
                ])

            default:
                break
            }
        }

        required init?(coder: NSCoder) {
            nil
        }
    }
}
