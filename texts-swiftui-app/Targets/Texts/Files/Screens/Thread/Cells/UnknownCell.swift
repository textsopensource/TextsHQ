//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class UnknownCell: UICollectionViewCell {

        private var url: URL?

        private lazy var nameLabel = UILabel()
        private lazy var imageView = UIImageView()

        lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(open))

        var onTap: () -> Void = { }

        override init(frame: CGRect) {
            super.init(frame: frame)

            setupContentView()
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with message: Unknown) {
            url = message.sourceUrl
            nameLabel.text = message.name
            imageView.image = message.variant == .file ? UIImage(systemName: "doc.fill") : UIImage(systemName: "xmark.octagon.fill")

            let color = message.variant == .file ? UIColor.label : UIColor.systemRed
            nameLabel.textColor = color
            imageView.tintColor = color
        }

        private func setupContentView() {
            contentView.addGestureRecognizer(tapGesture)
            contentView.backgroundColor = UIColor(.secondarySystemFill)
            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            imageView.tintColor = .label
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.setContentHuggingPriority(.required, for: .horizontal)
            contentView.addSubview(imageView)

            nameLabel.font = .systemFont(ofSize: 15, weight: .semibold)
            nameLabel.allowsDefaultTighteningForTruncation = true
            nameLabel.lineBreakMode = .byTruncatingTail
            nameLabel.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(nameLabel)

            NSLayoutConstraint.activate([
                imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
                imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                nameLabel.leftAnchor.constraint(equalTo: imageView.rightAnchor, constant: 8),
                nameLabel.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
                nameLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor, constant: -32)
            ])
        }

        @objc private func open() {
            if let url = url {
                UIApplication.shared.open(url)
            } else {
                onTap()
            }
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                return CGSize(width: width, height: 50)
            }
        }
    }
}
