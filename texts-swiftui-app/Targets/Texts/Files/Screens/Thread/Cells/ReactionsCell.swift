//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class ReactionsCell: UICollectionViewCell {

        private lazy var stackView = UIStackView(frame: bounds)
        private lazy var button = UIButton()

        var menuProvider: () -> UIMenu = {
            fatalError("ReactionsCell UIMenu must be provided")
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            stackView.axis = .horizontal
            stackView.spacing = 2

            button.showsMenuAsPrimaryAction = true
            button.menu = UIMenu(
                children: [
                    UIDeferredMenuElement { [unowned self] completion in
                        completion([self.menuProvider()])
                    }
                ]
            )

            [
                stackView,
                button
            ].forEach {
                contentView.addSubview($0)
                $0.translatesAutoresizingMaskIntoConstraints = false
            }

            NSLayoutConstraint.activate([
                stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
                stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                button.topAnchor.constraint(equalTo: stackView.topAnchor),
                button.leftAnchor.constraint(equalTo: stackView.leftAnchor),
                button.rightAnchor.constraint(equalTo: stackView.rightAnchor),
                button.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Configuration

        private lazy var leftAlignedConstraint = stackView.leftAnchor.constraint(
            equalTo: contentView.leftAnchor,
            constant: Globals.Views.messageSmallCornerRadius
        )

        private lazy var rightAlignedConstraint = stackView.rightAnchor.constraint(
            equalTo: contentView.rightAnchor,
            constant: -Globals.Views.messageSmallCornerRadius
        )

        func configure(with message: Reactions, isSender: Bool) {
            leftAlignedConstraint.isActive = !isSender
            rightAlignedConstraint.isActive = isSender

            stackView.arrangedSubviews.forEach {
                $0.removeFromSuperview()
            }

            message.countedEmojies
                .map(ReactionView.init)
                .forEach(stackView.addArrangedSubview)
        }

        // MARK: - Size Calculator

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                return CGSize(width: width, height: 32)
            }
        }

        // MARK: - Collection view

        private class ReactionView: UIView {

            init(emoji: String, count: Int) {
                super.init(frame: .zero)

                backgroundColor = .systemFill
                layer.masksToBounds = true
                layer.borderWidth = 2
                layer.borderColor = UIColor.systemFill.cgColor

                let emojiLabel = UILabel()
                emojiLabel.text = emoji
                emojiLabel.textAlignment = .right
                emojiLabel.font = .preferredFont(forTextStyle: .callout)
                emojiLabel.translatesAutoresizingMaskIntoConstraints = false
                emojiLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
                addSubview(emojiLabel)

                if count <= 1 {
                    NSLayoutConstraint.activate([
                        emojiLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 6),
                        emojiLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -6),
                        emojiLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
                    ])
                } else {
                    let countLabel = UILabel()
                    countLabel.text = String(count)
                    countLabel.textColor = .secondaryLabel
                    countLabel.font = .preferredFont(forTextStyle: .callout)
                    countLabel.translatesAutoresizingMaskIntoConstraints = false
                    countLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
                    addSubview(countLabel)

                    NSLayoutConstraint.activate([
                        emojiLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 6),
                        emojiLabel.rightAnchor.constraint(equalTo: centerXAnchor, constant: -2),
                        emojiLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
                        countLabel.leftAnchor.constraint(equalTo: centerXAnchor, constant: 2),
                        countLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -6),
                        countLabel.centerYAnchor.constraint(equalTo: centerYAnchor)
                    ])
                }
            }

            required init?(coder: NSCoder) {
                nil
            }

            override func layoutSubviews() {
                super.layoutSubviews()
                layer.cornerRadius = frame.height / 2
            }
        }
    }
}
