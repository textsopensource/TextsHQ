//
// Copyright (c) Texts HQ
//

import UIKit
import WebKit
import TextsCore
import MessageKit

extension ThreadViewController {

    class GifCell: UICollectionViewCell {

        lazy var webView = WKWebView(frame: bounds)

        var onTap: (GifCell) -> Void = { _ in }

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            webView.backgroundColor = .clear
            webView.isOpaque = false
            webView.isUserInteractionEnabled = false

            webView.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(webView)
            NSLayoutConstraint.activate([
                webView.topAnchor.constraint(equalTo: contentView.topAnchor),
                webView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                webView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                webView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])

            contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
        }

        required init?(coder: NSCoder) {
            nil
        }

        @objc func tapped() {
            onTap(self)
        }

        // MARK: - Loading

        func configure(with url: URL) {
            webView.loadMessageAttachment(url: url)
        }

        // MARK: - MessageKit

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let video = model as? Video else {
                    return .zero
                }

                return CGSize(width: width, height: (width * (video.size.height / video.size.width)).rounded(.down))
            }
        }
    }
}
