//
// Copyright (c) Texts HQ
//

import UIKit
import TextsCore
import MessageKit

extension ThreadViewController {

    class ImageCell: UICollectionViewCell {

        lazy var imageView = UIImageView(frame: bounds)

        var onTap: (ImageCell) -> Void = { _ in }

        lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))

        // MARK: - Init

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            imageView.backgroundColor = .clear
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.isUserInteractionEnabled = false

            contentView.addSubview(imageView)
            NSLayoutConstraint.activate([
                imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
                imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])

            contentView.addGestureRecognizer(tapGesture)
        }

        required init?(coder: NSCoder) {
            nil
        }

        @objc func tapped() {
            onTap(self)
        }

        // MARK: - Loading

        func configure(with image: Image) {
            if !image.isSticker {
                imageView.addLoadingShimmer()
            }
            if let data = image.data, let image = UIImage(data: data) {
                imageView.image = image
                imageView.removeLoadingShimmer()
            } else if let imageURL = image.url {
                imageView.sd_setImage(with: imageURL) { [weak imageView] image, error, _, _ in
                    imageView?.removeLoadingShimmer()
                }
            } else {
                imageView.backgroundColor = .darkGray
                imageView.removeLoadingShimmer()
            }
        }

        override func prepareForReuse() {
            super.prepareForReuse()

            imageView.removeLoadingShimmer()
        }

        // MARK: - MessageKit

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let image = model as? Image else {
                    return .zero
                }

                return CGSize(width: width, height: (width * (image.size.height / image.size.width)).rounded(.down))
            }
        }
    }
}
