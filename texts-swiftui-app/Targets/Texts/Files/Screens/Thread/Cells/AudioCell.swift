//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore
import AVFoundation
import AVKit

extension ThreadViewController {

    class AudioCell: UICollectionViewCell {

        static var audioCache = NSCache<NSString, AudioManager>()

        private var audio: Audio?
        private var manager: AudioManager? {
            guard let audio = audio else {
                return nil
            }
            return Self.audioCache.object(forKey: audio.url.absoluteString as NSString)
        }

        // MARK: - Outlets

        private lazy var playButton = UIButton()
        private lazy var progressLabel = UILabel()
        private lazy var progressBackground = UIView()
        private lazy var progressForeground = UIView()
        private lazy var progressConstraint = progressForeground.widthAnchor.constraint(
            equalTo: progressBackground.widthAnchor,
            multiplier: 0
        )
        private lazy var tapGesture = UITapGestureRecognizer(target: self, action: #selector(buttonTapped))

        private static let symbolConfig = UIImage.SymbolConfiguration(pointSize: 18, weight: .semibold)
        private static var playImage = UIImage(systemName: "play.fill", withConfiguration: symbolConfig)
        private static var pauseImage = UIImage(systemName: "pause.fill", withConfiguration: symbolConfig)
        private static var replayImage = UIImage(systemName: "gobackward", withConfiguration: symbolConfig)

        // MARK: - Inits

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            contentView.addGestureRecognizer(tapGesture)
            playButton.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)

            progressLabel.font = .preferredFont(forTextStyle: .body)

            progressBackground.backgroundColor = .systemFill
            progressForeground.backgroundColor = .label

            [progressBackground, progressForeground].forEach {
                $0.layer.masksToBounds = true
                $0.layer.cornerCurve = .continuous
                $0.layer.cornerRadius = 2.5/2
            }

            [playButton, progressLabel, progressBackground, progressForeground].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                playButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                playButton.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 12),
                playButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),

                progressLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -12),
                progressLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                progressBackground.leftAnchor.constraint(equalTo: playButton.rightAnchor, constant: 12),
                progressBackground.rightAnchor.constraint(equalTo: progressLabel.leftAnchor, constant: -8),
                progressBackground.heightAnchor.constraint(equalToConstant: 2.5),
                progressBackground.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                progressConstraint,
                progressForeground.leftAnchor.constraint(equalTo: progressBackground.leftAnchor),
                progressForeground.heightAnchor.constraint(equalTo: progressBackground.heightAnchor),
                progressForeground.centerYAnchor.constraint(equalTo: progressBackground.centerYAnchor)
            ])
        }

        override func prepareForReuse() {
            super.prepareForReuse()

            playButton.isSelected = false
            progressLabel.text = " -:- "
        }

        required init?(coder: NSCoder) {
            nil
        }

        // MARK: - Actions

        func configure(with message: Audio, isSender: Bool) {
            audio = message
            if case .none = manager {
                Self.audioCache.setObject(.init(url: message.url), forKey: message.url.absoluteString as NSString)
            }

            manager!.didChangeIsPlaying = { [weak self] isPlaying in
                guard let welf = self, let manager = welf.manager else { return }
                let image = isPlaying
                    ? Self.pauseImage
                    : manager.isAtEnd
                    ? Self.replayImage
                    : Self.playImage
                welf.playButton.setImage(image, for: .normal)
            }
            manager!.didChangeProgress = { [weak self] progress in
                guard let welf = self else { return }

                welf.progressLabel.text = welf.durationString(
                    duration: welf.manager!.duration
                )

                welf.updateProgressBar(
                    progress: progress,
                    animated: progress > welf.progressConstraint.multiplier
                )
            }

            contentView.backgroundColor = isSender
                ? UIColor(.accentColor)
                : .systemGray6

            playButton.tintColor = isSender
                ? .white
                : UIColor(.accentColor)

            progressForeground.backgroundColor = playButton.tintColor
            progressBackground.backgroundColor = progressForeground.backgroundColor!.withAlphaComponent(0.25)

            progressLabel.text = durationString(duration: manager!.duration)
            progressLabel.textColor = playButton.tintColor
            updateProgressBar(progress: manager!.currentProgress, animated: false)

            playButton.setImage(manager!.isPlaying ? Self.pauseImage : Self.playImage, for: .normal)
        }

        @objc private func buttonTapped() {
            guard let manager = manager else {
                return
            }

            switch (manager.isPlaying, manager.isPaused, manager.isAtEnd) {
            case (false, false, _):
                manager.play()
            case (false, true, true):
                manager.play()
            case (false, true, false):
                manager.resume()
            case (true, false, _):
                manager.pause()
            default:
                break
            }
        }

        private func durationString(duration: Double) -> String {
            guard !duration.isNaN else {
                return " -:- "
            }
            let hours = Int(duration / 3600)
            let minutes = Int(duration.truncatingRemainder(dividingBy: 3600) / 60)
            let seconds = Int(duration.truncatingRemainder(dividingBy: 60))
            return hours > 0
                ? String(format: "%d:%02d:%02d", hours, minutes, seconds)
                : String(format: "%02d:%02d", minutes, seconds)
        }

        private func updateProgressBar(progress: Double, animated: Bool) {
            guard !progress.isNaN else {
                return
            }

            progressConstraint.isActive = false
            progressConstraint = progressForeground.widthAnchor.constraint(
                equalTo: progressBackground.widthAnchor,
                multiplier: progress
            )
            progressConstraint.isActive = true

            if animated {
                UIView.animate(withDuration: 0.01, animations: contentView.layoutIfNeeded)
            } else {
                contentView.layoutIfNeeded()
            }
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                return CGSize(width: width, height: 50)
            }
        }
    }

    // MARK: - Audio Manager

    class AudioManager: NSObject {
        let url: URL
        let player: AVPlayer

        private var timeObserverToken: Any?
        private static var playbackEndedToken: Any?

        init(url: URL) {
            self.url = url
            self.player = AVPlayer(url: url)
            super.init()

            if case .none = Self.playbackEndedToken {
                Self.playbackEndedToken = NotificationCenter.default.addObserver(
                    forName: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                    object: nil, queue: .main
                ) { notification in
                    guard
                        let object = notification.object as? AVPlayerItem,
                        let asset = object.asset as? AVURLAsset,
                        let manager = AudioCell.audioCache.object(forKey: asset.url.absoluteString as NSString)
                    else {
                        return
                    }

                    if !manager.isAtEnd {
                        manager.end()
                    }
                }
            }
        }

        func play() {
            pauseOthers()
            if !isAtEnd {
                player.play()
            } else {
                didChangeProgress(0)
                player.seek(to: .zero) { [weak self] finished in
                    self?.player.play()
                    self?.isAtEnd = false
                }
            }

            didChangeIsPlaying(true)

            timeObserverToken = player.addPeriodicTimeObserver(
                forInterval: CMTime(seconds: 0.01, preferredTimescale: CMTimeScale(NSEC_PER_SEC)),
                queue: .main
            ) { [weak self] time in
                guard let welf = self else {
                    return
                }

                let progress = time.seconds / welf.player.currentItem!.duration.seconds
                welf.didChangeProgress(progress)
            }
        }

        func pause() {
            player.pause()
            didChangeIsPlaying(false)
        }

        func resume() {
            pauseOthers()
            player.play()
            didChangeIsPlaying(true)
        }

        func end() {
            isAtEnd = true
            didChangeIsPlaying(false)
            if let token = timeObserverToken {
                player.removeTimeObserver(token)
            }
        }

        private func pauseOthers() {
            (AudioCell.audioCache.value(forKey: "allObjects") as? NSArray)?
                .compactMap { $0 as? AudioManager }
                .filter { $0.url != self.url && $0.isPlaying }
                .forEach { $0.pause() }
        }

        var isAtEnd = false

        var isPlaying: Bool {
            !player.rate.isZero && player.error == nil
        }

        var isPaused: Bool {
            !isPlaying && !player.currentItem!.currentTime().seconds.isZero
        }

        var currentProgress: Double {
            player.currentItem!.currentTime().seconds / duration
        }

        var duration: Double {
            player.currentItem!.duration.seconds
        }

        deinit {
            if let token = timeObserverToken {
                player.removeTimeObserver(token)
            }
        }

        var didChangeIsPlaying: (Bool) -> Void = { _ in }
        var didChangeProgress: (Double) -> Void = { _ in }
    }
}
