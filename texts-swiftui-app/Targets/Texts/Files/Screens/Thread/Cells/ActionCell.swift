//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class ActionCell: UICollectionViewCell {

        private lazy var label = UILabel()

        override init(frame: CGRect) {
            super.init(frame: frame)

            label.font = .boldSystemFont(ofSize: 12)
            label.textColor = .secondaryLabel
            label.textAlignment = .center

            label.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(label)
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with action: Action) {
            label.text = action.title
        }

        class SizeCalculator: CellSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                CGSize(
                    width: self.layout!.collectionView!.frame.width,
                    height: UIFont.boldSystemFont(ofSize: 12).lineHeight
                )
            }
        }
    }
}
