//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class QuoteCell: UICollectionViewCell {
        private lazy var usernameLabel = UILabel()
        private lazy var textLabel = UILabel()

        var onTap: (QuoteCell) -> Void = { _ in }

        override init(frame: CGRect) {
            super.init(frame: frame)

            contentView.backgroundColor = UIColor(.systemBackground)
            contentView.layer.masksToBounds = true
            contentView.layer.cornerRadius = Globals.Views.messageSmallCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.borderColor = UIColor(.accentColor).cgColor
            contentView.layer.borderWidth = 2

            usernameLabel.font = .preferredFont(for: .caption1, weight: .bold)
            usernameLabel.allowsDefaultTighteningForTruncation = true
            usernameLabel.textColor = UIColor(.accentColor)

            textLabel.font = .systemFont(ofSize: 15)
            textLabel.numberOfLines = 0
            textLabel.setContentCompressionResistancePriority(.required, for: .vertical)
            textLabel.textColor = .label

            [usernameLabel, textLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            NSLayoutConstraint.activate([
                usernameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
                usernameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 12),
                usernameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -12),
                textLabel.trailingAnchor.constraint(equalTo: usernameLabel.trailingAnchor),
                textLabel.leadingAnchor.constraint(equalTo: usernameLabel.leadingAnchor),
                textLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8)
            ])

            contentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapped)))
        }

        required init?(coder: NSCoder) {
            nil
        }

        @objc func tapped() {
            onTap(self)
        }

        func configure(with quote: Quote) {
            usernameLabel.text = quote.displayName.isNilOrEmpty ? "Quote" : quote.displayName
            textLabel.text = quote.displayText
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let quote = model as? Quote else {
                    return .zero
                }

                let height = CGFloat(8)
                    + UIFont.systemFont(ofSize: 15, weight: .semibold).lineHeight.rounded(.up)
                    + 2
                    + (quote.displayText as NSString)
                    .boundingRect(
                        with: CGSize(width: width - 24, height: .greatestFiniteMagnitude),
                        options: [.usesFontLeading, .usesLineFragmentOrigin],
                        attributes: [.font: UIFont.systemFont(ofSize: 15)],
                        context: nil
                    ).height
                    + 8

                return CGSize(width: width, height: height)
            }
        }
    }
}
