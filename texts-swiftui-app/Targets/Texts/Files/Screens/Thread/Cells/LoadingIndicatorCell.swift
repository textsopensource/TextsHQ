//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import Macaw
import TextsCore

extension ThreadViewController {
    class LoadingIndicatorCell: UICollectionViewCell {

        lazy var spinner = UIActivityIndicatorView(style: .medium)
        lazy var imageView = UIImageView()

        override init(frame: CGRect) {
            super.init(frame: frame)

            spinner.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(spinner)

            imageView.contentMode = .scaleAspectFit
            imageView.tintColor = .systemGray5
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.image = try? SVGParser.parse(text: Globals.Icons.texts)
                .toNativeImage(
                    size: .init(
                        35 * UIScreen.main.scale,
                        35 * UIScreen.main.scale
                    )
                )
                .withRenderingMode(.alwaysTemplate)

            contentView.addSubview(imageView)

            NSLayoutConstraint.activate([
                imageView.heightAnchor.constraint(equalToConstant: 35),
                imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor),
                imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

                spinner.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
                spinner.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(isHidden: Bool) {
            spinner.isHidden = isHidden
            spinner.startAnimating()
        }

        class SizeCalculator: CellSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                CGSize(
                    width: layout!.collectionView!.frame.width * 0.75,
                    height: 40
                )
            }
        }
    }
}
