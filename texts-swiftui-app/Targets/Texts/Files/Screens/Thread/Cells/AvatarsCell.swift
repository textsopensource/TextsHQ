//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class AvatarsCell: UICollectionViewCell {

        private lazy var stackView = UIStackView(frame: bounds)
        private lazy var button = UIButton()

        var menuProvider: () -> UIMenu = {
            fatalError("AvatarsCell UIMenu must be provided")
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            stackView.axis = .horizontal
            stackView.spacing = 4

            button.showsMenuAsPrimaryAction = true
            button.menu = UIMenu(
                children: [
                    UIDeferredMenuElement { [unowned self] completion in
                        completion([self.menuProvider()])
                    }
                ]
            )

            [
                stackView,
                button
            ].forEach {
                contentView.addSubview($0)
                $0.translatesAutoresizingMaskIntoConstraints = false
            }
            NSLayoutConstraint.activate([
                stackView.topAnchor.constraint(equalTo: contentView.topAnchor),
                stackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),

                button.topAnchor.constraint(equalTo: stackView.topAnchor),
                button.leftAnchor.constraint(equalTo: stackView.leftAnchor),
                button.rightAnchor.constraint(equalTo: stackView.rightAnchor),
                button.bottomAnchor.constraint(equalTo: stackView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        private lazy var leftAlignedConstraint = stackView.leftAnchor.constraint(
            equalTo: contentView.leftAnchor,
            constant: Globals.Views.messageSmallCornerRadius
        )
        private lazy var rightAlignedConstraint = stackView.rightAnchor.constraint(
            equalTo: contentView.rightAnchor,
            constant: -Globals.Views.messageSmallCornerRadius
        )

        func configure(with avatars: Avatars, isSender: Bool) {
            leftAlignedConstraint.isActive = !isSender
            rightAlignedConstraint.isActive = isSender

            stackView.arrangedSubviews.forEach {
                $0.removeFromSuperview()
            }

            avatars.profiles.forEach { profile in
                stackView.addArrangedSubview(
                    ThreadAvatarView(
                        size: .messageSeenIndicator,
                        source: .single(profile)
                    )
                )
            }
        }

        // MARK: - Size Calculator

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                return CGSize(width: width, height: Avatar.Size.messageSeenIndicator.rawValue.height)
            }
        }
    }
}
