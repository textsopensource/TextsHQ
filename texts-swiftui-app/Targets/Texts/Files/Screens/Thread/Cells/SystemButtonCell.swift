//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class SystemButtonCell: UICollectionViewCell {
        let imageView = UIImageView()
        let label = UILabel()
        lazy var button = UIButton(frame: .zero, primaryAction: UIAction { [unowned self] _ in
            onPress()
        })
        var onPress: () -> Void = { }

        override init(frame: CGRect) {
            super.init(frame: frame)

            imageView.tintColor = .secondaryLabel
            label.font = .preferredFont(forTextStyle: .subheadline)
            label.textColor = .secondaryLabel
            button.titleLabel?.font = UIFont(descriptor: UIFont.preferredFont(forTextStyle: .footnote).fontDescriptor.withSymbolicTraits(.traitBold)!, size: 0)
            button.setTitleColor(.accentColor, for: .normal)

            [imageView, label, button].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview($0)
            }

            let guide = UILayoutGuide()
            contentView.addLayoutGuide(guide)

            NSLayoutConstraint.activate([
                guide.topAnchor.constraint(equalTo: contentView.topAnchor),
                guide.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),

                label.topAnchor.constraint(equalTo: guide.topAnchor),
                label.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
                imageView.trailingAnchor.constraint(equalTo: label.leadingAnchor, constant: -6),
                imageView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
                imageView.centerYAnchor.constraint(equalTo: label.centerYAnchor),

                button.topAnchor.constraint(equalTo: label.bottomAnchor),
                button.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with type: SystemButtonType) {
            switch type {
            case .markAsReadInStealth:
                imageView.image = UIImage(systemName: "eye.fill", withConfiguration: UIImage.SymbolConfiguration(font: button.titleLabel!.font))
                label.text = "Unread until you reply"
                button.setTitle("Mark as Read", for: .normal)
            }
        }

        class SizeCalculator: CellSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                CGSize(
                    width: layout!.collectionView!.frame.width * 0.75,
                    height: 40
                )
            }
        }
    }
}
