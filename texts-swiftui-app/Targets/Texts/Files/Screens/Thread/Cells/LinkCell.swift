//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class LinkCell: UICollectionViewCell {
        private lazy var imageView = UIImageView(
            frame: CGRect(
                origin: .zero,
                size: CGSize(
                    width: contentView.bounds.width,
                    height: contentView.bounds.width / 2
                )
            )
        )

        private lazy var titleLabel = UILabel()
        private lazy var summaryLabel = UILabel()
        private lazy var hostLabel = UILabel()
        private lazy var labelStack = UIStackView()
        private var url: URL?

        enum Style: String, CaseIterable {
            case image, plain

            var reuseIdentifier: String {
                "LinkCell-\(rawValue)"
            }
        }

        // MARK: - Inits

        private func setupSubviews() {
            let style = reuseIdentifier?
                .components(separatedBy: "-")
                .last
                .flatMap(Style.init)
                ?? .plain

            let openGesture = UITapGestureRecognizer(target: self, action: #selector(tapped))
            contentView.addGestureRecognizer(openGesture)

            contentView.backgroundColor = .secondarySystemFill
            contentView.layer.cornerRadius = Globals.Views.messageCornerRadius
            contentView.layer.cornerCurve = .continuous
            contentView.layer.masksToBounds = true

            titleLabel.font = .preferredFont(forTextStyle: .headline)
            titleLabel.numberOfLines = 0

            summaryLabel.font = .preferredFont(forTextStyle: .subheadline)
            summaryLabel.textColor = .secondaryLabel
            summaryLabel.numberOfLines = 0

            hostLabel.font = .preferredFont(forTextStyle: .footnote)
            hostLabel.textColor = .tertiaryLabel

            [titleLabel, summaryLabel, hostLabel].forEach {
                $0.translatesAutoresizingMaskIntoConstraints = false
            }

            labelStack.axis = .vertical
            labelStack.alignment = .leading
            labelStack.spacing = 4
            labelStack.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(labelStack)
            [titleLabel, summaryLabel, hostLabel].forEach(labelStack.addArrangedSubview)

            switch style {
            case .plain:
                NSLayoutConstraint.activate([
                    labelStack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16),
                    labelStack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
                    labelStack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
                ])

            case .image:
                imageView.contentMode = .scaleAspectFill
                imageView.clipsToBounds = true
                imageView.translatesAutoresizingMaskIntoConstraints = false
                contentView.addSubview(imageView)

                NSLayoutConstraint.activate([
                    imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
                    imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
                    imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
                    imageView.heightAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.5),

                    labelStack.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
                    labelStack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 16),
                    labelStack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -16),
                ])
            }
        }

        func configure(with link: Link) {
            if contentView.subviews.isEmpty {
                setupSubviews()
            }

            if let imageUrl = link.image {
                imageView.addLoadingShimmer()
                imageView.sd_setImage(with: imageUrl) { [weak self] _, error, _, _ in
                    self?.imageView.removeLoadingShimmer()
                    if error.isNotNil {
                        self?.imageView.backgroundColor = .systemGray
                    }
                }
            }

            titleLabel.text = link.title
            summaryLabel.text = link.summary
            hostLabel.text = link.displayUrl ?? link.url?.host
            labelStack.arrangedSubviews.forEach {
                $0.isHidden = ($0 as? UILabel)?.text?.isEmpty ?? false
            }
            url = link.url
        }

        @objc private func tapped() {
            guard let url = url else {
                return
            }

            UIApplication.shared.open(url)
        }

        class SizeCalculator: MessageSizeCalculator {
            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)
                let width = messageContainerMaxWidth(for: message)

                guard case .custom(let model) = message.kind, let link = model as? Link else {
                    return .zero
                }

                let texts = [
                    (
                        (link.title ?? "") as NSString,
                        UIFont.preferredFont(forTextStyle: .headline)
                    ),
                    (
                        (link.summary ?? "") as NSString,
                        UIFont.preferredFont(forTextStyle: .subheadline)
                    ),
                    (
                        (link.url?.host ?? "") as NSString,
                        UIFont.preferredFont(forTextStyle: .footnote)
                    )
                ]
                let heights = texts.map { string, font -> CGFloat in
                    string.isEmpty ? 0 : string.boundingRect(
                        with: CGSize(width: width - 32, height: .greatestFiniteMagnitude),
                        options: [.usesFontLeading, .usesLineFragmentOrigin],
                        attributes: [.font: font],
                        context: nil
                    ).height
                }.filter {
                    !$0.isZero
                }

                let imageHeight = link.image != nil ? width / 2 : .zero
                let textsHeight = heights.reduce(CGFloat.zero, +)
                let textsSpacing = CGFloat(heights.count - 1) * 4.0
                let textsPadding = CGFloat(32.0)

                return CGSize(width: width, height: imageHeight + textsHeight + textsSpacing + textsPadding)
            }
        }
    }
}
