//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class TimestampCell: UICollectionViewCell {
        private lazy var label = UILabel(frame: contentView.bounds)

        private static var formatter: DateFormatter = {
            $0.dateStyle = .medium
            $0.timeStyle = .none
            return $0
        }(DateFormatter())

        override init(frame: CGRect) {
            super.init(frame: frame)

            label.font = .systemFont(ofSize: 12)
            label.textColor = .secondaryLabel
            label.textAlignment = .center

            label.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(label)
            NSLayoutConstraint.activate([
                label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        func configure(with timestamp: Timestamp) {
            label.text = Self.formatter.string(from: timestamp.date)
        }

        class SizeCalculator: CellSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                CGSize(
                    width: self.layout!.collectionView!.frame.width,
                    height: UIFont.systemFont(ofSize: 12).lineHeight
                )
            }
        }
    }
}
