//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit
import TextsCore

extension ThreadViewController {
    class SendErrorCell: UICollectionViewCell {

        static let title = "Message not sent"

        private lazy var button = UIButton()

        var menuProvider: () -> UIMenu = {
            fatalError("SendErrorCell UIMenu must be provided")
        }

        override init(frame: CGRect) {
            super.init(frame: frame)

            button.setTitle(SendErrorCell.title, for: .normal)
            button.setTitleColor(.red, for: .normal)
            button.titleLabel?.font = .preferredFont(forTextStyle: .footnote)
            button.tintColor = .red

            button.showsMenuAsPrimaryAction = true
            button.menu = UIMenu(
                children: [
                    UIDeferredMenuElement { [unowned self] completion in
                        completion([self.menuProvider()])
                    }
                ]
            )

            button.translatesAutoresizingMaskIntoConstraints = false
            contentView.addSubview(button)
            NSLayoutConstraint.activate([
                button.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
                button.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            ])
        }

        required init?(coder: NSCoder) {
            nil
        }

        class SizeCalculator: MessageSizeCalculator {
            init(layout: MessagesCollectionViewFlowLayout) {
                super.init()
                self.layout = layout
            }

            override func sizeForItem(at indexPath: IndexPath) -> CGSize {
                guard
                    let layout = layout as? MessagesCollectionViewFlowLayout,
                    let collectionView = layout.collectionView as? MessagesCollectionView,
                    let dataSource = collectionView.dataSource as? ThreadViewController
                else {
                    return .zero
                }

                let message = dataSource.messageForItem(at: indexPath, in: collectionView)

                return CGSize(
                    width: messageContainerMaxWidth(for: message),
                    height: UIFont.preferredFont(forTextStyle: .footnote).lineHeight
                )
            }
        }
    }
}
