//
// Copyright (c) Texts HQ
//

import UIKit
import MessageKit

extension MessageCollectionViewCell {
    override open func delete(_ sender: Any?) {
        guard let collectionView = superview as? UICollectionView, let indexPath = collectionView.indexPath(for: self) else {
            return
        }

        (collectionView.delegate as? ThreadViewController)?.collectionView(collectionView, performAction: NSSelectorFromString("delete:"), forItemAt: indexPath, withSender: sender)
    }

    @objc func quote(_ sender: Any?) {
        guard let collectionView = superview as? UICollectionView, let indexPath = collectionView.indexPath(for: self) else {
            return
        }

        (collectionView.delegate as? ThreadViewController)?.quoteMessage(at: indexPath)
    }

    @objc func react(_ sender: Any?) {
        guard let collectionView = superview as? UICollectionView, let indexPath = collectionView.indexPath(for: self) else {
            return
        }

        (collectionView.delegate as? ThreadViewController)?.reactMessage(at: indexPath, sourceView: self)
    }
}
