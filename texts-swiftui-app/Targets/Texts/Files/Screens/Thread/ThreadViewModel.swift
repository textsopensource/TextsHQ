//
// Copyright (c) Texts HQ
//

import FoundationX
import TextsCore
import UIKit
import Merge

class ThreadViewModel: ObservableObject {
    @Published private var messageSeenParticipants: [Schema.MessageID: [Schema.UserID]] = [:]

    private(set) var isLoadingMessages = false {
        didSet {
            onLoadingChanged(isLoadingMessages)
        }
    }

    var onLoadingChanged: (Bool) -> Void = { _ in }

    let thread: ThreadStore

    @PublishedObject var itemsProvider: ThreadMessageItemsProvider

    var canLoadMessages: Bool {
        thread.messages.hasMore
    }

    private var cancellables = Cancellables()

    init(thread: ThreadStore) {
        self.thread = thread
        self.itemsProvider = ThreadMessageItemsProvider(thread: thread)

        if thread.messages.count <= 10 {
            loadMoreIfNeeded()
        }

        self.thread
            .$messages
            .debounce(for: .milliseconds(200), scheduler: DispatchQueue.main)
            .receiveOnMainQueue()
            .sink(in: cancellables) { [weak self] _ in
                self?.computeParticipantsLastSeen()
            }
    }

    var lastMessageDate: Date? {
        thread.messages.last?.timestamp
    }

    func loadMoreIfNeeded() {
        guard !isLoadingMessages, canLoadMessages else {
            return
        }

        isLoadingMessages = true

        Task { @MainActor in
            self.isLoadingMessages = true

            do {
                try await thread.getMessages(before: thread.messages.first)

                try await Task.sleep(.milliseconds(400))
            } catch {
                AppState.shared.handleError(error)
            }

            self.isLoadingMessages = false
        }
    }

    func triggerInboxUpdate() {
        thread.account?.instance?.accountsStore.updateDisplayThreads()
    }

    func markAsReadIfNeeded(lastMessage: Schema.Message? = nil) {
        if !Preferences.shared.stealthMode && thread.isUnread {
            Task { @MainActor in
                try await thread.markAsRead()

                triggerInboxUpdate()
            }
        }
    }

    func seenByParticipantIDs(for messageID: Schema.MessageID) -> [Schema.UserID] {
        messageSeenParticipants[messageID] ?? []
    }

    func seenByParticipants(for messageID: Schema.MessageID) -> [Schema.User] {
        seenByParticipantIDs(for: messageID).compactMap { thread.getParticipant(id: $0) }
    }

    private func computeParticipantsLastSeen() {
        let dict: [Schema.UserID: Schema.MessageID] = thread.messages.reduce(into: [:]) { dict, message in
            thread.seenByOtherUsers(for: message).forEach {
                dict[$0] = message.id
            }
        }
        messageSeenParticipants = Dictionary(grouping: dict, by: \.value).mapValues { $0.map(\.key) }
    }
}
