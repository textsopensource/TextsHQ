import Foundation
import Combine
import Swallow
import Merge
import TextsNotifications

final class PASBridgeRemote: PASBridge, GlobalPushRegistrar {
    // TODO: change
    private static let host = URLComponents(string: "http://example.com:1234/abc")!

    let platforms: [Schema.PlatformInfo]
    private let allPushes: PassthroughSubject<PASPush, Error>
    private let webSocket: URLSessionWebSocketTask

    struct APIError: Decodable, Error, LocalizedError, CustomStringConvertible {
        let errorName: String
        let errorMessage: String
        let errorStack: String?

        var description: String {
            errorStack ?? "\(errorName): \(errorMessage)"
        }

        var errorDescription: String? {
            description
        }
    }

    private enum APIResponse<T: Decodable>: Decodable {
        case result(T)
        case failure(APIError)

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: AnyCodingKey.self)
            if let value = try? container.decode(T.self, forKey: "result") {
                self = .result(value)
            } else {
                let container = try decoder.singleValueContainer()
                self = .failure(try container.decode(APIError.self))
            }
        }

        var value: Result<T, APIError> {
            switch self {
            case .result(let value):
                return .success(value)
            case .failure(let error):
                return .failure(error)
            }
        }
    }

    private struct StartRequest: Encodable {}

    private struct Request: Encodable {
        typealias ID = SchemaID<Request, String>

        struct Payload: Encodable {
            let platformName: String
            let accountID: Schema.AccountID
            let methodName: String
            let args: Data
        }

        let reqID: ID
        let routeName: String
        let routeData: Payload
    }

    @MutexProtectedValue<[Request.ID: CheckedContinuation<Data, Error>], OSUnfairLock>
    private var continuations = [:]

    private enum WSMessage: Decodable {
        private enum Kind: String, Decodable {
            case callback
            case response
        }

        case callback(Data)
        case response(Result<Data, APIError>, requestID: Request.ID)

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: AnyCodingKey.self)
            let type = try container.decode(Kind.self, forKey: "type")
            switch type {
            case .callback:
                let data = try container.decode(Data.self, forKey: "data")
                self = .callback(data)
            case .response:
                let data = try container.decode(APIResponse<Data>.self, forKey: "data")
                let requestID = try container.decode(Request.ID.self, forKey: "reqID")
                self = .response(data.value, requestID: requestID)
            }
        }
    }

    init(options: PASOptions) async throws {
        let decoder = PlatformServer.makeDecoder()

        allPushes = .init()

        let startURL = Self.host.url!.appendingPathComponent("startSession")
        var request = URLRequest(url: startURL)
        request.httpMethod = "POST"
        request.httpBody = try PlatformServer.makeEncoder().encode(StartRequest())
        let data = try await URLSession.shared.dataTaskPublisher(for: request).value.data
        platforms = try Array(decoder.decode(APIResponse<[String: Schema.PlatformInfo]>.self, from: data).value.get().values)

        var wsComponents = Self.host
        wsComponents.scheme = "ws"
        let wsURL = wsComponents.url!.appendingPathComponent("msgpack")
        webSocket = URLSession.shared.webSocketTask(with: wsURL)
        webSocket.resume()

        Task {
            while true {
                guard case .data(let data) = try await webSocket.receive() else { break }
                do {
                    let message = try decoder.decode(WSMessage.self, from: data)
                    switch message {
                    case let .callback(data):
                        let base = try decoder.decode(Schema.PushEventBase.self, from: data)
                        allPushes.send((base, data))
                    case let .response(result, reqID):
                        guard let cont = $continuations.mutate({ $0.removeValue(forKey: reqID) }) else {
                            throw CustomStringError(description: "Received response for unknown request \(reqID)!")
                        }
                        cont.resume(with: result)
                    }
                } catch {
                    AppState.shared.handleError(error)

                    continue
                }
            }
        }
    }

    var globalPushRegistrar: GlobalPushRegistrar? { self }

    private func performRegistrationRequest(_ endpoint: String, with subscription: PushSubscription) async throws {
        let url = Self.host.url!.appendingPathComponent(endpoint)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = try PlatformServer.makeEncoder().encode(subscription)
        _ = try await URLSession.shared.dataTaskPublisher(for: request).value
    }

    func registerForPushNotifications(with subscription: PushSubscription) async throws {
        try await performRegistrationRequest("register", with: subscription)
    }

    func unregisterForPushNotifications(with subscription: PushSubscription) async throws {
        try await performRegistrationRequest("unregister", with: subscription)
    }

    func call(
        _ method: String,
        on account: AccountInstance,
        with args: Data
    ) async throws -> Data {
        let id = Request.ID(rawValue: UUID().uuidString)

        let request = Request(
            reqID: id,
            routeName: "makeRequest",
            routeData: .init(
                platformName: account.platform.name,
                accountID: account.id,
                methodName: method,
                args: args
            )
        )
        let body = try PlatformServer.makeEncoder().encode(request)

        // save the response handler before sending the request,
        // since doing it after is racy
        async let response = withCheckedThrowingContinuation { c in $continuations.mutate { $0[id] = c } }
        do {
            try await webSocket.send(.data(body))
        } catch {
            // we have to resume the continuation per Swift rules, so we do that
            // with the error we just got
            $continuations.mutate({ $0.removeValue(forKey: id) })?
                .resume(throwing: error)
        }

        return try await response
    }

    var pushes: AsyncThrowingStream<PASPush, Error> {
        allPushes.values
    }
}
