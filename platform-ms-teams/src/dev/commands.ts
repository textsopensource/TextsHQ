import { Message } from '@textshq/platform-sdk'
import type TeamsMainController from '../apis'
import { GrabbedTokens } from '../types'

/** Created for development purposes. Disabled when `IS_DEV = false` */
export abstract class MessageLikeCommand {
  abstract prefix: string

  isMyCommand(content: string) {
    return content.startsWith(this.prefix)
  }

  abstract run(content: string): Promise<Message>
}

/** Lists all available commands. */
export class HelpCommand extends MessageLikeCommand {
  prefix = 'HELP'

  constructor(private readonly controller: TeamsMainController) {
    super()
  }

  async run(content: string): Promise<Message> {
    return {
      id: Math.random().toString(),
      timestamp: new Date(),
      isAction: true,
      senderID: 'none',
      isSender: false,
      text: 'Available commands: ' + this.controller.commands.map(cmd => cmd.prefix).join(', '),
    }
  }
}

/** Creates private.env.json */
export class DevEnvCommand extends MessageLikeCommand {
  prefix = 'DE'

  constructor(private readonly session: GrabbedTokens) {
    super()
  }

  async run(content: string): Promise<Message> {
    const jsonOutput = JSON.stringify({
      dev: {
        skypeToken: this.session.skypeToken,
        teamsToken: this.session.teamsToken,
        spacesToken: this.session.spacesToken,
        // onedriveToken: this.session.onedriveToken,
      },
    })
    return {
      id: Math.random().toString(),
      timestamp: new Date(),
      isAction: true,
      senderID: 'none',
      isSender: false,
      text: jsonOutput,
      textAttributes: {
        entities: [{ from: 0, to: jsonOutput.length, code: true }],
      },
    }
  }
}

/** Shows tokens */
export class TokensCommand extends MessageLikeCommand {
  prefix = 'TOKENS'

  constructor(private readonly session: GrabbedTokens) {
    super()
  }

  async run(content: string): Promise<Message> {
    const jsonOutput = JSON.stringify(this.session, undefined, 2)
    return {
      id: Math.random().toString(),
      timestamp: new Date(),
      isAction: true,
      senderID: 'none',
      isSender: false,
      text: jsonOutput,
      textAttributes: {
        entities: [{ from: 0, to: jsonOutput.length, code: true }],
      },
    }
  }
}
