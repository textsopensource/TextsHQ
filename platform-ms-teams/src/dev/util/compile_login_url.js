/** This script generates PKCE (RFC 7636), put code_challenge into info.ts:42 and code_verify into api.ts:111 */
// For more details check: https://www.authlete.com/developers/pkce/

const crypto = require('crypto')
const { writeFileSync } = require('fs')

const oldUrl = new URL('https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize?x-app-name=Teams&x-client-Ver=1.1.18&client-request-id=96E2AC4B-56F4-434D-9007-7BBED6E44308&instance_aware=true&response_type=code&code_challenge_method=S256&coa=1&x-app-ver=3.13.1&piamsadisambig=true&prompt=login&redirect_uri=x-msauth-ms-st%3A%2F%2Fcom.microsoft.skype.teams&x-client-CPU=64&state=RDRBNzg1NDItMTI5NC00MjNFLUE4OEYtODlCQjg3NjFBMjEz&return-client-request-id=true&X-AnchorMailbox=UPN%3Akrystek.postek%40outlook.com&scope=service%3A%3Aapi.fl.teams.microsoft.com%3A%3AMBI_SSL%20openid%20profile%20offline_access&haschrome=1&claims=%7B%22access_token%22%3A%7B%22xms_cc%22%3A%7B%22values%22%3A%5B%22CP1%22%2C%22protapp%22%5D%7D%7D%7D&x-client-SKU=MSAL.iOS&client_id=8ec6bc83-69c8-4392-8f08-b3c986009232&msafed=1&x-client-OS=15.0&client_info=1&nopa=2&x-client-DM=iPhone&code_challenge=dtcvci4wnDL_mP0jmkg7P3e4XUodNoibaPVp8iL3V7U')
const loginUrl = new URL('https://login.microsoftonline.com/common/oauth2/v2.0/authorize?response_type=id_token&scope=openid%20profile&client_id=5e3ce6c0-2b1f-4285-8d4b-75ee78787346&redirect_uri=https%3A%2F%2Fteams.microsoft.com%2Fgo&state=eyJpZCI6IjUyZjBkNDE0LTdhZTctNDNjOC1iY2Y5LWIwMTBiNzAwZmRmNyIsInRzIjoxNjMxODE5ODYxLCJtZXRob2QiOiJyZWRpcmVjdEludGVyYWN0aW9uIn0%3D&nonce=dbd464e1-0e62-4d8d-94c8-75d336f302bc&client_info=1&x-client-SKU=MSAL.JS&x-client-Ver=1.3.4&client-request-id=1f78d78f-372d-40b1-89f6-c5ce7ccd9a51&response_mode=fragment')

const codeSecret = crypto.randomBytes(22).toString('hex')
const state = { id: crypto.randomUUID().toString(), ts: new Date().getTime(), method: 'redirectInteraction' }

const newUrl = new URL('https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize')
newUrl.searchParams.set('redirect_uri', 'x-msauth-ms-st://com.microsoft.skype.teams')

newUrl.searchParams.set('response_type', 'code')
newUrl.searchParams.set('code_challenge_method', 'S256')
newUrl.searchParams.set('code_challenge', Buffer.from(crypto.createHash('sha256').update(codeSecret).digest()).toString('base64url'))
newUrl.searchParams.set('client_id', '8ec6bc83-69c8-4392-8f08-b3c986009232')
newUrl.searchParams.set('scope', 'service::api.fl.teams.microsoft.com::MBI_SSL openid profile offline_access')
newUrl.searchParams.set('prompt', 'login')
newUrl.searchParams.set('state', Buffer.from(crypto.randomUUID().toString()).toString('base64url'))
// newUrl.searchParams.set('nonce', crypto.randomUUID().toString())

writeFileSync('fixtures/code_verify.txt', codeSecret)

console.log(
  // Object.fromEntries(newUrl.searchParams.entries()),
  // Object.fromEntries(oldUrl.searchParams.entries()),
  newUrl.toString(),
  codeSecret,
)
