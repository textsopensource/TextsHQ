import { Attribute, MessageDeletionMode, PlatformInfo } from '@textshq/platform-sdk'

const info: PlatformInfo = {
  attributes: new Set([
    Attribute.CAN_MESSAGE_EMAIL,
    Attribute.CAN_MESSAGE_USERNAME,
    Attribute.SUPPORTS_EDIT_MESSAGE,
    Attribute.SUPPORTS_PRESENCE,
    Attribute.SUPPORTS_LIVE_TYPING,
    Attribute.SUPPORTS_QUOTED_MESSAGES,
  ]),
  deletionMode: MessageDeletionMode.UNSEND,
  icon: `
  <svg width="100%" height="100%" viewBox="0 0 4 4" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" xmlns:serif="http://www.serif.com/" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linejoin:round;stroke-miterlimit:2;"><path d="M3.84,1.2c0,-0.662 -0.538,-1.2 -1.2,-1.2l-1.44,0c-0.662,0 -1.2,0.538 -1.2,1.2l0,1.44c0,0.662 0.538,1.2 1.2,1.2l1.44,0c0.662,0 1.2,-0.538 1.2,-1.2l0,-1.44Z" style="fill:#3b3b3b;"/><clipPath id="_clip1"><rect x="0.461" y="0.461" width="2.918" height="2.918"/></clipPath><g clip-path="url(#_clip1)"><path d="M2.496,1.555l0.754,0c0.071,0 0.129,0.062 0.129,0.139l-0,0.738c-0,0.281 -0.212,0.509 -0.474,0.509l-0.002,0c-0.262,0 -0.474,-0.228 -0.474,-0.509l-0,-0.804c-0,-0.04 0.03,-0.073 0.067,-0.073Z" style="fill:#5059c9;fill-rule:nonzero;"/><ellipse cx="3.006" cy="1.081" rx="0.305" ry="0.328" style="fill:#5059c9;"/><ellipse cx="2.056" cy="0.935" rx="0.441" ry="0.474" style="fill:#7b83eb;"/><path d="M2.644,1.555l-1.244,0c-0.071,0.002 -0.126,0.065 -0.125,0.141l0,0.841c-0.009,0.454 0.325,0.831 0.747,0.842c0.422,-0.011 0.756,-0.388 0.746,-0.842l0,-0.841c0.002,-0.076 -0.054,-0.139 -0.124,-0.141Z" style="fill:#7b83eb;fill-rule:nonzero;"/><path d="M2.09,1.555l-0,1.18c-0.001,0.054 -0.031,0.102 -0.078,0.123c-0.015,0.007 -0.03,0.01 -0.047,0.01l-0.63,0c-0.009,-0.024 -0.017,-0.048 -0.024,-0.073c-0.023,-0.083 -0.036,-0.17 -0.036,-0.258l0,-0.842c-0.001,-0.075 0.054,-0.138 0.125,-0.14l0.69,0Z" style="fill-opacity:0.1;fill-rule:nonzero;"/><path d="M2.022,1.555l-0,1.253c-0,0.017 -0.003,0.034 -0.01,0.05c-0.019,0.05 -0.064,0.083 -0.114,0.083l-0.531,0c-0.012,-0.024 -0.022,-0.048 -0.032,-0.073c-0.009,-0.025 -0.017,-0.048 -0.024,-0.073c-0.023,-0.083 -0.036,-0.17 -0.036,-0.258l0,-0.842c-0.001,-0.075 0.054,-0.138 0.125,-0.14l0.622,0Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><path d="M2.022,1.555l-0,1.107c-0.001,0.073 -0.056,0.133 -0.124,0.133l-0.587,0c-0.023,-0.083 -0.036,-0.17 -0.036,-0.258l0,-0.842c-0.001,-0.075 0.054,-0.138 0.125,-0.14l0.622,0Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><path d="M1.954,1.555l-0,1.107c-0.001,0.073 -0.056,0.133 -0.124,0.133l-0.519,0c-0.023,-0.083 -0.036,-0.17 -0.036,-0.258l0,-0.842c-0.001,-0.075 0.054,-0.138 0.125,-0.14l0.554,0Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><path d="M2.09,1.178l-0,0.23c-0.012,0.001 -0.023,0.001 -0.034,0.001c-0.012,0 -0.023,-0 -0.034,-0.001c-0.023,-0.002 -0.046,-0.006 -0.068,-0.012c-0.137,-0.035 -0.251,-0.138 -0.305,-0.278c-0.01,-0.024 -0.017,-0.048 -0.022,-0.073l0.338,-0c0.069,-0 0.124,0.06 0.125,0.133Z" style="fill-opacity:0.1;fill-rule:nonzero;"/><path d="M2.022,1.251l-0,0.157c-0.023,-0.002 -0.046,-0.006 -0.068,-0.012c-0.137,-0.035 -0.251,-0.138 -0.305,-0.278l0.249,-0c0.068,-0 0.124,0.06 0.124,0.133Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><path d="M2.022,1.251l-0,0.157c-0.023,-0.002 -0.046,-0.006 -0.068,-0.012c-0.137,-0.035 -0.251,-0.138 -0.305,-0.278l0.249,-0c0.068,-0 0.124,0.06 0.124,0.133Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><path d="M1.954,1.251l-0,0.145c-0.137,-0.035 -0.251,-0.138 -0.305,-0.278l0.181,-0c0.068,-0 0.124,0.06 0.124,0.133Z" style="fill-opacity:0.2;fill-rule:nonzero;"/><clipPath id="_clip2"><path d="M0.586,1.118l1.244,-0c0.068,-0 0.124,0.059 0.124,0.133l-0,1.338c-0,0.074 -0.056,0.133 -0.124,0.133l-1.244,0c-0.069,0 -0.125,-0.059 -0.125,-0.133l0,-1.338c0,-0.074 0.056,-0.133 0.125,-0.133Z" clip-rule="nonzero"/></clipPath><g clip-path="url(#_clip2)"><use xlink:href="#_Image3" x="0.618" y="1.393" width="1.493px" height="1.605px" transform="matrix(0.746396,0,0,0.802376,0,5.32907e-17)"/></g><path d="M1.535,1.627l-0.249,-0l0,0.728l-0.158,-0l-0,-0.728l-0.248,-0l0,-0.142l0.655,0l-0,0.142Z" style="fill:#fff;fill-rule:nonzero;"/></g><defs><image id="_Image3" width="2px" height="2px" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAG0lEQVQImWMMiTnw//fvfwws3779Zvj54ycDAGVFCzq2j8duAAAAAElFTkSuQmCC"/></defs></svg>
  `,
  name: 'teams',
  displayName: 'Microsoft Teams',
  version: '0.7.0',
  tags: ['Beta'],
  loginMode: 'browser',
  browserLogin: {
    loginURL: 'https://login.microsoftonline.com/consumers/oauth2/v2.0/authorize?redirect_uri=x-msauth-ms-st%3A%2F%2Fcom.microsoft.skype.teams&response_type=code&code_challenge_method=S256&code_challenge=lYJFaVUGOffqS8dugCZnsgclVH64IXQWtkotE9A-q5s&client_id=8ec6bc83-69c8-4392-8f08-b3c986009232&scope=service%3A%3Aapi.fl.teams.microsoft.com%3A%3AMBI_SSL+openid+profile+offline_access&prompt=login&state=ODUwNWNhZjAtZDllYy00ZTQ4LWJjNTUtNjc1MDg4NDYzOTgx',
    closeOnRedirectRegex: 'x-msauth-ms-st:.*',
  },
  attachments: {
    gifMimeType: 'video/mp4',
  },
  prefs: {
    prefer_onedrive_upload: {
      label: 'Prefer OneDrive uploads (default on dektop) instead of Skype imgo (default on mobile)',
      type: 'checkbox',
      default: false,
    },
  },
  typingDurationMs: 500,
}

export default info
