import { FetchResponse } from '@textshq/platform-sdk'
import { TeamsMessage, TeamsResMessageArrived, TeamsResMessages, TeamsResNotificationsStart } from '../interfaces'
import TextsHttpProfileLayer from '../texts-http/profilelayer'
import { TeamsMessageContentType } from '../types'
import UrlBuilder from '../texts-http/urlBuilder'
import { HttpNewMiddleware } from '../texts-http/middleware'
import TeamsSkypeExpAPI from './previews'

class OverridePreviewLink extends HttpNewMiddleware {
  weight = 95

  constructor(private readonly skypeToken: string) {
    super()
  }

  async after(response: FetchResponse<any>): Promise<FetchResponse<any>> {
    if (!response.body.messages) return response

    const previewRegexp = new RegExp('(https://.{0,12}asm.skype.com/v1/objects/.*/views/imgo)')

    for (const message of (response as FetchResponse<TeamsResMessages>).body.messages) {
      if (message.properties?.files) {
        for (const urlFound of message.properties?.files!.match(previewRegexp) ?? []) {
          message.properties.files = message.properties.files.replace(urlFound, await new TeamsSkypeExpAPI(this.skypeToken).previewAsDataUri(urlFound))
        }
      }
    }

    return response
  }
}

export default class TeamsMSGAPI extends TextsHttpProfileLayer {
  constructor(private readonly skypeToken: string) {
    super()
    this.applySkypeToken(this.skypeToken)
    this.http.addMiddlewares([new OverridePreviewLink(this.skypeToken)])
  }

  getMessage(conversationId: string, messageId: string, searchParams: Record<string, string> = {}): Promise<FetchResponse<TeamsMessage>> {
    return this.http.request(
      UrlBuilder.msgapi().conversationMessages(conversationId, messageId).url, { searchParams },
    )
  }

  getMessages(conversationId: string, searchParams: Record<string, string> = {
    pageSize: '40',
    view: 'supportsMessageProperties',
  }): Promise<FetchResponse<TeamsResMessages>> {
    return this.http.request(
      UrlBuilder.msgapi().conversationMessages(conversationId).url, { searchParams },
    )
  }

  sendMessage(
    messageContent: string, conversationId: string, imdisplayname: string,
  ): Promise<FetchResponse<TeamsResMessageArrived>> {
    return this.http.request(
      UrlBuilder.msgapi().conversationMessages(conversationId).url,
      {
        method: 'POST',
        jsonBody: {
          content: messageContent,
          messagetype: 'RichText/Html',
          contenttype: 'text',
          imdisplayname,
        },
      },
    )
  }

  editMessage(
    conversationId: string,
    messageId: string,
    newContent: string,
    messageContentType: TeamsMessageContentType = 'Text',
    imdisplayname: string,
  ) {
    return this.http.request(
      UrlBuilder.msgapi().conversationMessages(conversationId, messageId).url,
      {
        method: 'PUT',
        jsonBody: {
          content: newContent,
          messagetype: messageContentType,
          contenttype: 'text',
          imdisplayname,
        },
      },
    )
  }

  deleteMessage(conversationId: string, messageId: string) {
    return this.http.request(
      UrlBuilder.msgapi().conversationMessages(conversationId, messageId).url,
      {
        method: 'DELETE',
        searchParams: {
          behavior: 'softDelete',
        },
      },
    )
  }

  requestNotificationBroadcast(): Promise<FetchResponse<TeamsResNotificationsStart>> {
    return this.http.request(
      UrlBuilder.msgapi().notificationRequest().url,
      {
        method: 'PUT',
        jsonBody: {
          startingTimeSpan: 0,
          endpointFeatures: 'Agent,Presence2015,MessageProperties,CustomUserProperties,NotificationStream,SupportsSkipRosterFromThreads',
          subscriptions: [{
            channelType: 'HttpLongPoll',
            interestedResources: ['/v1/users/ME/conversations/ALL/properties', '/v1/users/ME/conversations/ALL/messages', '/v1/threads/ALL'],
          }],
        },
      },
    )
  }
}
