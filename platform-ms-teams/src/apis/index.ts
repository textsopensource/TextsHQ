import {
  FetchResponse,
  Message,
  MessageContent,
  MessageSendOptions,
  Paginated,
  PaginationArg,
  Participant,
  texts,
  Thread,
  ThreadFolderName,
} from '@textshq/platform-sdk'
import _ from 'lodash'
import TeamsAPI from './teams'
import PresenceAPI from './presence'
import { TeamsResMessages } from '../interfaces'
import {
  ActivityFormatter,
  AttachmentsFormatter,
  Formatter,
  MentionFormatter,
  RemovedMessagesFormatter,
  ReplyFormatter,
  RichTextFormatter,
} from '../formatters'
import { DevEnvCommand, HelpCommand, MessageLikeCommand, TokensCommand } from '../dev/commands'
import TeamsNotificationAPI from './notifications'
import TeamsMSGAPI from './msg'
import UrlBuilder from '../texts-http/urlBuilder'
import TeamsSkypeExpAPI from './previews'
import { GrabbedTokens } from '../types'

/** Class responsible for linking SDK and Teams' APIs */
export default class TeamsMainController {
  /** user related info, might be helpful while identifying who is author of a message */
  public aboutUser: {
    /** Usually  */
    realName: string, // For example Krystian Postek
    teamsTag: string, // 8:live:.cid:765382937218936178 <- not a real tag
  } = { realName: 'NOT LOGGED!?!', teamsTag: '8:live:textsError' }

  public readonly commands: Array<MessageLikeCommand>

  public apis: {
    messages: TeamsMSGAPI,
    general: TeamsAPI,
    presence: PresenceAPI,
    notifications?: TeamsNotificationAPI,
    weirdSkype: TeamsSkypeExpAPI,
  }

  readonly formatters: Array<Formatter> = []

  constructor(private tokens: GrabbedTokens) {
    this.apis = {
      messages: new TeamsMSGAPI(tokens.skypeToken),
      general: new TeamsAPI(tokens.skypeToken, tokens.teamsToken),
      presence: new PresenceAPI(tokens.skypeToken),
      weirdSkype: new TeamsSkypeExpAPI(tokens.skypeToken),
    }

    this.commands = [
      new HelpCommand(this),
      new DevEnvCommand(this.tokens),
      new TokensCommand(this.tokens),
    ]

    this.formatters.push(
      new ActivityFormatter(this),
      new ReplyFormatter(this),
      new AttachmentsFormatter(this),
      new RichTextFormatter(this),
      new RemovedMessagesFormatter(this),
      new MentionFormatter(this),
    )
  }

  async resolveUserName() {
    const { body } = await this.apis.messages.http.request(UrlBuilder.msgapi().userProps().url)
    const { name } = JSON.parse(body.userDetails)
    return {
      mriTag: body.primaryMemberName,
      name,
    }
  }

  async initNotificationService() {
    const { body } = await this.apis.messages.requestNotificationBroadcast()

    // eslint-disable-next-line prefer-destructuring
    this.aboutUser.teamsTag = body.isActiveUrl.split('/')[4]

    this.apis.notifications = new TeamsNotificationAPI(
      this.tokens.skypeToken,
      body.subscriptions[0].longPollUrl,
      this.formatters,
      this.aboutUser.teamsTag,
    )
    return body
  }

  /**
   * Returns basic thread description
   */
  private async threads(): Promise<Array<Thread>> {
    const { body } = await this.apis.general.fetchMe()

    const threads: Array<Thread> = []

    for (const chat of body.chats) {
      const preloadedMessagesResponse = await this.apis.messages.getMessages(chat.id, { pageSize: '25', view: 'supportsMessageProperties' })
      let preloadedMessages = preloadedMessagesResponse
        .body.messages
        .map(m => Formatter.rebuildMessage(m, this.formatters, this.aboutUser.teamsTag))
      preloadedMessages = _.sortBy(preloadedMessages, 'timestamp')
      const hasMore = !!preloadedMessagesResponse.body._metadata.backwardLink
      if (hasMore) {
        preloadedMessages[0].cursor = preloadedMessagesResponse.body._metadata.backwardLink
      }

      // Fetch all members of a thread
      const members: Participant[] = (await this.apis.general.fetchUsers(chat.members.map(u => u.mri))).body.value
        .map(mem => ({
          id: mem.mri,
          fullName: mem.displayName,
          username: mem.displayName,
          cannotMessage: mem.isBlocked,
          imgURL: mem.imageUri,
        }))

      // Download avatars concurrently
      await Promise.all(members.map(async mem => {
        try {
          mem.imgURL = TeamsAPI.userImageToDataURI((await this.apis.general.fetchUserImage(mem.fullName!, mem.imgURL!, mem.id)).body)
        } catch (e) {
          mem.imgURL = ''
        }
      }))

      threads.push({
        id: chat.id,
        title: chat.title,
        type: 'group',
        messages: {
          items: preloadedMessages,
          hasMore,
        },
        isUnread: false,
        isReadOnly: false,
        participants: {
          items: members,
          hasMore: false,
        },
      })
    }

    return threads
  }

  // TODO: missing pagination
  async getThreads(folderName: ThreadFolderName, pagination?: PaginationArg): Promise<Paginated<Thread>> {
    return {
      items: await this.threads(),
      hasMore: false,
    }
  }

  async getMessages(threadID: string, pagination?: PaginationArg): Promise<Paginated<Message>> {
    const { body } = pagination ? await this.apis.messages.http.request(pagination.cursor) as FetchResponse<TeamsResMessages> : await this.apis.messages.getMessages(threadID)

    let allMessages: Array<Message> = body.messages.map(m => Formatter.rebuildMessage(m, this.formatters, this.aboutUser.teamsTag))
    allMessages = _.sortBy(allMessages, 'timestamp')

    if (body._metadata.backwardLink) {
      allMessages[0].cursor = body._metadata.backwardLink
      return {
        items: allMessages,
        hasMore: true,
      }
    }

    return {
      items: allMessages,
      hasMore: false,
    }
  }

  private invokesCommand(content: string) {
    for (const command of this.commands) {
      if (command.isMyCommand(content)) return command
    }
    return undefined
  }

  async sendTextMessage(target: string, content: MessageContent, options?: MessageSendOptions): Promise<Array<Message>> {
    if (this.invokesCommand(content.text ?? '') && texts.IS_DEV) return [await this.invokesCommand(content.text!)!.run(content.text!)]

    const encodedContent = await Formatter.encodeMessage({ content, options }, this.formatters, target, this.aboutUser.teamsTag)

    await this.apis.messages.sendMessage(encodedContent.text ?? 'quack error', target, this.aboutUser.realName)

    return [] // wait for response from notification api
  }

  // TODO: make it more elegant, has some issues
  async editMessage(threadID: string, messageID: string, content: MessageContent, options?: MessageSendOptions): Promise<Array<Message>> {
    const message: Message = {
      id: messageID,
      isSender: true,
      senderID: this.aboutUser.teamsTag,
      text: content.text ?? '',
      timestamp: new Date(),
      threadID,
    }
    await this.apis.messages.editMessage(threadID, messageID, message.text ?? '', 'Text', this.aboutUser.teamsTag)
    return [message]
  }

  async createThread(userIDs: string[], title?: string): Promise<Thread> {
    const { body } = await this.apis.general.createChatGroup(userIDs)

    if (userIDs.length === 1) title = 'Only you'

    if (title) await this.apis.general.updateThreadTopic(body.value.threadId, title)

    const participants: Participant[] = (await this.apis.general.fetchUsers(userIDs)).body.value
      .map(mem => ({
        id: mem.mri,
        fullName: mem.displayName,
        username: mem.displayName,
        cannotMessage: mem.isBlocked,
        imgURL: mem.imageUri,
      }))

    // Download avatars concurrently
    await Promise.all(participants.map(async mem => {
      try {
        mem.imgURL = TeamsAPI.userImageToDataURI((await this.apis.general.fetchUserImage(mem.fullName!, mem.imgURL!, mem.id)).body)
      } catch (e) {
        mem.imgURL = ''
      }
    }))

    return {
      id: body.value.threadId,
      isReadOnly: false,
      type: 'group',
      messages: { items: [], hasMore: false },
      participants: { items: participants, hasMore: false },
      isUnread: false,
      title,
    }
  }
}
