import { FetchResponse, OnServerEventCallback, ServerEventType, texts } from '@textshq/platform-sdk'
import { TeamsMessage, TeamsResNotificationEvent } from '../interfaces'
import { Formatter } from '../formatters'
import TextsHttpProfileLayer from '../texts-http/profilelayer'

/**
 * This API uses long poll method for fetching latest data from server.
 * To begin notification polling you need an url from msgapi, call MsgAPIClient.requestNotificationBroadcast method to get one.
 * */
export default class TeamsNotificationAPI extends TextsHttpProfileLayer {
  /** Represents status of polling, setting it on false, will stop polling. */
  polling = false

  eventCallback?: OnServerEventCallback

  constructor(private readonly skypeToken: string, private nextPoll: string, private readonly formatters: Array<Formatter>, private readonly sendersID: string) {
    super()
    this.applySkypeToken(skypeToken)
  }

  private async poll() {
    const { body } = await this.http.request(this.nextPoll) as FetchResponse<TeamsResNotificationEvent>
    texts.log('TEAMS: NEU: ', JSON.stringify(body, undefined, 2))
    for (const event of body.eventMessages ?? []) {
      switch (event.resourceType) {
        case 'NewMessage':
          this.handleMessageEvent(event.resource as TeamsMessage)
          break
        case 'MessageUpdate':
          this.handleMessageEvent(event.resource as TeamsMessage)
          break
      }
    }
    this.nextPoll = body.next
  }

  async startPolling() {
    this.polling = true
    while (this.polling) {
      await this.poll()
    }
  }

  /** Handles all events marked as `NewMessage` */
  private handleMessageEvent(teamsMessage: TeamsMessage) {
    const entries = [
      Formatter.rebuildMessage(teamsMessage, this.formatters, this.sendersID),
    ]

    this.eventCallback?.([{
      type: ServerEventType.STATE_SYNC,
      objectIDs: {
        messageID: teamsMessage.id,
        threadID: teamsMessage.to,
      },
      objectName: 'message',
      mutationType: 'upsert',
      entries,
    }])
  }
}
