import { FetchResponse, texts } from '@textshq/platform-sdk'
import { readFile } from 'fs/promises'
import TextsHttpProfileLayer from '../texts-http/profilelayer'
import { TokenDisposition } from '../interfaces'

export class TeamsAuth extends TextsHttpProfileLayer {
  async acquireTokens(tokens: { name: string, scope: string }[], credentials: { code?: string, refresh?: string }, skipFirst = false) {
    const resultMap = new Map()

    let refreshToken = credentials.refresh

    if (credentials.code && !skipFirst) {
      // eslint-disable-next-line prefer-const
      const response = await this.acquireToken('first', tokens[0].scope, credentials)
      refreshToken = response.refreshToken
      resultMap.set(tokens[0].name, response.accessToken)
    }

    for (const { name, scope } of skipFirst ? tokens.slice(1) : tokens) {
      const response = await this.acquireToken('n-th', scope, { refresh: refreshToken! })
      texts.log(response)
      refreshToken = response.refreshToken
      texts.log('refresh is', refreshToken)
      resultMap.set(name, response.accessToken)
    }

    texts.log('b')

    return {
      lastRefresh: refreshToken,
      tokens: Object.fromEntries(resultMap.entries()),
    }
  }

  async acquireToken(
    iteration: 'first' | 'n-th', scope: string, credentials: { code?: string, refresh?: string },
  ): Promise<{ accessToken: string, refreshToken: string, response: FetchResponse<TokenDisposition>, disposition: TokenDisposition }> {
    let response: FetchResponse<TokenDisposition>
    switch (iteration) {
      case 'first':
        response = await this.firstAcquire(scope, credentials.code!)
        break
      case 'n-th':
        response = await this.nthAcquire(scope, credentials.refresh!)
        break
    }

    return {
      accessToken: response.body.access_token,
      refreshToken: response.body.refresh_token,
      response,
      disposition: response.body,
    }
  }

  async firstAcquire(scope: string, code: string): Promise<FetchResponse<TokenDisposition>> {
    const codeVerify = '80c714d2da9d1acd52350b030bf09634d03899bc5436' // (await readFile('fixtures/code_verify.txt')).toString().trim()

    return this.http.request('https://login.microsoftonline.com/consumers/oauth2/v2.0/token', {
      method: 'POST',
      form: {
        claims: '{"access_token":{"xms_cc":{"values":["CP1","protapp"]}}}',
        client_info: 1,
        scope,
        code,
        grant_type: 'authorization_code',
        code_verifier: codeVerify,
        redirect_uri: 'x-msauth-ms-st://com.microsoft.skype.teams',
        client_id: '8ec6bc83-69c8-4392-8f08-b3c986009232',
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
  }

  nthAcquire(scope: string, refresh: string): Promise<FetchResponse<TokenDisposition>> {
    return this.http.request('https://login.microsoftonline.com/9188040d-6c67-4c5b-b112-36a304b66dad/oauth2/v2.0/token', {
      method: 'POST',
      form: {
        claims: '{"access_token":{"xms_cc":{"values":["CP1","protapp"]}}}',
        client_info: 1,
        scope,
        refresh_token: refresh,
        grant_type: 'refresh_token',
        redirect_uri: 'x-msauth-ms-st://com.microsoft.skype.teams',
        client_id: '8ec6bc83-69c8-4392-8f08-b3c986009232',
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    })
  }

  skypeToken(spacesToken: string): Promise<FetchResponse<{ skypeToken: { skypetoken: string } }>> {
    return this.http.request('https://teams.live.com/api/auth/v1.0/authz/consumer', {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${spacesToken}`,
      },
    })
  }
}
