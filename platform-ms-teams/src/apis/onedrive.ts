import { FetchResponse } from '@textshq/platform-sdk'
import TextsHttpProfileLayer from '../texts-http/profilelayer'
import { OnedriveUploadResponse } from '../interfaces'

export default class OnedriveClient extends TextsHttpProfileLayer {
  constructor(private readonly onedriveToken: string) {
    super()
    this.headers.set('Authorization', `WLID1.1 ${onedriveToken}`)
  }

  uploadFile(filename: string, uploadContent: Buffer, mimeType: string): Promise<FetchResponse<OnedriveUploadResponse>> {
    this.headers.set('Content-Type', mimeType)
    return this.http.request(`https://api.onedrive.com/v1.0/drive/root:/Microsoft%20Teams%20Chat%20Files/${filename}:/content`, {
      method: 'PUT',
      searchParams: {
        '@name.conflictBehavior': 'fail',
      },
      body: uploadContent,
    })
  }
}
