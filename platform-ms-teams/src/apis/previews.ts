import { FetchResponse } from '@textshq/platform-sdk'
import TextsHttpProfileLayer from '../texts-http/profilelayer'

export default class TeamsSkypeExpAPI extends TextsHttpProfileLayer {
  protected applySkypeToken(skypeToken: string) {
    this.headers.set('authorization', `skype_token ${skypeToken}`)
  }

  constructor(private readonly skypeToken: string) {
    super()
    this.applySkypeToken(skypeToken)
  }

  async previewAsDataUri(url: string) {
    const { body, headers } = await this.http.request(url, { preferBuffer: true }) as FetchResponse<Buffer>
    const bodyEncoded = body.toString('base64')
    return `data:${headers['content-type']};base64,${bodyEncoded}`
  }
}
