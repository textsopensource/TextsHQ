import { FetchResponse } from '@textshq/platform-sdk'
import TextsHttpProfileLayer from '../texts-http/profilelayer'
import UrlBuilder from '../texts-http/urlBuilder'
import { TeamsBetaUsersResponse, TeamsResMeStatus } from '../interfaces'

export default class TeamsClient extends TextsHttpProfileLayer {
  public applySkypeToken() {
    this.headers.clear()
    this.headers.set('X-Skypetoken', this.skypeToken)
    this.headers.set('Accept', 'application/json')
  }

  public applyBearerToken(bearerToken:string) {
    this.headers.clear()
    this.headers.set('Authorization', `Bearer ${bearerToken}`)
    this.headers.set('Accept', 'application/json')
  }

  public applyTeamsToken() {
    this.applyBearerToken(this.teamsToken)
  }

  public applyBothTokens() {
    this.headers.clear()
    this.headers.set('Authorization', `Bearer ${this.teamsToken}`)
    this.headers.set('X-Skypetoken', this.skypeToken)
    this.headers.set('Content-Type', 'application/json;charset=UTF-8')
    this.headers.set('Accept', 'application/json, text/plain, */*')
  }

  constructor(private readonly skypeToken: string, private readonly teamsToken: string) {
    super()
  }

  /**
   * Me endpoint has last chats and conversations.
   */
  fetchMe(): Promise<FetchResponse<TeamsResMeStatus>> {
    this.applySkypeToken()
    return this.http.request(UrlBuilder.teams().userSummary().url)
  }

  createChatGroup(idsOfMembers: Array<string>): Promise<FetchResponse<{ value: { threadId: string } }>> {
    this.applyBothTokens()
    return this.http.request(
      'https://teams.live.com/api/mt/beta/groups/create', {
        method: 'POST',
        jsonBody: {
          members: idsOfMembers.map(id => ({ id, role: 'Admin' })),
          properties: {
            threadType: 'chat',
          },
        },
      },
    )
  }

  updateThreadTopic(threadID: string, topic: string) {
    this.applyBothTokens()
    return this.http.request(UrlBuilder.teams().group(threadID).path('properties').url, {
      method: 'PUT',
      jsonBody: { topic },
    })
  }

  fetchUsers(usersMri: Array<string>): Promise<FetchResponse<TeamsBetaUsersResponse>> {
    this.applyBothTokens()
    return this.http.request(UrlBuilder.teams().users().fetch().url, {
      method: 'POST',
      body: JSON.stringify(usersMri),
    })
  }

  fetchUserImage(displayName: string, targetUrl: string, userMri: string): Promise<FetchResponse<{ value: string }>> {
    this.applyTeamsToken()
    return this.http.request(UrlBuilder.teams().users().profilePicture(userMri).url, {
      method: 'GET',
      searchParams: {
        displayname: displayName,
        imageUri: targetUrl,
        size: 'HR96x96',
      },
    })
  }

  static userImageToDataURI(response: { value: string }): string {
    return `data:image/pjpeg;base64,${response.value}`
  }

  deleteChat(threadId: string) {
    this.applyBothTokens()
    return this.http.request(UrlBuilder.teams().group(threadId).path('deleteChat').url, {
      method: 'DELETE',
    })
  }
}
