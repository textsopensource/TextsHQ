import TextsHttpProfileLayer from '../texts-http/profilelayer'

/** Class responsible for sending presence status. */
export default class PresenceClient extends TextsHttpProfileLayer {
  protected applySkypeToken(skypeToken: string) {
    this.headers.set('X-Skypetoken', skypeToken)
  }

  /**
   * Create client
   * @param skypeToken
   */
  constructor(private skypeToken: string) {
    super()
    this.applySkypeToken(skypeToken)
  }
}
