/* eslint class-methods-use-this: 2 */
import {
  AccountInfo,
  ActivityType,
  Awaitable,
  LoginCreds,
  LoginResult,
  Message,
  MessageContent,
  MessageSendOptions,
  OnServerEventCallback,
  Paginated,
  PaginationArg,
  PlatformAPI, texts,
  Thread,
  ThreadFolderName,
  User,
} from '@textshq/platform-sdk'
import TeamsMainController from './apis'
import { GrabbedTokens } from './types'
import { TeamsAuth } from './apis/auth'

export default class Teams implements PlatformAPI {
  protected teamsController?: TeamsMainController

  private tokens?: GrabbedTokens

  private static readonly emptyPagination: Paginated<any> = { items: [], hasMore: false } // only for debug

  createThread = async (userIDs: string[], title: string | undefined): Promise<boolean | Thread> =>
    await this.teamsController?.createThread(userIDs, title) ?? false

  deleteThread = async (threadID: string): Promise<void> => {
    await this.teamsController?.apis.general.deleteChat(threadID)
  }

  dispose = () => undefined

  getCurrentUser = async () => {
    const { realName, teamsTag } = await this.teamsController?.aboutUser ?? { realName: 'Loading...', teamsTag: '8:live:texts' }

    return {
      displayText: realName,
      id: teamsTag,
    }
  }

  getMessages = async (folderName: ThreadFolderName, pagination?: PaginationArg): Promise<Paginated<Message>> =>
    await this.teamsController?.getMessages(folderName, pagination) ?? Teams.emptyPagination

  getThreads = async (threadID: string, pagination?: PaginationArg): Promise<Paginated<Thread>> =>
    await this.teamsController?.getThreads(threadID, pagination) ?? Teams.emptyPagination

  // getThreadParticipants = async (threadID: string, pagination?: PaginationArg): Promise<Paginated<Participant>> =>
  //   await this.teamsController?.getThreadParticipants(threadID, pagination) ?? Teams.emptyPagination

  sendMessage = async (threadID: string, content: MessageContent, options?: MessageSendOptions): Promise<Message[]> =>
    this.teamsController!.sendTextMessage(threadID, content, options)

  editMessage = async (threadID: string, messageID: string, content: MessageContent) =>
    this.teamsController!.editMessage(threadID, messageID, content)

  deleteMessage = async (threadID: string, messageID: string, forEveryone?: boolean) => {
    await this.teamsController!.apis.messages.deleteMessage(threadID, messageID)
    return true
  }

  updateThread = async (threadID: string, updates: Partial<Thread>): Promise<boolean> => {
    this.teamsController?.apis.general.updateThreadTopic(threadID, updates.title!)
    return true
  }

  getUser = async (ids: { userID?: string, username?: string, email?: string, phoneNumber?: string }): Promise<User> => {
    const { body } = await this.teamsController!.apis.general.fetchUsers([ids.userID!])
    const teamsUser = body.value[0]
    return {
      id: teamsUser.mri,
      fullName: teamsUser.displayName,
      cannotMessage: teamsUser.isBlocked,
      // imgURL: `https://www.gravatar.com/avatar/${crypto.createHash('md5').update(teamsUser.mri).digest('hex')}.jpg`,
    }
  }

  searchUsers = (typed: string) => []

  sendActivityIndicator = (type: ActivityType, threadID: string): Awaitable<void> => undefined

  sendReadReceipt = (threadID: string, messageID: string): Awaitable<void | boolean> => undefined

  subscribeToEvents = (onEvent: OnServerEventCallback) => {
    this.teamsController!.apis.notifications!.eventCallback = onEvent
    this.teamsController!.apis.notifications!.startPolling().then()
  }

  init = async (tokens?: GrabbedTokens, accountInfo?: AccountInfo) => {
    if (!tokens) return // If there isn't any session
    this.tokens = tokens
    await this.login() // Continue login
  }

  login = async (credentials?: LoginCreds): Promise<LoginResult> => {
    // Let assume that texts, returned callback URL
    const loginDate = new Date()

    if (credentials) {
      // Called when received credentials from webview
      texts.log(credentials)
      const callbackURL = new URL(credentials.lastURL!)

      // const openIdToken = callbackURL.searchParams.get('client_info')!
      const code = callbackURL.searchParams.get('code')!

      const authClient = new TeamsAuth()

      texts.log('a')
      const { tokens, lastRefresh } = await authClient.acquireTokens([
        { name: 'teamsToken', scope: 'service::api.fl.teams.microsoft.com::MBI_SSL openid profile offline_access' },
        { name: 'spacesToken', scope: 'service::api.fl.spaces.skype.com::MBI_SSL openid profile offline_access' },
      ], { code })
      texts.log('a')

      /*
      const odResponse = await authClient.tokenFromRefresh('service%3A%3Aonedrivemobile.live.com%3A%3AMBI_SSL+openid+profile+offline_access', refreshToken)
      */

      const skypeResponse = await authClient.skypeToken(tokens.spacesToken)

      const skypeToken = skypeResponse.body.skypeToken.skypetoken

      this.tokens = {
        ...tokens,
        refreshToken: lastRefresh,
        skypeToken,
        onedriveToken: 'odToken',
      }

      texts.log('LOGIN TOKENS', this.tokens)
    } else {
      // Called when reusing credentials
      const { refreshToken } = this.tokens!

      const authClient = new TeamsAuth()

      const { tokens, lastRefresh } = await authClient.acquireTokens([
        { name: 'teamsToken', scope: 'service::api.fl.teams.microsoft.com::MBI_SSL openid profile offline_access' },
        { name: 'spacesToken', scope: 'service::api.fl.spaces.skype.com::MBI_SSL openid profile offline_access' },
      ], { refresh: refreshToken }, true)

      const skypeResponse = await authClient.skypeToken(tokens.skypeToken)

      const skypeToken = skypeResponse.body.skypeToken.skypetoken

      this.tokens = {
        ...tokens,
        refreshToken: lastRefresh,
        skypeToken,
        onedriveToken: 'odToken',
      }
    }

    this.teamsController = new TeamsMainController(this.tokens!) // Init API
    await this.teamsController!.initNotificationService() // Init notification service

    // this.teamsAPI.aboutUser.teamsTag = already set in initNotificationService
    this.teamsController.aboutUser.realName = (await this.teamsController.resolveUserName()).name
    return { type: 'success', title: 'done bro' } // Return ok
  }

  serializeSession = () => this.tokens
}
