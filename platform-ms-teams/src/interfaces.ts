import { TeamsContentType, TeamsMessageContentType } from './types'

/*
WARNING! ENORMOUS interfaces after this line. Thank you Teams team for terrible API responses.
Do NOT navigate without IDE with "Find usages" or "Fold lines" features.
*/

// Chapter one - common objects
export interface TeamsMessage {
  /** Describes content's type */
  messagetype: TeamsMessageContentType
  /** Depends on messagetype */
  content: string,
  /** Describes role of a message */
  contenttype?: TeamsContentType,
  clientmessageid: string,
  /** User's first name and last name */
  imdisplayname: string,
  /** {@see TeamsMessage.originalarrivaltime} */
  id: string,
  type?: string | 'Message',
  conversationid: string,
  conversationLink: string,
  // composeTime: string,
  composetime: string, // DO NOT CONFUSE WITH composeTime
  /** Probably it's just an ID, ofc it's also timestamp */
  originalarrivaltime: string,
  containerId?: string,
  parentMessageId?: string,
  /** Author's id */
  from: string,
  /** Target channel */
  to: string,
  sequenceId?: number,
  version: number,
  threadType?: string, // not sure about this one
  isEscalationToNewPerson?: boolean,
  properties?: {
    s2spartnername?: string,
    /** These are just reactions. */
    emotions?: Array<{
      key: string,
      users: Array<{
        mri: string,
        time: number,
        value: string
      }>
    }>,
    annotationsSummary?: {
      emotions?: Record<string, number>
    },
    /** Files are just references to the OneDrive files, they are part of a different platform. */
    files?: string, // DAMN MS CANT PASS JSON PROPERLY QUACK U
    filesSerialized?: Array<TeamsODAttachedFile>,
    /** Just a timestamp */
    edittime?: number,
    deletetime?: number,
    importance?: string,
    subject?: string,
    chatFilesIndexId?: string
    topic?: string,
    createRelatedMessagesIndex?: string,
    creator?: string,
    /** Usually passed with thread events. */
    members?: Array<{
      id: string,
      role: string,
      shareHistoryTime?: number
    }>
  }
}

export interface TeamsODAttachedFile {
  '@type': string,
  version: number,
  id: string,
  baseUrl: string,
  type: string,
  title: string,
  state: string,
  objectUrl: string,
  itemid: string,
  fileName: string,
  fileType: string,
  fileInfo: {
    itemId: string,
    fileUrl: string,
    siteUrl: string,
    shareUrl: string,
    shareId: string,
    serverRelativeUrl: string,
  },
  botFileProperties: Record<any, any>
  filePreview?: {
    previewUrl?: string,
    previewHeight?: number,
    previewWidth?: number,
  },
  fileChicletState: {
    serviceName: string,
    state: string,
  }
}

export interface TeamsChat {
  /** id of chat */
  id: string,
  /** no idea what horizons are */
  consuptionHorizon: {
    originalArrivalTime: number,
    timeStamp: number,
    clientMessageId: string
  },
  /** members of chat */
  members: Array<{
    isMuted: boolean,
    /** special word for just a username, looks like this `8:live:krystek.postek` */
    mri: string,
    /** looks like a last part of mri */
    objectId: string,
    /** role on a chat */
    role: string | 'Admin',
  }>,
  /** title of a chat */
  title: string,
  version: number,
  threadVersion: number,
  isRead: boolean,
  isHighImportance: boolean,
  isOneOnOne: boolean,
  // lastMessage: TeamsMessage, bruh moment it's different
  lastMessage: {
    messageType: string,
    content: string,
    clientMessageId: string,
    imDisplayName: string,
    id: string,
    type: string,
    composeTime: string,
    originalArrivalTime: string,
    /** Doesn't require extracting from url */
    from: string
  }
  isLastMessageFromMe: boolean,
  chatSubType: number,
  lastJoinAt: string,
  createdAt: string,
  creator: string,
  tenantId: string,
  hidden: boolean,
  isGapDetectionEnabled: boolean,
  interopType: number,
  isConversationDeleted: boolean,
  isExternal: boolean,
  consumerGroupId: string,
  isMessagingDisabled: boolean,
  isDisabled: boolean,
  importState: string | 'unknown',
  chatType: string | 'chat',
  interopConversationStatus: string | 'None',
  conversationBlockedAt: number,
  hasTranscript: boolean,
  isSticky: boolean,
  meetingPolicy: string | 'Unknown',
}

/** Used for pagination. */
export interface TeamsMetadata {
  /** If endpoint has more items, this field will have forward url for next page */
  backwardLink?: string,
  syncState: string,
  lastCompleteSegmentStartTime?: number,
  lastCompleteSegmentEndTime?: number,
}

export interface TeamsThreadMember {
  /** looks like 8:live:.cid.64b747188be6a648 */
  id: string,
  type: 'ThreadMember',
  userLink: string,
  role: 'User' | 'Admin',
  capabilities: Array<string | 'kickMember'>,
  /** can be just an empty string */
  linkedMri: string,
  /** string of int */
  cid: string,
  isFollowing: boolean,
  rangeStart: number,
  rangeEnd: number,
  hidden: boolean,
}

// Chapter two - msgapi
export interface TeamsResMessages {
  messages: Array<TeamsMessage>,
  _metadata: TeamsMetadata
}

export interface TeamsResMessageArrived {
  OriginalArrivalTime: number,
}

export interface TeamsResNotificationsStart {
  id: string,
  /** "," separated array */
  endpointFeatures: string,
  subscriptions: Array<{
    channelType: 'HttpLongPoll',
    interestedResources: Array<string>,
    /** URL required to start polling events. */
    longPollUrl: string,
  }>,
  isActiveUrl: string,
  longPollActiveTimeoutSupport: boolean,
}

export interface TeamsThreadActivityProperties {
  initiator: string,
  target?: string,
  value?: string,
}

// Chapter three - teams api

export interface TeamsBetaResponse<T> {
  type: string,
  value: Array<T>
}

export interface TeamsResMeStatus {
  teams: Array<any>, // TODO: RE
  chats: Array<TeamsChat>,
  users: Array<any>, // probably contact book, need to investigate
  privateFeeds: Array<PrivateFeedTeamsResponse>
  metadata: {
    syncToken: string
  }
}

export interface TeamsBetaUser {
  isShortProfile: boolean,
  isBlocked: boolean,
  imageUri: string,
  cid: string,
  objectId: string,
  skypeTeamsInfo: {
    isSkypeTeamsUser?: boolean,
  },
  featureSettings: {
    isPrivateChatEnabled: boolean,
    enableShiftPresence: boolean,
    enableScheduleOwnerPermissions: boolean
  },
  userPrincipalName: string,
  givenName: string,
  surname: string,
  status: number,
  displayName: string,
  type: 'TFLUser',
  mri: string,
}

export type TeamsBetaUsersResponse = TeamsBetaResponse<TeamsBetaUser>

// Chapter four - neu

export interface TeamsResNotificationEvent {
  /** Url of next poll */
  next: string,
  eventMessages?: Array<{
    /** ISO time */
    time: string,
    type: 'EventMessage',
    resourceLink: string,
    resourceType: 'NewMessage' | 'MessageUpdate' | 'CustomUserProperties' | 'ConversationUpdate',
    resource: any
  }>
}

// Chapter five - orphans or idc that much

export interface PrivateFeedTeamsResponse {
  id: string,
  type: string,
  version: number,
  properties: Record<string, any> | { isemptyconversation: boolean },
  lastMessage: any,
  messages: string,
  targetLink: string,
  streamType: string | 'bookmarks',
}

export interface TeamsUserProperties {
  /** Looks like mri */
  primaryMemberName: string,
  /** this one is JSON string, has name property */
  userDetails: string
}

export interface TeamsThreadDetailsResponse {
  id: string,
  type: 'Thread',
  properties: {
    /** yeah, strings are better than booleans */
    isStickyThread: 'true' | 'false',
    createRelatedMessagesIndex: 'true' | 'false',
    isMigrated: 'True' | 'False',
    /** id of author of thread */
    creator: string,
    threadType: 'chat',
    templateType: string | 'immutableroster',
    /** looks like an UUID4 */
    tenantid: string,
    historydisclosed: 'true' | 'false',
    gapDetectionEnabled: 'True' | 'False',
    /** probably string of timestamp of thread creation */
    createdat: string,
    capabalilities: Array<string>
  },
  members: Array<TeamsThreadMember>,
  version: number,
  messages: string,
}

export interface OnedriveUploadResponse {
  '@content.downloadUrl': string,
  createdBy: {
    application: {
      displayName: string,
      id: string,
    }
    user: {
      displayName: string,
      id: string,
    }
  },
  createdDateTime: string,
  cTag: string,
  eTag: string,
  id: string,
  size: number,
  webUrl: string,
}

// Chapter six - login stuff
export interface TokenDisposition {
  token_type: 'Bearer',
  scope: string,
  expires_in: number,
  ext_expires_in: number,
  refresh_in: number,
  access_token: string,
  refresh_token: string,
  foci: '1' | string,
  id_token: string,
  client_info: string,
}
