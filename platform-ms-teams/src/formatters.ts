/* eslint-disable no-param-reassign */
import {
  Awaitable,
  Message,
  MessageActionType,
  MessageAttachment,
  MessageAttachmentType,
  MessageContent,
  MessageSendOptions, TextEntity,
  texts,
} from '@textshq/platform-sdk'
import { parse as parseXML } from 'fast-xml-parser'
import { JSDOM } from 'jsdom'
import { parseInt } from 'lodash'
import { TeamsMessage, TeamsODAttachedFile, TeamsThreadActivityProperties } from './interfaces'
import type TeamsMainController from './apis'
import TeamsMSGAPI from './apis/msg'
import { TeamsThreadActivityType } from './types'

export abstract class Formatter {
  constructor(readonly parent: TeamsMainController) {}

  /**
   * Converts teams message into ✨texts✨ message
   * @param teamsMessage payload received from API
   * @param formatters kinda middleware
   * @param senderID user id to identify who is sender
   */
  static rebuildMessage(teamsMessage: TeamsMessage, formatters: Array<Formatter>, senderID: string): Message {
    const messageAuthorID = teamsMessage.from.split('/').pop()!

    let message: Message = {
      id: teamsMessage.id,
      text: teamsMessage.content,
      timestamp: new Date(teamsMessage.originalarrivaltime),
      senderID: messageAuthorID,
      isSender: messageAuthorID === senderID,
      editedTimestamp: teamsMessage.properties?.edittime ? new Date(teamsMessage.properties!.edittime) : undefined,
    }

    // console.log('isSender', messageAuthorID === senderID, messageAuthorID, senderID)

    for (const formatter of formatters.filter(f => f.formattingCondition(message, teamsMessage))) {
      message = formatter.format(message, teamsMessage)
    }

    if (texts.IS_DEV) message.extra = teamsMessage // debug stuff

    return message
  }

  /**
   * Converts beautiful ✨texts✨ message into teams compatible one
   * @param messageProperties content + options
   * @param formatters kinda middleware
   * @param thread targeted thread
   * @param senderID user id to identify who is sender
   */
  static async encodeMessage(messageProperties: { content: MessageContent, options?: MessageSendOptions }, formatters: Array<Formatter>, thread: string, senderID: string): Promise<MessageContent> {
    let { content } = messageProperties
    const { options } = messageProperties

    for (const formatter of formatters.filter(f => f.encodeCondition(messageProperties))) {
      content = await formatter.encodeFormat({ content, options })
    }

    return content
  }

  /** If condition is true, `format` method is called, otherwise nothing happens. */
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return false
  }

  /**
   * Main logic of `Formatter`. Requires to return message.
   *
   * @param message: "final" message object
   * @param original: the original object of message
   * @returns formatted/edited message
   * */
  format(message: Message, original: TeamsMessage): Message {
    return message
  }

  encodeCondition(messageProperties: { content: MessageContent, options?: MessageSendOptions }): boolean {
    return false
  }

  encodeFormat(messageProperties: { content: MessageContent, options?: MessageSendOptions }): Awaitable<MessageContent> {
    return messageProperties.content
  }
}

/** Formats system info like new user has been added or someone has changed chat title. */
export class ActivityFormatter extends Formatter {
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return original.messagetype.startsWith('ThreadActivity')
  }

  format(message: Message, original: TeamsMessage): Message {
    const activity = parseXML(message.text ?? '')
    const [activityName, activityProps] = Object.entries(activity)[0] as [TeamsThreadActivityType, TeamsThreadActivityProperties]

    message.isSender = false
    message.isAction = true
    message.senderID = '$thread'
    message.parseTemplate = true

    // Match thread actions
    switch (activityName as TeamsThreadActivityType) {
      case 'addmember':
        message.text = `{{${activityProps.target!}}} added by {{${activityProps.initiator}}}`
        message.action = {
          type: MessageActionType.THREAD_PARTICIPANTS_ADDED,
          participantIDs: [activityProps.target!],
          actorParticipantID: activityProps.initiator,
        }
        message.parseTemplate = true
        break
      case 'topicupdate':
        message.text = `{{${activityProps.initiator}}} changed topic to "${activityProps.value!}"`
        message.action = {
          type: MessageActionType.THREAD_TITLE_UPDATED,
          title: activityProps.value!,
          actorParticipantID: activityProps.initiator,
        }
        message.parseTemplate = true
        break
      case 'deletemember':
        message.text = `{{${activityProps.target!}}} removed by {{${activityProps.initiator}}}`
        message.action = {
          type: MessageActionType.THREAD_PARTICIPANTS_REMOVED,
          participantIDs: [activityProps.target!],
          actorParticipantID: activityProps.initiator,
        }
        message.parseTemplate = true
        break
      case 'joiningenabledupdate':
        message.parseTemplate = true
        if (activityProps.value === 'True') {
          message.text = `Joining has been enabled by {{${activityProps.initiator}}}`
        } else if (activityProps.value === 'False') {
          message.text = `Joining has been disabled by {{${activityProps.initiator}}}`
        }
        break
      case 'threadshared':
        message.parseTemplate = true
        message.text = `{{${activityProps.initiator}}} has sent an chat invitation, `
        break
      default:
        message.text = JSON.stringify([activityName, activityProps], undefined, 2)
        break
    }

    return message
  }
}

/** Tries to find quote blocks inside of messages. */
export class ReplyFormatter extends Formatter {
  private readonly messageApi: TeamsMSGAPI

  constructor(readonly parent: TeamsMainController) {
    super(parent)
    this.messageApi = parent.apis.messages
  }

  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return original.messagetype === 'RichText/Html' && original.content.includes('blockquote')
  }

  format(message: Message, original: TeamsMessage): Message {
    // const frag = JSDOM.fragment(original.content)
    const dom = new JSDOM(original.content)
    const { document } = dom.window
    const frag = document.body

    const blockquote = frag.querySelector('blockquote')
    if (!blockquote) return message

    message.linkedMessageID = blockquote!.getAttribute('itemid')!
    blockquote.remove()
    message.text = frag.innerHTML

    return message
  }

  encodeCondition(messageProperties: { content: MessageContent; options?: MessageSendOptions }): boolean {
    return !!messageProperties.options?.quotedMessageID
  }

  async encodeFormat(messageProperties: { content: MessageContent; options?: MessageSendOptions }): Promise<MessageContent> {
    const { content, options } = messageProperties

    const dom = new JSDOM()
    const { document } = dom.window

    if (options?.quotedMessageID) {
      // Fetch reply message
      const replyContext = await this.messageApi.getMessage(options.quotedMessageThreadID!, options.quotedMessageID)
      const replyContextSerialized = Formatter.rebuildMessage(replyContext.body, this.parent.formatters, this.parent.aboutUser.teamsTag)

      // Create parent block
      const blockQuote = document.createElement('blockquote')
      blockQuote.setAttribute('itemscope', '')
      blockQuote.setAttribute('itemtype', 'http://schema.skype.com/Reply')
      blockQuote.setAttribute('itemid', options.quotedMessageID)

      // Create message author
      const author = document.createElement('strong')
      author.setAttribute('itemprop', 'mri')
      author.setAttribute('itemid', replyContext.body.from.split('/').pop() ?? '')
      author.textContent = replyContext.body.imdisplayname
      blockQuote.appendChild(author)

      // Timestamp
      const timestamp = document.createElement('span')
      timestamp.setAttribute('itemprop', 'time')
      timestamp.setAttribute('itemid', options.quotedMessageID)
      blockQuote.appendChild(timestamp)

      // Create message content
      const quoteBody = document.createElement('p')
      quoteBody.setAttribute('itemprop', 'preview')
      quoteBody.textContent = replyContextSerialized.text ?? 'what the quack'
      blockQuote.appendChild(quoteBody)

      document.body.appendChild(blockQuote)

      // Append comment
      const paragraph = document.createElement('p')
      paragraph.textContent = content.text ?? ''
      document.body.appendChild(paragraph)
    }

    content.text = document.body.innerHTML
    return content
  }
}

/** Looks for mentions */
export class MentionFormatter extends Formatter {
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return original.messagetype === 'RichText/Html' && original.content.includes('itemtype="http://schema.skype.com/Mention"')
  }

  format(message: Message, original: TeamsMessage): Message {
    const frag = JSDOM.fragment(original.content)

    const mentions = frag.querySelectorAll('span[itemtype="http://schema.skype.com/Mention"]')

    let startPosition = 0

    for (const mention of mentions) {
      const mentionEntity: TextEntity = { from: 0, to: 0 }

      mentionEntity.from = message.text!.indexOf(mention.textContent!, startPosition)
      mentionEntity.to = mentionEntity.from + mention.textContent!.length
      mentionEntity.mentionedUser = { username: mention.textContent! }

      startPosition = mentionEntity.to

      if (message.textAttributes) message.textAttributes.entities?.push(mentionEntity)
      else message.textAttributes = { entities: [mentionEntity] }
    }

    return message
  }
}

/** Converts html tags into text attributes. */
export class RichTextFormatter extends Formatter {
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return original.messagetype === 'RichText/Html'
  }

  format(message: Message, original: TeamsMessage): Message {
    const dom = new JSDOM(message.text, { includeNodeLocations: true })
    const frag = dom.window.document.body

    // init objects
    message.attachments = message.attachments ?? []
    message.textAttributes = {
      entities: [],
    }

    // Find GIFs and post them as an attachments, images are attachments
    const images = frag.querySelectorAll('img')
    for (const image of images) {
      message.attachments.push({
        id: 'generic-image',
        type: MessageAttachmentType.IMG,
        srcURL: image.getAttribute('src')!,
        size: {
          height: parseInt(image.getAttribute('height')!) / 2,
          width: parseInt(image.getAttribute('width')!) / 2,
        },
      })
    }

    message.text = frag.textContent?.trim() ?? ''

    return message
  }

  encodeCondition(messageProperties: { content: MessageContent; options?: MessageSendOptions }): boolean {
    return !messageProperties.options?.quotedMessageID
  }

  async encodeFormat(messageProperties: { content: MessageContent; options?: MessageSendOptions }): Promise<MessageContent> {
    const { content } = messageProperties

    const dom = new JSDOM()
    const { document } = dom.window

    // this one should always be last
    if (content.text) {
      const paragraph = document.createElement('p')
      paragraph.textContent = content.text
      document.body.appendChild(paragraph)
    }

    content.text = document.body.innerHTML
    return content
  }
}

/** Hides removed messages */
export class RemovedMessagesFormatter extends Formatter {
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return original.content === '' && !!original.properties?.deletetime
  }

  format(message: Message, original: TeamsMessage): Message {
    message.isHidden = true
    return message
  }
}

/** Attaches attachments */
export class AttachmentsFormatter extends Formatter {
  formattingCondition(message: Message, original: TeamsMessage): boolean {
    return !!original.properties?.files
  }

  format(message: Message, original: TeamsMessage): Message {
    const attachments: Array<MessageAttachment> = []

    original.properties!.filesSerialized = JSON.parse(original.properties!.files!) as Array<TeamsODAttachedFile>

    for (const attachment of original.properties!.filesSerialized) {
      message.links = [{ title: attachment.fileName, url: attachment.fileInfo.shareUrl, img: attachment.filePreview?.previewUrl }]
    }

    message.attachments = attachments

    return message
  }
}
