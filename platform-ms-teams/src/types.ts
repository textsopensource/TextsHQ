export type TeamsMessageContentType =
  'Text' |
  'RichText/Html' |
  'ThreadActivity/ThreadShared' |
  'ThreadActivity/JoiningEnabledUpdate' |
  'ThreadActivity/TopicUpdate' |
  'ThreadActivity/AddMember' |
  'Control/Typing'

export type TeamsContentType = 'text' | '' | 'Application/Message' // yes, it can be an empty string

export type TeamsThreadActivityType =
  'addmember' |
  'topicupdate' |
  'joiningenabledupdate' |
  'threadshared' |
  'deletemember' |
  string

export type GrabbedTokens = {
  skypeToken: string,
  teamsToken: string,
  spacesToken: string,
  refreshToken: string,
  onedriveToken: string,
  expiration?: {
    expiresIn: number,
    refreshIn: number,
    created: Date,
  }
}
