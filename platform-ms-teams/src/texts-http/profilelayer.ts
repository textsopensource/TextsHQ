import TextsHttpShared from './index'
import { ApplyHeaders, CheckAuthStatus, JSONify, PrintBodies } from './middleware'

export default abstract class TextsHttpProfileLayer {
  public readonly http: TextsHttpShared = new TextsHttpShared()

  public headers: Map<string, string> = new Map<string, string>()

  constructor() {
    this.http.addMiddlewares([new CheckAuthStatus(), new JSONify(), new ApplyHeaders(this), new PrintBodies()])
    // if (texts.IS_DEV) this.http.addMiddlewares([new PrintBodies()])
  }

  public get headersAsObject(): Record<string, string> {
    return Object.fromEntries(this.headers)
  }

  protected applySkypeToken(skypeToken: string): void {
    this.headers.set('authentication', `skypetoken=${skypeToken}`)
  }

  protected applyBearerToken(bearerToken: string): void { // for future use
    this.headers.set('Authorization', `Bearer ${bearerToken}`)
  }
}
