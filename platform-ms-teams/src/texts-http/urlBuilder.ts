import * as uuid from 'uuid'

export default class UrlBuilder {
  constructor(public url: string) {}

  static msgapi() {
    return new UrlBuilder('https://msgapi.teams.live.com')
  }

  static teams() {
    return new UrlBuilder('https://teams.live.com')
  }

  path(path: string) {
    this.url += `/${path}`
    return this
  }

  // msgapi paths
  conversationMessages(conversation: string, message?: string) {
    this.url += message ? `/v1/users/ME/conversations/${conversation}/messages/${message}` : `/v1/users/ME/conversations/${conversation}/messages`
    return this
  }

  notificationRequest() {
    this.url += `/v2/users/ME/endpoints/${uuid.v4()}`
    return this
  }

  userProps() {
    this.url += '/v1/users/ME/properties'
    return this
  }

  // teams paths
  userSummary() {
    this.url += '/api/csa/api/v1/teams/users/me?isPrefetch=false&enableMembershipSummary=true'
    return this
  }

  group(threadID: string) {
    this.url += `/api/mt/beta/groups/${threadID}`
    return this
  }

  users() {
    this.url += '/api/mt/beta/users'
    return this
  }

  fetch() {
    this.url += '/fetch'
    return this
  }

  profilePicture(mri: string) {
    this.url += `/${mri}/profilepicture`
    return this
  }
}
