import { FetchOptions, FetchResponse, texts } from '@textshq/platform-sdk'
import _ from 'lodash'
import { HttpNewMiddleware } from './middleware'

const sharedHttpClient = texts.createHttpClient!()

export default class TextsHttpShared {
  private readonly http = sharedHttpClient

  readonly predefinedHeaders: Record<string, string> = {
    'User-Agent': texts.constants.USER_AGENT,
    Accept: 'application/json',
  }

  public middleware: Array<HttpNewMiddleware> = []

  public addMiddlewares(middlewares: Array<HttpNewMiddleware>) {
    this.middleware.push(...middlewares)
    this.middleware = _.sortBy(this.middleware, 'weight')
  }

  async request(url: string, options: FetchOptions & { jsonBody?: Record<string, any> | Array<any>, preferBuffer?: boolean } = {}) {
    for (const middleware of this.middleware) options = await middleware.before(options)

    let response: FetchResponse<any>
    if (options.preferBuffer) {
      response = await this.http.requestAsBuffer(url, options)
    } else {
      response = await this.http.requestAsString(url, options)
    }

    for (const middleware of this.middleware) response = await middleware.after(response)

    return response as FetchResponse<any>
  }

  async proxyRequest(url: string, options: FetchOptions & { jsonBody?: Record<string, any> | Array<any>, preferBuffer?: boolean }): Promise<string> {
    options.preferBuffer = true
    const { body, headers } = await this.request(url, options)

    return `data:${headers['content-type']};base64,${Buffer.from(body).toString('base64')}`
  }
}
