/* eslint-disable no-param-reassign */
import { Awaitable, FetchOptions, FetchResponse, texts } from '@textshq/platform-sdk'
import { TeamsAuthTokenError } from '../errors'
import type TextsHttpProfileLayer from './profilelayer'

export abstract class HttpNewMiddleware {
  /** weight determines order of execution */
  abstract weight: number

  constructor(protected readonly parentProfile?: TextsHttpProfileLayer) {}

  before(options: FetchOptions): Awaitable<FetchOptions> {
    return options
  }

  after(response: FetchResponse<any>): Awaitable<FetchResponse<any>> {
    return response
  }
}

export class CheckAuthStatus extends HttpNewMiddleware {
  weight = 0

  after(response: FetchResponse<any>): Awaitable<FetchResponse<any>> {
    switch (response.statusCode) {
      case 401:
        throw new TeamsAuthTokenError(response)
      case 404:
        throw new Error('404 Probably url constructing error')
      default:
        if (response.statusCode >= 400) {
          throw new Error('Unpredicted error! ' + (response.body as string))
        }
    }

    return response
  }
}

export class JSONify extends HttpNewMiddleware {
  weight = 90

  before(options: FetchOptions & { jsonBody?: Record<string, any> }): Awaitable<FetchOptions> {
    if (options.jsonBody) options.body = JSON.stringify(options.jsonBody!)
    return options
  }

  after(response: FetchResponse<any>): Awaitable<FetchResponse<any>> {
    if (response.headers['content-type']?.toLowerCase().includes('application/json')) response.body = JSON.parse(response.body)
    return response
  }
}

export class ApplyHeaders extends HttpNewMiddleware {
  weight = 10

  before(options: FetchOptions): Awaitable<FetchOptions> {
    if (this.parentProfile) options.headers = { ...this.parentProfile.http.predefinedHeaders, ...options.headers, ...this.parentProfile.headersAsObject }
    return options
  }
}

export class PrintBodies extends HttpNewMiddleware {
  weight = 99

  before(options: FetchOptions): FetchOptions {
    if (process.env.PRINT_HTTP_TEAMS) {
      texts.log('headers', JSON.stringify(options.headers))
    }

    return options
  }

  after(response: FetchResponse<any>): Awaitable<FetchResponse<any>> {
    if (process.env.PRINT_HTTP_TEAMS) {
      texts.log('body', JSON.stringify(response.body))
      texts.log('status', JSON.stringify(response.statusCode))
    }

    return response
  }
}
