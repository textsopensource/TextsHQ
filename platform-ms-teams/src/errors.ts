import { FetchResponse } from '@textshq/platform-sdk'

export class TeamsAuthTokenError extends Error {
  message: string

  name: string = 'TeamsAuthTokenError'

  constructor(response: FetchResponse<any>) {
    super()
    this.message = `Reauthenticating account should solve issue - ${response.statusCode} - ${response.body}`
  }
}
