_ = {
  event: 'patch',
  data: [
    {
      op: 'replace',
      path: '/direct_v2/threads/340282366841710300949128119219161184162/participants/31748619766/has_seen',
      value: '{"item_id": "30017407817042111042197838874279936", "client_context": "6825168512507441856", "timestamp": 1627246979468234, "created_at": 1627246938381021, "shh_seen_state": {}}'
    }
  ],
  message_type: 1,
  seq_id: 24214,
  mutation_token: null,
  realtime: true,
  message: {
    path: '/direct_v2/threads/340282366841710300949128119219161184162/participants/31748619766/has_seen',
    op: 'replace',
    participants: { '31748619766': 'has_seen' },
    threadId: '340282366841710300949128119219161184162',
    item_id: '30017407817042111042197838874279936',
    client_context: '6825168512507441856',
    timestamp: 1627246979468234,
    created_at: 1627246938381021,
    shh_seen_state: {}
  }
}
