import { texts, MessageDeletionMode, Attribute, PlatformInfo } from '@textshq/platform-sdk'

import icon from './icon'
import { genClientContext } from './util'

const onNavigate = `
(async () => {
  if (window.location.href.includes('https://www.instagram.com/accounts/onetap')) {
    window.location = '/'
  } else if (document.cookie.includes('ds_user_id') && window.require && !window.textsBackupCodes) {
    window.textsBackupCodes = await require('PolarisInstajax').post("/accounts/two_factor_authentication/ajax/get_backup_codes/", { refresh: false }).catch(() => ({}))
    setTimeout(() => window.close(), 100)
  }
  return JSON.stringify(window.textsBackupCodes)
})()
`

const info: PlatformInfo = {
  name: 'instagram',
  version: '1.0.0',
  displayName: 'Instagram',
  icon,
  reactions: {
    supported: {
      '❤️': { title: '❤️', render: '❤️' },
      '😂': { title: '😂', render: '😂' },
      '😮': { title: '😮', render: '😮' },
      '😢': { title: '😢', render: '😢' },
      '😡': { title: '😡', render: '😡' },
      '👍': { title: '👍', render: '👍' },
    },
    canReactWithAllEmojis: true,
  },
  deletionMode: MessageDeletionMode.UNSEND,
  typingDurationMs: 10_000,
  attributes: new Set([
    // Attribute.SUPPORTS_STOP_TYPING_INDICATOR,
    Attribute.SUPPORTS_REQUESTS_INBOX,
    Attribute.SUPPORTS_QUOTED_MESSAGES,
    Attribute.SUPPORTS_GROUP_PARTICIPANT_ROLE_CHANGE,
    Attribute.SUPPORTS_DELETE_THREAD,
    Attribute.SUPPORTS_PRESENCE,
    Attribute.CAN_MESSAGE_USERNAME,
    Attribute.SUBSCRIBE_TO_CONN_STATE_CHANGE,
    Attribute.SUBSCRIBE_TO_ONLINE_OFFLINE_ACTIVITY,
    Attribute.SUPPORTS_PUSH_NOTIFICATIONS,
  ]),
  loginMode: 'browser',
  autofillHostnames: ['instagram.com', 'facebook.com'],
  auth: texts.React?.lazy(() => import('./auth')),
  browserLogins: [
    {
      label: 'Open Instagram Login Page',
      loginURL: 'https://www.instagram.com',
      // authCookieName: 'ds_user_id',
      runJSOnClose: onNavigate,
    },
  ],
  attachments: {
    noSupportForFiles: true,
  },
  notifications: {
    // web: {
    //   vapidKey: 'BIBn3E_rWTci8Xn6P9Xj3btShT85Wdtne0LtwNUyRQ5XjFNkuTq9j4MPAVLvAFhXrUU1A9UxyxBA7YIOjqDIDHI',
    // },
    android: {
      senderID: '390017741467',
    },
  },
  generateUniqueMessageID: () => genClientContext().toString(),
  getUserProfileLink: ({ username }) =>
    `https://www.instagram.com/${username}/`,
}

export default info
