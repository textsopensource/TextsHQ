import crypto from 'crypto'
import { promises as fs } from 'fs'
import { pick } from 'lodash'
import urlRegex from 'url-regex'
import Jimp from 'jimp'
import mem from 'mem'
import { setTimeout as setTimeoutAsync } from 'timers/promises'

import { texts, PlatformAPI, Paginated, Thread, Message, MessageContent, OnServerEventCallback, LoginResult, InboxName, CodeRequiredReason, ReAuthError, PaginationArg, AccountInfo, MessageSendOptions, ActivityType, OnConnStateChangeCallback, ConnectionStatus, LoginCreds, SerializedSession, UpsertStateSyncEvent, Awaitable } from '@textshq/platform-sdk'
import { AndroidIgpapi, IgLoginBadPasswordError, IgLoginInvalidUserError, IgChallengeWrongCodeError, IgLoginTwoFactorRequiredError, IgCheckpointError, IgUserHasLoggedOutError, IgLoginRequiredError } from '@textshq/android'
import { AndroidRealtimeClient } from '@textshq/android-realtime'
import { GraphQLSubscriptions, SkywalkerSubscriptions, TypingStatus } from '@textshq/realtime-core'

import IgAPI from './ig-api'
import { mapThread, mapMessages, mapEvents, mapParticipant, mapCurrentUser, mapPresence, IGUser } from './mappers'
import { genClientContext } from './util'
// import { IgResponseError } from '../../../igpapi/node_modules/@textshq/packages/core/dist'

const { IS_DEV } = texts

const enum VerificationMethod {
  TOTP = '0',
  SMS = '1',
  BACKUP_CODE = '2',
}

const convertImageBuffer = (imgBuffer: Buffer) =>
  Jimp.read(imgBuffer)
    .then(i => i.getBufferAsync('image/jpeg'))

function assignDeviceBuildAndDescriptor(device: any) {
  // from igpapi/packages/android/src/samples/builds.json
  device.build = 'N2G48C'
  // from igpapi/packages/android/src/samples/devices.json
  device.descriptor = '27/8.1.0; 480dpi; 1080x2280; samsung; SM-T3XTS; a6plte; qcom'
}

const APP_VERSION = '213.0.0.29.120'
const APP_VERSION_CODE = '321303819'

export default class Instagram implements PlatformAPI {
  private readonly ig = new AndroidIgpapi()

  private readonly mqtt = new AndroidRealtimeClient(this.ig)

  private readonly api = new IgAPI(this.ig)

  private readonly sendMessageResolvers = new Map<string, (value: Message[]) => void>()

  currentUser: IGUser = null

  private eventHandler: OnServerEventCallback = () => {}

  private seqId = 0

  // We need to use the first SeqId when reconnecting realtime
  private initialSeqId = 0

  accountID: string

  // httpClient = texts.createHttpClient()

  init = async (session: SerializedSession, { accountID }: AccountInfo) => {
    this.accountID = accountID
    if (!session) return
    if (session.state) { // older instagram-private-api state
      Object.assign(this.ig.state.device, pick(session.state, ['uuid', 'phoneId', 'adid', 'build']))
      this.ig.state.device.descriptor = session.state.deviceString
      this.ig.state.device.id = session.state.deviceId
      this.ig.state.deserialize({ cookies: session.cookies })
    } else {
      this.ig.state.deserialize(session)
    }
    this.ig.state.application.APP_VERSION = APP_VERSION
    this.ig.state.application.APP_VERSION_CODE = APP_VERSION_CODE
    try {
      await this.afterAuth()
    } catch (error) {
      if ([IgUserHasLoggedOutError.name, IgLoginRequiredError.name].includes(error.name)) throw new ReAuthError()
      throw error
    }
  }

  private updateSeqID = (newID: number) => {
    if (newID == null) return
    this.seqId = Math.max(this.seqId, newID)
  }

  subscribeToEvents = (onEvent: OnServerEventCallback) => {
    this.eventHandler = onEvent
  }

  dispose = async () => {
    try {
      await this.mqtt?.disconnect()
    } catch (err) {
      console.error(err)
    }
  }

  private connCallback: OnConnStateChangeCallback = () => { }

  onConnectionStateChange = (onEvent: OnConnStateChangeCallback) => {
    this.connCallback = onEvent
  }

  private reconnectMqtt = async (delay = 0) => {
    await setTimeoutAsync(delay)
    // This will set the `safeDisconnect` as true
    await this.mqtt?.disconnect()
    await this.connectRealtime()
  }

  private connectRealtime = async () => {
    const { mqtt } = this

    const connect = async (wasDisconnected = false) => {
      const snapshot_at_ms = Date.now()
      texts.log('[ig] mqtt connecting', { wasDisconnected }, this.initialSeqId, snapshot_at_ms)
      try {
        await mqtt.connect({
          graphQlSubs: [
            GraphQLSubscriptions.getAppPresenceSubscription(),
            GraphQLSubscriptions.getClientConfigUpdateSubscription(),
            GraphQLSubscriptions.getDirectStatusSubscription(),
            GraphQLSubscriptions.getDirectTypingSubscription(this.ig.state.cookies.userId),
          ],
          skywalkerSubs: [
            SkywalkerSubscriptions.directSub(this.ig.state.cookies.userId),
            SkywalkerSubscriptions.liveSub(this.ig.state.cookies.userId),
          ],
          irisData: {
            seq_id: this.initialSeqId,
            snapshot_at_ms,
            snapshot_app_version: APP_VERSION,
          },
        })
      } catch (err) {
        // IgCookieNotFoundError: Cookie "ds_user_id" not found
        if (err.name === 'IgCookieNotFoundError') {
          texts.error(err)
          this.connCallback({ status: ConnectionStatus.UNAUTHORIZED })
        } else {
          throw err
        }
      }
    }

    mqtt.message$.subscribe(payload => {
      this.updateSeqID(payload.seq_id)
      if (IS_DEV) console.log('message$', payload)
      const resolve = this.sendMessageResolvers.get(payload.mutation_token)
      const mapped = mapEvents('message', payload, this.currentUser)
      if (!mapped?.length) return
      if (resolve) {
        resolve((mapped[0] as UpsertStateSyncEvent).entries as Message[])
        this.sendMessageResolvers.delete(payload.mutation_token)
      } else {
        this.eventHandler(mapped)
      }
    })
    mqtt.realtimeDirect$.subscribe(data => {
      if (IS_DEV) console.log('realtimeDirect$', data)
      const mapped = mapEvents('realtimeDirect', data)
      if (mapped?.length) this.eventHandler(mapped)
    })
    mqtt.appPresence$.subscribe(data => {
      if (IS_DEV) console.log('appPresence$', data)
      const mapped = mapEvents('presence', data)
      if (mapped?.length) this.eventHandler(mapped)
    })

    if (texts.isLoggingEnabled) {
      mqtt.warning$.subscribe((...args) => console.log('warning', ...args))
      mqtt.error$.subscribe((...args) => console.log('[ig] mqtt error', ...args))
      mqtt.mqttClient?.$disconnect?.subscribe((...args) => {
        texts.log('[ig] mqtt disconnected', ...args)
        texts.Sentry.captureMessage('ig: mqtt.mqttClient?.$disconnect')
        // setTimeout(() => {
        //   if (mqtt.safeDisconnect) mqtt.disconnect()
        //   else connect(true)
        // }, 1000)
      })

      // mqtt.liveComments$.subscribe((...args) => console.log('liveComments$', ...args))
      mqtt.clientConfigUpdate$.subscribe((...args) => console.log('clientConfigUpdate$', ...args))
      mqtt.regionHint$.subscribe((...args) => console.log('regionHint$', ...args))
      mqtt.realtimeSub$.subscribe((...args) => console.log('realtimeSub$', ...args))
      mqtt.zeroProvision$.subscribe((...args) => console.log('zeroProvision$', ...args))
    }
    connect()
  }

  reconnectRealtime = async () => {
    texts.log('[ig] reconnectRealtime')
    await this.reconnectMqtt()
  }

  serializeSession = () => this.ig.state.toJSON()

  private async afterAuth() {
    this.currentUser = await this.ig.account.currentUser()
  }

  getCurrentUser = () => mapCurrentUser(this.currentUser)

  logout = async () => {
    await this.ig.account.logout()
  }

  searchUsers = mem(async (typed: string) => {
    if (!typed) return []
    const body = await this.api.rankedRecipients(typed)
    const { ranked_recipients } = body
    return ranked_recipients.map(r => r.user && mapParticipant(r.user, [])).filter(Boolean) // r.thread is returned in many instances
  })

  createThread = async (userIDs: string[]) => {
    if (userIDs.length === 0) return null
    const body = await this.api.createGroupThread(userIDs, undefined)
    return mapThread(body, this.currentUser)
  }

  getThread = async (threadID: string) => {
    const body = await this.api.getThread(this.seqId, threadID)
    if (!body?.thread) return
    return mapThread(body.thread, this.currentUser)
  }

  getThreads = async (inboxName: InboxName, pagination: PaginationArg): Promise<Paginated<Thread>> => {
    const { cursor, direction } = pagination || { cursor: null, direction: null }
    const inboxType = {
      [InboxName.NORMAL]: 'inbox',
      [InboxName.REQUESTS]: 'pending_inbox',
    }[inboxName]
    const { seq_id, inbox } = await this.api.getThreads(this.seqId, inboxType, cursor, direction)
    this.initialSeqId = seq_id
    const { threads } = inbox
    const rtNotConnected = !this.seqId
    this.updateSeqID(seq_id)
    if (rtNotConnected) this.connectRealtime()
    return {
      items: (threads as any[]).map(t => mapThread(t, this.currentUser)),
      hasMore: inbox.has_older,
      oldestCursor: inbox.oldest_cursor,
      // newestCursor: inbox.newest_cursor,
    }
  }

  getMessages = async (threadID: string, pagination: PaginationArg): Promise<Paginated<Message>> => {
    const { cursor, direction } = pagination || { cursor: null, direction: null }
    const body = await this.api.getMessages(this.seqId, threadID, cursor, direction)
    const items = mapMessages(body.thread, body.thread.viewer_id)
    return { items, hasMore: items.length > 0 }
  }

  sendMessage = async (threadID: string, content: MessageContent, sendOptions: MessageSendOptions) => {
    const mutationToken = sendOptions.pendingMessageID.includes('-')
      ? genClientContext().toString() // for ios
      : sendOptions.pendingMessageID
    const promise = new Promise<Message[]>(resolve => {
      this.sendMessageResolvers.set(mutationToken, resolve)
    })
    if (content.fileBuffer) {
      await this.sendFileFromBuffer(mutationToken, threadID, content.fileBuffer, content.mimeType, sendOptions)
    }
    if (content.filePath) {
      const buffer = await fs.readFile(content.filePath)
      await this.sendFileFromBuffer(mutationToken, threadID, buffer, content.mimeType, sendOptions)
    }
    if (content.text) {
      await this.sendTextMessage(mutationToken, threadID, content.text, sendOptions)
    }
    return Promise.race([
      promise,
      setTimeoutAsync(5_000).then(() => true), // workaround for not having send failure if mqtt is disconnected
    ])
  }

  private sendRegularTextMessage = async (mutationToken: string, threadID: string, text: string, sendOptions: MessageSendOptions) => {
    const options = {
      item: 'text',
      form: {
        text,
        replied_to_item_id: sendOptions.quotedMessageID,
        replied_to_client_context: crypto.randomUUID(),
      },
    }
    const res = await this.api.broadcast(mutationToken, threadID, options)
    if (IS_DEV) console.log(res)
    if (res.status !== 'ok') throw Error('failed to send: ' + res.status_code)
  }

  private sendTextMessage = async (mutationToken: string, threadID: string, text: string, sendOptions: MessageSendOptions) => {
    const urls = text.match(urlRegex({ strict: false }))
    if (Array.isArray(urls)) {
      try {
        const res = await this.api.broadcast(mutationToken, threadID, {
          item: 'link',
          form: {
            link_text: text,
            link_urls: JSON.stringify(urls),
          },
        })
        if (res.status !== 'ok') throw Error('failed to send: ' + res.status_code)
      } catch (err) {
        if (err.name === 'IgResponseError' && err.message.includes('400 Bad Request')) {
          console.error('retrying message as regular', err.message)
          return this.sendRegularTextMessage(mutationToken, threadID, text, sendOptions)
        }
        throw err
      }
    } else {
      return this.sendRegularTextMessage(mutationToken, threadID, text, sendOptions)
    }
  }

  private sendFileFromBuffer = async (mutationToken: string, threadID: string, fileBuffer: Buffer, mimeType: string, sendOptions: MessageSendOptions) => {
    if (mimeType.startsWith('image/')) {
      const file = mimeType === 'image/jpeg' ? fileBuffer : await convertImageBuffer(fileBuffer)
      const res = await this.api.broadcastPhoto(mutationToken, threadID, { file })
      if (res.status !== 'ok') throw Error('failed to send: ' + res.status_code)
    } else if (mimeType.startsWith('video/')) {
      const res = await this.api.broadcastVideo(mutationToken, threadID, { video: fileBuffer })
      if (res.status !== 'ok') throw Error('failed to send: ' + res.status_code)
    }
  }

  sendActivityIndicator = async (type: ActivityType, threadID: string) => {
    switch (type) {
      case ActivityType.TYPING:
        await this.mqtt.directCommands.indicateActivity({
          thread_id: threadID,
          activity_status: TypingStatus.Text,
        })
        break

      case ActivityType.NONE:
        await this.mqtt.directCommands.indicateActivity({
          thread_id: threadID,
          activity_status: TypingStatus.Off,
        })
        break

      case ActivityType.ONLINE:
        await this.mqtt.directCommands.sendForegroundState({ inForegroundDevice: true, inForegroundApp: true })
        break

      case ActivityType.OFFLINE:
        await this.mqtt.directCommands.sendForegroundState({ inForegroundDevice: false, inForegroundApp: false })
        break

      default:
    }
  }

  private broadcastReaction = (threadID: string, messageID: string, reactionKey: string, created = true) =>
    this.api.broadcast(undefined, threadID, {
      item: 'reaction',
      form: {
        item_id: messageID,
        item_type: 'reaction',
        node_type: 'item',
        emoji: created ? reactionKey : '',
        reaction_action_source: '',
        reaction_status: created ? 'created' : 'deleted',
        reaction_type: 'like',
      },
    })

  addReaction = async (threadID: string, messageID: string, reactionKey: string) => {
    await this.broadcastReaction(threadID, messageID, reactionKey)
  }

  removeReaction = async (threadID: string, messageID: string, reactionKey: string) => {
    await this.broadcastReaction(threadID, messageID, reactionKey, false)
  }

  deleteMessage = async (threadID: string, messageID: string) => {
    const body = await this.api.deleteItem(threadID, messageID)
    return body.status === 'ok'
  }

  sendReadReceipt = async (threadID: string, messageID: string) => {
    const body = await this.api.markItemSeen(threadID, messageID)
    return body.status === 'ok'
  }

  updateThread = async (threadID: string, updates: Partial<Thread>) => {
    if ('title' in updates) {
      const body = await this.api.updateTitle(threadID, updates.title)
      return body.status === 'ok'
    }
    if ('mutedUntil' in updates) {
      const body = await this.api.muteThread(threadID, updates.mutedUntil === 'forever')
      return body.status === 'ok'
    }
  }

  deleteThread = async (threadID: string) => {
    await this.api.hide(threadID)
  }

  addParticipant = async (threadID: string, participantID: string) => {
    const body = await this.api.addUser(threadID, [participantID])
    return body.status === 'ok'
  }

  removeParticipant = async (threadID: string, participantID: string) => {
    const body = await this.api.removeUser(threadID, participantID)
    return body.status === 'ok'
  }

  changeParticipantRole = async (threadID: string, participantID: string, role: string) => {
    const body = await this.api.changeRole(threadID, participantID, role)
    return body.status === 'ok'
  }

  getPresence = async () => {
    const p = await this.api.getPresence()
    return mapPresence(p.user_presence)
  }

  // getAsset = async (key: string, hex?: string) => {
  //   if (key === 'proxy') {
  //     const url = Buffer.from(hex, 'hex').toString()
  //     // cannot use texts.fetchStream because it sends CORS headers too
  //     const body = await this.httpClient.requestAsBuffer(url)
  //     return body.body
  //   }
  // }

  // authentication

  private sendSecurityCode = (code: string) =>
    this.ig.challenge.sendSecurityCode(code)

  private loginMetadata: { username: string, twoFactorIdentifier: string, verificationMethod: VerificationMethod } = null

  private twoFactorLogin = (code: string, { username, twoFactorIdentifier, verificationMethod } = this.loginMetadata) =>
    this.ig.account.twoFactorLogin({
      username,
      verificationCode: code,
      twoFactorIdentifier,
      verificationMethod,
      trustThisDevice: '1',
    })

  private loginWithCode = async (reason: CodeRequiredReason, code: string): Promise<LoginResult> => {
    try {
      if (reason === CodeRequiredReason.CHECKPOINT) await this.sendSecurityCode(code)
      else if (reason === CodeRequiredReason.TWO_FACTOR) await this.twoFactorLogin(code)
    } catch (err) {
      texts.error(err)
      return this.handleLoginError(err, this.loginMetadata ? [this.loginMetadata.username] : undefined)
    }
    await this.afterAuth()
    process.nextTick(() => this.ig.simulate.postLoginFlow())
    return { type: 'success' }
  }

  private handleLoginError = (err: any, redact: string[] = []): LoginResult => {
    if (![IgLoginBadPasswordError.name, IgLoginInvalidUserError.name, IgChallengeWrongCodeError.name, 'IgResponseError'].includes(err.name)) throw err
    const { error_title, message } = err.response?.body || {}
    if (IS_DEV) console.log(JSON.stringify(err.response?.body))
    const errorMessage = [error_title, message].filter(Boolean).join('\n')
      || [err.name, err.message].filter(Boolean).join('\n')
      || JSON.stringify(err)
    texts.Sentry.captureMessage('Instagram login error: ' + errorMessage.replaceAll(redact[0], '<username>'))
    return { type: 'error', errorMessage }
  }

  private loginFlowSimulated: string

  simulatePreLoginFlow = async (seed: string) => {
    if (this.loginFlowSimulated !== seed) await this.ig.simulate.preLoginFlow().catch(console.error)
    this.loginFlowSimulated = seed
  }

  loginWithJar = async (cookieJarJSON: LoginCreds['cookieJarJSON'], jsCodeResult: string): Promise<LoginResult> => {
    const seed = this.accountID
    this.ig.state.device.generate(seed)
    assignDeviceBuildAndDescriptor(this.ig.state.device)
    this.ig.state.cookies.deserialize(cookieJarJSON)
    try {
      await this.ig.simulate.upgrade()
    } catch (err) {
      if (err.response?.body?.error_type === 'two_factor_required') {
        const { username, two_factor_identifier: twoFactorIdentifier } = err.response.body.two_factor_info
        this.loginMetadata = { username, twoFactorIdentifier, verificationMethod: VerificationMethod.BACKUP_CODE }
        const { backup_codes } = JSON.parse(jsCodeResult)
        await this.loginWithCode(CodeRequiredReason.TWO_FACTOR, backup_codes[0].replace(' ', ''))
      } else {
        throw err
      }
    }
    await this.afterAuth()
    process.nextTick(() => this.ig.simulate.postLoginFlow())
    return { type: 'success' }
  }

  login = async ({ lastLoginResult, cookieJarJSON, code, username, password, jsCodeResult }: LoginCreds): Promise<LoginResult> => {
    if (cookieJarJSON) return this.loginWithJar(cookieJarJSON, jsCodeResult)
    try {
      if (lastLoginResult) {
        // eslint-disable-next-line @typescript-eslint/return-await
        return await this.loginWithCode(lastLoginResult.reason, code)
      }
      const seed = this.accountID
      this.ig.state.device.generate(seed)
      assignDeviceBuildAndDescriptor(this.ig.state.device)
      await this.simulatePreLoginFlow(seed)
      await this.ig.account.login(username, password)
      await this.afterAuth()
      process.nextTick(() => this.ig.simulate.postLoginFlow())
      return { type: 'success' }
    } catch (err) {
      if (err.name === IgCheckpointError.name) {
        await this.ig.challenge.auto(true)
        return { type: 'code_required', reason: CodeRequiredReason.CHECKPOINT }
      }
      if (err.name === IgLoginTwoFactorRequiredError.name) {
        const { totp_two_factor_on, two_factor_identifier: twoFactorIdentifier } = err.response.body.two_factor_info
        const verificationMethod = totp_two_factor_on ? VerificationMethod.TOTP : VerificationMethod.SMS
        this.loginMetadata = { username, twoFactorIdentifier, verificationMethod }
        return {
          type: 'code_required',
          reason: CodeRequiredReason.TWO_FACTOR,
          title: `Enter two-factor code ${verificationMethod === '1' ? 'received via SMS' : 'from your authenticator app'}`,
        }
      }
      return this.handleLoginError(err, [username])
    }
  }

  registerForPushNotifications = async (type: 'android', token: string) => {
    if (type !== 'android') throw Error('invalid type')
    const body = await this.api.register(token)
    if (body.status !== 'ok') throw Error(`failed to register ${JSON.stringify(body)}`)
  }
}
