import React from 'react'

const auth: React.FC = ({ children }) => (
  <div>
    {children}
    <footer>You'll get a login notification from Instagram saying Texts has logged in as "Samsung SM-T3XTS". There is no actual or simulated device used but how Texts identifies itself. All data stays local.</footer>
  </div>
)

export default auth
