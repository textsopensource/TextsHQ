/* eslint-disable no-restricted-syntax */
import { orderBy } from 'lodash'
import { Message, Thread, MessageAttachment, MessageReaction, MessageSeen, ServerEvent, Participant, CurrentUser, MessageAttachmentType, ServerEventType, PresenceMap, MessageActionType, TextEntity, ActivityType, texts, InboxName } from '@textshq/platform-sdk'
import type { IrisPayload } from '@textshq/realtime-core'

export type IGThread = any & {}
export type IGMessage = any & {}
export type IGUser = any & {
  pk: number | string
}

function mapReactions(reactions: any) {
  const emojiReactions = reactions?.emojis as any[] || []
  const heartReactions = reactions?.likes as any[] || []
  return emojiReactions.length > 0 ? emojiReactions.map<MessageReaction>((r: any) => ({
    id: r.sender_id.toString(),
    reactionKey: r.emoji,
    participantID: r.sender_id.toString(),
    emoji: true,
  })) : heartReactions.map<MessageReaction>((r: any) => ({
    id: r.sender_id.toString(),
    reactionKey: '❤️',
    participantID: r.sender_id.toString(),
  }))
}

function getSeen(last_seen_at: { [userID: string]: IGUser }, msg: IGMessage): MessageSeen {
  const result: { [userID: string]: Date } = {}
  Object.entries(last_seen_at).forEach(([userID, lastRead]) => {
    const { timestamp, item_id } = lastRead
    if (BigInt(item_id) >= BigInt(msg.item_id)) {
      result[userID] = new Date(+timestamp / 1000)
    }
  })
  if (Object.keys(result).length > 0) return result
}

const mapEntities = (attributes: any[]) =>
  (attributes ? attributes.map<TextEntity>(attr => {
    const basic = { from: attr.start, to: attr.end }
    const isChangeThemeIntent = attr.intent?.startsWith('instagram://thread/') && attr.intent?.endsWith('/details/change_theme')
    if (isChangeThemeIntent) {
      return { ...basic, replaceWith: '' }
    }
    const isUsernameIntent = attr.intent?.startsWith('instagram://user?username=')
    const username = isUsernameIntent ? attr.intent?.split('=')?.pop() : undefined
    return {
      ...basic,
      bold: attr.bold === 1,
      mentionedUser: username ? { username } : undefined,
    }
  }) : undefined)

type ItemType = 'igtv' | 'ig_story' | 'ig_post' | 'ig_reel'

export function mapMessage(msg: IGMessage, currentUserID: number | string, last_seen_at: any = {}) {
  // if (msg.hide_in_thread === 1) return null
  const senderID = String(msg.user_id)
  const mapped: Message = {
    _original: JSON.stringify(msg),
    id: msg.item_id,
    text: msg.text,
    timestamp: new Date(+msg.timestamp / 1000),
    senderID,
    isSender: senderID === String(currentUserID),
    reactions: mapReactions(msg.reactions),
    seen: getSeen(last_seen_at, msg),
    isHidden: msg.hide_in_thread === 1,
    forwardedCount: msg.show_forward_attribution ? 1 : undefined,
    isAction: undefined,
  }
  const pushAttachment = (att: MessageAttachment) => {
    if (!mapped.attachments) mapped.attachments = []
    if (typeof att.id !== 'string') att.id = String(att.id)
    mapped.attachments.push(att)
  }
  const pushVideo = (v: any, itemType: ItemType, caption: string) => {
    const firstVideo = v.video_versions[0]
    pushAttachment({
      id: v.id,
      type: MessageAttachmentType.VIDEO,
      srcURL: firstVideo.url,
      size: { width: firstVideo.width || undefined, height: firstVideo.height || undefined },
      caption,
      extra: { itemType },
    })
  }
  const pushImage = (i: any, itemType: ItemType, caption: string) => {
    const firstImg = i.image_versions2.candidates[0]
    pushAttachment({
      id: i.id,
      type: MessageAttachmentType.IMG,
      srcURL: firstImg.url,
      size: { width: firstImg.width || undefined, height: firstImg.height || undefined },
      caption,
      extra: { itemType },
    })
  }
  const pushMedia = (m: any, itemType: ItemType, _caption: string = undefined) => {
    // mapped.text = msg.media_share.caption && msg.media_share.caption.text
    if (!m) return
    const caption = m.user?.username
      ? `${m.clips_metadata ? 'Reel ' : ''}${m.is_reel_media ? 'Story ' : ''}by @${m.user?.username}`
      : _caption
    if (m.video_versions) {
      pushVideo(m, itemType, caption)
    } else if (m.image_versions2) {
      pushImage(m, itemType, caption)
    } else if (m.carousel_media) {
      const items = m.carousel_media as any[]
      items.forEach((cm, i) => pushMedia(cm, itemType, i === 0 ? caption : undefined))
      // m.carousel_share_child_media_id is the one that was selected while sending the message
      const initialIndex = items.findIndex(i => i.id === m.carousel_share_child_media_id)
      const firstAtt = mapped.attachments[0]
      if (!firstAtt.extra) firstAtt.extra = {}
      firstAtt.extra.initialIndex = initialIndex
    } else if (m.preview_url) {
      pushAttachment({
        id: m.preview_media_fbid,
        type: m.preview_url_mime_type.startsWith('image/') ? MessageAttachmentType.IMG : MessageAttachmentType.VIDEO,
        srcURL: m.preview_url,
        size: { width: m.preview_width || undefined, height: m.preview_height || undefined },
        caption: `by @${m.header_title_text}`,
        extra: { itemType },
      })
    }
  }
  const pushAnimatedMedia = (m: any) => {
    if (!m.animated_media?.images) return
    const vid = m.animated_media.images.fixed_height
    pushAttachment({
      id: m.animated_media.id,
      type: MessageAttachmentType.VIDEO,
      isGif: true,
      srcURL: vid.mp4,
      size: { width: +vid.width || undefined, height: +vid.height || undefined },
    })
  }
  if (!mapped.text) {
    if (msg.like) {
      mapped.text = '❤️'
    } else if (msg.reel_share) {
      pushMedia(msg.reel_share.media, 'ig_story')
      const mentionText = String(currentUserID) === String(msg.reel_share.reel_owner_id)
        ? 'You mentioned them in your story'
        : 'Mentioned you in their story'
      mapped.textHeading = msg.reel_share.type === 'mention' ? mentionText : 'Replied to story'
      mapped.text = msg.reel_share.text
      pushAnimatedMedia(msg.reel_share)
    } else if (msg.clip) {
      pushMedia(msg.clip.clip, 'ig_reel')
    } else if (msg.selfie_sticker) {
      pushAttachment({
        id: msg.selfie_sticker.media.id,
        type: MessageAttachmentType.VIDEO,
        isGif: true,
        srcURL: msg.selfie_sticker.media.media_urls.video_url,
        size: { height: 100, width: 100 },
      })
    } else if (msg.action_log) {
      mapped.isAction = true
      mapped.text = msg.action_log.description
      if (msg.action_log?.is_reaction_log) {
        mapped.action = {
          type: MessageActionType.MESSAGE_REACTION_CREATED,
        }
      }
      mapped.textAttributes = { entities: mapEntities(msg.action_log.text_attributes) }
    } else if (msg.video_call_event) {
      mapped.isAction = true
      mapped.text = msg.video_call_event.description
      mapped.textAttributes = { entities: mapEntities(msg.video_call_event.text_attributes) }
    } else if (msg.media) {
      pushMedia(msg.media, undefined) // regular media
    } else if (msg.media_share) {
      pushMedia(msg.media_share, 'ig_post')
    } else if (msg.direct_media_share) {
      pushMedia(msg.direct_media_share, 'ig_post')
    } else if (msg.xma_media_share) {
      (msg.xma_media_share as any[]).forEach(ms => {
        pushMedia(ms, 'ig_post')
      })
    } else if (msg.felix_share) { // IGTV
      if (msg.felix_share.video) {
        pushVideo(msg.felix_share.video, 'igtv', 'IGTV')
      }
      if (!mapped.text && msg.felix_share.text) mapped.text = msg.felix_share.text
    } else if (msg.guide_share) {
      pushMedia(msg.guide_share.guide_summary.cover_media, undefined)
      mapped.textHeading = [msg.guide_share.guide_summary.title, msg.guide_share.guide_summary.description].join('\n')
    } else if (msg.animated_media) {
      pushAnimatedMedia(msg)
    } else if (msg.voice_media) {
      pushAttachment({
        id: msg.voice_media.media.id,
        type: MessageAttachmentType.AUDIO,
        srcURL: msg.voice_media.media.audio.audio_src,
      })
    } else if (msg.link) {
      mapped.text = msg.link.text
      mapped.links = [{
        url: msg.link.link_context.link_url,
        img: msg.link.link_context.link_image_url,
        title: msg.link.link_context.link_title,
        summary: msg.link.link_context.link_summary,
      }]
    } else if (msg.placeholder) {
      mapped.textHeading = [msg.placeholder.title, msg.placeholder.message].filter(Boolean).join('\n')
    } else if (msg.profile) {
      mapped.links = [{
        url: 'https://www.instagram.com/' + msg.profile.username,
        img: msg.profile.profile_pic_url,
        title: msg.profile.full_name,
      }]
    } else if (msg.xma_profile) {
      mapped.links = (msg.xma_profile as any[]).map(p => ({
        url: p.target_url,
        img: p.header_icon_url_info?.url,
        title: p.header_subtitle_text,
      }))
    } else if (msg.visual_media) {
      pushMedia(msg.visual_media.media, undefined)
      if (!mapped.attachments) {
        mapped.textHeading = 'Instagram Visual Media'
      }
    } else if (msg.story_share) {
      pushMedia(msg.story_share.media, 'ig_story')
      mapped.textHeading = [msg.story_share.text, msg.story_share.title, msg.story_share.message].filter(Boolean).join('\n')
    } else if (msg.ar_effect) {
      mapped.textHeading = 'AR Effect · View in app'
    } else if (msg.live_viewer_invite) {
      mapped.textHeading = [msg.live_viewer_invite.title, msg.live_viewer_invite.message, msg.live_viewer_invite.text].filter(Boolean).join('\n')
      if (!mapped.textHeading) mapped.textHeading = 'Live broadcast · View in app'
    } else if (msg.live_video_share) {
      mapped.text = msg.live_video_share.text
    } else if (msg.product_share) {
      mapped.text = msg.product_share.text
      mapped.textHeading = `${msg.product_share.product.name} · ${msg.product_share.product.price}`
      mapped.textFooter = `📦 Product by @${msg.product_share.product.merchant.username}`
      pushMedia(msg.product_share.product.main_image, undefined)
    } else if (msg.p2b_order) {
      mapped.links = (msg.p2b_order as any[]).map(order => ({
        url: order.target_url,
        img: order.preview_url_info?.url,
        title: order.title_text + ' · ' + order.subtitle_text,
      }))
    } else if (msg.music) {
      mapped.links = (msg.music as any[]).map(m => ({
        url: m.target_url,
        img: m.preview_url_info?.url,
        title: m.title_text,
      }))
      mapped.textFooter = [msg.music[0]?.caption_body_text, msg.music[0]?.subtitle_text].filter(Boolean).join(' · ')
    }
  }
  if (msg.replied_to_message) {
    mapped.linkedMessageID = msg.replied_to_message.item_id
  }
  if (msg.message_power_up) {
    const style = {
      1: 'love',
      2: 'gift',
      3: 'confetti',
      4: 'fire',
    }[String(msg.message_power_up.style)]
    mapped.textFooter = (mapped.textFooter ? mapped.textFooter + '\n' : '') + `Sent with ${style} effect`
  }
  return mapped
}

export function mapMessages(thread: IGThread, currentUserID: number): Message[] {
  const { items } = thread
  return orderBy((items as any[]).map(m => mapMessage(m, currentUserID, thread.last_seen_at)), 'timestamp')
}

export function mapParticipant(user: IGUser, adminIDs: IGUser['pk'][], hasExited = false): Participant {
  return {
    id: user.pk.toString(),
    username: user.username,
    fullName: user.full_name,
    imgURL: user.profile_pic_url,
    isVerified: user.is_verified,
    isAdmin: adminIDs.includes(user.pk),
    hasExited,
  }
}

export function mapCurrentUser(user: IGUser): CurrentUser {
  return {
    id: user.pk.toString(),
    fullName: user.full_name,
    imgURL: user.profile_pic_url,
    displayText: '@' + user.username,
    isVerified: user.is_verified,
  }
}

function mapParticipants(thread: IGThread, currentUserIG: IGUser) {
  const adminIDs = thread.admin_user_ids
  const currentUser = mapParticipant(currentUserIG, thread.admin_user_ids)
  const participants: Participant[] = []
  // instagram server sends duplicate users in thread.users sometimes
  const added = new Set<IGUser['pk']>()
  const add = (user: IGUser, hasExited = false) => {
    if (!added.has(user.pk)) {
      const p = mapParticipant(user, adminIDs, hasExited)
      participants.push(p)
      added.add(user.pk)
    }
  }
  for (const user of thread.users) add(user)
  for (const user of thread.left_users) add(user)
  participants.push(currentUser)
  return participants
}

export function mapThread(thread: IGThread, currentUserIG: IGUser): Thread {
  const messages = mapMessages(thread, currentUserIG.pk)
  const currentUserLastSeen = thread.last_seen_at?.[currentUserIG.pk]
  return {
    _original: JSON.stringify(thread),
    id: thread.thread_id,
    folderName: thread.pending ? InboxName.REQUESTS : InboxName.NORMAL,
    isUnread: BigInt(thread.items[0]?.timestamp || 0) > BigInt(currentUserLastSeen?.timestamp || 0),
    lastReadMessageID: currentUserLastSeen?.item_id,
    mutedUntil: thread.muted ? 'forever' : undefined,
    isReadOnly: thread.input_mode !== 0,
    messages: {
      items: messages,
      hasMore: thread.has_older,
    },
    timestamp: messages[messages.length - 1]?.timestamp,
    type: thread.is_group ? 'group' : 'single',
    title: thread.is_group ? thread.thread_title : undefined,
    participants: {
      items: mapParticipants(thread, currentUserIG),
      hasMore: false,
    },
  }
}

type Op = 'add' | 'replace' | 'remove'

export function mapEvents(type: 'message' | 'realtimeDirect' | 'presence', event: IrisPayload | any, currentUser?: IGUser): ServerEvent[] {
  switch (type) {
    case 'message': {
      const ev = event as IrisPayload
      if (!Array.isArray(ev.data)) {
        texts.log('ig unknown realtime event', type, ev)
        texts.Sentry.captureMessage(`ig unknown realtime event: ${type}`)
        return
      }
      return (ev.data as { op: Op, path: string, value: string }[]).map<ServerEvent>(data => {
        const { op, path, value } = data
        const threadID = /\/threads\/(\d+)\/?/.exec(path)?.[1]
        if (path.endsWith(`/participants/${currentUser!.pk}/has_seen`)) { // current user has seen thread on another device
          return {
            type: ServerEventType.STATE_SYNC,
            objectName: 'thread',
            mutationType: 'update',
            objectIDs: {},
            entries: [{
              id: threadID,
              isUnread: false,
            }],
          }
        }
        if (path.endsWith('/has_seen') && op === 'replace') {
          // we only have enough data to construct message.seen[participantID]
          // and platform-sdk doesn't support that level of granularity atm
          return { type: ServerEventType.THREAD_MESSAGES_REFRESH, threadID }
        }
        // /direct_v2/threads/340282366841710300949128439552881286561/items/30498109180343140618703725723123712
        if (/^\/direct_v2\/threads\/\d+\/items\/\d+$/.test(path)) {
          switch (op) {
            case 'add':
            case 'replace': {
              const message = JSON.parse(value)
              return {
                type: ServerEventType.STATE_SYNC,
                objectName: 'message',
                mutationType: 'upsert',
                objectIDs: { threadID },
                entries: [mapMessage(message, currentUser!.pk)],
              }
            }
            case 'remove':
              return {
                type: ServerEventType.STATE_SYNC,
                objectName: 'message',
                mutationType: 'delete',
                objectIDs: { threadID },
                entries: [value],
              }
            default:
          }
          // /direct_v2/inbox/threads/340282366841710300949128148460184488415
        } else if (/^\/direct_v2\/inbox\/threads\/\d+$/.test(path)) {
          switch (op) {
            case 'add':
            case 'replace': {
              const igThread = JSON.parse(value)
              const thread = mapThread(igThread, currentUser)
              delete thread.messages
              delete thread.timestamp
              return {
                type: ServerEventType.STATE_SYNC,
                objectName: 'thread',
                mutationType: 'update',
                objectIDs: {},
                entries: [thread],
              }
            }
            case 'remove': {
              return {
                type: ServerEventType.STATE_SYNC,
                objectName: 'thread',
                mutationType: 'delete',
                objectIDs: {},
                entries: [threadID],
              }
            }
            default:
          }
        }
        texts.Sentry.captureMessage(`platform-ig: sending THREAD_MESSAGES_REFRESH for ${type} ${op} ${path}`)
        texts.log('platform-ig: sending THREAD_MESSAGES_REFRESH for', type, ev)
        return { type: ServerEventType.THREAD_MESSAGES_REFRESH, threadID }
      })
    }

    case 'realtimeDirect': {
      return (event.data as any[]).map<ServerEvent>(({ path, value }) => {
        // '/direct_v2/threads/340282366841710300949128171149686823771/items/30466241369594309139163642352107520'
        const threadID = /\/threads\/(\d+)\/?/.exec(path)?.[1]
        const { sender_id: participantID, ttl, activity_status } = JSON.parse(value)
        if (activity_status === 1) return { type: ServerEventType.USER_ACTIVITY, activityType: ActivityType.TYPING, threadID, participantID, durationMs: ttl }
        return { type: ServerEventType.USER_ACTIVITY, activityType: ActivityType.NONE, threadID, participantID }
      })
    }

    case 'presence': {
      const ev = event.presence_event
      return [{
        type: ServerEventType.USER_PRESENCE_UPDATED,
        presence: {
          userID: ev.user_id,
          status: ev.is_active ? 'online' : 'offline',
          lastActive: new Date(+ev.last_activity_at_ms),
        },
      }]
    }

    default:
      break
  }
}

export function mapPresence(presence: { [userID: string]: IGUser }) {
  const presenceMap: PresenceMap = {}
  Object.entries(presence).forEach(([userID, p]) => {
    if (!p.last_activity_at_ms && !p.is_active) return
    presenceMap[userID] = {
      userID,
      status: p.is_active ? 'online' : 'offline',
      lastActive: new Date(+p.last_activity_at_ms),
    }
  })
  return presenceMap
}
