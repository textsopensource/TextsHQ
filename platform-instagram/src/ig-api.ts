import bluebird from 'bluebird'
import crypto from 'crypto'
import { IgResponseError } from '@textshq/core'
import { PublishService } from '@textshq/android/dist/services/publish.service'
import type { AndroidIgpapi, DirectInboxFeedResponse, DirectRepositoryCreateGroupThreadResponseRootObject, DirectRepositoryGetPresenceResponseRootObject, DirectRepositoryRankedRecipientsResponseRootObject, DirectThreadBroadcastOptions, DirectThreadBroadcastPhotoOptions, DirectThreadBroadcastVideoOptions, DirectThreadBroadcastVoiceOptions, DirectThreadFeedResponse, DirectThreadRepositoryAddUserResponseRootObject, DirectThreadRepositoryBroadcastResponseRootObject, DirectThreadRepositoryUpdateTitleResponseRootObject, StatusResponse } from '@textshq/android'

import { genClientContext } from './util'

const THREAD_LIMIT = 100
const MSG_LIMIT = 10

const enum DeviceType {
  // extracted from website source
  ANDROID_LITE_FCM = 'android_lite_fcm',
  ANDROID_LITE_GCM = 'android_lite_gcm',
  WEB = 'web',
  WEB_ENCRYPTED = 'web_encrypted',
  WEB_VAPID = 'web_vapid',

  // android source
  ANDROID_ADM = 'android_adm',
  ANDROID_GCM = 'android_gcm',
  ANDROID_MQTT = 'android_mqtt',
  ANDROID_NOKIA = 'android_nokia',
  ANDROID_FCM = 'android_fcm',
  MSYS = 'msys',
  // LOCAL = 'local',
}

// functions forked from
// * igpapi/android/src/feeds/direct-inbox.feed.ts
// * igpapi/android/src/entities/direct-thread.entity.ts
// * igpapi/android/src/repositories/direct.repository.ts
// * igpapi/android/src/repositories/direct-thread.repository.ts
export default class IgAPI {
  constructor(private readonly ig: AndroidIgpapi) { }

  getThreads(seqId: number, inboxType: string, cursor: string, direction: 'after' | 'before') {
    return this.ig.http.body<DirectInboxFeedResponse>({
      url: `/api/v1/direct_v2/${inboxType}/`,
      searchParams: {
        visual_message_return_type: 'unseen',
        ...(cursor ? { cursor } : {}),
        // 'newer' likely doesnt work
        direction: direction === 'after' ? 'newer' : 'older',
        ...(seqId ? { seq_id: seqId } : {}),
        thread_message_limit: MSG_LIMIT,
        persistentBadging: true,
        limit: THREAD_LIMIT,
      },
    })
  }

  getThread(seqId: number, threadID: string) {
    return this.ig.http.body<DirectThreadFeedResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/`,
      searchParams: {
        visual_message_return_type: 'unseen',
        direction: 'older',
        ...(seqId ? { seq_id: seqId } : {}),
        limit: MSG_LIMIT,
      },
    })
  }

  getMessages(seqId: number, threadID: string, cursor: string, direction: 'after' | 'before') {
    return this.ig.http.body<DirectThreadFeedResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/`,
      searchParams: {
        visual_message_return_type: 'unseen',
        ...(cursor ? { cursor } : {}),
        // 'newer' likely doesnt work
        direction: direction === 'after' ? 'newer' : 'older',
        ...(seqId ? { seq_id: seqId } : {}),
        limit: MSG_LIMIT,
      },
    })
  }

  _broadcast(mutationToken: string, params: DirectThreadBroadcastOptions): Promise<DirectThreadRepositoryBroadcastResponseRootObject> {
    const recipients = params.threadIds || params.userIds
    const recipientsType = params.threadIds ? 'thread_ids' : 'recipient_users'
    const recipientsIds = recipients instanceof Array ? recipients : [recipients]

    // mutation_token and client_context are same
    // unknown if offline_threading_id is too
    const form = {
      action: 'send_item',
      send_attribution: 'inbox',
      [recipientsType]: JSON.stringify(recipientsType === 'thread_ids' ? recipientsIds : [recipientsIds]),
      client_context: mutationToken,
      _csrftoken: this.ig.state.cookies.csrfToken,
      device_id: this.ig.state.device.id,
      mutation_token: mutationToken,
      _uuid: this.ig.state.device.uuid,
      offline_threading_id: mutationToken,
      ...params.form,
    }

    return this.ig.http.body<DirectThreadRepositoryBroadcastResponseRootObject>({
      url: `/api/v1/direct_v2/threads/broadcast/${params.item}/`,
      method: 'POST',
      form: params.signed ? this.ig.http.sign(form) : form,
      searchParams: params.searchParams,
    })
  }

  async broadcast(mutationToken = genClientContext().toString(), threadID: string, options: Partial<DirectThreadBroadcastOptions>, userIDs: string[] = null) {
    if (threadID === null && userIDs === null) {
      throw new Error('No recipients set')
    }
    const baseParams = options
    const params = Object.assign(baseParams, threadID ? { threadIds: threadID } : { userIds: userIDs })
    return this._broadcast(mutationToken, params as DirectThreadBroadcastOptions)
  }

  async broadcastPhoto(mutationToken: string, threadID: string, options: DirectThreadBroadcastPhotoOptions) {
    const { upload_id } = await this.ig.upload.photo({
      uploadId: options.uploadId,
      file: options.file,
    })
    return this.broadcast(mutationToken, threadID, {
      item: 'configure_photo',
      form: {
        allow_full_aspect_ratio: options.allowFullAspectRatio || true,
        upload_id,
      },
    })
  }

  async broadcastVideo(mutationToken: string, threadID: string, options: DirectThreadBroadcastVideoOptions) {
    const uploadId = options.uploadId || Date.now().toString()
    const videoInfo = PublishService.getVideoInfo(options.video)
    await this.ig.upload.video({
      video: options.video,
      uploadId,
      isDirect: true,
      ...videoInfo,
    })

    await bluebird.try(() =>
      this.ig.media.uploadFinish({
        upload_id: uploadId,
        source_type: '2',
        video: { length: videoInfo.duration / 1000.0 },
      })).catch(IgResponseError, PublishService.catchTranscodeError(videoInfo, options.transcodeDelay || 4 * 1000))

    return this.broadcast(mutationToken, threadID, {
      item: 'configure_video',
      form: {
        video_result: '',
        upload_id: uploadId,
        sampled: typeof options.sampled !== 'undefined' ? options.sampled : true,
      },
    })
  }

  async broadcastVoice(mutationToken: string, threadID: string, options: DirectThreadBroadcastVoiceOptions) {
    const duration = PublishService.getMP4Duration(options.file)
    const uploadId = options.uploadId || Date.now().toString()
    await this.ig.upload.video({
      duration,
      video: options.file,
      uploadId,
      isDirectVoice: true,
      mediaType: '11',
    })

    await bluebird.try(() =>
      this.ig.media.uploadFinish({
        upload_id: uploadId,
        source_type: '4',
      })).catch(IgResponseError, PublishService.catchTranscodeError({ duration }, options.transcodeDelay || 4 * 1000))

    return this.broadcast(mutationToken, threadID, {
      item: 'share_voice',
      form: {
        // create a nice sine wave if the waveform is not provided
        waveform: JSON.stringify(
          options.waveform || Array.from(Array(20), (_, i) => Math.sin(i * (Math.PI / 10)) * 0.5 + 0.5),
        ),
        waveform_sampling_frequency_hz: options.waveformSamplingFrequencyHz || '10',
        upload_id: uploadId,
      },
    })
  }

  deleteItem(threadID: string | number, itemId: string | number) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/items/${itemId}/delete/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
      },
    })
  }

  markItemSeen(threadID: string, threadItemId: string) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/items/${threadItemId}/seen/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        use_unified_inbox: true,
        action: 'mark_seen',
        thread_id: threadID,
        item_id: threadItemId,
      },
    })
  }

  updateTitle(threadID: string | number, title: string) {
    return this.ig.http.body<DirectThreadRepositoryUpdateTitleResponseRootObject>({
      url: `/api/v1/direct_v2/threads/${threadID}/update_title/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        title,
      },
    })
  }

  muteThread(threadID: string | number, mute: boolean): Promise<StatusResponse> {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/${mute ? 'mute' : 'unmute'}/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
      },
    })
  }

  addUser(threadID: string | number, userIds: string[] | number[]) {
    return this.ig.http.body<DirectThreadRepositoryAddUserResponseRootObject>({
      url: `/api/v1/direct_v2/threads/${threadID}/add_user/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        user_ids: JSON.stringify(userIds),
      },
    })
  }

  removeUser(threadID: string, participantID: string) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/remove_users/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        user_ids: JSON.stringify([participantID]),
      },
    })
  }

  changeRole(threadID: string, participantID: string, role: string) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/${role === 'admin' ? 'add_admins' : 'remove_admins'}/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        user_ids: JSON.stringify([participantID]),
      },
    })
  }

  leave(threadID: string) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/leave/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
      },
    })
  }

  hide(threadID: string) {
    return this.ig.http.body<StatusResponse>({
      url: `/api/v1/direct_v2/threads/${threadID}/hide/`,
      method: 'POST',
      form: {
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
      },
    })
  }

  rankedRecipients(query: string) {
    return this.ig.http.body<DirectRepositoryRankedRecipientsResponseRootObject>({
      url: '/api/v1/direct_v2/ranked_recipients/',
      method: 'GET',
      searchParams: {
        mode: 'reshare',
        query,
        show_threads: false,
      },
    })
  }

  createGroupThread(recipientUsers: string[], threadTitle: string) {
    return this.ig.http.body<DirectRepositoryCreateGroupThreadResponseRootObject>({
      url: '/api/v1/direct_v2/create_group_thread/',
      method: 'POST',
      form: this.ig.http.sign({
        _csrftoken: this.ig.state.cookies.csrfToken,
        _uuid: this.ig.state.device.uuid,
        _uid: this.ig.state.cookies.userId,
        recipient_users: JSON.stringify(recipientUsers),
        thread_title: threadTitle,
      }),
    })
  }

  getPresence() {
    return this.ig.http.body<DirectRepositoryGetPresenceResponseRootObject>({
      url: '/api/v1/direct_v2/get_presence/',
      method: 'GET',
    })
  }

  register(token: string): Promise<StatusResponse> {
    return this.ig.http.body<StatusResponse>({
      url: '/api/v1/push/register/',
      method: 'POST',
      form: {
        // android:
        device_type: DeviceType.ANDROID_FCM,
        device_sub_type: 0,
        is_main_push_channel: true, // unknown what this does, maybe used for setting a primary when using multiple push deliveries (android_mqtt with true and android_fcm with false)
        device_token: token,
        family_device_id: crypto.randomUUID(),

        // web:
        // device_type: DeviceType.WEB_VAPID,
        // device_token: endpoint,
        // mid,
        // subscription_keys: JSON.stringify({ p256dh, auth }),

        // _csrftoken: this.ig.state.cookies.csrfToken,
        guid: this.ig.state.device.uuid,
        _uuid: this.ig.state.device.uuid,
        users: this.ig.state.cookies.userId,
      },
    })
  }
}
