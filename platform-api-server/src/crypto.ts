import crypto from 'crypto'
import util from 'util'

export const md5 = (str: string) =>
  crypto.createHash('md5').update(str).digest('hex')

export const randomBytes = util.promisify(crypto.randomBytes)
