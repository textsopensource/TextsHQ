import getIndexerPlugin from './indexer-plugin'
import getErrorCheckerPlugin from './error-checker-plugin'
import mapMissingPlugin from './map-missing-plugin'

import type { Plugin } from './types'
import type PlatformAPIServer from './PlatformAPIServer'

export default class PluginsStore {
  readonly outputHooks: { [methodName: string]: Set<Function> } = {}

  readonly inputHooks: { [methodName: string]: Set<Function> } = {}

  constructor(pas: PlatformAPIServer) {
    const allPlugins: Plugin[] = [
      ...pas.options.plugins,
      mapMissingPlugin,
    ]

    if (pas.options.developerMode) {
      allPlugins.push(getErrorCheckerPlugin(pas.options.platformInfoMap))
    }
    if (pas.indexDB) {
      allPlugins.push(getIndexerPlugin(pas.indexDB, pas.options.platformInfoMap))
    }

    allPlugins.forEach(plugin => {
      const map = plugin.type === 'output' ? this.outputHooks : this.inputHooks
      Object.keys(plugin.hooks).forEach(methodName => {
        if (!map[methodName]) map[methodName] = new Set()
        map[methodName].add(plugin.hooks[methodName])
      })
    })
  }
}
