import type { TextEntity } from '@textshq/platform-sdk'

export function getCodeEntities(messageText: string): TextEntity[] {
  const toScalarIndex = (idx: number) => Array.from(messageText.substring(0, idx)).length

  const regex = /```([a-z0-9A-Z]+)\n([^]+?)\n```/g
  const codeEntities: TextEntity[] = []
  const matches = messageText.matchAll(regex)
  for (const match of matches) {
    const from = toScalarIndex(match.index)
    const to = toScalarIndex(match.index + match[0].length)
    const codeLanguage = match[1]
    codeEntities.push({
      from,
      to,
      code: true,
      pre: true,
      codeLanguage,
    })
    codeEntities.push({
      from,
      to: from + codeLanguage.length + 4,
      replaceWith: '',
    })
    codeEntities.push({
      from: to - 3,
      to,
      replaceWith: '',
    })
  }
  return codeEntities
}
