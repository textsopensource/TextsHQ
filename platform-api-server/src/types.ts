import type http from 'http'
import type { PlatformInfo, PlatformAPI, MessageContent, MessageSendOptions, Awaitable, CurrentUser, SerializedSession } from '@textshq/platform-sdk'

import type PlatformAPIServer from './PlatformAPIServer'
import type { AccountState } from './enums'

export type SerializersMap = Record<string, TransportSerializer>

export type PlatformInfoMap = Record<string, PlatformInfo>

export type LoadPlatformAPIMap = Record<string, () => PlatformAPI>

export type PushFunction = (data: any) => void

export type GenericRoute = (deserialized: any, push: PushFunction) => Promise<TransportResult> | TransportResult

export type HTTPRoute = (req: http.IncomingMessage, res: http.ServerResponse, push?: PushFunction) => void

export type HTTPRoutes = { [routeName: string]: http.RequestListener }

export type Plugin = {
  type: 'output' | 'input'
  hooks: { [methodName: string]: (platformName: string, accountID: string, args: any[], result?: any) => Awaitable<any> }
}

export type TransportOptions = Readonly<{
  /** a cryptographically random string used to secure routes */
  nonce: string
  httpSerializerName: string
  port: number
  hostname?: string

  proxyFileURLs?: boolean

  extraHTTPRoutes?: HTTPRoutes
  extraGenericRoutes?: Record<string, GenericRoute>
}>

export type EncryptionKeyName = 'db'

export type PASOptions = Readonly<{
  developerMode?: boolean

  transportOptions: TransportOptions

  indexingDisabled: boolean
  dataDirPath: string

  loadPlatformAPIMap: LoadPlatformAPIMap
  platformInfoMap: PlatformInfoMap
  serializers?: SerializersMap

  plugins?: Plugin[]

  getEncryptionKey?: (keyName: EncryptionKeyName) => Awaitable<string>
  beforeInit?: (pas: PlatformAPIServer) => Awaitable<void>
}>

export type PlatformAPIRelayerData = {
  platformName?: string
  accountID: string
  methodName: keyof PlatformAPI
  args?: any[]
}

export type ErrorTransportResult = {
  errorName: string
  errorMessage: string
  errorStack?: string
}
export type TransportResult = { result: any } | ErrorTransportResult

export type TransportSerializer = {
  name: string
  encode: (data: any) => any
  decode: (data: any) => any
}

export type SnoozeOptions = {
  note?: string
  remindOnlyOnNoResponse: boolean
}

export type SnoozedMessage = {
  accountID: string
  threadID: string
  messageID: string
  messageTimestamp: Date
  addedOnTimestamp: Date
  remindOnTimestamp: Date
  options: SnoozeOptions
}
export type SnoozedThread = {
  accountID: string
  threadID: string
  addedOnTimestamp: Date
  remindOnTimestamp: Date
  options: SnoozeOptions
}

export type MessageSendArgs = {
  msgContent: MessageContent
  msgOptions: MessageSendOptions
}

export type ScheduleOptions = {
  addedOn: Date
  sendOn: Date
  messageSendArgs: MessageSendArgs
  sendOnlyOnNoResponse: boolean
  canceled?: boolean
}

export type ScheduledMessage = {
  accountID: string
  threadID: string
  pendingMessageID: string
  options: ScheduleOptions
}

export type FullMessageID = { accountID: string, threadID: string, messageID: string }
export type StarredMessage = { accountID: string, threadID: string, messageID: string, starredOn: Date }

export type KeyValueKey =
  'accountProps' |
  'accounts.backup' | // deprecated
  'accounts' | // deprecated
  'labels' |
  'lastLoginHint' |
  'loggedInUser' |
  'prefs' |
  'remote.backup' |
  'remote' |
  'tabs'

export type AccountProps = {
  isMuted?: boolean
}

export type Account = {
  id: string
  platformName: string
  state: AccountState
  user: CurrentUser | null
  session: SerializedSession | null
  props: AccountProps | null
}
