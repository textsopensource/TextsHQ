import { Message, Paginated, Thread, ServerEvent, ServerEventType, PaginationArg, Attribute, User } from '@textshq/platform-sdk'

import { IGNORE_RESULT_SYMBOL } from './hook-constants'
import type IndexDatabase from './IndexDatabase'
import type { Plugin, PlatformInfoMap } from './types'

const getIndexerPlugin = (indexDB: IndexDatabase, platformInfoMap: PlatformInfoMap): Plugin => {
  const shouldCache = (platformName: string) =>
    !platformInfoMap[platformName]?.attributes.has(Attribute.NO_CACHE)

  const hooks = {
    getThreads(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Thread>) {
      const canCache = shouldCache(platformName)
      if (canCache) {
        result.items.forEach(t => {
          indexDB.cacheThread(platformName, accountID, t)
          indexDB.cacheMessages(platformName, t.id, accountID, t.messages.items)
          t.participants.items.forEach(p => {
            indexDB.cacheUser(platformName, accountID, p)
          })
        })
      }
      return IGNORE_RESULT_SYMBOL
    },

    getMessages(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Message>) {
      const canCache = shouldCache(platformName)
      if (canCache) {
        const threadID = args[0]
        indexDB.cacheMessages(platformName, threadID, accountID, result.items)
      }
      return IGNORE_RESULT_SYMBOL
    },

    getThread(platformName: string, accountID: string, _: [string], thread: Thread) {
      const canCache = shouldCache(platformName)
      if (canCache) {
        if (typeof thread === 'object') {
          indexDB.cacheThread(platformName, accountID, thread)
          indexDB.cacheMessages(platformName, thread.id, accountID, thread.messages.items)
          thread.participants.items.forEach(p => {
            indexDB.cacheUser(platformName, accountID, p)
          })
        }
      }
      return IGNORE_RESULT_SYMBOL
    },

    getMessage(platformName: string, accountID: string, args: [string, string], message: Message) {
      const canCache = shouldCache(platformName)
      if (canCache) {
        if (typeof message === 'object') {
          const threadID = args[0]
          indexDB.cacheMessages(platformName, threadID, accountID, [message])
        }
      }
      return IGNORE_RESULT_SYMBOL
    },

    // deleteMessage(platformName: string, accountID: string, args: [string, string]) {
    //   indexDB.deleteCachedMessage(accountID, args[0], args[1])
    //   return IGNORE_RESULT_SYMBOL
    // },

    deleteThread(platformName: string, accountID: string, args: [string]) {
      // todo: only delete on server side success
      indexDB.deleteCachedThread(accountID, args[0])
      return IGNORE_RESULT_SYMBOL
    },

    sendMessage(platformName: string, accountID: string, args: [string], result: boolean | Message[]) {
      const threadID = args[0]
      if (Array.isArray(result)) {
        indexDB.cacheMessages(platformName, threadID, accountID, result)
      }
      return IGNORE_RESULT_SYMBOL
    },

    searchMessages(platformName: string, accountID: string, args: [string, PaginationArg, string], result: Paginated<Message>) {
      const supportsNatively = platformInfoMap[platformName].attributes.has(Attribute.SUPPORTS_SEARCH)
      if (supportsNatively) return IGNORE_RESULT_SYMBOL
      return indexDB.searchMessages(accountID, ...args)
    },

    createThread(platformName: string, accountID: string, args: [string[], string], result: boolean | Thread) {
      const canCache = shouldCache(platformName)
      if (canCache && typeof result === 'object') indexDB.cacheThread(platformName, accountID, result)
      return IGNORE_RESULT_SYMBOL
    },

    subscribeToEvents(platformName: string, accountID: string, args: any[], events: ServerEvent[]) {
      const canCache = shouldCache(platformName)
      if (canCache) {
        events.forEach(event => {
          if (event.type !== ServerEventType.STATE_SYNC) return
          switch (event.objectName) {
            case 'thread': {
              switch (event.mutationType) {
                case 'upsert': {
                  event.entries.forEach(entry => {
                    indexDB.cacheThread(platformName, accountID, entry as Thread)
                  })
                  break
                }
                case 'update': {
                  event.entries.forEach(update => {
                    indexDB.updateCachedThread(accountID, update.id, update as Partial<Thread>)
                  })
                  break
                }
                case 'delete': {
                  event.entries.forEach(threadID => {
                    indexDB.deleteCachedThread(accountID, threadID)
                  })
                  break
                }
              }
              break
            }
            case 'message': {
              switch (event.mutationType) {
                case 'upsert': {
                  indexDB.cacheMessages(platformName, event.objectIDs.threadID, accountID, event.entries as Message[])
                  break
                }
                case 'update': {
                  event.entries.forEach(update => {
                    indexDB.updateCachedMessage(accountID, update.id, update as Partial<Message>)
                  })
                  break
                }
                case 'delete': {
                  event.entries.forEach(messageID => {
                    indexDB.deleteCachedMessage(accountID, event.objectIDs.threadID, messageID)
                  })
                  break
                }
              }
              break
            }
            case 'participant': {
              switch (event.mutationType) {
                case 'upsert': {
                  event.entries.forEach(entry => {
                    indexDB.cacheUser(platformName, accountID, entry as User)
                  })
                  break
                }
                case 'update': {
                  event.entries.forEach(update => {
                    indexDB.updateCachedUser(accountID, update.id, update as Partial<User>)
                  })
                  break
                }
              }
              break
            }
          }
        })
      }
      return IGNORE_RESULT_SYMBOL
    },

    logout(platformName: string, accountID: string, args: any[], result: void) {
      indexDB.deleteAccountData(accountID)
      return IGNORE_RESULT_SYMBOL
    },
  }

  return {
    type: 'output',
    hooks,
  }
}

export default getIndexerPlugin
