/* eslint-disable no-param-reassign */
import { promises as fs } from 'fs'
import type { Readable } from 'stream'
import type { Message, Thread } from '@textshq/platform-sdk'

// converts date strings to Date objects
export function jsonToMessage(msg: Message) {
  msg.timestamp = new Date(msg.timestamp)
  if (msg.editedTimestamp) msg.editedTimestamp = new Date(msg.editedTimestamp)
  if (typeof msg.seen === 'string') {
    msg.seen = new Date(msg.seen)
  } else if (typeof msg.seen === 'object') {
    for (const [pid, seenAt] of Object.entries(msg.seen)) {
      msg.seen[pid] = new Date(seenAt)
    }
  }
  msg.tweets?.forEach(tweet => {
    if (tweet.timestamp) tweet.timestamp = new Date(tweet.timestamp)
  })
  return msg
}

export function jsonToThread(thread: Thread, lastMessage?: Message): Thread {
  if (thread._original) thread._original = undefined
  if (thread.timestamp) thread.timestamp = new Date(thread.timestamp)
  thread.messages = {
    items: lastMessage ? [jsonToMessage(lastMessage)] : [],
    hasMore: true,
  }
  return thread
}

export const isReadableStream = (stream: any): stream is Readable =>
  stream !== null
    && typeof stream === 'object'
    && stream.readable !== false
    && typeof stream._read === 'function'

export const pathExists = (fp: string) =>
  fs.access(fp)
    .then(() => true)
    .catch(() => false)
