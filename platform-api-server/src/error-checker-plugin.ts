import chalk from 'chalk'
import util from 'util'
import cp from 'child_process'
import { PaginationArg, Paginated, Thread, Message, ServerEvent, ServerEventType, Attribute, Participant } from '@textshq/platform-sdk'

import { IGNORE_RESULT_SYMBOL } from './hook-constants'
import type { PlatformInfoMap, Plugin } from './types'

// TODO: all the type checking here can be more strictly done with a library that generates automated type-checking code for TS types

type Context = {
  platformName?: string
  methodName?: string
  accountID?: string
  threadID?: string
  mutationType?: string
  objectName?: string
}

function macOSNotify(title: string, subtitle: string, body: string) {
  if (process.platform !== 'darwin') return
  cp.spawn('osascript', ['-e', `display notification ${JSON.stringify(body)} with title ${JSON.stringify(title)} subtitle ${JSON.stringify(subtitle)}`], { detached: true })
}

const DONT_NOTIFY = !!process.env.DONT_NOTIFY

function reportError(context: Context, throwErr = false, title: string, ...args: any[]) {
  const msg = util.format(context, title, ...args)
  console.log(chalk.red(msg))
  if (!DONT_NOTIFY) macOSNotify('Texts Error Checker', title, `${context.platformName} ${context.methodName}`)
  if (throwErr) throw Error(`error checker: ${msg}`)
}

function checkDuplicateItems<T extends { id: string }>(items: T[], itemName: string, context: Context) {
  const ids = new Set<string>()
  items?.forEach(item => {
    if (ids.has(item.id)) {
      reportError(context, false, `found duplicate ${itemName}`, items.map(m => m.id))
    }
    ids.add(item.id)
  })
}

function checkMessageSort(messages: Message[], context: Context) {
  const messageSortIncorrect = messages.some((message, index) => {
    const prevMessage = messages[index - 1]
    if (!prevMessage) return
    const result = message.timestamp < prevMessage.timestamp
    if (result) {
      console.log(message.timestamp, prevMessage.timestamp, message.text, prevMessage.text)
    }
    return result
  })
  if (messageSortIncorrect) {
    reportError(context, false, 'wrong message sort', messages.map(m => [m.timestamp, m.text]))
  }
}

function checkThreadSort(threads: Thread[], context: Context) {
  const threadSortIncorrect = threads.some((thread, index) => {
    const prevMessage = threads[index - 1]
    if (!prevMessage) return
    const result = thread.timestamp > prevMessage.timestamp
    if (result) {
      console.log(thread.timestamp, prevMessage.timestamp, thread.title, prevMessage.title)
    }
    return result
  })
  if (threadSortIncorrect) {
    reportError(context, false, 'wrong thread sort', threads.map(m => [m.timestamp, m.title]))
  }
}

function checkThreadTypes(thread: Thread, context: Context) {
  if (!thread.id) reportError(context, true, `invalid thread.id ${thread.id}`)
  if (thread.timestamp) {
    if (!(thread.timestamp instanceof Date) || isNaN(thread.timestamp.getTime())) reportError(context, true, `invalid thread.timestamp ${thread.timestamp}`)
  }
}

export default function getErrorCheckerPlugin(platformInfoMap: PlatformInfoMap) {
  const definesMessageCursor = (platformName: string) =>
    platformInfoMap[platformName]?.attributes.has(Attribute.DEFINES_MESSAGE_CURSOR)

  function checkMessagesTypes(messages: Message[] | Partial<Message[]>, context: Context) {
    if (!messages && context.methodName !== 'searchMessages') {
      reportError(context, true, 'messages is falsey')
    }
    messages?.forEach((message, index) => {
      if (!message) {
        reportError(context, true, `messages[${index}] is falsey`)
      }
      if (!message.id) {
        reportError(context, true, `invalid messages[${index}].id: ${message.id}`)
      }
      if (definesMessageCursor(context.platformName) && !(context.objectName === 'message' && context.mutationType === 'update') && !message.cursor) {
        reportError(context, true, `platform attributes include \`Attribute.DEFINES_MESSAGE_CURSOR\`, invalid messages[${index}].cursor: ${message.cursor}`)
      }
      if ('timestamp' in message) {
        if (!(message.timestamp instanceof Date) || isNaN(message.timestamp.getTime())) {
          reportError(context, true, `invalid message.timestamp ${message.id} ${message.timestamp}`, message)
        }
      }
      message.attachments?.forEach((attachment, attIndex) => {
        if (!attachment.id) {
          reportError(context, false, `falsey messages[${index}].attachments[${attIndex}] id: ${attachment.id}`)
        }
        if (typeof attachment.id !== 'string') {
          reportError(context, false, `invalid messages[${index}].attachments[${attIndex}] id: ${typeof attachment.id}`)
        }
        if (attachment.size?.height === 0 || attachment.size?.width === 0) {
          reportError(context, false, `invalid messages[${index}].attachments[${attIndex}] size: ${attachment.size.width}x${attachment.size.height}`, attachment.id)
        }
      })
    })
  }

  function checkParticipantTypes(participants: Participant[] | Partial<Participant[]>, context: Context) {
    if (!participants && context.methodName !== 'searchMessages') {
      reportError(context, true, 'participants is falsey')
    }
    participants?.forEach((user, index) => {
      if (!user) {
        reportError(context, true, `participants[${index}] is falsey`)
      }
      if (!user.id) {
        reportError(context, true, `invalid participants[${index}].id: ${user.id}`)
      }
    })
  }

  function checkThread(context: Context, thread: Thread) {
    checkThreadTypes(thread, context)
    checkMessagesTypes(thread.messages.items, context)
    checkParticipantTypes(thread.participants.items, context)
    checkMessageSort(thread.messages.items, context)
    checkDuplicateItems(thread.messages.items, 'message', context)
    checkDuplicateItems(thread.participants.items, 'participant', context)
  }

  const errorCheckerPlugin: Plugin = {
    type: 'output',
    hooks: {
      getThread(platformName: string, accountID: string, args: [string], result: Thread) {
        if (typeof result === 'object') {
          const thread = result
          const context: Context = { methodName: 'getThread', platformName, accountID, threadID: result.id }
          checkThread(context, thread)
        }
        return IGNORE_RESULT_SYMBOL
      },
      getThreads(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Thread>) {
        const threads = result.items
        threads.forEach(thread => {
          const context: Context = { methodName: 'getThreads', platformName, accountID, threadID: thread.id }
          checkThread(context, thread)
        })
        const context = { methodName: 'getThreads', platformName, accountID }
        // checkThreadSort(threads, context)
        checkDuplicateItems(threads, 'thread', context)
        if (result.hasMore && result.oldestCursor == null) {
          reportError(context, true, 'oldest cursor is null while hasMore is true')
        }
        return IGNORE_RESULT_SYMBOL
      },
      getMessages(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Message>) {
        const [, pagination] = args
        const context: Context = { methodName: 'getMessages', platformName, accountID, threadID: args[0] }
        checkDuplicateItems(result.items, 'message', context)
        checkMessageSort(result.items, context)
        checkMessagesTypes(result.items, context)
        if (pagination?.cursor && result.items.some(m => pagination.cursor === (m.cursor || m.id))) {
          reportError(context, false, 'cursor item is present in result')
        }
        return IGNORE_RESULT_SYMBOL
      },
      searchMessages(platformName: string, accountID: string, args: [string, PaginationArg, string], result: Paginated<Message>) {
        if (result) {
          const context: Context = { methodName: 'searchMessages', platformName, accountID, threadID: args[0] }
          checkDuplicateItems(result.items, 'message', context)
          checkMessagesTypes(result.items, context)
        }
        return IGNORE_RESULT_SYMBOL
      },
      subscribeToEvents(platformName: string, accountID: string, args: any[], events: ServerEvent[]) {
        const context: Context = { platformName, accountID, methodName: 'subscribeToEvents' }
        if (!events) {
          reportError(context, true, '`events` is falsey', 'subscribeToEvents')
          return IGNORE_RESULT_SYMBOL
        }
        events.forEach((se, seIndex) => {
          if (!se) {
            reportError(context, true, `event ${seIndex} is falsey`, 'subscribeToEvents')
          }
          if (se.type !== ServerEventType.STATE_SYNC) return
          if (se.mutationType === 'delete-all') return
          if (se.mutationType === 'delete') {
            if (!Array.isArray(se.entries)) {
              reportError(context, true, `invalid StateSyncEvent.entries[${seIndex}] for mutationType = 'delete'\nshould be an array:`, se.entries)
            }
            if (!se.entries.every(i => typeof i === 'string')) {
              reportError(context, true, `invalid StateSyncEvent.entries[${seIndex}] for mutationType = 'delete'\nshould be a string array:`, se.entries)
            }
            return
          }
          se.entries.forEach((entry, entryIndex) => {
            if (!entry.id) reportError(context, true, `invalid StateSyncEvent.entries[${entryIndex}].id:`, entry)
          })
          switch (se.objectName) {
            case 'message': {
              const newContext: Context = { ...context, threadID: se.objectIDs.threadID, mutationType: se.mutationType, objectName: se.objectName }
              checkMessagesTypes(se.entries as Message[], newContext)
              checkDuplicateItems(se.entries, 'message', newContext)
              break
            }
            case 'thread': {
              se.entries.forEach(entry => {
                const thread = entry as Thread
                const newContext: Context = { ...context, threadID: thread.id, mutationType: se.mutationType, objectName: se.objectName }
                if (se.objectIDs.threadID) {
                  reportError(newContext, false, `invalid StateSyncEvent.entries[${seIndex}]: objectIDs.threadID must not be set`)
                }
                if ('participants' in thread) {
                  checkParticipantTypes(thread.participants.items, newContext)
                  checkDuplicateItems(thread.participants.items, 'participant', newContext)
                }
                if ('messages' in thread) {
                  const messages = thread.messages.items
                  checkMessagesTypes(messages, newContext)
                  checkMessageSort(messages, newContext)
                  checkDuplicateItems(messages, 'message', newContext)
                }
                checkThreadTypes(thread, newContext)
              })
              break
            }
            case 'participant': {
              const newContext: Context = { ...context, threadID: se.objectIDs.threadID, mutationType: se.mutationType, objectName: se.objectName }
              checkDuplicateItems(se.entries, 'participant', newContext)
              break
            }
          }
        })
        return IGNORE_RESULT_SYMBOL
      },
    },
  }
  return errorCheckerPlugin
}
