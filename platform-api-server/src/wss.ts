import bluebird from 'bluebird'
import WebSocket from 'ws'
import crypto from 'crypto'
import type http from 'http'

import { trackEvent } from './telemetry'
import IS_DEV_ENVIRON from './is-dev-environ'
import type { TransportSerializer, SerializersMap } from './types'

export default class WebSocketServer {
  private wss: WebSocket.Server

  private closing = false

  private serializer: TransportSerializer

  constructor(
    private readonly allSerializers: Readonly<SerializersMap>,
    private readonly server: http.Server,
    private readonly transportNonce: string,
    private readonly onMessage?: (data: any) => void,
  ) {}

  create() {
    console.log('wss create')
    this.wss = new WebSocket.Server({
      server: this.server,
      maxPayload: 0,
    })

    this.wss
      .on('close', () => {
        console.log('wss close')
        if (this.closing) return
        setTimeout(() => this.create(), 50)
      })
      .on('error', err => {
        console.error('wss error', err)
        setTimeout(
          () => this.create(),
          (err as any)?.code === 'EADDRINUSE' ? 1000 : 50,
        )
      })
      .on('connection', (ws, request) => {
        const [, nonce, serializerName] = request.url.split('/')
        if (nonce.length !== this.transportNonce.length || !crypto.timingSafeEqual(Buffer.from(nonce), Buffer.from(this.transportNonce))) {
          trackEvent('WS Error', 'Dropped connection – ' + request.url)
          ws.close()
          return
        }
        if (IS_DEV_ENVIRON) console.log('ws connection', request.url)
        this.serializer = this.allSerializers[serializerName]
        if (!this.serializer) {
          console.log('invalid serializer', serializerName)
          ws.close()
          return
        }
        if (!this.onMessage) return
        ws.on('message', data => {
          const deserialize = this.serializer.decode(data)
          this.onMessage(deserialize)
        })
      })
  }

  private async waitAndSend(data: any) {
    while (this.wss.clients.size === 0) {
      await bluebird.delay(5)
    }
    this.send(data)
  }

  send = (data: any) => {
    if (this.wss.clients.size === 0) return this.waitAndSend(data)
    if (this.wss.clients.size !== 1) {
      trackEvent('WS', 'More than one connected client: ' + this.wss.clients.size)
    }
    this.wss.clients.forEach(ws => {
      ws.send(this.serializer.encode(data))
    })
  }

  shutdown() {
    console.log('wss shutdown')
    this.closing = true
    this.wss?.close()
  }
}
