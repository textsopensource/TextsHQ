import http from 'http'

export default class HttpServer {
  server: http.Server

  constructor(
    private readonly port: number,
    private readonly hostname: string,
    onRequest: http.RequestListener,
  ) {
    this.server = http.createServer(onRequest)
  }

  listen() {
    return new Promise<void>(resolve => {
      this.server.listen(this.port, this.hostname, () => {
        console.log('http listening')
        resolve()
      })
    })
  }

  shutdown() {
    console.log('http shutdown')
    this.server?.close()
  }
}
