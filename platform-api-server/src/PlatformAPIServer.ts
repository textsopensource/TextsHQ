import crypto from 'crypto'
import type http from 'http'

import { trackEvent } from './telemetry'
import HttpServer from './http'
import WebSocketServer from './wss'
import getGenericRoutes from './generic-routes'
import getHTTPRoutes from './http-routes'
import IndexDatabase from './IndexDatabase'
import PlatformAPIStore from './PlatformAPIStore'
import v8 from './serializers/v8'
import type { GenericRoute, HTTPRoute, PASOptions, PushFunction, SerializersMap, TransportOptions } from './types'

class DefaultTransports {
  private readonly genericRoutes: Record<string, GenericRoute>

  private readonly httpRoutes: Record<string, HTTPRoute>

  private readonly nonceStartBuffer: Buffer

  private readonly httpServer: HttpServer

  private readonly wss: WebSocketServer

  readonly wssSend: WebSocketServer['send']

  private readonly pushFunction: PushFunction = (data: any) =>
    this.wss.send({ type: 'callback', data })

  private readonly onWSMessage = async (message: any) => {
    const { reqID, routeName, routeData } = message
    const gr = this.genericRoutes[routeName]
    if (!gr) throw Error(`unknown routeName: ${routeName}`)
    const data = await gr(routeData, this.pushFunction)
    this.wss.send({ type: 'response', reqID, data })
  }

  private readonly onHTTPRequest: http.RequestListener = (req, res) => {
    const [, firstPart, secondPart] = req.url.split('/', 3)
    const isUnauthenticatedRoute = firstPart[0] === '_'
    if (!isUnauthenticatedRoute) {
      const nonceBuffer = Buffer.from(firstPart)
      if (nonceBuffer.length !== this.nonceStartBuffer.length || !crypto.timingSafeEqual(nonceBuffer, this.nonceStartBuffer)) {
        trackEvent('HTTP Error', `Dropped connection – ${req.url}`)
        res.end()
        return
      }
    }
    const routeName = isUnauthenticatedRoute ? firstPart : secondPart
    const route = this.httpRoutes[routeName]
    if (route) route(req, res, this.pushFunction)
    else res.end()
  }

  constructor(
    options: TransportOptions,
    allSerializers: Readonly<SerializersMap>,
    platformAPIStore: PlatformAPIStore,
    indexDB: IndexDatabase,
  ) {
    this.genericRoutes = getGenericRoutes(platformAPIStore, indexDB, options.extraGenericRoutes)
    this.httpRoutes = getHTTPRoutes(allSerializers[options.httpSerializerName], options.extraHTTPRoutes, this.genericRoutes, platformAPIStore, options.proxyFileURLs)
    this.nonceStartBuffer = Buffer.from(options.nonce)
    this.httpServer = new HttpServer(options.port, options.hostname, this.onHTTPRequest)
    this.wss = new WebSocketServer(allSerializers, this.httpServer.server, options.nonce, this.onWSMessage)
    this.wssSend = this.wss.send
  }

  init() {
    this.wss.create()
    return this.httpServer.listen()
  }

  shutdown() {
    this.httpServer.shutdown()
    this.wss.shutdown()
  }
}

export default class PlatformAPIServer {
  readonly allSerializers: Readonly<SerializersMap>

  readonly indexDB: IndexDatabase

  readonly platformAPIStore: PlatformAPIStore

  readonly defaultTransports: DefaultTransports

  readonly initPromise: Promise<void>

  constructor(readonly options: PASOptions) {
    this.allSerializers = { v8, ...options.serializers }
    // ordering matters, these 3 class props MUST exactly be in this order
    this.indexDB = options.indexingDisabled
      ? null
      : new IndexDatabase(options.dataDirPath, options.getEncryptionKey)
    this.platformAPIStore = new PlatformAPIStore(this)
    this.defaultTransports = options.transportOptions
      ? new DefaultTransports(options.transportOptions, this.allSerializers, this.platformAPIStore, this.indexDB)
      : null
    this.initPromise = this.init()
  }

  async init() {
    await this.options.beforeInit?.(this)
    await this.indexDB?.initPromise
    await this.defaultTransports?.init()
  }

  async cleanup() {
    await Promise.all([
      this.indexDB?.dispose(),
      this.platformAPIStore.disposeAll(),
      this.defaultTransports?.shutdown(),
    ])
  }
}
