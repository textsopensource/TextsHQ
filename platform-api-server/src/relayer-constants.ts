import type { PlatformAPI } from '@textshq/platform-sdk'

export const CB_METHODS: Set<keyof PlatformAPI> = new Set([
  'subscribeToEvents',
  'onLoginEvent',
  'onConnectionStateChange',
])
