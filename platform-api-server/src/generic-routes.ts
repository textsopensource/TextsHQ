import type IndexDatabase from './IndexDatabase'
import type PlatformAPIStore from './PlatformAPIStore'
import type { TransportResult, PlatformAPIRelayerData, GenericRoute } from './types'

export const serializeError = (error: Error): TransportResult => {
  if (error instanceof Error) return { errorName: error.name, errorMessage: error.message, errorStack: error.stack }
  return { errorName: `${typeof error} error`, errorMessage: JSON.stringify(error) }
}

const getGenericRoutes = (
  platformAPIStore: PlatformAPIStore,
  indexDB: IndexDatabase,
  extraGenericRoutes: Record<string, GenericRoute> = {},
): Record<string, GenericRoute> => ({
  heartbeat(deserialized): TransportResult {
    try {
      const result = JSON.stringify(deserialized)
      return { result }
    } catch (error) {
      console.error('pas: heartbeat route error', error)
      globalThis.Sentry.captureException(error)
      return serializeError(error)
    }
  },

  async platform(deserialized: PlatformAPIRelayerData, push): Promise<TransportResult> {
    try {
      const result = await platformAPIStore.callMethod(deserialized, push)
      return { result }
    } catch (error) {
      console.error('pas: platform route error', deserialized.platformName, deserialized.methodName, error)
      globalThis.Sentry.captureException(error)
      return serializeError(error)
    }
  },

  async database(deserialized): Promise<TransportResult> {
    try {
      const result = await indexDB[deserialized.methodName](...deserialized.args)
      return { result }
    } catch (error) {
      console.error('pas: database route error', deserialized.methodName, error)
      globalThis.Sentry.captureException(error)
      return serializeError(error)
    }
  },

  ...extraGenericRoutes,
})

export default getGenericRoutes
