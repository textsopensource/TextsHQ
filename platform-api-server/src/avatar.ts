import got from 'got'
import mem from 'mem'

import { md5 } from './crypto'
import IS_DEV_ENVIRON from './is-dev-environ'

// const getImgURLFromInitials = (initials: string) =>
//   (initials ? `https://ui-avatars.com/api/?size=256&name=${initials}` : undefined)

const symbols = /[\r\n%#()<>?[\\\]^`{|}]/g
export function urlEncodeSVG(_data: string) {
  const data = _data.replace(/>\s{1,}</g, '><').replace(/\s{2,}/g, ' ')
  return data.replace(symbols, encodeURIComponent)
}
export const getImgURLFromInitials = (initials: string) => {
  if (!initials) return
  const fontSize = initials.length > 2 ? 80 : 120
  return 'data:image/svg+xml,' + urlEncodeSVG(`<svg height="1em" width="1em" viewBox="0 0 256 256" xmlns="http://www.w3.org/2000/svg"><path d="m0 0h256v256h-256z" fill="#ddd"/><text text-anchor="middle" dominant-baseline="middle" fill="#222" font-family="-apple-system,BlinkMacSystemFont,sans-serif" font-size="${fontSize}" x="128" y="142">${initials}</text></svg>`)
}

const getImgURLFromEmailHash = (emailHash: string) =>
  `https://www.gravatar.com/avatar/${emailHash}.jpg?d=404&size=256`

export const getAvatarURL = mem(async (emails: string[], initials?: string) => {
  if (IS_DEV_ENVIRON) console.log('fetching avatar for', emails, initials)
  for (const email of emails) {
    const emailURL = getImgURLFromEmailHash(md5(email))
    const { statusCode } = await got.head(emailURL, { throwHttpErrors: false })
    if (statusCode === 200) return emailURL
    if (IS_DEV_ENVIRON) console.log('avatar fetch failed for', email, emailURL, statusCode)
  }
  return getImgURLFromInitials(initials || emails[0][0])
}, {
  cacheKey: JSON.stringify,
})
