import path from 'path'
import { promises as fs } from 'fs'
import { omit } from 'lodash'
import { Message, Paginated, Thread, User, PaginationArg, MessageAttachment, InboxName, Participant, texts, IAsyncSqlite } from '@textshq/platform-sdk'
import bluebird from 'bluebird'

import IS_DEV_ENVIRON from './is-dev-environ'
import { pathExists, jsonToMessage, jsonToThread } from './util'
import type { KeyValueKey, SnoozedMessage, SnoozedThread, ScheduledMessage, ScheduleOptions, FullMessageID, StarredMessage, Account, PASOptions } from './types'

const AsyncSqlite = (globalThis as any).AsyncSqlite as IAsyncSqlite

const MESSAGES_LIMIT = 20
const THREADS_LIMIT = 20

const SCHEMA_MIGRATIONS = [
  `PRAGMA journal_mode=wal;

  CREATE TABLE IF NOT EXISTS "threads" (
    "threadID" VARCHAR NOT NULL,
    "accountID" VARCHAR NOT NULL,
    "platformName" VARCHAR NOT NULL,
    "thread" JSON NOT NULL,
    PRIMARY KEY (accountID, threadID)
  );

  CREATE TABLE IF NOT EXISTS "messages" (
    "messageID" VARCHAR NOT NULL,
    "accountID" VARCHAR NOT NULL,
    "platformName" VARCHAR NOT NULL,
    "timestamp" INTEGER NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "message" JSON NOT NULL,
    PRIMARY KEY (accountID, messageID)
  );

  CREATE TABLE IF NOT EXISTS "users" (
    "userID" VARCHAR NOT NULL,
    "accountID" VARCHAR NOT NULL,
    "platformName" VARCHAR NOT NULL,
    "user" JSON NOT NULL,
    PRIMARY KEY (accountID, userID)
  );

  CREATE INDEX IF NOT EXISTS "messages_message_text_idx" ON "messages" (JSON_EXTRACT(message, '$.text') COLLATE NOCASE);`, // 0

  `CREATE TABLE IF NOT EXISTS "thread_props" (
    "accountID" VARCHAR NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "props" JSON NOT NULL,
    PRIMARY KEY (accountID, threadID)
  );`, // 1

  `CREATE TABLE IF NOT EXISTS "key_values" (
    "key" VARCHAR NOT NULL,
    "value" JSON NOT NULL,
    PRIMARY KEY (key)
  );`, // 2

  'PRAGMA journal_mode=wal', // 3

  `CREATE TABLE IF NOT EXISTS "message_props" (
    "accountID" VARCHAR NOT NULL,
    "messageID" VARCHAR NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "props" JSON NOT NULL,
    PRIMARY KEY (accountID, messageID)
  );

  CREATE TABLE IF NOT EXISTS "snoozed_messages" (
    "messageID" VARCHAR NOT NULL,
    "accountID" VARCHAR NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "messageTimestamp" INTEGER NOT NULL,
    "remindOnTimestamp" INTEGER NOT NULL,
    "addedOnTimestamp" INTEGER NOT NULL,
    "options" JSON NOT NULL,
    PRIMARY KEY (accountID, messageID)
  );

  CREATE TABLE IF NOT EXISTS "snoozed_threads" (
    "accountID" VARCHAR NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "remindOnTimestamp" INTEGER NOT NULL,
    "addedOnTimestamp" INTEGER NOT NULL,
    "options" JSON NOT NULL,
    PRIMARY KEY (accountID, threadID)
  );

  CREATE TABLE IF NOT EXISTS "scheduled_messages" (
    "pendingMessageID" VARCHAR NOT NULL,
    "accountID" VARCHAR NOT NULL,
    "threadID" VARCHAR NOT NULL,
    "options" JSON NOT NULL,
    PRIMARY KEY (accountID, pendingMessageID)
  );`, // 4

  `CREATE TABLE IF NOT EXISTS "counters" (
    "key" VARCHAR NOT NULL,
    "date" VARCHAR NOT NULL,
    "count" INTEGER NOT NULL,
    PRIMARY KEY(key, date)
  );`, // 5

  `CREATE TABLE IF NOT EXISTS "accounts" (
    "accountID" VARCHAR NOT NULL,
    "platformName" VARCHAR NOT NULL,
    "state" VARCHAR NOT NULL,
    "user" JSON,
    "session" JSON,
    "props" JSON,
    PRIMARY KEY (accountID)
  );`, // 6
]

const SQLS = {
  schemaVersion: 'PRAGMA user_version',
  setSchemaVersion: (version: number) => 'PRAGMA user_version = ' + version,

  cacheMessage: 'INSERT OR REPLACE INTO messages VALUES (?, ?, ?, ?, ?, ?)',
  cacheThread: 'INSERT OR REPLACE INTO threads (threadID, accountID, platformName, thread) VALUES (?, ?, ?, ?)',
  cacheUser: 'INSERT OR REPLACE INTO users VALUES (?, ?, ?, ?)',
  updateCachedMessage: 'UPDATE messages SET message = json_patch(message, ?) WHERE accountID = ? AND messageID = ?',
  updateCachedThread: 'UPDATE threads SET thread = json_patch(thread, ?) WHERE accountID = ? AND threadID = ?',
  updateCachedUser: 'UPDATE users SET user = json_patch(user, ?) WHERE accountID = ? AND userID = ?',
  deleteCachedThread: 'DELETE FROM threads WHERE accountID = ? AND threadID = ?',
  deleteCachedMessage: 'DELETE FROM messages WHERE accountID = ? AND threadID = ? AND messageID = ?',

  search: (cursorDirection: string, threadID: string) => `SELECT threadID,timestamp,message
FROM messages
WHERE accountID = ?
AND json_extract(message,'$.text') LIKE ? ESCAPE '\\' COLLATE NOCASE
${cursorDirection ? `AND timestamp ${cursorDirection} ?` : ''}
${threadID ? 'AND threadID = ?' : ''}
ORDER BY timestamp DESC
LIMIT ${MESSAGES_LIMIT}`,
  getMessage: 'SELECT message FROM messages WHERE accountID = ? AND threadID = ? AND messageID = ?',
  getThread: 'SELECT thread FROM threads WHERE accountID = ? AND threadID = ?',
  getThreads: (cursorDirection: string) => `SELECT thread, message, MAX(messages.timestamp)
FROM threads
LEFT JOIN messages ON messages.threadID = threads.threadID
WHERE threads.accountID = ?
AND messages.accountID = threads.accountID
${cursorDirection ? `AND messages.timestamp ${cursorDirection} ?` : ''}
GROUP by threads.threadID
ORDER BY timestamp DESC
LIMIT ${THREADS_LIMIT}`,
  getMessages: (cursorDirection: string) => `SELECT message
FROM messages
WHERE accountID = ?
AND threadID = ?
${cursorDirection ? `AND timestamp ${cursorDirection} ?` : ''}
ORDER BY timestamp DESC
LIMIT ${MESSAGES_LIMIT}`,

  getThreadPropsForAccount: 'SELECT threadID, props FROM thread_props WHERE accountID = ?',
  deleteThreadPropsForThread: 'DELETE FROM thread_props WHERE accountID = ? AND threadID = ?',
  createThreadPropRecord: "INSERT OR IGNORE INTO thread_props VALUES (?, ?, '{}')",
  setThreadProp: 'UPDATE thread_props SET props = json_patch(props, ?) WHERE accountID = ? AND threadID = ?',
  deleteThreadProp: 'UPDATE thread_props SET props = json_remove(props, ?) WHERE accountID = ? AND threadID = ?',

  deleteMessageProps: 'DELETE FROM message_props WHERE accountID = ? AND messageID = ?',
  createMessagePropRecord: "INSERT OR IGNORE INTO message_props VALUES (?, ?, ?, '{}')",
  setMessageProp: 'UPDATE message_props SET props = json_patch(props, ?) WHERE accountID = ? AND messageID = ?',
  deleteMessageProp: 'UPDATE message_props SET props = json_remove(props, ?) WHERE accountID = ? AND messageID = ?',

  getAllKeyValues: 'SELECT * from key_values',
  getKeyValue: 'SELECT value from key_values WHERE key = ?',
  setKeyValue: 'INSERT OR REPLACE INTO key_values VALUES (?, ?)',
  deleteKeyValue: 'DELETE FROM key_values WHERE key = ?',

  insertKeyValueRecord: "INSERT OR IGNORE INTO key_values VALUES (?, '{}')",
  jsonPatchKeyValueValue: 'UPDATE key_values SET value = json_patch(value, ?) WHERE key = ?',
  jsonSetKeyValueValue: 'UPDATE key_values SET value = json_set(value, ?, json(?)) WHERE key = ?',
  jsonRemoveKeyValueValue: 'UPDATE key_values SET value = json_remove(value, ?) WHERE key = ?',
}

const MAP_DIRECTION_TO_SQL_OP = {
  after: '>',
  before: '<',
}

export function jsonStringToMessage(json: any) {
  const msg = JSON.parse(json)
  return jsonToMessage(msg)
}

const mapAttachmentForBackwardsCompatibility = (accountID: string) => (att: MessageAttachment) => {
  if (att.srcURL?.startsWith('asset://') && !att.srcURL?.startsWith(`asset://${accountID}`)) {
    att.srcURL = att.srcURL.replace('asset://', `asset://${accountID}`)
  }
}

const UNENCRYPTED_DB_NAME = '.cache.db'
const ENCRYPTED_DB_NAME = '.index.db'

export default class IndexDatabase {
  private readonly unencryptedDbPath: string

  private readonly encryptedDbPath: string

  readonly initPromise: Promise<void>

  constructor(
    private readonly dirPath: string,
    private readonly getEncryptionKey: PASOptions['getEncryptionKey'],
  ) {
    this.unencryptedDbPath = path.join(dirPath, UNENCRYPTED_DB_NAME)
    this.encryptedDbPath = path.join(dirPath, ENCRYPTED_DB_NAME)
    this.initPromise = this.init()
  }

  readonly db = new AsyncSqlite()

  private async migrateUnencryptedDb() {
    if (!await pathExists(this.unencryptedDbPath)) return console.log('[IndexDatabase] called migrateUnencryptedDb but unencrypted db not present')
    console.log('[IndexDatabase] migrateUnencryptedDb')
    console.time('[IndexDatabase] migrateUnencryptedDb')
    await this.db.exec(`ATTACH DATABASE ${JSON.stringify(this.unencryptedDbPath)} AS plain KEY '';
SELECT sqlcipher_export('main', 'plain');
DETACH DATABASE plain;`)

    // schemaVersion isn't copied over
    const unencDb = new AsyncSqlite()
    await unencDb.init(this.unencryptedDbPath)
    const unencSchemaVersion = await unencDb.pluck_get(SQLS.schemaVersion, [])
    await this.db.exec(SQLS.setSchemaVersion(unencSchemaVersion))
    await unencDb.dispose()

    await fs.rename(this.unencryptedDbPath, this.unencryptedDbPath + `.${Date.now()}.migrated`)
    console.timeEnd('[IndexDatabase] migrateUnencryptedDb')
  }

  private async updateSchema() {
    // this should be 0 when new
    const currentSchemaVersion = await this.db.pluck_get(SQLS.schemaVersion, [])
    let i = currentSchemaVersion
    for (const schemaMigration of SCHEMA_MIGRATIONS.slice(currentSchemaVersion)) {
      if (IS_DEV_ENVIRON) console.log({ currentSchemaVersion, schemaMigration })
      await this.db.exec(schemaMigration)
      await this.db.exec(SQLS.setSchemaVersion(++i))
    }
  }

  private async setLockingMode() {
    if (globalThis.DONT_LOCK_SQLITE) return
    try {
      await this.db.get('PRAGMA locking_mode = EXCLUSIVE')
    } catch (err) {
      console.error('[IndexDatabase] setLockingMode failed', err)
      texts.Sentry.captureException(err)
    }
  }

  private _isEncrypted: boolean

  isEncrypted = () => this._isEncrypted

  async init() {
    /**
     * if encrypted db is present, init with encrypted
     * if unencrypted db is present, init with unencrypted
     * if unencrypted db is present and user wants to encrypt, init with encrypted, migrate and rename unencrypted
     */
    const shouldMigrateUnencryptedDbFlagPath = path.join(this.dirPath, 'shouldMigrateUnencryptedDb')
    const shouldMigrateUnencryptedDb = pathExists(shouldMigrateUnencryptedDbFlagPath)
    const isUnencrypted = await pathExists(this.unencryptedDbPath) && !await shouldMigrateUnencryptedDb
    this._isEncrypted = !isUnencrypted
    const dbPath = this._isEncrypted ? this.encryptedDbPath : this.unencryptedDbPath
    if (IS_DEV_ENVIRON) console.log({ dbPath })
    await this.db.init(dbPath)
    if (IS_DEV_ENVIRON) this.db.get('SELECT sqlite_version()').then(console.log)
    if (this._isEncrypted) {
      const keyHex = await this.getEncryptionKey('db')
      const pragmaKeySql = `PRAGMA key = "x'${keyHex}'";`
      await this.db.exec(pragmaKeySql)
      if (IS_DEV_ENVIRON) console.log(pragmaKeySql)
      if (await shouldMigrateUnencryptedDb) {
        await this.migrateUnencryptedDb()
        fs.unlink(shouldMigrateUnencryptedDbFlagPath)
      }
    }
    await this.setLockingMode()
    await this.updateSchema()
  }

  dispose() {
    return this.db.dispose()
  }

  cacheMessage = async (platformName: string, threadID: string, accountID: string, message: Message) => {
    const json = JSON.stringify(message)
    await this.db.run(
      SQLS.cacheMessage,
      [message.id, accountID, platformName, message.timestamp.getTime(), threadID, json],
    )
  }

  cacheMessages = (platformName: string, threadID: string, accountID: string, messages: Message[]) => {
    messages.forEach(m => {
      this.cacheMessage(platformName, threadID, accountID, m)
    })
  }

  updateCachedMessage = async (accountID: string, messageID: string, update: Partial<Message>) => {
    const json = JSON.stringify(update)
    await this.db.run(SQLS.updateCachedMessage, [json, accountID, messageID])
  }

  cacheUser = async (platformName: string, accountID: string, user: User | Participant) => {
    await this.db.run(SQLS.cacheUser, [user.id, accountID, platformName, JSON.stringify(omit(user, ['isAdmin', 'hasExited']))])
  }

  updateCachedUser = async (accountID: string, userID: string, update: Partial<Message>) => {
    const json = JSON.stringify(update)
    await this.db.run(SQLS.updateCachedUser, [json, accountID, userID])
  }

  cacheThread = async (platformName: string, accountID: string, thread: Thread) => {
    if (thread.extra?.noCache) return
    const threadStub = omit(thread, ['messages'])
    await this.db.run(SQLS.cacheThread, [thread.id, accountID, platformName, JSON.stringify(threadStub)])
  }

  updateCachedThread = async (accountID: string, threadID: string, update: Partial<Message>) => {
    const json = JSON.stringify(update)
    await this.db.run(SQLS.updateCachedThread, [json, accountID, threadID])
  }

  deleteCachedThread = async (accountID: string, threadID: string) => {
    await this.db.run(SQLS.deleteCachedThread, [accountID, threadID])
  }

  deleteCachedMessage = async (accountID: string, threadID: string, messageID: string) => {
    await this.db.run(SQLS.deleteCachedMessage, [accountID, threadID, messageID])
  }

  getThreads = async (accountID: string, inboxName: InboxName, pagination: PaginationArg): Promise<Paginated<Thread>> => {
    const { cursor, direction } = pagination || { cursor: null, direction: null }
    const cursorDirection = cursor && MAP_DIRECTION_TO_SQL_OP[direction]
    const bindings = cursor
      ? [accountID, +cursor]
      : [accountID]
    const rows: any[] = await this.db.all(SQLS.getThreads(cursorDirection), bindings)
    rows.reverse()
    const items = rows.map(r => jsonToThread(JSON.parse(r.thread), JSON.parse(r.message)))
    return {
      items,
      hasMore: rows.length === THREADS_LIMIT,
      oldestCursor: items[0]?.timestamp.getTime().toString(),
    }
  }

  getMessages = async (accountID: string, threadID: string, pagination: PaginationArg): Promise<Paginated<Message>> => {
    const { cursor, direction } = pagination || { cursor: null, direction: null }
    const cursorDirection = cursor && MAP_DIRECTION_TO_SQL_OP[direction]
    const bindings = cursor
      ? [accountID, threadID, +cursor]
      : [accountID, threadID]
    const rows: any[] = await this.db.all(SQLS.getMessages(cursorDirection), bindings)
    rows.reverse()
    const items = rows.map(r => {
      const msg = jsonToMessage(JSON.parse(r.message))
      msg.attachments?.forEach(mapAttachmentForBackwardsCompatibility(accountID))
      return msg
    })
    return {
      items,
      hasMore: rows.length === MESSAGES_LIMIT,
    }
  }

  searchMessages = async (accountID: string, typed: string, pagination: PaginationArg, threadID?: string): Promise<Paginated<Message>> => {
    const { cursor, direction } = pagination || { cursor: null, direction: null }
    const cursorDirection = cursor && MAP_DIRECTION_TO_SQL_OP[direction]
    const typedEscaped = `%${typed.replaceAll('%', '\\%')}%`
    const bindings = cursor
      ? [accountID, typedEscaped, +cursor]
      : [accountID, typedEscaped]
    if (threadID) bindings.push(threadID)
    const rows: any[] = await this.db.all(SQLS.search(cursorDirection, threadID), bindings)
    rows.reverse()
    const items = rows.map(r => {
      const msg = jsonStringToMessage(r.message)
      msg.attachments?.forEach(mapAttachmentForBackwardsCompatibility(accountID))
      msg.threadID = r.threadID
      return msg
    })
    return {
      items,
      hasMore: rows.length === MESSAGES_LIMIT,
      oldestCursor: rows[0]?.timestamp?.toString(),
    }
  }

  deleteAccountData = async (accountID: string) => {
    const bindings = [accountID]
    await bluebird.map([
      'users',
      'messages',
      'threads',
      'thread_props',
      'message_props',
      'snoozed_messages',
      'snoozed_threads',
      'scheduled_messages',
    ], tableName =>
      this.db.run(`DELETE FROM ${tableName} WHERE accountID = ?`, bindings))
  }

  changeAccountID = async (oldAccountID: string, newAccountID: string) => {
    const bindings = [newAccountID, oldAccountID]
    await this.deleteAccountData(newAccountID) // todo review if better way to ignore UNIQUE constraint
    await bluebird.map([
      'accounts',
      'users',
      'messages',
      'threads',
      'thread_props',
      'message_props',
      'snoozed_messages',
      'snoozed_threads',
      'scheduled_messages',
    ], tableName =>
      this.db.run(`UPDATE ${tableName} SET accountID = ? WHERE accountID = ?`, bindings))
  }

  getThread = async (accountID: string, threadID: string) => {
    const thread = await this.db.pluck_get(SQLS.getThread, [accountID, threadID])
    if (thread) return jsonToThread(JSON.parse(thread))
  }

  getMessage = async (accountID: string, threadID: string, messageID: string) => {
    const message = await this.db.pluck_get(SQLS.getMessage, [accountID, threadID, messageID])
    if (message) return jsonStringToMessage(message)
  }

  getThreadPropsForAccount = async (accountID: string) => {
    const rows: any[] = await this.db.raw_all(SQLS.getThreadPropsForAccount, [accountID])
    const result: { [threadID: string]: any } = {}
    for (const [threadID, json] of rows) {
      result[threadID] = JSON.parse(json)
    }
    return result
  }

  setThreadProp = async (accountID: string, threadID: string, propName: string, value: any) => {
    await this.db.run(SQLS.createThreadPropRecord, [accountID, threadID])
    await this.db.run(SQLS.setThreadProp, [JSON.stringify({ [propName]: value }), accountID, threadID])
  }

  deleteThreadProp = async (accountID: string, threadID: string, propName: string) => {
    await this.db.run(SQLS.createThreadPropRecord, [accountID, threadID])
    // propName is quoted because propName may contain a period character, `pms.uuid`
    await this.db.run(SQLS.deleteThreadProp, [`$."${propName}"`, accountID, threadID])
  }

  deleteThreadPropsForThread = async (accountID: string, threadID: string) => {
    await this.db.run(SQLS.deleteThreadPropsForThread, [accountID, threadID])
  }

  deleteMessageProps = async (accountID: string, messageID: string) => {
    await this.db.run(SQLS.deleteMessageProps, [accountID, messageID])
  }

  setMessageProp = async (accountID: string, threadID: string, messageID: string, propName: string, value: any) => {
    await this.db.run(SQLS.createMessagePropRecord, [accountID, messageID, threadID])
    await this.db.run(SQLS.setMessageProp, [JSON.stringify({ [propName]: value }), accountID, messageID])
  }

  deleteMessageProp = async (accountID: string, threadID: string, messageID: string, propName: string) => {
    await this.db.run(SQLS.createMessagePropRecord, [accountID, messageID, threadID])
    // propName is quoted because propName may contain a period character, `pms.uuid`
    await this.db.run(SQLS.deleteMessageProp, [`$."${propName}"`, accountID, messageID])
  }

  async getAllKeyValues() {
    const rows = await this.db.raw_all(SQLS.getAllKeyValues)
    const result: Record<string, any> = {}
    for (const [key, value] of rows) {
      try {
        result[key] = value ? JSON.parse(value) : null
      } catch (err) {
        console.log(`[IndexDatabase] error parsing key value, key: ${key}, value: ${value}`, err.message)
        texts.Sentry.captureException(err)
      }
    }
    return result
  }

  async getKeyValue(key: KeyValueKey) {
    const value = await this.db.pluck_get(SQLS.getKeyValue, [key])
    if (value) return JSON.parse(value)
  }

  async setKeyValue(key: KeyValueKey, value: any) {
    await this.db.run(SQLS.setKeyValue, [key, JSON.stringify(value)])
  }

  async deleteKeyValue(key: KeyValueKey) {
    await this.db.run(SQLS.deleteKeyValue, [key])
  }

  async setKeyValueValue(parentKey: KeyValueKey, childKey: string, childValue: any) {
    await this.db.run(SQLS.insertKeyValueRecord, [parentKey])
    await this.db.run(SQLS.jsonSetKeyValueValue, [`$.${JSON.stringify(childKey)}`, JSON.stringify(childValue), parentKey])
  }

  async deleteKeyValueValue(parentKey: KeyValueKey, childKey: string) {
    // propName is quoted because propName may contain a period character, `pms.uuid`
    await this.db.run(SQLS.jsonRemoveKeyValueValue, [`$.${JSON.stringify(childKey)}`, parentKey])
  }

  async getSnoozedMessages() {
    const rows: any[] = await this.db.all('SELECT * FROM snoozed_messages', [])
    return rows.map(row => ({
      ...row,
      messageTimestamp: new Date(row.messageTimestamp),
      addedOnTimestamp: new Date(row.addedOnTimestamp),
      remindOnTimestamp: new Date(row.remindOnTimestamp),
      options: JSON.parse(row.options),
    }))
  }

  async getSnoozedThreads() {
    const rows: any[] = await this.db.all('SELECT * FROM snoozed_threads', [])
    return rows.map(row => ({
      ...row,
      addedOnTimestamp: new Date(row.addedOnTimestamp),
      remindOnTimestamp: new Date(row.remindOnTimestamp),
      options: JSON.parse(row.options),
    }))
  }

  async getScheduledMessages() {
    const rows: any[] = await this.db.all('SELECT * FROM scheduled_messages', [])
    return rows.map(row => {
      const options = JSON.parse(row.options)
      return {
        ...row,
        options: {
          ...options,
          addedOn: new Date(options.addedOn),
          sendOn: new Date(options.sendOn),
        },
      }
    })
  }

  async upsertSnoozedMessages(snoozedMessages: SnoozedMessage[]) {
    const promises = snoozedMessages.map(sm =>
      this.db.run('INSERT OR REPLACE INTO snoozed_messages VALUES (?, ?, ?, ?, ?, ?, ?)', [
        sm.messageID,
        sm.accountID,
        sm.threadID,
        sm.messageTimestamp.getTime(),
        sm.remindOnTimestamp.getTime(),
        sm.addedOnTimestamp.getTime(),
        JSON.stringify(sm.options),
      ]))
    await Promise.all(promises)
  }

  async deleteSnoozedMessagesByID(ids: [string, string][]) {
    const promises = ids.map(([accountID, messageID]) =>
      this.db.run('DELETE FROM snoozed_messages WHERE accountID = ? AND messageID = ?', [accountID, messageID]))
    await Promise.all(promises)
  }

  async upsertSnoozedThreads(snoozedThreads: SnoozedThread[]) {
    const promises = snoozedThreads.map(sm =>
      this.db.run('INSERT OR REPLACE INTO snoozed_threads VALUES (?, ?, ?, ?, ?)', [
        sm.accountID,
        sm.threadID,
        sm.remindOnTimestamp.getTime(),
        sm.addedOnTimestamp.getTime(),
        JSON.stringify(sm.options),
      ]))
    await Promise.all(promises)
  }

  async deleteSnoozedThreadsByID(ids: [string, string][]) {
    const promises = ids.map(([accountID, threadID]) =>
      this.db.run('DELETE FROM snoozed_threads WHERE accountID = ? AND threadID = ?', [accountID, threadID]))
    await Promise.all(promises)
  }

  async upsertScheduledMessages(scheduledMessages: ScheduledMessage[]) {
    const promises = scheduledMessages.map(sm => (
      this.db.run('INSERT OR REPLACE INTO scheduled_messages VALUES (?, ?, ?, ?)', [
        sm.pendingMessageID,
        sm.accountID,
        sm.threadID,
        JSON.stringify(sm.options),
      ])
    ))
    await Promise.all(promises)
  }

  async updateScheduledMessage(accountID: string, pendingMessageID: string, scheduleOptions: ScheduleOptions) {
    await this.db.run('UPDATE scheduled_messages SET options = ? WHERE pendingMessageID = ? AND accountID = ?', [
      JSON.stringify(scheduleOptions),
      pendingMessageID,
      accountID,
    ])
  }

  async deleteScheduledMessage(accountID: string, pendingMessageID: string) {
    await this.db.run('DELETE FROM scheduled_messages WHERE accountID = ? AND pendingMessageID = ?', [accountID, pendingMessageID])
  }

  async getStarredMessages(): Promise<StarredMessage[]> {
    const rows = await this.db.all("SELECT accountID, threadID, messageID, JSON_EXTRACT(props, '$.starredOn') as starredOn FROM message_props WHERE JSON_EXTRACT(props, '$.starredOn') IS NOT NULL", [])
    return rows.map(row => ({
      ...row,
      starredOn: row.starredOn,
    }))
  }

  async starMessages(messageIDs: FullMessageID[], starredOn = Date.now()) {
    const promises = messageIDs.map(({ accountID, threadID, messageID }) =>
      this.setMessageProp(accountID, threadID, messageID, 'starredOn', starredOn))
    await Promise.all(promises)
  }

  async unstarMessages(messageIDs: [string, string][]) {
    const promises = messageIDs.map(([accountID, messageID]) =>
      // deleting all props since we don't use message props for anything other than starredOn atm
      this.deleteMessageProps(accountID, messageID))
    await Promise.all(promises)
  }

  async incrementCounter(key: string, by = 1) {
    const date = new Date().toISOString().slice(0, 10)
    const count = await this.db.pluck_get('SELECT count FROM counters WHERE key = ? AND date = ?', [key, date])
    await this.db.run('INSERT OR REPLACE INTO counters VALUES (?, ?, ?)', [key, date, (count || 0) + by])
  }

  async getAllAccounts(): Promise<Record<string, Account>> {
    const accounts = await this.db.all('SELECT accountID as id, platformName, state, user, session, props FROM accounts')
    const map: Record<string, Account> = {}
    for (const account of accounts) {
      if (account.user) account.user = JSON.parse(account.user)
      if (account.session) account.session = JSON.parse(account.session)
      if (account.props) account.props = JSON.parse(account.props)
      map[account.id] = account
    }
    return map
  }

  async upsertAccount(account: Account) {
    const { id, platformName, state, user, session, props } = account
    await this.db.run('INSERT OR REPLACE INTO accounts VALUES (?, ?, ?, ?, ?, ?)', [
      id,
      platformName,
      state,
      JSON.stringify(user),
      JSON.stringify(session),
      JSON.stringify(props),
    ])
  }

  async setAccountValue(accountID: string, key: Exclude<keyof Account, 'accountID' | 'platformName'>, _value: Account[keyof Account]) {
    const value = ['user', 'session', 'props'].includes(key) ? JSON.stringify(_value) : _value
    await this.db.run(`UPDATE accounts SET ${key} = ? WHERE accountID = ?`, [value, accountID])
  }

  async deleteAccount(accountID: string) {
    await this.db.run('DELETE FROM accounts WHERE accountID = ?', [accountID])
  }

  vacuum() {
    return this.db.exec('VACUUM')
  }
}
