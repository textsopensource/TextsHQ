/* eslint-disable no-param-reassign */
import path from 'path'
import chalk from 'chalk'
import { ThreadFolderName, PaginationArg, Paginated, Thread, Message, ServerEvent, ServerEventType, MessageAttachment, texts, MessageBehavior } from '@textshq/platform-sdk'
import extName from 'ext-name'

import IS_DEV_ENVIRON from './is-dev-environ'
import { IGNORE_RESULT_SYMBOL } from './hook-constants'
import { getCodeEntities } from './text-entities'
import type { Plugin } from './types'

const getExtFromMimeType = (mimeType: string) => {
  const [ext] = extName.mime(mimeType)
  return ext?.ext || mimeType.split('/')?.[1].split(/\W/)[0]
}

// todo: this shouldn't be a global
const originalObjMap = new Map<string, Map<string, string>>()

const CODE_BACKTICKS = '```'

function modifyMessage(accountID: string, threadID: string, message: Message, platformName: string, isPartial = false) {
  if (!isPartial) {
    originalObjMap.get(accountID)?.set(message.id, message._original)
    message._original = undefined
    if (!message.timestamp) {
      const msg = `${accountID} falsey ${platformName} message.timestamp: ${message.timestamp}`
      texts.log(msg, message)
      texts.Sentry.captureMessage(msg)
      message.timestamp = new Date()
    } else if (isNaN(message.timestamp.getTime())) {
      const msg = `${accountID} invalid ${platformName} message.timestamp: ${message.timestamp.toString()}`
      texts.log(msg, message)
      texts.Sentry.captureMessage(msg)
      message.timestamp = new Date()
    }
  }
  if (message.editedTimestamp && isNaN(message.editedTimestamp.getTime())) {
    const msg = `${accountID} invalid ${platformName} message.editedTimestamp: ${message.editedTimestamp.toString()}`
    texts.log(msg, message)
    texts.Sentry.captureMessage(msg)
    message.editedTimestamp = undefined
  }
  function modifyAttachment(att: MessageAttachment) {
    if (!att) {
      texts.Sentry.captureMessage(`falsey attachment sent by ${platformName}`)
      return texts.log('falsey attachment', accountID, threadID)
    }
    const isAssetURL = att.srcURL?.startsWith('asset://')
    if (isAssetURL) att.srcURL = att.srcURL.replace('asset://$accountID/', `asset://${accountID}/`)
    if (!att.fileName && att.mimeType) {
      att.fileName = `${att.id}.${getExtFromMimeType(att.mimeType)}`
      if (isAssetURL) {
        att.srcURL += '/' + att.fileName
      }
    } else if (!att.fileName && att.srcURL) {
      const parsed = new URL(att.srcURL)
      if (parsed.pathname) {
        const basename = path.basename(parsed.pathname)
        att.fileName = basename
      }
    }
    if (IS_DEV_ENVIRON && !att.fileName) console.log(chalk.bgRed.white('NO ATTACHMENT FILENAME'), accountID, threadID, message.id)
  }
  message.links?.forEach(link => {
    if (!link) return
    if (link.img?.startsWith('asset://')) link.img = link.img.replace('asset://$accountID/', `asset://${accountID}/`)
  })
  message.linkedMessage?.attachments?.forEach(modifyAttachment)
  message.attachments?.forEach(modifyAttachment)
  if (message.tweet && !message.tweets) {
    message.tweets = [message.tweet]
    message.tweet = null
  }
  if (message.silent) {
    message.behavior = MessageBehavior.SILENT
  }
  if (message.text?.includes(CODE_BACKTICKS)) {
    message.textAttributes = message.textAttributes || { entities: [] }
    message.textAttributes.entities = message.textAttributes.entities || []
    message.textAttributes.entities.push(...getCodeEntities(message.text))
  }
  message.buttons?.forEach(button => {
    if (button.linkURL?.startsWith('texts://')) {
      button.linkURL = button.linkURL.replace('/$accountID/', `/${accountID}/`)
    }
  })
}

function modifyThread(accountID: string, thread: Thread, folderName?: ThreadFolderName) {
  originalObjMap.get(accountID)?.set(thread.id, thread._original)
  thread._original = undefined
  if (!thread.folderName && folderName) thread.folderName = folderName
}
function getOriginalObject(platformName: string, accountID: string, args: [string, string]) {
  return originalObjMap.get(accountID)?.get(args[1])
}
texts.getOriginalObject = getOriginalObject

const mapMissingPlugin: Plugin = {
  type: 'output',
  hooks: {
    init(platformName: string, accountID: string) {
      originalObjMap.set(accountID, new Map())
      return IGNORE_RESULT_SYMBOL
    },
    dispose(platformName: string, accountID: string) {
      originalObjMap.delete(accountID)
      return IGNORE_RESULT_SYMBOL
    },
    getOriginalObject,
    getThreads(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Thread>) {
      const threads = result.items
      threads.forEach(thread => {
        modifyThread(accountID, thread, args[0])
        thread.messages.items.forEach(message => {
          modifyMessage(accountID, thread.id, message, platformName)
        })
      })
      return IGNORE_RESULT_SYMBOL
    },
    getMessages(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Message>) {
      const threadID = args[0]
      result.items.forEach(message => {
        modifyMessage(accountID, threadID, message, platformName)
      })
      return IGNORE_RESULT_SYMBOL
    },
    getThread(platformName: string, accountID: string, args: [string], thread: Thread) {
      if (typeof thread === 'object') {
        modifyThread(accountID, thread, args[0])
        thread.messages.items.forEach(message => {
          modifyMessage(accountID, thread.id, message, platformName)
        })
      }
      return IGNORE_RESULT_SYMBOL
    },
    getMessage(platformName: string, accountID: string, args: [string, string], message: Message) {
      const threadID = args[0]
      if (typeof message === 'object') modifyMessage(accountID, threadID, message, platformName)
      return IGNORE_RESULT_SYMBOL
    },
    searchMessages(platformName: string, accountID: string, args: [string, PaginationArg], result: Paginated<Message>) {
      const threadID = args[0]
      if (result) {
        result.items.forEach(message => {
          modifyMessage(accountID, threadID, message, platformName)
        })
      }
      return IGNORE_RESULT_SYMBOL
    },
    sendMessage(platformName: string, accountID: string, args: [string], result: boolean | Message[]) {
      const threadID = args[0]
      if (Array.isArray(result)) {
        result.forEach(message => {
          modifyMessage(accountID, threadID, message, platformName)
        })
      }
      return IGNORE_RESULT_SYMBOL
    },
    createThread(platformName: string, accountID: string, args: [string[], string], result: boolean | Thread) {
      if (typeof result === 'object') modifyThread(accountID, result)
      return IGNORE_RESULT_SYMBOL
    },
    subscribeToEvents(platformName: string, accountID: string, args: any[], events: ServerEvent[]) {
      events?.forEach(se => {
        if (se?.type !== ServerEventType.STATE_SYNC) return
        if (se.objectName === 'message') {
          switch (se.mutationType) {
            case 'upsert':
              se.entries.forEach(entry => {
                modifyMessage(accountID, se.objectIDs.threadID, entry as Message, platformName)
              })
              break
            case 'update':
              se.entries.forEach(entry => {
                modifyMessage(accountID, se.objectIDs.threadID, entry as Message, platformName, true)
              })
              break
          }
        }
        if (se.objectName === 'thread' && se.mutationType === 'upsert') {
          se.entries.forEach(entry => {
            const thread = entry as Thread
            modifyThread(accountID, thread)
            thread.messages.items.forEach(message => {
              modifyMessage(accountID, thread.id, message, platformName)
            })
          })
        }
      })
      return IGNORE_RESULT_SYMBOL
    },
  },
}

export default mapMissingPlugin
