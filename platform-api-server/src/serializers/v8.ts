import type { TransportSerializer } from '../types'

let v8: typeof import('v8')
try {
  v8 = require('v8')
} catch (err) {
  // swallow
}

const V8Serializer: TransportSerializer = v8 ? {
  name: 'v8',
  encode: v8.serialize,
  decode: v8.deserialize,
} : null

export default V8Serializer
