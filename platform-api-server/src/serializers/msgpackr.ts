import { encode, decode } from 'msgpackr'
import type { TransportSerializer } from '../types'

const MessagePackSerializer: TransportSerializer = {
  name: 'msgpack',
  encode,
  decode,
}

export default MessagePackSerializer
