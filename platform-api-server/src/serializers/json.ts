import type { TransportSerializer } from '../types'

const JSONSerializer: TransportSerializer = {
  name: 'json',
  encode: JSON.stringify,
  decode: JSON.parse,
}

export default JSONSerializer
