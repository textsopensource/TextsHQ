import { encode, decode } from 'cbor-x'

import type { TransportSerializer } from '../types'

const CBORSerializer: TransportSerializer = {
  name: 'cbor',
  encode,
  decode,
}

export default CBORSerializer
