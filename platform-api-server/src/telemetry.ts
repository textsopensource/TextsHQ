import IS_DEV_ENVIRON from './is-dev-environ'

// TODO FIXME

export const trackTime = (category: string, value: string, time: number) => {
  if (IS_DEV_ENVIRON) console.log('Time:', category, '—', value, '—', time)
  // return analytics.send('timing', {
  //   utc: category,
  //   utv: value,
  //   utt: time.toFixed(0),
  // })
}

export const trackEvent = (category: string, action: string, label: string = undefined) => {
  if (IS_DEV_ENVIRON) console.log('Event:', category, '—', action, label || '')
  // return analytics.send('event', {
  //   ec: category,
  //   ea: action,
  //   el: label,
  // })
}
