import type { PlatformAPI } from '@textshq/platform-sdk'

import IS_DEV_ENVIRON from './is-dev-environ'
import { CB_METHODS } from './relayer-constants'
import PluginsStore from './PluginsStore'
import { IGNORE_RESULT_SYMBOL } from './hook-constants'
import type { PlatformAPIRelayerData, PushFunction } from './types'
import type PlatformAPIServer from './PlatformAPIServer'

type ConstructiblePlatformAPI = PlatformAPI & { new(): PlatformAPI }

export default class PlatformAPIStore {
  // key is accountID
  private readonly apiInstances = new Map<string, PlatformAPI>()

  // key is platformName
  private readonly platformAPIMap = new Map<string, PlatformAPI>()

  private readonly pluginsStore: PluginsStore

  constructor(private readonly pas: PlatformAPIServer) {
    this.pluginsStore = new PluginsStore(pas)
  }

  private async dispose(accountID: string) {
    if (IS_DEV_ENVIRON) console.log('disposing', accountID)
    await this.apiInstances.get(accountID).dispose()
    this.apiInstances.delete(accountID)
  }

  disposeAll() {
    for (const accountID of this.apiInstances.keys()) {
      this.dispose(accountID)
    }
  }

  private getPlatformAPI(platformName: string): PlatformAPI {
    const PAPI = this.platformAPIMap.get(platformName)
    if (PAPI) return PAPI
    const loadPlatformAPI = this.pas.options.loadPlatformAPIMap[platformName]
    if (!loadPlatformAPI) throw new Error('invalid platformName: ' + platformName)
    this.platformAPIMap.set(platformName, loadPlatformAPI())
    return this.platformAPIMap.get(platformName)
  }

  async callMethod({ platformName, accountID, methodName, args: _args }: PlatformAPIRelayerData, callback: PushFunction) {
    if (!accountID) throw new Error('invalid accountID: ' + accountID)
    if (methodName === 'init') {
      if (this.apiInstances.has(accountID)) {
        console.log('disposing existing', accountID, platformName)
        await this.dispose(accountID)
      }
      const PAPI = this.getPlatformAPI(platformName) as ConstructiblePlatformAPI
      const instance = new PAPI()
      this.apiInstances.set(accountID, instance)
    }
    const account = this.apiInstances.get(accountID)
    if (!account) throw new Error(`called method ${methodName} for uninitialized ${platformName} account: ${accountID}`)
    const method: Function = account[methodName]
    let args = _args
    for (const hook of this.pluginsStore.inputHooks[methodName] || []) {
      const newArgs = await hook(platformName, accountID, args)
      if (newArgs !== IGNORE_RESULT_SYMBOL) args = newArgs
    }
    if (CB_METHODS.has(methodName) && method) {
      method(async (_cbData: any) => {
        let cbData = _cbData
        for (const hook of this.pluginsStore.outputHooks[methodName] || []) {
          const newData = await hook(platformName, accountID, args, cbData)
          if (newData !== IGNORE_RESULT_SYMBOL) cbData = newData
        }
        callback({ accountID, methodName, cbData })
      })
      return
    }
    if (methodName === 'dispose') {
      await this.dispose(accountID)
      return
    }
    let result: any
    if (method) {
      // console.time(accountID + methodName)
      result = await method(...args)
      // console.timeEnd(accountID + methodName)
    } else if (IS_DEV_ENVIRON) {
      console.log('PlatformAPIStore: no method found', platformName + '.' + methodName)
    }
    for (const hook of this.pluginsStore.outputHooks[methodName] || []) {
      const newResult = await hook(platformName, accountID, args, result)
      if (newResult !== IGNORE_RESULT_SYMBOL) result = newResult
    }
    return result
  }
}
