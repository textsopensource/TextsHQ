import { mapValues } from 'lodash'
import { pipeline } from 'stream/promises'
import { GetAssetOptions, texts } from '@textshq/platform-sdk'
import type http from 'http'

import { getAvatarURL } from './avatar'
import { isReadableStream } from './util'
import IS_DEV_ENVIRON from './is-dev-environ'
import parseRange from './range-parser'
import type { TransportSerializer, HTTPRoutes, HTTPRoute, PushFunction, GenericRoute } from './types'
import type PlatformAPIStore from './PlatformAPIStore'

export default function getHTTPRoutes(
  serializer: TransportSerializer,
  extraHTTPRoutes: HTTPRoutes,
  genericRoutes: Record<string, Function>,
  platformAPIStore: PlatformAPIStore,
  proxyFileURLs: boolean,
): Record<string, HTTPRoute> {
  const wrapGenericRoute = (gr: GenericRoute) => (req: http.IncomingMessage, res: http.ServerResponse, push?: PushFunction) => {
    const chunks: Buffer[] = []
    req.on('data', chunk => {
      chunks.push(chunk)
    }).on('end', async () => {
      const buf = Buffer.concat(chunks)
      const deserialized: any = serializer.decode(buf)
      const data = await gr(deserialized, push)
      res.end(serializer.encode(data))
    }).on('error', error => {
      console.error('pas: generic route error', error)
      globalThis.Sentry.captureException(error)
      if (!res.headersSent) res.writeHead(500).end()
    })
  }

  async function asset(req: http.IncomingMessage, res: http.ServerResponse) {
    try {
      const [, transportNonce, , accountID, ...args] = req.url.split('/')
      if (accountID === '$accountID') {
        console.error('$accountID was not replaced', req.url)
        globalThis.Sentry.captureEvent({
          message: '$accountID was not replaced',
          extra: {
            url: req.url,
          },
        })
        return res.writeHead(400).end()
      }
      const opts: GetAssetOptions = {}
      if (req.headers.range) {
        const info = await platformAPIStore.callMethod({
          accountID,
          methodName: 'getAssetInfo',
          args: [{}, ...args],
        }, () => {})
        if (info) {
          const { contentLength, contentType } = info || {}
          if (contentType) res.setHeader('Content-Type', contentType)
          const range = contentLength == null ? undefined : parseRange(contentLength, req.headers.range)
          if (range) {
            if (typeof range === 'object' && range.type === 'bytes') {
              const [firstRange] = range
              opts.range = firstRange
              res.statusCode = 206
              res.setHeader('Content-Range', `bytes ${firstRange.start}-${firstRange.end}/${contentLength}`)
              res.setHeader('Content-Length', firstRange.end - firstRange.start + 1)
            } else {
              res.statusCode = 416
              res.setHeader('Content-Range', `bytes */${info.contentLength}`)
              res.setHeader('Content-Length', contentLength)
            }
          }
        } else {
          texts.error("pas asset route: couldn't fetch asset info", accountID, req.headers.range, info)
        }
      }
      const _result = await platformAPIStore.callMethod({
        accountID,
        methodName: 'getAsset',
        args: [opts, ...args],
      }, () => {})
      const isAsset = typeof _result === 'object' && 'data' in _result
      const result = isAsset ? _result.data : _result
      if (!result) {
        res.writeHead(404).end()
      } else if (typeof result === 'string') {
        if (IS_DEV_ENVIRON) {
          try {
            new URL(result)
          } catch (err) {
            if (err.code === 'ERR_INVALID_URL') throw Error(`invalid URL returned by ${accountID} getAsset: ${result}`)
            else throw err
          }
        }
        // sync with texts-app-desktop/src/renderer/attachments.ts
        // /fileProxy/ route is implemented in texts-app-desktop and passed as a generic route
        const Location = proxyFileURLs && result.startsWith('file://')
          ? `/${transportNonce}/fileProxy/${Buffer.from(result).toString('base64')}`
          : result
        res.writeHead(301, { Location }).end()
      } else if (Buffer.isBuffer(result)) {
        res.end(result)
      } else if (isReadableStream(result)) {
        await pipeline(result, res)
      } else {
        console.log('unknown response from getAsset', accountID)
        if (IS_DEV_ENVIRON) {
          console.log(opts, args, { result })
        }
        res.end()
      }
    } catch (error) {
      console.error('pas: asset error', error)
      globalThis.Sentry.captureException(error)
      if (!res.headersSent) res.writeHead(500).end()
    }
  }

  async function avatar(req: http.IncomingMessage, res: http.ServerResponse) {
    try {
      const [encEmailsArray, initials] = req.url.split('?')?.[1]?.split('&') || []
      if (!encEmailsArray) return res.writeHead(404).end()
      const emails = decodeURIComponent(encEmailsArray).split(',')
      const Location = await getAvatarURL(emails, initials) || null
      res.writeHead(302, { Location }).end()
    } catch (err) {
      console.error('pas: avatar error', err)
      if (!res.headersSent) res.writeHead(404).end()
    }
  }

  return {
    ...mapValues(genericRoutes, wrapGenericRoute),
    // following are easiest to do over an http server since they primarily serve <img> resources and do redirects
    asset,
    avatar,
    ...extraHTTPRoutes,
  }
}
