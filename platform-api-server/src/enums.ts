export enum AccountState {
  DISABLED = 'disabled',
  ENABLED = 'enabled',
}
