# platform-api-server

platform-api-server runs a HTTP and WebSocket server (on `httpPort`) which a Texts client app can connect to and communicate with platform integrations (Telegram etc.) and the SQLite database it creates.

With the exception of some routes, almost all data passing through HTTP and WebSocket is serialized and deserialized with `serializer` (which can be `v8.serialize`/`v8.deserialize`, MessagePack or similar). The HTTP server doesn't conform to REST and is similar to RPC.

It loads platform integrations you specify with `platformInfoMap` & `loadPlatformAPIMap` and lets you call all methods defined in `PlatformAPI` over HTTP and WebSocket. `ServerEvent`s sent by the platform integration are always sent over WebSocket.

If `AsyncSqlite` global is present, it creates a SQLite db at `$dataDirPath/.cache.db` and indexes all messages, threads and users sent out of platform integrations.

Sample usage code:
```ts
import PlatformAPIServer from '@textshq/platform-api-server/dist/PlatformAPIServer'
import { V8Serializer } from '@textshq/platform-api-server/dist/serializers'

const pas = new PlatformAPIServer({
  transportOptions: {
    httpSerializerName: 'v8',
    port: 1337,
    nonce: 'random secret',
  },

  plugins: [],
  indexingDisabled: false,
  dataDirPath: 'path/to/data/directory',

  platformInfoMap: {
    telegram: require('@textshq/platform-telegram').default.info,
  },
  loadPlatformAPIMap: {
    telegram: () => require('@textshq/platform-telegram').default.api,
  },
})
await pas.initPromise
```
