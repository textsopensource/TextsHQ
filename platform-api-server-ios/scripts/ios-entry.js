/**
 * patches for nodejs-mobile
 * load via `-r /absolute/path/to/ios-entry.js
 */
const path = require('path')

const frameworksDirPath = path.join(__dirname, '../../Frameworks')

const ogDlopen = process.dlopen
process.dlopen = function patchedDlopen(exp, filename) {
  const modname = path.basename(filename, '.node')
  const modpath = path.join(frameworksDirPath, `${modname}.framework`, modname)
  return ogDlopen(exp, modpath)
}

// add our node_modules path
require('module').globalPaths.unshift(path.join(__dirname, 'node_modules'))
