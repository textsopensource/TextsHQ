#!/usr/bin/env bash

BF_DIR="built-frameworks"
BR_DIR="Sources/PASMobile/built-resources"
NM_DIR="${BR_DIR}/node_modules"

MODULES=(
  "@signalapp/libsignal-client"
  "better-sqlite"
)

FILES_TO_BE_MINIFIED=(
  ${BR_DIR}/{file,ios-entry,worker{-pipeline,,1}}.js
  ${BR_DIR}/platform-signal/{bridge,setup-sandbox}.js
)

TEMPDIR=$(mktemp -d)

echo 'pre pruning:'
du -sh ${BR_DIR}

rm -rf ${BR_DIR}/build/Release/obj.target

# other-worker.ts post processing
mv ${BR_DIR}/ow.js ${BR_DIR}/ow.js.dir
mv ${BR_DIR}/ow.js.dir/index.js ${BR_DIR}/ow.js
mv ${BR_DIR}/ow.js.dir/index.js.map ${BR_DIR}/ow.js.map
rm -rf ${BR_DIR}/ow.js.dir

NCC_FLAGS="--target es2021 --minify"

for MODULE in "${MODULES[@]}"; do
  MOD_PATH="${NM_DIR}/${MODULE}"
  if [ ! -d "${MOD_PATH}" ]; then
    continue
  fi

  rm -rf ${MOD_PATH}/ncc-built
  echo "Processing ${MODULE}..."

  npx @vercel/ncc build ${MOD_PATH} ${NCC_FLAGS} -o ${MOD_PATH}/ncc-built > /dev/null

  if [ -d "${MOD_PATH}/prebuilds" ]; then
    rm -rf "${MOD_PATH}/ncc-built/prebuilds"
    cp -r "${MOD_PATH}/prebuilds" "${MOD_PATH}/ncc-built"
  fi

  if [ -d "${MOD_PATH}/bin" ]; then
    rm -rf "${MOD_PATH}/ncc-built/bin"
    cp -r "${MOD_PATH}/bin" "${MOD_PATH}/ncc-built"
  fi

  if ls ${MOD_PATH}/build/Release/*.node 1> /dev/null 2>&1; then
    rm -rf "${MOD_PATH}/ncc-built/build"
    mkdir -p "${MOD_PATH}/ncc-built/build/Release"
    cp -r "${MOD_PATH}"/build/Release/*.node "${MOD_PATH}/ncc-built/build/Release"
  fi

  # workarounds
  if [ "${MODULE}" = "@signalapp/libsignal-client" ]; then
    npx @vercel/ncc build ${MOD_PATH}/zkgroup.js ${NCC_FLAGS} -o ${MOD_PATH}/ncc-built/zk > /dev/null
    cp ${MOD_PATH}/ncc-built/zk/index.js ${MOD_PATH}/ncc-built/zkgroup.js
    rm -rf ${MOD_PATH}/ncc-built/zk
  fi

  # workaround for scoped packages
  mkdir -p ${TEMPDIR}/${MODULE}
  mv ${MOD_PATH}/ncc-built/* ${TEMPDIR}/${MODULE}
  rm -rf ${MOD_PATH}/ncc-built
done

# Remove non Mach-O files
rm -rf \
  $(find ${TEMPDIR} -type f \( -name '*.node' -o -name '*.dll' -o -name '*.so' -o -name '*.dylib' \) -exec sh -c "file '{}' | fgrep -qv 'Mach-O'" \; -print)

# Delete empty directories
find ${TEMPDIR} -empty -type d -delete

# Move our pruned node_modules back to built-resources
rm -rf "${NM_DIR}"
mv "${TEMPDIR}" "${NM_DIR}"

# node checks for the existence of native modules in the fs before requiring, so
# write stubs at those paths
find ${BR_DIR} -name \*.node -exec bash -c 'echo "NODEJS_MOBILE_RELOCATED" > {}' \;

# our signal-client is patched to load from ./build/Release
rm -rf ${NM_DIR}/@signalapp/libsignal-client/prebuilds

# Minify other files
for FILE in "${FILES_TO_BE_MINIFIED[@]}"; do
  FILENAME="$(basename ${FILE})"
  TEMPDIR=$(mktemp -d)
  npx @vercel/ncc build ${FILE} --target es2021 --minify -o "${TEMPDIR}" > /dev/null

  rm -rf "${FILE}"
  mv "${TEMPDIR}/index.js" "${FILE}"
  rm -rf "${TEMPDIR}"
done

# Remove some frameworks
rm -rf ${BF_DIR}/test_extension.xcframework
rm -rf ${BR_DIR}/build/Release/obj.*
rm -rf ${BR_DIR}/*.dylib

# some fixes
sed -i'.bk' -e 's/\.default\.PureComponent/\.PureComponent/g' ${BR_DIR}/platform-signal/bridge-entry.js
rm ${BR_DIR}/platform-signal/bridge-entry.js.bk

echo 'post pruning:'
du -sh ${BR_DIR}
