#!/usr/bin/env bash

VERSION="$1"
COMMIT_PREFIX="$2"

echo '!built-*' >> .gitignore
echo '!/Sources/PASMobile/built-resources/build' >> .gitignore

bash scripts/prune-app-node-modules.sh

git add -A

git add --force Sources/PASMobile/built-resources/node_modules

git commit -m "${COMMIT_PREFIX}Deploy $(git rev-parse --short HEAD)"
[[ -n "$VERSION" ]] && git tag "$VERSION"
cur_branch="$(git branch --show-current)"
git push --force origin "${cur_branch}:build/${cur_branch}"
git push --tags
