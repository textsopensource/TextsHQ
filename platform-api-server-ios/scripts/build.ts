import { spawn, SpawnOptions } from 'child_process'
import path, { join } from 'path'
import fs from 'fs/promises'
import { exit } from 'process'

const root = path.resolve(__dirname, '..')
const textsPackages = join(root, 'node_modules/@textshq')
const resources = join(root, 'Sources/PASMobile/built-resources')
const frameworks = join(root, 'built-frameworks')
const MinimumOSVersion = '14.0'
const esVersion = 'es2021'

async function runAndCapture(command: string, args: readonly string[], options: SpawnOptions = {}): Promise<string> {
  return new Promise((res, rej) => {
    let output = ''
    const proc = spawn(command, args, options)
    proc.stdout.on('data', d => {
      output += d.toString()
    })
    proc.on('close', code => {
      if (code === 0) res(output)
      else rej(new Error(`command ${command} exited with code: ${code}`))
    })
  })
}

async function run(command: string, args: readonly string[], options: SpawnOptions = {}): Promise<void> {
  return new Promise((res, rej) => {
    spawn(command, args, {
      stdio: 'inherit',
      ...options,
    }).on('close', code => {
      if (code === 0) res()
      else rej(new Error(`command ${command} exited with code: ${code}`))
    })
  })
}

const dropboxIgnoreDir = (dirPath: string) =>
  run('xattr', ['-w', 'com.dropbox.ignored', '1', dirPath])

// git clone https://github.com/1Conan/nodejs-mobile --depth 1 --branch node-ios-v16
// wget https://codeload.github.com/1Conan/nodejs-mobile/zip/refs/heads/node-ios-v16
async function getNodeMobileRoot() {
  const author = '1Conan'
  const repoName = 'nodejs-mobile'
  const branch = 'node-ios-v16'
  // we extract to node_modules instead of root because nodejs-mobile includes .ts files and that makes vercel/ncc read them and complain
  const nodeMobileRoot = join(root, `node_modules/${repoName}-${branch}`)
  const exists = await fs.access(nodeMobileRoot).then(() => true).catch(() => false)
  if (!exists) {
    const zipName = `${repoName}-${branch}.zip`
    await run('wget', ['-cO', zipName, `https://codeload.github.com/${author}/${repoName}/zip/refs/heads/${branch}`])
    await run('unzip', ['-oq', zipName, '-d', 'node_modules'])
  }
  return nodeMobileRoot
}

const sanitizeModuleName = (name: string) => name.replace(/[^a-zA-Z0-9.-]/g, '')

class Target {
  constructor(
    public arch: string,
    public triple: string,
    public cargoTriple: string,
    public swiftmoduleTriples: string[],
    public platform: string,
    public simulator?: boolean,
  ) {}

  pathForModule(module: string, toExecutable: boolean): string {
    const frameworkDir = join(frameworks, this.triple, `${module}.framework`)
    return toExecutable ? join(frameworkDir, module) : frameworkDir
  }
}

const targets: Record<string, Target> = {
  'arm64-ios': new Target(
    'arm64',
    'arm64-apple-ios14',
    'aarch64-apple-ios',
    ['arm64', 'arm64-apple-ios'],
    'iphoneos',
  ),

  'x64-sim': new Target(
    'x64',
    'x86_64-apple-ios14-simulator',
    'x86_64-apple-ios',
    ['x86_64', 'x86_64-apple-ios-simulator'],
    'iphonesimulator',
    true,
  ),
}
const allTargets = Object.keys(targets)

// arm sim is a special target: we can simply vtool the arm64 ios
// binary to change LC_BUILD_VERSION to indicate that it's for the
// simulator; otherwise, it's basically identical to iOS
const armSimTarget = new Target(
  'arm64',
  'arm64-apple-ios14-simulator',
  'aarch64-apple-ios-sim',
  ['arm64', 'arm64-apple-ios-simulator'],
  'iphonesimulator',
  true,
)

async function* findNodeFiles(dir: string): AsyncGenerator<string> {
  const ents = await fs.readdir(dir, { withFileTypes: true })
  for (const ent of ents) {
    const filePath = join(dir, ent.name)
    if (ent.isDirectory()) {
      yield* findNodeFiles(filePath)
    } else if (filePath.endsWith('.node') || filePath.endsWith('.dylib')) {
      const fileType = await runAndCapture('file', [filePath])
      if (!fileType.includes('dynamically linked shared library')) {
        console.warn('warning: .node file is not a dylib, skipping:', filePath)
        return []
      }
      yield filePath
    }
  }
}

async function runRebuild(target: Target): Promise<void> {
  await run('yarn', ['rebuild'], {
    env: {
      YARN_CHECKSUM_BEHAVIOR: 'update',
      NODESWIFT_PLATFORM: target.platform,
      NODESWIFT_TARGET: target.triple,
      CARGO_BUILD_TARGET: target.cargoTriple,
      GYP_DEFINES: `OS=ios simulator=${target.simulator ? '1' : '0'}`,
      npm_config_platform: 'ios',
      npm_config_format: 'make-ios',
      npm_config_node_engine: 'chakracore',
      npm_config_arch: target.arch,
      npm_config_nodedir: await getNodeMobileRoot(),
      npm_config_build_from_source: 'true',
      ...process.env,
    },
  })
  console.log('Rebuilt', target.triple)
}

const ignoredExts = ['.dSYM', '.d.ts', '.dll', '.so']

// returns whether the dir was kept
async function pruneDir(dir: string): Promise<boolean> {
  const contents = await fs.readdir(dir, { withFileTypes: true })
  const didKeep = await Promise.all(contents.map(async ent => {
    const file = join(dir, ent.name)
    if (ignoredExts.some(ext => file.endsWith(ext))) {
      await fs.rm(file, { recursive: true, force: true })
      return false
    }
    if (ent.isDirectory()) {
      return pruneDir(file)
    }
    return true
  }))
  // don't remove the directory if any of its children were kept
  if (didKeep.includes(true)) return true
  await fs.rm(dir, { recursive: true, force: true })
  return false
}

async function buildJS(prod: boolean, watch: boolean, register: boolean): Promise<void> {
  await fs.rm(resources, { recursive: true, force: true })
  await fs.mkdir(resources)
  await dropboxIgnoreDir(resources)
  // TODO: Use webpack instead of ncc for the node stuff as well
  // (would make building simpler and more efficient)
  await run('webpack', [], { cwd: root })
  for (const dirname of await fs.readdir(textsPackages)) {
    if (!dirname.startsWith('platform-')) continue
    const dir = join(textsPackages, dirname, 'binaries')
    if (await fs.stat(dir).then(() => false).catch(() => true)) continue
    await fs.cp(dir, join(resources, dirname), { recursive: true })

    if (dirname === 'platform-signal') {
      const modules = [
        // @signalapp/libsignal-client
        'uuid', 'better-sqlite', 'bindings', 'file-uri-to-path',
      ]
      const copyToPath = join(resources, 'node_modules/@signalapp/libsignal-client')
      await fs.mkdir(copyToPath, { recursive: true })
      await fs.cp('node_modules/@signalapp/libsignal-client', copyToPath, { recursive: true })
      for (const mod of modules) {
        await fs.cp(join('node_modules', mod), join(resources, 'node_modules/', mod), { recursive: true })
      }
    }
  }

  await fs.cp(join(__dirname, 'ios-entry.js'), join(resources, 'ios-entry.js'))

  const nccFlags = [
    ...(prod ? ['--minify'] : []),
    ...(watch ? ['--watch'] : []),
    '--source-map',
    // source-map-support increases launch time by like 10 sec
    ...(register ? [] : ['--no-source-map-register']),
    '--target', esVersion,
  ]

  await Promise.all([
    run('ncc', [
      'build',
      ...nccFlags,
      '--out', resources,
    ], { cwd: root }),
    run('ncc', [
      'build',
      'src/worker/other-worker.ts',
      ...nccFlags,
      '--out', join(resources, 'ow.js'),
    ], { cwd: root }),
  ])

  await pruneDir(resources)
}

const BUNDLE_ID_PREFIX = 'com.texts'
async function processNodeFiles(target: Target, dir: string, beforeSigning?: (execPath: string) => Promise<void>): Promise<Set<string>> {
  const modules = new Set<string>()
  const execPaths: string[] = []
  const dylibFiles = new Map<string, string>() // moduleName : oldName
  for await (const file of findNodeFiles(dir)) {
    const ext = path.extname(file)
    let moduleName = path.basename(file, ext)

    // skip node.napi.node files from modules that uses node-gyp-build.
    // If you have native modules that use node-gyp-build make sure to copy it to build/Release
    // and change it's name to avoid conflicts with other modules. Fix the require call to use the
    // new name as well. Check TextsHQ/libsignal-client/node for an example.
    if (moduleName === 'node.napi') continue

    // some files from building rust native modules seems to be included
    // this should skip those. No native modules follow the same naming scheme
    // so this should be safe
    if (/\w+_\w+-[a-f0-9]+/.test(moduleName)) continue

    if (ext === '.dylib') {
      const oldName = moduleName
      if (moduleName.startsWith('lib')) {
        moduleName = moduleName.slice(3)
      }
      dylibFiles.set(moduleName, oldName)
    }
    if (modules.has(moduleName)) {
      console.warn('warning: Duplicate native Node module', moduleName)
      continue
    }
    modules.add(moduleName)
    const framework = join(frameworks, target.triple, `${moduleName}.framework`)
    const execPath = join(framework, moduleName)
    execPaths.push(execPath)
    await fs.rm(framework, { recursive: true, force: true })
    await fs.mkdir(framework, { recursive: true })
    await fs.rename(file, execPath)
    // node checks for the existence of native modules in the fs before requiring, so
    // write stubs at those paths
    if (ext === '.node') await fs.writeFile(file, 'NODEJS_MOBILE_RELOCATED\n')
    await fs.writeFile(join(framework, 'Info.plist'), `<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
  <dict>
    <key>CFBundleDevelopmentRegion</key>
    <string>English</string>
    <key>CFBundleExecutable</key>
    <string>${moduleName}</string>
    <key>CFBundleIdentifier</key>
    <string>${BUNDLE_ID_PREFIX}.${sanitizeModuleName(moduleName)}</string>
    <key>CFBundleInfoDictionaryVersion</key>
    <string>6.0</string>
    <key>CFBundlePackageType</key>
    <string>FMWK</string>
    <key>CFBundleShortVersionString</key>
    <string>1.0</string>
    <key>CFBundleSignature</key>
    <string>????</string>
    <key>CFBundleVersion</key>
    <string>1</string>
    <key>MinimumOSVersion</key>
    <string>${MinimumOSVersion}</string>
  </dict>
</plist>
`)
    // hackily special-case NodeAPI so that the xcframework includes a swiftmodule
    // (because we want to import it in texts-swiftui-app)
    if (moduleName === 'NodeAPI') {
      const modulesDir = join(framework, 'Modules', `${moduleName}.swiftmodule`)
      await fs.mkdir(modulesDir, { recursive: true })
      const buildDir = await path.resolve(await fs.realpath(join(root, 'pas-native/build/PASNative.node')), '..')
      await run('/usr/bin/sed', [
        '-i', '',
        // we currently need to add -enable-experimental-concurrency due to https://github.com/apple/swift/issues/58478
        '-e', `s/swift-module-flags: -target [^ ]*/swift-module-flags: -target ${target.triple} -enable-experimental-concurrency/`,
        join(buildDir, `${moduleName}.swiftinterface`),
      ])
      await Promise.all(['swiftmodule', 'swiftdoc', 'swiftinterface'].flatMap(
        e => target.swiftmoduleTriples.map(
          t => fs.copyFile(
            join(buildDir, `${moduleName}.${e}`),
            join(modulesDir, `${t}.${e}`),
          ),
        ),
      ))
      // allows cmd+click to work in Xcode (at least for local builds of pas-ios)
      const projectDir = join(modulesDir, 'Project')
      await fs.mkdir(projectDir)
      target.swiftmoduleTriples.map(
        t => fs.copyFile(
          join(buildDir, `${moduleName}.swiftsourceinfo`),
          join(projectDir, `${t}.swiftsourceinfo`),
        ),
      )
    }
    console.log('Processed native Node module:', moduleName)
  }
  const lcArgs = [
    // SPM's build system builds dynamic libs as `@rpath/libFoo.dylib`,
    // whereas Xcode's uses `@rpath/Foo.framework/Foo`. The latter is what
    // we actually use so we update our load commands accordingly.
    ...[...dylibFiles.keys()].flatMap(moduleName => [
      '-change',
      `@rpath/${dylibFiles.get(moduleName)}.dylib`,
      `@rpath/${moduleName}.framework/${moduleName}`,
    ]),
  ]
  for (const execPath of execPaths) {
    const moduleName = path.basename(execPath)
    // Xcode's SPM integration decides that we should link against the
    // binary frameworks even if we aren't using their symbols; however, we
    // can't let native modules load before Node wants to because self-registration
    // is finicky. Therefore we masquerade as NodeMobile.
    const idModule = dylibFiles.has(moduleName) ? moduleName : 'NodeMobile'
    await run(
      'install_name_tool',
      [
        ...lcArgs,
        '-id', `@rpath/${idModule}.framework/${idModule}`,
        execPath,
      ],
    )
    if (beforeSigning) await beforeSigning(execPath)
    await run(
      'codesign',
      ['-fs', '-', execPath],
    )
  }
  return modules
}

function areSetsEqual<T>(a: Set<T>, b: Set<T>): boolean {
  if (a.size !== b.size) return false
  if (a.size === 0 /* === b.size */) return true
  return typeof [...a].find(e => !b.has(e)) === 'undefined'
}

async function armToSim(execPath: string, outputPath?: string) {
  const buildVersion = await runAndCapture('vtool', ['-show-build', execPath])
  // LC_BUILD_VERSION uses `minos`, LC_VERSION_MIN_IPHONEOS uses `version`
  const minos = buildVersion.match(/(?:minos|version) (.*)/)[1]
  const sdk = buildVersion.match(/sdk (.*)/)[1]
  await run('vtool', [
    '-set-build-version', 'iossim', minos, sdk,
    '-replace', '-output', outputPath || execPath, execPath,
  ])
}

async function createXCFramework(paths: readonly string[], output: string) {
  await run(
    'xcodebuild',
    [
      '-create-xcframework',
      ...paths.flatMap(p => ['-framework', p]),
      '-output', output,
    ],
  )
}

async function createFatBinary(files: readonly string[], output: string) {
  await run('lipo', [
    '-create', ...files,
    '-output', output,
  ])
}

/*
// "install" a patched NodeMobile.xcframework (with arm sim support) into built-frameworks
async function installNodeMobile() {
  const nodeMobileDest = join(frameworks, 'NodeMobile.xcframework')
  const nodeMobileTmp = join(frameworks, 'NodeMobile-tmp.xcframework')
  await run('/usr/bin/tar', [
    '-C', frameworks,
    '-xzf', join(nodeMobileRoot, 'NodeMobile.xcframework.tar.zip'),
  ])
  const simPath = join(nodeMobileDest, 'ios-x86_64-simulator/NodeMobile.framework/NodeMobile')
  const armPath = join(nodeMobileDest, 'ios-arm64/NodeMobile.framework/NodeMobile')
  const simTmpPath = `${armPath}-sim`
  await armToSim(armPath, simTmpPath)
  await createFatBinary([simTmpPath, simPath], simPath)
  await fs.rm(simTmpPath, { recursive: true, force: true })
  await fs.rename(nodeMobileDest, nodeMobileTmp)
  const currPaths = (await fs.readdir(nodeMobileTmp)).filter(f => f !== 'Info.plist')
  await createXCFramework(
    currPaths.map(p => join(nodeMobileTmp, p, 'NodeMobile.framework')),
    nodeMobileDest,
  )
  await fs.rm(nodeMobileTmp, { recursive: true, force: true })
}
*/

async function fullBuild(selectedTargets: readonly string[] = allTargets, register: boolean, noSim: boolean) {
  const unknown = selectedTargets.filter(t => typeof targets[t] === 'undefined')
  if (unknown.length) {
    console.error('unknown targets', unknown)
    console.log('available:', allTargets)
    exit(1)
  }
  console.log('building targets', selectedTargets)
  await fs.rm(frameworks, { recursive: true, force: true })
  await fs.mkdir(frameworks)
  await dropboxIgnoreDir(frameworks)
  let modules: Set<string>
  const finalTargets = selectedTargets.map(t => targets[t])
  for (const targetName of selectedTargets) {
    const target = targets[targetName]
    if (typeof target === 'undefined') {
      console.warn('unknown target', targetName)
      continue
    }

    // skip simulator if --no-sim
    if (noSim && target.triple.includes('sim')) continue

    await runRebuild(target)
    await buildJS(true, false, register)
    if (target.arch === 'arm64' && target.platform === 'iphoneos' && !noSim) {
      const resourcesCopy = `${resources}-copy`
      await fs.rm(resourcesCopy, { recursive: true, force: true })
      await run('cp', ['-a', resources, resourcesCopy])
      await processNodeFiles(armSimTarget, resourcesCopy, armToSim)
      await fs.rm(resourcesCopy, { recursive: true, force: true })
      finalTargets.push(armSimTarget)
    }
    const newModules = await processNodeFiles(target, resources)
    if (typeof modules !== 'undefined' && !areSetsEqual(modules, newModules)) {
      console.warn('warning: modules differ between architectures', modules, newModules)
    }
    modules = newModules
  }

  // group targets by platform
  const groupedTargets = finalTargets.reduce((acc, cur) => {
    if (acc[cur.platform]) {
      acc[cur.platform].push(cur)
    } else {
      acc[cur.platform] = [cur]
    }
    return acc
  }, {} as Record<string, Target[]>)

  for (const module of modules) {
    const platformTargets: Target[] = []

    for (const platform of Object.keys(groupedTargets)) {
      const allPlatformTargets = [...groupedTargets[platform]]
      const firstTarget = allPlatformTargets.shift()
      if (allPlatformTargets.length) {
        // lipo the rest into firstTarget
        await createFatBinary(
          [firstTarget, ...allPlatformTargets].map(t => t.pathForModule(module, true)),
          firstTarget.pathForModule(module, true),
        )
        const firstModulesDir = join(firstTarget.pathForModule(module, false), 'Modules/')
        if (await fs.access(firstModulesDir).then(() => true).catch(() => false)) {
          // also merge swiftmodules
          await run('/usr/bin/rsync', [
            '-a',
            ...allPlatformTargets.map(t => join(t.pathForModule(module, false), 'Modules/')),
            firstModulesDir,
          ])
        }
      }
      platformTargets.push(firstTarget)
    }

    await createXCFramework(
      platformTargets.map(t => t.pathForModule(module, false)),
      join(frameworks, `${module}.xcframework`),
    )
  }

  await Promise.all(
    finalTargets.map(
      t => fs.rm(
        join(frameworks, t.triple),
        { recursive: true, force: true },
      ),
    ),
  )

  // await installNodeMobile()
}

// returns whether a full build was performed
async function fullBuildIfNeeded(register: boolean, noSim: boolean): Promise<boolean> {
  if (await fs.access(frameworks).then(() => true).catch(() => false)) {
    return false
  }
  console.log('Frameworks dir doesn\'t exist. Running a full build...')
  await fullBuild(allTargets, register, noSim)
  return true
}

async function main() {
  const { argv } = process

  const args = argv.splice(2)
  const options: string[] = []
  while (args.length && args[0].startsWith('-')) {
    options.push(args.shift())
  }

  if (options.includes('-h') || options.includes('--help')) {
    console.log('Usage: yarn build [--register] [--no-sim] [target...|js|watch|all]')
    return
  }

  const register = options.includes('--register')
  const noSim = options.includes('--no-sim')

  switch (args[0] || 'all') {
    case 'js':
      if (await fullBuildIfNeeded(register, noSim)) return
      return buildJS(false, false, register)
    case 'watch':
      await fullBuildIfNeeded(register, noSim)
      return buildJS(false, true, register)
    case 'all':
      return fullBuild(allTargets, register, noSim)
    default:
      return fullBuild(args, register, noSim)
  }
}
main()
