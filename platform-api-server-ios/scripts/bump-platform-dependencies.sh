#!/usr/bin/env bash

PACKAGES=($(jq -r '.dependencies | keys[]' package.json))
PACKAGES_URLS=($(jq -r '.dependencies | values[]' package.json))

YARN_PACKAGES_TO_UPGRADE=""

for I in "${!PACKAGES[@]}"; do
  PACKAGE="${PACKAGES[$I]}"
  URL="${PACKAGES_URLS[$I]}"

  if [[ "${PACKAGE}" != @textshq/platform-* ]]; then
    continue
  fi

  YARN_PACKAGES_TO_UPGRADE+="${PACKAGE}@${URL} "
done

yarn up ${YARN_PACKAGES_TO_UPGRADE}
