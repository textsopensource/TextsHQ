const TerserPlugin = require('terser-webpack-plugin')
const path = require('path')
const webpack = require('webpack')

const PROXIES_DIR = path.join(__dirname, 'src/renderer/proxies')
const BROWSER_ALIASES = {
  // built-in node modules
  fs: path.join(PROXIES_DIR, 'fs.ts'),
  os: path.join(PROXIES_DIR, 'os.ts'),
  worker_threads: path.join(PROXIES_DIR, 'worker_threads.ts'), // undefined
}

module.exports = {
  node: {
    __dirname: false,
    __filename: false,
  },
  entry: {
    renderer: './src/renderer/index.tsx',
  },
  output: {
    path: path.resolve(__dirname, 'Sources/PASMobile/built-resources'),
    filename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /baileys-md\/.+\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            plugins: ['@babel/plugin-proposal-class-properties'],
          },
        },
      },
      {
        test: /\.tsx?$/,
        use: [{
          loader: 'ts-loader',
          options: {
            configFile: 'tsconfig.renderer.json',
            transpileOnly: true,
          },
        }],
        exclude: /node_modules/,
      },
      {
        test: /\.(scss|png)?$/,
        use: 'null-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    modules: [
      'node_modules',
    ],
    extensions: ['.tsx', '.ts', '.js', '.jsx'],
    alias: {
      ...BROWSER_ALIASES,
    },
    fallback: {
      buffer: require.resolve('buffer/'),
      crypto: require.resolve('crypto-browserify'),
      stream: require.resolve('stream-browserify'),
    },
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.platform': JSON.stringify('ios'),
    }),
    new webpack.ProvidePlugin({
      Buffer: ['buffer', 'Buffer'],
    }),
  ],
  optimization: {
    minimizer: [
      new TerserPlugin({
        parallel: true,
        extractComments: false,
        terserOptions: {
          ecma: 2020,
          keep_fnames: true,
        },
      }),
    ],
  },
  watchOptions: {
    ignored: /node_modules/,
  },
  devtool: 'source-map',
  mode: 'production',
}
