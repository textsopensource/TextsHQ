# platform-api-server-ios

## Building

Add this to `~/.yarnrc.yml`:
```yaml
npmScopes:
  textshq:
    npmPublishRegistry: https://npm.pkg.github.com
    npmRegistryServer: https://npm.pkg.github.com
    npmAuthToken: YOUR_GH_TOKEN_HERE
```

Then run:
```sh
git clone --depth 1 https://github.com/TextsHQ/platform-api-server-ios
cd platform-api-server-ios
yarn # installs deps
yarn build # builds the JS + native modules
```

## Development

Once you've finished the setup, you can modify and rebuild just the JS with
```sh
yarn build js
```

Or watch source changes with
```sh
yarn dev
```

## Deploying

```
yarn
yarn build
scripts/deploy.sh X.X.X
git reset @^ --hard
```

## Running

### Inside texts-swiftui-app

In `texts-swiftui-app/Modules/TextsCore/Package.swift`, change the PASMobile dependency to `.package(name: "PASMobile", path: "/path/to/platform-api-server-ios")`. Make sure you don't commit this change!

### Locally

Run the server with `node Sources/PASMobile/built-resources/index.js JSON_OPTIONS_HERE`. `JSON_OPTIONS_HERE` is a JSON string comprising of fields described in `src/env.ts`:
```json
{
  "machineID": "hashed device ID",
  "transportNonce": "random cryptographically secure string",
  "httpPort": 12345
}
```

For instance, `node Sources/PASMobile/built-resources/index.js '{"machineID":"hashed device ID","transportNonce":"random cryptographically secure string","httpPort":12345}'`
