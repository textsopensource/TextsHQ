// swift-tools-version:5.4

import PackageDescription
import Foundation

let package = Package(
    name: "PASMobile",
    platforms: [.iOS("14.0"), .macOS("11.0")],
    products: [
        .library(
            name: "PASShared",
            type: .dynamic,
            targets: ["PASShared"]
        )
    ],
    targets: [
        .target(name: "PASShared")
    ]
)

// pas-native depends on this package (for PASShared), but when it does so, the built-*
// files aren't necessarily generated. Since all we need in that pass is PASShared, we
// can simply avoid declaring the remaining products.

if ProcessInfo.processInfo.environment["PASMOBILE_BUILD"] != "1" {
    var nativeModulesSet = (try? Set(
        FileManager.default.contentsOfDirectory(
            at: URL(fileURLWithPath: #filePath).deletingLastPathComponent().appendingPathComponent("built-frameworks"),
            includingPropertiesForKeys: nil,
            options: []
        )
        .filter { $0.pathExtension == "xcframework" }
        .map { $0.deletingPathExtension().lastPathComponent }
    )) ?? []
    // if the package is evaluated in a sandbox, listing might fail.
    // However, we still want to always include NodeAPI (because Texts
    // actually imports it) and exclude PASShared (because we manually
    // declare it)
    nativeModulesSet.subtract([
        "PASShared",
        "NodeMobile"
    ])
    nativeModulesSet.formUnion([
        "NodeAPI"
    ])
    let nativeModules = Array(nativeModulesSet)

    if nativeModules.isEmpty {
        print("warning: No native modules found")
    }

    package.products += [
        .library(
            name: "PASMobile",
            targets: ["PASMobile"]
        ),
        .library(
            name: "NodeAPI",
            targets: ["NodeAPI"]
        )
    ]

    package.targets += [
        .target(
            name: "CPASMobile",
            dependencies: ["NodeMobile"]
        ),
        .target(
            name: "PASMobile",
            dependencies: ["CPASMobile"] + nativeModules.map { .byName(name: $0) },
            resources: [.copy("built-resources")]
        ),
        .binaryTarget(
            name: "NodeMobile",
            url: "https://github.com/1Conan/nodejs-mobile/releases/download/v16.16.0-ios/NodeMobile.xcframework.zip",
            checksum: "c68e07ddbfefa1e5855cacdc6ace54e73973c474e7d543f8caa1d3bf93c0d21e"
        )
    ] + nativeModules.map {
        .binaryTarget(name: $0, path: "built-frameworks/\($0).xcframework")
    }
}
