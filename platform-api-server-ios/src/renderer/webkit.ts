declare global {
  interface Window {
    webkit: {
      messageHandlers: {
        [handler: string]: {
          postMessage<T>(params: any): Promise<T>
        }
      }
    }
  }
}

export {}
