/* eslint-disable global-require */
import textsBase from '../common/base-globals'

globalThis.texts = {
  ...textsBase,
  React: require('react'),
  ReactJSXRuntime: require('react/jsx-runtime'),
  Sentry: {},
}
