import './inject-globals'
import ReactDOM from 'react-dom'
import App from './App'

// document.getElementById('root').innerText = 'hello, world!'
ReactDOM.render(<App />, document.getElementById('root'))
