import { CB_METHODS } from '@textshq/platform-api-server/dist/relayer-constants'
import type { Awaitable, PlatformAPI } from '@textshq/platform-sdk'
import './webkit'
import serializer from '../common/serializer'

const textsCallbacks: Record<string, Function> = {}
globalThis.textsCallbacks = textsCallbacks

export const METHODS: Set<keyof PlatformAPI> = new Set([
  'init',
  'dispose',

  'getCurrentUser',

  'login',
  'logout',
  'serializeSession',

  'searchUsers',
  'searchMessages',

  'getPresence',
  'getCustomEmojis',

  'getThreads',
  'getMessages',

  'getThread',
  'getMessage',
  'getUser',

  'createThread',
  'updateThread',
  'deleteThread',
  'reportThread',

  'sendMessage',
  'editMessage',
  'addReaction',
  'removeReaction',
  'forwardMessage',

  'deleteMessage',

  'sendActivityIndicator',
  'sendReadReceipt',

  'changeThreadImage',

  'addParticipant',
  'removeParticipant',
  'changeParticipantRole',

  'markAsUnread',
  'archiveThread',
  'pinThread',
  'notifyAnyway',

  'onThreadSelected',
  'loadDynamicMessage',

  'takeoverConflict',
  'getOriginalObject',

  'registerForPushNotifications',
  'unregisterForPushNotifications',

  'getAsset',
  'handleDeepLink',
  'reconnectRealtime',
])

const PlatformAPIRelayer = class PlatformAPIRelayer {
  dispose: () => Awaitable<void>

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(accountID: string, platformName: string) {
    const callMethod = async <T>(methodName: string, hasCallback: boolean, args: any[] = []): Promise<T> => {
      const result = await window.webkit.messageHandlers.api.postMessage({
        methodName,
        hasCallback: hasCallback ? '1' : '0',
        args: serializer.encode(args).toString('base64'),
      })
      if (typeof result === 'string') {
        return serializer.decode(Buffer.from(result, 'base64'))
      }
    }

    // todo: refactor to use Proxy instead
    for (const methodName of METHODS) {
      this[methodName] = (...args: any[]) => callMethod(methodName, false, args)
    }

    for (const methodName of CB_METHODS) {
      this[methodName] = (cbArg: Function) => {
        textsCallbacks[methodName] = payload => {
          cbArg(serializer.decode(Buffer.from(payload as string, 'base64')).cbData)
        }
        callMethod(methodName, true)
      }
    }

    this.dispose = () => {
      for (const key of Object.keys(textsCallbacks)) delete textsCallbacks[key]
      return callMethod('dispose', false)
    }
  }
} as { new(accountID: string, platformName: string): PlatformAPI }

export default PlatformAPIRelayer
