// if you modify this file, also modify included-platforms.ts

import telegram from '@textshq/platform-telegram/dist/info'
import signal from '@textshq/platform-signal/dist/info'
import reddit from '@textshq/platform-reddit/dist/info'
import whatsapp from '@textshq/platform-whatsapp/dist/info'
import messenger from '@textshq/platform-messenger/dist/info'
import twitter from '@textshq/platform-twitter/dist/info'
import instagram from '@textshq/platform-instagram/dist/info'
import test from '@textshq/platform-test/dist/info'
import discord from '@textshq/platform-discord/dist/info'
import linkedin from '@textshq/platform-linkedin/dist/info'
import slack from '@textshq/platform-slack/dist/info'
import openai from '@textshq/platform-openai/dist/info'

const includedPlatformInfos = {
  telegram,
  signal,
  instagram,
  twitter,
  'whatsapp-baileys': whatsapp,
  'fb-messenger': messenger,
  discord,
  linkedin,
  reddit,
  slack,
  openai,
  test,
}

export default includedPlatformInfos
