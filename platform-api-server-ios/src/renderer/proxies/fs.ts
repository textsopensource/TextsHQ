export const promises = new Proxy({}, {
  get: (target, key) =>
    (...args: any[]) =>
      globalThis.rt.callObjProxyMethod({
        objName: 'fsp',
        methodName: key,
        args,
      }),
})

const fsProxy: any = new Proxy({}, {
  get: (target, key) =>
    (...args: any[]) => { throw Error(`fs.${String(key)} cannot be proxied`) },
})

export const readFileSync = (...args: any[]) => { throw Error('fs.readFileSync cannot be proxied') }
export const watch = () => { throw Error('fs.watch cannot be proxied') }

const fsCallbackProxy: any = new Proxy({}, {
  get: (target, key) =>
    (arg: any, callback: Function) => {
      promises[key](arg)
        .then(data => {
          callback(null, data)
        })
        .catch(err => {
          callback(err)
        })
    },
})

export const { stat } = fsCallbackProxy

export default fsProxy
