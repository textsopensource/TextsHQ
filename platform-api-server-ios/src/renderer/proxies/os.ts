// forked from https://github.com/CoderPuppy/os-browserify/blob/master/browser.js

// export const endianness = () => 'LE'

// export const hostname = () => (typeof window.location !== 'undefined' ? window.location.hostname : '')

// export const loadavg = () => []
// export const cpus = () => []
// export const uptime = () => 0
// export const freemem = () => Number.MAX_VALUE
// export const totalmem = () => Number.MAX_VALUE

// export const type = () => 'Browser'
// export const networkInterfaces = () => {}
// export const getNetworkInterfaces = networkInterfaces
// export const tmpDir = tmpdir

// export const tmpdir = () => '/tmp'
// export const EOL = '\n'
export const userInfo = () => globalThis.INITIAL_STATE.os_userInfo
export const release = () => globalThis.INITIAL_STATE.os_release
export const arch = () => globalThis.INITIAL_STATE.os_arch
export const platform = () => globalThis.INITIAL_STATE.os_platform
export const homedir = () => globalThis.INITIAL_STATE.os_homedir

export const version = release

const os = {
  homedir,
  platform,
  arch,
  release,
  version,
  userInfo,
}
export default os
