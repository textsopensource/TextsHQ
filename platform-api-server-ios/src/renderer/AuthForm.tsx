import type { LoginCreds, LoginResult } from '@textshq/platform-sdk'

import './webkit'
import PlatformAPIRelayer from './PlatformAPIRelayer'
import infoMap from './included-infos'
import serializer from '../common/serializer'

type AuthFormProps = {
  platformName: string
  accountID: string
  isReauthing: boolean
}

const AuthForm: React.FC<AuthFormProps> = ({ platformName, accountID, isReauthing }) => {
  if (!(platformName in infoMap)) {
    return <div>{platformName} info not present</div>
  }
  const { auth: AuthComponent } = infoMap[platformName]
  const api = new PlatformAPIRelayer(accountID, platformName)
  const login = async (creds?: LoginCreds): Promise<LoginResult> => {
    const result = await api.login(creds)
    window.webkit.messageHandlers.handleLoginResult.postMessage(serializer.encode(result).toString('base64'))
    return result
  }
  return <AuthComponent {...{ api, login, isReauthing }} />
}

export default AuthForm
