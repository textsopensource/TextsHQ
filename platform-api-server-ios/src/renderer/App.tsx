import { FC, Suspense } from 'react'
import AuthForm from './AuthForm'
import ErrorBoundary from './ErrorBoundary'

declare global {
  interface Window {
    textsProps: {
      platformName: string
      accountID: string
      isReauthing: boolean
    }
  }
}

const App: FC = () => {
  const c = (
    <Suspense fallback={<div>Loading...</div>}>
      <ErrorBoundary>
        <AuthForm {...globalThis.window.textsProps} />
      </ErrorBoundary>
    </Suspense>
  )
  return c
}

export default App
