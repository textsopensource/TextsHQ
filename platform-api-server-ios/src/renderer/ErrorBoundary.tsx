import { Component } from 'react'
// import * as Sentry from '@sentry/browser'

const ErrorRenderer: React.FC<{
  error: Error
  errorInfo: React.ErrorInfo
}> = ({ error, errorInfo = null }) => (
  <section style={{ padding: '1em' }}>
    <h2>Looks like an error occurred</h2>
    <button type="button" onClick={() => window.location.reload()}>Reload</button>
    <p style={{ fontFamily: 'monospace' }}>{error.message}</p>
    <details style={{ fontFamily: 'monospace', whiteSpace: 'pre-wrap' }}>
      <summary>Error Info</summary>
      {errorInfo && errorInfo.componentStack}<br /><br />
      {error.stack}
    </details>
  </section>
)

class ErrorBoundary extends Component {
  constructor(props: Readonly<{}>) {
    super(props)
    this.state = { error: null, errorInfo: null }
  }

  componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
    // Sentry.withScope(scope => {
    //   // @ts-expect-error
    //   scope.setExtras(errorInfo)
    //   const eventId = Sentry.captureException(error)
    //   Sentry.showReportDialog({
    //     eventId,
    //     title: 'Looks like an error occurred',
    //     subtitle: 'If you have more details, let us know so we can fix it faster.',
    //     subtitle2: '',
    //   })
    // })
    console.error(error, errorInfo)
    this.setState({ error, errorInfo })
  }

  render() {
    const { children } = this.props
    const { error, errorInfo } = this.state as any
    if (!error) return children
    return <ErrorRenderer {...{ error, errorInfo }} />
  }
}

export default ErrorBoundary
