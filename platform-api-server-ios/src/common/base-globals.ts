import IS_DEV from './is-dev'
import { USER_AGENT } from './constants'

const isLoggingEnabled = IS_DEV

const texts = {
  IS_DEV,
  isLoggingEnabled,
  log: isLoggingEnabled ? (...args: any[]) => console.log(...args) : () => {},
  error: isLoggingEnabled ? (...args: any[]) => console.error(...args) : () => {},
  constants: {
    USER_AGENT,
  },
}

export default texts
