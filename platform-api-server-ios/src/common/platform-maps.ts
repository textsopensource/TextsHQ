import type { LoadPlatformAPIMap, PlatformInfoMap } from '@textshq/platform-api-server/dist/types'

import includedPlatforms from '../included-platforms'

function getPlatformMaps() {
  const loadPlatformAPIMap: LoadPlatformAPIMap = {}
  const platformInfoMap: PlatformInfoMap = {}

  for (const platformName of Object.keys(includedPlatforms)) {
    console.log('loading platform', platformName)
    loadPlatformAPIMap[platformName] = () => includedPlatforms[platformName].api
    platformInfoMap[platformName] = includedPlatforms[platformName].info
  }

  return { loadPlatformAPIMap, platformInfoMap }
}

export default getPlatformMaps()
