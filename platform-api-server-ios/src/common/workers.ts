import path from 'path'
import { Worker } from 'worker_threads'
import onExit from 'exit-hook'
import bluebird from 'bluebird'
// import path from 'path'

import machineID from './machine-id'
import { WorkerName } from './worker-constants'

const MAX_RETRY_ATTEMPTS = 10

// @ts-expect-error
const { Sentry } = globalThis

const isDebug = process.env.NODE_ENV === 'development' || process.argv.includes('--debug')

const workers = new Set<Worker>()

function disposeWorker(worker: Worker) {
  worker?.postMessage('cleanup')
  workers.delete(worker)
}

// const compiled = __filename.endsWith('.bin')
export function runWorker(workerName: WorkerName, workerData: any, workerFileName: string) {
  let worker: Worker
  // let closing = false
  let retryAttempt = 0
  function createWorker() {
    console.log(workerName, 'starting worker')
    const startTime = Date.now()
    worker = new Worker(workerFileName, { // compiled ? path.join(__dirname, 'start.js') :
      workerData: {
        workerName,
        machineID,
        ...workerData,
      },
    })
    worker.on('online', () => {
      console.log(workerName, 'took', Date.now() - startTime, 'ms to be online')
      workers.add(worker)
      retryAttempt = 0
    })
    worker.on('error', err => {
      disposeWorker(worker)
      console.error(workerName, 'worker error event', err)
      Sentry.captureException(err, { extra: { workerName } })
      if (++retryAttempt <= MAX_RETRY_ATTEMPTS) {
        setTimeout(createWorker, Math.max(100 + (2 ** retryAttempt + Math.random() * 100), 2000))
      } else {
        console.error(workerName, 'max retry attempts reached')
      }
    })
    worker.on('exit', exitCode => {
      // if (closing) return
      // cleanup()
      workers.delete(worker)
      console.error(workerName, 'worker exit event', exitCode)
      // setTimeout(createWorker, 100)
    })
    return worker
  }
  // since worker threads don't support signals
  onExit(() => {
    console.log('[exit handler] disposing worker', workerName)
    disposeWorker(worker)
  })
  return createWorker()
}

export const runOtherWorker = (userDataDirPath: string) => (workerFilePath: string, workerData: any = {}) =>
  runWorker(
    WorkerName.OTHER_WORKER,
    {
      ...workerData,
      userDataDirPath,
      workerFilePath,
    },
    path.join(__dirname, 'ow.js'),
  )

export const postMessageToAllWorkers = (value: any) => {
  for (const worker of workers) {
    worker.postMessage(value)
  }
}

if (isDebug) {
  process.once('SIGINT', async () => {
    workers.forEach(w => {
      console.log('[SIGINT handler] disposing worker')
      disposeWorker(w)
    })
    while (workers.size > 0) {
      await bluebird.delay(100)
    }
    console.log('[SIGINT handler] calling process.exit')
    process.exit()
  })
}
