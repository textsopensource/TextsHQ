declare const __non_webpack_require__: NodeRequire

export default typeof __non_webpack_require__ === 'undefined' ? require : __non_webpack_require__
