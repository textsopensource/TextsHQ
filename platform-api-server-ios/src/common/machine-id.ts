import { isMainThread, workerData } from 'worker_threads'

// eslint-disable-next-line global-require
export default isMainThread ? require('../env').machineID : workerData.machineID
