import { UNKNOWN_DATE } from '@textshq/platform-sdk'
import { Packr } from 'msgpackr'

type BufferSerializer = {
  name: string
  encode: (data: any) => Buffer
  decode: (data: Buffer | Uint8Array) => any
}

const packr = new Packr({
  encodeUndefinedAsNil: true,
  useRecords: false,
  onInvalidDate: () => UNKNOWN_DATE,
})

const serializer: BufferSerializer = {
  name: 'msgpackr-ios',
  encode: packr.encode.bind(packr),
  decode: packr.decode.bind(packr),
}

export default serializer
