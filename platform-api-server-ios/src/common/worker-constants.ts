export enum WorkerName {
  PLATFORM_WORKER = 'platform_worker',
  SQLITE_WORKER = 'sqlite_worker',
  OTHER_WORKER = 'other_worker',
}
