import { Client as HttpClient, LogLevel } from 'rust-fetch'
import type FormData from 'form-data'
import type { FetchResponse, FetchOptions } from '@textshq/platform-sdk'

class Client {
  private httpClient = new HttpClient({
    logLevel: process.env.RF_LOG ? LogLevel.Debug : LogLevel.Off,
  })

  async requestAsString(url: string, opts?: FetchOptions): Promise<FetchResponse<string>> {
    const response = await this.httpClient.request<string>(url, {
      ...opts,
      // todo add support for Readable
      body: opts.body as string | Buffer | FormData,
      responseType: 'text',
    })
    return {
      statusCode: response.statusCode,
      headers: response.headers,
      body: response.body,
    }
  }

  async requestAsBuffer(url: string, opts?: FetchOptions): Promise<FetchResponse<Buffer>> {
    const response = await this.httpClient.request<Buffer>(url, {
      ...opts,
      body: opts.body as string | Buffer | FormData,
      responseType: 'binary',
    })
    return {
      statusCode: response.statusCode,
      headers: response.headers,
      body: response.body,
    }
  }
}

export const createHttpClient = () => new Client()
