import injectGlobals from './inject-globals'
import Sentry from './init-sentry'
import IS_DEV from './common/is-dev'
import { userDataDirPath } from './env'

injectGlobals(userDataDirPath)

require('./pas')

if (IS_DEV) {
  // in prod, sentry registers this callback
  // https://github.com/getsentry/sentry-javascript/blob/master/packages/node/src/integrations/onunhandledrejection.ts
  process.on('unhandledRejection', err => {
    console.error('unhandledRejection', err)
  })
}
process.on('uncaughtException', err => {
  console.error('uncaughtException', err)
  Sentry.captureException(err)
})
