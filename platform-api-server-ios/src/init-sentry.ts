import * as Sentry from '@sentry/node'

import pkg from '../package.json'
import IS_DEV from './common/is-dev'
import { SENTRY_DSN } from './common/constants'
import machineID from './common/machine-id'

Sentry.init({
  enabled: !IS_DEV,
  dsn: SENTRY_DSN,
  release: `${pkg.name}v${pkg.version}`,
  // beforeSend: (event: Event) => scrub(event),
  ignoreErrors: ['close'],
  integrations: integrations => integrations.filter(integration => !['Http', 'OnUncaughtException'].includes(integration.name)),
})
Sentry.setUser({ ip_address: '{{auto}}' })
Sentry.setTag('device_id', machineID);

(global as any).Sentry = Sentry

export default Sentry
