import { workerData } from 'worker_threads'

export const {
  machineID,
  userDataDirPath,
}: {
  machineID: string
  userDataDirPath: string
} = workerData || {}
