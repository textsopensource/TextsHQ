import { workerData } from 'worker_threads'
import injectGlobals from '../inject-globals'
import { userDataDirPath } from './worker-data'
import actualRequire from '../common/util/actual-require'

injectGlobals(userDataDirPath)
actualRequire(workerData.workerFilePath as string)
