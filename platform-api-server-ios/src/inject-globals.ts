import textsBase from './common/base-globals'
import { fetch, fetchStream } from './fetch'
import { createHttpClient } from './rust-fetch'
import Sentry from './init-sentry'
import { runOtherWorker } from './common/workers'

export default function injectGlobals(userDataDirPath: string) {
  globalThis.texts = {
    ...textsBase,
    fetch,
    fetchStream,
    createHttpClient: process.env.NO_RUST_FETCH
      ? require('./got-fetch').createHttpClient
      : createHttpClient,
    runWorker: runOtherWorker(userDataDirPath),
    Sentry,
    constants: {
      ...textsBase.constants,
      BUILD_DIR_PATH: __dirname,
    },
  }
}
