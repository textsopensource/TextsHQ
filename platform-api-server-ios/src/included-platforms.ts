// if you modify this file, also modify included-infos.ts

import type { Platform } from '@textshq/platform-sdk'

import telegram from '@textshq/platform-telegram'
import signal from '@textshq/platform-signal'
import reddit from '@textshq/platform-reddit'
import whatsapp from '@textshq/platform-whatsapp'
import messenger from '@textshq/platform-messenger'
import twitter from '@textshq/platform-twitter'
import instagram from '@textshq/platform-instagram'
import test from '@textshq/platform-test'
import discord from '@textshq/platform-discord'
import linkedin from '@textshq/platform-linkedin'
import slack from '@textshq/platform-slack'
import openai from '@textshq/platform-openai'

const includedPlatforms: { [platformName: string]: Platform } = {
  telegram,
  signal,
  instagram,
  twitter,
  'whatsapp-baileys': whatsapp,
  'fb-messenger': messenger,
  discord,
  linkedin,
  reddit,
  slack,
  openai,
  test,
}

export default includedPlatforms
