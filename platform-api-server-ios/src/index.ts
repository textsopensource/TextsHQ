import { bridge } from '@textshq/pas-native'

// catches top level errors; without this, Node will terminate the app
try {
  import('./entry')
} catch (e) {
  console.error('pas-ios crashed', e)
  bridge.didFail(e)
}
