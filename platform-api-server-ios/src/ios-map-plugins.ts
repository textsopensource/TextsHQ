import type { Plugin } from '@textshq/platform-api-server/dist/types'
import type { FetchURL, FetchInfo, Participant } from '@textshq/platform-sdk'
import type { Readable } from 'stream'
import platformMaps from './common/platform-maps'

const { platformInfoMap } = platformMaps

const inputPlugin: Plugin = {
  type: 'input',
  hooks: {
    init(platformName: string, accountID: string, args: any[]) {
      return [JSON.parse(args[0]), ...args.slice(1)]
    },
  },
}

const outputPlugin: Plugin = {
  type: 'output',
  hooks: {
    serializeSession(platformName: string, accountID: string, args: any[], result: any) {
      return JSON.stringify(result)
    },
    getUserProfileLink(platformName: string, accountID: string, args: [Participant]) {
      return platformInfoMap[platformName]?.getUserProfileLink(args[0])
    },
    async getAsset(platformName: string, accountID: string, args: any[], result: FetchURL | FetchInfo | Buffer | Readable): Promise<{ url: string } | { data: Buffer }> {
      if (typeof result === 'string') return { url: result }

      if (Buffer.isBuffer(result)) return { data: result }

      if (result !== null && typeof result === 'object') {
        const isReadableStream = (stream: any): stream is Readable =>
          typeof stream._read === 'function' && stream.readable !== false

        if (isReadableStream(result)) {
          const parts = []
          result.on('data', chunk => parts.push(chunk))
          const buf = await new Promise<Buffer>((resolve, reject) => {
            result.on('end', () => resolve(Buffer.concat(parts)))
            result.on('error', err => reject(err))
          })
          return { data: buf }
        }

        if ('url' in result) {
          return result
        }
      }

      throw new Error('Asset not found')
    },
  },
}

const iOSMapPlugins = [inputPlugin, outputPlugin]

export default iOSMapPlugins
