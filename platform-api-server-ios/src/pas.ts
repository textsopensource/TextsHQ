import PlatformAPIServer from '@textshq/platform-api-server/dist/PlatformAPIServer'
import type { PlatformAPIRelayerData, PushFunction } from '@textshq/platform-api-server/dist/types'
import { bridge } from '@textshq/pas-native'

// explicitly import msgpackr-extract on the node side since
// msgpackr uses a dynamic require that ncc doesn't pick up
import 'msgpackr-extract'
import 'better-sqlite'
import '@signalapp/libsignal-client'

// import getKeychainKey from './keys'
import serializer from './common/serializer'
import platformMaps from './common/platform-maps'
import iOSMapPlugins from './ios-map-plugins'

(async () => {
  const { loadPlatformAPIMap, platformInfoMap } = platformMaps

  const pas = new PlatformAPIServer({
    transportOptions: null,

    plugins: iOSMapPlugins,
    indexingDisabled: true,
    dataDirPath: null,

    platformInfoMap,
    loadPlatformAPIMap,
  })
  await pas.initPromise

  const pushFunction: PushFunction = data => {
    bridge.push(serializer.encode(data))
  }

  bridge.didLaunch(
    Object.values(platformInfoMap).map(serializer.encode),
    async (relayerData: PlatformAPIRelayerData) => {
      const decoded = serializer.decode(relayerData.args as unknown as Uint8Array)
      const ret = await pas.platformAPIStore.callMethod({
        ...relayerData,
        // FIXME: This is a hack; PlatformServer.swift should always pass an array
        // but currently it sometimes passes individual objects
        args: Array.isArray(decoded) ? decoded : [decoded],
      }, pushFunction)
      return serializer.encode(ret)
    },
  )

  // FIXME: implement pas.cleanup()
})()
