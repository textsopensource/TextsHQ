@_implementationOnly import CPASMobile
import Foundation

public enum PASRunner {
    public static var resources: URL {
        Bundle.module.url(forResource: "built-resources", withExtension: nil)!
    }

    public static func run(withArguments arguments: [String]) throws {
        let pasJSURL = resources.appendingPathComponent("index.js")
        let entryJSURL = resources.appendingPathComponent("ios-entry.js")
        let status = NodeRunner.startEngine(withArguments: ["node", "-r", entryJSURL.path, pasJSURL.path] + arguments)
        if status != 0 {
            throw NSError(domain: NSPOSIXErrorDomain, code: Int(status))
        }
    }
}
