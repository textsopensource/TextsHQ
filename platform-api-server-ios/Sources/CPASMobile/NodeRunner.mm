//
//  NodeRunner.mm
//  Texts
//
// Copyright (c) Texts HQ
//

#include <string>
#include "NodeRunner.h"

// we can include this from NodeMobile.h but xcframework header extraction
// is buggy
extern "C" int node_start(int argc, char *argv[]);

@implementation NodeRunner

// node's libUV requires all arguments being on contiguous memory.
+ (int) startEngineWithArguments:(NSArray*)arguments {
	int c_arguments_size = 0;

	// Compute byte size need for all arguments in contiguous memory.
	for (id argElement in arguments) {
		c_arguments_size += strlen([argElement UTF8String]);
		c_arguments_size++; // for '\0'
	}

	// Stores arguments in contiguous memory.
	char* args_buffer = static_cast<char*>(calloc(c_arguments_size, sizeof(char)));

	// argv
	char* argv[[arguments count]];

	// argc
	int argument_count = 0;

	// To iterate through the expected start position of each argument in args_buffer.
	char* current_args_position = args_buffer;

	//Populate the args_buffer and argv.
	for (id argElement in arguments) {
		const char* current_argument = [argElement UTF8String];

		// Copy current argument to its expected position in args_buffer
		strncpy(current_args_position, current_argument, strlen(current_argument));

		// Save current argument start position in argv and increment argc.
		argv[argument_count] = current_args_position;
		argument_count++;

		// Increment to the next argument's expected position.
		current_args_position += strlen(current_args_position) + 1;
	}

	// Start node, with argc and argv.
	int res = node_start(argument_count, argv);
	free(args_buffer);

	return res;
}

@end
