//
//  NodeRunner.h
//  Texts
//
// Copyright (c) Texts HQ
//

#ifndef NodeRunner_h
#define NodeRunner_h

#import <Foundation/Foundation.h>

@interface NodeRunner : NSObject
+ (int) startEngineWithArguments:(NSArray*)arguments;
@end

#endif
