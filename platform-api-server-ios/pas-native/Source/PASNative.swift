import NodeAPI
import PASShared

@main struct PASNative: NodeModule {
    let exports: NodeValueConvertible

    init() throws {
        exports = try (PASInit as! @NodeActor () throws -> NodeValueConvertible)()
    }
}
