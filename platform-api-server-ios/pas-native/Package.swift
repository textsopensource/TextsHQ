// swift-tools-version:5.4

import PackageDescription

let package = Package(
    name: "PASNative",
    platforms: [.iOS("14.0"), .macOS("11.0")],
    products: [
        .library(
            name: "PASNative",
            targets: ["PASNative"]
        ),
    ],
    dependencies: [
        .package(name: "PASMobile", path: ".."),
        .package(path: "../node_modules/node-swift"),
    ],
    targets: [
        .target(
            name: "PASNative",
            dependencies: [
                .product(name: "PASShared", package: "PASMobile"),
                .product(name: "NodeAPI", package: "node-swift"),
            ],
            path: "Source"
        ),
    ]
)
