class PASBridge {
  didLaunch(infoMap: any, callMethod: any): void
  didFail(error: any): void
  push(data: Buffer): void
}
export const bridge: PASBridge
