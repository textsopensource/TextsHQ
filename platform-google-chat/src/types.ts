export type GChatTokens = {
  accessToken: string
  refreshToken: string
  dynamiteToken: string
  accessExpires: Date
  dynamiteExpires: Date
}

export type Params = {
  sessionId?: string
  csessionid?: string
  lastArrayId: number
  lastOfs: number
  requestId: number
}
