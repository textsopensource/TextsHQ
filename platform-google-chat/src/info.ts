import { Attribute, MessageDeletionMode, PlatformInfo } from '@textshq/platform-sdk'
import icon from './icon'

const info: PlatformInfo = {
  name: 'google-chat',
  version: '0.0.1',
  displayName: 'Google Chat',
  deletionMode: MessageDeletionMode.UNSEND,
  icon,
  typingDurationMs: 10_000,
  reactions: {
    supported: {},
    canReactWithAllEmojis: true,
    allowsMultipleReactionsToSingleMessage: true,
  },
  attributes: new Set([
    Attribute.CAN_MESSAGE_EMAIL,
    Attribute.CAN_MESSAGE_USERNAME,
    Attribute.SUPPORTS_EDIT_MESSAGE,
    Attribute.SUPPORTS_PRESENCE,
    Attribute.SUPPORTS_LIVE_TYPING,
    Attribute.SUPPORTS_QUOTED_MESSAGES,
  ]),
  loginMode: 'browser',
  browserLogins: [{
    loginURL: 'https://accounts.google.com/o/oauth2/programmatic_auth?hl=en&scope=https%3A%2F%2Fwww.google.com%2Faccounts%2FOAuthLogin+https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email&client_id=936475272427.apps.googleusercontent.com&access_type=offline&delegated_client_id=576267593750-vfjl7lner4l26202v1l4i75h2ch452sl.apps.googleusercontent.com&top_level_cookie=1',
    authCookieName: 'oauth_code',
    userAgent: 'Chrome',
  }],
  attachments: {
    maxSize: {
      // https://support.google.com/chat/answer/7651457
      // "You can attach files up to 200 MB from your computer, mobile device, or Google Drive directly to Google Chat messages."
      image: 200 * 1024 * 1024,
      video: 200 * 1024 * 1024,
      audio: 200 * 1024 * 1024,
      files: 200 * 1024 * 1024,
    },
  },
}

export default info
