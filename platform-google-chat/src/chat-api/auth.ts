import { texts } from '@textshq/platform-sdk'
import type { GChatTokens } from '../types'

const OAUTH2_TOKEN_REQUEST_URL = 'https://accounts.google.com/o/oauth2/token'
const OAUTH2_CLIENT_ID = '936475272427.apps.googleusercontent.com'
const OAUTH2_CLIENT_SECRET = 'KWsJlkaMn1jGLxQpWxMnOox-'
const { constants } = texts
const { USER_AGENT } = constants

const commonHeaders = {
  'Accept-Language': 'en',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': USER_AGENT,
}

export class GChatAuth {
  private readonly http = texts.createHttpClient!()

  private tokens?: GChatTokens

  getTokens = async (): Promise<GChatTokens> => {
    if (this.tokens != null) {
      const currentDate = new Date()
      const deltaAcess = this.tokens.accessExpires.getTime() - currentDate.getTime()
      const deltaDynamite = this.tokens.dynamiteExpires.getTime() - currentDate.getTime()
      const tenMinutesMs = 10 * 1000 * 60
      if (deltaAcess < tenMinutesMs) {
        console.log('Access token expired.')
        await this.refreshOAuth()
      }
      if (deltaDynamite < tenMinutesMs) {
        console.log('Dynamite token expired.')
        await this.refreshDynamite(this.tokens.accessToken)
      }
      return this.tokens
    }
  }

  setTokens = (tokens: GChatTokens) => {
    this.tokens = tokens
  }

  authWithCode = async (authorizationCode: string): Promise<GChatTokens> => {
    const res = await this.http.requestAsString(OAUTH2_TOKEN_REQUEST_URL, {
      method: 'POST',
      form: {
        client_id: OAUTH2_CLIENT_ID,
        client_secret: OAUTH2_CLIENT_SECRET,
        code: authorizationCode,
        grant_type: 'authorization_code',
        redirect_uri: 'urn:ietf:wg:oauth:2.0:oob',
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        ...commonHeaders,
      },
    })

    if (res.statusCode === 200) {
      const json = JSON.parse(res.body)
      const accessToken = json.access_token
      const refreshToken = json.refresh_token
      const accessExpires = json.expires_in

      const expiration = new Date()
      expiration.setSeconds(expiration.getSeconds() + accessExpires)

      this.tokens = {
        accessToken,
        refreshToken,
        accessExpires: expiration,
        dynamiteToken: null,
        dynamiteExpires: null,
      }

      await this.refreshDynamite(accessToken)

      return this.tokens
    }

    return null
  }

  refreshOAuth = async (): Promise<GChatTokens> => {
    const res = await this.http.requestAsString(OAUTH2_TOKEN_REQUEST_URL, {
      method: 'POST',
      form: {
        client_id: OAUTH2_CLIENT_ID,
        client_secret: OAUTH2_CLIENT_SECRET,
        grant_type: 'refresh_token',
        refresh_token: this.tokens.refreshToken,
      },
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        ...commonHeaders,
      },
    })

    if (res.statusCode === 200) {
      const json = JSON.parse(res.body)
      const accessToken = json.access_token
      const accessExpires = json.expires_in
      const expiration = new Date()
      expiration.setSeconds(expiration.getSeconds() + accessExpires)

      await this.refreshDynamite(accessToken)

      this.tokens.accessToken = accessToken
      this.tokens.accessExpires = expiration
      return this.tokens
    }
    return null
  }

  private refreshDynamite = async (accessToken: string) => {
    const headers = {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/x-www-form-urlencoded',
      ...commonHeaders,
    }

    const issueTokenURL = 'https://oauthaccountmanager.googleapis.com/v1/issuetoken'

    const scope = [
      'https://www.googleapis.com/auth/dynamite',
      'https://www.googleapis.com/auth/drive',
      'https://www.googleapis.com/auth/mobiledevicemanagement',
      'https://www.googleapis.com/auth/notifications',
      'https://www.googleapis.com/auth/supportcontent',
      'https://www.googleapis.com/auth/chat.integration',
      'https://www.googleapis.com/auth/peopleapi.readonly',
    ]

    const form = {
      app_id: 'com.google.Dynamite',
      client_id: '576267593750-sbi1m7khesgfh1e0f2nv5vqlfa4qr72m.apps.googleusercontent.com',
      passcode_present: 'YES',
      response_type: 'token',
      scope: scope.join(' '),
    }

    const res = await this.http.requestAsString(issueTokenURL, {
      method: 'POST',
      headers,
      form,
    })

    if (res.statusCode !== 200) {
      console.error(`Error refreshing dynamite token: ${res.statusCode}`)
    }

    const json = JSON.parse(res.body)

    const { expiresIn, token } = json

    const expiration = new Date()
    expiration.setSeconds(expiration.getSeconds() + expiresIn)
    this.tokens.dynamiteToken = token
    this.tokens.dynamiteExpires = expiration
  }
}
