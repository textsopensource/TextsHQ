import { ActivityType, FetchOptions, Message, MessageBehavior, MessageContent, OnServerEventCallback, PaginationArg, ServerEvent, ServerEventType, texts, Thread } from '@textshq/platform-sdk'
// eslint-disable-next-line import/no-extraneous-dependencies
import debug from 'debug'
import { EventEmitter } from 'events'
import { promises as fs } from 'fs'
import { zip, groupBy } from 'lodash'

import { GROUP_THREADS_MESSAGES_TO_BACKFILL, GROUP_THREADS_TO_BACKFLL, MESSAGES_TO_BACKFILL, UPLOAD_URL } from '../constants'
import { Channel } from './channel'
import { GroupSubscriptionEvent, Message as GCMessage, CreateTopicRequest, Event, GetMembersRequest, MemberId, MessageInfo, PaginatedWorldRequest, PingEvent, RequestHeader, StreamEventsRequest, UserId, SetTypingStateRequest, TypingState, User, MarkGroupReadstateRequest, GetUserPresenceRequest, UpdateReactionRequest, Emoji, MessageId, MessageParentId, SetDndDurationRequest, SetPresenceSharedRequest, DeleteMessageRequest, WorldSectionRequest, UploadMetadata, Annotation, AnnotationType, ListTopicsRequest, ListMessagesRequest, CatchUpGroupRequest, CatchUpRange, GetGroupRequest, Member, CatchUpResponse, ClockSyncRequest, ClientNotificationsState, MessageReactionEvent } from './googlechat'
import { groupIdFromThreadId, mapMessage, mapReactionEvent, mapThread, removeThreadIdPrefix, threadIdFromGroupId } from './mappers'
import { ProtoRequest } from './protoRequest'
import { Session } from './session'
import type { GChatAuth } from './auth'
import type { GroupInfo } from './GroupInfo'

export class GChatClient {
  private session?: Session

  private channel?: Channel

  private requestHeader?: RequestHeader

  private selfId?: string

  private allMessages: GCMessage[] = []

  private allThreads: Thread[] = []

  private selfUser?: User

  private protoRequest?: ProtoRequest

  private allGroups: GroupInfo[] = []

  eventCallback?: OnServerEventCallback

  constructor(private authClient: GChatAuth) {
    this.session = new Session(this.authClient)
    this.channel = new Channel(this.session, this)
    this.protoRequest = new ProtoRequest(this.session)
  }

  connect = async () => {
    this.channel.channelEmitter = new EventEmitter()
    this.channel.channelEmitter.on('connect', () => {
      this.clientLog('Channel connected.')
    })
    this.channel.channelEmitter.on('disconnect', () => this.clientLog('Channel disconnected.'))
    this.channel.channelEmitter.on('stream', this.handleStreamEvents)

    await this.channel.listen()
  }

  dispose = () => {
    this.channel.dispose()
  }

  clientLog = (msg: string, ...args: string[]) => debug('gc:client')(`${msg}`, ...args)

  getSelfUser = async () => {
    if (this.selfUser === undefined) {
      const id = await this.getSelfUserId()
      const membersResponse = await this.getMembers([id])
      if (membersResponse) {
        this.selfUser = membersResponse.members[0].user
      }
    }
    return this.selfUser
  }

  getSelfUserId = async () => {
    if (this.selfId === undefined) {
      const status = await this.protoRequest.getSelfUserStatus()
      this.selfId = status.userStatus.userId.id
    }
    return this.selfId
  }

  getSelfUserStatus = () => this.protoRequest.getSelfUserStatus()

  getPresence = async (userIds: UserId[]) => {
    const res = await this.protoRequest.getUserPresence({
      userIds,
      includeActiveUntil: true,
      includeUserStatus: true,
    })
    return res
  }

  sendReadReceipt = async (threadID: string, messageID: string) => {
    const message = this.allMessages.find(m => m.id.messageId === messageID)
    if (message === undefined) return false
    const res = await this.protoRequest.markGroupReadState({
      id: groupIdFromThreadId(threadID),
      lastReadTime: Date.now() * 1000,
    })
    return !!res
  }

  sendTyping = async (type: ActivityType, threadID: string, off = false): Promise<void> => {
    this.clientLog(`Activity indicator: ${type}`)
    if (type === ActivityType.TYPING) {
      await this.protoRequest.setTypingState({
        requestHeader: this.requestHeader,
        context: {
          groupId: groupIdFromThreadId(threadID),
        },
        state: off ? TypingState.STOPPED : TypingState.TYPING,
      })
    }
  }

  getMembers = async (userIds: string[]) => {
    const membersRequest = {
      requestHeader: this.requestHeader,
      memberIds: userIds.map((userId: string) => MemberId.fromObject({
        userId: UserId.fromObject({
          id: userId,
        }),
      })),
    }
    const membersResponse = await this.protoRequest.getMembersRequest(membersRequest)
    return membersResponse
  }

  setActive = async () => {
    this.clientLog('Setting client to active')

    const pingEvent: PingEvent = PingEvent.fromObject({
      state: PingEvent.State.ACTIVE,
      clientNotificationsEnabled: true,
      notificationsState: ClientNotificationsState.create({
        deviceSettingState: ClientNotificationsState.DeviceNotificationSettingState.DEVICE_NOTIFICATIONS_ENABLED,
      }),
      applicationFocusState: PingEvent.ApplicationFocusState.FOCUS_STATE_FOREGROUND,
      clientInteractiveState: PingEvent.ClientInteractiveState.INTERACTIVE,
    })

    await this.channel.sendStreamEvent(StreamEventsRequest.create({ pingEvent }))

    this.clientLog(JSON.stringify(await this.getPresence([UserId.create({ id: await this.getSelfUserId() })]), null, 4))
  }

  updateReaction = async (threadID: string, messageID: string, reactionKey: string, remove = false) => {
    const type: UpdateReactionRequest.ReactionUpdateType = remove ? UpdateReactionRequest.ReactionUpdateType.REMOVE : UpdateReactionRequest.ReactionUpdateType.ADD
    const message = this.allMessages.find(m => m.id.messageId === messageID)
    const reaction = {
      messageId: MessageId.fromObject({ ...message.id }),
      emoji: Emoji.fromObject({
        unicode: reactionKey,
      }),
      type,
    }

    await this.protoRequest.updateReactionRequest(reaction)
  }

  parseBodies = (event: Event) => {
    let bodies: Event.IEventBody[]
    if (event.body) {
      bodies = [event.body]
    } else {
      bodies = event.bodies
    }
    return bodies
  }

  filterEventsByBodyType = (events: Event[], types: Event.EventType[]) => {
    const filtered: Event.IEventBody[] = []
    const bodies = events.map(event => this.parseBodies(event)).flat()
    filtered.push(...bodies.filter(body => types.includes(body.eventType)))
    return filtered
  }

  messagesFromEventBodies = (bodies: Event.IEventBody[]) => {
    const messages: GCMessage[] = []
    messages.push(...bodies.filter(body => body.eventType === Event.EventType.WEB_PUSH_NOTIFICATION).map(body => body.webPushNotification.notification.message))
    messages.push(...bodies.filter(body => body.eventType === Event.EventType.MESSAGE_POSTED).map(body => body.messagePosted.message))
    return messages
  }

  handleStreamEvents = async (events: Event[], catchup = false) => {
    const bodyMessages: GCMessage[] = []
    const bodyReactions = []
    for (const event of events) {
      // this.clientLog(`Processing event =>\n ${JSON.stringify(event, null, 2)}...`)

      if (event === null) continue
      const bodies = this.parseBodies(event)

      for (const body of bodies) {
        // this.clientLog(`Received ${Event_EventType[body.eventType]} event`)
        switch (body.eventType) {
          case Event.EventType.WEB_PUSH_NOTIFICATION:
            bodyMessages.push(body.webPushNotification.notification.message)
            break
          case Event.EventType.MESSAGE_POSTED:
            bodyMessages.push(body.messagePosted.message)
            break
          case Event.EventType.USER_PRESENCE_SHARED_UPDATED_EVENT:
            // this.clientLog(JSON.stringify(body.userStatusUpdated, null, 4))
            break
          case Event.EventType.MESSAGE_REACTED:
            bodyReactions.push((body.messageReaction))
            // this.clientLog(JSON.stringify(body.messageReaction, null, 4))
            break
          case Event.EventType.USER_STATUS_UPDATED_EVENT:
            // this.clientLog(JSON.stringify(body, null, 4))
            break
          case Event.EventType.USER_HUB_AVAILABILITY_UPDATED_EVENT:
            // this.clientLog(JSON.stringify(body, null, 4))
            break

          case Event.EventType.UNKNOWN:
            // this.clientLog(JSON.stringify(event, null, 4))
            break
          case Event.EventType.SESSION_READY:
            this.channel.channelEmitter.emit('connect')

            // await this.protoRequest.initUser()
            // await this.protoRequest.setUserSettings()

            await this.channel.sendStreamEvent(StreamEventsRequest.create({ clockSyncRequest: ClockSyncRequest.create({
              originTimeMsec: Date.now(),
            }),
            }))

            await this.protoRequest.setPresenceShared({
              presenceShared: true,
            })

            await this.protoRequest.doNotDisturbDuration({
              currentDndState: SetDndDurationRequest.State.AVAILABLE,
            })

            await this.setActive()

            break
          default:
            // this.clientLog(JSON.stringify(event, null, 4))
            break
        }
      }
    }

    await this.handleMessages(bodyMessages, catchup)
    await this.handleReactions(bodyReactions)
  }

  handleReactions = async (bodyReactions: MessageReactionEvent[]) => {
    const reactions = bodyReactions.map(mapReactionEvent)
    const serverEvents: ServerEvent[] = zip(reactions, bodyReactions).map(pair => {
      const event: ServerEvent = {
        type: ServerEventType.STATE_SYNC,
        objectIDs: {
          threadID: threadIdFromGroupId(pair[1].messageId.parentId.topicId.groupId),
          messageID: pair[1].messageId.messageId as string,
        },
        objectName: 'message_reaction',
        mutationType: 'update',
        entries: [
          pair[0],
        ],
      }
      return event
    })
    if (serverEvents.length) { this.eventCallback?.(serverEvents) }
  }

  handleMessages = async (bodyMessages: GCMessage[], catchup = false) => {
    const currentId = await this.getSelfUserId()

    const messages = bodyMessages.map(mapMessage).map(msg =>
      ({ ...msg, isSender: msg.senderID === currentId, behavior: catchup ? MessageBehavior.KEEP_READ : undefined }))

    await Promise.all(messages.map(async message => {
      for (const attach of message.attachments) {
        const res = await this.session.requestAuth(attach.srcURL, {})
        attach.data = res.body
        attach.srcURL = null
      }
    }))

    const groupedByThread = groupBy(messages, 'threadID')
    for (const [threadID, threadMessages] of Object.entries(groupedByThread)) {
      const currentThread = this.allThreads.find(thread => thread.id === threadID)
      if (!currentThread) continue
      const { participants } = currentThread
      for (const message of threadMessages) {
        for (const reaction of message.reactions) {
          if (reaction.participantID) continue
          const participantPosition = parseInt(reaction.id, 10)
          if (participants.items.length < participantPosition) {
            const participant = participants.items[participantPosition]
            reaction.id = `${participant.id}${reaction.reactionKey}`
            reaction.participantID = participant.id
          }
        }
      }
    }

    if (!catchup) {
      const serverEvents: ServerEvent[] = messages.map(message => ({
        type: ServerEventType.STATE_SYNC,
        objectIDs: {
          threadID: message.threadID,
        },
        objectName: 'message',
        mutationType: 'upsert',
        entries: [
          message,
        ],
      }))
      if (serverEvents.length) { this.eventCallback?.(serverEvents) }
    }

    this.allMessages.push(...bodyMessages)
    return messages
  }

  getUploadURL = async (threadID: string, content: MessageContent, fileLength: number) => {
    const options: FetchOptions = {
      method: 'POST',
      headers: {
        'x-goog-upload-protocol': 'resumable',
        'x-goog-upload-command': 'start',
        'x-goog-upload-content-length': fileLength.toString(),
        'x-goog-upload-content-type': content.mimeType,
        'x-goog-upload-file-name': content.fileName,
      },
      searchParams: {
        group_id: removeThreadIdPrefix(threadID),
      },
    }
    const res = await this.session.requestAuth(UPLOAD_URL, options)

    const uploadURL = res.headers['x-goog-upload-url']

    if (uploadURL) {
      return uploadURL as string
    }

    return null
  }

  uploadFile = async (threadID: string, content: MessageContent) => {
    const file = await fs.readFile(content.filePath)

    const uploadURL = await this.getUploadURL(threadID, content, file.byteLength)

    this.clientLog('Upload URL: ', uploadURL)

    const options: FetchOptions = {
      method: 'PUT',
      headers: {
        'x-goog-upload-command': 'upload, finalize',
        'x-goog-upload-protocol': 'resumable',
        'x-goog-upload-offset': '0',
      },
      body: file,
    }

    const res = await this.session.requestAuth(uploadURL as string, options)
    if (res.statusCode === 200) {
      const metadata = UploadMetadata.decode(Buffer.from(res.body.toString(), 'base64url'))
      return metadata
    }
    return null
  }

  sendMessage = async (threadID: string, content: MessageContent): Promise<Message[]> => {
    const annotations: Annotation[] = []
    if (content.fileName !== undefined) {
      const uploadMetadata = await this.uploadFile(threadID, content)
      if (uploadMetadata) {
        annotations.push(Annotation.fromObject({
          type: AnnotationType.UPLOAD_METADATA,
          uploadMetadata,
        }))
      }
    }

    let { text } = content

    // Hack: can't send an empty message when uploading a file (400) but we can send a space instead
    if (content.fileName && !text) {
      text = ' '
    }

    const request = {
      groupId: groupIdFromThreadId(threadID),
      textBody: text,
      historyV2: true,
      annotations,
      messageInfo: MessageInfo.fromObject({
        acceptFormatAnnotations: true,
      }),
    }

    await this.sendTyping(ActivityType.TYPING, threadID, true)
    const res = await this.protoRequest.createTopic(request)

    const lastReply = res.topic.replies[res.topic.replies.length - 1]

    return [
      {
        id: lastReply.id.messageId,
        senderID: await this.getSelfUserId(),
        text: content.text,
        timestamp: new Date(lastReply.createTime / 1000),
      },
    ]
  }

  deleteMessage = async (threadID: string, messageID: string) => {
    const groupInfo = this.allGroups.find(group => group.threadId === threadID)
    // deleting messages is only available for Workspace
    if (groupInfo?.isSpace) {
      const message = this.allMessages.find(m => m.id.messageId === messageID)
      const res = DeleteMessageRequest.fromObject({
        messageId: message.id,
      })
      await this.protoRequest.deleteMessage(res)
    }
  }

  private loadTopics = async (groupInfo: GroupInfo) => {
    const messages: GCMessage[] = []
    const listTopics = await this.protoRequest.listTopics({
      groupId: groupInfo.groupId,
      pageSizeForTopics: groupInfo.isThreaded ? GROUP_THREADS_TO_BACKFLL : MESSAGES_TO_BACKFILL,
    })

    // A topic will be a single message for a non threaded group, and a thread for a threaded group
    if (groupInfo.isThreaded) {
    // For threaded groups we need to retrieve messages for each topic individually
      await Promise.all(listTopics.topics.map(async topic => {
        const listMessages = await this.protoRequest.listMessages({
          parentId: MessageParentId.fromObject({ topicId: topic.id }),
          pageSize: GROUP_THREADS_MESSAGES_TO_BACKFILL,
        })
        this.clientLog(`Fetched ${listMessages.messages.length} messages for topic ${topic.id.topicId}`)
        messages.push(...listMessages.messages)
      }))
    } else {
    // For non threaded groups, each topic will be a single message and has a single "reply" which is the message
      messages.push(...listTopics.topics.map(topic => topic.replies[0]))
    }
    this.clientLog(`Fetched ${messages.length} total messages for group ${groupInfo.threadId}`)
    return messages
  }

  getMessages = async (threadID: string, pagination?: PaginationArg) => {
    const { cursor } = pagination || { cursor: null, direction: null }
    let mappedMessages: Message[]
    let hasMore = false
    if (cursor) {
      const message = this.allMessages.find(m => m.id.messageId === cursor)
      if (!message) return
      const groupId = groupIdFromThreadId(threadID)
      const catchupResponse = await this.protoRequest.catchupGroup({
        groupId,
        range: CatchUpRange.fromObject({
          fromRevisionTimestamp: 0,
          toRevisionTimestamp: message.createTime,
        }),
        pageSize: 100,
        cutoffSize: 100,
      })

      hasMore = !(catchupResponse.status === CatchUpResponse.ResponseStatus.COMPLETED)
      const filteredEvents = await this.filterEventsByBodyType(catchupResponse.events, [Event.EventType.WEB_PUSH_NOTIFICATION, Event.EventType.MESSAGE_POSTED])
      const messagesFromEvents = await this.messagesFromEventBodies(filteredEvents)
      mappedMessages = await this.handleMessages(messagesFromEvents, true)
    } else {
      const groupInfo = this.allGroups.find(group => group.threadId === threadID)
      if (!groupInfo) return
      const initialMessages = await this.loadTopics(groupInfo)
      mappedMessages = await this.handleMessages(initialMessages, true)
      hasMore = true
    }
    return {
      items: mappedMessages.sort((a, b) =>
        a.timestamp.getTime() - b.timestamp.getTime()),
      hasMore,
    }
  }

  getThreads = async (folderName: string, pagination?: PaginationArg) => {
    const { cursor } = pagination || { cursor: null, direction: null }
    if (!cursor) {
      // We have to load every thread at once, so we can't use pagination
      // This is an expensive operation, as it has to additionally fetch user information
      const threads = await this.loadGroups()
      return {
        items: threads,
        hasMore: false,
      }
    }
    return {
      items: [],
      hasMore: false,
    }
  }

  private loadGroups = async (): Promise<Thread[]> => {
    const response = await this.protoRequest.paginatedWorld({
      fetchFromUserSpaces: true,
      fetchOptions: [PaginatedWorldRequest.FetchOptions.EXCLUDE_GROUP_LITE],
      worldSectionRequests: [WorldSectionRequest.fromObject({ pageSize: 5 })],
    })

    if (!response) {
      // no groups?
      return []
    }

    // We will also need users to properly build the group information
    const prefetchUsers = new Set<string>()
    for (const item of response.worldItems) {
      item.dmMembers?.members.forEach(member => prefetchUsers.add(member.id))
      item.nameUsers?.nameUserIds.forEach(member => prefetchUsers.add(member.id))
    }

    const users = await this.getMembers(Array.from(prefetchUsers))
    const threads: Thread[] = []
    await Promise.all(response.worldItems.map(async item => {
      const { groupId } = item
      let members: UserId[]
      let userInfo: Member[]
      let isSpace: boolean

      // On the API, both group conversations and spaces will have "spaceId"
      // Actual spaces do not have either dmMembers or nameUsers and we want to skip them

      if (item.dmMembers === null && item.nameUsers === null) {
        isSpace = true
        // We need to fetch information for the space
        const groupResponse = await this.protoRequest.getGroup({
          groupId: item.groupId,
          fetchOptions: [
            GetGroupRequest.FetchOptions.MEMBERS,
            GetGroupRequest.FetchOptions.INCLUDE_DYNAMIC_GROUP_NAME,
          ],
        })

        // Now we fetch information about the space's users
        const membersResponse = await this.getMembers(groupResponse.memberships.map(m => m.id.memberId.userId.id))
        members = groupResponse.memberships.map(m => m.id.memberId.userId)
        userInfo = membersResponse.members
      } else {
        // We already fetched information for all users that are not in spaces
        members = item.dmMembers ? item.dmMembers.members : item.nameUsers.nameUserIds
        userInfo = users.members
      }

      const groupInfo: GroupInfo = {
        groupId,
        threadId: threadIdFromGroupId(groupId),
        isThreaded: !!item.threadedGroup,
        isSpace,
        roomName: item.roomName,
        sortTimestamp: item.sortTimestamp,
      }

      const thread = mapThread(groupInfo, members, userInfo, this.selfUser)
      this.allGroups.push(groupInfo)
      this.allThreads.push(thread)
      threads.push(thread)
    }))

    // We will subscribe to each thread to receive events
    const event = StreamEventsRequest.fromObject({
      groupSubscriptionEvent: GroupSubscriptionEvent.fromObject({
        groupIds: this.allGroups.map(group => group.groupId),
      }),
    })

    await this.channel.sendStreamEvent(event)

    return threads
  }
}
