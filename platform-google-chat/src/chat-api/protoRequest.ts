import { FetchOptions, texts } from '@textshq/platform-sdk'
// eslint-disable-next-line import/no-extraneous-dependencies
import { API_KEY, GC_BASE_URL } from '../constants'
import { UpdateUserSettingsRequest, UpdateUserSettingsResponse, InitUserResponse, CatchUpGroupRequest, CatchUpResponse, ClientFeatureCapabilities, CreateTopicRequest, CreateTopicResponse, DeleteMessageRequest, DeleteMessageResponse, GetGroupRequest, GetGroupResponse, GetMembersRequest, GetMembersResponse, GetSelfUserStatusRequest, GetSelfUserStatusResponse, GetUserPresenceRequest, GetUserPresenceResponse, InitUserRequest, ListMessagesRequest, ListMessagesResponse, ListTopicsRequest, ListTopicsResponse, MarkGroupReadstateRequest, MarkGroupReadstateResponse, PaginatedWorldRequest, PaginatedWorldResponse, RequestHeader, SetCustomStatusRequest, SetCustomStatusResponse, SetDndDurationRequest, SetDndDurationResponse, SetPresenceSharedRequest, SetPresenceSharedResponse, SetTypingStateRequest, SetTypingStateResponse, UpdateReactionRequest, UpdateReactionResponse, MemberId, WorldSectionRequest, CatchUpRange, TypingState, Annotation, GroupId, MessageInfo, UserId, Emoji, MessageId, MessageParentId } from './googlechat'
import type { Session } from './session'

export class ProtoRequest {
  private readonly requestHeader?: RequestHeader = RequestHeader.fromObject({
    clientType: RequestHeader.ClientType.IOS,
    clientVersion: 2440378181258,
    clientFeatureCapabilities: ClientFeatureCapabilities.fromObject(
      {
        spamRoomInvitesLevel: ClientFeatureCapabilities.CapabilityLevel.FULLY_SUPPORTED,
      },
    ),
  })

  constructor(private readonly session?: Session) { }

  getSelfUserStatus = async () => this.protoRequestDecode('get_self_user_status', GetSelfUserStatusRequest, GetSelfUserStatusResponse)

  getMembersRequest = async (obj: { requestHeader?: RequestHeader, memberIds?: MemberId[] }) => this.protoRequestDecode('get_members', GetMembersRequest, GetMembersResponse, obj)

  paginatedWorld = async (obj: { fetchFromUserSpaces?: boolean, fetchOptions?: PaginatedWorldRequest.FetchOptions[], worldSectionRequests?: WorldSectionRequest[] }) => this.protoRequestDecode('paginated_world', PaginatedWorldRequest, PaginatedWorldResponse, obj)

  getGroup = async (obj: { groupId?: any, fetchOptions?: GetGroupRequest.FetchOptions[] }) => this.protoRequestDecode('get_group', GetGroupRequest, GetGroupResponse, obj)

  catchupGroup = async (obj: { groupId?: GroupId, range?: CatchUpRange, pageSize?: number, cutoffSize?: number }) => this.protoRequestDecode('catch_up_group', CatchUpGroupRequest, CatchUpResponse, obj)

  setTypingState = async (obj: { requestHeader?: RequestHeader, context?: { groupId: GroupId }, state?: TypingState }) => this.protoRequestDecode('set_typing_state', SetTypingStateRequest, SetTypingStateResponse, obj)

  listTopics = async (obj: { groupId?: GroupId, pageSizeForTopics?: number }) => this.protoRequestDecode('list_topics', ListTopicsRequest, ListTopicsResponse, obj)

  getSelfUsercreateTopictatus = async () => this.protoRequestDecode('get_self_user_status', ListTopicsRequest, ListTopicsResponse)

  createTopic = async (obj: { groupId?: GroupId, textBody?: string, historyV2?: boolean, annotations?: Annotation[], messageInfo?: MessageInfo }) => this.protoRequestDecode('create_topic', CreateTopicRequest, CreateTopicResponse, obj)

  deleteMessage = async (obj: DeleteMessageRequest) => this.protoRequestDecode('delete_message', DeleteMessageRequest, DeleteMessageResponse, obj)

  getUserPresence = async (obj: { userIds?: UserId[], includeActiveUntil?: boolean, includeUserStatus?: boolean }) => this.protoRequestDecode('get_user_presence', GetUserPresenceRequest, GetUserPresenceResponse, obj)

  setPresenceShared = async (obj: { presenceShared?: boolean }) => this.protoRequestDecode('set_presence_shared', SetPresenceSharedRequest, SetPresenceSharedResponse, obj)

  doNotDisturbDuration = async (obj: { currentDndState?: SetDndDurationRequest.State }) => this.protoRequestDecode('set_dnd_duration', SetDndDurationRequest, SetDndDurationResponse, obj)

  setCustomStatus = async () => this.protoRequestDecode('set_custom_status', SetCustomStatusRequest, SetCustomStatusResponse)

  markGroupReadState = async (obj: { id?: GroupId, lastReadTime?: number }) => this.protoRequestDecode('mark_group_readstate', MarkGroupReadstateRequest, MarkGroupReadstateResponse, obj)

  updateReactionRequest = async (obj: { messageId?: MessageId, emoji?: Emoji, type?: UpdateReactionRequest.ReactionUpdateType }) => this.protoRequestDecode('update_reaction', UpdateReactionRequest, UpdateReactionResponse, obj)

  listMessages = async (obj: { parentId?: MessageParentId, pageSize?: number }) => this.protoRequestDecode('list_messages', ListMessagesRequest, ListMessagesResponse, obj)

  initUser = async () => this.protoRequestDecode('init_user', InitUserRequest, InitUserResponse)

  setUserSettings = async () => this.protoRequestDecode('set_user_settings', UpdateUserSettingsRequest, UpdateUserSettingsResponse)

  protoRequestDecode = async (topic: string, request, response, requestObj = {}) => {
    const req = request.fromObject({
      ...requestObj,
      requestHeader: this.requestHeader,
    })

    // prevent mishaps since we are losing typing info
    if (texts.IS_DEV) {
      const topicWithoutUnderscore = topic.replace(/_/g, '').toLocaleLowerCase()
      if (request.name.toLocaleLowerCase() !== `${topicWithoutUnderscore}request` || response.name.toLocaleLowerCase() !== `${topicWithoutUnderscore}response`) {
        texts.log(`Possible mismatch: ${request.name} ${response.name} ${topic}`)
      }
    }

    try {
      const res = await this.request(topic, Buffer.from(request.encode(req).finish()))
      if (res.statusCode === 200) {
        return response.decode(res.body)
      }
      texts.error(`${request.name} ${response.name} ${topic} ${response.statusCode}`)
    } catch (e) {
      texts.Sentry.captureMessage(`${request.name} ${response.name} ${topic} ${e.message}`)
      throw e
    }
  }

  request = async (endpoint: string, request: Buffer) => {
    const options: FetchOptions = { headers: { 'Content-Type': 'application/x-protobuf' }, body: request }
    const res = await this.baseRequest(`${GC_BASE_URL}/api/${endpoint}`, 'proto', options)
    return res
  }

  public baseRequest = async (url: string, responseType: string, options: FetchOptions) => {
    const requestOptions = {
      ...options,
    }
    if (responseType === 'proto') {
      requestOptions.headers['X-Goog-Encode-Response-If-Executable'] = 'base64'
    }

    if (options.searchParams === undefined) {
      requestOptions.searchParams = {}
    }

    requestOptions.method = 'POST'
    requestOptions.searchParams.alt = responseType
    requestOptions.searchParams.key = API_KEY
    requestOptions.searchParams.rt = 'b'

    const res = await this.session.requestAuth(url, requestOptions)
    return res
  }
}
