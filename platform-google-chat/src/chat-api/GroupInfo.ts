import type { GroupId } from './googlechat'

// Used for storing information about groups
export class GroupInfo {
  threadId: string

  groupId: GroupId

  isThreaded: boolean

  isSpace: boolean

  roomName: string

  sortTimestamp: Long
}
