import { FetchResponse, texts } from '@textshq/platform-sdk'
// eslint-disable-next-line import/no-extraneous-dependencies
import debug from 'debug'
import type { EventEmitter } from 'events'
import { Agent, request } from 'node:https'
import type { Params } from '../types'
import type { Session } from './session'
import { CHANNEL_URL_BASE, GC_BASE_URL, TWO_MINUTES } from '../constants'
import { ClientNotificationsState, PingEvent, StreamEventsRequest, StreamEventsResponse } from './googlechat'
import type { GChatClient } from '.'

export class Channel {
  private session?: Session

  private sessionParams: Params

  private connected = false

  channelEmitter?: EventEmitter

  private client: GChatClient

  private lastRequest?: number

  private watch?: NodeJS.Timer

  private disposed = false

  constructor(session: Session, client: GChatClient) {
    this.sessionParams = {
      lastArrayId: 0,
      lastOfs: 0,
      requestId: 0,
      csessionid: null,
      sessionId: null,
    }
    this.session = session
    this.client = client
    // we need to register once in a while, around once per hour
    this.watch = setInterval(async () => {
      await this.watchListen()
    }, TWO_MINUTES)
  }

  watchListen = async () => {
    if (this.disposed) return
    texts.log('Checking if we need to register again.')
    // 2 minutes
    if (Date.now() - this.lastRequest > TWO_MINUTES) {
      // should only be required once an hour
      texts.log('Registering again...')
      await this.register()
    }
    // reuses same underlying connection
    if (!this.connected) {
      await this.longPollRequest()
    }
  }

  listen = async () => {
    await this.register()
    await this.longPollRequest()
  }

  register = async (): Promise<string> => {
    const headers = {
      'Content-Type': 'application/x-protobuf',
    }

    const res = await this.session.requestAuth(`${CHANNEL_URL_BASE}register`, { method: 'POST', headers })
    if (res.statusCode === 200) {
      const compass = await this.session.getCookie(CHANNEL_URL_BASE, 'COMPASS')
      if (compass != null && compass.startsWith('dynamite')) {
        return compass.split('=')[1]
      }
    }
    return null
  }

  longPollRequest = async () => {
    let params: any = {
      VER: 8,
      CVER: 22,
      AID: this.sessionParams.lastArrayId,
      t: 1,
    }

    if (this.sessionParams.sessionId === null) {
      params = {
        ...params,
        $req: 'count=0',
        RID: 0,
        SID: 'null',
        TYPE: 'init',
      }
    } else {
      params = {
        ...params,
        CI: 0,
        RID: 'rpc',
        SID: this.sessionParams.sessionId,
        TYPE: 'xmlhttp',
      }
    }

    if (this.sessionParams.csessionid != null) {
      params.csessionid = this.sessionParams.csessionid
    }

    this.sessionParams.requestId++

    const req = await this.session.eventsLongPoll(params, (async res => {
      await this.initialResponse(res)
      res.on('data', async chunk => { await this.poll(chunk) })
      res.on('end', async () => {
        this.channelEmitter.emit('disconnect')
        this.connected = false
        await this.watchListen()
      })
    }))
    req.end()
  }

  private poll = async chunk => {
    try {
      this.lastRequest = Date.now()
      await this.parseData(chunk)
    } catch (e) {
      texts.log(`Exception: ${e.message}`)
    }
  }

  private initialResponse = async res => {
    texts.log(`Received response from webchannel: ${res.statusCode}`)
    const initialResponse = res.headers['x-http-initial-response']
    if (initialResponse != null) {
      texts.log('Received initial response')
      const sidJson = JSON.parse(initialResponse.toString())
      const sid = sidJson[0][1][1]
      texts.log(`SID: ${initialResponse.toString()} ${sid}`)

      if (this.sessionParams.sessionId !== sid) {
        texts.log('Updating SID')
        this.sessionParams.sessionId = sid
        this.sessionParams.lastArrayId = 0
        this.sessionParams.lastOfs = 0

        const pingEvent: PingEvent = PingEvent.fromObject({
          state: PingEvent.State.ACTIVE,
          clientNotificationsEnabled: true,
          notificationsState: ClientNotificationsState.create({
            deviceSettingState: ClientNotificationsState.DeviceNotificationSettingState.DEVICE_NOTIFICATIONS_ENABLED,
          }),
          applicationFocusState: PingEvent.ApplicationFocusState.FOCUS_STATE_FOREGROUND,
          clientInteractiveState: PingEvent.ClientInteractiveState.INTERACTIVE,
        })

        const streamEvents: StreamEventsRequest = StreamEventsRequest.fromObject({
          pingEvent,
        })

        await this.sendStreamEvent(streamEvents)
      }
    }
  }

  parseData = async (data: Buffer) => {
    const chunks = await this.parseChunks(data)

    if (!this.connected) {
      this.connected = true
    }

    const events = []

    for (const chunk of chunks) {
      const container = JSON.parse(chunk)[0]
      const arrayId = container[0]
      const arrayData = container[1]
      this.sessionParams.lastArrayId = arrayId
      for (const inner of arrayData) {
        if (inner === 'noop') {
          texts.log('Received event: noop')
          continue
        }

        const innerData = inner.data
        if (innerData === undefined) {
          continue
        }

        const streamResponse = StreamEventsResponse.decode(Buffer.from(innerData, 'base64'))
        if (streamResponse.event === null) texts.log(`Event was null: ${container}`)
        events.push(streamResponse.event)
      }
    }

    if (events.length) {
      this.channelEmitter.emit('stream', events)
    }
  }

  private previousDecoded = ''

  parseChunks = async (data: Buffer) => {
    const chunks = []
    let decoded = data.toString().split('\n').join('')
    decoded = this.previousDecoded + decoded
    while (decoded.length > 0) {
      let currentPosition = 0
      const length = parseInt(decoded, 10)
      if (Number.isNaN(length)) {
        break
      }
      currentPosition += length.toString().length

      if (decoded.length < length.toString().length + length) {
        break
      }

      const currentChunk = decoded.substring(currentPosition, currentPosition + length)
      chunks.push(currentChunk)
      currentPosition += length
      decoded = decoded.substring(currentPosition)
    }
    this.previousDecoded = decoded
    return chunks
  }

  sendStreamEvent = async (eventsRequest: StreamEventsRequest): Promise<FetchResponse<Buffer>> => {
    const params: any = {
      VER: 8,
      RID: this.sessionParams.requestId - 1,
      SID: this.sessionParams.sessionId,
      AID: this.sessionParams.lastArrayId,
      t: '1',
    }

    if (this.sessionParams.csessionid) {
      params.csessionid = this.sessionParams.csessionid
    }

    const headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    const encoded = StreamEventsRequest.encode(eventsRequest).finish()

    const form: Record<string, string | number> = {
      count: '1',
      ofs: this.sessionParams.lastOfs,
      req0___data__: JSON.stringify({ data: Buffer.from(encoded).toString('base64') }),
    }

    this.sessionParams.lastOfs++
    this.sessionParams.requestId++

    const res = await this.session.requestAuth(`${CHANNEL_URL_BASE}events_encoded`, { method: 'POST', searchParams: params, form, headers })
    texts.log(`Stream response status: ${res.statusCode}`)
    texts.log(JSON.stringify(res.body.toString().replace('\n', '')))
    return res
  }

  dispose = () => {
    if (this.watch) clearInterval(this.watch)
    this.disposed = true
  }
}
