import type { FetchOptions } from '@textshq/platform-sdk'
import { texts } from '@textshq/platform-sdk'
import { CookieJar } from 'tough-cookie'
import * as https from 'https'
import type { GChatAuth } from './auth'

const { constants } = texts

const commonHeaders = {
  'Accept-Language': 'en',
  'Sec-Fetch-Dest': 'empty',
  'Sec-Fetch-Mode': 'cors',
  'Sec-Fetch-Site': 'same-site',
  'User-Agent': constants.USER_AGENT,
}

export class Session {
  private authClient?: GChatAuth

  private jar: CookieJar = new CookieJar()

  private httpClient = texts.createHttpClient!()

  private agent: https.Agent

  constructor(authClient: GChatAuth) {
    this.authClient = authClient
    // we need to use the underlying connection so we don't have to reregister each time
    this.agent = new https.Agent({
      keepAlive: true,
      maxSockets: 1,
      keepAliveMsecs: 3000,
    })
  }

  requestAuth = async (url: string, options: FetchOptions) => {
    const tokens = await this.authClient.getTokens()
    const requestOptions = {
      ...options,
    }
    requestOptions.headers = {
      Authorization: `Bearer ${tokens.dynamiteToken}`,
      Connection: 'Keep-Alive',
      ...options.headers,
    }
    const res = this.request(url, requestOptions)
    return res
  }

  request = async (url: string, options: FetchOptions) => {
    const requestOptions = {
      ...options,
    }
    requestOptions.cookieJar = this.jar
    requestOptions.headers = {
      ...commonHeaders,
      ...options.headers,
    }
    const res = this.httpClient.requestAsBuffer(url, requestOptions)
    return res
  }

  eventsLongPoll = async (params, cb) => {
    const tokens = await this.authClient.getTokens()
    const options: https.RequestOptions = {
      agent: this.agent,
      hostname: 'chat.google.com',
      path: '/webchannel/events_encoded?' + new URLSearchParams(params),
      headers:
          {
            Authorization: `Bearer ${tokens.dynamiteToken}`,
            Connection: 'Keep-Alive',
          },
    }
    return https.request(options, cb)
  }

  getCookie = (url: string, key: string): string => {
    let value = null
    this.jar.getCookies(url, (err, cookies) => {
      if (!err) {
        const cookie = cookies.find(e => e.key === key)
        if (cookie !== undefined) value = cookie.value
      }
    })
    return value
  }
}
