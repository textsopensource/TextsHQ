import { Message, MessageAttachment, MessageAttachmentType, MessageReaction, Participant, Thread } from '@textshq/platform-sdk'

import { ATTACHMENT_URL } from '../constants'
import { Annotation, AnnotationType, DmId, GroupId, Member, Message as GCMessage, MessageReactionEvent, SpaceId, User as GCUser, UserId } from './googlechat'
import type { GroupInfo } from './GroupInfo'

// GroupId can either contain a spaceId (for spaces and group conversations) or a dmId (private messages)
// Internally we need a way to distinguish them
// We do this by prefixing the id with "dm:" or "space:"
// We must remove the prefix before using in an API call
export const threadIdFromGroupId = (groupId: GroupId) =>
  (groupId.dmId
    ? `dm:${groupId.dmId.dmId}`
    : `space:${groupId.spaceId.spaceId}`)

export const removeThreadIdPrefix = (threadId: string) =>
  (threadId.startsWith('dm:')
    ? threadId.substring('dm:'.length)
    : threadId.substring('space:'.length))

export function groupIdFromThreadId(threadId: string): GroupId {
  if (threadId.startsWith('dm:')) {
    return GroupId.fromObject({
      dmId: DmId.fromObject({ dmId: removeThreadIdPrefix(threadId) }),
    })
  }
  return GroupId.fromObject(
    {
      spaceId: SpaceId.fromObject({
        spaceId: removeThreadIdPrefix(threadId),
      }),
    },
  )
}

export function mapParticipant(member: Member): Participant {
  if (!member) return
  return {
    id: member.user.userId.id,
    fullName: member.user.name,
    // todo review:
    cannotMessage: (member.user.blockRelationship !== null ?? member.user.blockRelationship.hasBlockedRequester),
    imgURL: member.user.avatarUrl,
    email: member.user.email,
  }
}

export function mapThread(groupInfo: GroupInfo, memberships: UserId[], members: Member[], selfUser: GCUser): Thread {
  const participants = memberships.map<Participant>(membership => {
    if (membership.id === selfUser.userId.id) return
    const mapping = members.find(member => member.user.userId.id === membership.id)
    return mapping ? mapParticipant(mapping) : undefined
  }).filter(Boolean)
  return {
    _original: JSON.stringify([groupInfo, memberships]),
    id: groupInfo.threadId,
    title: groupInfo.isSpace ? groupInfo.roomName : undefined,
    type: groupInfo.groupId.spaceId ? 'group' : 'single',
    messages: {
      items: [],
      hasMore: true,
    },
    isUnread: false,
    isReadOnly: false,
    participants: {
      items: participants,
      hasMore: false,
    },
    timestamp: groupInfo.sortTimestamp ? new Date(Math.trunc(+groupInfo.sortTimestamp / 1000)) : undefined,
  }
}
export function mapAttachments(annotations: Annotation[]): MessageAttachment[] {
  const attachments: MessageAttachment[] = []
  for (const annotation of annotations) {
    if (!annotation.uploadMetadata || !annotation.uploadMetadata.contentType.startsWith('image/')) {
      continue
    }

    const attachment: MessageAttachment = {
      id: annotation.uploadMetadata.localId,
      fileName: annotation.uploadMetadata.contentName,
      type: MessageAttachmentType.IMG,
      mimeType: annotation.uploadMetadata.contentType,
      srcURL: `${ATTACHMENT_URL}?url_type=FIFE_URL&attachment_token=${encodeURIComponent(annotation.uploadMetadata.attachmentToken)}`,
    }

    attachments.push(attachment)
  }
  return attachments
}

export function mapReactionsFromMessage(message: GCMessage): MessageReaction[] {
  const { reactions } = message
  const mappedReactions: MessageReaction[] = []
  for (const reaction of reactions) {
    for (let i = 0; i < reaction.count; i++) {
      // we will later use this to index into participants and we want to skip the first one if we didn't react
      if (i === 0 && !reaction.currentUserParticipated) continue
      mappedReactions.push({
        id: null,
        participantID: i.toString(),
        reactionKey: reaction.emoji.unicode,
        emoji: true,
      })
    }
  }
  return mappedReactions
}

export function mapMessage(message: GCMessage): Message {
  const messageText = message.textBody
  let isAction = false

  const attachments = mapAttachments(message.annotations)
  const membershipChanged: Annotation[] = message.annotations.filter(a => a.type === AnnotationType.MEMBERSHIP_CHANGED)
  if (membershipChanged.length) {
    isAction = true
  }

  const { topicId } = message.id.parentId.topicId
  return {
    _original: JSON.stringify(message),
    id: message.id.messageId,
    threadID: threadIdFromGroupId(message.id.parentId.topicId.groupId),
    text: messageText,
    timestamp: new Date(message.createTime as number / 1000),
    senderID: message.creator.userId.id,
    seen: true,
    isDelivered: true,
    attachments,
    isAction,
    linkedMessageID: topicId !== message.id.messageId ? topicId : null,
    reactions: mapReactionsFromMessage(message),
  }
}

export function mapReactionEvent(reactionEvent: MessageReactionEvent): MessageReaction {
  const reaction: MessageReaction = {
    id: `${reactionEvent.userId.id}${reactionEvent.emoji.unicode}`,
    reactionKey: reactionEvent.emoji.unicode,
    participantID: reactionEvent.userId.id,
    emoji: true,
  }
  return reaction
}
