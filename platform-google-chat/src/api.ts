/* eslint-disable @typescript-eslint/no-throw-literal */
import { texts, ActivityType, Awaitable, CurrentUser, LoginCreds, LoginResult, Message, OnServerEventCallback, Paginated, PaginationArg, PlatformAPI, Thread, User, MessageContent, MessageSendOptions, ReAuthError } from '@textshq/platform-sdk'
import { GChatAuth } from './chat-api/auth'
import { GChatClient } from './chat-api'
import type { GChatTokens } from './types'

const { IS_DEV } = texts

if (IS_DEV) {
  // eslint-disable-next-line import/no-extraneous-dependencies, global-require
  require('source-map-support').install()
}

export default class GoogleChat implements PlatformAPI {
  private readonly authClient = new GChatAuth()

  protected client?: GChatClient

  init = async (tokens?: GChatTokens) => {
    if (!tokens) {
      return
    }
    this.client = new GChatClient(this.authClient)
    this.authClient.setTokens(tokens)
    await this.authClient.refreshOAuth()
    await this.afterAuth()
  }

  dispose = () => { this.client.dispose() }

  getCurrentUser = async (): Promise<CurrentUser> => {
    const currentUser = await this.client.getSelfUserStatus()
    if (!currentUser) throw new ReAuthError('Not logged in.')
    const users = await this.client.getMembers([currentUser.userStatus.userId.id])
    const { user } = users.members[0]
    return { displayText: user.name, id: user.userId.id }
  }

  login = async (creds?: LoginCreds): Promise<LoginResult> => {
    if (creds) {
      if (IS_DEV) console.log('[+] Logging from embedded browser')
      const authorizationCode = creds.cookieJarJSON.cookies.find(e => e.key === 'oauth_code').value
      await this.authClient.authWithCode(authorizationCode)
    } else if (IS_DEV) console.log('[+] Logging from stored refresh token')
    if (this.authClient.getTokens() == null) {
      return { type: 'error', title: 'Failed at retrieving access token.' }
    }

    this.client = new GChatClient(this.authClient)
    await this.afterAuth()
    return { type: 'success', title: 'done' }
  }

  afterAuth = async () => {
    await this.client.connect()
    await this.client.getSelfUser() // this is to cache the self user
  }

  private static readonly emptyPagination: Paginated<any> = { items: [], hasMore: false }

  sendMessage = async (threadID: string, content: MessageContent, options?: MessageSendOptions): Promise<Message[]> => this.client.sendMessage(threadID, content)

  getThreads = async (folderName: string, pagination?: PaginationArg): Promise<Paginated<Thread>> => this.client.getThreads(folderName, pagination)

  updateThread = async (threadID: string, updates: Partial<Thread>): Promise<boolean> => true

  subscribeToEvents = (onEvent: OnServerEventCallback) => {
    this.client.eventCallback = onEvent
  }

  searchUsers = (typed: string): Awaitable<User[]> => undefined

  getMessages = (threadID: string, pagination?: PaginationArg): Awaitable<Paginated<Message>> => this.client.getMessages(threadID, pagination)

  createThread = (userIDs: string[], title?: string, messageText?: string): Awaitable<boolean | Thread> => true

  deleteMessage = async (threadID: string, messageID: string, forEveryone?: boolean) => {
    await this.client.deleteMessage(threadID, messageID)
    return true
  }

  sendActivityIndicator = (type: ActivityType, threadID: string): Awaitable<void> => this.client.sendTyping(type, threadID)

  sendReadReceipt = (threadID: string, messageID: string): Awaitable<boolean> => this.client.sendReadReceipt(threadID, messageID)

  addReaction = (threadID: string, messageID: string, reactionKey: string) => this.client.updateReaction(threadID, messageID, reactionKey)

  removeReaction = (threadID: string, messageID: string, reactionKey: string) => this.client.updateReaction(threadID, messageID, reactionKey, true)

  serializeSession = () => this.authClient.getTokens()
}
