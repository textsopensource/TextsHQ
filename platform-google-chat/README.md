# platform-google-chat

## Endpoints

**API**
**https://chat.google.com/api/**
```
get_space_summaries
get_unread_invites_state
get_user_presence
get_user_settings
get_user_status
get_working_hours
get_world_lite
get_world
catch_up_group
hide_group
init_user
list_blocked_groups
list_blocked_users
list_custom_emojis
list_files
list_invited_rooms
list_members
list_message_flight_logs
list_messages
list_reactors
list_spam_invited_dms
list_topics
manage_blocked_rooms
mark_group_readstate
mark_topic_readstate
modify_space_target_audience
paginated_world
post_message_flight_logs
xsrf_token
remove_memberships
search_topics
search_messages
search_messages_v2
send_to_inbox
set_custom_status
set_dnd_duration
set_mark_as_unread_timestamp
set_presence_shared
set_typing_state
set_working_hours
star_group
sync_custom_emojis
sync
trigger_group_cache_invalidation
trigger_user_cache_invalidation
update_group_notification_settings
update_group
update_group_retention_settings
update_membership_role
update_reaction
update_retention_settings
mark_Topic_mute_state
update_unread_invites_state
set_user_settings
catch_up_user
```

**WebChannel**
**https://chat.google.com/webchannel/**
```
register
events_encoded
```

**Integration Service**
**https://chat.google.com/v1/**
```
submit_form_action
```

**Blobstore**
**https://chat.google.com/**
```
api/get_attachment_url
api/get_attachment_url
api/get_projector_config
uploads
```